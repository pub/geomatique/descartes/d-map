function load() {
	
	initExampleMapContentSelect();
	
	Descartes.Log.setLevel('debug');
	chargeCouchesGroupes();
	groupeInfrasSpec.options.opened=true;
	loadContext();
}

function doMap(){
	
	Descartes.Messages.Descartes_Messages_UI_GazetteerInPlace.BUTTON_MESSAGE="<i class=\"fa fa-crosshairs\"></i> Localiser";
	Descartes.Messages.Descartes_Messages_UI_GazetteerInPlace.TITLE_MESSAGE="<i class=\"fa fa-map-marker\"></i> Localisation parcellaire";
	Descartes.Messages.Descartes_Messages_UI_LocalisationAdresseInPlace.TITLE_MESSAGE="<i class=\"fa fa-map-marker\"></i> Localisation à l'adresse";
	Descartes.Messages.Descartes_Messages_UI_ScaleChooserInPlace.BUTTON_MESSAGE= "<i class=\"fa fa-check\"></i> Activer";
	Descartes.Messages.Descartes_Messages_UI_ScaleChooserInPlace.TITLE_MESSAGE="<i class=\"fa fa-list\"></i> Choisir l\'échelle de la carte";
	Descartes.Messages.Descartes_Messages_UI_ScaleSelectorInPlace.TITLE_MESSAGE="<i class=\"fa fa-list\"></i> Choisir l\'échelle de la carte";
	Descartes.Messages.Descartes_Messages_UI_SizeSelectorInPlace.TITLE_MESSAGE="<i class=\"fa fa-image\"></i> Choisir la taille de la carte";
	Descartes.Messages.Descartes_Messages_UI_CoordinatesInputInPlace.BUTTON_MESSAGE="<i class=\"fa fa-crosshairs\"></i> Recentrer";
	Descartes.Messages.Descartes_Messages_UI_CoordinatesInputInPlace.TITLE_MESSAGE="<i class=\"fa fa-edit\"></i> Choisir les coordonnées";
	Descartes.Messages.Descartes_Messages_UI_PrinterSetupInPlace.BUTTON_MESSAGE="<i class=\"fa fa-file\"></i> Générer le PDF";
	Descartes.Messages.Descartes_Messages_UI_PrinterSetupInPlace.TITLE_MESSAGE="<i class=\"fa fa-wrench\"></i> Paramètres de la mise en page";
	Descartes.Messages.Descartes_Messages_UI_BookmarksInPlace.TITLE_MESSAGE="<i class=\"fa fa-cog\"></i> Gérer des vues personnalisées";
	Descartes.Messages.Descartes_Messages_UI_BookmarksInPlace.BUTTON_MESSAGE="<i class=\"fa fa-save\"></i> Enregistrer la vue courante";
	Descartes.Messages.Descartes_Messages_UI_RequestManagerInPlace.TITLE_MESSAGE= '<i class=\"fa fa-list\"></i> Requêtes attributaires';
	Descartes.Messages.Descartes_Messages_UI_RequestManagerInPlace.BUTTON_MESSAGE_RECHERCHE="<i class=\"fa fa-search\"></i> Rechercher";
	Descartes.Messages.Descartes_Messages_UI_RequestManagerInPlace.BUTTON_MESSAGE_UNSELECT="<i class=\"fa fa-trash\"></i> Effacer la sélection";
    Descartes.Messages.Descartes_Messages_UI_PrinterSetupDialog.DIALOG_TITLE= "<i class=\"fa fa-wrench\"></i> Paramètres de la mise en page";
    Descartes.Messages.Descartes_Messages_UI_PrinterSetupDialog.OK_BUTTON= "<i class=\"fa fa-file\"></i> Générer le PDF";
    Descartes.Messages.Descartes_Messages_UI_PrinterSetupDialog.CANCEL_BUTTON= "<i class=\"fa fa-times\"></i> Annuler";
    Descartes.Messages.Descartes_Messages_Info_Legend.TITLE_MESSAGE= "<i class=\"fa fa-image\"></i> Légende";
	
	Descartes.Layer.ResultLayer.config.displayMarker = true;

	var contenuCarte = new Descartes.MapContent({
		editable: true, 
	    editInitialItems:true,
		displayMoreInfos:true,
		fctQueryable: true,
		fctOpacity:true,
		fctDisplayLegend:true,
		fctMetadataLink: true,
		fctContextMenu:true
	});

	contenuCarte.populate(context.items);

	initBounds = [context.bbox.xMin, context.bbox.yMin, context.bbox.xMax, context.bbox.yMax];
	
    // Construction de la carte
	var carte = new Descartes.Map.ContinuousScalesMap(
				'map',
				contenuCarte,
				{
					projection: projection,
					initExtent: initBounds,
					maxExtent: maxBounds,
					displayExtendedOLExtent: true,
					minScale:2150000,
					maxScale:100,
					autoSize: true
				}
			);

	var toolsBar = carte.addNamedToolBar('toolBar');
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.DRAG_PAN});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.INITIAL_EXTENT, args : initBounds});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.MAXIMAL_EXTENT});

    var otherTools = [{type : Descartes.Map.ZOOM_IN},
    				 {type : Descartes.Map.ZOOM_OUT},
    				 {type : Descartes.Map.CENTER_MAP},
    				 {type : Descartes.Map.COORDS_CENTER, args :{projection: true, displayProjections: ["EPSG:3857","EPSG:4326","EPSG:2154"], selectedDisplayProjectionIndex:2}},
    				 {type : Descartes.Map.NAV_HISTORY},
    				 {type : Descartes.Map.DISTANCE_MEASURE},
	 	             {type : Descartes.Map.AREA_MEASURE},
	 	             {type : Descartes.Map.PNG_EXPORT},
	 	             {type : Descartes.Map.PDF_EXPORT}];
	
	carte.addToolInNamedToolBar(toolsBar,
			{type : Descartes.Map.TOOLBAR_OPENER, 
		     args : { 
				displayClass: 'defaultOpenerButton',
				title: "Plus d'outils",
				tools: otherTools
				}
			}
	);

	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_SELECTION,
		args: {
			persist: true, 
			resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_SELECTION,
		args: {
			persist: true, 
			resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.CIRCLE_SELECTION,
		args: {
			persist: true, 
			resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.RECTANGLE_SELECTION,
		args: {
			persist: true, 
			resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POINT_RADIUS_SELECTION,
        args: {
        	persist:true, 
        	infoRadius: true,
        	exportBuffer: true,
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.POLYGON_BUFFER_HALO_SELECTION,
        args: {
        	persist:true, 
        	infoBuffer: true, 
        	exportBuffer: true,
        	configHalo: true,
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});
	carte.addToolInNamedToolBar(toolsBar,{type : Descartes.Map.LINE_BUFFER_HALO_SELECTION,
        args: {
        	persist:true, 
        	infoBuffer: true, 
        	exportBuffer: true,
        	configHalo: true,
            resultUiParams: {
                withReturn: true,
                withUIExports: true,
                withAvancedView: true,
                withResultLayerExport: true,
                withFilterColumns: true,
                withListResultLayer: true
            },
            resultLayerParams: {
                display: true
            }
        }});   
	carte.addToolInNamedToolBar(toolsBar,{type: Descartes.Map.DISPLAY_LAYERSTREE_SIMPLE});
	carte.addToolInNamedToolBar(toolsBar,{type: Descartes.Map.SHARE_LINK_MAP});
	carte.addToolInNamedToolBar(toolsBar,{type: Descartes.Map.GEOLOCATION_SIMPLE,
										  args: {
											projection: true,
											displayProjections: ["EPSG:4326"],
											displayExportInfos: false,
				                            infos: {
				                               accuracy: false,
										       altitude: false,
										       altitudeAccuracy: false,
										       heading: false,
										       speed: false,
										       projection: false
				                            }
    }});
	carte.addToolInNamedToolBar(toolsBar,{type: Descartes.Map.GEOLOCATION_TRACKING,
										  args: {
											projection: true,
											displayProjections: ["EPSG:3857","EPSG:4326","EPSG:2154"], 
											selectedDisplayProjectionIndex:2,
											displayInfos: true,
											displayExportInfos: true,
											extraFormatExportTrace: {
										       gpxWpt: false,
										       gpxRte: true,
										       gpxTrk: false
										    }
    }});

	carte.addContentManager(
		'layersTree',
		[],
		{
            uiOptions: {
				nodeRootTitle: "Ma carte",
                moreInfosUiParams: {
					fctQueryable: true,
    				fctDisplayLegend:true,
    				fctOpacity:true,
    				fctMetadataLink:true,
    				displayOnClickLayerName: true
    			},
    			contextMenuTools:[
					{type : Descartes.Action.MapContentManager.ADD_GROUP_TOOL},
					{type : Descartes.Action.MapContentManager.ADD_LAYER_TOOL},
					{type : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL},
					{type : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL},
					{type : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL},
					{type : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL},
					{type : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL},
					{type : Descartes.Action.MapContentManager.EXPORT_VECTORLAYER_TOOL}
				],
				resultUiParams: {
	                withReturn: true,
	                withUIExports: true,
	                withAvancedView: true,
	                withResultLayerExport: true,
	                withFilterColumns: true,
	                withListResultLayer: true
	            }
            }
		}
	);
    carte.show();

	// Ajout des zones informatives
	carte.addInfo({type : Descartes.Map.GRAPHIC_SCALE_INFO, div : null});
	carte.addInfo({type : Descartes.Map.MOUSE_POSITION_INFO, div : 'LocalizedMousePosition', options:{displayProjections: ["EPSG:2154","EPSG:3857","EPSG:4326"], inline:true}});
	carte.addInfo({type : Descartes.Map.METRIC_SCALE_INFO, div : 'MetricScale'});
	carte.addInfo({type : Descartes.Map.MAP_DIMENSIONS_INFO, div : 'MapDimensions'});
	carte.addInfo({type : Descartes.Map.LEGEND_INFO, div : 'Legend' ,options: {
                    displayLayerTitle: true, optionsPanel: {collapsible: true,collapsed: false,panelCss: 'myPanelStyle'}
                }});
	carte.addInfo({type : Descartes.Map.ATTRIBUTION_INFO, div : null});


	// Ajout des assistants
	carte.addAction({type : Descartes.Map.SCALE_SELECTOR_ACTION, div : 'ScaleSelector',
		options : {
			optionsPanel: {collapsible: true,collapsed: false,panelCss: 'myPanelStyle'}
		}});
	carte.addAction({type : Descartes.Map.SCALE_CHOOSER_ACTION, div : 'ScaleChooser',
		options : {
			optionsPanel: {collapsible: true,collapsed: false,panelCss: 'myPanelStyle'}
		}});
//	carte.addAction({type : Descartes.Map.SIZE_SELECTOR_ACTION, div : 'SizeSelector'});
	carte.addAction({type : Descartes.Map.COORDS_INPUT_ACTION, div : 'CoordinatesInput',
		options : {
			optionsPanel: {collapsible: true,collapsed: false,panelCss: 'myPanelStyle'},
			projection: true, displayProjections: ["EPSG:3857","EPSG:4326","EPSG:2154"], selectedDisplayProjectionIndex:2
		}});
	carte.addAction({type : Descartes.Map.PRINTER_SETUP_ACTION, div : 'Pdf',
		options : {
			optionsPanel: {collapsible: true,collapsed: false,panelCss: 'myPanelStyle'}
		}});

	// Ajout asssitant localisation à l'adresse
    carte.addAction({type: Descartes.Action.LocalisationAdresse, div: 'localisationAdresse',
    	 options : {optionsPanel: {collapsible: true,collapsed: false,panelCss: 'myPanelStyle'}}});


	// Ajout de la rose des vents
	carte.addDirectionalPanPanel();
    
	// Ajout de la minicarte
	carte.addMiniMap("https://carto2.geo-ide.din.developpement-durable.gouv.fr/rest-api/ows/22c7124f-8453-4535-b735-555f03d2c88d?LAYERS=c_natural_Valeurs_type");

	// Ajout du gestionnaire de requete	
	if (mapContentType !== "KML" && mapContentType !== "GEOJSON" && mapContentType !== "VECTORTILE") {
		var gestionnaireRequetes = carte.addRequestManager('Requetes',{
			optionsPanel: {collapsible: true,collapsed: false,panelCss: 'myPanelStyle'},
			withChooseExtent: true,
			chooseExtentGazetteerConfig: {
		       startLevel: 1,
	           initValue: "93",
	           selectpicker: false
	        },
	        resultUiParams: {
	            withReturn: true,
	            withUIExports: true,
	            withAvancedView: true,
	            withResultLayerExport: true,
	            withFilterColumns: true,
	            withListResultLayer: true
	        }});
	    
	    var laCoucheBati = carte.mapContent.getLayerById("coucheBati");
		var requeteBati = new Descartes.Request(laCoucheBati, "Filtrer les constructions", Descartes.Layer.POLYGON_GEOMETRY);
		var critereType = new Descartes.RequestMember(
						"Sélectionner le type",
						"type",
						"==",
						["chateau", "batiment public", "ecole", "eglise"],
						true
					);
	
		requeteBati.addMember(critereType);
		gestionnaireRequetes.addRequest(requeteBati);
	}
	
	// Ajout du gestionnaire d'info-bulle
	if (mapContentType !== "VECTORTILE"){
	  if (mapContentType !== "KML" && mapContentType !== "GEOJSON") {
		var laCoucheParkings = carte.mapContent.getLayerById("coucheParkings");
		var laCoucheStations = carte.mapContent.getLayerById("coucheStations");
		carte.addToolTip('ToolTip', [
			{layer: laCoucheParkings, fields: ['name']},
			{layer: laCoucheStations, fields: ['name']}
		], {displayOnClick: true});
	  } else {
		var laCoucheParkings = carte.mapContent.getLayerById("coucheParkings");
		var laCoucheStations = carte.mapContent.getLayerById("coucheStations");
		carte.addSelectToolTip([
			{layer: laCoucheParkings, fields: ['name']},
			{layer: laCoucheStations, fields: ['name']}
		]);
	  }
	}
	
	// Ajout du gestionnaire de contextes
	carte.addBookmarksManager('Bookmarks', 'exemple-descartes',{behavior : Descartes.Action.BookmarksManager.BEHAVIOR_MANAGED_BY_CREATOR, optionsPanel: {collapsible: true,collapsed: false,panelCss: 'myPanelStyle'}});

	// Ajout du gestionnaire de localisation parcellaire
	var gazetteerLevels = [];
    gazetteerLevels.push(new Descartes.GazetteerLevel("Choisissez un département","Aucun departemant","dep"));  
    gazetteerLevels.push(new Descartes.GazetteerLevel("Choisissez une commune","Aucune commune","com"));  
    gazetteerLevels.push(new Descartes.GazetteerLevel("Choisissez une section cadastrale","Aucune section cadastrale pour cette commune",""));
    gazetteerLevels.push(new Descartes.GazetteerLevel("Choisissez une parcelle","Aucune parcelle pour cette section cadastrale",""));
	var gazetteerService = new Descartes.LocalisationParcellaire(Descartes.LocalisationParcellaire.NIVEAU_DEPARTEMENT);
    carte.addGazetteer('Gazetteer', "93", gazetteerLevels,{service:gazetteerService, selectpicker:true,  optionsPanel: {collapsible: true,collapsed: false,panelCss: 'myPanelStyle'}});

    carte.addOpenLayersInteractions([
        {type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys}},
        {type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM}]);

}


