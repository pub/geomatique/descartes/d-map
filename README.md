# D-Map, composant Javascript

Le composant Javascript D-Map est le module de visualisation de Descartes.

Ce composant est basé sur la librairie Openlayers
 
## Exemples de démonstration

https://pub.gitlab-pages.din.developpement-durable.gouv.fr/geomatique/descartes/d-map
 
## Paquet NPM

Le composant Javascript D-Map est disponible sous forme de paquet NPM sous l'appellation `@descartes/d-map`.

### Utilisation dans une application tierce

#### Utilisation via NPM

##### Installation

Pour installer le composant JS d-map dans un projet NPM :
```bash
$ npm install @descartes/d-map
```

Note : le paquet est disponible sur différents dépôts, il faut donc faire pointer le scope `@descartes` sur le dépôt souhaité:
- dépôt public gitlab-forge (pour les versions diffusées au sein du ministère MTE) :  
  https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17127/packages/npm 
- dépôt privé (pour les versions en cours de développement) :  
  https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/8282/packages/npm
- dépôts public NPM (pour les versions diffusées "grand public" à accès CDN)
  https://registry.npmjs.org

Pour que NPM puisse se connecter aux dépôts, il faut renseigner le fichier `.npmrc` :
```
@descartes:registry=https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17127/packages/npm
...
```

##### Inclusion du composant

Pour utiliser le composant JS D-Map, il suffit d'inclure les fichiers suivants dans la page HTML de l'application (les chemins relatifs doivent être respectés) :
* `vendor/d-map-required.css`
* `vendor/d-map-required.js`
* `css/d-map.css`
* `d-map.js`

#### Utilisation via CDN

Pour utiliser le composant JS D-Map, il suffit d'inclure les fichiers suivants dans la page HTML de l'application  :
* `https://cdn.jsdelivr.net/npm/@descartes/d-map@X.X.X/dist/vendor/d-map-required.css`
* `https://cdn.jsdelivr.net/npm/@descartes/d-map@X.X.X/dist/vendor/d-map-required.js`
* `https://cdn.jsdelivr.net/npm/@descartes/d-map@X.X.X/dist/css/d-map.css`
* `https://cdn.jsdelivr.net/npm/@descartes/d-map@X.X.X/dist/d-map.js`

Note : l'utilisation via CDN n'est pas recommandé en production.


### Utilisation du composant en cours de développement

#### Pre-requis

Installation sur son poste:
* NODE 16.14.0
* NPM 8.5.2
* Java 8 (pour la compilation d'Openlayers 4 via closure)

#### Utilisation de link

Il est possible de travailler simultanément sur le composant d-map et sur une application tierce, afin de vérifier
par exemple le bon comportement d'une évolution de d-map.

Pour cela, on commence par demander à npm de créer une référence locale globale du paquet, en se positionnant à la racine du projet d-map :
```
npm link
```

Ensuite, il faut indiquer à NPM que le paquet `@descartes/d-map` doit être résolu localement, en se positionnant dans le projet tierce :
```
$ npm link @descartes/d-map
```

Une fois les développements terminés, pour revenir à un état normal, en se positionnant dans le projet tierce :
```
npm unlink --no-save @descartes/d-map
npm install
```

Finalement, on peut demander à npm de nettoyer la référence locale globale, en se positionnant à la racine du projet d-map :
```
npm unlink
```

#### Compilation

La compilation du composant JS de D-Map se fait à l'aide de Webpack. Par ailleurs le composant possède les dépendances suivantes :
* openlayers
* proj4js
* geostyler
* jsts

Pour compiler le composant :
```bash
$ npm run build
```

Lors de la compilation, les fichiers suivants sont générés dans le répertoire `dist` à la racine de ce module :
* `d-map.js` et `d-map.css` : contiennent le code source et les feuilles de style utilisées par D-Map
  > ces fichiers sont générés par Webpack
* le répertoire `vendor` contient les fichiers `d-map-required.js` et `d-map-required.css`; ces deux fichiers contiennent l'intégralité des dépendances requises par D-Map
  > ces fichiers sont générés par le script `build/build.js`
* tous les autre fichiers (images, polices, ...) sont des ressources requises pour le bon fonctionnement du composant

#### Développement du composant

Pour lancer les exemples D-Map :
```bash
$ npm run dev
```

Si besoin, pour spécifier un port : `npm run dev -- --port=XXXX`. 
Le port par défaut est `4200`.

L'index des exemples est accessible sur http://localhost:4200. Les exemples se rafraichissent automatiquement en cas de changement dans les fichiers source.

> Note : Le répertoire `dist` ainsi que les répertoires `/demo` et `/node_modules` sont rendus accessibles à la racine du serveur.

#### Publication

La publication du paquet NPM se fait via GITLAB-CI à la création d'un TAG.
