/* global __dirname */

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

let root = path.resolve(__dirname, '../');

module.exports = function (ui, georef) {
    return {
        entry: './src/dmap_index',
        output: {
            path: path.resolve(root, './dist'),
            publicPath: '../',
            filename: 'd-map.js',
            library: 'Descartes',
            libraryTarget: 'umd',
            umdNamedDefine: true
        },
        devtool: 'source-map',
        module: {
            rules: [{
                enforce: 'pre',
                test: /\.js$/,
                loader: 'eslint-loader',
                options: {
                    quiet: true //show or not warnings
                }
            }, {
                test: /\.(jpe?g|gif|png|svg)$/,
                loader: 'file-loader?name=[name].[ext]&outputPath=img/'
            }, {
                test: /\.ejs$/,
                loader: 'ejs-loader'
            }, {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: 'css-loader'
                })
            }]
        },
        externals: {
            "openlayers": {
                commonjs: "openlayers",
                commonjs2: "openlayers",
                amd: "openlayers",
                root: "ol"
            },
            "jquery": {
                commonjs: "jQuery",
                commonjs2: "jQuery",
                amd: "jQuery",
                root: "$"
            },
            "jsTree": {
                commonjs: "jsTree",
                commonjs2: "jsTree",
                amd: "jsTree",
                root: "jsTree"
            },
            "ol-ext": {
                commonjs: "olExt",
                commonjs2: "olExt",
                amd: "olExt",
                root: "olExt"
            },
            "ol-mapbox-style": {
                commonjs: "olms",
                commonjs2: "olms",
                amd: "olms",
                root: "olms"
            },
            "jsts": {
                commonjs: "jsts",
                commonjs2: "jsts",
                amd: "jsts",
                root: "jsts"
            }
        },
        plugins: [
            new webpack.DefinePlugin({
                VERSION: JSON.stringify(require("../package.json").version),
                NAME: JSON.stringify(require("../package.json").name),
                MODE: JSON.stringify(ui),
                GEOREF: JSON.stringify(georef)
            }),
            new UglifyJsPlugin({
                sourceMap: true,
                test: /\.js$/,
                parallel: true
            }),
            new ExtractTextPlugin('css/d-map.css')
        ]
    }
};
