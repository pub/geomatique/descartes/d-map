require('shelljs/global');
var fs = require('fs-extra');
var exec = require('child_process').exec;

var customBuildConfig = 'build/ol-custom/ol-custom.json';
var customBuildDestFile = 'build/ol-custom/dist/ol.min.js';

var sourceMappingUrl = '//# sourceMappingURL=ol.min.js.map';


console.log('Creating custom openlayers file');

var filesToReplace = [{
        original: 'node_modules/openlayers/src/ol/tileurlfunction.js',
        custom: 'src/DMap/OpenLayersExtras/tileUrlFunction_overloaded.js'
    },{
        original: 'node_modules/openlayers/src/ol/format/wmscapabilities.js',
        custom: 'src/DMap/OpenLayersExtras/wmscapabilities_overloaded.js'
    },{
        original: 'node_modules/openlayers/src/ol/interaction/draw.js',
        custom: 'src/DMap/OpenLayersExtras/draw_overloaded.js'
    },{
        original: 'node_modules/openlayers/src/ol/interaction/draweventtype.js',
        custom: 'src/DMap/OpenLayersExtras/draweventtype_overloaded.js'
    }];

console.log('Replace file in openlayers');
for (var i = 0; i < filesToReplace.length; i++) {
    var fileToReplace = filesToReplace[i];

    //var originalExist = fs.existsSync(fileToReplace.original)

    console.log('Replace file %s with %s', fileToReplace.original, fileToReplace.custom);

    //keep original
    var keep = fileToReplace.original + '.keep';
    if (!fs.existsSync(keep)) {
        console.log('Keep original file %s in %s', fileToReplace.original, keep);
        fs.copySync(fileToReplace.original, keep);
    }
    fs.copySync(fileToReplace.custom, fileToReplace.original);
}

var callback = function (error, stdout, stderr) {
    console.log('stdout: ' + stdout);
    console.log('stderr: ' + stderr);
    if (error !== null) {
        console.log('exec error: ' + error);
    }
    console.log('Append sourcemapurl');

    fs.appendFileSync(customBuildDestFile, sourceMappingUrl);
};

var commandLine = 'node node_modules/openlayers/tasks/build.js ';
commandLine += customBuildConfig + " ";
commandLine += customBuildDestFile;


console.log('Command line is %s', commandLine);
exec(commandLine, callback);



