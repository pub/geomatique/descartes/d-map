const path = require('path');
const fs = require('fs');
const exec = require('child_process').exec;
require('shelljs/global');
const utils = require('./utils.js');

// à compléter selon l'implémentation de nouveau mode
const availableModes = ['bootstrap4'];

const argv = require('yargs')
    .help('h')
    .alias('h', 'help')
    .option('ui', {
        type: 'string',
        description: 'UI mode to select',
        default: availableModes[0],
        choices: availableModes
    })
    .option('georef', {
        type: 'boolean',
        default: true,
        description: 'Whether to activate the Georef plugin',
    })
    .option('port', {
        type: 'number',
        default: 4200,
        description: 'Port on which to run dev server',
    }).argv;

console.log('Using UI mode:', argv.ui);
console.log('Georef plugin activated:', argv.georef);

const root = path.resolve(__dirname, '../');
const nodeModules = path.resolve(root, 'node_modules');
const distPath = path.resolve(root, 'dist');
const vendorPath = path.resolve(distPath, 'vendor');

rm('-rf', distPath);
console.log('Dist folder emptied');

fs.mkdirSync(distPath);
fs.mkdirSync(vendorPath);

console.log('Concatenating vendor files (JS and CSS)...');
const jsFiles = [
    path.resolve(nodeModules, 'jquery/dist/jquery.min.js'),
    path.resolve(nodeModules, 'jquery-ui-dist/jquery-ui.min.js'),
    path.resolve(nodeModules, 'jstree/dist/jstree.min.js'),
    path.resolve(nodeModules, 'popper.js/dist/umd/popper.min.js'),
    path.resolve(nodeModules, 'bootstrap/dist/js/bootstrap.min.js'),
    path.resolve(nodeModules, 'bootstrap-table/dist/bootstrap-table.min.js'),
    path.resolve(nodeModules, 'bootstrap-table/dist/locale/bootstrap-table-fr-FR.min.js'),
    path.resolve(nodeModules, 'bootstrap-select/dist/js/bootstrap-select.min.js'),
    path.resolve(nodeModules, 'bootstrap-select/dist/js/i18n/defaults-fr_FR.min.js'),
    
    path.resolve(nodeModules, 'jquery-bootstrap-scrolling-tabs/dist/jquery.scrolling-tabs.min.js'),
    path.resolve(nodeModules, 'datatables.net/js/jquery.dataTables.min.js'),
    path.resolve(nodeModules, 'datatables.net-bs4/js/dataTables.bootstrap4.min.js'),
    path.resolve(nodeModules, 'datatables.net-buttons/js/dataTables.buttons.min.js'),
    path.resolve(nodeModules, 'datatables.net-buttons/js/buttons.html5.min.js'),
    path.resolve(nodeModules, 'datatables.net-buttons/js/buttons.print.min.js'),
    path.resolve(nodeModules, 'datatables.net-buttons/js/buttons.colVis.min.js'),
    path.resolve(nodeModules, 'datatables.net-select/js/dataTables.select.min.js'),

    
    path.resolve(nodeModules, 'proj4/dist/proj4.js'),
    path.resolve(nodeModules, 'jsts/dist/jsts.min.js'),
    path.resolve(root, 'build/ol-custom/dist/ol.min.js'),
    path.resolve(nodeModules, 'ol-ext/dist/ol-ext.min.js'),
    path.resolve(nodeModules, 'ol-mapbox-style/dist/olms.js'),
];
const cssFiles = [
    path.resolve(nodeModules, 'openlayers/css/ol.css'),
    path.resolve(nodeModules, 'ol-ext/dist/ol-ext.min.css'),
    path.resolve(nodeModules, 'bootstrap/dist/css/bootstrap.min.css'),
    path.resolve(nodeModules, 'bootstrap-table/dist/bootstrap-table.min.css'),
    path.resolve(nodeModules, 'bootstrap-select/dist/css/bootstrap-select.min.css'),
    path.resolve(nodeModules, 'jquery-ui-dist/jquery-ui.min.css'),
    path.resolve(nodeModules, 'jstree/dist/themes/default/style.min.css'),  
    path.resolve(nodeModules, 'jquery-bootstrap-scrolling-tabs/dist/jquery.scrolling-tabs.min.css'),
    path.resolve(nodeModules, 'datatables.net-bs4/css/dataTables.bootstrap4.min.css'),
    path.resolve(nodeModules, 'datatables.net-buttons-dt/css/buttons.dataTables.min.css'),
    path.resolve(nodeModules, 'datatables.net-select-bs4/css/select.bootstrap4.min.css'),
    path.resolve(nodeModules, 'font-awesome/css/font-awesome.min.css')
];
utils.concatFiles(jsFiles, path.resolve(vendorPath, 'd-map-required.js'));
utils.concatFiles(cssFiles, path.resolve(vendorPath, 'd-map-required.css'));

console.log('Copying assets...');
const fonts = [
    path.resolve(nodeModules, 'font-awesome/fonts/FontAwesome.otf'),
    path.resolve(nodeModules, 'font-awesome/fonts/fontawesome-webfont.eot'),
    path.resolve(nodeModules, 'font-awesome/fonts/fontawesome-webfont.svg'),
    path.resolve(nodeModules, 'font-awesome/fonts/fontawesome-webfont.ttf'),
    path.resolve(nodeModules, 'font-awesome/fonts/fontawesome-webfont.woff'),
    path.resolve(nodeModules, 'font-awesome/fonts/fontawesome-webfont.woff2'),
];
utils.copyFiles(fonts, path.resolve(vendorPath, '../fonts'));
const uiImages = [
    path.resolve(nodeModules, 'jquery-ui-dist/images/ui-icons_444444_256x240.png'),
    path.resolve(nodeModules, 'jquery-ui-dist/images/ui-icons_555555_256x240.png'),
    path.resolve(nodeModules, 'jquery-ui-dist/images/ui-icons_777620_256x240.png'),
    path.resolve(nodeModules, 'jquery-ui-dist/images/ui-icons_777777_256x240.png'),
    path.resolve(nodeModules, 'jquery-ui-dist/images/ui-icons_cc0000_256x240.png'),
    path.resolve(nodeModules, 'jquery-ui-dist/images/ui-icons_ffffff_256x240.png')
];
utils.copyFiles(uiImages, path.resolve(vendorPath, 'images'));
const jstreeImages = [
    path.resolve(nodeModules, 'jstree/dist/themes/default/32px.png'),
    path.resolve(nodeModules, 'jstree/dist/themes/default/40px.png'),
    path.resolve(nodeModules, 'jstree/dist/themes/default/throbber.gif')
];
utils.copyFiles(jstreeImages, path.resolve(vendorPath));

module.exports = {
    argv
};
