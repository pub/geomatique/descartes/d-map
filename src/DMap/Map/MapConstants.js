var constants = {
    /**
     * Constante: COORDS_INPUT_ACTION
     * Code de l'action pour le recentrage de la carte suite à la saisie d'un couple de coordonnées géographiques.
     */
    COORDS_INPUT_ACTION: 'CoordinatesInput',
    /**
     * Constante: SCALE_CHOOSER_ACTION
     * Code de l'action pour le recadrage de la carte suite à la saisie d'une échelle de visualisation.
     */
    SCALE_CHOOSER_ACTION: 'ScaleChooser',
    /**
     * Constante: SCALE_SELECTOR_ACTION
     * Code de l'action pour le recadrage de la carte suite au choix d'une échelle de visualisation dans une liste.
     */
    SCALE_SELECTOR_ACTION: 'ScaleSelector',
    /**
     * Constante: SIZE_SELECTOR_ACTION
     * Code de l'action pour le changement de la taille la carte suite au choix dans une liste.
     */
    SIZE_SELECTOR_ACTION: 'SizeSelector',
    /**
     * Constante: PRINTER_SETUP_ACTION
     * Code de l'action pour le gestionnaire d'export PDF.
     */
    PRINTER_SETUP_ACTION: 'PrinterParamsManager',
    /**
     * Constante: GRAPHIC_SCALE_INFO
     * Code de l'information pour l'affichage d'une échelle graphique.
     */
    GRAPHIC_SCALE_INFO: 'GraphicScale',
    /**
     * Constante: METRIC_SCALE_INFO
     * Code de l'information pour l'affichage d'une échelle métrique.
     */
    METRIC_SCALE_INFO: 'MetricScale',
    /**
     * Constante: MAP_DIMENSIONS_INFO
     * Code de l'information pour l'affichage des dimensions 'terrain' de la carte.
     */
    MAP_DIMENSIONS_INFO: 'MapDimensions',
    /**
     * Constante: MOUSE_POSITION_INFO
     * Code de l'information pour l'affichage des coordonnées courantes.
     */
    MOUSE_POSITION_INFO: 'LocalizedMousePosition',
    /**
     * Constante: LEGEND_INFO
     * Code de l'information pour l'affichage des coordonnées courantes.
     */
    LEGEND_INFO: 'Legend',
    /**
     * Constante: ATTRIBUTION_INFO
     * Code de l'information pour l'affichage des textes de copyright des couches (éventuellement liens vers des images).
     */
    ATTRIBUTION_INFO: 'Attribution',
    /**
     * Constante: MAXIMAL_EXTENT
     * Code de l'outil pour recadrer la carte sur son emprise maximale.
     */
    MAXIMAL_EXTENT: 'ZoomToMaximalExtent',
    /**
     * Constante: INITIAL_EXTENT
     * Code de l'outil pour recadrer la carte sur son emprise initiale.
     */
    INITIAL_EXTENT: 'ZoomToInitialExtent',
    /**
     * Constante: DRAG_PAN
     * Code de l'outil pour recentrer la carte suite à un 'glisser/relacher' du pointeur sur celle-ci.
     */
    DRAG_PAN: 'DragPan',
    /**
     * Constante: GEOLOCATION_SIMPLE
     * Code de l'outil pour afficher sa position courante.
     */
    GEOLOCATION_SIMPLE: 'GeolocationSimple',
    /**
     * Constante: GEOLOCATION_TRACKING
     * Code de l'outil pour tracer les positions courantes.
     */
    GEOLOCATION_TRACKING: 'GeolocationTracking',
    /**
     * Constante: ZOOM_IN
     * Code de l'outil pour effectuer un zoom avant par simple clic ou dessin d'un rectangle sur la carte.
     */
    ZOOM_IN: 'ZoomIn',
    /**
     * Constante: ZOOM_OUT
     * Code de l'outil pour effectuer un zoom arriére par simple clic sur la carte.
     */
    ZOOM_OUT: 'ZoomOut',
    /**
     * Constante: NAV_HISTORY
     * Code de l'outil pour gérer l'historique de navigation (zooms, déplacements, etc.).
     */
    NAV_HISTORY: 'NavigationHistory',
    /**
     * Constante: CENTER_MAP
     * Code de l'outil pour recentrer la carte suite à un clic du pointeur sur celle-ci.
     */
    CENTER_MAP: 'CenterMap',
    /**
     * Constante: COORDS_CENTER
     * Code de l'outil pour recentrer la carte sur un couple de coordonnées géographiques.
     */
    COORDS_CENTER: 'CenterToCoordinates',
    /**
     * Constante: DISTANCE_MEASURE
     * Code de l'outil pour effectuer des calculs de distance par dessin de polylignes sur la carte.
     */
    DISTANCE_MEASURE: 'MeasureDistance',
    /**
     * Constante: AREA_MEASURE
     * Code de l'outil pour effectuer des calculs de surface par dessin de polygones sur la carte.
     */
    AREA_MEASURE: 'MeasureArea',
    /**
     * Constante: PDF_EXPORT
     * Code de l'outil pour exporter la carte courante sous forme de fichier PDF.
     */
    PDF_EXPORT: 'ExportPDF',
    /**
     * Constante: PNG_EXPORT
     * Code de l'outil pour exporter la carte courante sous forme de fichier PNG.
     */
    PNG_EXPORT: 'ExportPNG',
    /**
     * Constante: SHARE_LINK_MAP
     * Code de l'outil pour partager le contexte courant de la parte.
     */
    SHARE_LINK_MAP: 'ShareLinkMap',
    /**
     * Constante: DISPLAY_LAYERSTREE_SIMPLE
     * Code de l'outil pour afficher l'arbre des couches simplifié.
     */
    DISPLAY_LAYERSTREE_SIMPLE: 'DisplayLayersTreeSimple',
    /**
     * Constante: POINT_SELECTION
     * Code de l'outil pour sélectionner des objets par simple clic sur la carte.
     */
    POINT_SELECTION: 'PointSelection',
    /**
     * Constante: POINT_RADIUS_SELECTION
     * Code de l'outil pour sélectionner des objets par construction d'un cercle sur la carte.
     */
    POINT_RADIUS_SELECTION: 'PointRadiusSelection',
    /**
     * Constante: CIRCLE_SELECTION
     * Code de l'outil pour sélectionner des objets par dessin d'un cercle sur la carte.
     */
    CIRCLE_SELECTION: 'CircleSelection',
    /**
     * Constante: POLYGON_SELECTION
     * Code de l'outil pour sélectionner des objets par dessin d'un polygone quelconque sur la carte.
     */
    POLYGON_SELECTION: 'PolygonSelection',
    /**
     * Constante: POLYGON_BUFFER_HALO_SELECTION
     * Code de l'outil pour sélectionner des objets par dessin d'un polygone quelconque avec paramètrage d'un buffer sur la carte.
     */
    POLYGON_BUFFER_HALO_SELECTION: 'PolygonBufferHaloSelection',
    /**
     * Constante: LINE_BUFFER_HALO_SELECTION
     * Code de l'outil pour sélectionner des objets par dessin d'une ligne quelconque avec paramètrage d'un buffer sur la carte.
     */
    LINE_BUFFER_HALO_SELECTION: 'LineBufferHaloSelection',
    /**
     * Constante: RECTANGLE_SELECTION
     * Code de l'outil pour sélectionner des objets par dessin d'un rectangle sur la carte.
     */
    RECTANGLE_SELECTION: 'RectangleSelection',
    /**
     * Constante: TOOLBAR_OPENER
     * Code de l'outil d'ouverture des barres d'outils.
     */
    TOOLBAR_OPENER: 'ToolBarOpener',
    /**
     * Constante: EXPORT_VECTORLAYER
     * Code de l'outil d'export des couches vecteurs.
     */
    EXPORT_VECTORLAYER: 'ExportVectorLayer',

    /**
     * Private
     */
    _TOOLS_WITH_MAPCONTENT: [
        'Descartes.Tool.Selection.PointSelection',
        'Descartes.Tool.Selection.PointRadiusSelection',
        'Descartes.Tool.Selection.PolygonSelection',
        'Descartes.Tool.Selection.PolygonBufferHaloSelection',
        'Descartes.Tool.Selection.LineBufferHaloSelection',
        'Descartes.Tool.Selection.CircleSelection',
        'Descartes.Tool.Selection.RectangleSelection',
        'Descartes.Button.ExportPNG',
        'Descartes.Button.ExportPDF',
        'Descartes.Button.ShareLinkMap',
        'Descartes.Button.DisplayLayersTreeSimple',
        'Descartes.Info.Legend',
        'Descartes.Action.PrinterParamsManager',
        'Descartes.Action.LocalisationAdresse'
    ],
    OL_DOUBLE_CLICK_ZOOM: 'DoubleClickZoom',
    OL_DRAG_AND_DROP: 'DragAndDrop',
    OL_DRAG_BOX: 'DragBox',
    OL_DRAG_PAN: 'DragPan',
    OL_DRAG_ROTATE: 'DragRotate',
    OL_DRAG_ROTATE_AND_ZOOM: 'DragRotateAndZoom',
    OL_DRAG_ZOOM: 'DragZoom',
    OL_DRAW: 'draw',
    OL_EXTENT: 'Extent',
    OL_KEYBOARD_PAN: 'KeyboardPan',
    OL_KEYBOARD_ZOOM: 'KeyboardZoom',
    OL_MODIFY: 'Modify',
    OL_MOUSE_WHEEL_ZOOM: 'MouseWheelZoom',
    OL_PINCH_ROTATE: 'PinchRotate',
    OL_PINCH_ZOOM: 'PinchZoom',
    OL_SELECT: 'Select',
    OL_SNAP: 'Snap',
    OL_TRANSLATE: 'Translate',

    OL_FULL_SCREEN: 'FullScreen',
    OL_ZOOM: 'Zoom',
    OL_ZOOM_SLIDER: 'ZoomSlider',
    OL_ZOOM_TO_EXTENT: 'ZoomToExtent',
    OL_SCALE_LINE: 'ScaleLine',
    OL_MOUSE_POSITION: 'MousePosition',
    OL_ATTRIBUTION: 'Attribution',
    OL_ROTATE: 'Rotate',
    OL_OVERVIEW_MAP: 'OverviewMap',
    MAP_TYPES: {
        CONTINUOUS: 'Continuous',
        DISCRETE: 'Discrete'
    }
};

/**
 * Constante: ACTIONS_NAME
 * {Array()} Codes des types d'actions disponibles (voir <Descartes.Action>).
 */
constants.ACTIONS_NAME = [
    constants.COORDS_INPUT_ACTION,
    constants.SCALE_CHOOSER_ACTION,
    constants.SCALE_SELECTOR_ACTION,
    constants.SIZE_SELECTOR_ACTION,
    constants.PRINTER_SETUP_ACTION
];
/**
 * Constante: INFOS_NAME
 * {Array()} Codes des types d'informations disponibles (voir <Descartes.Info>).
 */
constants.INFOS_NAME = [
    constants.GRAPHIC_SCALE_INFO,
    constants.METRIC_SCALE_INFO,
    constants.MAP_DIMENSIONS_INFO,
    constants.MOUSE_POSITION_INFO,
    constants.LEGEND_INFO,
    constants.ATTRIBUTION_INFO
];
/**
 * Constante: TOOLS_NAME
 * {Array()} Codes des types d'outils disponibles (voir <Descartes.Tool> et <Descartes.Button>).
 */
constants.TOOLS_NAME = [
    constants.MAXIMAL_EXTENT,
    constants.INITIAL_EXTENT,
    constants.DRAG_PAN,
    constants.GEOLOCATION_SIMPLE,
    constants.GEOLOCATION_TRACKING,
    constants.ZOOM_IN,
    constants.ZOOM_OUT,
    constants.NAV_HISTORY,
    constants.CENTER_MAP,
    constants.COORDS_CENTER,
    constants.DISTANCE_MEASURE,
    constants.AREA_MEASURE,
    constants.PDF_EXPORT,
    constants.PNG_EXPORT,
    constants.SHARE_LINK_MAP,
    constants.DISPLAY_LAYERSTREE_SIMPLE,
    constants.POINT_SELECTION,
    constants.POINT_RADIUS_SELECTION,
    constants.CIRCLE_SELECTION,
    constants.POLYGON_SELECTION,
    constants.POLYGON_BUFFER_HALO_SELECTION,
    constants.LINE_BUFFER_HALO_SELECTION,
    constants.RECTANGLE_SELECTION,
    constants.TOOLBAR_OPENER
];

/**
 * Constante: OL_INTERACTIONS_NAME
 * {Array()} Codes des types de intéractions d'openlayers disponible
 * https://openlayers.org/en/latest/apidoc/ol.interaction.html
 */
constants.OL_INTERACTIONS_NAME = [
    constants.OL_DOUBLE_CLICK_ZOOM,
    constants.OL_DRAG_AND_DROP,
    constants.OL_DRAG_BOX,
    constants.OL_DRAG_PAN,
    constants.OL_DRAG_ROTATE,
    constants.OL_DRAG_ROTATE_AND_ZOOM,
    constants.OL_DRAG_ZOOM,
    constants.OL_DRAW,
    constants.OL_EXTENT,
    constants.OL_KEYBOARD_PAN,
    constants.OL_KEYBOARD_ZOOM,
    constants.OL_MODIFY,
    constants.OL_MOUSE_WHEEL_ZOOM,
    constants.OL_PINCH_ROTATE,
    constants.OL_PINCH_ZOOM,
    constants.OL_SELECT,
    constants.OL_SNAP,
    constants.OL_TRANSLATE
];

/**
 * Constante: OL_CONTROLS_NAME
 * {Array()} Codes des types de intéractions d'openlayers disponible
 * https://openlayers.org/en/latest/apidoc/ol.control.html
 */
constants.OL_CONTROLS_NAME = [
    constants.OL_FULL_SCREEN,
    constants.OL_ZOOM,
    constants.OL_ZOOM_SLIDER,
    constants.OL_ZOOM_TO_EXTENT,
    constants.OL_SCALE_LINE,
    constants.OL_MOUSE_POSITION,
    constants.OL_ATTRIBUTION,
    constants.OL_ROTATE,
    constants.OL_OVERVIEW_MAP
];


module.exports = constants;
