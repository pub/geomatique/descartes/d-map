var _ = require('lodash');

var Map = require('../Core/Map');
var ContinuousScalesMap = require('./ContinuousScalesMap');
var DiscreteScalesMap = require('./DiscreteScalesMap');
var mapConstants = require('./MapConstants');

var namespace = {
    ContinuousScalesMap: ContinuousScalesMap,
    DiscreteScalesMap: DiscreteScalesMap
};

_.extend(namespace, mapConstants);

_.extend(Map, namespace);

module.exports = Map;
