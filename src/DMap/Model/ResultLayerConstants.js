var _ = require('lodash');
var ResultLayer = require('../Model/ResultLayer');


var static = {
    /**
     * Staticpropriete: blinkLayerTimerId
     * Identifiant du timer pour l'alternance d'affichage de la couche de sélection.
     */
    blinkLayerTimerId: null,
    /**
     * Staticpropriete: blinkLayerDelayId
     * Identifiant du timeOut pour l'alternance d'affichage de la couche de sélection.
     */
    blinkLayerDelayId: null,

    /**
     * Staticmethode: blinkLayer
     * Alterne l'affichage de la couche de sélection.
     */
    blinkLayer: function () {
        _.each(ResultLayer.olLayersResult, function (olLayerResult) {
            olLayerResult.setVisible(!olLayerResult.getVisible());
        });
    },
    stopBlinkLayer: function () {
        _.each(ResultLayer.olLayersResult, function (olLayerResult) {
            olLayerResult.setVisible(true);
        });
        clearInterval(this.blinkLayerTimerId);
    }
};

module.exports = static;

