var Utils = require('../Utils/DescartesUtils');
var _ = require('lodash');
var Group = require('./Group');
var EventManager = require('../Utils/Events/EventManager');
var LayerConstants = require('./LayerConstants');
var Layer = require('./Layer');
var ClusterLayerConstants = require('./ClusterLayerConstants');
var log = require('loglevel');

var GeoJSON = require('./Layer/GeoJSON');
var KML = require('./Layer/KML');
var TMS = require('./Layer/TMS');
var WFS = require('./Layer/WFS');
var WMS = require('./Layer/WMS');
var WMSC = require('./Layer/WMSC');
var WMTS = require('./Layer/WMTS');
var OSM = require('./Layer/OSM');
var XYZ = require('./Layer/XYZ');
var VectorTile = require('./Layer/VectorTile');
var GenericVector = require('./Layer/GenericVector');
var ClusterWFS = require('./ClusterLayer/WFS');
var ClusterKML = require('./ClusterLayer/KML');
var ClusterGeoJSON = require('./ClusterLayer/GeoJSON');

/**
 * Class: Descartes.MapContent
 * Objet "métier" correspondant au contenu de la carte en terme de groupes et couches.
 *
 * :
 * Les éléments constituant le contenu de la carte peuvent être des instances de <Descartes.Group> et/ou <Descartes.Layer>.
 *
 * Evénements déclenchés:
 * changed - La liste des éléments du contenu de la carte a changé.
 * itemPropertyChanged - Les propriétés des éléments du contenu de la carte ont changé (visibilités, opacités).
 * layerRemoved - Une couche a été supprimée.
 */
var MapContent = Utils.Class({
    /**
     * Propriete: item
     * {<Descartes.Group>} Groupe racine virtuel.
     */
    item: null,
    /**
     * Private
     */
    layers: [],
    /**
     * Private
     */
    nLayers: 0,
    /**
     * Propriete: fixedDisplayOrders
     * {Boolean} Indique si l'ordre de superposition des couches est modifiable (false par défaut).
     *
     * :
     * En fonction de la valeur de cette propriété, l'ajout d'éléments au contenu de la carte doit suivre les principes suivants :
     * - si la valeur est *true*, chaque couche <Descartes.Layer> doit être instanciée avec l'option "displayOrder" (voir <Descartes.Layer.displayOrder>).
     * Globalement, les "displayOrder" des couches doivent de plus être cohérentes.
     * - si la valeur est *false*, les éventuelles options "displayOrder" des couches sont ignorées et calculées automatiquement selon
     * l'ordre d'ajout des éléments dans le contenu de la carte.
     */
    fixedDisplayOrders: false,
    /**
     * Propriete: editable
     * {Boolean} Indique si le contenu de la carte peut être modifié (false par défaut).
     *
     * :
     * Prend automatiquement la valeur "false" si la propriété <fixedDisplayOrders> vaut "true".
     *
     * :
     * Les modifications possibles sont :
     * - ajouts, modifications, suppressions d'éléments (groupes ou couches). Voir <Descartes.Button.ContentTask>.
     * - modification de l'ordre des éléments. Voir <Descartes.UI.LayersTree>.
     */
    editable: false,
    /**
     * Propriete: editInitialItems
     * {Boolean} Indique si les éléments initiaux (fournis par l'application) sont modifiables (false par défaut).
     *
     * :
     * N'est pertinent, et donc utilisé, que si la propriété <editable> a pour valeur "true".
     */
    editInitialItems: false,
    /**
     * Propriete: fctVisibility
     * {Boolean} Indique si la fonctionnalité "Afficher ou masquer des couches et des groupes" est offerte (true par défaut).
     */
    fctVisibility: true,
    /**
     * Propriete: fctOpacity
     * {Boolean} Indique si la fonctionnalité "Régler l'opacité des couches" est offerte (true par défaut).
     */
    fctOpacity: true,
    /**
     * Propriete: fctQueryable
     * {Boolean} Indique si la fonctionnalité "Activer ou désactiver des couches pour l'interrogation" est offerte (true par défaut).
     */
    fctQueryable: true,
    /**
     * Propriete: fctContextMenu
     * {Boolean} Indique si la fonctionnalité "Utiliser le menu contextuel" est offerte (false par défaut).
     */
    fctContextMenu: false,
    /**
     * Propriete: fctDisplayLegend
     * {Boolean} Indique si la fonctionnalité "Affichage de la légende" est offerte (false par défaut).
     */
    fctDisplayLegend: false,
    /**
     * Propriete: fctMetadataLink
     * {Boolean} Indique si la fonctionnalité "Affichage des liens vers les fiches de métadonnées" est offerte (false par défaut).
     */
    fctMetadataLink: false,
    /**
     * Propriete: displayMoreInfos
     * {Boolean} Indique si la fonctionnalité "Affichage de la légende" est offerte (false par défaut).
     */
    displayMoreInfos: false,
    /**
     * Propriete: mapped
     * {Boolean} Indique si le contenu a été "cartographié" dans une carte OpenLayers.
     */
    mapped: false,
    /**
     * Propriété displayIconLoading
     * {Boolean} Indique que l'icône d'attente de chargement des couches est visible ou non.
     */
    displayIconLoading: true,
    /**
     * Propriete: events
     * {Descartes.Events} Gestionnaire d'événements
     */
    events: null,
    EVENT_TYPES: ['changed', 'itemPropertyChanged', 'layerRemoved'],
    /**
     * Constructeur: Descartes.MapContent
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * fixedDisplayOrders - {Boolean} Indique si l'ordre de superposition des couches est modifiable.
     * editable - {Boolean} Indique si le contenu de la carte peut être modifié.
     * editInitialItems - {Boolean} Indique si les éléments initiaux (fournis par l'application) sont modifiables.
     * fctVisibility - {Boolean} Indique si la fonctionnalité "Afficher ou masquer des couches et des groupes" est offerte.
     * fctOpacity - {Boolean} Indique si la fonctionnalité "Régler l'opacité des couches" est offerte.
     * fctQueryable - {Boolean} Indique si la fonctionnalité "Activer ou désactiver des couches pour l'interrogation" est offerte.
     */
    initialize: function (options) {
        _.extend(this, options);
        this.editable = (this.editable && !this.fixedDisplayOrders);
        this.item = new Group('Racine', {opened: true});
        this.events = new EventManager();
    },
    /**
     * Methode: populate
     * Alimente le contenu de la carte à partir d'un objet JSON pour la restauration d'un contexte de consultation.
     *
     * Paramêtres:
     * items - {Object} Objet JSON.
     */
    populate: function (items) {
        this.item.items = [];
        this.item.visible = null;
        this.mapped = false;
        this._populate(items, this.item);
        this.mapped = true;
        this.refresh(this.item);
    },
    /*
     * PRIVATE
     */
    _populate: function (items, baseItem) {
        for (var i = 0, len = items.length; i < len; i++) {
            var item = items[i];
            if (item && item.itemType === 'Group') {
                var groupItem = this.addItem(new Group(item.title, item.options), baseItem);
                this._populate(item.items, groupItem);
            } else if (item && item.itemType === 'Layer') {
                var LayerClass;
                switch (item.type) {
                    case LayerConstants.TYPE_WMS :
                        LayerClass = WMS;
                        break;
                    case LayerConstants.TYPE_WMSC :
                        LayerClass = WMSC;
                        break;
                    case LayerConstants.TYPE_TMS :
                        LayerClass = TMS;
                        break;
                    case LayerConstants.TYPE_WMTS :
                        LayerClass = WMTS;
                        break;
                    case LayerConstants.TYPE_WFS :
                        LayerClass = WFS;
                        break;
                    case LayerConstants.TYPE_KML :
                        LayerClass = KML;
                        break;
                    case LayerConstants.TYPE_GeoJSON :
                        LayerClass = GeoJSON;
                        break;
                    case LayerConstants.TYPE_GenericVector :
                        LayerClass = GenericVector;
                        break;
                    case LayerConstants.TYPE_OSM :
                        LayerClass = OSM;
                        break;
                    case LayerConstants.TYPE_XYZ :
                        LayerClass = XYZ;
                        break;
                    case LayerConstants.TYPE_VectorTile :
                        LayerClass = VectorTile;
                        break;
                    case LayerConstants.TYPE_GEOPORTAIL :
                        //ANNULER Descartes V5
                        log.warn('Couche du type Geoportail non pris en charge dans Descartes V5');
                        //LayerClass = LayerConstants.GeoPortail;
                        break;
                    case ClusterLayerConstants.TYPE_WFS :
                        LayerClass = ClusterWFS;
                        break;
                    case ClusterLayerConstants.TYPE_KML :
                        LayerClass = ClusterKML;
                        break;
                    case ClusterLayerConstants.TYPE_GeoJSON :
                        LayerClass = ClusterGeoJSON;
                        break;
                }
                this.addItem(new LayerClass(item.title, item.definition, item.options), baseItem);
            }
        }
    },
    /**
     * Methode: serialize
     * Fournit une représentation du contenu de la carte sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var context = [];
        for (var i = 0, len = this.item.items.length; i < len; i++) {
            context.push(this._serialize(this.item.items[i]));
        }
        return context;
    },
    /*
     * PRIVATE
     */
    _serialize: function (item) {
        var itemSerialize = item.serialize();
        if (item instanceof Group) {
            itemSerialize.items = [];
            for (var i = 0, len = item.items.length; i < len; i++) {
                itemSerialize.items.push(this._serialize(item.items[i]));
            }
        }
        return itemSerialize;
    },
    /**
     * Methode: refresh
     * Reconstruit les index hiérarchiques des éléments suite à une modification quelconque du contenu.
     *
     * Paramêtres:
     * baseItem - {<Descartes.Group> | <Descartes.Layer>} Elément ayant subit la modification (null dans le cas d'une suppression).
     */
    refresh: function (baseItem) {
        this.nLayers = 0;
        this._indexItems(this.item);
        if (baseItem !== undefined) {
            this.refreshVisibilities(baseItem, true);
        }
        if (this.mapped) {
            this.events.triggerEvent('changed');
        }
    },
    /**
     * Methode: refreshOnlyContentManager
     * Demande de rafraichissement de l'arbre des couches uniquement.
     *
     */
    refreshOnlyContentManager: function () {
        if (this.mapped) {
            this.events.triggerEvent('changed', true);
        }
    },
    _reIndexItems: function () {
        this.nLayers = 0;
        this._indexItems(this.item);
    },
    _indexItems: function (item) {
        if (item.items !== undefined) {
            for (var i = 0, len = item.items.length; i < len; i++) {
                if (item.index !== '') {
                    item.items[i].index = item.index + '#' + i.toString();
                } else {
                    item.items[i].index = i.toString();
                }
                if (!this.fixedDisplayOrders && item.items[i] instanceof Layer) {
                    this.nLayers++;
                    item.items[i].displayOrder = this.nLayers;
                }
                this._indexItems(item.items[i]);
            }
        }
    },
    /**
     * Methode: addItem
     * Ajoute un élément au contenu de la carte.
     *
     * Paramêtres:
     * item - {<Descartes.Group> | <Descartes.Layer>} Elément à ajouter.
     * parentItem - {<Descartes.Group>} Groupe à ajouter l'élément. S'il est nul, l'élément est ajouté à la racine.
     *
     * Retour:
     * {<Descartes.Group> | <Descartes.Layer>} Elément ajouté.
     */
    addItem: function (item, parentItem) {
        log.debug('addItem', item);
        if (item instanceof Layer || item instanceof Group) {
            (parentItem || this.item).items.push(item);
            if (parentItem instanceof Group && !_.isNil(parentItem.visible)) {
                item.setVisibility(parentItem.visible);
            }
            if (item instanceof Layer && item.isVisible() === false) {
                item.loading = false;
            }
            if (this.mapped) {
                this.refresh(item);
            }
        }
        return item;
    },
    /**
     * Methode: insertItemBefore
     * Insère un élément au contenu de la carte, avant un autre élément.
     *
     * Paramêtres:
     * item - {<Descartes.Group> | <Descartes.Layer>} Elément à ajouter.
     * refItem - {<Descartes.Group> | <Descartes.Layer>}} Elément avant lequel insérer le nouvel élément.
     *
     * Retour:
     * {<Descartes.Group> | <Descartes.Layer>} Elément inséré.
     */
    insertItemBefore: function (item, refItem) {
        if (item instanceof Layer || item instanceof Group) {
            var parentRefItem = this.getParentItem(refItem);
            parentRefItem.items.splice(_.findIndex(parentRefItem.items, refItem), 0, item);
            this.refresh(item);
        }
        return item;
    },
    /**
     * Methode: insertItemAfter
     * Insère un élément au contenu de la carte, après un autre élément.
     *
     * Paramêtres:
     * item - {<Descartes.Group> | <Descartes.Layer>} Elément à ajouter.
     * refItem - {<Descartes.Group> | <Descartes.Layer>}} Elément après lequel insérer le nouvel élément.
     *
     * Retour:
     * {<Descartes.Group> | <Descartes.Layer>} Elèment inséré.
     */
    insertItemAfter: function (item, refItem) {
        if (item instanceof Layer || item instanceof Group) {
            var parentRefItem = this.getParentItem(refItem);
            parentRefItem.items.splice(_.findIndex(parentRefItem.items, refItem) + 1, 0, item);
            this.refresh(item);
        }
        return item;
    },
    /**
     * Methode: getParentItem
     * Fournit l'élément parent d'un élément du contenu de la carte.
     *
     * Paramêtres:
     * item - {<Descartes.Group> | <Descartes.Layer>} Elément dont le parent est recherché.
     *
     * Retour:
     * {<Descartes.Group>} Elément parent.
     */
    getParentItem: function (item) {
        return this._getParentItem(item, this.item);
    },
    _getParentItem: function (item, node) {
        var parentItem = null;
        if (node.items !== undefined) {
            if (_.findIndex(node.items, item) !== -1) {
                parentItem = node;
            } else {
                for (var iNode = 0, len = node.items.length; iNode < len; iNode++) {
                    parentItem = this._getParentItem(item, node.items[iNode]);
                    if (parentItem !== null) {
                        break;
                    }
                }
            }
        }
        return parentItem;
    },
    /**
     * Methode: getItemByIndex
     * Fournit un élément du contenu de la carte.
     *
     * Paramètres:
     * index - {String} Index hiérarchique de l'élément recherché.
     *
     * Retour:
     * {<Descartes.Group> | <Descartes.Layer>} Elément trouvé.
     */
    getItemByIndex: function (index) {
        return this._getItemByIndex(this.item, index);
    },
    /*
     * PRIVATE
     */
    _getItemByIndex: function (item, index) {

        var indexes = index.split('#');
        if (indexes.length !== 1) {
            index = indexes[0];
            indexes.splice(0, 1);
            return this._getItemByIndex(item.items[index], indexes.join('#'));
        } else if (!_.isNil(item) && !_.isNil(item.items)) {
            return item.items[index];
        } else {
            return this.item;
        }
    },
    /**
     * Methode: removeItemByIndex
     * Supprime un élément du contenu de la carte, et déclenche l'événement 'layerRemoved'.
     *
     * Paramètres:
     * index - {String} Index hiérarchique de l'élément à supprimer.
     * skipRefresh - {Boolean} indique si le rafraichissement de l'arbre doit être omis ou non
     */
    removeItemByIndex: function (index, skipRefresh) {
        var indexes = index.split('#');
        if (indexes.length !== 1) {
            index = indexes[indexes.length - 1];
            indexes.splice(indexes.length - 1, 1);
            this.getItemByIndex(indexes.join('#')).items.splice(index, 1);
        } else {
            this.item.items.splice(index, 1);
        }
        if (!skipRefresh) {
            this.refresh();
        }
        /**
         * FIXDOC
         */
        this.events.triggerEvent('layerRemoved');
    },
    /**
     * Methode: reaffectItem
     * Déplace un élément au contenu de la carte, avant ou après un autre élément.
     *
     * Paramêtres:
     * movedItemIndex - {String} Index hiérarchique de l'élément à déplacer.
     * destItemIndex - {String} Index hiérarchique de l'élément de référence.
     * position - {'Top' | 'Bottom'} Destination de l'élément par rapport à l'élément de référence.
     *
     * Retour:
     * {<Descartes.Group> | <Descartes.Layer>} Elément déplacé.
     */
    reaffectItem: function (movedItemIndex, destItemIndex, position) {
        var item = this.getItemByIndex(movedItemIndex);
        var destItem = this.getItemByIndex(destItemIndex);
        this.removeItemByIndex(movedItemIndex, true);
        if (position === 'Top') {
            this.insertItemBefore(item, destItem);
        } else {
            this.insertItemAfter(item, destItem);
        }
    },
    /**
     * Methode: refreshVisibilities
     * Rafraichit, par propagation ascendante et descendante, les visibilités de l'ensemble des éléments.
     *
     * Paramètres:
     * item - {<Descartes.Group> | <Descartes.Layer>} Elément provoquant le rafraichissement.
     * unchanged - {Boolean} si true, indique que le rafraichissement n'est pas provoqué par une bascule utilisateur.
     */
    refreshVisibilities: function (item, unchanged) {
        var visibility;
        if (unchanged === true) {
            visibility = item.visible;
        } else {
            visibility = !item.visible;
        }
        this._cascadeVisibility(item, visibility);
        this._bubbleVisibility(item, visibility);
        if (this.mapped) {
            this.events.triggerEvent('itemPropertyChanged');
        }
    },

    _cascadeVisibility: function (item, visibility) {
        if (!_.isNil(visibility)) {
            item.setVisibility(visibility);
        }
        if (item.items !== undefined) {
            for (var i = 0, len = item.items.length; i < len; i++) {
                this._cascadeVisibility(item.items[i], visibility);
            }
        }
    },
    _bubbleVisibility: function (item, visibility) {
        var parentItem = this.getParentItem(item);
        if (!_.isNil(parentItem)) {
            var allTheSame = true;
            for (var i = 0, len = parentItem.items.length; i < len; i++) {
                if (parentItem.items[i].visible !== visibility) {
                    allTheSame = false;
                    break;
                }
            }
            parentItem.setVisibility(allTheSame ? visibility : null);
            if (parentItem !== this.item) {
                this._bubbleVisibility(parentItem, visibility);
            }
        }
    },
    /**
     * Methode: changeOpacity
     * Modifie l'opacité d'un élément.
     *
     * Paramètres:
     * item - {<Descartes.Group> | <Descartes.Layer>} Elément à modifier.
     * opacity - {Integer} Nouvelle opacité.
     */
    changeOpacity: function (item, opacity) {
        item.setOpacity(opacity);
        if (this.mapped) {
            this.events.triggerEvent('itemPropertyChanged');
        }
    },
    /**
     * Methode: getLayers
     * Fournit les couches du contenu de la carte.
     *
     * Retour:
     * {Array(<Descartes.Layer>)} Liste des couches.
     */
    getLayers: function () {
        this.layers = [];
        this._getLayers(this.item);
        return this.layers;
    },
    /**
     * Methode: getVisibleLayers
     * Fournit les couches visibles du contenu de la carte.
     *
     * Retour:
     * {Array(<Descartes.Layer>)} Liste des couches visibles.
     */
    getVisibleLayers: function () {
        this.layers = [];
        this._getLayers(this.item, 'isVisible');
        return this.layers;
    },
    /**
     * Methode: getQueryableLayers
     * Fournit les couches du contenu de la carte pour les interrogations.
     *
     * Retour:
     * {Array(<Descartes.Layer>)} Liste des couches pour les interrogations.
     */
    getQueryableLayers: function () {
        this.layers = [];
        this._getLayers(this.item, 'isQueryable');
        return this.layers;
    },

    /**
     * Methode: getLayerById
     * Retourne une layer selon son identifiant
     *
     * Retour:
     * {Array (<Descartes.Layer>)}
     */
    getLayerById: function (id) {
        var layers = this.getLayers();
        return _.find(layers, function (o) {
            return o.id === id;
        });
    },
    /*
     * PRIVATE
     */
    getLayerByOLLayer: function (olLayer) {
        var result = this._getLayersByFilter(this.item, function (layer) {
            for (var i = 0; i < layer.OL_layers.length; i++) {
                var anOLLayer = layer.OL_layers[i];
                if (anOLLayer === olLayer) {
                    return layer;
                }
            }
            return null;
        });
        if (result.length > 0) {
            return result[0];
        }
        return null;
    },
    getLayerByOLSource: function (olSource) {
        var result = this._getLayersByFilter(this.item, function (layer) {
            for (var i = 0; i < layer.OL_layers.length; i++) {
                var anOLLayer = layer.OL_layers[i];
                if (anOLLayer.getSource() === olSource) {
                    return layer;
                }
            }
            return null;
        });
        if (result.length > 0) {
            return result[0];
        }
        return null;
    },
    _getLayers: function (item, criteria) {
        if (!_.isNil(item.items)) {
            for (var i = 0, len = item.items.length; i < len; i++) {
                if (item.items[i] instanceof Layer) {
                    if (criteria === undefined || item.items[i][criteria]()) {
                        this.layers.push(item.items[i]);
                    }
                } else {
                    this._getLayers(item.items[i], criteria);
                }
            }
        }
    },
    _getLayersByFilter: function (item, filter) {
        var result = [];
        if (!_.isNil(item.items)) {
            for (var i = 0, len = item.items.length; i < len; i++) {
                var anItem = item.items[i];
                if (anItem instanceof Layer) {
                    var filtered = filter(anItem);
                    if (filtered !== null) {
                        result.push(filtered);
                    }
                } else {
                    result = result.concat(this._getLayersByFilter(anItem, filter));
                }

            }
        }
        return result;
    },
    CLASS_NAME: 'Descartes.MapContent'
});

module.exports = MapContent;
