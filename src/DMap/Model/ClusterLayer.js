var _ = require('lodash');
var ol = require('openlayers');

var Utils = require('../Utils/DescartesUtils');
var Symbolizers = require('../Symbolizers');

/**
 * Class: Descartes.Layer.ClusterLayer
 * Objet "métier" correspondant à une couche de type cluster pour le contenu de la carte.
 *
 */
var Class = Utils.Class({

    defaultClusterConfig: {
        distance: 100,
        scaleLimit: 1500000,
        symbolizersClusterPoint: {
            'Point': {
                pointRadius: 20,
                graphicName: 'circle',
                fillColor: '#666666',
                fillOpacity: 1,
                strokeWidth: 1,
                strokeOpacity: 1,
                strokeColor: '#fff'
            }
        }
    },

    /**
     * Propriete: cluster
     * {Object} Configuration du cluster.
     *
     *  Objet JSON de la forme:
     * (start code)
     * {
     * 		distance: <Distance de regroupement>,
     *      scaleLimit: <Echelle limite de fin de regroupement>,
     * 		symbolizersClusterPoint: {<Style d'affichage du regroupement>}
     * }
     *  (end)
     */
    cluster: null,

    _loaderWithRemoveFeatures: true,

    /**
     * Methode: initClusterConfig
     * Méthode d'initialisation du cluster.
     *
     * Paramètres:
     * opts - {Object} options d'initialisation.
     */
    initClusterConfig: function (opts) {

         this.cluster = _.extend({}, this.defaultClusterConfig);

         if (!_.isNil(opts) && !_.isNil(opts.cluster)) {
             this.cluster = _.extend(this.cluster, opts.cluster);
             delete opts.cluster;
         }

         opts._loaderWithRemoveFeatures = this._loaderWithRemoveFeatures;

         return opts;
    },

    /**
     * Methode: initStyles
     * Méthode d'initialisation des styles.
     */
    initStyles: function () {
         this.featureStyles = Symbolizers.getOlStyle(this.symbolizers);
         for(var geometryTypeStyle in this.featureStyles) {
             this.featureStyles[geometryTypeStyle].setGeometry(this.featureStyleGeometryFunction);
         }

         this.clusterStylePoint = Symbolizers.getOlStyle(this.cluster.symbolizersClusterPoint)["Point"];
    },

    /**
     * Methode: initVectorCluster
     * Méthode d'initialisation de la ressource vecteur.
     *
     * Paramètres:
     * vectorRessource - {Object} la ressource vecteur.
     */
    initVectorCluster: function (vectorRessource) {
         var clusterSource = new ol.source.Cluster({
                distance: this.cluster.distance,
                source: vectorRessource.getSource(),
                geometryFunction: function (feature) {
                    return new ol.geom.Point(ol.extent.getCenter(feature.getGeometry().getExtent()));
                }
            });

            var that = this;
            var featureStyleFunction = function (feature, resolution) {
                   var size = feature.get('features').length;
                   var view = that.OL_layers[0].map.getView();
                   var units = view.getProjection().getUnits();
                   var scale = Utils.getScaleFromResolution(resolution, units);

                   var style;
                   if (scale > that.cluster.scaleLimit) {
                        return that.clusterStyle(size);
                  } else{
                       var styleName = that.geometryType;
                    if (that.geometryType === 'Line') {
                        styleName = 'LineString';
                    } else if (that.geometryType === 'MultiLine') {
                        styleName = 'MultiLineString';
                    }
                       return that.featureStyles[styleName];
               }
                };

            var vector = new ol.layer.Vector({
                source: clusterSource,
                style: featureStyleFunction
            });

            return vector;

    },

    clusterStyle: function (text) {
        this.clusterStylePoint.setText(new ol.style.Text({
             text: text.toString(),
             fill: new ol.style.Fill({
               color: '#fff'
             })
           }));

        return this.clusterStylePoint;
    },

    featureStyleGeometryFunction: function (feature) {
         var geometries = [];
         for (var i = 0, len = feature.get('features').length; i < len; i++) {
             var originalFeature = feature.get('features')[i];
             geometries.push(originalFeature.getGeometry());
         }
         return new ol.geom.GeometryCollection(geometries);
    },

    registerLayerLoading: function (layer, scope, loadStart, loadEnd) {
        var source = layer.getSource().getSource();
        if (source instanceof ol.source.Image) {
            if (this._sourceImageListenersMethod.indexOf(loadStart.name) === -1) {
                source.on('imageloadstart', loadStart.bind(this), scope);
                this._sourceImageListenersMethod.push(loadStart.name);
            }
            if (this._sourceImageListenersMethod.indexOf(loadEnd.name) === -1) {
                source.on('imageloadend', loadEnd.bind(this), scope);
                this._sourceImageListenersMethod.push(loadEnd.name);
            }
            if (this._sourceImageListenersMethod.indexOf(this._onLoadError.name) === -1) {
                source.on('imageloaderror', this._onLoadError.bind(this), this);
                this._sourceImageListenersMethod.push(this._onLoadError.name);
            }
        } else if (source instanceof ol.source.Tile) {
            if (this._sourceTileListenersMethod.indexOf(loadStart.name) === -1) {
                source.on('tileloadstart', loadStart.bind(this), scope);
                this._sourceTileListenersMethod.push(loadStart.name);
            }
            if (this._sourceTileListenersMethod.indexOf(loadEnd.name) === -1) {
                source.on('tileloadend', loadEnd.bind(this), scope);
                this._sourceTileListenersMethod.push(loadEnd.name);
            }
            if (this._sourceTileListenersMethod.indexOf(this._onLoadError.name) === -1) {
                source.on('tileloaderror', this._onLoadError.bind(this), this);
                this._sourceTileListenersMethod.push(this._onLoadError.name);
            }
        } else if (source instanceof ol.source.Vector) {
            if (this._sourceFeatureListenersMethod.indexOf(loadStart.name) === -1) {
                source.on('featureloadstart', loadStart.bind(this), scope);
                this._sourceFeatureListenersMethod.push(loadStart.name);
            }
            if (this._sourceFeatureListenersMethod.indexOf(loadEnd.name) === -1) {
                source.on('featureloadend', loadEnd.bind(this), scope);
                this._sourceFeatureListenersMethod.push(loadEnd.name);
            }
            if (this._sourceFeatureListenersMethod.indexOf(this._onLoadError.name) === -1) {
                source.on('featureloaderror', this._onLoadError.bind(this), this);
                this._sourceFeatureListenersMethod.push(this._onLoadError.name);
            }
        }
    },

    unregisterLayerLoading: function (layer, scope, loadStart, loadEnd) {
        var source = layer.getSource().getSource();
        if (source instanceof ol.source.Image) {
            source.un('imageloadstart', loadStart, scope);
            source.un('imageloadend', loadEnd, scope);
            source.un('imageloaderror', this._onLoadError, this);
        } else if (source instanceof ol.source.Tile) {
            source.un('tileloadstart', loadStart, scope);
            source.un('tileloadend', loadEnd, scope);
            source.un('tileloaderror', this._onLoadError, this);
        } else if (source instanceof ol.source.Vector) {
            source.un('featureloadstart', loadStart, scope);
            source.un('featureloadend', loadEnd, scope);
            source.un('featureloaderror', this._onLoadError, this);
        }
    },

    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde
     * d'un contexte de consultation.
     *
     * :
     * Doit être surchargée par les classes dérivées.
     *
     * Retour: {Object} Objet JSON.
     */
    serialize: function () {
        var json = {};
        json.itemType = 'Layer';
        json.title = this.title;
        json.type = this.type;
        json.options = {
            format: this.format,
            legend: this.legend,
            metadataURL: this.metadataURL,
            attribution: this.attribution,
            visible: this.visible,
            alwaysVisible: this.alwaysVisible,
            queryable: this.queryable,
            activeToQuery: this.activeToQuery,
            sheetable: this.sheetable,
            opacity: this.opacity,
            opacityMax: this.opacityMax,
            displayOrder: this.displayOrder,
            maxScale: this.maxScale,
            minScale: this.minScale,
            id: this.id,
            symbolizers: this.symbolizers,
            attributes: this.attributes,
            geometryType: this.geometryType,
            cluster: this.cluster
        };
        json.definition = [];
        for (var i = 0, len = this.resourceLayers.length; i < len; i++) {
            json.definition.push(this.resourceLayers[i].serialize());
        }
        return json;
    },
    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 	title: <Intitulé>,
     * 	type: <Type de couche>,
     * 	layersDefinition: {
     *      layerName: <Nom de la couche ou de l'objet OWS>
     * 	    serverUrl: <URL du serveur OWS>
     * 	} [],
     * 	options: {
     *      format: <Type MIME pour l'affichage des couches OpenLayers>,
     *      legend: <Adresses d'accés aux légendes>,
     *      metadataURL: <Adresse d'accés aux informations complémentaires>,
     *      attribution: <Texte de copyright>,
     *      queryable: <Interrogation potentielle>,
     *      sheetable: <Affichage des propriétés sous forme de tableau>,
     *      maxScale: <Dénominateur de l'échelle maximale>,
     *      minScale: <Dénominateur de l'échelle minimale>,
     *      id: <Identifiant de la couche>
     *      symbolizers: <Styles de la couche>
     *      attributes: <Attributs de la couche>
     *      geometryType: <Type de géométrie de la couche>
     *      cluster: <Configuration du cluster>
     * 	}
     * }
     * (end)
     */
    toJSON: function () {
        var featureOL_layers = this.getFeatureOL_layers();
        var json = {};
        json.title = this.title;
        json.type = this.type;
        json.layersDefinition = [{
                layerName: featureOL_layers[0].get('layerName'),
                serverUrl: featureOL_layers[0].getSource().getUrl()
            }];
        json.options = {
            format: this.format,
            legend: (this.legend !== null) ? this.legend[0] : "",
            metadataURL: (this.metadataURL !== null) ? this.metadataURL : "",
            attribution: (this.attribution !== null) ? this.attribution : "",
            queryable: this.queryable,
            sheetable: this.sheetable,
            maxScale: (this.maxScale !== null) ? this.maxScale : "",
            minScale: (this.minScale !== null) ? this.minScale : "",
            id: this.id,
            symbolizers: this.symbolizers,
            attributes: this.attributes,
            geometryType: this.geometryType,
            cluster: this.cluster
        };
        return json;
    },

    CLASS_NAME: "Descartes.Layer.ClusterLayer"
});

module.exports = Class;
