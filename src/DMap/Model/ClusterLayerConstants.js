module.exports = {
    /**
     * Constante: TYPE_WFS
     * Code du layer d'édition de type WFS.
     */
    TYPE_WFS: 20,
    /**
     * Constante: TYPE_KML
     * Code du layer d'édition de type KML.
     */
    TYPE_KML: 21,
    /**
     * Constante: TYPE_GeoJSON
     * Code du layer d'édition de type GeoJSON.
     */
    TYPE_GeoJSON: 22
};
