var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');

var Url = require('./Url');

/**
 * Class: Descartes.GazetteerService
 * Objet "métier" correspondant à un service côté serveur pour la fourniture des objets de référence.
 */
var Class = Utils.Class({
    /**
     * Propriete: url
     * {String} URL du service.
     */
    url: null,

    /**
     * Propriete: levelParam
     * {String} Nom de la clé définissant le niveau recherche dans la pyramide.
     */
    levelParam: null,

    /**
     * Propriete: projectionParam
     * {String} Nom de la cé définissant la projection.
     */
    projectionParam: null,

    /**
     * Propriete: codeParam
     * {String} Nom de la clé définissant le code de l'objet "père" des objets recherchés.
     */
    codeParam: null,

    /**
     * Constructeur: Descartes.GazetteerService
     * Constructeur d'instances
     *
     * Paramètres:
     * urlRoot - {String} "Racine" de l'URL du service.
     * levelParam - {String} Nom de la clé définissant le niveau recherché dans la pyramide.
     * projectionParam - {String} Nom de la clé définissant la projection.
     * codeParam - {String} Nom de la clé définissant le code de l'objet "père" des objets recherchés.
     * urlFixedParams - {Array(String)} Tableau d'objets JSON (ayant les propriétés 'key' et 'value') définissant des paramètres complémentaires et fixes pour la requête HTTP
     */
    initialize: function (urlRoot, levelParam, projectionParam, codeParam, urlFixedParams) {
        this.levelParam = levelParam;
        this.projectionParam = projectionParam;
        this.codeParam = codeParam;
        this.url = new Url(urlRoot);
        if (_.isArray(urlFixedParams)) {
            for (var i = 0, len = urlFixedParams.length; i < len; i++) {
                var param = urlFixedParams[i];
                if (param.key !== undefined && param.value !== undefined) {
                    this.url.addParam(param.key, param.value);
                }
            }
        }
    },
    CLASS_NAME: 'Descartes.GazetteerService'
});

module.exports = Class;
