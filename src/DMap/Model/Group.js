/* global Descartes */
var Utils = require('../Utils/DescartesUtils');
var _ = require('lodash');

/**
 * Class: Descartes.Group
 * Objet "métier" correspondant à un groupe d'éléments pour le contenu de la carte.
 *
 * :
 * Les éléments du groupe peuvent être des instances de <Descartes.Group> ou <Descartes.Layer>.
 *
 * :
 * Aucune limite de profondeur "groupe / sous-groupe / sous-sous-groupe / ..." n'existe.
 */
var Group = Utils.Class({
    /**
     * Propriete: title
     * {String} Intitulé du groupe.
     */
    title: null,
    /**
     * Propriete: visible
     * {Boolean | null} Indique comment les éléments du groupe sont visibles.
     *
     * :
     * Les valeurs possibles sont :
     * :
     * true - Tous les éléments du groupe sont visibles.
     * false - Aucun élément du groupe n'est visible.
     * null - Des éléments du groupe sont visibles et d'autres non.
     */
    visible: null,
    /**
     * Propriete: hide
     * {Boolean} Indique si le groupe est caché ou non dans l'arbre des couches.
     */
    hide: false,
    /**
     * Propriete: opened
     * {Boolean} Indique si le groupe est déplié ou non dans la vue affichant l'arborescence du contenu de la carte.
     */
    opened: true,
    /**
     * Propriete: index
     * {String} Index hiérarchique du groupe dans l'arborescence.
     *
     * :
     * Automatiquement calculé lors de l'ajout du groupe dans la classe <Descartes.MapContent>.
     */
    index: '',
    /**
     * Propriete: addedByUser
     * {Boolean} Indique si le groupe a été ajouté par l'utilisateur ou fourni initialement par l'application.
     */
    addedByUser: false,
    /**
     * Propriete: items
     * {Array(<Descartes.Group> | <Descartes.Layer>)} Eléments du groupe.
     */
    items: null,
    /**
     * Constructeur: Descartes.Group
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé du groupe.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * opened - {Boolean} Indique si le groupe est déplié ou non dans la vue affichant l'arborescence du contenu de la carte.
     */
    initialize: function (title, options) {
        this.items = [];
        this.title = title;
        if (!_.isNil(options)) {
            _.extend(this, options);
        }
    },
    /**
     * Methode: setVisibility
     * Modifie la visibilité du groupe.
     *
     * Paramètres:
     * isVisible - {Boolean | null} Visibilité souhaitée.
     */
    setVisibility: function (isVisible) {
        this.visible = isVisible;
    },
    /**
     * Methode: setFromJSON
     * Met à jour les propriétés modifiables du groupe.
     *
     * :
     * Les propriétés modifiables (par un outil de type <Descartes.Action.MapContentManager.ALTER_GROUP_TOOL>) sont :
     *
     * :
     * title - {String} Intitulé du groupe.
     * opened - {Boolean} Déplié ou non.
     */
    setFromJSON: function (json) {
        _.extend(this, json);
    },
    /**
     * Methode: toJSON
     * Fournit une représentation simplifié du groupe sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON sous la forme {title: <Titre>, opened: <Déplié ou non>}.
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.opened = this.opened;
        if (this.addedByUser) {
           json.infosContent = {};
           json.infosContent.nbLayer = this.countLayerInGroup(this.items);
           json.infosContent.nbLayerAddedByUser = this.countLayerAddedByUserInGroup(this.items);
        }
        return json;
    },
    /**
     * Methode: serialize
     * Fournit une représentation du groupe sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = {};
        json.itemType = 'Group';
        json.title = this.title;
        json.options = {
            opened: this.opened,
            addedByUser: this.addedByUser,
            visible: this.visible
        };
        return json;
    },
    countLayerInGroup: function (items) {
        var nbLayer = 0;
        for(var i = 0; i < items.length; i++) {
           if (items[i] instanceof Descartes.Layer) {
              nbLayer++;
           } else if (items[i] instanceof Descartes.Group) {
              nbLayer += this.countLayerInGroup(items[i].items);
           }
        }
        return nbLayer;
    },
    countLayerAddedByUserInGroup: function (items) {
        var nbLayerAddedByUser = 0;
        for(var i = 0; i < items.length; i++) {
           if (items[i] instanceof Descartes.Layer) {
              if (items[i].addedByUser) {
                  nbLayerAddedByUser++;
              }
           } else if (items[i] instanceof Descartes.Group) {
              nbLayerAddedByUser += this.countLayerAddedByUserInGroup(items[i].items);
           }
        }
        return nbLayerAddedByUser;
    },
    CLASS_NAME: 'Descartes.Group'
});

module.exports = Group;
