module.exports = {
    /**
     * Constante: DEFAULT_FEATURE_GEOMETRY_NAME
     * Nom de l'attribut contenant la géométrie.
     */
    DEFAULT_FEATURE_GEOMETRY_NAME: 'msGeometry'
};

