var ol = require('openlayers');

var Utils = require('../Utils/DescartesUtils');

/**
 * Class: Descartes.RequestMember
 * Objet "métier" correspondant à un critère de requête attributaire.
 *
 * :
 * La valeur du critère peut être alimentée d'une des trois manières suivantes :
 * - saisie libre
 * - choix dans une liste déroulante de valeurs possibles
 * - valeur fixe
 */
var Class = Utils.Class({
    /**
     * Propriete: title
     * {String} Libellé du critère.
     */
    title: null,

    /**
     * Propriete: field
     * {String} Propriété du FeatureType WFS à interroger.
     */
    field: null,

    /**
     * Propriete: operator
     * Opérateur de comparaison pour le critère.
     *
     * :
     * La valeur doit être une des suivantes :
     * - ol.format.filter.Comparison.EQUAL_TO (==)
     * - ol.format.filter.Comparison.NOT_EQUAL_TO (!=)
     * - ol.format.filter.Comparison.LESS_THAN (<)
     * - ol.format.filter.Comparison.GREATER_THAN (>)
     * - ol.format.filter.Comparison.LESS_THAN_OR_EQUAL_TO (<=)
     * - ol.format.filter.Comparison.GREATER_THAN_OR_EQUAL_TO (>=)
     * - ol.format.filter.Comparison.LIKE (~)
     */
    operator: null,

    /**
     * Propriete: value
     * {String | Array(String)} Valeur du critère s'il est fixe ou liste des valeurs proposées s'il correspond à une liste.
     */
    value: null,

    /**
     * Propriete: visible
     * {Boolean} Indique si le critère est disponible pour la saisie d'une valeur. "False" permet de prendre en compte des critères fixes.
     */
    visible: true,

    /**
     * Propriete: list
     * {Boolean} Indique si les valeurs possibles du critère sont restreints à une liste de choix.
     */
    list: false,
    /**
     * Constructeur: Descartes.RequestMember
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Libellé du critère.
     * field - {String} Propriété du FeatureType WFS à interroger.
     * operator - Opérateur de comparaison pour le critère.
     * value - {String | Array(String)} Valeur du critère s'il est fixe ou liste des valeurs proposées s'il correspond à une liste.
     * visible - {Boolean} Indique si le critère est disponible pour la saisie d'une valeur.
     */
    initialize: function (title, field, operator, value, visible) {
        this.title = title;
        this.field = field;
        this.operator = operator;
        this.value = value;
        this.visible = visible;
        if (this.value.length > 1) {
            this.list = true;
        }
    },
    /**
     * Methode: addMember
     * Ajoute un critère à la requête.
     *
     * Paramètres:
     * member - <Descartes.RequestMember>) Critère de requête attributaire.
     */
    addMember: function (member) {
        this.members.push(member);
    },

    /**
     * Methode: getFilter
     * Fournit un filtre de comparaison construit en prenant en compte la valeur saisie pour le critère.
     *
     * Paramètres:
     * valueIHM - {String} Valeur saisie pour le critère.
     *
     * Retour:
     * {ol.format.filter} Filtre de comparaison généré.
     */
    getFilter: function (valueIHM) {
        var value = this.visible ? valueIHM : this.value;
        value = value.toString().replace(/&#39;/g, "'").replace(/'/g, "\\'");
        var switchToIsLike = false;

        if (!this.list && this.visible) {
            if (value.toString().indexOf('é') !== -1) {
                switchToIsLike = true;
                value = value.toString().replace(/é/g, '.');
            }
            if (value.toString().indexOf('è') !== -1) {
                switchToIsLike = true;
                value = value.toString().replace(/è/g, '.');
            }
            if (value.toString().indexOf('ê') !== -1) {
                switchToIsLike = true;
                value = value.toString().replace(/ê/g, '.');
            }
            if (value.toString().indexOf('à') !== -1) {
                switchToIsLike = true;
                value = value.toString().replace(/à/g, '.');
            }
            if (value.toString().indexOf('ù') !== -1) {
                switchToIsLike = true;
                value = value.toString().replace(/ù/g, '.');
            }
            if (value.toString().indexOf('ç') !== -1) {
                switchToIsLike = true;
                value = value.toString().replace(/ç/g, '.');
            }
        }

        if (switchToIsLike) {
            //GB MODIF: PB ol.format.filter.IsLike retourne "undefined"
            //return ol.format.filter.IsLike(this.field, value, '*', '.', '!', false);
            var switchFilterIsLike = ol.format.filter.equalTo(this.field, '*' + value + '*', false);
            switchFilterIsLike.tagName_ = "PropertyIsLike";
            switchFilterIsLike.pattern = switchFilterIsLike.expression;
            switchFilterIsLike.wildCard = "*";
            switchFilterIsLike.singleChar = ".";
            switchFilterIsLike.escapeChar = "!"; //ol ne gere que "escapeChar" pour GS, pour MS, il faut "escape" (gerer dans "Request.js")
            return switchFilterIsLike;
        } else {
            switch (this.operator) {
                case '~':
                    //GB MODIF: PB ol.format.filter.IsLike retourne "undefined"
                    //return ol.format.filter.IsLike(this.field,  '*' + value + '*', '*', '.', '!', false);
                    var filterIsLike = ol.format.filter.equalTo(this.field, '*' + value + '*', false);
                    filterIsLike.tagName_ = "PropertyIsLike";
                    filterIsLike.pattern = filterIsLike.expression;
                    filterIsLike.wildCard = "*";
                    filterIsLike.singleChar = ".";
                    filterIsLike.escapeChar = "!"; //ol ne gere que "escapeChar" pour GS, pour MS, il faut "escape" (gerer dans "Request.js")
                    return filterIsLike;
                case '==' :
                    if (!this.list && this.visible) {
                        return ol.format.filter.equalTo(this.field, value, false);
                    }
                    return ol.format.filter.equalTo(this.field, value, true);
                case '!=':
                    return ol.format.filter.notEqualTo(this.field, value, true);
                case '<' :
                    return ol.format.filter.LessThan(this.field, value);
                case '<=':
                    return ol.format.filter.LessThanOrEqualTo(this.field, value);
                case '>':
                    return ol.format.filter.GreaterThan(this.field, value);
                case '>=':
                    return ol.format.filter.GreaterThanOrEqualTo(this.field, value);
            }
        }
        return null;
    },

    /**
     * Methode: getJsonFilter
     * Fournit un filtre de comparaison construit en prenant en compte la valeur saisie pour le critère.
     *
     * Paramètres:
     * valueIHM - {String} Valeur saisie pour le critère.
     *
     * Retour:
     * {ol.format.filter} Filtre de comparaison généré.
     */
    getJsonFilter: function (valueIHM) {
        var value = this.visible ? valueIHM : this.value;
        value = value.toString().replace(/&#39;/g, "'").replace(/'/g, "\\'");
        var switchToIsLike = false;

        if (!this.list && this.visible) {
            if (value.toString().indexOf('é') !== -1) {
                switchToIsLike = true;
                value = value.toString().replace(/é/g, '.');
            }
            if (value.toString().indexOf('è') !== -1) {
                switchToIsLike = true;
                value = value.toString().replace(/è/g, '.');
            }
            if (value.toString().indexOf('ê') !== -1) {
                switchToIsLike = true;
                value = value.toString().replace(/ê/g, '.');
            }
            if (value.toString().indexOf('à') !== -1) {
                switchToIsLike = true;
                value = value.toString().replace(/à/g, '.');
            }
            if (value.toString().indexOf('ù') !== -1) {
                switchToIsLike = true;
                value = value.toString().replace(/ù/g, '.');
            }
            if (value.toString().indexOf('ç') !== -1) {
                switchToIsLike = true;
                value = value.toString().replace(/ç/g, '.');
            }
        }

        if (switchToIsLike) {
            return ['*=', this.field, '*' + value + '*'];
        } else {
            switch (this.operator) {
                case '~':
                    return ['*=', this.field, '*' + value + '*'];
                case '==' :
                    if (!this.list && this.visible) {
                        return ['==', this.field, value];
                    }
                    return ['==', this.field, value];
                case '!=':
                    return ['!=', this.field, value];
                case '<' :
                    return ['<', this.field, value];
                case '<=':
                    return ['<=', this.field, value];
                case '>':
                    return ['>', this.field, value];
                case '>=':
                    return ['>=', this.field, value];
            }
        }
        return null;
    },

    CLASS_NAME: 'Descartes.RequestMember'
});

module.exports = Class;
