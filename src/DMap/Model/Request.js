var _ = require('lodash');
var ol = require('openlayers');
var GeoStylerSLDParser = require('geostyler-sld-parser');
var xml2js = require('xml2js');

var Utils = require('../Utils/DescartesUtils');
var Symbolizers = require('../Symbolizers');

/**
 * Class: Descartes.Request
 * Objet "métier" correspondant à une requête attributaire.
 */
var Class = Utils.Class({
    /**
     * Propriete: layer
     * {<Descartes.Layer>} couche sur laquelle porte la requête.
     */
    layer: null,

    /**
     * Propriete: title
     * {String} Libellé de la requête.
     */
    title: '',

    /**
     * Propriete: geometryType
     * Type de géométrie de la couche.
     *
     * :
     * La valeur doit être une des suivantes :
     * - <Descartes.Layer.POINT_GEOMETRY>
     * - <Descartes.Layer.LINE_GEOMETRY>
     * - <Descartes.Layer.POLYGON_GEOMETRY>
     */
    geometryType: null,

    /**
     * Propriete: members
     * {Array(<Descartes.RequestMember>)} Critères de la requête.
     */
    members: null,

    /**
     * Propriete: exportSelectionLayer
     * {Boolean} Indique si la couche de sélection résultant de la requête doit être incluse dans les exportations PNG et PDF.
     */
    exportSelectionLayer: false,

    /**
     * Propriete: symbolizers
     * {Object} Objet JSON surchargeant les styles par défaut pour la couche graphique de sélection.
     */
    symbolizers: null,
    /**
     * Propriete: selectionLayerLoaderMode
     * {String} Mode de chargement de la couche graphique de sélection.
     */
    selectionLayerLoaderMode: null,
    /**
     * Propriete: selectionLayerUseProxy
     * {String} Utilisation ou non d'un proxy.
     */
    selectionLayerUseProxy: false,
    /**
     * Constructeur: Descartes.Request
     * Constructeur d'instances
     *
     * Paramètres:
     * layer - {<Descartes.Layer>} couche sur laquelle porte la requête.
     * title - {String} Libellé de la requête.
     * geometryType - Type de géométrie de la couche sur laquelle porte la requête.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * symbolizers - {Object} Objet JSON surchargeant les styles par défaut pour la couche graphique de sélection.
     * exportSelectionLayer - {Boolean} Indique si la couche de sélection résultant de la requête doit être incluse dans les exportations PNG et PDF.
     * selectionLayerLoaderMode - {String} Mode de chargement de la couche graphique de sélection.
     * selectionLayerUseProxy - {String} Utilisation ou non d'un proxy.
     */
    initialize: function (layer, title, geometryType, options) {
        this.layer = layer;
        this.members = [];
        this.title = title;
        this.geometryType = geometryType;

        this.symbolizers = _.extend({}, Symbolizers.Descartes_Symbolizers_RequestSLD);
        this.sldparser = new GeoStylerSLDParser.SldStyleParser();
        if (!_.isNil(options)) {
            _.extend(this.symbolizers, options.symbolizers);
            this.exportSelectionLayer = options.exportSelectionLayer || false;
            this.selectionLayerLoaderMode = options.selectionLayerLoaderMode || null;
            this.selectionLayerUseProxy = options.selectionLayerUseProxy || false;
        }
    },
    /**
     * Methode: addMember
     * Ajoute un critère à la requête.
     *
     * Paramètres:
     * member - <Descartes.RequestMember>) Critère de requête attributaire.
     */
    addMember: function (member) {
        this.members.push(member);
    },

    /**
     * Methode: getQueryWithBBOX
     * Fournit un fragment FEI à partir des valeurs de critères saisies pour la génération du tableau des données attributaires.
     *
     * Paramètres:
     * values - {Array(String)} Valeurs saisies pour les critères.
     * maxBBOX - {String} Fragment GML représentant l'emprise maximale de la carte.
     * geometryName - {String} Nom de l'attribut géométrique.
     *
     * Retour:
     * {String} Fragment FEI.
     */
    getQueryWithBBOX: function (values, maxBBOX, geometryName) {
        var filters = this.getFilters(values);
        var feiText = '';
        _.each(filters, function (filter) {
            //by default support wfs version 1.1.0
            var result = ol.format.WFS.writeFilter(filter);
            feiText += result.innerHTML;
        });
        if (feiText === "") {
           feiText = '<BBOX><PropertyName>' + geometryName + '</PropertyName>' + maxBBOX + '</BBOX>';
        } else {
           feiText = '<And>' + feiText + '<BBOX><PropertyName>' + geometryName + '</PropertyName>' + maxBBOX + '</BBOX></And>';
        }
        //si operateur "~" (isLike), Mapserver utilise "escape" et Geoserver utilise "escapeChar"
        feiText = feiText.replace("escapeChar=\"!\"", "escapeChar=\"!\" escape=\"!\"");
        return feiText;
    },

    /**
     * Methode: checkPropertyFilterType
     * Vérification du type de property.
     *
     * Paramètres:
     * values - {Array(String)} Valeurs saisies pour les critères.
     *
     * Retour:
     * {String} Fragment SLD/FEI.
     */
    checkPropertyFilterType: function (values) {
        var filters = this.getFilters(values);
        var feiText = '';
        _.each(filters, function (filter) {
            var result = ol.format.WFS.writeFilter(filter);
            feiText += result.innerHTML;
        });
        //si operateur "~" (isLike), Mapserver utilise "escape" et Geoserver utilise "escapeChar"
        feiText = feiText.replace("escapeChar=\"!\"", "escapeChar=\"!\" escape=\"!\"");
        return feiText;
    },

    /**
     * Methode: getFilters
     * Fournit les filtres de comparaison construits par les critères.
     *
     * Paramètres:
     * values - {Array(Object)} Valeurs saisies pour les critères.
     *
     * Retour:
     * {Array(ol.filter.Comparison)} Filtres de comparaison générés.
     */
    getFilters: function (values) {
        var filters = [];

        _.each(this.members, function (member, index) {
            var valueObj = null;
            if (member.visible) {
                valueObj = values[index];
                if (!_.isNil(valueObj) && !_.isEmpty(valueObj.value)) {
                    filters.push(member.getFilter(valueObj.value));
                }
            } else {
                filters.push(member.getFilter(valueObj.value));
            }
        });
        return filters;
    },

    /**
     * Methode: getJsonFilters
     * Fournit les filtres de comparaison construits par les critères au format json.
     *
     * Paramètres:
     * values - {Array(Object)} Valeurs saisies pour les critères.
     *
     * Retour:
     * {Array(Object)} Filtres de comparaison générés.
     */
    getJsonFilters: function (values) {
        var filters = [];

        _.each(this.members, function (member, index) {
            var valueObj = null;
            if (member.visible) {
                valueObj = values[index];
                if (!_.isNil(valueObj) && !_.isEmpty(valueObj.value)) {
                    filters.push(member.getJsonFilter(valueObj.value));
                }
            } else {
                filters.push(member.getJsonFilter(valueObj.value));
            }
        });
        return filters;
    },

    CLASS_NAME: 'Descartes.Request'
});

module.exports = Class;
