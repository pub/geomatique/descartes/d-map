/* global Descartes */

var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var UrlExterne = require('./UrlExterne');
var LocalisationParcellaireConstants = require('./LocalisationParcellaireConstants');

/**
 * Class: Descartes.LocalisationParcellaire
 * Objet "métier" correspondant à un service externe pour la fourniture des objets de référence.
 */
var Class = Utils.Class({
    /**
     * Propriete: url
     * {<Descartes.UrlExterne>} URL du service.
     */
    url: null,

    /**
     * Propriete: levelParam
     * {String} Nom de la clé définissant le niveau recherché dans la pyramide.
     */
    levelParam: null,

    /**
     * Propriete: projectionParam
     * {String} Nom de la clé définissant la projection.
     */
    projectionParam: null,

    /**
     * Propriete: codeParam
     * {String} Nom de la clé définissant le code de l'objet "père" des objets recherchés.
     */
    codeParam: null,

    /**
     * Propriete: urlService
     * {String} Url du service distant.
     */
    urlService: null,

    /**
     * Constructeur: LocalisationParcellaire
     * Constructeur d'instances
     *
     * Paramètres:
     * niveauBase - {String} Niveau de base.
     */
    initialize: function (niveauBase) {
        this.levelParam = 'niveau';
        this.projectionParam = 'projection';
        this.codeParam = 'parent';
        this.urlService = Descartes.GEOREF_SERVER + '/localize?';
        this.url = new UrlExterne(this.urlService);
        this.url.addParam('niveauBase', niveauBase);
    },
    CLASS_NAME: 'Descartes.LocalisationParcellaire'
});

_.extend(Class, LocalisationParcellaireConstants);

module.exports = Class;
