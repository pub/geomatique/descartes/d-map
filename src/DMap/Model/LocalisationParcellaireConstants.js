
/**
 * Constantes utilisées pour la localisation parcellaire.
 */
module.exports = {
    /**
     * Constante: NIVEAU_REGION
     * Le niveau région est le niveau de base.
     */
    NIVEAU_REGION: 0,
    /**
     * Constante: NIVEAU_DEPARTEMENT
     * Le niveau département est le niveau de base.
     */
    NIVEAU_DEPARTEMENT: 1,
    /**
     * Constante: NIVEAU_COMMUNE
     * Le niveau commune est le niveau de base.
     */
    NIVEAU_COMMUNE: 2,
    /**
     * Constante: TYPE_SERVICE
     * Type de service.
     */
    TYPE_SERVICE: "localisationParcellaire"
};
