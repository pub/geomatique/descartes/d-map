var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var Vector = require('./Vector');
var LayerConstants = require('../LayerConstants');
var Symbolizers = require('../../Symbolizers');
var ol = require('openlayers');
var log = require('loglevel');

/**
 * Class: Descartes.Layer.WFS
 * Objet "métier" correspondant à une couche pour le contenu de la carte, basée sur des *ressources WFS*.
 *
 * Hérite de:
 * - <Descartes.Layer.Vector>
 */
var Class = Utils.Class(Vector, {

    olFormat: null,

    /**
     * Constructeur: Descartes.Layer.WFS
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches WFS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     */
    initialize: function (title, layersDefinition, options) {
        this.type = LayerConstants.TYPE_WFS;
        this.symbolizers = _.extend({}, Symbolizers.Descartes_Symbolizers_WFS);

        Vector.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },
    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers Vector représentant la couche WFS.
     *
     * Retour:
     * {ol.layer.Vector} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {
        var params = this.getCommonParams();
        params.layerName = layerDefinition.layerName;

        var version = '1.1.0';
        if (!_.isNil(layerDefinition.serverVersion)) {
            version = layerDefinition.serverVersion;
        }
        var gmlFormat = new ol.format.GML3();
        if (version === '1.0.0') {
            gmlFormat = new ol.format.GML2();
        }
        this.olFormat = new ol.format.WFS({
            featureNS: layerDefinition.featureNameSpace,
            gmlFormat: gmlFormat
        });
        this.olFormat.wfsVersion = version;

        if (!_.isNil(layerDefinition.internalProjection)) {
            this.olFormat.defaultDataProjection = layerDefinition.internalProjection;
        }
        //this.url = this.getUrl(Utils.adaptationUrl(layerDefinition.serverUrl));
        this.url = Utils.adaptationUrl(layerDefinition.serverUrl);
        var loader = null;

        if (_.isNil(layerDefinition.featureLoaderMode) || layerDefinition.featureLoaderMode === LayerConstants.GET_REQUEST_LOADER) {
            var url = function (extent, resolution, projection) {

                var finalBbox = extent;
                var finalSrsName = projection.getCode();
                if (!_.isNil(layerDefinition.internalProjection)) {
                    finalBbox = ol.proj.transformExtent(extent, projection, layerDefinition.internalProjection);
                    finalSrsName = layerDefinition.internalProjection;
                }

                if (layerDefinition.featureReverseAxisOrientation) {
                    finalBbox = [finalBbox[1], finalBbox[0], finalBbox[3], finalBbox[2]];
                }

                finalBbox = finalBbox.join(',');

                if (layerDefinition.useBboxSrsProjection) {
                    finalBbox += ',' + finalSrsName;
                }

                var typeName = layerDefinition.layerName;
                if (layerDefinition.featurePrefix) {
                    typeName = layerDefinition.featurePrefix + ':' + layerDefinition.layerName;
                }

                var labelTypeName = 'TYPENAME=';
                if (version === '2.0.0') {
                    labelTypeName = 'TYPENAMES=';
                }
                //var finalUrl = Utils.adaptationUrl(this.url);
                var finalUrl = this.url;
                finalUrl += 'SERVICE=WFS';
                finalUrl += '&VERSION=' + version;
                finalUrl += '&REQUEST=GetFeature';
                finalUrl += '&' + labelTypeName + typeName;
                finalUrl += '&SRSNAME=' + finalSrsName;
                finalUrl += '&BBOX=' + finalBbox;

                if (layerDefinition.displayMaxFeatures) {
                    finalUrl += '&MAXFEATURES=' + layerDefinition.displayMaxFeatures;
                }
                if (version === '2.0.0') {//force gml version cause ol 4 not support gml32
                    finalUrl += '&OUTPUTFORMAT=gml3';
                }

                finalUrl = this.getUrl(finalUrl);

                return finalUrl;
            }.bind(this);
            loader = this.getRequestGetLoader(url, this.olFormat, this.type, this._loaderWithRemoveFeatures);
        } else {
            //comme ol2 (voir protocol wfs)
            var geometryName = Utils.DEFAULT_FEATURE_GEOMETRY_NAME;
            if (!_.isNil(layerDefinition.featureGeometryName)) {
                geometryName = layerDefinition.featureGeometryName;
            }
            var prefix = Utils.DEFAULT_FEATURE_PREFIX;
            if (!_.isNil(layerDefinition.featurePrefix)) {
                prefix = layerDefinition.featurePrefix;
            }
            var maxFeatures = null;
            if (!_.isNil(layerDefinition.displayMaxFeatures)) {
                maxFeatures = layerDefinition.displayMaxFeatures;
            }
            this.url = this.getUrl(this.url);
            loader = this.getRequestPostLoader(this.url, this.olFormat, layerDefinition.layerName, geometryName, prefix, maxFeatures, this._loaderWithRemoveFeatures);
        }

        params.source = new ol.source.Vector({
            format: this.olFormat,
            loader: loader,
            strategy: ol.loadingstrategy.bbox
        });
        var vector = new ol.layer.Vector(params);
        return vector;
    },

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 		title: <Intitulé>,
     * 		type: <Type de couche>,
     * 		layersDefinition: {
     * 				layerName: <Nom de la couche ou de l'objet OWS>
     * 				serverUrl: <URL du serveur OWS>
     * 			} [],
     * 		options: {
     * 				format: <Type MIME pour l'affichage des couches OpenLayers>,
     * 				legend: <Adresses d'accés aux légendes>,
     * 				metadataURL: <Adresse d'accés aux informations complémentaires>,
     * 				attribution: <Texte de copyright>,
     * 				queryable: <Interrogation potentielle>,
     * 				sheetable: <Affichage des propriétés sous forme de tableau>,
     * 				maxScale: <Dénominateur de l'échelle maximale>,
     * 				minScale: <Dénominateur de l'échelle minimale>
     * 			}
     * }
     * (end)
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.type = this.type;
        json.layersDefinition = [{
                layerName: this.OL_layers[0].get('layerName'),
                serverUrl: this.url
            }];
        json.options = {
            format: this.format,
            legend: (this.legend !== null) ? this.legend[0] : '',
            metadataURL: (this.metadataURL !== null) ? this.metadataURL : '',
            attribution: (this.attribution !== null) ? this.attribution : '',
            queryable: this.queryable,
            sheetable: this.sheetable,
            maxScale: (this.maxScale !== null) ? this.maxScale : '',
            minScale: (this.minScale !== null) ? this.minScale : ''
        };
        return json;
    },

    CLASS_NAME: 'Descartes.Layer.WFS'
});

module.exports = Class;
