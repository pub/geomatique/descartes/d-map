var LayerConstants = require('../LayerConstants');
var Utils = require('../../Utils/DescartesUtils');
var Layer = require('../Layer');
var ol = require('openlayers');
var _ = require('lodash');

/**
 * Class: Descartes.Layer.XYZ
 * Objet "métier" correspondant à une couche pour le contenu de la carte, basée sur des *ressources GeoRef XYZ*.
 *
 * Hérite de:
 * - <Descartes.Layer>
 */
var Class = Utils.Class(Layer, {

	queryable: false,

	useProxy: false,

    /**
     * Constructeur: Descartes.Layer.OSM
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     */
    initialize: function (title, layersDefinition, options) {
        this.type = LayerConstants.TYPE_XYZ;
        if (_.isNil(options)) {
           options = {};
        }
        if (options && _.isNil(options.id)) {
           options.id = "descartesXYZ";
        }
        if (options && _.isNil(options.attribution)) {
            options.attribution = "&#169;GeoRef(xyz)";
        }
        Layer.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },

    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers.
     *
     * Retour:
     * {ol.layer.Tile} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {

        var olLayer = new ol.layer.Tile({
            title: this.title,
            descartesLayerId: this.id,
            opacity: this.opacity / 100.0,
            visible: this.visible,
            extent: this.extent,
            minResolution: this.minResolution,
            maxResolution: this.maxResolution,
            source: new ol.source.OSM({
                url: layerDefinition.serverUrl
            })
        });

        return olLayer;
    },

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 	 title: <Intitulé>,
     * 	 type: <Type de couche>,
     * 	 layersDefinition: {
     * 	   serverUrl: <URL du serveur XYZ>
     * 	 } [],
     * 	 options: {
     * 	   format: <Type MIME pour l'affichage des couches OpenLayers>,
     * 	   legend: <Adresses d'accés aux légendes>,
     * 	   metadataURL: <Adresse d'accés aux informations complémentaires>,
     * 	   attribution: <Texte de copyright>,
     * 	   queryable: <Interrogation potentielle>,
     * 	   sheetable: <Affichage des propriétés sous forme de tableau>,
     * 	   maxScale: <Dénominateur de l'échelle maximale>,
     * 	   minScale: <Dénominateur de l'échelle minimale>
     * 	 }
     * }
     * (end)
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.type = this.type;
        json.layersDefinition = [{
            serverUrl: this.url
        }];
        json.options = {
            format: this.format,
            legend: (this.legend !== null) ? this.legend[0] : "",
            metadataURL: (this.metadataURL !== null) ? this.metadataURL : "",
            attribution: (this.attribution !== null) ? this.attribution : "",
            queryable: this.queryable,
            sheetable: this.sheetable,
            maxScale: (this.maxScale !== null) ? this.maxScale : "",
            minScale: (this.minScale !== null) ? this.minScale : ""
        };
        return json;
    },

    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = Layer.prototype.serialize.apply(this);
        return json;
    },

    CLASS_NAME: 'Descartes.Layer.XYZ'
});

module.exports = Class;
