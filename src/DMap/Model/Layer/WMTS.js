var _ = require('lodash');
var LayerConstants = require('../LayerConstants');
var Utils = require('../../Utils/DescartesUtils');
var Tiled = require('./Tiled');
var ol = require('openlayers');

/**
 * Class: Descartes.Layer.WMTS
 * Objet "métier" correspondant à une couche pour le contenu de la carte, basée sur des *ressources WMTS*.
 *
 * Hérite de:
 * - <Descartes.Layer.Tiled>
 */
var Class = Utils.Class(Tiled, {
    /**
     * Propriete: matrixSet
     * {String} Nom de la 'collection' grilles du tuilage.
     *
     * :
     * Voir la documentation d'OpenLayers pour plus d'informations.
     */
    matrixSet: null,

    /**
     * Propriete: matrixIds
     * {Array(Object)} Définitions des grilles du tuilage associées à la 'collection'.
     *
     * :
     * Voir la documentation d'OpenLayers pour plus d'informations.
     */
    matrixIds: null,

    /**
     * Constructeur: Descartes.Layer.WMTS
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches WMTS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * matrixSet - {String} Nom de la 'collection' grilles du tuilage.
     * matrixIds - {Array(Object)} Définitions des grilles du tuilage associées à la 'collection'.
     */
    initialize: function (title, layersDefinition, options) {
        this.type = LayerConstants.TYPE_WMTS;
        Tiled.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },

    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers WMTS participant à l'agrégat représentant la couche.
     *
     * Retour:
     * {ol.layer.Tile} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {

        var crossOrigin = null;
        if (!_.isNil(layerDefinition.crossOrigin)) {
            crossOrigin = layerDefinition.crossOrigin;
        }

        var style = 'default';
        if (!_.isNil(layerDefinition.layerStyles)) {
            style = layerDefinition.layerStyles;
        }

        var tileGrid = new ol.tilegrid.WMTS({
            extent: this.extent,
            origin: this.origin,
            origins: this.origins,
            resolutions: this.resolutions,
            matrixIds: this.matrixIds,
            tileSize: this.tileSize
        });

        var urls = [];
        if (_.isString(layerDefinition.serverUrl)) {
            var url = this.getUrl(layerDefinition.serverUrl);
            urls.push(url);
        } else if (_.isArray(layerDefinition.serverUrl)) {
            _.forEach(layerDefinition.serverUrl, function (serverUrl) {
                var url = this.getUrl(serverUrl);
                urls.push(url);
            }.bind(this));
        }

        var source = new ol.source.WMTS({
            //url: this.getUrl(layerDefinition.serverUrl),
            urls: urls,
            layer: layerDefinition.layerName, //required
            matrixSet: this.matrixSet,
            format: this.format,
            projection: this.projection,
            tileGrid: tileGrid, //required
            style: style, //required dans Descartes V4 vaut '_null'
            attributions: this.attribution,
            crossOrigin: crossOrigin
        });

        var tile = {
            title: this.title,
            descartesLayerId: this.id,
            opacity: this.opacity / 100.0,
            visible: this.visible,
            extent: this.extent,
            source: source
        };
        if (!_.isNil(this.minResolution)) {
            tile["minResolution"] = this.minResolution;
        }
        if (!_.isNil(this.maxResolution)) {
            tile["maxResolution"] = this.maxResolution;
        }
        var olLayer = new ol.layer.Tile(tile);

        return olLayer;
    },

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 	 title: <Intitulé>,
     * 	 type: <Type de couche>,
     * 	 layersDefinition: {
     * 	   layerName: <Nom de la couche ou de l'objet OWS>
     * 	   serverUrl: <URL du serveur OWS>
     * 	 } [],
     * 	 options: {
     * 	   format: <Type MIME pour l'affichage des couches OpenLayers>,
     * 	   legend: <Adresses d'accés aux légendes>,
     * 	   metadataURL: <Adresse d'accés aux informations complémentaires>,
     * 	   attribution: <Texte de copyright>,
     * 	   queryable: <Interrogation potentielle>,
     * 	   sheetable: <Affichage des propriétés sous forme de tableau>,
     * 	   maxScale: <Dénominateur de l'échelle maximale>,
     * 	   minScale: <Dénominateur de l'échelle minimale>
     * 	 }
     * }
     * (end)
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.type = this.type;
        json.layersDefinition = [{
                layerName: this.OL_layers[0].getSource().getLayer(),
                serverUrl: this.OL_layers[0].getSource().getUrls()
            }];
        json.options = {
            format: this.format,
            legend: (this.legend !== null) ? this.legend[0] : '',
            metadataURL: (this.metadataURL !== null) ? this.metadataURL : '',
            attribution: (this.attribution !== null) ? this.attribution : '',
            queryable: this.queryable,
            sheetable: this.sheetable,
            maxScale: (this.maxScale !== null) ? this.maxScale : '',
            minScale: (this.minScale !== null) ? this.minScale : ''
        };
        return json;
    },

    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour
     * la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = Tiled.prototype.serialize.apply(this);
        json.options.extent = this.extent;
        json.options.projection = this.projection;
        json.options.origin = this.origin;
        json.options.origins = this.origins;
        json.options.matrixSet = this.matrixSet;
        json.options.matrixIds = this.matrixIds;
        return json;
    },

    CLASS_NAME: 'Descartes.Layer.WMTS'
});

module.exports = Class;
