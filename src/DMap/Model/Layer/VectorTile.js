var LayerConstants = require('../LayerConstants');
var Utils = require('../../Utils/DescartesUtils');
var Layer = require('../Layer');
var ol = require('openlayers');
var olms = require('ol-mapbox-style');
var _ = require('lodash');

/**
 * Class: Descartes.Layer.VectorTile
 * Objet "métier" correspondant à une couche pour le contenu de la carte, basée sur des *ressources VectorTile*.
 *
 * Hérite de:
 * - <Descartes.Layer>
 */
var Class = Utils.Class(Layer, {

	queryable: false,

	useProxy: false,

    projection: new ol.proj.Projection({code: "EPSG:3857"}),

	vectorTileformat: new ol.format.MVT(),

    maxZoom: 22,

	tileGrid: null,

    /**
     * Constructeur: Descartes.Layer.VectorTile
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     */
    initialize: function (title, layersDefinition, options) {
        this.type = LayerConstants.TYPE_VectorTile;
        Layer.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },

    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers.
     *
     * Retour:
     * {ol.layer.Tile} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {

        if (layerDefinition.internalProjection) {
            this.projection = layerDefinition.internalProjection;
        }

        if (layerDefinition.vectorTileFormat) {
            this.vectorTileformat = layerDefinition.vectorTileFormat;
        }

        if (this.maxZoom) {
            this.tileGrid = ol.tilegrid.createXYZ({maxZoom: this.maxZoom});
        }

        var olLayer = new ol.layer.VectorTile({
            title: this.title,
            descartesLayerId: this.id,
            opacity: this.opacity / 100.0,
            visible: this.visible,
            source: new ol.source.VectorTile({
				tileGrid: this.tileGrid,
				projection: this.projection,
                format: this.vectorTileformat,
                url: layerDefinition.serverUrl
            })
        });

        if (layerDefinition.vectorTileStyle && layerDefinition.vectorTileStyleName) {
            olms.applyStyle(olLayer, layerDefinition.vectorTileStyle, layerDefinition.vectorTileStyleName);
        } else if (layerDefinition.vectorTileStyleUrl && layerDefinition.vectorTileStyleName) {
            var xhr = new XMLHttpRequest();
			xhr.open('GET', layerDefinition.vectorTileStyleUrl);
			xhr.onload = function () {
                if (this.status === 200 && JSON.parse(this.responseText)) {
                    olms.applyStyle(olLayer, this.responseText, layerDefinition.vectorTileStyleName);
                }
            };
            xhr.send();
        }

        return olLayer;
    },

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 	 title: <Intitulé>,
     * 	 type: <Type de couche>,
     * 	 layersDefinition: {
     * 	   serverUrl: <URL du serveur XYZ>
     * 	 } [],
     * 	 options: {
     * 	   format: <Type MIME pour l'affichage des couches OpenLayers>,
     * 	   legend: <Adresses d'accés aux légendes>,
     * 	   metadataURL: <Adresse d'accés aux informations complémentaires>,
     * 	   attribution: <Texte de copyright>,
     * 	   queryable: <Interrogation potentielle>,
     * 	   sheetable: <Affichage des propriétés sous forme de tableau>,
     * 	   maxScale: <Dénominateur de l'échelle maximale>,
     * 	   minScale: <Dénominateur de l'échelle minimale>
     * 	 }
     * }
     * (end)
     */
    toJSON: function () {
        var json = Layer.prototype.toJSON.apply(this);
        return json;
    },

    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = Layer.prototype.serialize.apply(this);
        return json;
    },

    CLASS_NAME: 'Descartes.Layer.VectorTile'
});

module.exports = Class;
