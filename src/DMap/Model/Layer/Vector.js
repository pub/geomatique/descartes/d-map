var ol = require('openlayers');

var Layer = require('../Layer');
var Utils = require('../../Utils/DescartesUtils');
var _ = require('lodash');
var defaultSymbolizers = require('../../Symbolizers');

/**
 * Class: Descartes.Layer.Vector
 * Objet "métier" correspondant à une couche pour le contenu de la carte, basée sur des *ressources vectorielles*.
 *
 * Hérite de:
 * - <Descartes.Layer>
 *
 * Classes dérivées:
 * - <Descartes.Layer.WFS>
 * - <Descartes.Layer.KML>
 * - <Descartes.Layer.GeoJSON>
 */
var Vector = Utils.Class(Layer, {
    /**
     * Propriete: maxScale
     * {Float} Dénominateur de l'échelle maximale de visibilité de la couche.
     */
    maxScale: null,

    /**
     * Propriete: minScale
     * {Float} Dénominateur de l'échelle minimale de visibilité de la couche.
     */
    minScale: null,

    /**
     * Propriete: symbolizers
     * {Object} Objet JSON surchargeant les styles vectoriels par défaut.
     */
    symbolizers: null,

    /**
     * Propriete: symbolizersFunction
     * {Function} Fonction surchargeant les styles vectoriels par défaut.
     */
    symbolizersFunction: null,

    /**
     * Utilisation ou non d'un proxy pour récupérer les données.
     */
    useProxy: true,

    /**
     * Constructeur: Descartes.Layer.Vector
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * maxScale - {Float} Dénominateur de l'échelle maximale de visibilité de la couche.
     * minScale - {Float} Dénominateur de l'échelle minimale de visibilité de la couche.
     * symbolizers - {Object} Objet JSON surchargeant les styles par défaut pour la couche graphique de sélection.
     * symbolizersFunction - {Function} Fonction surchargeant les styles par défaut pour la couche graphique de sélection.
     */
    initialize: function (title, layersDefinition, options) {
        if (!_.isNil(options) && !_.isNil(options.symbolizers)) {
            _.extend(this.symbolizers, options.symbolizers);
        }
        if (!_.isNil(options) && !_.isNil(options.symbolizersFunction)) {
            if (typeof options.symbolizersFunction === "string") {
               this.symbolizersFunction = eval('(' + options.symbolizersFunction + ')();');
               delete options.symbolizersFunction;
			} else {
               _.extend(this.symbolizersFunction, options.symbolizersFunction);
			}
        }
        Layer.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },

    /**
     * Methode: getCommonParams
     * Fournit les paramètres communs à tous les types de couche vectorielle.
     *
     * Retour:
     * {Object} Objet JSON contenant les paramètres communs.
     */
    getCommonParams: function () {
        var olStyles = defaultSymbolizers.getOlStyle(this.symbolizers);
        var styleFunction = function (feature, resolution) {
            var featureStyleFunction = feature.getStyleFunction();
            if (featureStyleFunction) {
                return featureStyleFunction.call(feature, resolution);
            } else {
                var style = olStyles[feature.getGeometry().getType()];
                return style;
            }
        };
        if (this.symbolizersFunction) {
            styleFunction = this.symbolizersFunction;
        }
        var params = {
            title: this.title,
            descartesLayerId: this.id,
            layerName: this.layerName,
            opacity: this.opacity / 100.0,
            visible: this.visible,
            extent: this.extent,
            zIndex: this.zIndex,
            minResolution: this.minResolution,
            maxResolution: this.maxResolution,
            style: styleFunction
        };
        return params;
    },
    /**
     * Methode: acceptMaxScaleRange
     * Indique si l'échelle maximale de la couche est supérieure à l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} L'échelle maximale de la couche est supérieure à l'échelle courante de la carte.
     */
    acceptMaxScaleRange: function () {
        if (this.OL_layers[0].map) {
            var view = this.OL_layers[0].map.getView();
            var units = view.getProjection().getUnits();
            var resolution = view.getResolution();
            var currentScale = Utils.getScaleFromResolution(resolution, units);
            return (this.maxScale === null || (this.maxScale <= currentScale));
        }
        return true;
    },

    /**
     * Methode: acceptMinScaleRange
     * Indique si l'échelle minimale de la couche est inférieure à l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} L'échelle minimale de la couche est inférieure à l'échelle courante de la carte.
     */
    acceptMinScaleRange: function () {
        if (this.OL_layers[0].map) {
            var view = this.OL_layers[0].map.getView();
            var units = view.getProjection().getUnits();
            var resolution = view.getResolution();
            var currentScale = Utils.getScaleFromResolution(resolution, units);
            return (this.minScale === null || (this.minScale >= currentScale));
        }
        return true;
    },

    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = Layer.prototype.serialize.apply(this);
        json.options.maxScale = this.maxScale;
        json.options.minScale = this.minScale;
        json.options.symbolizers = this.symbolizers;
        if (this.symbolizersFunction) {
            json.options.symbolizersFunction = this.symbolizersFunction.toString();
        }
        return json;
    },

    CLASS_NAME: 'Descartes.Layer.Vector'
});

module.exports = Vector;
