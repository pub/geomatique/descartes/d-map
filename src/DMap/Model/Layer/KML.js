var Vector = require('./Vector');
var Utils = require('../../Utils/DescartesUtils');
var ol = require('openlayers');
var LayerConstants = require('../LayerConstants');
var _ = require('lodash');
var Symbolizers = require('../../Symbolizers');

/**
 * Class: Descartes.Layer.KML
 * Objet "métier" correspondant à une couche pour le contenu de la carte, basée sur des *fichiers KML*.
 *
 * Hérite de:
 * - <Descartes.Layer.Vector>
 */
var Class = Utils.Class(Vector, {

    /**
     * Propriete: keepInternalStyles
     * {Boolean} Indique si les styles embarqués dans le KML doivent être conservés.
     */
    keepInternalStyles: false,

    olFormat: null,

    /**
     * Constructeur: Descartes.Layer.KML
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches KML constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * keepInternalStyles - {Boolean} Indique si les styles embarqués dans le KML doivent être conservés.
     */
    initialize: function (title, layersDefinition, options) {
        this.type = LayerConstants.TYPE_KML;

        this.symbolizers = _.extend({}, Symbolizers.Descartes_Symbolizers_KML);

        if (!_.isNil(options) && !_.isNil(options.keepInternalStyles)) {
            this.keepInternalStyles = options.keepInternalStyles;
            delete options.keepInternalStyles;
        }

        Vector.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },

    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers Vector représentant la couche KML.
     *
     * Retour:
     * {ol.layer.Vector} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {
        var params = this.getCommonParams(layerDefinition);

        var optionsFormat = {
            extractStyles: this.keepInternalStyles,
            showPointNames: this.showPointNames,
            defaultDataProjection: 'EPSG:4326' //flux kml toujours en EPSG:4326
        };

        this.olFormat = new ol.format.KML(optionsFormat);

        params.source = new ol.source.Vector({
            loader: this.getRequestGetLoader(this.getUrl(layerDefinition.serverUrl), this.olFormat, this.type, this._loaderWithRemoveFeatures),
            strategy: ol.loadingstrategy.bbox
        });

        var vector = new ol.layer.Vector(params);

        return vector;
    },

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 		title: <Intitulé>,
     * 		type: <Type de couche>,
     * 		layersDefinition: {
     * 				serverUrl: <URL du serveur OWS>
     * 			} [],
     * 		options: {
     * 				format: <Type MIME pour l'affichage des couches OpenLayers>,
     * 				legend: <Adresses d'accés aux légendes>,
     * 				metadataURL: <Adresse d'accés aux informations complémentaires>,
     * 				attribution: <Texte de copyright>,
     * 				queryable: <Interrogation potentielle>,
     * 				sheetable: <Affichage des propriétés sous forme de tableau>,
     * 				maxScale: <Dénominateur de l'échelle maximale>,
     * 				minScale: <Dénominateur de l'échelle minimale>
     * 			}
     * }
     * (end)
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.type = this.type;
        json.layersDefinition = [{
                serverUrl: this.OL_layers[0].getSource().getUrl()
            }];
        json.options = {
            format: this.format,
            legend: (this.legend !== null) ? this.legend[0] : '',
            metadataURL: (this.metadataURL !== null) ? this.metadataURL : '',
            attribution: (this.attribution !== null) ? this.attribution : '',
            queryable: this.queryable,
            sheetable: this.sheetable,
            maxScale: (this.maxScale !== null) ? this.maxScale : '',
            minScale: (this.minScale !== null) ? this.minScale : ''
        };
        return json;
    },

    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = Vector.prototype.serialize.apply(this);
        json.options.keepInternalStyles = this.keepInternalStyles;
        return json;
    },

    CLASS_NAME: 'Descartes.Layer.KML'
});

module.exports = Class;
