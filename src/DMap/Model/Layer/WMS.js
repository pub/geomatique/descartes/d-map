/* global Descartes */
var Layer = require('../Layer');
var LayerConstants = require('../LayerConstants');
var Utils = require('../../Utils/DescartesUtils');
var ol = require('openlayers');
var _ = require('lodash');

/**
 * Class: Descartes.Layer.WMS
 * Objet "métier" correspondant à une couche pour le contenu de la carte, basée sur des *ressources WMS non-tuilées*.
 *
 * Hérite de:
 * - <Descartes.Layer>
 */
var WMS = Utils.Class(Layer, {
    /**
     * Propriete: maxScale
     * {Float} Dénominateur de l'échelle maximale de visibilité de la couche.
     */
    maxScale: null,

    /**
     * Propriete: minScale
     * {Float} Dénominateur de l'échelle minimale de visibilité de la couche.
     */
    minScale: null,

    /**
     * Propriete: ratio
     * {Float} Proportion d'agrandissement des 'images' WMS par rapport à la taille de la carte.
     *
     * :
     * Voir la documentation d'OpenLayers pour plus d'informations.
     */
    ratio: 1,

    geometryType: null,

    initialize: function () {
        this.type = LayerConstants.TYPE_WMS;
        Layer.prototype.initialize.apply(this, arguments);
        if (Descartes.Layer.ResultLayer.config.randomColorStyle) {
            this._randomColorStyleGeostylerStyles = Descartes.Symbolizers.getRandomColorStyleGeostylerStyles();
        }
    },

    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers WMS non-tuilée participant à l'agrégat représentant la couche.
     *
     * Retour:
     * {ol.layer.Image} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {
        var params = {
            LAYERS: layerDefinition.layerName,
            ISBASELAYER: false,
            TRANSPARENT: !(this.format === 'image/jpg' || this.format === 'image/jpeg'),
            FORMAT: this.format,
            VERSION: '1.1.1'
        };

        if (!_.isNil(layerDefinition.serverVersion)) {
            params.VERSION = layerDefinition.serverVersion;
            if (params.VERSION === '1.3' || params.VERSION === '1.3.0') {
                params.EXCEPTIONS = 'inimage';
            }
        }

        if (!_.isNil(layerDefinition.layerStyles)) {
            params.STYLES = layerDefinition.layerStyles;
        }
        if (!_.isNil(layerDefinition.layerSldBody)) {
            params.SLD_BODY = layerDefinition.layerSldBody;
            params.SLD_VERSION = "1.0.0";
            if (!_.isNil(layerDefinition.layerSldVersion)) {
                params.SLD_VERSION = layerDefinition.layerSldVersion;
            }
        }
        var projection = null;
        if (!_.isNil(layerDefinition.internalProjection)) {
            projection = layerDefinition.internalProjection;
            params.SRS = projection;
        }

        var crossOrigin = null;
        if (!_.isNil(layerDefinition.crossOrigin)) {
            crossOrigin = layerDefinition.crossOrigin;
        }

        var wms = new ol.layer.Image({
            title: this.title,
            layerName: layerDefinition.layerName,
            descartesLayerId: this.id,
            opacity: this.opacity / 100.0,
            visible: this.visible,
            extent: this.extent,
            minResolution: this.minResolution,
            maxResolution: this.maxResolution,
            zIndex: this.zIndex,
            source: new ol.source.ImageWMS({
                url: this.getUrl(layerDefinition.serverUrl),
                serverType: layerDefinition.serverType,
                ratio: this.ratio,
                projection: projection,
                params: params,
                attributions: this.attribution,
                crossOrigin: crossOrigin
            })
        });

        //Pour la mise en place des couches supports mais désactivé à la création de la couche
        //this._createOLFeatureResourceLayer();

        return wms;
    },

    /**
     * Methode: acceptMaxScaleRange
     * Indique si l'échelle maximale de la couche est supérieure à l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} L'échelle maximale de la couche est supérieure à l'échelle courante de la carte.
     */
    acceptMaxScaleRange: function () {
        if (this.OL_layers[0].map) {
            var view = this.OL_layers[0].map.getView();
            var units = view.getProjection().getUnits();
            var resolution = view.getResolution();
            var currentScale = Utils.getScaleFromResolution(resolution, units);
            return (this.maxScale === null || (this.maxScale <= currentScale));
        }
        return true;
    },

    /**
     * Methode: acceptMinScaleRange
     * Indique si l'échelle minimale de la couche est inférieure à l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} L'échelle minimale de la couche est inférieure à l'échelle courante de la carte.
     */
    acceptMinScaleRange: function () {
        if (this.OL_layers[0].map) {
            var view = this.OL_layers[0].map.getView();
            var units = view.getProjection().getUnits();
            var resolution = view.getResolution();
            var currentScale = Utils.getScaleFromResolution(resolution, units);
            return (this.minScale === null || (this.minScale >= currentScale));
        }
        return true;
    },

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    toJSON: function () {
        var json = Layer.prototype.toJSON.apply(this);
        json.layersDefinition[0].serverVersion = this.OL_layers[0].getSource().getParams().VERSION;
        json.layersDefinition[0].layerStyles = this.OL_layers[0].getSource().getParams().STYLES;
        return json;
    },

    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = Layer.prototype.serialize.apply(this);
        json.options.maxScale = this.maxScale;
        json.options.minScale = this.minScale;
        json.options.ratio = this.ratio;
        if (this.isResultLayer) {
           json.options.isResultLayer = this.isResultLayer;
           json.options.resultLayerDatas = this.resultLayerDatas;
           json.options.resultLayerRequestParamsInfosJson = this.resultLayerRequestParamsInfosJson;
        }
        return json;
    },

    CLASS_NAME: 'Descartes.Layer.WMS'
});

module.exports = WMS;
