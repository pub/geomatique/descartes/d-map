var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var Vector = require('./Vector');
var LayerConstants = require('../LayerConstants');
var Symbolizers = require('../../Symbolizers');
var ol = require('openlayers');
var log = require('loglevel');

/**
 * Class: Descartes.Layer.GenericVector
 * Objet "métier" correspondant à une couche pour le contenu de la carte, basée sur des *ressources *ressources génériques* (alimentation manuelle)
 *
 * Hérite de:
 * - <Descartes.Layer.Vector>
 */
var Class = Utils.Class(Vector, {

	_loaderWithRemoveFeatures: true,

    /**
     * Constructeur: Descartes.Layer.GenericVector
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches WFS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     */
    initialize: function (title, layersDefinition, options) {
        this.type = LayerConstants.TYPE_GenericVector;
        this.symbolizers = _.extend({}, Symbolizers.Descartes_Symbolizers_GenericVector);
        if (_.isNil(layersDefinition)) {
            layersDefinition = [{}];
        }
        Vector.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },
    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers Vector représentant la couche générique.
     *
     * Retour:
     * {ol.layer.Vector} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {
        var params = this.getCommonParams();

        var opts = {
            strategy: ol.loadingstrategy.bbox
        };
        var that = this;
        var loader = function (extent, resolution, projection) {
            this.dispatchEvent('featureloadstart');
            if (that._loaderWithRemoveFeatures && (layerDefinition.displayFeatures || layerDefinition.loaderFunction)) {
                this.clear();
            }
            if (layerDefinition.loaderFunction) {
                layerDefinition.loaderFunction.call(this, extent, resolution, projection);
            }
            if (layerDefinition.displayFeatures) {
                this.addFeatures(layerDefinition.displayFeatures);
            }
            this.dispatchEvent('featureloadend');
        };
        opts.loader = loader;

        params.source = new ol.source.Vector(opts);
        var vector = new ol.layer.Vector(params);
        return vector;
    },

    /**
     * Methode: addFeatures
     * Permet d'ajouter des objets dans la couche.
     *
     */
    addFeatures: function (features) {
        var addObjects = features;
        if (!(addObjects instanceof Array)) {
            addObjects = [addObjects];
        }
        this.OL_layers[0].getSource().addFeatures(addObjects);
    },

    /**
     * Methode: getFeatures
     * Permet de récupérer les objets de la couche.
     *
     */
    getFeatures: function () {
        return this.OL_layers[0].getSource().getFeatures();
    },

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 		title: <Intitulé>,
     * 		type: <Type de couche>,
     * 		layersDefinition: {
     * 				layerName: <Nom de la couche ou de l'objet OWS>
     * 				serverUrl: <URL du serveur OWS>
     * 			} [],
     * 		options: {
     * 				format: <Type MIME pour l'affichage des couches OpenLayers>,
     * 				legend: <Adresses d'accés aux légendes>,
     * 				metadataURL: <Adresse d'accés aux informations complémentaires>,
     * 				attribution: <Texte de copyright>,
     * 				queryable: <Interrogation potentielle>,
     * 				sheetable: <Affichage des propriétés sous forme de tableau>,
     * 				maxScale: <Dénominateur de l'échelle maximale>,
     * 				minScale: <Dénominateur de l'échelle minimale>
     * 			}
     * }
     * (end)
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.type = this.type;
        json.layersDefinition = [{
                loaderFct: this.OL_layers[0].getSource().loader_
            }];
        json.options = {
            legend: (this.legend !== null) ? this.legend[0] : '',
            metadataURL: (this.metadataURL !== null) ? this.metadataURL : '',
            attribution: (this.attribution !== null) ? this.attribution : '',
            queryable: this.queryable,
            sheetable: this.sheetable,
            maxScale: (this.maxScale !== null) ? this.maxScale : '',
            minScale: (this.minScale !== null) ? this.minScale : ''
        };
        return json;
    },
        /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = Vector.prototype.serialize.apply(this);
        return json;
    },

    CLASS_NAME: 'Descartes.Layer.GenericVector'
});

module.exports = Class;
