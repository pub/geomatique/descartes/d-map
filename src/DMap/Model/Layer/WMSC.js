var LayerConstants = require('../LayerConstants');
var Utils = require('../../Utils/DescartesUtils');
var Tiled = require('./Tiled');
var ol = require('openlayers');
var _ = require('lodash');

/**
 * Class: Descartes.Layer.WMSC
 * Objet "métier" correspondant é une couche pour le contenu de la carte, basée sur des *ressources WMS tuilées*.
 *
 * Hérite de:
 * - <Descartes.Layer.Tiled>
 *
 */
var Class = Utils.Class(Tiled, {

    /**
     * Constructeur: Descartes.Layer.WMSC
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches WMS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     */
    initialize: function (title, layersDefinition, options) {
        this.type = LayerConstants.TYPE_WMSC;
        Tiled.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },

    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers WMS tuilée participant à l'agrégat représentant la couche.
     *
     * Retour:
     * {ol.layer.Tile} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {
        var params = {
            LAYERS: layerDefinition.layerName,
            STYLES: '',
            FORMAT: this.format,
            VERSION: '1.1.1'
        };

        if (!_.isNil(layerDefinition.serverVersion)) {
            params.VERSION = layerDefinition.serverVersion;
            if (params.VERSION === '1.3' || params.VERSION === '1.3.0') {
                params.EXCEPTIONS = 'inimage';
            }
        }

        if (!_.isNil(layerDefinition.layerStyles)) {
            params.STYLES = layerDefinition.layerStyles;
        }

        if (!_.isNil(layerDefinition.internalProjection)) {
            params.SRS = ol.proj.get(layerDefinition.internalProjection);
        }

        var crossOrigin = null;
        if (!_.isNil(layerDefinition.crossOrigin)) {
            crossOrigin = layerDefinition.crossOrigin;
        }

        var tileGrid = new ol.tilegrid.TileGrid({
            extent: this.extent,
            minZoom: this.minZoom,
            origin: this.origin,
            origins: this.origins,
            resolutions: this.resolutions,
            tileSize: this.tileSize
        });

        var urls = [];
        if (_.isString(layerDefinition.serverUrl)) {
            var url = this.getUrl(layerDefinition.serverUrl);
            urls.push(url);
        } else if (_.isArray(layerDefinition.serverUrl)) {
            _.forEach(layerDefinition.serverUrl, function (serverUrl) {
                var url = this.getUrl(serverUrl);
                urls.push(url);
            }.bind(this));
        }

        var source = new ol.source.TileWMS({
            urls: urls,
            params: params,
            tileGrid: tileGrid,
            attributions: this.attribution,
            crossOrigin: crossOrigin
        });

        var tile = {
            title: this.title,
            descartesLayerId: this.id,
            opacity: this.opacity / 100.0,
            visible: this.visible,
            extent: this.extent,
            zIndex: this.zIndex,
            source: source
        };
        if (!_.isNil(this.minResolution)) {
            tile["minResolution"] = this.minResolution;
        }
        if (!_.isNil(this.maxResolution)) {
            tile["maxResolution"] = this.maxResolution;
        }
        var olLayer = new ol.layer.Tile(tile);

        return olLayer;
    },

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 	 title: <Intitulé>,
     * 	 type: <Type de couche>,
     * 	 layersDefinition: {
     * 	   layerName: <Nom de la couche ou de l'objet OWS>
     * 	   serverUrl: <URL du serveur OWS>
     * 	 } [],
     * 	 options: {
     * 	   format: <Type MIME pour l'affichage des couches OpenLayers>,
     * 	   legend: <Adresses d'accés aux légendes>,
     * 	   metadataURL: <Adresse d'accés aux informations complémentaires>,
     * 	   attribution: <Texte de copyright>,
     * 	   queryable: <Interrogation potentielle>,
     * 	   sheetable: <Affichage des propriétés sous forme de tableau>,
     * 	   maxScale: <Dénominateur de l'échelle maximale>,
     * 	   minScale: <Dénominateur de l'échelle minimale>
     * 	 }
     * }
     * (end)
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.type = this.type;
        json.layersDefinition = [{
                layerName: this.OL_layers[0].layername,
                serverUrl: this.OL_layers[0].url
            }];
        json.options = {
            format: this.format,
            legend: (this.legend !== null) ? this.legend[0] : "",
            metadataURL: (this.metadataURL !== null) ? this.metadataURL : "",
            attribution: (this.attribution !== null) ? this.attribution : "",
            queryable: this.queryable,
            sheetable: this.sheetable,
            maxScale: (this.maxScale !== null) ? this.maxScale : "",
            minScale: (this.minScale !== null) ? this.minScale : ""
        };
        return json;
    },

    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = Tiled.prototype.serialize.apply(this);
        json.options.extent = this.extent;
        json.options.minZoom = this.minZoom;
        json.options.origin = this.origin;
        json.options.origins = this.origins;
        json.options.zIndex = this.zIndex;
        return json;
    },

    CLASS_NAME: 'Descartes.Layer.WMSC'
});

module.exports = Class;
