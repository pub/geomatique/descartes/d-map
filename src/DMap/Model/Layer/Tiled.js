var Layer = require('../Layer');
var Utils = require('../../Utils/DescartesUtils');
var _ = require('lodash');
var ol = require('openlayers');

/**
 * Class: Descartes.Layer.Tiled
 * Objet "métier" correspondant à une couche pour le contenu de la carte, basée sur des *ressources tuilées*.
 *
 * Hérite de:
 * - <Descartes.Layer>
 *
 * Classes dérivées:
 * - <Descartes.Layer.WMSC>
 * - <Descartes.Layer.TMS>
 * - <Descartes.Layer.WMTS>
 */
var Class = Utils.Class(Layer, {
    /**
     * Propriete: tileSize
     * {Array} Dimensions des tuiles.
     *
     * :
     * Voir la documentation d'OpenLayers pour plus d'informations.
     */
    tileSize: [256, 256],

    /**
     * Propriete: maxResolution
     * {Float} Résolution maximale de disponibilité de la couche.
     *
     * :
     * Voir la documentation d'OpenLayers pour plus d'informations.
     */
    maxResolution: null,

    /**
     * Propriete: minResolution
     * {Float} Résolution minimale de disponibilité de la couche.
     *
     * :
     * Voir la documentation d'OpenLayers pour plus d'informations.
     */
    minResolution: null,

    /**
     * Propriete: resolutions
     * {Array(Float)} Résolutions disponibles pour la couche.
     *
     * :
     * Voir la documentation d'OpenLayers pour plus d'informations.
     */
    resolutions: [],

    /**
     * Propriete: buffer
     * {Integer} Nombre de 'couronnes' sollicités autour de la carte.
     *
     * :
     * Voir la documentation d'OpenLayers pour plus d'informations.
     */
    buffer: 0,

    units: 'm',
    /**
     * Constructeur: Descartes.Layer.Tiled
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches OWS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * tileSize - {Array(Object)} Dimensions des tuiles.
     * maxResolution - {Float} Résolution maximale de disponibilité de la couche.
     * minResolution - {Float} Résolution minimale de disponibilité de la couche.
     * resolutions - {Array(Float)} Résolutions disponibles pour la couche.
     * buffer - {Integer} Nombre de 'couronnes' sollicités autour de la carte.
     */
    initialize: function (title, layersDefinition, options) {
        if (options !== undefined && options.tileSize !== undefined) {
            if (_.isArray(options.tileSize)) {
                this.tileSize = options.tileSize;
            } else if (!_.isNil(options.tileSize.w) && !_.isNil(options.tileSize.h)) {
                this.tileSize = [options.tileSize.w, options.tileSize.h];
            }
            delete options.tileSize;
        }
        if (options.resolutions.length !== 0) {
            options.resolutions.sort(function (a, b) {
                return b - a;
            });
            this.maxResolution = options.resolutions[0];
            this.minResolution = options.resolutions[options.resolutions.length - 1];
        }
        if (!_.isNil(options.minScale) && !_.isNil(options.projection)) {
            this.maxResolution = Utils.getResolutionForScale(options.minScale, ol.proj.get(options.projection).getUnits());
        }
        if (!_.isNil(options.maxScale) && !_.isNil(options.projection)) {
            this.minResolution = Utils.getResolutionForScale(options.maxScale, ol.proj.get(options.projection).getUnits());
        }

        Layer.prototype.initialize.apply(this, [title, layersDefinition, options]);
        var units = this.units;
        if (this.OL_layers[0].map) {
            units = this.OL_layers[0].map.getView().getProjection().getUnits();
        }
        if (_.isNil(options.minScale)) {
            this.minScale = Utils.getScaleFromResolution(this.maxResolution, units);
        }
        if (_.isNil(options.maxScale)) {
            this.maxScale = Utils.getScaleFromResolution(this.minResolution, units);
        }
    },

    /**
     * Methode: acceptMaxScaleRange
     * Indique si l'échelle maximale de la couche est supérieure à l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} L'échelle maximale de la couche est supérieure à l'échelle courante de la carte.
     */
    acceptMaxScaleRange: function () {
        if (this.OL_layers[0].map) {
            var currentResolution = this.OL_layers[0].map.getView().getResolution();
            return (this.minResolution === null || (this.minResolution <= currentResolution));
        }
        return true;
    },

    /**
     * Methode: acceptMinScaleRange
     * Indique si l'échelle minimale de la couche est inférieure à l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} L'échelle minimale de la couche est inférieure à l'échelle courante de la carte.
     */
    acceptMinScaleRange: function () {
        if (this.OL_layers[0].map) {
            var currentResolution = this.OL_layers[0].map.getView().getResolution();
            return (this.maxResolution === null || (this.maxResolution >= currentResolution));
        }
        return true;
    },

    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde
     * d'un contexte de consultation.
     *
     * :
     * Doit être surchargée par les classes dérivées.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = Layer.prototype.serialize.apply(this);
        json.options.tileSize = this.tileSize;
        json.options.maxResolution = this.maxResolution;
        json.options.minResolution = this.minResolution;
        json.options.resolutions = this.resolutions;
        json.options.buffer = this.buffer;
        return json;
    },

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 	title: <Intitulé>,
     * 	type: <Type de couche>,
     * 	layersDefinition: {
     *      layerName: <Nom de la couche ou de l'objet OWS>
     * 	serverUrl: <URL du serveur OWS>
     * 	} [],
     * 	options: {
     *      format: <Type MIME pour l'affichage des couches OpenLayers>,
     *      legend: <Adresses d'accés aux légendes>,
     *      metadataURL: <Adresse d'accés aux informations complémentaires>,
     *      attribution: <Texte de copyright>,
     *      queryable: <Interrogation potentielle>,
     *      sheetable: <Affichage des propriétés sous forme de tableau>,
     *      maxScale: <Dénominateur de l'échelle maximale>,
     *      minScale: <Dénominateur de l'échelle minimale>,
     *      id: <Identifiant de la couche>
     * 	}
     * }
     * (end)
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.type = this.type;
        json.layersDefinition = [{
                layerName: this.OL_layers[0].get('title'),
                serverUrl: this.OL_layers[0].getSource().getUrls()
            }];
        json.options = {
            format: this.format,
            legend: (this.legend !== null) ? this.legend[0] : '',
            metadataURL: (this.metadataURL !== null) ? this.metadataURL : '',
            attribution: (this.attribution !== null) ? this.attribution : '',
            queryable: this.queryable,
            sheetable: this.sheetable,
            maxScale: (this.maxScale !== null) ? this.maxScale : '',
            minScale: (this.minScale !== null) ? this.minScale : '',
            id: this.id
        };
        return json;
    },

    CLASS_NAME: 'Descartes.Layer.Tiled'
});

module.exports = Class;
