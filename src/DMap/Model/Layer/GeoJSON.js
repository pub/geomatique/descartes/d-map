var Vector = require('./Vector');
var Utils = require('../../Utils/DescartesUtils');
var ol = require('openlayers');
var LayerConstants = require('../LayerConstants');
var _ = require('lodash');
var Symbolizers = require('../../Symbolizers');

/**
 * Class: Descartes.Layer.GeoJSON
 * Objet "métier" correspondant à une couche pour le contenu de la carte, basée sur des *fichiers GeoJSON*.
 *
 * Hérite de:
 * - <Descartes.Layer.Vector>
 */
var GeoJSON = Utils.Class(Vector, {

    olFormat: null,

    /**
     * Constructeur: Descartes.Layer.GeoJSON
     * Constructeur d'instances
     */
    initialize: function () {
        this.type = LayerConstants.TYPE_GeoJSON;
        this.symbolizers = _.extend({}, Symbolizers.Descartes_Symbolizers_GEOJSON);

        Vector.prototype.initialize.apply(this, arguments);
    },

    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers Vector représentant la couche GeoJSON.
     *
     * Retour:
     * {ol.layer.Vector} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {
        var params = this.getCommonParams();

        //default data projection EPSG:4326
        var optionsFormat = {
            defaultDataProjection: 'EPSG:4326'
        };

        this.olFormat = new ol.format.GeoJSON(optionsFormat);

        if (!_.isNil(layerDefinition.internalProjection)) {
            this.olFormat.defaultDataProjection = layerDefinition.internalProjection;
        }
        params.source = new ol.source.Vector({
            loader: this.getRequestGetLoader(this.getUrl(layerDefinition.serverUrl), this.olFormat, this.type, this._loaderWithRemoveFeatures),
            strategy: ol.loadingstrategy.bbox
        });

        var vector = new ol.layer.Vector(params);

        return vector;
    },

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 		title: <Intitulé>,
     * 		type: <Type de couche>,
     * 		layersDefinition: {
     * 				serverUrl: <URL du serveur OWS>
     * 			} [],
     * 		options: {
     * 				format: <Type MIME pour l'affichage des couches OpenLayers>,
     * 				legend: <Adresses d'accés aux légendes>,
     * 				metadataURL: <Adresse d'accés aux informations complémentaires>,
     * 				attribution: <Texte de copyright>,
     * 				queryable: <Interrogation potentielle>,
     * 				sheetable: <Affichage des propriétés sous forme de tableau>,
     * 				maxScale: <Dénominateur de l'échelle maximale>,
     * 				minScale: <Dénominateur de l'échelle minimale>
     * 			}
     * }
     * (end)
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.type = this.type;
        json.layersDefinition = [{
                serverUrl: this.OL_layers[0].getSource().getUrl()
            }];
        json.options = {
            format: this.format,
            legend: (this.legend !== null) ? this.legend[0] : '',
            metadataURL: (this.metadataURL !== null) ? this.metadataURL : '',
            attribution: (this.attribution !== null) ? this.attribution : '',
            queryable: this.queryable,
            sheetable: this.sheetable,
            maxScale: (this.maxScale !== null) ? this.maxScale : '',
            minScale: (this.minScale !== null) ? this.minScale : ''
        };
        return json;
    },

    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = Vector.prototype.serialize.apply(this);
        return json;
    },

    CLASS_NAME: 'Descartes.Layer.GeoJSON'
});

module.exports = GeoJSON;
