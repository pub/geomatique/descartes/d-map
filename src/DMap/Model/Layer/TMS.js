var LayerConstants = require('../LayerConstants');
var Utils = require('../../Utils/DescartesUtils');
var Tiled = require('./Tiled');
var ol = require('openlayers');
var _ = require('lodash');

/**
 * Class: Descartes.Layer.TMS
 * Objet "métier" correspondant à une couche pour le contenu de la carte, basée sur des *ressources TMS*.
 *
 * Hérite de:
 * - <Descartes.Layer.Tiled>
 */
var Class = Utils.Class(Tiled, {
    /**
     * Propriete: tileOrigin
     * {ol.Coordinate} Coordonnées de l'origine du système de tuilage.
     *
     * :
     * Voir la documentation d'OpenLayers pour plus d'informations.
     */
    tileOrigin: null,

    /**
     * Constructeur: Descartes.Layer.TMS
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches WMS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     * serverResolutions - {Array(Float)} Résolutions disponibles sur le serveur
     *
     * Options de construction propres à la classe:
     * tileOrigin - {ol.Coordinate} Coordonnées de l'origine du système de tuilage.
     */
    initialize: function (title, layersDefinition, options) {
        this.type = LayerConstants.TYPE_TMS;
        if (!_.isNil(options) && !_.isNil(options.tileOrigin)) {
            if (_.isArray(options.tileOrigin)) {
                this.tileOrigin = options.tileOrigin;
            } else if (!_.isNil(options.tileOrigin.lon) && !_.isNil(options.tileOrigin.lat)) {
                this.tileOrigin = [options.tileOrigin.lon, options.tileOrigin.la];
            }
            delete options.tileOrigin;
        }
        Tiled.prototype.initialize.apply(this, [title, layersDefinition, options]);
    },

    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers TMS participant à l'agrégat représentant la couche.
     *
     * Retour:
     * {ol.layer.Tile} La couche OpenLayers créée.
     */
    createOL_layer: function (layerDefinition) {

        var tileGrid = new ol.tilegrid.TileGrid({
            extent: this.extent,
            minZoom: this.minZoom,
            origin: this.origin,
            origins: this.origins,
            resolutions: this.resolutions,
            tileSize: this.tileSize
        });

        var version = '1.0.0';
        if (!_.isNil(layerDefinition.serverVersion)) {
            version = layerDefinition.serverVersion;
        }

        if (!_.isArray(layerDefinition.serverUrl)) {
            layerDefinition.serverUrl = [layerDefinition.serverUrl];
        }

        var crossOrigin = null;
        if (!_.isNil(layerDefinition.crossOrigin)) {
            crossOrigin = layerDefinition.crossOrigin;
        }

        var urls = [];
        _.each(layerDefinition.serverUrl, function (serverUrl) {
            var url = serverUrl;
            url += version;
            url += '/' + layerDefinition.layerName;
            url += '/{z}/{x}/{-y}.' + this.format;

            url = this.getUrl(url);
            urls.push(url);
        }.bind(this));


        //var source = new ol.source.TileDebug({
        var source = new ol.source.TileImage({
            urls: urls,
            tileSize: this.tileSize,
            tileGrid: tileGrid,
            crossOrigin: crossOrigin
        });


        var olLayer = new ol.layer.Tile({
            title: this.title,
            descartesLayerId: this.id,
            opacity: this.opacity / 100.0,
            visible: this.visible,
            extent: this.extent,
            //minResolution: this.minResolution,
            //maxResolution: this.maxResolution,
            zIndex: this.zIndex,
            source: source
        });

        return olLayer;
    },

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 	 title: <Intitulé>,
     * 	 type: <Type de couche>,
     * 	 layersDefinition: {
     * 	   layerName: <Nom de la couche ou de l'objet OWS>
     * 	   serverUrl: <URL du serveur OWS>
     * 	 } [],
     * 	 options: {
     * 	   format: <Type MIME pour l'affichage des couches OpenLayers>,
     * 	   legend: <Adresses d'accés aux légendes>,
     * 	   metadataURL: <Adresse d'accés aux informations complémentaires>,
     * 	   attribution: <Texte de copyright>,
     * 	   queryable: <Interrogation potentielle>,
     * 	   sheetable: <Affichage des propriétés sous forme de tableau>,
     * 	   maxScale: <Dénominateur de l'échelle maximale>,
     * 	   minScale: <Dénominateur de l'échelle minimale>
     * 	 }
     * }
     * (end)
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.type = this.type;
        json.layersDefinition = [{
                layerName: this.OL_layers[0].layername,
                serverUrl: this.OL_layers[0].url
            }];
        json.options = {
            format: this.format,
            legend: (this.legend !== null) ? this.legend[0] : "",
            metadataURL: (this.metadataURL !== null) ? this.metadataURL : "",
            attribution: (this.attribution !== null) ? this.attribution : "",
            queryable: this.queryable,
            sheetable: this.sheetable,
            maxScale: (this.maxScale !== null) ? this.maxScale : "",
            minScale: (this.minScale !== null) ? this.minScale : ""
        };
        return json;
    },

    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = Tiled.prototype.serialize.apply(this);
        json.options.tileOrigin = this.tileOrigin;
        json.options.serverResolutions = this.serverResolutions;
        json.options.extent = this.extent;
        json.options.minZoom = this.minZoom;
        json.options.origin = this.origin;
        json.options.origins = this.origins;
        json.options.zIndex = this.zIndex;
        return json;
    },

    CLASS_NAME: 'Descartes.Layer.TMS'
});

module.exports = Class;
