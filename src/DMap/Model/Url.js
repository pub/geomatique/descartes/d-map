var Utils = require('../Utils/DescartesUtils');
var UrlParam = require('./UrlParam');
var _ = require('lodash');

/**
 * Class: Descartes.Url
 * Classe "technique" permettant d'effectuer des manipulations sur les paramètres de l'URL d'une requête HTTP-GET.
 */
var Class = Utils.Class({
    /**
     * Propriete: root
     * {String} Fragment de l'URL ne comportant pas les paramètres
     */
    root: null,

    /**
     * Propriete: params
     * {Array(<Descartes.UrlParam>)} Ensemble des paramètres de l'URL
     */
    params: [],
    /**
     * Constructeur: Descartes.Url
     * Constructeur d'instances
     *
     * Paramètres:
     * url - {String} URL de la requête HTTP-GET.
     */
    initialize: function (url) {
        this.params = [];
        if (url.indexOf('?') === -1) {
            this.root = url;
        } else {
            if (url.indexOf('?') === (url.length - 1)) {
                this.root = url.substring(0, url.length - 1);
            } else {
                this.root = url.split('?')[0];
                var paramQueryString = url.split('?')[1];
                if (paramQueryString.indexOf('&') === -1) {
                    this.params.push(new UrlParam(paramQueryString.split('=')[0], paramQueryString.split('=')[1]));
                } else {
                    var paramKVs = paramQueryString.split('&');
                    var that = this;
                    _.each(paramKVs, function (paramKV) {
                        that.params.push(new UrlParam(paramKV.split('=')[0], paramKV.split('=')[1]));
                    });
                }
            }
        }
    },
    /**
     * Methode: addParam
     * Ajoute un paramètre à l'URL.
     *
     * Paramètres:
     * paramKey - {String} Clé du paramètre.
     * paramValue - {String} Valeur du paramètre.
     */
    addParam: function (paramKey, paramValue) {
        this.params.push(new UrlParam(paramKey, paramValue));
    },

    /**
     * Methode: addParams
     * Ajoute plusieurs paramètres à l'URL.
     *
     * Paramètres:
     * paramsKV - {Array} Tableau des paramètres sous la forme [ [clé1, valeur1], [clé2, valeur2], [clé3, valeur3], ... ].
     */
    addParams: function (paramsKV) {
        for (var iParamKV = 0; iParamKV < paramsKV.length; iParamKV++) {
            this.addParam(paramsKV[iParamKV][0], paramsKV[iParamKV][1]);
        }
    },

    /**
     * Methode: removeParam
     * Supprime un paramètre de l'URL.
     *
     * Paramètres:
     * paramKey - {String} Clé du paramètre.
     */
    removeParam: function (paramKey) {
        for (var iParam = 0; iParam < this.params.length; iParam++) {
            if (this.params[iParam].key === paramKey) {
                this.params.splice(iParam, 1);
                break;
            }
        }
    },

    /**
     * Methode: removeParams
     * Supprime plusieurs paramètres de l'URL.
     *
     * Paramètres:
     * paramsKey - {Array} Tableau des paramètres sous la forme [ clé1, clé2, clé3, ... ].
     */
    removeParams: function (paramsKey) {
        for (var iParam = 0; iParam < paramsKey.length; iParam++) {
            this.removeParam(paramsKey[iParam]);
        }
    },

    /**
     * Methode: alterParam
     * Modifie un paramètre de l'URL.
     *
     * Paramètres:
     * paramKey - {String} Clé du paramètre.
     * paramValue - {String} Valeur du paramètre.
     */
    alterParam: function (paramKey, paramValue) {
        var done = false;
        for (var iParam = 0; iParam < this.params.length; iParam++) {
            if (this.params[iParam].key === paramKey) {
                this.params[iParam].value = paramValue;
                done = true;
                break;
            }
        }
        return done;
    },

    /**
     * Methode: alterParams
     * Modifie plusieurs paramètres de l'URL.
     *
     * Paramètres:
     * paramsKV - {Array} Tableau des paramètres sous la forme [ [clé1, valeur1], [clé2, valeur2], [clé3, valeur3], ... ].
     */
    alterParams: function (paramsKV) {
        for (var iParamKV = 0; iParamKV < paramsKV.length; iParamKV++) {
            this.alterParam(paramsKV[iParamKV][0], paramsKV[iParamKV][1]);
        }
    },

    /**
     * Methode: alterOrAddParam
     * Ajoute un paramètre à l'URL ou le met à jour s'il existe déjà.
     *
     * Paramètres:
     * paramKey - {String} Clé du paramètre.
     * paramValue - {String} Valeur du paramètre.
     */
    alterOrAddParam: function (paramKey, paramValue) {
        if (!this.alterParam(paramKey, paramValue)) {
            this.addParam(paramKey, paramValue);
        }
    },

    /**
     * Methode: alterOrAddParams
     * Ajoute plusieurs paramètres é l'URL ou les met é jour s'ils existent déjà.
     *
     * Paramètres:
     * paramsKV - {Array} Tableau des paramètres sous la forme [ [clé1, valeur1], [clé2, valeur2], [clé3, valeur3], ... ].
     */
    alterOrAddParams: function (paramsKV) {
        for (var iParamKV = 0; iParamKV < paramsKV.length; iParamKV++) {
            this.alterOrAddParam(paramsKV[iParamKV][0], paramsKV[iParamKV][1]);
        }
    },

    /**
     * Methode: getParamValue
     * Fournit la valeur d'un paramètre de l'URL.
     *
     * Paramètres:
     * paramKey - {String} Clé du paramètre.
     *
     * Retour:
     * {String} Valeur du paramètre.
     */
    getParamValue: function (paramKey) {
        var done = '';
        for (var iParam = 0; iParam < this.params.length; iParam++) {
            if (this.params[iParam].key === paramKey) {
                done = this.params[iParam].value;
                break;
            }
        }
        return done;
    },

    /**
     * Methode: getUrlString
     * Construit l'URL de la requête avec les paramètres courants.
     *
     * Retour:
     * {String} URL de la requête.
     */
    getUrlString: function () {
        var newUrl = this.root;
        if (this.params.length !== 0) {
            newUrl += '?';
            for (var iParam = 0; iParam < this.params.length; iParam++) {
                newUrl += this.params[iParam].getQueryString();
                if (iParam < (this.params.length - 1)) {
                    newUrl += '&';
                }
            }
        }
        return newUrl;
    },
    CLASS_NAME: 'Descartes.Url'
});

module.exports = Class;
