var constants = require('./ResourceLayerConstants');
var ResourceLayer = require('./ResourceLayer');
var _ = require('lodash');

var namespace = ResourceLayer;

namespace = _.extend(namespace, constants);

module.exports = namespace;

