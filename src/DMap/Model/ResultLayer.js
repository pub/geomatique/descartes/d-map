/* global MODE, Descartes */
var _ = require('lodash');
var ol = require('openlayers');
var GeoStylerSLDParser = require('geostyler-sld-parser');
var xml2js = require('xml2js');
var WMS = require('./Layer/WMS');
var LayerConstants = require('./LayerConstants');
var Group = require('./Group');
var Layer = require('./Layer');

var ModalFormDialog = require('../UI/' + MODE + "/ModalFormDialog");
var template = require('../UI/' + MODE + '/templates/ResultLayerConfig.ejs');

/**
 * Class: Descartes.Layer.ResultLayer
 * Singleton permettant l'affichage d'une couche graphique de sélection.
 */
var Object = {
    /**
     * Staticpropriete: OL_layers_result
     * {Array(ol.Layer.Image)} Couches OpenLayers constituant la couche graphique de sélection.
     */
    olLayersResult: [],

    olLayerMarker: null,

    config: {
        randomColorStyle: false,
        displayMarker: false
    },

    /**
     * Staticpropriete: selectionLayersToExport
     * {Array(String)} URLs des ressources WMS/SLD constituant la couche graphique de sélection.
     *
     * :
     * Seules les URLs des ressources destinées à être incluses dans les exports PNG et PDF sont présentes dans cette propriété.
     */
    selectionLayersToExport: [],

    _sldparser: new GeoStylerSLDParser.SldStyleParser(),

    searchDataOnError: false,
    /**
     * Staticmethode: refresh
     * Affiche ou rafraichit la couche graphique de sélection pour les requêtes attributaires
     *
     * Paramètres:
     * requestedLayer - {<Descartes.Request>} Requête déclenchée.
     * olMap - {ol.Map} Carte OpenLayers accueillant la couche de sélection.
     * alwaysExportSelectionLayers - {Boolean} Indique si la couche de sélection doit être systématiquement incluse dans les exportations PNG et PDF.
     * values - {Array(String)} Valeurs saisies pour les critères.
     */
    refresh: function (requestedLayer, olMap, exportAllSelectionLayers, values) {
        _.each(this.olLayersResult, function (olLayerResult) {
            olMap.removeLayer(olLayerResult);
        });
        this.olLayersResult = [];
        this.selectionLayersToExport = [];

        if (!_.isNil(requestedLayer)) {
            var requests = this.prepareRequests(requestedLayer.layer, olMap);
            var that = this;
            _.each(requests, function (request) {
                var exportLayer = (exportAllSelectionLayers === true || requestedLayer.exportSelectionLayer === true);
                var loaderMode = requestedLayer.selectionLayerLoaderMode;
                var useProxy = requestedLayer.selectionLayerUseProxy;
                var symbolizers;
                if (that.config.randomColorStyle) {
                    symbolizers = requestedLayer.layer._randomColorStyleGeostylerStyles[requestedLayer.geometryType];
                } else {
                    symbolizers = Descartes.Symbolizers.getGeostylerStyle(requestedLayer.symbolizers[requestedLayer.geometryType], requestedLayer.geometryType);
                }
                //var symbolizers = Descartes.Symbolizers.getGeostylerStyle(requestedLayer.symbolizers[requestedLayer.geometryType], requestedLayer.geometryType);
                var sldBody = that.getBodySLDRequest(values, requestedLayer, request.wmsLayerName, symbolizers, request.layerTitle);
                that.displayLayer(request, sldBody, olMap, loaderMode, exportLayer, useProxy);
            });
        }
    },

    /**
     * Staticmethode: refreshWithCustomExtent
     * Affiche ou rafraichit la couche graphique de sélection pour les requêtes attributaires
     *
     * Paramètres:
     * requestedLayer - {<Descartes.Request>} Requête déclenchée.
     * olMap - {ol.Map} Carte OpenLayers accueillant la couche de sélection.
     * alwaysExportSelectionLayers - {Boolean} Indique si la couche de sélection doit être systématiquement incluse dans les exportations PNG et PDF.
     * values - {Array(String)} Valeurs saisies pour les critères.
     */
    refreshWithCustomExtent: function (requestedLayer, olMap, exportAllSelectionLayers, values, fluxGeometry) {
        _.each(this.olLayersResult, function (olLayerResult) {
            olMap.removeLayer(olLayerResult);
        });
        this.olLayersResult = [];
        this.selectionLayersToExport = [];

        if (!_.isNil(requestedLayer)) {
            var fluxGeometryForGeoserver = "";
            if (!_.isNil(fluxGeometry)) {
                fluxGeometryForGeoserver = this.adaptFluxGeometryForGeoserver(fluxGeometry);
            }
            var requests = this.prepareRequests(requestedLayer.layer, olMap);
            var that = this;
            _.each(requests, function (request) {
                var exportLayer = (exportAllSelectionLayers === true || requestedLayer.exportSelectionLayer === true);
                var loaderMode = requestedLayer.selectionLayerLoaderMode;
                var useProxy = requestedLayer.selectionLayerUseProxy;
                var symbolizers;
                if (that.config.randomColorStyle) {
                    symbolizers = requestedLayer.layer._randomColorStyleGeostylerStyles[requestedLayer.geometryType];
                } else {
                    symbolizers = Descartes.Symbolizers.getGeostylerStyle(requestedLayer.symbolizers[requestedLayer.geometryType], requestedLayer.geometryType);
                }
                var flxGeometry = fluxGeometry;
                //if (request.serverType === Descartes.Layer.SERVER_TYPE_GEOSERVER) {
                    flxGeometry = fluxGeometryForGeoserver;
                //}
                var sldBody = that.getBodySLDRequestWithCustomExtent(values, requestedLayer, request.wmsLayerName, symbolizers, request.featureGeometryName, flxGeometry, request.layerTitle);
                that.displayLayer(request, sldBody, olMap, loaderMode, exportLayer, useProxy);
            });
        }
    },
    /**
     * Staticmethode: refreshSelections
     * Affiche ou rafraichit la couche graphique de sélection pour les interrogations surfaciques
     *
     * Paramètres:
     * layers - {<Descartes.Request>} Requête déclenchée.
     * olMap - {ol.Map} Carte OpenLayers accueillant la couche de sélection.
     * alwaysExportSelectionLayers - {Boolean} Indique si la couche de sélection doit être systématiquement incluse dans les exportations PNG et PDF.
     * values - {Array(String)} Valeurs saisies pour les critères.
     */
    refreshSelections: function (layers, olMap, params, fluxGeometry) {
        _.each(this.olLayersResult, function (olLayerResult) {
            olMap.removeLayer(olLayerResult);
        });
        this.olLayersResult = [];
        this.selectionLayersToExport = [];
        var that = this;
        if (!_.isNil(layers)) {
            var fluxGeometryForGeoserver = "";
            if (!_.isNil(fluxGeometry)) {
                fluxGeometryForGeoserver = this.adaptFluxGeometryForGeoserver(fluxGeometry);
            }
            _.each(layers, function (layer) {
                var requests = that.prepareRequests(layer, olMap);
                _.each(requests, function (request) {
                    var exportLayer = params.exportAllSelectionLayers;
                    var loaderMode = params.loaderMode;
                    var useProxy = params.selectionLayerUseProxy;
                    var geomType = layer.geometryType;
                    if (_.isNil(geomType)) {
                        geomType = "Polygon";
                    }
                    var symbolizers;
                    if (that.config.randomColorStyle) {
                        symbolizers = layer._randomColorStyleGeostylerStyles[geomType];
                    } else {
                        symbolizers = Descartes.Symbolizers.getGeostylerStyle(params.symbolizers[geomType], geomType);
                    }
                    var flxGeometry = fluxGeometry;
                    //if (request.serverType === Descartes.Layer.SERVER_TYPE_GEOSERVER) {
                        flxGeometry = fluxGeometryForGeoserver;
                    //}
                    var sldBody = that.getBodySLDSelection(request.wmsLayerName, request.featureGeometryName, symbolizers, flxGeometry, request.layerTitle);
                    that.displayLayer(request, sldBody, olMap, loaderMode, exportLayer, useProxy);
                 });
            });
        }

    },

    prepareRequests: function (layer, olMap) {
        var requests = [];
        _.each(layer.resourceLayers, function (resourceLayer) {
            var imageServerParams = resourceLayer.getImageServerParams();
            if (!imageServerParams) { return; }
            var params = {
                LAYERS: imageServerParams.layerName,
                ISBASELAYER: false,
                TRANSPARENT: !(layer.format === 'image/jpg' || layer.format === 'image/jpeg'),
                FORMAT: layer.format,
                VERSION: '1.1.1'
             };

             if (!_.isNil(imageServerParams.serverVersion)) {
                 params.VERSION = imageServerParams.serverVersion;
                 if (params.VERSION === '1.3' || params.VERSION === '1.3.0') {
                     params.EXCEPTIONS = 'inimage';
                 }
             }

             var projection = null;
             if (!_.isNil(imageServerParams.internalProjection)) {
                 projection = imageServerParams.internalProjection;
                 params.SRS = projection;
             }

             var crossOrigin = null;
             if (!_.isNil(imageServerParams.crossOrigin)) {
                 crossOrigin = imageServerParams.crossOrigin;
             }
            if (imageServerParams !== null) {
                var featureServerParams = resourceLayer.getFeatureServerParams();
                if (!featureServerParams) { return; }
                var ExternalCallsUtils = require('../Core/ExternalCallsUtils');
                var layerName = imageServerParams.layerName;
                if (resourceLayer.featureNameSpace) {
                    layerName = resourceLayer.featureNameSpace + ":" + imageServerParams.layerName;
                }
                requests.push({
                    urlExport: ExternalCallsUtils.getWmsRequestForImage(imageServerParams, olMap, layer.format),
                    url: imageServerParams.serverUrl,
                    serverType: resourceLayer.serverType,
                    wmsLayerName: layerName,
                    params: params,
                    layerTitle: layer.title,
                    layerId: layer.id,
                    projection: projection,
                    crossOrigin: crossOrigin,
                    featureGeometryName: featureServerParams.featureGeometryName
                });
            }
        });
        return requests;
    },

    displayLayer: function (request, sldBody, olMap, loaderMode, exportLayer, useProxy) {
        var url = new URL(request.url);
        url.searchParams.set('SLD_BODY', sldBody);
        url.searchParams.set('SLD_VERSION', '1.0.0');
        var extent = olMap.getView().calculateExtent(olMap.getSize());
        var source = new ol.source.ImageWMS({
            url: url.toString(),
            params: request.params,
            projection: request.projection,
            crossOrigin: request.crossOrigin
        });

        if (!_.isNil(loaderMode) && loaderMode === Descartes.Layer.POST_REQUEST_LOADER) {
            source.setImageLoadFunction(function (image, src) {
                var img = image.getImage();
                var urlArray = src.split("?");
                var url = urlArray[0];
                if (useProxy) {
                   url = Descartes.Utils.makeSameOrigin(url, Descartes.PROXY_SERVER);
                }
                var params = urlArray[1];
                var xhr = new XMLHttpRequest();
                xhr.open('POST', url);
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xhr.responseType = 'arraybuffer';
                xhr.onload = function (e) {
                    if (xhr.status === 200) {
                        var contentType = xhr.getResponseHeader('content-type');
                        var uInt8Array = new Uint8Array(this.response);
                        var i = uInt8Array.length;
                        var binaryString = new Array(i);
                        while (i--) {
                            binaryString[i] = String.fromCharCode(uInt8Array[i]);
                        }
                        var data = binaryString.join('');
                        var base64 = window.btoa(data);
                        img.src = "data:" + contentType + ";base64," + base64;
                    }
                };
                xhr.send(params);
            });
        }

        var imageLayer = new ol.layer.Image({
            title: 'resultLayer',
            layerName: request.wmsLayerName,
            extent: extent,
            source: source,
            descartesLayerId: request.layerId
        });

        this.olLayersResult.push(imageLayer);

        if (exportLayer) {
            this.selectionLayersToExport.push(request.urlExport + "&SLD_BODY=" + encodeURIComponent(sldBody));
        }

        olMap.addLayer(imageLayer);
    },

    displayMarker: function (olMap, extent) {
        if (this.config.displayMarker) {
            this.removeMarker(olMap);
            var style = new ol.style.Style({
                text: new ol.style.Text({
                    text: '\uf041', // fa-map-marker
                    font: 'normal 18px FontAwesome',
                    fill: new ol.style.Fill({color: 'black'})
                })
            });
            var centerCoords = ol.extent.getCenter(extent);
            this.olLayerMarker = new ol.layer.Vector({
                style: style,
                source: new ol.source.Vector({
                    features: [
                        new ol.Feature({
                            geometry: new ol.geom.Point(centerCoords)
                        })
                    ]
                })
            });
            olMap.addLayer(this.olLayerMarker);
        }
    },

    removeMarker: function (olMap) {
        if (this.olLayerMarker) {
            olMap.removeLayer(this.olLayerMarker);
        }
    },

    /**
     * Methode: getBodySLDRequest
     * Fournit un fragment SLD/FEI à partir des valeurs de critères saisies pour la génération de la couche graphique de sélection (Assistant Requêtes attributaires).
     *
     * Paramètres:
     * values - {Array(String)} Valeurs saisies pour les critères.
     * wmsLayerName - {String} "LayerName" de la couche WMS.
     *
     * Retour:
     * {String} Fragment SLD/FEI.
     */
    getBodySLDRequest: function (values, layer, wmsLayerName, symbolizers, layerTitle) {
        var filters = layer.getJsonFilters(values);
        var filterFEI;
        if (filters.length === 1) {
            filterFEI = filters[0];
        } else {
            filterFEI = ['&&'];
            _.each(filters, function (filter) {
                 filterFEI.push(filter);
            });
        }

        var sldJson = {
              name: wmsLayerName,
              rules: [{
                 name: layerTitle,
                 filter: filterFEI,
                 symbolizers: symbolizers
                 }]
        };
        var sldXML = '';
        if (sldJson.name !== undefined && sldJson.name !== null) {
            var sldObject = this._sldparser.geoStylerStyleToSldObject(sldJson);
            var builder = new xml2js.Builder({renderOpts: {pretty: false}});
            sldXML = builder.buildObject(sldObject);
            //si operateur "~" (isLike), Mapserver utilise "escape" et Geoserver utilise "escapeChar"
            sldXML = sldXML.replaceAll("escape=\"!\"", "escape=\"!\" escapeChar=\"!\"");
        }
        return sldXML;
    },
    /**
     * Methode: getBodySLDRequestWithCustomExtent
     * Fournit un fragment SLD/FEI à partir des valeurs de critères saisies pour la génération de la couche graphique de sélection (Assistant Requêtes attributaires).
     *
     * Paramètres:
     * values - {Array(String)} Valeurs saisies pour les critères.
     * wmsLayerName - {String} "LayerName" de la couche WMS.
     *
     * Retour:
     * {String} Fragment SLD/FEI.
     */
    getBodySLDRequestWithCustomExtent: function (values, layer, wmsLayerName, symbolizers, featureGeometryName, intersectGeometry, layerTitle) {
        var filters = layer.getJsonFilters(values);
        var filterFEI;
        if (filters.length === 1) {
            filterFEI = filters[0];
        } else {
            filterFEI = ['&&'];
            _.each(filters, function (filter) {
                 filterFEI.push(filter);
            });
        }

        var sldJson = {
              name: wmsLayerName,
              rules: [{
                 name: layerTitle,
                 filter: filterFEI,
                 symbolizers: symbolizers
                 }]
        };
        var sldXML = '';
        if (sldJson.name !== undefined && sldJson.name !== null) {
            var sldObject = this._sldparser.geoStylerStyleToSldObject(sldJson);
            var builder = new xml2js.Builder({renderOpts: {pretty: false}});
            sldXML = builder.buildObject(sldObject);
            //si operateur "~" (isLike), Mapserver utilise "escape" et Geoserver utilise "escapeChar"
            sldXML = sldXML.replaceAll("escape=\"!\"", "escape=\"!\" escapeChar=\"!\"");

            if (intersectGeometry) {
               var filterIntersect = "<Intersects><PropertyName>" + featureGeometryName + "</PropertyName>" + intersectGeometry + "</Intersects>";
               if (sldXML.indexOf("<And>") === -1) {
                   if (sldXML.indexOf("<Filter xmlns=\"http://www.opengis.net/ogc\"><And/>") === -1) {
                       filterIntersect = "<And>" + filterIntersect;
                       sldXML = sldXML.replace("<Filter xmlns=\"http://www.opengis.net/ogc\">", "<Filter>" + filterIntersect);
                       //sldXML = sldXML.replace("</PropertyIsEqualTo></Filter>", "</PropertyIsEqualTo></And></Filter>");
                       //sldXML = sldXML.replace("</PropertyIsLike></Filter>", "</PropertyIsLike></And></Filter>");
                       sldXML = sldXML.replace("</Filter>", "</And></Filter>");
                   } else {
                       sldXML = sldXML.replace("<Filter xmlns=\"http://www.opengis.net/ogc\"><And/>", "<Filter>" + filterIntersect);
                   }
               } else {
                   sldXML = sldXML.replace("<Filter xmlns=\"http://www.opengis.net/ogc\"><And>", "<Filter><And>" + filterIntersect);
               }
            }
        }
        return sldXML;
    },
    /**
     * Methode: getBodySLDSelection
     * Fournit un fragment SLD/FEI à partir des valeurs de critères saisies pour la génération de la couche graphique de sélection (Outils d'interrogations).
     *
     * Paramètres:
     * wmsLayerName - {String} "LayerName" de la couche WMS.
     *
     * Retour:
     * {String} Fragment SLD/FEI.
     */
    getBodySLDSelection: function (wmsLayerName, featureGeometryName, symbolizers, intersectGeometry, layerTitle) {
        var sldJson = {
              name: wmsLayerName,
              rules: [{
                 name: layerTitle,
                 symbolizers: symbolizers
                 }]
        };
        var sldXML = '';
        if (sldJson.name !== undefined && sldJson.name !== null) {
            var sldObject = this._sldparser.geoStylerStyleToSldObject(sldJson);
            var builder = new xml2js.Builder({renderOpts: {pretty: false}});
            sldXML = builder.buildObject(sldObject);
            var filterIntersect = "<Filter><Intersects><PropertyName>" + featureGeometryName + "</PropertyName>" + intersectGeometry + "</Intersects></Filter>";
            sldXML = sldXML.replace("<Rule><Name>" + layerTitle + "</Name>", "<Rule><Name>" + layerTitle + "</Name>" + filterIntersect);
       }
       return sldXML;
    },

    adaptFluxGeometryForGeoserver: function (fluxGeometry) {
        var flux = fluxGeometry.replace(/exterior/g, "outerBoundaryIs");
        flux = flux.replace('posList srsDimension="2"', "posList");
        var flux1 = flux.split("<posList>");
        var flux2 = flux1[1].split("</posList>");
        var coordinates = flux2[0].split(" ");
        var newCoordinates = "";
        for (var i = 0, len = coordinates.length ; i < len; i++) {
            newCoordinates += coordinates[i] + "," + coordinates[i + 1];
            if (i !== len - 2) {
                newCoordinates += " ";
            }
            i++;
        }
        flux = flux1[0] + "<coordinates>" + newCoordinates + "</coordinates>" + flux2[1];
        return flux;
    },

    /**
     * Methode: setMapContentManager
     * Associe le gestionnaire du contenu de la carte au bouton.
     *
     * Paramètres:
     * mapContentManager - {<Descartes.Action.MapContentManager>} Gestionnaire du contenu de la carte (groupes et couches).
     */
    setMapContentManager: function (mapContentManager) {
        this.mapContentManager = mapContentManager;
    },

    addInLayersTree: function (datas, requestParams) {
        if (this.olLayersResult && this.olLayersResult.length > 0) {
            this.jsonResultLayer = this.toJSON(datas, requestParams);
            if (this.jsonResultLayer && this.jsonResultLayer.layersDefinition && this.jsonResultLayer.layersDefinition.length > 0) {
                this.openConfigDialog();
            } else {
                alert("Aucune sélection disponible");
            }
        } else {
            alert("Aucune sélection disponible");
        }
	},
    openConfigDialog: function () {
        var content = template({
          colorFill: "#A5F38D"
        });
        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: "Ajouter la sélection à l'arbre des couches",
            content: content
        });
        dialog.open(this.sendDatas.bind(this), this.cancel.bind(this));

    },
    cancel: function (form) {
    },
    sendDatas: function (form) {
        if (form.nameResultLayer !== "") {
           this.jsonResultLayer.title = form.nameResultLayer;
        }
        if (form.colorFillResultLayer !== "#A5F38D") {
           var color = form.colorFillResultLayer.split('#')[1];
           _.each(this.jsonResultLayer.layersDefinition, function (layerDefinition) {
              layerDefinition.serverUrl = layerDefinition.serverUrl.replaceAll("A5F38D", color);
           });
           var legends = [];
           _.each(this.jsonResultLayer.options.legend, function (legend) {
              legends.push(legend.replaceAll("A5F38D", color));
           });
           this.jsonResultLayer.options.legend = legends;
        }
        var couche = new WMS(
            this.jsonResultLayer.title,
            this.jsonResultLayer.layersDefinition,
            this.jsonResultLayer.options
        );
        var childs;
        if (!_.isEmpty(this.mapContentManager.uiLayersTree.selectedItem)) {
            childs = this.mapContentManager.mapContent.getItemByIndex(this.mapContentManager.uiLayersTree.selectedItem).items;
            if (childs && childs.length > 0) {
                this.mapContentManager.mapContent.insertItemBefore(couche, this.mapContentManager.mapContent.getItemByIndex(childs[0].index));
            } else {
                if (this.mapContentManager.mapContent.getItemByIndex(this.mapContentManager.uiLayersTree.selectedItem) instanceof Group) {
                   this.mapContentManager.mapContent.addItem(couche, this.mapContentManager.mapContent.getItemByIndex(this.mapContentManager.uiLayersTree.selectedItem));
                } else {
                   this.mapContentManager.mapContent.insertItemBefore(couche, this.mapContentManager.mapContent.getItemByIndex(this.mapContentManager.uiLayersTree.selectedItem));
                }
            }
            this.mapContentManager.uiLayersTree.selectedItem = '';
        } else if (this.mapContentManager.mapContent.getItemByIndex('0') !== undefined && this.mapContentManager.mapContent.getItemByIndex('0') !== null) {
            this.mapContentManager.mapContent.insertItemBefore(couche, this.mapContentManager.mapContent.getItemByIndex('0'));
        } else {
            this.mapContentManager.mapContent.addItem(couche);
        }
    },
    toJSON: function (datas, requestParams) {
        var layersDefinition = [];
        var legends = [];
        var resultLayerDatas = [];
        var that = this;
        for (var i = 0, ilen = datas.length; i < ilen; i++) {
            var data = datas[i];
            for (var j = 0, jlen = this.olLayersResult.length; j < jlen; j++) {
                if (data.layerId === this.olLayersResult[j].get('descartesLayerId')) {
                   this.olLayersResult[j].set('descartesDatasResult', data);
                   break;
                }
            }
        }
        _.each(this.olLayersResult, function (olLayerResult) {
             if (olLayerResult.getProperties().descartesDatasResult && olLayerResult.getProperties().descartesDatasResult.layerDatas && olLayerResult.getProperties().descartesDatasResult.layerDatas.length > 0) {
                 layersDefinition.push({
                     layerName: olLayerResult.getProperties().layerName,
                     serverUrl: olLayerResult.getSource().getUrl()
                 });
                 legends.push(that.getWmsLegendUrl(olLayerResult.getSource().getUrl(), olLayerResult.getProperties().layerName));
                 resultLayerDatas.push(olLayerResult.getProperties().descartesDatasResult);
             }
        });
        var requestParamsInfosJson = requestParams.infosJson;
        _.each(requestParamsInfosJson, function (requestParamsInfoJson) {
                   delete requestParamsInfoJson.defaultDataProjection;
                   delete requestParamsInfoJson.featureGeometryName;
                   delete requestParamsInfoJson.featureNameSpace;
                   delete requestParamsInfoJson.mapProjection;
                   delete requestParamsInfoJson.serviceType;
                   delete requestParamsInfoJson.version;
        });
        var json = {};
        json.title = "Couche résultat";
        json.type = LayerConstants.TYPE_WMS;
        json.layersDefinition = layersDefinition;
        json.options = {
			maxScale: null,
			minScale: null,
			alwaysVisible: false,
			visible: true,
            queryable: false,
			activeToQuery: false,
			sheetable: true,
			opacity: 100,
			opacityMax: 100,
			addedByUser: true,
			isResultLayer: true,
			resultLayerDatas: resultLayerDatas,
			resultLayerRequestParamsInfosJson: requestParamsInfosJson,
			legend: legends,
			format: "image/png"
		};
        return json;
    },
    getWmsLegendUrl: function (serverUrl, layerName) {
       var url = new URL(serverUrl);
       url.searchParams.append('SERVICE', 'WMS');
       url.searchParams.append('VERSION', '1.1.1');
       url.searchParams.append('REQUEST', 'GetLegendGraphic');
       url.searchParams.append('FORMAT', 'image/png');
       url.searchParams.append('LAYER', layerName);
       url.searchParams.append('LEGEND_OPTIONS', 'forceLabels:on');
       return url.toString();
    }
};

module.exports = Object;
