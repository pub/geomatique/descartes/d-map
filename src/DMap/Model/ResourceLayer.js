var Utils = require('../Utils/DescartesUtils');
var _ = require('lodash');

var ResourceLayerConstants = require('./ResourceLayerConstants');
var LayerConstants = require('./LayerConstants');

/**
 * Class: Descartes.ResourceLayer
 * Objet "métier" correspondant à une couche OWS.
 *
 * :
 * La couche OWS est éventuellement associée à des couches OWS de substitution :
 * - une couche "image" de substitution étant la couche WMS prise en compte pour les services d'exportation PNG et PDF.
 * - une couche "veteur" de substitution étant la couche WFS prise en compte pour les services d'interrogation utilisant l'opération WFS/GetFeature.
 */
var ResourceLayer = Utils.Class({
    /**
     * Propriete: type
     * Type de la couche.
     *
     * :
     * La valeur doit être une des suivantes :
     * - <Descartes.Layer.TYPE_WMS>
     * - <Descartes.Layer.TYPE_WMSC>
     * - <Descartes.Layer.TYPE_TMS>
     * - <Descartes.Layer.TYPE_WMTS>
     * - <Descartes.Layer.TYPE_WFS>
     * - <Descartes.Layer.TYPE_KML>
     * - <Descartes.Layer.TYPE_GeoJSON>
     * - <Descartes.Layer.TYPE_GEOPORTAIL>
     * - <Descartes.Layer.EditionLayer.TYPE_WFS>
     * - <Descartes.Layer.EditionLayer.TYPE_KML>
     * - <Descartes.Layer.EditionLayer.TYPE_GeoJSON>
     */
    type: null,

    /**
     * Propriete: serverUrl
     * {String} URL du serveur OWS.
     */
    serverUrl: null,

    /**
     * Propriete: serverVersion
     * {String} Version du serveur OWS.
     */
    serverVersion: null,

    serverType: null,

    /**
     * Propriete: layerName
     * {String} Nom de la couche ou de l'objet OWS.
     */
    layerName: null,

    /**
     * Propriete: layerStyles
     * {String} Style à appliquer (GetMap WMS).
     */
    layerStyles: null,

    /**
     * Propriete: imageServerUrl
     * {String} URL du serveur WMS associé.
     */
    imageServerUrl: null,

    /**
     * Propriete: imageServerVersion
     * {String} Version du serveur WMS associé.
     */
    imageServerVersion: null,

    imageServerType: null,

    /**
     * Propriete: imageLayerName
     * {String} Nom de la couche WMS associée.
     */
    imageLayerName: null,

    /**
     * Propriete: imageLayerStyles
     * {String} Style à appliquer (GetMap WMS).
     */
    imageLayerStyles: null,

    /**
     * Propriete: featureServerUrl
     * {String} URL du serveur WFS associé.
     */
    featureServerUrl: null,

    /**
     * Propriete: featureServerVersion
     * {String} Version du serveur WFS associé.
     */
    featureServerVersion: null,

    /**
     * Propriete: featureName
     * {String} Nom de l'objet WFS associé.
     */
    featureName: null,

    /**
     * Propriete: featurePrefix
     * {String} Prefix de l'objet WFS associé
     */
    featurePrefix: null,

    /**
     * Propriete: featureNameSpace
     * {String} Espace de nommage des features WFS.
     *
     * Cette propriété n'est utile que pour les couches de type <Descartes.Layer.TYPE_WFS> et la couche 'vecteur' de substitution
     */
    featureNameSpace: null,

    /**
     * Propriete: featureGeometryName
     * {String} Nom de l'attribut géométrique.
     *
     * :
     * Cette propriété n'est utile que pour les couches de type <Descartes.Layer.TYPE_WFS> et la couche 'vecteur' de substitution
     */
    featureGeometryName: null,

    /**
     * Propriete: internalProjection
     * {String} Code de la projection de la couche.
     *
     * :
     * Cette propriété doit obligatoirement être renseignée pour les couches de type
     * <Descartes.Layer.EditionLayer.TYPE_WFS>, <Descartes.Layer.EditionLayer.TYPE_GeoJSON> et <Descartes.Layer.TYPE_GeoJSON>
     * (par défaut la projection 'EPSG:4326' est utilisée).
     *
     * Cette propriété n'est utile que si elle est différente de la projection de la carte, et uniquement dans les cas suivants :
     * - La couche est de type <Descartes.Layer.TYPE_WFS>. Les features peuvent donc être aisément reprojetés à la volée.
     * - La carte est de type <Descartes.Map.GeoPortailMap> et la couche est de type <Descartes.Layer.TYPE_WMS> ou <Descartes.Layer.TYPE_WMSC>.
     * Une 'reprojection à la volée' est possible si la projection est EPSG:4326.
     */
    internalProjection: null,

    /**
     * Propriete: displayProjection
     * {String} Code de la projection d'affichage la couche.
     *
     * :
     * Cette propriété doit obligatoirement être renseignée pour les couches de type
     * <Descartes.Layer.TYPE_KML>, <Descartes.Layer.EditionLayer.TYPE_KML>, <Descartes.Layer.TYPE_GeoJSON> et
     * Descartes.Layer.EditionLayer.TYPE_GeoJSON> (par défaut la projection 'EPSG:4326' est utilisée).
     *
     */
    displayProjection: null,
    /**
     * Propriete: displayMaxFeatures
     * {String} Nombre max de feature de la couche à afficher.
     *
     * :
     * Cette propriété doit obligatoirement être renseignée pour les couches de type
     * <Descartes.Layer.TYPE_WFS>, <Descartes.Layer.EditionLayer.TYPE_WFS>.
     *
     */
    displayMaxFeatures: null,

    featureReverseAxisOrientation: false, //pour wfs PB 4326 en mode GET

    /**
     * Mode de chargement des couches de type vector.
     * Par défaut GET
     */
    featureLoaderMode: null,

    featureInternalProjection: null,

    crossOrigin: null,

    useBboxSrsProjection: false,

    /**
     * Propriete: displayFeatures
     * {Array} Liste d'objets à afficher.
     *
     * :
     * Cette propriété doit être renseignée pour les couches de type
     * <Descartes.Layer.TYPE_GenericVector>, <Descartes.Layer.EditionLayer.TYPE_GenericVector>.
     *
     */
    displayFeatures: null,

    vectorTileStyle: null,

    vectorTileStyleName: null,

    vectorTileStyleUrl: null,

    vectorTileFormat: null,

    /**
     * Constructeur: Descartes.ResourceLayer
     * Constructeur d'instances
     *
     * Paramètres:
     * type - Type de la couche.
     * params - {Object} Objet JSON contenant les paramètres de la couche.
     *
     * :
     * Les propriétés de l'objet JSON contenant les paramêtres sont les suivantes :
     *
     * :
     * serverUrl - {String} URL du serveur OWS. _Obligatoire_.
     * serverVersion - {String} Version du serveur OWS. _Facultatif
     * layerName - {String} Nom de la couche ou de l'objet OWS. _Obligatoire_.
     * layerStyles - {String} Style à appliquer. _Facultatif
     * imageServerUrl - {String} URL du serveur WMS associé. _Facultatif mais requiert la propriété suivante_.
     * imageServerVersion - {String} Version du serveur WMS associé. _Facultatif
     * imageLayerName - {String} Nom de la couche WMS associée. _Facultatif mais requiert la propriété précédente_.
     * imageLayerStyles - {String} Style à appliquer. _Facultatif
     * featureServerUrl - {String} URL du serveur WFS associé. _Facultatif mais requiert la propriété suivante_.
     * featureServerVersion - {String} Version du serveur WFS associé. _Facultatif
     * featureName - {String} Nom de l'objet WFS associé. _Facultatif mais requiert la propriété précédente_.
     * featureNameSpace - {String} Espace de nommage des features WFS. _Facultatif_.
     * featureGeometryName - {String} Nom de l'attribut géométrique. _Facultatif_.
     * internalProjection - {String} Code de la projection de la couche. _Facultatif_.
     * displayProjection - {String} Code de la projection d'affichage la couche. _Facultatif_.
     */
    initialize: function (type, params) {

        this.type = type;
        if (params !== undefined) {
            _.extend(this, params);
        }
        if (_.isEmpty(this.featureGeometryName)) {
            this.featureGeometryName = ResourceLayerConstants.DEFAULT_FEATURE_GEOMETRY_NAME;
        }
    },

    /**
     * Methode: getImageServerParams
     * Fournit les paramètres de la couche WMS à prendre en compte pour les services d'exportation PNG et PDF.
     *
     * :
     * Si la couche est de type <Descartes.Layer.TYPE_WMS>, les paramètres sont ceux de la couche OWS
     *
     * :
     * Dans le cas contraire, les paramètres sont ceux de la couche 'image' de substitution ou sont nuls en abscence d'une telle couche.
     *
     * Retour:
     * {Object} Objet JSON contenant les paramètres sous la forme {serverUrl: <URL du serveur>, layerName: <Nom de la couche>}.
     */
    getImageServerParams: function () {
        var imageServerParams = null;
        if (this.type === LayerConstants.TYPE_WMS) {
            if (this.serverUrl !== null && this.layerName !== null) {
                imageServerParams = {serverUrl: this.serverUrl, layerName: this.layerName};
                if (this.serverVersion !== null) {
                    imageServerParams.serverVersion = this.serverVersion;
                }

                if (this.layerStyles !== null) {
                    imageServerParams.layerStyles = this.layerStyles;
                }

                if (this.crossOrigin !== null) {
                    imageServerParams.crossOrigin = this.crossOrigin;
                }
            }
        } else {
            if (this.imageServerUrl !== null && this.imageLayerName !== null) {
                imageServerParams = {serverUrl: this.imageServerUrl, layerName: this.imageLayerName};
                if (this.imageServerVersion !== null) {
                    imageServerParams.serverVersion = this.imageServerVersion;
                }

                if (this.layerStyles !== null) {
                    imageServerParams.layerStyles = this.imageLayerStyles;
                }

                if (this.crossOrigin !== null) {
                    imageServerParams.crossOrigin = this.crossOrigin;
                }
            }
        }

        if (this.internalProjection !== null && imageServerParams !== null) {
            imageServerParams.internalProjection = this.internalProjection;
        }

        return imageServerParams;
    },

    /**
     * Methode: getFeatureServerParams
     * Fournit les paramètres de la couche WFS à prendre en compte pour les services d'interrogation utilisant l'opération WFS/GetFeature.
     *
     * :
     * Si la couche est de type <Descartes.Layer.TYPE_WFS>, les paramètres sont ceux de la couche OWS
     *
     * :
     * Dans le cas contraire, les paramètres sont ceux de la couche 'vecteur' de substitution ou sont nuls en abscence d'une telle couche.
     *
     * Retour:
     * {Object} Objet JSON contenant les paramètres sous la forme {serverUrl: <URL du serveur>, featureName: <Nom de l'objet>}.
     */
    getFeatureServerParams: function () {
        var featureServerParams = null;

        if (this.type === LayerConstants.TYPE_WFS) {
            if (this.serverUrl !== null && this.layerName !== null) {
                featureServerParams = {serverUrl: this.serverUrl, featureName: this.layerName, featureGeometryName: this.featureGeometryName};
                if (this.serverVersion !== null) {
                    featureServerParams.serverVersion = this.serverVersion;
                }
            }
        } else {
            if (this.featureServerUrl !== null && this.featureName !== null) {
                featureServerParams = {
                    serverUrl: this.featureServerUrl,
                    featureName: this.featureName,
                    featureGeometryName: this.featureGeometryName
                };
                if (this.featureServerVersion !== null) {
                    featureServerParams.serverVersion = this.featureServerVersion;
                }
            }
        }
        return featureServerParams;
    },

    /**
     * Methode: serialize
     * Fournit une représentation de la couche OWS sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = {
            serverUrl: this.serverUrl,
            serverVersion: this.serverVersion,
            layerName: this.layerName,
            layerStyles: this.layerStyles,
            imageServerUrl: this.imageServerUrl,
            imageServerVersion: this.imageServerVersion,
            imageLayerName: this.imageLayerName,
            imageLayerStyles: this.imageLayerStyles,
            featureServerUrl: this.featureServerUrl,
            featureServerVersion: this.featureServerVersion,
            featureName: this.featureName,
            featurePrefix: this.featurePrefix,
            featureNameSpace: this.featureNameSpace,
            featureGeometryName: this.featureGeometryName,
            featureLoaderMode: this.featureLoaderMode,
            featureReverseAxisOrientation: this.featureReverseAxisOrientation,
            featureInternalProjection: this.featureInternalProjection,
            crossOrigin: this.crossOrigin,
            internalProjection: this.internalProjection,
            displayProjection: this.displayProjection,
            useBboxSrsProjection: this.useBboxSrsProjection,
            displayMaxFeatures: this.displayMaxFeatures,
            displayFeatures: this.displayFeatures,
            loaderFunction: this.loaderFunction,
            vectorTileStyle: this.vectorTileStyle,
            vectorTileStyleName: this.vectorTileStyleName,
            vectorTileStyleUrl: this.vectorTileStyleUrl
        };
        return json;
    },

    CLASS_NAME: 'Descartes.ResourceLayer'
});

module.exports = ResourceLayer;
