var Utils = require('../Utils/DescartesUtils');

/**
 * Class: Descartes.UrlParam
 * Classe "technique" stockant un paramètre d'URL d'une requête HTTP-GET.
 */
var Class = Utils.Class({
    /**
     * Propriete: key
     * {String} Clé du paramètre
     */
    key: null,

    /**
     * Propriete: value
     * {String} Valeur du paramètre
     */
    value: null,

    /**
     * Constructeur: Descartes.UrlParam
     * Constructeur d'instances
     *
     * Paramètres:
     * paramKey - {String} Clé du paramètre.
     * paramValue - {String} Valeur du paramètre.
     */
    initialize: function (paramKey, paramValue) {
        this.key = paramKey;
        this.value = paramValue;
    },
    /**
     * Methode: getQueryString
     * Construit le fragment de l'URL de la requête correspondant au paramètre.
     *
     * Retour:
     * {String} URL de la requête.
     */
    getQueryString: function () {
        var kvp = '';
        if (this.key !== null) {
            kvp = this.key + '=';
            if (this.value !== null) {
                kvp += this.value;
            }
        }
        return kvp;
    },
    CLASS_NAME: 'Descartes.UrlParam'
});

module.exports = Class;
