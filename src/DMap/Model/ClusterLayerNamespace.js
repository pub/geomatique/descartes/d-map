var _ = require('lodash');

var ClusterLayer = require('./ClusterLayer');

var GeoJSON = require('./ClusterLayer/GeoJSON');
var WFS = require('./ClusterLayer/WFS');
var KML = require('./ClusterLayer/KML');

var constants = require('./ClusterLayerConstants');

var namespace = {
    GeoJSON: GeoJSON,
    KML: KML,
    WFS: WFS
};

_.extend(namespace, constants);
_.extend(ClusterLayer, namespace);

module.exports = ClusterLayer;
