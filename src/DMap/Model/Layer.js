/* global Descartes */

var _ = require('lodash');
var ol = require('openlayers');
var log = require('loglevel');

var Utils = require('../Utils/DescartesUtils');
var ResourceLayer = require('./ResourceLayer');
var LayerConstants = require('./LayerConstants');

var defaultSymbolizers = require('../Symbolizers');

/**
 * Class: Descartes.Layer
 * Objet "métier" correspondant à une couche pour le contenu de la carte.
 *
 * :
 * Une couche peut comporter plusieurs couches OpenLayers. Ces dernières sont alors manipulées globalement.
 *
 * :
 * C'est en fait un agrégat de couches OpenLayers.
 *
 * :
 * Par exemple, rendre la couche invisible masque simultanément toutes les couches OpenLayers qu'elle comporte.
 *
 * Classes dérivées:
 * - <Descartes.Layer.WMS>
 * - <Descartes.Layer.Tiled>
 * - <Descartes.Layer.Vector>
 */
var Layer = Utils.Class({
    /**
     * Propriete: OL_layers
     * {Array(ol.layer)} Couches OpenLayers.
     */
    OL_layers: [],
    /**
     * Propriete: resourceLayers
     * {Array(<Descartes.ResourceLayer>)} Définition des couches OWS constituant la couche.
     */
    resourceLayers: [],
    /**
     * Propriete: type
     * Type de la couche.
     *
     * :
     * La valeur doit être une des suivantes :
     * - <Descartes.Layer.TYPE_WMS>
     * - <Descartes.Layer.TYPE_WMSC>
     * - <Descartes.Layer.TYPE_TMS>
     * - <Descartes.Layer.TYPE_WMTS>
     * - <Descartes.Layer.TYPE_WFS>
     * - <Descartes.Layer.TYPE_KML>
     * - <Descartes.Layer.TYPE_GeoJSON>
     * - <Descartes.Layer.TYPE_GenericVector>
     * - <Descartes.Layer.TYPE_OSM>
     */
    type: null,
    /**
     * Propriete: title
     * {String} Intitulé de la couche.
     */
    title: null,
    /**
     * Propriete: alwaysVisible
     * {Boolean} Indique si la couche est toujours visible.
     */
    alwaysVisible: false,
    /**
     * Propriete: visible
     * {Boolean} Indique si la couche est visible.
     */
    visible: true,
    /**
     * Propriete: queryable
     * {Boolean} Indique si la couche est potientiellement interrogeable.
     */
    queryable: true,
    /**
     * Propriete: activeToQuery
     * {Boolean} Indique si la couche est active pour les fonctionnalités d'interrogation.
     */
    activeToQuery: true,
    /**
     * Propriete: sheetable
     * {Boolean} Indique si les propriétes de l'ensemble des objets de la couche peuvent être affichées sous forme de tableau.
     */
    sheetable: false,
    /**
     * Propriete: opacity
     * {Integer} Opacité de la couche (entre 0 et l'opacité maximale).
     */
    opacity: 100,
    /**
     * Propriete: opacityMax
     * {Integer} Opacité maximale de la couche (entre 0 et 100).
     */
    opacityMax: 100,
    /**
     * Propriete: legend
     * {Array(String)} Adresses d'accès aux légendes.
     */
    legend: null,
    /**
     * Propriete: metadataURL
     * {String} Adresse d'accès à des informations complémentaires.
     */
    metadataURL: null,
    /**
     * Propriete: attribution
     * {String} Texte de copyright de la couche (éventuellement lien vers une image).
     */
    attribution: null,
    /**
     * Propriete: format
     * {String} Type MIME pour l'affichage des couches OpenLayers associées de type "image".
     */
    format: 'image/png',
    /**
     * Propriete: displayOrder
     * {Integer} Ordre de superposition de la couche dans la carte.
     */
    displayOrder: null,
    /**
     * Propriete: index
     * {String} Index hiérarchique du groupe dans l'arborescence.
     *
     * :
     * Automatiquement calculé lors de l'ajout du groupe dans la classe <Descartes.MapContent>.
     */
    index: '',
    /**
     * Propriete: addedByUser
     * {Boolean} Indique si la couche a été ajoutée par l'utilisateur ou fournie initialement par l'application.
     */
    addedByUser: false,
    /**
     * Propriete: id
     * {String} Identifiant de la couche
     */
    id: null,
    /*
     * Private
     * {Boolean} Indique qu'au moins un layer est en cours de chargement
     */
    loading: true,

    /**
     * Propriete: useProxy
     * {Boolean} Utilisation ou non d'un proxy pour récupérer les données.
     */
    useProxy: false,

    /**
     * Utilisation ou non d'un proxy pour récupérer les données des layers de ressources.
     */
    useProxyForAssociatedLayer: true,

    /**
     * Propriete: attributes
     * {Objet} Définit les alias des attributs des objets qui seront affichés lors des résultats des recherches.
     *
     * (start code)
     * attributes: {
     *   attributesAlias: [{
     *     fieldName: 'name',
     *     label: 'Nom'
     *   }]
     * }
     * (end)
     */
    attributes: null,

    /**
     * Propriete: hide
     * {Boolean} Indiques si la couche est cachée ou non dans l'arbre des couches.
     */
    hide: false,

    /**
     * Propriete: geometryType
     * {String} Indique le type de géométrie de la couche.
     *
     *  La valeur doit être une des suivantes :
     * - <Descartes.Layer.POINT_GEOMETRY>
     * - <Descartes.Layer.LINE_GEOMETRY>
     * - <Descartes.Layer.POLYGON_GEOMETRY>
     * - <Descartes.Layer.MULTI_POINT_GEOMETRY>
     * - <Descartes.Layer.MULTI_LINE_GEOMETRY>
     * - <Descartes.Layer.MULTI_POLYGON_GEOMETRY>
     */
    geometryType: null,

    units: 'm',

    _loaderWithRemoveFeatures: false,

    _sourceImageListenersMethod: null,
    _sourceTileListenersMethod: null,
    _sourceFeatureListenersMethod: null,
    /**
     * Constructeur: Descartes.Layer
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches OWS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * alwaysVisible -  {Boolean} Indique si la couche est toujours visible.
     * visible -  {Boolean} Indique si la couche est visible.
     * queryable - {Boolean} Indique si la couche est potientiellement interrogeable.
     * activeToQuery - {Boolean} Indique si la couche est active pour les fonctionnalités d'interrogation.
     * sheetable - {Boolean} Indique si les propriétes de l'ensemble des objets de la couche peuvent être affichées sous forme de tableau.
     * opacity - {Integer} Opacité initiale de la couche.
     * opacityMax - {Integer} Opacité maximale de la couche.
     * legend - {Array(URL)} Adresses d'accès aux légendes.
     * metadataURL - {URL} Adresse d'accés à des informations complémentaires.
     * attribution - {String} Texte de copyright de la couche (éventuellement lien vers une image).
     * format - {String} Type MIME pour l'affichage des couches OpenLayers associées.
     * displayOrder - {Integer} Ordre de superposition de la couche dans la carte.
     * id - {String} Identifiant de la couche.
     */
    initialize: function (title, layersDefinition, options) {
        this.title = title;
        if (!_.isNil(options)) {
            _.extend(this, options);
            if (!_.isNil(this.metadataURL) && this.metadataURL === '-') {
                this.metadataURL = null;
            }
            this.visible = this.alwaysVisible ? true : this.visible;
            this.activeToQuery = this.queryable && this.activeToQuery;
            this.opacity = (this.opacity === 0) ? 1 : Math.min(this.opacity, this.opacityMax);
        }
        //generation d'un identifiant aléatoire si non défini
        if (_.isNil(this.id)) {
            this.id = LayerConstants.TEMPLATE_ID + Math.random().toString(36).substring(7);
        }
        //calcul max/min resolution si nécéssaire
        if (this.minScale && _.isNil(this.maxResolution)) {
            this.maxResolution = Utils.getResolutionForScale(this.minScale, this.units);
        }
        if (this.maxScale && _.isNil(this.minResolution)) {
            this.minResolution = Utils.getResolutionForScale(this.maxScale, this.units);
        }
        this._createOL_layers(layersDefinition);
    },
    getUrl: function (url, forAssociatedLayer) {
        var useProxy = this.useProxy;
        if (forAssociatedLayer) {
            useProxy = this.useProxyForAssociatedLayer;
        }

        if (useProxy) {
            return Utils.makeSameOrigin(url, Descartes.PROXY_SERVER);
        }
        return url;
    },
    /**
     * Private
     */
    _createOL_layers: function (layersDefinition) {
        this.OL_layers = [];
        this.resourceLayers = [];
        this._sourceImageListenersMethod = [];
        this._sourceTileListenersMethod = [];
        this._sourceFeatureListenersMethod = [];

        if (!_.isNil(layersDefinition)) {
            if (!_.isArray(layersDefinition)) {
                layersDefinition = [layersDefinition];
            }

            _.each(layersDefinition, function (layerDefinition) {
                this.resourceLayers.push(new ResourceLayer(this.type, layerDefinition));
                var olLayer = this.createOL_layer(layerDefinition);
                this.checkLayerLoading(olLayer);
                olLayer.isBaseLayer = false;
                olLayer.attribution = this.attribution;
                this.OL_layers.push(olLayer);
            }.bind(this));
        }
    },
    /*
     * Private
     */
    _createOLFeatureResourceLayer: function () {
        var lastResourceLayer = this.resourceLayers[this.resourceLayers.length - 1];
        if (!_.isNil(lastResourceLayer) && !_.isNil(lastResourceLayer.featureServerUrl)) {
            if (_.isNil(this.symbolizers)) {
                this.symbolizers = _.extend({}, defaultSymbolizers.Descartes_Symbolizers_WFS);
            }

            var olStyles = defaultSymbolizers.getOlStyle(this.symbolizers);

            var version = '1.1.0';
            if (!_.isNil(lastResourceLayer.featureServerVersion)) {
                version = lastResourceLayer.featureServerVersion;
            }
            var gmlFormat = new ol.format.GML3();
            if (version === '1.0.0') {
                gmlFormat = new ol.format.GML2();
            }
            var format = new ol.format.WFS({
                featureNS: lastResourceLayer.featureNameSpace,
                gmlFormat: gmlFormat
            });
            format.wfsVersion = version;

            if (!_.isNil(lastResourceLayer.featureInternalProjection)) {
                format.defaultDataProjection = lastResourceLayer.featureInternalProjection;
            }

            //var url = this.getUrl(Utils.adaptationUrl(lastResourceLayer.featureServerUrl), true);
            var url = Utils.adaptationUrl(lastResourceLayer.featureServerUrl);
            var loader = null;
            if (_.isNil(lastResourceLayer.featureLoaderMode) || lastResourceLayer.featureLoaderMode === LayerConstants.GET_REQUEST_LOADER) {

                var urlFunc = function (extent, resolution, projection) {
                    var finalBbox = extent;
                    var finalSrsName = projection.getCode();
                    if (!_.isNil(lastResourceLayer.featureInternalProjection)) {
                        finalBbox = ol.proj.transformExtent(extent, projection, lastResourceLayer.featureInternalProjection);
                        finalSrsName = lastResourceLayer.featureInternalProjection;
                    }

                    if (lastResourceLayer.featureReverseAxisOrientation) {
                        finalBbox = [finalBbox[1], finalBbox[0], finalBbox[3], finalBbox[2]];
                    }

                    finalBbox = finalBbox.join(',');

                    if (lastResourceLayer.useBboxSrsProjection) {
                        finalBbox += ',' + finalSrsName;
                    }

                    var typeName = lastResourceLayer.featureName;
                    if (lastResourceLayer.featurePrefix) {
                        typeName = lastResourceLayer.featurePrefix + ':' + lastResourceLayer.featureName;
                    }

                    var labelTypeName = 'TYPENAME=';
                    if (version === '2.0.0') {
                        labelTypeName = 'TYPENAMES=';
                    }

                    //var finalUrl = Utils.adaptationUrl(url);
                    var finalUrl = url;
                    finalUrl += 'SERVICE=WFS';
                    finalUrl += '&VERSION=' + version;
                    finalUrl += '&REQUEST=GetFeature';
                    finalUrl += '&' + labelTypeName + typeName;
                    finalUrl += '&SRSNAME=' + finalSrsName;
                    finalUrl += '&BBOX=' + finalBbox;

                    if (lastResourceLayer.displayMaxFeatures) {
                        finalUrl += '&MAXFEATURES=' + lastResourceLayer.displayMaxFeatures;
                    }
                    if (version === '2.0.0') {//force gml version cause ol 4 not support gml32
                        finalUrl += '&OUTPUTFORMAT=gml3';
                    }
                    finalUrl = this.getUrl(finalUrl, true);

                    return finalUrl;
                }.bind(this);
                loader = this.getRequestGetLoader(urlFunc, format, LayerConstants.TYPE_WFS, this._loaderWithRemoveFeatures);
            } else {
                var geometryName = Utils.DEFAULT_FEATURE_GEOMETRY_NAME;
                if (!_.isNil(lastResourceLayer.featureGeometryName)) {
                    geometryName = lastResourceLayer.featureGeometryName;
                }

                var prefix = Utils.DEFAULT_FEATURE_PREFIX;
                if (!_.isNil(lastResourceLayer.featurePrefix)) {
                    prefix = lastResourceLayer.featurePrefix;
                }
                var maxFeatures = null;
                if (!_.isNil(lastResourceLayer.displayMaxFeatures)) {
                    maxFeatures = lastResourceLayer.displayMaxFeatures;
                }
                url = this.getUrl(url, true);
                loader = this.getRequestPostLoader(
                        url,
                        format,
                        lastResourceLayer.featureName,
                        geometryName,
                        prefix,
                        maxFeatures,
                        this._loaderWithRemoveFeatures);
            }

            var params = {
                title: this.title + '_feature',
                layerName: lastResourceLayer.layerName,
                visible: false,
                style: function (feature, resolution) {
                    var featureStyleFunction = feature.getStyleFunction();
                    if (featureStyleFunction) {
                        return featureStyleFunction.call(feature, resolution);
                    } else {
                        var style = olStyles[feature.getGeometry().getType()];
                        return style;
                    }
                }
            };

            params.source = new ol.source.Vector({
                format: format,
                loader: loader,
                strategy: ol.loadingstrategy.bbox
            });

            var vector = new ol.layer.Vector(params);

            this.checkLayerLoading(vector);
            this.OL_layers.push(vector);

            return vector;
        }
        return null;
    },
    getRequestGetLoader: function (url, format, type, withRemoveFeatures) {
        return function (extent, resolution, projection) {
            this.dispatchEvent('featureloadstart');

            var finalUrl = url;
            if (_.isFunction(url)) {
                finalUrl = url(extent, resolution, projection);
            }

            if (type === LayerConstants.TYPE_KML || type === LayerConstants.TYPE_GeoJSON) {
                //if (finalUrl.indexOf('?') !== -1) {
                //    finalUrl += '&ver=' + new Date().getTime();
                //} else {
                finalUrl += '?ver=' + new Date().getTime();
                //}
            }

            var xhr = new XMLHttpRequest();
            xhr.open('GET', finalUrl);
            if (format.getType() === 'arraybuffer') {
                xhr.responseType = 'arraybuffer';
            }

            if (withRemoveFeatures) {
                this.clear();
            }

            xhr.onload = function (event) {
                if (!xhr.status || xhr.status >= 200 && xhr.status < 300) {
                    var type = format.getType();
                    /** @type {Document|Node|Object|string|undefined} */
                    var source;
                    if (type === 'json' ||
                            type === 'text') {
                        if (xhr.responseText.indexOf('html') === -1) {
                            source = xhr.responseText;
                        } else {
                            source = null;
                        }
                    } else if (type === 'xml') {
                        source = xhr.responseXML;
                        if (!source) {
                            if (xhr.responseText.indexOf('html') === -1) {
                                source = ol.xml.parse(xhr.responseText);
                            } else {
                                source = null;
                            }
                        }
                    } else if (type === 'arraybuffer') {
                        source = (xhr.response);
                    }
                    if (source) {
                        var readOpts = {
                            featureProjection: projection
                        };
                        if (!_.isNil(format.defaultDataProjection)) {
                            readOpts.dataProjection = format.defaultDataProjection;
                        }
                        var features = format.readFeatures(source, readOpts);
                        this.addFeatures(features);
                        this.dispatchEvent('featureloadend');
                    } else {
                        this.dispatchEvent('featureloaderror');
                    }
                } else {
                    this.dispatchEvent('featureloaderror');
                }
            }.bind(this);
            xhr.onerror = function () {
                this.dispatchEvent('featureloaderror');
            }.bind(this);

            //send request
            xhr.send();
        };
    },
    getRequestPostLoader: function (url, format, layerName, geometryName, prefix, maxFeatures, withRemoveFeatures) {
        return function (extent, resolution, projection) {
            this.dispatchEvent('featureloadstart');
            var proj = projection.getCode();
            var opts = {
                //srsName: layerDefinition.internalProjection,
                //featureNS: layerDefinition.featureNameSpace,
                featurePrefix: prefix,
                featureTypes: [layerName],
                filter: ol.format.filter.bbox(geometryName, extent, proj)
            };
            if (!_.isNil(maxFeatures)) {
                opts.maxFeatures = maxFeatures;
            }
            var featureRequest = format.writeGetFeature(opts);

            var requestBody = new XMLSerializer().serializeToString(featureRequest);
            requestBody = Utils.adaptFluxWFSGetFeaturePost(requestBody, format.wfsVersion);

            var finalUrl = (typeof url === 'function') ? url(extent, resolution, projection) : url;
            var xhr = new XMLHttpRequest();
            xhr.open('POST', finalUrl);
            xhr.setRequestHeader('Content-Type', 'text/xml');
            if (format.getType() === 'arraybuffer') {
                xhr.responseType = 'arraybuffer';
            }

            if (withRemoveFeatures) {
                this.clear();
            }

            xhr.onload = function () {
                if (!xhr.status || xhr.status >= 200 && xhr.status < 300) {
                    var type = format.getType();
                    /** @type {Document|Node|Object|string|undefined} */
                    var source;
                    if (type === 'json' ||
                            type === 'text') {
                         if (xhr.responseText.indexOf('html') === -1) {
                             source = xhr.responseText;
                         } else {
                             source = null;
                         }
                    } else if (type === 'xml') {
                        source = xhr.responseXML;
                        if (!source) {
                            if (xhr.responseText.indexOf('html') === -1) {
                                source = ol.xml.parse(xhr.responseText);
                            } else {
                                source = null;
                            }
                        }
                    } else if (type === 'arraybuffer') {
                        source = (xhr.response);
                    }
                    if (source) {
                        var readOpts = {
                            featureProjection: projection
                        };
                        if (!_.isNil(format.defaultDataProjection)) {
                            readOpts.dataProjection = format.defaultDataProjection;
                        }
                        var features = format.readFeatures(source, readOpts);
                        this.addFeatures(features);
                        this.dispatchEvent('featureloadend');
                    } else {
                        this.dispatchEvent('featureloaderror');
                    }
                } else {
                    this.dispatchEvent('featureloaderror');
                }
            }.bind(this);
            xhr.onerror = function () {
                this.dispatchEvent('featureloaderror');
            }.bind(this);
            //send request
            xhr.send(requestBody);
        };
    },
    /**
     * Methode checkLayerLoading
     * Lorsque la couche OL passée en paramètre est ajoutée à la carte ou qu'elle est déjà présente dans la carte
     * alors la méthode loadStart est ajouté comme listener de début de chargement de la couche
     * alors la méthode loadEnd est ajouté comme listener de fin de chargement de la couche.
     */
    checkLayerLoading: function (layer, scope, loadStart, loadEnd) {
        if (!scope) {
            scope = this;
        }
        if (!loadStart) {
            loadStart = this._onLoadStart;
        }
        if (!loadEnd) {
            loadEnd = this._onLoadEnd;
        }

        this.registerLayerLoading(layer, scope, loadStart, loadEnd);

    },
    registerLayerLoading: function (layer, scope, loadStart, loadEnd) {
        var source = layer.getSource();
        if (source instanceof ol.source.Image) {
            if (this._sourceImageListenersMethod.indexOf(loadStart.name) === -1) {
            log.debug('Register image layer "%s" listener %s', this.title, loadStart.name);
            source.on('imageloadstart', loadStart.bind(this), scope);
                this._sourceImageListenersMethod.push(loadStart.name);
            }
            if (this._sourceImageListenersMethod.indexOf(loadEnd.name) === -1) {
            source.on('imageloadend', loadEnd.bind(this), scope);
                this._sourceImageListenersMethod.push(loadEnd.name);
            }
            if (this._sourceImageListenersMethod.indexOf(this._onLoadError.name) === -1) {
            source.on('imageloaderror', this._onLoadError.bind(this), this);
                this._sourceImageListenersMethod.push(this._onLoadError.name);
            }
        } else if (source instanceof ol.source.Tile) {
            if (this._sourceTileListenersMethod.indexOf(loadStart.name) === -1) {
            log.debug('Register tile layer "%s" listener %s', this.title, loadStart.name);
            source.on('tileloadstart', loadStart.bind(this), scope);
                this._sourceTileListenersMethod.push(loadStart.name);
            }
            if (this._sourceTileListenersMethod.indexOf(loadEnd.name) === -1) {
            source.on('tileloadend', loadEnd.bind(this), scope);
                this._sourceTileListenersMethod.push(loadEnd.name);
            }
            if (this._sourceTileListenersMethod.indexOf(this._onLoadError.name) === -1) {
            source.on('tileloaderror', this._onLoadError.bind(this), this);
                this._sourceTileListenersMethod.push(this._onLoadError.name);
            }
        } else if (source instanceof ol.source.Vector) {
            if (this._sourceFeatureListenersMethod.indexOf(loadStart.name) === -1) {
            log.debug('Register vector layer "%s" listener %s', this.title, loadStart.name);
            source.on('featureloadstart', loadStart.bind(this), scope);
                this._sourceFeatureListenersMethod.push(loadStart.name);
            }
            if (this._sourceFeatureListenersMethod.indexOf(loadEnd.name) === -1) {
            source.on('featureloadend', loadEnd.bind(this), scope);
                this._sourceFeatureListenersMethod.push(loadEnd.name);
            }
            if (this._sourceFeatureListenersMethod.indexOf(this._onLoadError.name) === -1) {
            source.on('featureloaderror', this._onLoadError.bind(this), this);
                this._sourceFeatureListenersMethod.push(this._onLoadError.name);
            }
        }
    },
    unregisterLayerLoading: function (layer, scope, loadStart, loadEnd) {
        var source = layer.getSource();
        log.debug('Unregister to loading event for layer "%s" ');
        if (source instanceof ol.source.Image) {
            source.un('imageloadstart', loadStart.bind(this), scope);
            source.un('imageloadend', loadEnd.bind(this), scope);
            source.un('imageloaderror', this._onLoadError.bind(this), this);
        } else if (source instanceof ol.source.Tile) {
            source.un('tileloadstart', loadStart.bind(this), scope);
            source.un('tileloadend', loadEnd.bind(this), scope);
            source.un('tileloaderror', this._onLoadError.bind(this), this);
        } else if (source instanceof ol.source.Vector) {
            source.un('featureloadstart', loadStart.bind(this), scope);
            source.un('featureloadend', loadEnd.bind(this), scope);
            source.un('featureloaderror', this._onLoadError.bind(this), this);
        }
    },
    /*
     * Private _onLoadStart
     * Listener appelé lorsque le layer se charge
     */
    _onLoadStart: function () {
        log.debug('Start loading "%s" visibility is %s', this.title, this.isVisible());
        if (this.isVisible()) {
            this.loading = true;
        }
    },
    /*
     * Private
     * Listener appelé lorsque le layer à terminé de ce charger.
     */
    _onLoadEnd: function () {
        log.debug('End loading layer "%s", visibility is %s', this.title, this.isVisible());
        if (this.isVisible() && !_.isString(this.loading)) {
            this.loading = false;
        }
    },
    _onLoadError: function (e) {
        this.loading = 'Impossible de charger la couche';
        if (e.image) {
            log.warn('Error Loading layer "%s" with url %s', this.title, e.image.src_);
        } else {
            log.warn('Error Loading layer "%s"', this.title);
        }

        // il faut indiquer que le chargement est terminé afin que les listeners prennent en compte
        // l'erreur et le changement d'état
        switch (e.type) {
            case 'imageloaderror':
                e.target.dispatchEvent('imageloadend');
                break;
            case 'tileloaderror':
                e.target.dispatchEvent('tileloadend');
                break;
            case 'featureloaderror':
                e.target.dispatchEvent('featureloadend');
                break;
        }
    },
    /**
     * Methode: createOL_layer
     * Crée une couche OpenLayers participant à l'agrégat représentant la couche.
     *
     * :
     * Doit être surchargée par les classes dérivées pour fournir la couche selon le type OpenLayers adéquat.
     *
     * Retour:
     * {ol.layer} La couche OpenLayers créée.
     */
    createOL_layer: function () {
        return null;
    },
    /**
     * Methode: getFeatureOL_layers
     * Retourne les couches vecteurs de cette couche.
     * S'il n'y a pas de couche vecteur et que createIfNotExist est vrai alors
     * On essaye de créer les couche feature associé.
     *
     * Retour:
     * {ol.layer.Vector} couche vecteur
     */
    getFeatureOL_layers: function (createIfNotExist) {
        var result = [];
        for (var i = 0, len = this.OL_layers.length; i < len; i++) {
            var aLayer = this.OL_layers[i];
            if (aLayer instanceof ol.layer.Vector) {
                result.push(aLayer);
            }
        }

        if (result.length === 0 && createIfNotExist === true) {
            var featureLayer = this._createOLFeatureResourceLayer();
            if (featureLayer !== null) {
                result.push(featureLayer);
            }
        }

        return result;
    },
    getOLLayerByCriteria: function (criteria) {
        var result = [];

        for (var i = 0; i < this.OL_layers.length; i++) {
            var olLayer = this.OL_layers[i];
            if (criteria !== null && criteria(olLayer)) {
                result.push(olLayer);
            }
        }

        return result;
    },
    /**
     * Methode: getAttributesAlias
     * Récupération des alias des attributs de la couche.
     */
    getAttributesAlias: function () {
        var attributes = null;
        if (this.attributes && this.attributes.attributesAlias) {
            attributes = {};
            for (var i = 0, ilen = this.attributes.attributesAlias.length; i < ilen; i++) {
                var fieldName = this.attributes.attributesAlias[i].fieldName;
                //replace special chars to match backend responses
                fieldName = fieldName.replace(/[-:_\.]/g, " ");
                attributes[fieldName] = this.attributes.attributesAlias[i].label;
            }
        }
        return attributes;
    },
    /**
     * Methode: setVisibility
     * Modifie la visibilité de la couche.
     *
     * Paramêtres:
     * isVisible - {Boolean} Visibilité souhaitée.
     */
    setVisibility: function (isVisible) {
        if (this.visible !== isVisible) {
            this.visible = isVisible;
            _.each(this.OL_layers, function (olLayer) {
                olLayer.setVisible(isVisible);
            });
        }
    },
    /**
     * Methode: isInScalesRange
     * Indique si la couche est potientiellement visible au regard de l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} La couche est potentiellement visible ou non.
     */
    isInScalesRange: function () {
        return this.acceptMaxScaleRange() && this.acceptMinScaleRange();
    },
    /**
     * Methode: acceptMaxScaleRange
     * Indique si l'échelle maximale de la couche est supérieure à l'échelle courante de la carte.
     *
     * :
     * Doit être surchargée par les classes dérivées.
     *
     * Retour:
     * {Boolean} L'échelle maximale de la couche est supérieure à l'échelle courante de la carte.
     */
    acceptMaxScaleRange: function () {
        return true;
    },
    /**
     * Methode: acceptMinScaleRange
     * Indique si l'échelle minimale de la couche est inférieure à l'échelle courante de la carte.
     *
     * :
     * Doit être surchargée par les classes dérivées.
     *
     * Retour:
     * {Boolean} L'échelle minimale de la couche est inférieure à l'échelle courante de la carte.
     */
    acceptMinScaleRange: function () {
        return true;
    },
    /**
     * Methode: canChangeOpacity
     * Indique si l'opacité de la couche peut être modifiée par l'utilisateur.
     *
     * :
     * Faux uniquement si le navigateur est Microsoft Internet 6 et le format d'image est PNG.
     *
     * Retour:
     * {Boolean} L'opacité de la couche peut être modifiée.
     */
    canChangeOpacity: function () {
        return true;
    },
    /**
     * Methode: setOpacity
     * Modifie l'opacité de la couche.
     *
     * Paramêtres:
     * opacity - Nouvelle opacité (entre 0 et l'opacité maximale).
     */
    setOpacity: function (opacity) {
        if (this.opacity !== opacity) {
            this.opacity = opacity;
            for (var i = 0, len = this.OL_layers.length; i < len; i++) {
                this.OL_layers[i].setOpacity(opacity / 100.0);
            }
        }
    },
    /**
     * Methode: isVisible
     * Indique si la couche est visible au regard de l'échelle courante de la carte.
     *
     * Retour:
     * {Boolean} La couche est visible ou non.
     */
    isVisible: function () {
        return (this.visible || this.alwaysVisible) && this.isInScalesRange();
    },
    /**
     * Methode: isQueryable
     * Indique si les fonctionnalités d'interrogation doivent prendre en compte la couche.
     *
     * :
     * Pour cela, la couche doit être :
     *
     * :
     * - visible au regard de l'échelle courante de la carte.
     * - active pour les fonctionnalités d'interrogation
     *
     * Retour:
     * {Boolean} La couche est prise en compte.
     */
    isQueryable: function () {
        return this.isVisible() && this.activeToQuery;
    },
    /**
     * Methode: isCompositeGeometry
     * Retourne true si la géometrie est de type MULTIPOLYGONE, MULTIPOINT OU MULTILIGNE
     *
     */
    isCompositeGeometry: function () {
        switch (this.geometryType) {
            case LayerConstants.POINT_GEOMETRY:
                return false;
            case LayerConstants.LINE_GEOMETRY:
                return false;
            case LayerConstants.POLYGON_GEOMETRY:
                return false;
            case LayerConstants.MULTI_POINT_GEOMETRY:
                return true;
            case LayerConstants.MULTI_LINE_GEOMETRY:
                return true;
            case LayerConstants.MULTI_POLYGON_GEOMETRY:
                return true;
        }
        return false;
    },
    /**
     * Methode: setFromJSON
     * Met à jour les propriétés modifiables de la couche.
     *
     * :
     * Les propriétés modifiables (par un outil de type <Descartes.Action.MapContentManager.ALTER_LAYER_TOOL>) sont :
     *
     * :
     * title - {String} Intitulé de la couche.
     * type - Type de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches OWS constituant la couche.
     * maxScale - {Float} Dénominateur de l'échelle maximale de visibilité de la couche.
     * minScale - {Float} Dénominateur de l'échelle minimale de visibilité de la couche.
     * queryable - {Boolean} Indique si la couche est potientiellement interrogeable.
     * sheetable - {Boolean} Indique si les propriétes de l'ensemble des objets de la couche peuvent être affichées sous forme de tableau.
     * legend - {Array(URL)} Adresses d'accés aux légendes.
     * metadataURL - {URL} Adresse d'accés à des informations complémentaires.
     * attribution - {String} Texte de copyright de la couche (éventuellement lien vers une image).
     * format - {String} Type MIME pour l'affichage des couches OpenLayers associées.
     */
    setFromJSON: function (json) {
        this.title = json.title;
        this.type = json.type;
        _.extend(this, json.options);
        this._createOL_layers(json.layersDefinition);
    },
    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 	title: <Intitulé>,
     * 	type: <Type de couche>,
     * 	layersDefinition: {
     *      layerName: <Nom de la couche ou de l'objet OWS>
     * 	serverUrl: <URL du serveur OWS>
     * 	} [],
     * 	options: {
     *      format: <Type MIME pour l'affichage des couches OpenLayers>,
     *      legend: <Adresses d'accés aux légendes>,
     *      metadataURL: <Adresse d'accés aux informations complémentaires>,
     *      attribution: <Texte de copyright>,
     *      queryable: <Interrogation potentielle>,
     *      sheetable: <Affichage des propriétés sous forme de tableau>,
     *      maxScale: <Dénominateur de l'échelle maximale>,
     *      minScale: <Dénominateur de l'échelle minimale>,
     *      id: <Identifiant de la couche>
     * 	}
     * }
     * (end)
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.type = this.type;
        json.layersDefinition = [{
                layerName: this.OL_layers[0].get('layerName'),
                serverUrl: this.OL_layers[0].getSource().getUrl()
            }];
        json.options = {
            format: this.format,
            legend: (this.legend !== null) ? this.legend[0] : '',
            metadataURL: (this.metadataURL !== null) ? this.metadataURL : '',
            attribution: (this.attribution !== null) ? this.attribution : '',
            queryable: this.queryable,
            sheetable: this.sheetable,
            maxScale: (this.maxScale !== null) ? this.maxScale : '',
            minScale: (this.minScale !== null) ? this.minScale : '',
            id: this.id,
            attributes: this.attributes,
            geometryType: this.geometryType,
            hide: this.hide
        };
        return json;
    },
    /**
     * Methode: serialize
     * Fournit une représentation de la couche sous forme d'objet JSON pour la sauvegarde d'un contexte de consultation.
     *
     * :
     * Doit être surchargée par les classes dérivées.
     *
     * Retour:
     * {Object} Objet JSON.
     */
    serialize: function () {
        var json = {};
        json.itemType = 'Layer';
        json.title = this.title;
        json.type = this.type;
        json.options = {
            format: this.format,
            legend: this.legend,
            metadataURL: this.metadataURL,
            attribution: this.attribution,
            visible: this.visible,
            alwaysVisible: this.alwaysVisible,
            queryable: this.queryable,
            activeToQuery: this.activeToQuery,
            sheetable: this.sheetable,
            opacity: this.opacity,
            opacityMax: this.opacityMax,
            displayOrder: this.displayOrder,
            addedByUser: this.addedByUser,
            id: this.id,
            attributes: this.attributes,
            geometryType: this.geometryType,
            hide: this.hide
        };
        json.definition = [];
        for (var i = 0, len = this.resourceLayers.length; i < len; i++) {
            json.definition.push(this.resourceLayers[i].serialize());
        }
        return json;
    },
    CLASS_NAME: 'Descartes.Layer'
});

module.exports = Layer;
