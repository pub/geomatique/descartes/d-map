/* eslint-disable camelcase */

module.exports = {
    /**
     * Constante: TYPE_WMS
     * Couche de type WMS.
     */
    TYPE_WMS: 0,

    /**
     * Constante: TYPE_WMSC
     * Couche de type WMS-C.
     */
    TYPE_WMSC: 1,

    /**
     * Constante: TYPE_TMS
     * Couche de type TMS.
     */
    TYPE_TMS: 2,

    /**
     * Constante: TYPE_WMTS
     * Couche de type WMTS.
     */
    TYPE_WMTS: 3,

    /**
     * Constante: TYPE_WFS
     * Couche de type WFS.
     */
    TYPE_WFS: 4,

    /**
     * Constante: TYPE_KML
     * Couche de type KML.
     */
    TYPE_KML: 5,

    /**
     * Constante: TYPE_GEOPORTAIL
     * Couche de type GEOPORTAIL.
     */
    TYPE_GEOPORTAIL: 6,

    /**
     * Constante: TYPE_GeoJSON
     * Couche de type GeoJSON.
     */
    TYPE_GeoJSON: 7,

    /**
     * Constante: TYPE_GenericVector
     * Couche de type GeoJSON.
     */
    TYPE_GenericVector: 8,

    /**
     * Constante: TYPE_OSM
     * Couche de type GeoJSON.
     */
    TYPE_OSM: 9,

    /**
     * Constante: TYPE_XYZ
     * Couche de type GeoRefXYZ.
     */
    TYPE_XYZ: 100,

    /**
     * Constante: TYPE_VectorTile
     * Couche de type VectorTile.
     */
    TYPE_VectorTile: 101,

    /**
     * Constante: POINT_GEOMETRY
     * Géométrie ponctuelle.
     */
    POINT_GEOMETRY: 'Point',

    /**
     * Constante: LINE_GEOMETRY
     * Géométrie linéaire.
     */
    LINE_GEOMETRY: 'Line',

    /**
     * Constante: POLYGON_GEOMETRY
     * Géométrie surfacique.
     */
    POLYGON_GEOMETRY: 'Polygon',

    /**
     * Constante MULTI_POINT_GEOMETRY
     * Géométrie multi points.
     */
    MULTI_POINT_GEOMETRY: 'MultiPoint',

    /**
     * Constante MULTI_LINE_GEOMETRY
     * Géométrie multi lignes.
     */
    MULTI_LINE_GEOMETRY: 'MultiLine',

    /**
     * Constante MULTI_POLYGON_GEOMETRY
     * Géométrie multi polygones.
     */
    MULTI_POLYGON_GEOMETRY: 'MultiPolygon',

    /**
     * Constante GENERIC_GEOMETRY
     * Géométrie générique.
     */
    GENERIC_GEOMETRY: 'Generic',

    /**
     * Constante: TEMPLATE_ID
     * Template pour l'identifiant de la couche.
     */
    TEMPLATE_ID: 'DescartesLayer_',

    /**
     * Constante: GET_REQUEST_LOADER
     * Requête de chargement de type GET.
     */
    GET_REQUEST_LOADER: 'Get',

    /**
     * Constante: POST_REQUEST_LOADER
     * Requête de chargement de type POST.
     */
    POST_REQUEST_LOADER: 'Post',

    /**
     * Constante: SERVER_TYPE_MAPSERVER
     * Serveur de type Mapserver.
     */
    SERVER_TYPE_MAPSERVER: 'mapserver',

    /**
     * Constante: SERVER_TYPE_GEOSERVER
     * Serveur de type Geoserver.
     */
    SERVER_TYPE_GEOSERVER: 'geoserver'
};
