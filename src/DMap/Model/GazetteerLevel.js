var _ = require('lodash');
var Utils = require('../Utils/DescartesUtils');

/**
 * Class: Descartes.GazetteerLevel
 * Objet "métier" correspondant à un niveau de localisation pour le recadrage de la carte suite au choix d'un objet de référence.
 *
 * :
 * Voir <Descartes.Action.Gazetteer> pour l'utilisation de cette classe.
 */
var Class = Utils.Class({
    /**
     * Propriete: index
     * {String} Index du niveau dans l'orde d'imbrication.
     *
     * :
     * Automatiquement calculé lors de l'instanciation de la classe <Descartes.Action.Gazetteer>.
     */
    index: null,

    /**
     * Propriete: message
     * {String} Eventuel message d'invite apparaissant au début de la liste déroulante du niveau.
     */
    message: null,

    /**
     * Propriete: error
     * {String} Eventuel message d'erreur apparaissant comme seul élément de la liste déroulante du niveau si aucun objet de référence n'existe.
     */
    error: null,

    /**
     * Propriete: name
     * {String} Base pour le nom du fichier JSON stockant les objets de référence du niveau.
     */
    name: null,

    /**
     * Propriete: datasList
     * {Array(Object)} Liste des objets de référence du niveau, obtenue après l'exécution de la méthode <Descartes.UI.GazetteerInPlace.ajaxCall>.
     *
     * :
     * Les objets JSON ont les propriétés suivantes :
     *
     * :
     * code - Code de l'objet (dans le cas de <Descartes.Action.DefaultGazetteer>, il s'agit du code INSEE).
     * nom - Nom de l'objet.
     * xmin - Abscisse minimale de l'emprise de l'objet
     * ymin - Ordonnée minimale de l'emprise de l'objet
     * xmax - Abscisse maximale de l'emprise de l'objet
     * ymax - Ordonnée maximale de l'emprise de l'objet
     */
    datasList: null,

    /**
     * Propriete: defaultDisplayOptions
     * {Object} Liste des objets de référence du niveau, obtenue après l'exécution de la méthode <Descartes.UI.GazetteerInPlace.ajaxCall>.
     *
     * :
     * Les objets JSON ont les propriétés suivantes :
     *
     * :
     * liveSearch - {Boolean} Acitivation ou non du filtre.
     * size - {Int} Nombre d'éléments affichés dans la liste.
     */
    defaultDisplayOptions: {
        liveSearch: true,
        size: 3,
        width: '95%',
        noneSelectedText: ""
    },

    /**
     * Propriete: displayOptions
     * {Object} Options d'affichage de la liste.
     */
    displayOptions: null,

    /**
     * Constructeur: Descartes.GazetteerLevel
     * Constructeur d'instances
     *
     * Paramètres:
     * message - {String} Eventuel message d'invite apparaissant au début de la liste déroulante du niveau.
     * error - {String} Eventuel message d'erreur apparaissant comme seul élément de la liste déroulante du niveau si aucun objet de référence n'existe.
     * name - {String} Base pour le nom du fichier JSON stockant les objets de référence du niveau.
     * options - {Object} Options d'affichage.
     */
     initialize: function (message, error, name, options) {
        this.message = message;
        this.error = error;
        this.name = name;
        this.displayOptions = _.extend({}, this.defaultDisplayOptions);
        _.extend(this.displayOptions, options);
    },
    CLASS_NAME: 'Descartes.GazetteerLevel'
});

module.exports = Class;
