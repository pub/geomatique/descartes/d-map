var _ = require('lodash');
var log = require('loglevel');

var Utils = require('../Utils/DescartesUtils');
var LayerConstants = require('./LayerConstants');

/**
 * Class: Descartes.WmsLayer
 * Objet "métier" correspondant à une couche WMS découverte et pouvant être ajoutée à la carte.
 *
 * :
 * Cette classe est utilisée par l'outil <Descartes.Button.ContentTask.ChooseWmsLayers>
 */
var Class = Utils.Class({

    formatsAllowed: ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'],

    /**
     * Propriete: urlServer
     * {String} URL du serveur WMS.
     */
    urlServer: null,

    /**
     * Propriete: serverVersion
     * {String} version du serveur WMS.
     */
    serverVersion: null,

    /**
     * Propriete: name
     * {String} Nom WMS de la couche
     */
    name: null,

    /**
     * Propriete: title
     * {String} Titre de la couche
     */
    title: null,

    /**
     * Propriete: description
     * {String} Description de la couche (Abstract).
     */
    description: '-',

    /**
     * Propriete: minScale
     * {Float} Dénominateur de l'échelle minimale de visibilité de la couche.
     */
    minScale: 0,

    /**
     * Propriete: maxScale
     * {Float} Dénominateur de l'échelle maximale de visibilité de la couche.
     */
    maxScale: 0,

    /**
     * Propriete: formats
     * {Array(String)} Formats disponibles.
     */
    formats: [],

    /**
     * Propriete: styles
     * {Array(String)} Styles disponibles.
     */
    styles: [],

    /**
     * Propriete: legend
     * {Array(String)} Adresses d'accés aux légendes.
     */
    legend: [],

    /**
     * Propriete: metadataURL
     * {String} Adresse d'accés à des informations complémentaires.
     */
    metadataURL: '-',

    /**
     * Propriete: keywordList
     * {String} Liste des mots-clés (concaténée avec des espaces).
     */
    keywordList: '-',

    /**
     * Propriete: selectedFormat
     * {String} Format sélectionné par l'utilisateur.
     */
    selectedFormat: null,

    /**
     * Propriete: selectedStyle
     * {String} Style sélectionné par l'utilisateur.
     */
    selectedStyle: null,

    /**
     * Private
     */
    valid: false,

    /**
     * Constructeur: Descartes.Layer
     * Constructeur d'instances
     *
     * Paramètres:
     * capabilitiesLayer - {Object} Couche découverte par interrogation WMS/GetCapabilities.
     * urlServer - {String} URL du serveur WMS.
     *
     * :
     * La couche découverte est une propriété "capability.layers[i]" d'une instance de OpenLayers.Format.WMSCapabilities.
     */
    initialize: function (capabilitiesLayer, urlServer, formats) {
        var cl = capabilitiesLayer;
        this.urlServer = urlServer;

        // name ok
        if (!_.isNil(cl.Name)) {
            this.name = cl.Name;
            this.valid = true;
        }
        //title ok
        if (!_.isNil(cl.Title)) {
            this.title = cl.Title;
        } else {
            this.title = this.name;
        }
        //MaxScaleDenominator en remplacement de maxScale
        if (!_.isNil(cl.MaxScaleDenominator)) {
            this.minScale = cl.MaxScaleDenominator;
        } else if (!_.isNil(cl.ScaleHint) && !_.isNil(cl.ScaleHint[0].minScale)) {
            this.minScale = cl.ScaleHint[0].minScale;
        }
        //MinScaleDenominator en remplacement min scale
        if (!_.isNil(cl.MinScaleDenominator)) {
            this.maxScale = cl.MinScaleDenominator;
        } else if (!_.isNil(cl.ScaleHint) && !_.isNil(cl.ScaleHint[0].maxScale)) {
            this.maxScale = cl.ScaleHint[0].maxScale;
        }
        //queryable ok
        if (!_.isNil(cl.queryable)) {
            this.queryable = cl.queryable;
        }

        this.srs = this.getCRSOrSRS(cl);

        this.formats = [];
        //formats ok
        if (!_.isNil(formats)) {
            var that = this;
            _.each(this.formatsAllowed, function (formatAllowed) {
                if (that.isInClFormats(formats, formatAllowed)) {
                    that.formats.push(formatAllowed);
                }
            });
        }
        //keywords ok
        if (!_.isNil(cl.KeywordList) && cl.KeywordList.length !== 0) {
            this.keywordList = this._join(cl.KeywordList);
        }
        //metadataURL ok
        if (!_.isNil(cl.MetadataURL) && cl.MetadataURL.length > 0) {
            this.metadataURL = cl.MetadataURL[0].OnlineResource;
        }
        this.legend = [];
        this.styles = [];

        //styles ok
        if (!_.isNil(cl.Style)) {
            this.styles = cl.Style;
        }

        //abstract ok
        if (!_.isNil(cl.Abstract)) {
            this.description = cl.Abstract;
        }
    },

    // CRS (SRS for WMS version < 1.3.0)
    // append ancestor srs or crs
    getCRSOrSRS: function (layer) {
        var result = [];

        if (!_.isNil(layer.CRS)) {
            result = layer.CRS;
        } else if (!_.isNil(layer.SRS)) {
            result = layer.SRS;
        }

        if (!_.isNil(layer.parent)) {
            result = result.concat(this.getCRSOrSRS(layer.parent));
        }

        return result;
    },

    /**
     * Methode: isValid
     * Indique si la couche découverte est valide pour la carte.
     *
     * Paramètres:
     * mapProjection - {String} Projection de la carte
     *
     * Retour:
     * {Boolean} La couche est valide si elle est disponible dans la projection de la carte.
     */
    isValid: function (mapProjection) {
        if (this.valid === true) {
            var code = mapProjection.getCode();
            log.debug('Map projection=', code);
            for (var i = 0; i < this.srs.length; i++) {
                var aCode = this.srs[i];
                log.debug('Layer SRS=', aCode);
                if (code === aCode) {
                    log.debug('ok');
                    return true;
                }
            }
            log.debug('ko');
            return false;
        }
        return false;
    },

    /**
     * Methode: toJSON
     * Fournit une représentation simplifié de la couche sous forme d'objet JSON.
     *
     * Retour:
     * {Object} Objet JSON.
     *
     * :
     * L'objet JSON est de la forme suivante :
     * (start code)
     * {
     * 		title: <Intitulé>,
     * 		type: <Descartes.Layer.TYPE_WMS>,
     * 		layersDefinition: {
     * 				layerName: <Nom de la couche ou de l'objet OWS>
     * 				serverUrl: <URL du serveur OWS>
     * 			} [],
     * 		options: {
     * 				format: <Type MIME pour l'affichage des couches OpenLayers>,
     * 				legend: <Adresses d'accés aux légendes>,
     * 				metadataURL: <Adresse d'accés aux informations complémentaires>,
     * 				queryable: <Interrogation potentielle>,
     * 				maxScale: <Dénominateur de l'�chelle maximale>,
     * 				minScale: <Dénominateur de l'�chelle minimale>
     * 			}
     * }
     * (end)
     */
    toJSON: function () {
        var json = {};
        json.title = this.title;
        json.type = LayerConstants.TYPE_WMS;
        json.layersDefinition = [{
                layerName: this.name,
                serverUrl: this.urlServer
            }];


        if (this.serverVersion !== null) {
            json.layersDefinition[0].serverVersion = this.serverVersion;
        }

        if (this.selectedStyle !== null) {
            json.layersDefinition[0].layerStyles = this.selectedStyle;
        }

        json.options = {
            format: this.selectedFormat,
            queryable: this.queryable
        };

        /*if (this.legend !== null) {
         json.options.legend = this.legend;
         }*/
        if (this.styles !== null && this.styles.length !== 0) {
            for (var i = 0, len = this.styles.length; i < len; i++) {
                if (this.styles[i].Title === this.selectedStyle && !_.isNil(this.styles[i].LegendURL) && !_.isNil(this.styles[i].LegendURL[0]) && !_.isNil(this.styles[i].LegendURL[0].OnlineResource)) {
                    this.legend.push(this.styles[i].LegendURL[0].OnlineResource);
                }
            }
            json.options.legend = this.legend;
        }

        if (this.metadataURL !== null) {
            json.options.metadataURL = this.metadataURL;
        }
        if (this.maxScale !== 0) {
            json.options.maxScale = parseFloat(this.maxScale);
        }
        if (this.minScale !== 0) {
            json.options.minScale = parseFloat(this.minScale);
        }
        return json;
    },

    /*
     * private
     */
    isInClFormats: function (tabFormats, format) {
        for (var i = 0; i < tabFormats.length; i++) {
            var tabFormat = tabFormats[i];
            var leFormat = tabFormat.split(';');
            if (leFormat[0].toLowerCase() === format) {
                return true;
            }
        }
        return false;
    },

    /*
     * private
     */
    _join: function (keywords) {
        var keywordList = '';
        _.each(keywords, function (keyword, index) {
            if (index > 0) {
                keywordList += ' ';
            }
            keywordList += keyword;
        });

        return keywordList;
    },

    CLASS_NAME: 'Descartes.WmsLayer'
});

module.exports = Class;


