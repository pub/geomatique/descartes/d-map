/* global Descartes */

var Utils = require('../Utils/DescartesUtils');
var UrlParam = require('./UrlParam');
var Url = require('./Url');
/**
 * Class: Descartes.UrlExterne
 * Classe "technique" permettant d'effectuer des manipulations sur les paramètres de l'URL externe d'une requête HTTP-GET.
 */
var Class = Utils.Class({
    /**
     * Propriete: root
     * {String} Fragment de l'URL ne comportant pas les paramètres
     */
    root: null,

    /**
     * Propriete: params
     * {Array(<Descartes.UrlParam>)} Ensemble des paramètres de l'URL
     */
    params: [],

    /**
     * Propriete: urlProxy
     * {<Descartes.Url>} Url du proxy
     */
    urlProxy: null,

    /**
     * Constructeur: Descartes.UrlExterne
     * Constructeur d'instances
     *
     * Paramètres:
     * url - {String} URL de la requête HTTP-GET.
     */
    initialize: function (url) {
        this.params = [];

        var urlDescartesProxy = new Url(Descartes.PROXY_SERVER);
        this.urlProxy = urlDescartesProxy.getUrlString();
        if (this.urlProxy.indexOf('?') === -1) {
            this.urlProxy += '?';
        }
        this.root = this.urlProxy + url;
    },

    /**
     * Methode: addParam
     * Ajoute un paramètre à l'URL.
     *
     * Paramètres:
     * paramKey - {String} Clé du paramètre.
     * paramValue - {String} Valeur du paramètre.
     */
    addParam: function (paramKey, paramValue) {
        this.params.push(new UrlParam(paramKey, paramValue));
    },

    /**
     * Methode: addParams
     * Ajoute plusieurs paramètres à l'URL.
     *
     * Paramètres:
     * paramsKV - {Array} Tableau des paramètres sous la forme [ [clé1, valeur1], [clé2, valeur2], [clé3, valeur3], ... ].
     */
    addParams: function (paramsKV) {
        for (var iParamKV = 0; iParamKV < paramsKV.length; iParamKV++) {
            this.addParam(paramsKV[iParamKV][0], paramsKV[iParamKV][1]);
        }
    },

    /**
     * Methode: removeParam
     * Supprime un paramètre de l'URL.
     *
     * Paramètres:
     * paramKey - {String} Clé du paramètre.
     */
    removeParam: function (paramKey) {
        for (var iParam = 0; iParam < this.params.length; iParam++) {
            if (this.params[iParam].key === paramKey) {
                this.params.splice(iParam, 1);
                break;
            }
        }
    },

    /**
     * Methode: removeParams
     * Supprime plusieurs paramètres de l'URL.
     *
     * Paramètres:
     * paramsKey - {Array} Tableau des paramètres sous la forme [ clé1, clé2, clé3, ... ].
     */
    removeParams: function (paramsKey) {
        for (var iParam = 0; iParam < paramsKey.length; iParam++) {
            this.removeParam(paramsKey[iParam]);
        }
    },

    /**
     * Methode: alterParam
     * Modifie un paramètre de l'URL.
     *
     * Paramètres:
     * paramKey - {String} Clé du paramètre.
     * paramValue - {String} Valeur du paramètre.
     */
    alterParam: function (paramKey, paramValue) {
        var done = false;
        for (var iParam = 0; iParam < this.params.length; iParam++) {
            if (this.params[iParam].key === paramKey) {
                this.params[iParam].value = paramValue;
                done = true;
                break;
            }
        }
        return done;
    },

    /**
     * Methode: alterParams
     * Modifie plusieurs paramètres de l'URL.
     *
     * Paramètres:
     * paramsKV - {Array} Tableau des paramètres sous la forme [ [clé1, valeur1], [clé2, valeur2], [clé3, valeur3], ... ].
     */
    alterParams: function (paramsKV) {
        for (var iParamKV = 0; iParamKV < paramsKV.length; iParamKV++) {
            this.alterParam(paramsKV[iParamKV][0], paramsKV[iParamKV][1]);
        }
    },

    /**
     * Methode: alterOrAddParam
     * Ajoute un paramètre à l'URL ou le met à jour s'il existe déjà.
     *
     * Paramètres:
     * paramKey - {String} Clé du paramètre.
     * paramValue - {String} Valeur du paramètre.
     */
    alterOrAddParam: function (paramKey, paramValue) {
        if (!this.alterParam(paramKey, paramValue)) {
            this.addParam(paramKey, paramValue);
        }
    },

    /**
     * Methode: alterOrAddParams
     * Ajoute plusieurs paramètres à l'URL ou les met à jour s'ils existent déjà.
     *
     * Paramètres:
     * paramsKV - {Array} Tableau des paramètres sous la forme [ [clé1, valeur1], [clé2, valeur2], [clé3, valeur3], ... ].
     */
    alterOrAddParams: function (paramsKV) {
        for (var iParamKV = 0; iParamKV < paramsKV.length; iParamKV++) {
            this.alterOrAddParam(paramsKV[iParamKV][0], paramsKV[iParamKV][1]);
        }
    },

    /**
     * Methode: getParamValue
     * Fournit la valeur d'un paramètre de l'URL.
     *
     * Paramètres:
     * paramKey - {String} Clé du paramètre.
     *
     * Retour:
     * {String} Valeur du paramètre.
     */
    getParamValue: function (paramKey) {
        var done = '';
        for (var iParam = 0; iParam < this.params.length; iParam++) {
            if (this.params[iParam].key === paramKey) {
                done = this.params[iParam].value;
                break;
            }
        }
        return done;
    },

    /**
     * Methode: getUrlString
     * Construit l'URL de la requête avec les paramètres courants.
     *
     * Retour:
     * {String} URL de la requête.
     */
    getUrlString: function () {
        var newUrl = this.root;
        if (this.params.length !== 0) {
            //newUrl += "?";
            for (var iParam = 0; iParam < this.params.length; iParam++) {
                newUrl += this.params[iParam].getQueryString();
                if (iParam < (this.params.length - 1)) {
                    newUrl += '&';
                }
            }
        }
        return newUrl;
    },
    CLASS_NAME: 'Descartes.UrlExterne'
});

module.exports = Class;
