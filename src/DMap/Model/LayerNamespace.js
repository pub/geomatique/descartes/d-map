/* global GEOREF */

var _ = require('lodash');

var Layer = require('./Layer');
var GeoJSON = require('./Layer/GeoJSON');
var KML = require('./Layer/KML');
var TMS = require('./Layer/TMS');
var WFS = require('./Layer/WFS');
var WMS = require('./Layer/WMS');
var WMSC = require('./Layer/WMSC');
var WMTS = require('./Layer/WMTS');
var GenericVector = require('./Layer/GenericVector');
var Tiled = require('./Layer/Tiled');
var Vector = require('./Layer/Vector');
var OSM = require('./Layer/OSM');
var XYZ = require('./Layer/XYZ');
var VectorTile = require('./Layer/VectorTile');

var ResultLayer = require('./ResultLayer');
var ResultLayerConstants = require('./ResultLayerConstants');

var ClusterLayer = require('./ClusterLayerNamespace');

var constants = require('./LayerConstants');

_.extend(ResultLayer, ResultLayerConstants);

var namespace = {
    GeoJSON: GeoJSON,
    ClusterLayer: ClusterLayer,
    KML: KML,
    Tiled: Tiled,
    TMS: TMS,
    WFS: WFS,
    GenericVector: GenericVector,
    WMS: WMS,
    WMSC: WMSC,
    WMTS: WMTS,
    OSM: OSM,
    XYZ: XYZ,
    Vector: Vector,
    VectorTile: VectorTile,
    ResultLayer: ResultLayer
};

if (GEOREF) {
    namespace.LocalisationAdresseLayer = require('./LocalisationAdresseLayer');
}

_.extend(namespace, constants);
_.extend(Layer, namespace);

module.exports = Layer;
