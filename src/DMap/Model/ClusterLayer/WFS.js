/* global */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = require('../../Utils/DescartesUtils');

var WFS = require('../Layer/WFS');
var ClusterLayer = require('../ClusterLayer');
var ClusterLayerConstants = require('../ClusterLayerConstants');

/**
 * Class: Descartes.Layer.ClusterLayer.WFS
 * Objet "métier" correspondant à une couche de type cluster pour le contenu de la carte, basée sur des *ressources WFS*.
 *
 * Hérite de:
 * - <Descartes.Layer.ClusterLayer>
 * - <Descartes.Layer.WFS>
 */
var Class = Utils.Class(WFS, ClusterLayer, {

    /**
     * Constructeur: Descartes.Layer.ClusterLayer.WFS
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches OWS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     */
     initialize: function (title, layersDefinition, options) {
         var opts = this.initClusterConfig(options);
         WFS.prototype.initialize.apply(this, [title, layersDefinition, opts]);
         this.type = ClusterLayerConstants.TYPE_WFS;
         this.initStyles();
    },

    createOL_layer: function (layerDefinition) {
        var vector = WFS.prototype.createOL_layer.apply(this, [layerDefinition]);
        return this.initVectorCluster(vector);
    },

    CLASS_NAME: "Descartes.Layer.ClusterLayer.WFS"
});

module.exports = Class;
