/* global */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = require('../../Utils/DescartesUtils');

var KML = require('../Layer/KML');
var ClusterLayer = require('../ClusterLayer');
var ClusterLayerConstants = require('../ClusterLayerConstants');

/**
 * Class: Descartes.Layer.ClusterLayer.KML
 * Objet "métier" correspondant à une couche de type cluster pour le contenu de la carte, basée sur des *ressources KML*.
 *
 * Hérite de:
 * - <Descartes.Layer.ClusterLayer>
 * - <Descartes.Layer.KML>
 */
var Class = Utils.Class(KML, ClusterLayer, {

    /**
     * Constructeur: Descartes.Layer.ClusterLayer.KML
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches OWS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     */
     initialize: function (title, layersDefinition, options) {
         var opts = this.initClusterConfig(options);
         KML.prototype.initialize.apply(this, [title, layersDefinition, opts]);
         this.type = ClusterLayerConstants.TYPE_KML;
         this.initStyles();
     },

     createOL_layer: function (layerDefinition) {
        var vector = KML.prototype.createOL_layer.apply(this, [layerDefinition]);
        return this.initVectorCluster(vector);
     },

     CLASS_NAME: "Descartes.Layer.ClusterLayer.KML"
});

module.exports = Class;
