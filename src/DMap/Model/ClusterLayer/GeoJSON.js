/* global */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = require('../../Utils/DescartesUtils');

var GeoJSON = require('../Layer/GeoJSON');
var ClusterLayer = require('../ClusterLayer');
var ClusterLayerConstants = require('../ClusterLayerConstants');

/**
 * Class: Descartes.Layer.ClusterLayer.GeoJSON
 * Objet "métier" correspondant à une couche de type cluster pour le contenu de la carte, basée sur des *ressources GeoJSON*.
 *
 * Hérite de:
 * - <Descartes.Layer.ClusterLayer>
 * - <Descartes.Layer.GeoJSON>
 */
var Class = Utils.Class(GeoJSON, ClusterLayer, {

    /**
     * Constructeur: Descartes.Layer.ClusterLayer.GeoJSON
     * Constructeur d'instances
     *
     * Paramètres:
     * title - {String} Intitulé de la couche.
     * layersDefinition - {Array(<Descartes.ResourceLayer>)} Définition des couches OWS constituant la couche.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     */
     initialize: function (title, layersDefinition, options) {
         var opts = this.initClusterConfig(options);
         GeoJSON.prototype.initialize.apply(this, [title, layersDefinition, opts]);
         this.type = ClusterLayerConstants.TYPE_GeoJSON;
         this.initStyles();
     },

     createOL_layer: function (layerDefinition) {
        var vector = GeoJSON.prototype.createOL_layer.apply(this, [layerDefinition]);
        return this.initVectorCluster(vector);
     },

     CLASS_NAME: "Descartes.Layer.ClusterLayer.GeoJSON"
});

module.exports = Class;
