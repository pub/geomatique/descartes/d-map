/* global MODE */
var _ = require('lodash');

var Tool = require('./' + MODE + '/Tool');
var ZoomToInitialExtent = require('./ZoomToInitialExtent');
var ZoomToMaximalExtent = require('./ZoomToMaximalExtent');
var ZoomIn = require('./ZoomIn');
var ZoomOut = require('./ZoomOut');
var DragPan = require('./DragPan');
var CenterMap = require('./' + MODE + '/CenterMap');
var MeasureNamespace = require('./Measure/MeasureNamespace');
var GeolocationNamespace = require('./Geolocation/GeolocationNamespace');
var SelectionNamespace = require('./Selection/SelectionNamespace');
var CenterMapWithInteraction = require('./CenterMapWithInteraction');
var MiniMap = require('./MiniMap');
var NavigationHistory = require('./' + MODE + '/NavigationHistory');

var namespace = {
    CenterMap: CenterMap,
    CenterMap2: CenterMapWithInteraction,
    DragPan: DragPan,
    Geolocation: GeolocationNamespace,
    Measure: MeasureNamespace,
    MiniMap: MiniMap,
    NavigationHistory: NavigationHistory,
    Selection: SelectionNamespace,
    ZoomIn: ZoomIn,
    ZoomOut: ZoomOut,
    ZoomToInitialExtent: ZoomToInitialExtent,
    ZoomToMaximalExtent: ZoomToMaximalExtent
};

_.extend(Tool, namespace);

module.exports = Tool;
