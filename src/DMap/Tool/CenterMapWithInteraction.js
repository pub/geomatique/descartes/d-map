var Utils = require('../Utils/DescartesUtils');
var Tool = require('../Core/Tool');
var _ = require('lodash');
var ol = require('openlayers');

var CenterMap = Utils.Class(Tool, {
    initialize: function () {
        Tool.prototype.initialize.apply(this, arguments);
        this.interaction = new ol.interaction.Pointer({
            handleDownEvent: this.onMapClick
        });
        this.interaction.setActive(false);
    },
    handleClick: function () {
        var map = this.getMap();
        var interactions = map.getInteractions().getArray();
        if (_.indexOf(interactions, this.interaction) === -1) {
            map.addInteraction(this.interaction);
        }
        if (this.interaction.getActive()) {
            this.deactivate();
            this.setCursor('auto');
        } else {
            this.activate();
            this.setCursor('crosshair');
        }
    },
    onMapClick: function (evt) {
        var coordinate = evt.coordinate;
        var view = this.getMap().getView();
        view.setCenter(coordinate);
    },
    setCursor: function (cursor) {
        this.getMap().getTargetElement().style.cursor = cursor;
    },
    CLASS_NAME: 'Descartes.Tool.CenterMapWithInteraction'
});

module.exports = CenterMap;
