var $ = require('jquery');

var Utils = require('../../Utils/DescartesUtils');
var Tool = require('./Tool');

var template = require('./templates/Tool.ejs');

require('../css/CenterMap.css');

/**
 * Class: Descartes.Tool.CenterMap
 * Classe définissant un bouton permettant de recentrer la carte suite à un clic du pointeur sur celle-ci.
 *
 * Hérite de:
 *  - <Descartes.Tool>
 */
var Class = Utils.Class(Tool, {

    active: false,

    /**
     * Constructeur: Descartes.Tool.CenterMap
     * Constructeur d'instances
     */
    initialize: function (options) {
        Tool.prototype.initialize.apply(this, arguments);
    },
    /**
     * Methode: handleClick
     * Effectue le recentrage suite au clic du pointeur
     *
     */
    handleClick: function () {
        var map = this.getMap();
        if (this.active) {
            this.deactivate();
        } else {
            map.on('click', this.onMapClick, this);
            this.active = true;
            this.activate();
        }
    },
    createElement: function () {
        var elementClass = this.displayClass + 'ItemActive';

        var element = template({
            id: this.id,
            displayClass: elementClass,
            btnCss: this.btnCss,
            active: this.active,
            title: this.title
        });
        return element;
    },
    updateElement: function () {
        var elementHtml = this.createElement();
        $('#' + this.id).replaceWith(elementHtml);
        $('#' + this.id).on('click', this.handleClick.bind(this));
    },
    /**
     * Methode: onMapClick
     * Effectue le recentrage suite au clic du pointeur
     */
    onMapClick: function (evt) {
        var coordinate = evt.coordinate;
        var view = this.getMap().getView();
        view.setCenter(coordinate);
    },

    deactivate: function () {
        var map = this.getMap();
        map.un('click', this.onMapClick, this);
        this.active = false;
        this.updateElement();
    },
    CLASS_NAME: 'Descartes.Tool.CenterMap'
});

module.exports = Class;
