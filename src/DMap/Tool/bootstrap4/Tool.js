/* global Descartes */

var _ = require('lodash');
var $ = require('jquery');
var log = require('loglevel');

var Utils = require('../../Utils/DescartesUtils');
var Tool = require('../../Core/Tool');

var template = require('./templates/Tool.ejs');

/**
 * Class: Descartes.Tool
 * Classe "abstraite" définissant un bouton d'interaction avec la carte.
 *
 * :
 * Le bouton doit être placé dans une barre d'outils définie par la classe <Descartes.ToolBar>.
 *
 * Un seul bouton de ce type peut être accessible à chaque instant parmi l'ensemble des boutons d'interaction de la barre d'outils.
 *
 * Hérite de:
 *  - <Descartes.Tool>
 *
 */
var Class = Utils.Class(Tool, {

    btnCss: null,

    /**
     * Constructeur: Descartes.Tool
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * title - {String} Texte de l'info-bulle du bouton.
     */
    initialize: function () {
        this.btnCss = Descartes.UIBootstrap4Options.btnCssToolBar;
        Tool.prototype.initialize.apply(this, arguments);
    },
    createElement: function () {
        var elementClass = this.displayClass + 'ItemActive';

        var active = false;
        if (!_.isNil(this.interaction)) {
            active = this.interaction.getActive();
        } else if (!_.isNil(this.active)) {
            active = this.active;
        }

        var element = template({
            id: this.id,
            displayClass: elementClass,
            btnCss: this.btnCss,
            active: active,
            title: this.title
        });
        return element;
    },
    updateElement: function () {
        log.debug('update element with id=%s of class %s', this.id, this.CLASS_NAME);
        var elementHtml = this.createElement();
        $('#' + this.id).replaceWith(elementHtml);
        $('#' + this.id).on('click', this.handleClick.bind(this));
    },
    CLASS_NAME: 'Descartes.Button'
});

module.exports = Class;
