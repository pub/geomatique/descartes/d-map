var _ = require('lodash');
var $ = require('jquery');
var log = require('loglevel');

var Utils = require('../../Utils/DescartesUtils');
var Tool = require('./Tool');

var template = require('./templates/NavigationHistory.ejs');

require('../css/NavigationHistory.css');

/**
 * Class: Descartes.Tool.NavigationHistory
 * Classe définissant un ensemble de boutons gérant l'historique de navigation (zooms, déplacements, etc.).
 *
 * Hérite de:
 *  - <Descartes.Tool>
 */
var Class = Utils.Class(Tool, {

    active: false,

    /**
     * Direction, avant ou arrière.
     * Si true, alors avant, sinon arrière
     */
    direction: true,

    duration: 300,

    localHistory: [],

    localHistoryKey: 'navigationHistory',

    localHistoryIndexKey: 'navigationHistoryIndex',

    localHistoryClicked: 'navigationHistoryClicked',

    /**
     * Constructeur: Descartes.Tool.NavigationHistory
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * previousOptions - {Object} Objet JSON stockant le titre du bouton "Retours" et sa classe CSS sous la forme {title:"Titre", displayClass:"classeCss"}.
     * nextOptions - {Object} Objet JSON stockant le titre du bouton "Allers" et sa classe CSS sous la forme {title:"Titre", displayClass:"classeCss"}.
     */
    initialize: function (options, direction) {
        this.direction = direction;
        this.active = true;

        if (!_.isNil(window.localStorage)) {
            this.localHistory = this.getLocalHistory();
        } else {
            log.warn('LocalStorate n\'est pas supporté.');
            return;
        }
        this.initLocalHistory();
        Tool.prototype.initialize.apply(this, arguments);

        if (_.isNil(options.title)) {
            this.title = this.getMessage('PREVIOUS_TITLE');
            if (this.direction === true) {
                this.title = this.getMessage('NEXT_TITLE');
            }
        }
        if (!_.isNil(options.duration)) {
            this.duration = options.duration;
        }
    },
    createElement: function (update) {
        if (!update) {
            this.id = this.id + this.direction;
        }
        var directionName = 'Previous';
        if (this.direction === true) {
            directionName = 'Next';
            this.active = this.hasNextItem();
        } else {
            this.active = this.hasPreviousItem();
        }

        var elementClass = this.displayClass + directionName;

        if (this.active) {
            elementClass += 'ItemActive';
        } else {
            elementClass += 'ItemInactive';
        }

        /*this.title = this.getMessage('PREVIOUS_TITLE');
         if (this.direction === true) {
         this.title = this.getMessage('NEXT_TITLE');
         }*/

        var element = template({
            id: this.id,
            displayClass: elementClass,
            btnCss: this.btnCss,
            active: this.active,
            title: this.title
        });
        return element;
    },
    setMap: function (map) {
        if (this.direction === true) {
            map.on('moveend', this.addHistoryItem.bind(this), this);
        } else {
            //il faut que l'outil navigation history next s'abonne avant previous
            setTimeout(function () {
                map.on('moveend', this.updateElement.bind(this), this);
            }.bind(this), 200);
        }
        Tool.prototype.setMap.apply(this, arguments);
    },
    updateElement: function () {
        this.localHistory = this.getLocalHistory();
        var elementHtml = this.createElement(true);
        $('#' + this.id).replaceWith(elementHtml);
        $('#' + this.id).on('click', this.handleClick.bind(this));
    },
    handleClick: function () {
        log.debug('click %s active=%s', this.direction, this.active);

        var item = null;
        if (this.direction === true) {
            item = this.nextItem();
        } else {
            item = this.previousItem();
        }
        if (!_.isNil(item)) {
            var view = this.getMap().getView();

            var that = this;
            view.animate({
                center: item.center,
                resolution: item.resolution,
                duration: this.duration
            }, function () {
                //navigation via l'historique, il ne faut pas ajouter d'élément.
                window.localStorage.setItem(that.localHistoryClicked, true);
            });
        }
    },

    addHistoryItem: function () {
        if (window.localStorage.getItem(this.localHistoryClicked) === "true") {
            //navigation via l'historique, il ne faut pas ajouter d'élément.
            log.debug('history navigation');
            window.localStorage.setItem(this.localHistoryClicked, false);
            this.updateElement();
            return;
        }
        this.localHistory = this.getLocalHistory();
        log.debug('normal navigation item length %s', this.localHistory.length);

        var view = this.getMap().getView();

        var localHistoryIndex = window.localStorage.getItem(this.localHistoryIndexKey);
        if (_.isNil(localHistoryIndex)) {
            localHistoryIndex = -1;
        }
        localHistoryIndex++;

        if (this.localHistory.length !== localHistoryIndex) {
            this.localHistory.length = localHistoryIndex;
        }
        this.localHistory.push({
            center: view.getCenter(),
            resolution: view.getResolution()
        });
        window.localStorage.setItem(this.localHistoryIndexKey, localHistoryIndex);

        this.saveLocalHistory();
    },

    nextItem: function () {
        var localHistoryIndex = window.localStorage.getItem(this.localHistoryIndexKey);
        this.localHistory = this.getLocalHistory();
        localHistoryIndex++;
        log.debug('localHistoryIndex=', localHistoryIndex);
        var next = this.localHistory[localHistoryIndex];
        log.debug('next=', next);

        if (!_.isNil(next)) {
            window.localStorage.setItem(this.localHistoryIndexKey, localHistoryIndex);
            return next;
        }
        return null;
    },

    previousItem: function () {
        var localHistoryIndex = window.localStorage.getItem(this.localHistoryIndexKey);

        this.localHistory = this.getLocalHistory();
        localHistoryIndex--;
        log.debug('localHistoryIndex=', localHistoryIndex);
        var previous = this.localHistory[localHistoryIndex];
        log.debug('previous=', previous);

        if (!_.isNil(previous)) {
            window.localStorage.setItem(this.localHistoryIndexKey, localHistoryIndex);
            return previous;
        }
        return null;
    },
    initLocalHistory: function () {
        window.localStorage.setItem(this.localHistoryKey, JSON.stringify([]));
        window.localStorage.setItem(this.localHistoryIndexKey, -1);
        window.localStorage.setItem(this.localHistoryClicked, false);
    },
    saveLocalHistory: function () {
        var localHistoryStr = JSON.stringify(this.localHistory);
        window.localStorage.setItem(this.localHistoryKey, localHistoryStr);
        this.updateElement();
    },
    getLocalHistory: function () {
        var historyStr = window.localStorage.getItem(this.localHistoryKey);
        if (_.isNil(historyStr)) {
            this.localHistory = [];
            this.saveLocalHistory();
            return this.getLocalHistory();
        }
        return JSON.parse(historyStr);
    },
    hasNextItem: function () {
        var index = window.localStorage.getItem(this.localHistoryIndexKey);
        index++;
        return !_.isNil(this.localHistory[index]);
    },
    hasPreviousItem: function () {
        var index = window.localStorage.getItem(this.localHistoryIndexKey);
        index--;
        return !_.isNil(this.localHistory[index]);
    },

    CLASS_NAME: 'Descartes.Tool.NavigationHistory'
});

module.exports = Class;
