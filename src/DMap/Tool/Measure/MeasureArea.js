/* global MODE */

var ol = require('openlayers');
var Utils = require('../../Utils/DescartesUtils');
var Measure = require('./' + MODE + '/Measure');

require('./css/Measure.css');

/**
 * Class: Descartes.Tool.Measure.MeasureArea
 * Classe définissant un bouton permettant des calculs de surface par dessin de polygones sur la carte.
 *
 * Hérite de:
 *  - <Descartes.Tool.Measure>
 */
var Class = Utils.Class(Measure, {

    type: 'Polygon',

    /**
     * Propriete: perimetre
     * {Boolean} Indicateur pour l'affichage du périmètre en plus de l'affichage de l'aire (true par défaut).
     */
    perimetre: true,

	/**
	 * Constructeur: Descartes.Tool.Measure.MeasureArea
	 * Constructeur d'instances
	 *
	 * Paramètres:
	 * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
	 *
	 * Options de construction propres à la classe:
	 * perimetre - {Boolean} Indicateur pour l'affichage du périmètre en plus de l'affichage de l'aire.
	 */
    initialize: function () {
        Measure.prototype.initialize.apply(this, arguments);
    },
    format: function (polygon) {
        var projection = this.olMap.getView().getProjection();
        var area = ol.Sphere.getArea(polygon, {
            projection: projection
        });

        var unit = this.getMessage('TITLE_AIRE_UNITE');
        var output = this.getMessage('TITLE_AIRE');
        if (area > 10000) {
            output += (Math.round(area / 1000000 * 100) / 100) + ' km' + unit;
        } else {
            output += (Math.round(area * 100) / 100) + ' m' + unit;
        }
        if (this.perimetre) {
            var perimeter = Utils.getPolygonLength(polygon, projection);
            output += this.formatPerimeter(perimeter);
        }

        return output;
    },
    formatPerimeter: function (perimeter) {
        var length = Math.round(perimeter * 100) / 100;

        var output = this.getMessage('TITLE_PERIMETRE');
        if (length > 100) {
            output += (Math.round(length / 1000 * 100) / 100) + ' km';
        } else {
            output += (Math.round(length * 100) / 100) + ' m';
        }
        return output;
    },
    CLASS_NAME: 'Descartes.Tool.Measure.MeasureArea'
});

module.exports = Class;
