/* global MODE, Descartes */

var ol = require('openlayers');
var _ = require('lodash');
var $ = require('jquery');

var Utils = require('../../../Utils/DescartesUtils');
var Tool = require('../../../Core/Tool');
var template = require('../../' + MODE + '/templates/Tool.ejs');
var defaultSymbolizers = require('../../../Symbolizers');

require('../css/Measure.css');

/**
 * Class: Descartes.Tool.Measure
 * Classe "abstraite" définissant un bouton permettant des calculs de mesure par interaction graphique sur la carte.
 *
 * Hérite de:
 *  - <Descartes.Tool>
 *
 * Classes dérivées:
 *  - <Descartes.Tool.Measure.MeasureArea>
 *  - <Descartes.Tool.Measure.MeasureDistance>
 */
var Class = Utils.Class(Tool, {

    measureTooltipElement: null,
    measureTooltip: null,
    sketch: null,
    listener: null,
    nbOverlay: 0,
    type: null,
    btnCss: null,

    /**
     * Constructeur: Descartes.Tool.Measure
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * symbolizers - {Object} Objet JSON stockant les styles graphiques associé.
     */
    initialize: function (args, options) {
        this.btnCss = Descartes.UIBootstrap4Options.btnCssToolBar;
        Tool.prototype.initialize.apply(this, arguments);

        var measureSymbolizers = _.extend({}, defaultSymbolizers.Descartes_Symbolizers_Measure);
        if (!_.isNil(args)) {
            _.extend(this, args);
            if (!_.isNil(args.symbolizers)) {
                _.extend(measureSymbolizers, args.symbolizers);
            }
        }

        this.source = new ol.source.Vector();

        var olStyles = defaultSymbolizers.getOlStyle(measureSymbolizers);
        var styleFunction = function (feature, resolution) {
            var featureStyleFunction = feature.getStyleFunction();
            if (featureStyleFunction) {
                return featureStyleFunction.call(feature, resolution);
            } else {
                var style = olStyles[feature.getGeometry().getType()];
                return style;
            }
        };

        this.vector = new ol.layer.Vector({
            source: this.source,
            style: styleFunction
        });

        this.interaction = new ol.interaction.Draw({
            source: this.source,
            type: this.type,
            style: styleFunction
        });
        this.interaction.set('id', 'Measure_' + Utils.createUniqueID());
        this.interaction.on('drawstart', this.onDrawStart.bind(this), this);
        this.interaction.on('drawend', this.onDrawEnd.bind(this), this);
        this.interaction.setActive(false);
    },
    createElement: function () {
        var elementClass = this.displayClass + 'ItemActive';

        var active = false;
        if (!_.isNil(this.interaction)) {
            active = this.interaction.getActive();
        }
        var element = template({
            id: this.id,
            displayClass: elementClass,
            btnCss: this.btnCss,
            active: active,
            title: this.title
        });
        return element;
    },
    updateElement: function () {
        var elementHtml = this.createElement();
        $('#' + this.id).replaceWith(elementHtml);
        $('#' + this.id).on('click', this.handleClick.bind(this));
    },
    handleClick: function () {
        var map = this.getMap();
        if (this.interaction.getActive()) {
            this.deactivate();
        } else {
            map.addLayer(this.vector);
            map.addInteraction(this.interaction);
            this.activate();
            this.createMeasureTooltip();
        }
    },
    removeOverlays: function () {
        var overlaysToRemove = [];
        this.getMap().getOverlays().forEach(function (overlay) {
            if (_.startsWith(overlay.getId(), this.id)) {
                overlaysToRemove.push(overlay);
            }
        }.bind(this), this);

        for (var i = 0; i < overlaysToRemove.length; i++) {
            this.getMap().removeOverlay(overlaysToRemove[i]);
        }
    },
    createMeasureTooltip: function () {
        if (this.measureTooltipElement) {
            this.measureTooltipElement.parentNode.removeChild(this.measureTooltipElement);
        }
        this.measureTooltipElement = document.createElement('div');
        this.measureTooltipElement.className = 'Descartes-overlay-tooltip Descartes-tooltip-measure';
        this.nbOverlay++;
        this.measureTooltip = new ol.Overlay({
            id: this.id + '_' + this.nbOverlay,
            element: this.measureTooltipElement,
            offset: [0, -15],
            positioning: 'bottom-center'
        });
        this.getMap().addOverlay(this.measureTooltip);
    },
    onDrawStart: function (evt) {
        this.sketch = evt.feature;
        var tooltipCoord = evt.coordinate;
        this.listener = this.sketch.getGeometry().on('change', function (evt) {
            var geom = evt.target;
            var output = this.format(geom);
            if (geom instanceof ol.geom.LineString) {
                tooltipCoord = geom.getLastCoordinate();
            } else if (geom instanceof ol.geom.Polygon) {
                tooltipCoord = geom.getInteriorPoint().getCoordinates();
            }
            this.measureTooltipElement.innerHTML = output;
            this.measureTooltip.setPosition(tooltipCoord);
        }.bind(this), this);
    },
    onDrawEnd: function () {
        this.measureTooltipElement.className = 'Descartes-overlay-tooltip Descartes-tooltip-static';
        this.measureTooltip.setOffset([0, -7]);
        // unset sketch
        this.sketch = null;
        // unset tooltip so that a new one can be created
        this.measureTooltipElement = null;
        this.createMeasureTooltip();
        ol.Observable.unByKey(this.listener);
    },
    deactivate: function () {
        var map = this.getMap();
        this.source.clear();
        this.removeOverlays();
        map.removeLayer(this.vector);
        Tool.prototype.deactivate.apply(this, arguments);
        return true;
    },
    CLASS_NAME: 'Descartes.Tool.Measure'
});

module.exports = Class;
