/* global MODE */
var ol = require('openlayers');

var Utils = require('../../Utils/DescartesUtils');
var Measure = require('./' + MODE + '/Measure');

require('./css/Measure.css');

/**
 * Class: Descartes.Tool.Measure.MeasureDistance
 * Classe définissant un bouton permettant des calculs de distance par dessin de polylignes sur la carte.
 *
 * Hérite de:
 *  - <Descartes.Tool.Measure>
 */
var Class = Utils.Class(Measure, {

    type: 'LineString',

	/**
	 * Constructeur: Descartes.Tool.Measure.MeasureDistance
	 * Constructeur d'instances
	 *
	 * Paramètres:
	 * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
	 */
    initialize: function () {
        Measure.prototype.initialize.apply(this, arguments);
    },
    format: function (line) {
        var length = ol.Sphere.getLength(line, {
            projection: this.olMap.getView().getProjection()
        });
        var output;
        if (length > 100) {
            output = (Math.round(length / 1000 * 100) / 100) +
                    ' ' + 'km';
        } else {
            output = (Math.round(length * 100) / 100) +
                    ' ' + 'm';
        }
        return output;
    },
    CLASS_NAME: 'Descartes.Tool.Measure.MeasureDistance'
});

module.exports = Class;
