/* global MODE */
var _ = require('lodash');

var MeasureDistance = require('./MeasureDistance');
var MeasureArea = require('./MeasureArea');

var namespace = {
    MeasureDistance: MeasureDistance,
    MeasureArea: MeasureArea
};

var Measure = require('./' + MODE + '/Measure');

_.extend(Measure, namespace);

module.exports = Measure;
