/* global MODE */

var ol = require('openlayers');
var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var Tool = require('./' + MODE + '/Tool');
var MapConstants = require('../Map/MapConstants');

require('./css/ZoomToMaximalExtent.css');


/**
 * Class: Descartes.Tool.ZoomToMaximalExtent
 * Classe définissant un bouton permettant de recadrer la carte sur son emprise maximale.
 *
 * Hérite de:
 *  - <Descartes.Tool>
 */
var Class = Utils.Class(Tool, {

    /**
     * Constructeur: Descartes.Button.ZoomToMaximalExtent
     * Constructeur d'instances
     *
     * Paramètres:
     * bounds - {Array} Emprise initiale de la carte.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        this.maxExtent = options.maxExtent;
        Tool.prototype.initialize.apply(this, arguments);
    },
    handleClick: function () {
        var extent = this.getMaxExtent();
        var view = this.getMap().getView();
        var center = ol.extent.getCenter(extent);

        var mapType = this.getMap().get('mapType');
        var constrainResolution = mapType === MapConstants.MAP_TYPES.DISCRETE;

        view.fit(extent, {
            constrainResolution: constrainResolution,
            minResolution: view.getMinResolution(),
            maxResolution: view.getMaxResolution()
        });
        view.setCenter(center);
        document.getElementById(this.id).blur();
    },
    getMaxExtent: function () {
        if (!_.isNil(this.maxExtent)) {
            return this.maxExtent;
        }

        var projection = this.getMap().getView().getProjection();
        if (!_.isNil(projection.getExtent())) {
            return projection.getExtent();
        }

        projection = ol.proj.createProjection(projection.getCode());
        return projection.getExtent();
    },

    CLASS_NAME: 'Descartes.Tool.ZoomToMaximalExtent'
});

module.exports = Class;
