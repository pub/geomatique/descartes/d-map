/* global MODE */
var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var Tool = require('./' + MODE + '/Tool');

require('./css/ZoomOut.css');

/**
 * Class: Descartes.Tool.ZoomOut
 * Classe définissant un bouton permettant d'effectuer un zoom arrière par simple clic sur la carte.
 *
 * Hérite de:
 *  - <Descartes.Tool>
 */
var Class = Utils.Class(Tool, {
    /**
     * Constructeur: Descartes.Tool.ZoomOut
     * Constructeur d'instances
     */
    initialize: function (options) {
        Tool.prototype.initialize.apply(this, arguments);
        _.extend(this, options);
    },
    /**
     * Méthod: handleClick
     * Effectue le zoom arrière
     */
    handleClick: function () {
        var map = this.getMap();
        var view = map.getView();
        var zoom = view.getZoom();
        view.animate({
            zoom: zoom - 1,
            duration: this.duration
        });
        document.getElementById(this.id).blur();
    },
    CLASS_NAME: 'Descartes.Tool.ZoomOut'
});

module.exports = Class;
