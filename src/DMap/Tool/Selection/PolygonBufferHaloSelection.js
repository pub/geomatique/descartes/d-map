/* global MODE */
var ol = require('openlayers');
var _ = require('lodash');
var jsts = require('jsts');

var Utils = require('../../Utils/DescartesUtils');
var AbstractBufferHaloSelection = require('./AbstractBufferHaloSelection');

require('./css/PolygonBufferHaloSelection.css');

/**
 * Class: Descartes.Tool.Selection.PolygonBufferHaloSelection
 * Classe définissant un bouton permettant la sélection d'objets par dessin d'un polygone quelconque sur la carte.
 *
 * Hérite de:
 *  - <Descartes.Tool.Selection.AbstractBufferHaloSelection>
 */
var Class = Utils.Class(AbstractBufferHaloSelection, {

    /**
     * Constructeur: Descartes.Tool.Selection.PolygonBufferHaloSelection
     * Constructeur d'instances
     */
    initialize: function (options) {

        AbstractBufferHaloSelection.prototype.initialize.apply(this, arguments);

        this.interaction = new ol.interaction.Draw({
            type: 'Polygon',
            source: this.getSource(),
            style: this.styleFunction
        });
        this.interaction.set('id', 'PolygonBufferHaloSelection_' + Utils.createUniqueID());
        this.interaction.setActive(false);
        this.interaction.on('drawend', this.onDrawEnd.bind(this), this);
    },

    CLASS_NAME: 'Descartes.Tool.Selection.PolygonBufferHaloSelection'
});

module.exports = Class;
