var ol = require('openlayers');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var FeatureSelection = require('./FeatureSelection');

require('./css/CircleSelection.css');

/**
 * Class: Descartes.Tool.Selection.CircleSelection
 * Classe définissant un bouton permettant la sélection d'objets par dessin d'un cercle sur la carte.
 *
 * Hérite de:
 *  - <Descartes.Tool.Selection.FeatureSelection>
 */
var Class = Utils.Class(FeatureSelection, {

    /**
     * Propriete: infoRadius
     * {Boolean} afficher ou non l'information sur le rayon.
     */
    infoRadius: false,

    /**
     * Constructeur: Descartes.Tool.Selection.CircleSelection
     * Constructeur d'instances
     */
    initialize: function (options) {

        if (!_.isNil(options)) {
            this.infoRadius = options.infoRadius || false;
            delete options.infoRadius;
        }

        FeatureSelection.prototype.initialize.apply(this, arguments);

        this.interaction = new ol.interaction.Draw({
            type: 'Circle',
            geometryFunction: ol.interaction.Draw.createRegularPolygon(100),
            source: this.getSource(),
            style: this.styleFunction
        });
        this.interaction.setActive(false);
        this.interaction.on('drawend', this.onDrawEnd.bind(this), this);
    },

    onDrawEnd: function (event) {
        if (this.persist) {
            this.getSource().clear();
            this.source.addFeature(event.feature);
        }
        this.executeQuery(event.feature);
    },

    /**
     * Methode: showInfo
     * Affiche le rayon.
     */
    showInfo: function () {
        if (this.persist && this.infoRadius) {
            this.removeOverlays();

            var features = this.source.getFeatures();
            var geom = features[0].getGeometry();
            var radius = Math.sqrt(geom.getArea() / Math.PI);

            var out = this.getMessage('INFO_RADIUS') + Utils.adaptUnits(radius);

            var div = document.createElement('div');
            div.className = 'Descartes-overlay-tooltip Descartes-tooltip-selection';
            div.innerHTML = out;
            this.nbOverlay++;

            var extent = geom.getExtent();
            var center = ol.extent.getCenter(extent);

            var infoOverlay = new ol.Overlay({
                id: this.id + '_' + this.nbOverlay,
                element: div,
                position: center,
                positioning: 'bottom-center'
            });
            this.getMap().addOverlay(infoOverlay);
        }
    },
    CLASS_NAME: 'Descartes.Tool.Selection.CircleSelection'
});

module.exports = Class;
