/* global MODE, Descartes */
var ol = require('openlayers');
var $ = require('jquery');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var FeatureSelection = require('./FeatureSelection');
var ModalFormDialog = require('../../UI/' + MODE + '/ModalFormDialog');
var BufferInputDialog = require('../../UI/' + MODE + '/templates/BufferInputDialog.ejs');

require('./css/PointRadiusSelection.css');

/**
 * Class: Descartes.Tool.Selection.PointRadiusSelection
 * Classe définissant un bouton permettant la sélection d'objets par construction d'un cercle sur la carte.
 *
 * :
 * La construction du cercle consiste à cliquer simplement sur le futur centre du cercle, puis à saisir le rayon en mètres souhaité.
 *
 * Hérite de:
 *  - <Descartes.Tool.Selection.FeatureSelection>
 */
var Class = Utils.Class(FeatureSelection, {

    /**
     * Propriete: radius
     * {Float} le rayon demandé.
     */
    radius: 0,

    /**
     * Propriete: infoRadius
     * {Boolean} afficher ou non l'information sur le rayon.
     */
    infoRadius: false,

    /**
     * Propriete: exportBuffer
     * {Boolean} afficher ou non le bouton d'export de buffer.
     */
    exportBuffer: false,

    /**
     * Constructeur: Descartes.Tool.Selection.PointRadiusSelection
     * Constructeur d'instances
     */
    initialize: function (options) {
        if (!_.isNil(options)) {
            this.infoRadius = options.infoRadius || false;
            this.exportBuffer = options.exportBuffer || false;
            delete options.infoRadius;
            delete options.exportBuffer;
        }
        FeatureSelection.prototype.initialize.apply(this, arguments);
        this.interaction = new ol.interaction.Draw({
            type: 'Point',
            source: this.getSource(),
            style: this.styleFunction
        });
        this.interaction.set('id', 'PointRadiusSelection_' + Utils.createUniqueID());
        this.interaction.setActive(false);
        this.interaction.on('drawend', this.onEnd.bind(this), this);
    },

    /**
     * Methode: onEnd
     * Construit la géométrie du cercle de sélection et exécute la méthode <Descartes.Tool.Selection.executeQueryAction>.
     *
     */
    onEnd: function (event) {

        this.feature = event.feature;

        var content = BufferInputDialog({
            id: this.id,
            bufferLabel: this.getMessage('BUFFER_LABEL'),
            isHalo: false,
            bufferValue: this.value
        });

        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('TITLE_MESSAGE'),
            formClass: 'form-horizontal',
            sendLabel: this.getMessage('OK_BUTTON'),
            content: content
        });
        var done = this.done.bind(this);
        var cancel = this.cancel.bind(this);
        dialog.open(done, cancel);
    },

    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'createObjet',
     * ou affiche les messages d'erreur rencontrés.
     */
    done: function (result) {
        this.result = result;
        if (this.validateDatas(result)) {
            if (this.radius !== null) {
                this.radius = parseFloat(this.radius, 10);

                if (!isNaN(this.radius)) {

                    if (this.persist) {
                        this.source.clear();
                        this.source.addFeature(this.feature);
                    }

                    var projection = this.getMap().getView().getProjection();
                    var units = projection.getUnits();
                    var coordinate = this.feature.getGeometry().getCoordinates();
                    var feature;
                    var geom;
                    if (units === 'm') {
                        geom = new ol.geom.Circle(coordinate, this.radius / (ol.proj.getPointResolution(projection, 1, ol.extent.getCenter(this.feature.getGeometry().getExtent()), units) * projection.getMetersPerUnit()));
                        feature = new ol.Feature(geom);
                    } else if (units === 'degrees') {
                        geom = new ol.geom.Circle(ol.proj.transform(coordinate, projection, 'EPSG:2154'), this.radius);
                        feature = new ol.Feature(geom.clone().transform('EPSG:2154', projection));
                    }

                    if (this.persist) {
                        this.source.addFeature(feature);
                    }

                    //circle n'est pas compatible avec une interrogation, il faut convertir en polygon
                    var geometryToQuery = ol.geom.Polygon.fromCircle(feature.getGeometry(), 100);
                    feature.setGeometry(geometryToQuery);

                    this.executeQuery(feature);
                } else {
                    alert(this.getMessage('FORMAT_MESSAGE'));
                    this.effacerGeometrySelection();
                }
            }
        } else {
            this.showErrors();
            this.effacerGeometrySelection();
        }
    },
    cancel: function () {
        this.effacerGeometrySelection();
    },
    /**
     * Methode: validateDatas
     * Effectue les contrôles de surface après la saisie.
     *
     * Met à jour le modèle si ceux-ci sont assurés.
     *
     * Alimente la liste des erreurs dans le cas contraire.
     */
    validateDatas: function (result) {
        this.errors = [];

        this.radius = Number(result.buffer);

        if (!_.isNumber(this.radius)) {
            this.errors.push(this.getMessage('FORMAT_BUFFER_ERROR'));
        }

        return (this.errors.length === 0);
    },
    /**
     * Methode: showErrors
     * Affiche la liste des erreurs rencontrés lors des contrôles de surface.
     */
    showErrors: function () {
        if (this.errors.length !== 0) {
            var errorsMessage = '';
            _.each(this.errors, function (error) {
                errorsMessage += error + '\n';
            });
            alert(errorsMessage);
        }
    },
    /**
     * Methode: showInfo
     * Affiche le rayon.
     */
    showInfo: function () {
        if (this.persist && this.infoRadius) {
            this.removeOverlays();
            var out = this.getMessage('INFO_RADIUS') + Utils.adaptUnits(this.radius);

            var div = document.createElement('div');
            div.className = 'Descartes-overlay-tooltip Descartes-tooltip-selection';
            div.innerHTML = out;

            if (this.exportBuffer) {
                var span = document.createElement('span');
                span.className = 'DescartesBufferExportButton';
                div.appendChild(span);
            }

            this.nbOverlay++;

            var features = this.source.getFeatures();
            var geom = features[0].getGeometry().getExtent();
            var center = ol.extent.getCenter(geom);

            var infoOverlay = new ol.Overlay({
                id: this.id + '_' + this.nbOverlay,
                element: div,
                position: center,
                positioning: 'bottom-center'
            });
            this.getMap().addOverlay(infoOverlay);

            if (this.exportBuffer) {
                var that = this;
                $('.DescartesBufferExportButton').on('click', function () {

                    var dialogListExportFormat = new Descartes.Button.ContentTask.ExportVectorLayer();
                    dialogListExportFormat.title = 'Exporter le buffer';
                    dialogListExportFormat.runExport = function (form) {

                        var selectedFormat = form.format;
                        var Classname = null;
                        var filename = '';
                        for (var i = 0, len = this.listExportFormat.length; i < len; i++) {
                            if (selectedFormat === this.listExportFormat[i].id) {
                                Classname = this.listExportFormat[i].olClass;
                                filename = this.listExportFormat[i].fileName;
                                break;
                            }
                        }

                        if (Classname) {
                            var optionsFormatOutput = {};
                            //if (selectedFormat === "kml") {
                            //flux kml toujours en EPSG:4326 mais pour l'instant, on force aussi pour le GeoJson
                            optionsFormatOutput.featureProjection = that.getMap().getView().getProjection().getCode();
                            optionsFormatOutput.defaultDataProjection = 'EPSG:4326';
                            //}
                            var format = new Classname();
                            var buffer = [features[1]];
                            var flux = format.writeFeatures(buffer, optionsFormatOutput);
                            filename = 'buffer_epsg_4326.' + selectedFormat;
                            Utils.download(filename, flux);
                        }
                    };
                    dialogListExportFormat.execute();

                });

            }
        }

    },
    CLASS_NAME: 'Descartes.Tool.Selection.PointRadiusSelection'
});

module.exports = Class;
