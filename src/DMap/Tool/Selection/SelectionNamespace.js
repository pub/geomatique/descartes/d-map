/* global MODE */
var _ = require('lodash');

var PointSelection = require('./PointSelection');
var PointRadiusSelection = require('./PointRadiusSelection');
var CircleSelection = require('./CircleSelection');
var PolygonSelection = require('./PolygonSelection');
var AbstractBufferHaloSelection = require('./AbstractBufferHaloSelection');
var PolygonBufferHaloSelection = require('./PolygonBufferHaloSelection');
var LineBufferHaloSelection = require('./LineBufferHaloSelection');
var RectangleSelection = require('./RectangleSelection');
var FeatureSelection = require('./FeatureSelection');
var Selection = require('./' + MODE + '/Selection');

var namespace = {
    PointSelection: PointSelection,
    PointRadiusSelection: PointRadiusSelection,
    CircleSelection: CircleSelection,
    PolygonSelection: PolygonSelection,
    AbstractBufferHaloSelection: AbstractBufferHaloSelection,
    PolygonBufferHaloSelection: PolygonBufferHaloSelection,
    LineBufferHaloSelection: LineBufferHaloSelection,
    FeatureSelection: FeatureSelection,
    RectangleSelection: RectangleSelection
};

_.extend(Selection, namespace);

module.exports = Selection;
