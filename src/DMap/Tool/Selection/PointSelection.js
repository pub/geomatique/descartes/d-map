/* global MODE, Descartes */

var ol = require('openlayers');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var Selection = require('./' + MODE + '/Selection');
var ResultLayer = require('../../Model/ResultLayer');
var ResultLayerConstants = require('../../Model/ResultLayerConstants');

var Messages = require('../../Messages');

var ExternalCallsUtils = require('../../Core/ExternalCallsUtils');
var ExternalCallsUtilsConstants = require('../../Core/ExternalCallsUtilsConstants');

require('./css/PointSelection.css');

/**
 * Class: Descartes.Tool.Selection.PointSelection
 * Classe définissant un bouton permettant la sélection d'objets par simple clic sur la carte.
 *
 * Hérite de:
 *  - <Descartes.Tool.Selection>
 */
var Class = Utils.Class(Selection, {

    /**
     * Constructeur: Descartes.Tool.Selection.PointSelection
     * Constructeur d'instances
     */
    initialize: function () {
        Selection.prototype.initialize.apply(this, arguments);

        this.interaction = new ol.interaction.Draw({
            type: 'Point',
            source: this.getSource(),
            style: this.styleFunction
        });
        this.interaction.setActive(false);
        this.interaction.on('drawend', this.executeQuery.bind(this), this);
    },

    /**
     * Methode: executeQuery
     * Récupère la géométrie du point de sélection et exécute la méthode <Descartes.Tool.Selection.executeQueryAction>, après finalisation du OpenLayer.Handler associé.
     *
     */
    executeQuery: function (event) {
        if (this.persist) {
            this.getSource().clear();
        }
        var pixel = Utils.getPixelInMapFromFeature(this.olMap, event.feature);
        var fluxPixel = "#X=" + pixel[0] + "#Y=" + pixel[1];

        if (!_.isNil(this.mapContent)) {
            var layers = this.mapContent.getQueryableLayers().sort(function (a, b) {
                return b.displayOrder - a.displayOrder;
            });

            if (!_.isEmpty(layers)) {

                var fluxInfos = ExternalCallsUtils.writeQueryPostBody(ExternalCallsUtilsConstants.WMS_SERVICE, layers, this.olMap);
                var infosJson = ExternalCallsUtils.writeGetFeatureExportRequestJson(ExternalCallsUtilsConstants.WMS_SERVICE, layers, this.olMap, {fluxPixel: fluxPixel});

                if (fluxInfos !== null) {
                    var requestParams = {
                        distantService: Descartes.FEATUREINFO_SERVER,
                        infos: fluxInfos,
                        pixelMask: fluxPixel,
                        infosJson: infosJson
                    };
                    if (this.resultLayerParams.display) {
                        var tolerance = this.resultLayerParams.pixelTolerance;
                        var pixel1 = [pixel[0] - tolerance, pixel[1] - tolerance];
                        var pixel2 = [pixel[0] + tolerance, pixel[1] + tolerance];
                        var coords1 = this.olMap.getCoordinateFromPixel(pixel1);
                        var coords2 = this.olMap.getCoordinateFromPixel(pixel2);
                        var geom = ol.geom.Polygon.fromExtent([coords1[0], coords1[1], coords2[0], coords2[1]]);
                        var feature = new ol.Feature({geometry: geom});
                        var gml = new ol.format.GML({
                            featureType: 'feature',
                            featureNS: 'http://example.com/feature',
                            srsName: this.olMap.getView().getProjection().getCode()
                        });
                        var nodeGML = gml.writeFeaturesNode([feature]);
                        nodeGML.setAttribute('srsName', "http://www.opengis.net/gml/srs/epsg.xml#" + this.olMap.getView().getProjection().getCode());
                        var fluxGML = new XMLSerializer().serializeToString(nodeGML.getElementsByTagName('Polygon')[0]);

                        ResultLayer.refreshSelections(layers, this.olMap, this.resultLayerParams, fluxGML);

                        if (ResultLayerConstants.blinkLayerTimerId) {
                            clearInterval(ResultLayerConstants.blinkLayerTimerId);
                        }
                        if (ResultLayerConstants.blinkLayerDelayId) {
                            clearTimeout(ResultLayerConstants.blinkLayerDelayId);
                        }
                        ResultLayerConstants.blinkLayerTimerId = setInterval(ResultLayerConstants.blinkLayer, 1000);
                        ResultLayerConstants.blinkLayerDelayId = setTimeout(ResultLayerConstants.stopBlinkLayer, 10000);
                    }
                    this.executeQueryAction(requestParams);
                } else {
                    alert(Messages.Descartes_SEARCH_ERROR_GETFEATUREINFO);
                    this.effacerGeometrySelection();
                }
            } else {
                alert(Messages.Descartes_SEARCH_ERROR_C_INTERROGEABLE);
                this.effacerGeometrySelection();
            }
        }
    },
    CLASS_NAME: 'Descartes.Tool.Selection.PointSelection'
});

module.exports = Class;
