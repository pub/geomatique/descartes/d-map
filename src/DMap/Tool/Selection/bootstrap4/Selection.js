/* global MODE, Descartes*/

var ol = require('openlayers');
var _ = require('lodash');
var $ = require('jquery');

var Utils = require('../../../Utils/DescartesUtils');
var Tool = require('../../../Core/Tool');

var UIConstants = require('../../../UI/UIConstants');
var Symbolizers = require('../../../Symbolizers');

var AjaxSelection = require('../../../Action/AjaxSelection');
var ResultLayer = require('../../../Model/ResultLayer');
var ResultLayerConstants = require('../../../Model/ResultLayerConstants');

var template = require('../../' + MODE + '/templates/Tool.ejs');

/**
 * Class: Descartes.Tool.Selection
 * Classe "abstraite" définissant un bouton permettant la sélection d'objets par interaction graphique sur la carte.
 *
 * Hérite de:
 *  - <Descartes.Tool>
 *
 * Classes dérivées:
 *  - <Descartes.Tool.Selection.PointSelection>
 *  - <Descartes.Tool.Selection.FeatureSelection>
 */
var Class = Utils.Class(Tool, {

    /**
     * Propriete: handlerOptions
     * {Object} Objet JSON stockant les propriétés du OpenLayer.Handler associé.
     *
     * Pour les styles graphiques, construit par défaut à partir de l'objet <Descartes_Symbolizers_Selection>.
     * Peut être surchargé grâce aux options du constructeur.
     */
    handlerOptions: null,

    /**
     * Propriete: defaultResultUiParams
     * {Object} Objet JSON stockant les propriétés par défaut nécessaires à la génération de l'affichage des résultats de la sélection.
     *
     * :
     * type - <Descartes.UI.TYPE_WIDGET>
     * withReturn - false
     * withCsvExport - false
     * withResultLayerExport - false
     * withUIExports - false
     * withAvancedView - false
     * withFilterColumns - false
     * withListResultLayer - false
     * withPagination - false
     * withTabs - false
     */
    defaultResultUiParams: {
        type: UIConstants.TYPE_WIDGET,
        withReturn: false,
        withCsvExport: false,
        withResultLayerExport: false,
        withUIExports: false,
        withAvancedView: false,
        withFilterColumns: false,
        withListResultLayer: false,
        withPagination: false,
        withTabs: false
    },

    /**
     * Propriete: resultUiParams
     * {Object} Objet JSON stockant les propriétés nécessaires à la génération de l'affichage des résultats de la sélection.
     *
     * :
     * Construit à partir de defaultResultUiParams, puis surchargé en préence d'options complémentaires.
     *
     * :
     * type - <Descartes.UI.TYPE_WIDGET> | <Descartes.UI.TYPE_INPLACEDIV> | <Descartes.UI.TYPE_POPUP> : Type de vue pour l'affichage des résultats.
     * view - {<Descartes.UI>} : Classe de la vue si le type est <Descartes.UI.TYPE_WIDGET>
     * div - {DOMElement|String} : Elément DOM de la page accueillant le résultat de la sélection si le type est <Descartes.UI.TYPE_WIDGET> ou <Descartes.UI.TYPE_INPLACEDIV>.
     * format - "JSON"|"HTML"|"XML" : format de retour des informations souhaité.
     * withReturn - {Boolean} : Indique si le résultat doit proposer une localisation rapide sur les objets retournés.
     * withCsvExport - {Boolean} : Indique si le résultat doit proposer une exportation CSV (côté serveur) pour les objets retournés.
     * withResultLayerExport - {Boolean} : Indique si le résultat doit proposer une exportation des objets retournés avec les géométries.
     * withUIExports - {Boolean} : Indique si le résultat doit proposer des exports (côté client) pour les objets retournés.
     * withAvancedView - {Boolean} : Indique si le résultat doit être affiché dans une vue (UI) avancée.
     * withFilterColumns - {Boolean} : Indicateur pour la possibilité de filtrer les colonnes.
     * withListResultLayer - {Boolean} : Indicateur pour la possibilité d'affichage de la liste des couches'.
     * withPagination - {Boolean} : Indicateur pour la possibilité d'afficher la pagination'.
     * withTabs - {Boolean} : Indicateur pour la possibilité d'afficher les onglets'.
     */
    resultUiParams: null,

    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,

    /**
     * Propriete: persist
     * {Boolean} Indicateur pour l'affichage ou non de la selection jusqu'à l'affichage des resutats.
     */
    persist: false,

    btnCss: null,

    defaultResultLayerParams: {
        display: false,
        symbolizers: null,
        exportAllSelectionLayers: false,
        loaderMode: null,
        pixelTolerance: 10
    },
    resultLayerParams: null,
    /**
     * Constructeur: Descartes.Tool.Selection
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * symbolizers - {Object} Objet JSON stockant les styles graphiques du OpenLayer.Handler associé.
     * resultUiParams - {Object} Objet JSON stockant les propriétés nécessaires à la génération de l'affichage des résultats de la sélection.
     */
    initialize: function (options, toolOptions) {
        this.btnCss = Descartes.UIBootstrap4Options.btnCssToolBar;
        this.handlerOptions = {};

        _.merge(options, toolOptions);

        this.symbolizers = _.extend({}, Symbolizers.Descartes_Symbolizers_Selection);
        this.resultUiParams = _.extend({}, this.defaultResultUiParams);
        this.defaultResultLayerParams.symbolizers = _.extend({}, Symbolizers.Descartes_Symbolizers_RequestSLD);
        this.resultLayerParams = _.extend({}, this.defaultResultLayerParams);

        if (!_.isNil(options)) {
            _.extend(this.symbolizers, options.symbolizers);
            delete options.symbolizers;
            _.extend(this.resultUiParams, options.resultUiParams);
            delete options.resultUiParams;
            _.extend(this.resultLayerParams, options.resultLayerParams);
            delete options.resultLayerParams;

            this.persist = options.persist || false;
            delete options.persist;

            this.waitingMsg = options.waitingMsg;
            delete options.waitingMsg;
        }

        var that = this;
        this.olStyles = Symbolizers.getOlStyle(this.symbolizers);
        this.styleFunction = function (feature, resolution) {
            var featureStyleFunction = feature.getStyleFunction();
            if (featureStyleFunction) {
                return featureStyleFunction.call(feature, resolution);
            } else {
                var style = that.olStyles[feature.getGeometry().getType()];
                return style;
            }
        };

        this.source = new ol.source.Vector();
        this.vector = new ol.layer.Vector({
            source: this.source,
            style: this.styleFunction
        });

        Tool.prototype.initialize.apply(this, [options]);
    },
    getSource: function () {
        if (this.persist) {
            return this.vector.getSource();
        }
        return null;
    },
    createElement: function () {
        var elementClass = this.displayClass + 'ItemActive';

        var active = false;
        if (!_.isNil(this.interaction)) {
            active = this.interaction.getActive();
        }
        var element = template({
            id: this.id,
            displayClass: elementClass,
            btnCss: this.btnCss,
            active: active,
            title: this.title
        });
        return element;
    },
    updateElement: function () {
        var elementHtml = this.createElement();
        $('#' + this.id).replaceWith(elementHtml);

        $('#' + this.id).click(function () {
            this.handleClick();
        }.bind(this));
    },
    activate: function () {
        if (!this.interaction.getActive()) {
            var map = this.getMap();
            map.addLayer(this.vector);
            this.source.clear();
            map.addInteraction(this.interaction);
            Tool.prototype.activate.apply(this, arguments);
        }
    },
    deactivate: function () {
        if (this.interaction.getActive()) {
            var map = this.getMap();
            if (!_.isNil(map)) {
                map.removeLayer(this.vector);
                map.removeInteraction(this.interaction);
            }
            this.source.clear();
            this.removeOverlays();
            Tool.prototype.deactivate.apply(this, arguments);
        }
    },
    handleClick: function () {
        if (this.interaction.getActive()) {
            this.deactivate();
        } else {
            this.activate();
        }
    },
    removeOverlays: function () {
        var overlaysToRemove = [];
        this.getMap().getOverlays().forEach(function (overlay) {
            if (_.startsWith(overlay.getId(), this.id)) {
                overlaysToRemove.push(overlay);
            }
        }.bind(this), this);

        for (var i = 0; i < overlaysToRemove.length; i++) {
            this.getMap().removeOverlay(overlaysToRemove[i]);
        }
    },
    /**
     * Methode: executeQueryAction
     * Lance l'appel au service d'interrogation grâce à la classe <Descartes.Action.AjaxSelection>, après finalisation du OpenLayer.Handler associé.
     *
     * Paramètres:
     * requestParams - {Object} Objet JSON stockant les paramètres nécessaires pour la génération de la requête au service d'interrogation.
     */
    executeQueryAction: function (requestParams) {
        if (this.CLASS_NAME === 'Descartes.Tool.Selection.PointRadiusSelection' ||
                this.CLASS_NAME === 'Descartes.Tool.Selection.CircleSelection' ||
                this.CLASS_NAME === 'Descartes.Tool.Selection.PolygonBufferHaloSelection' ||
                this.CLASS_NAME === 'Descartes.Tool.Selection.LineBufferHaloSelection') {
            this.showInfo();
        }
        var attributesAliasSubstitution = {};
        if (!_.isNil(this.mapContent)) {
            var layers = this.mapContent.getQueryableLayers().sort(function (a, b) {
                return b.displayOrder - a.displayOrder;
            });
            for (var i = 0, len = layers.length; i < len; i++) {
                var attributesAlias = layers[i].getAttributesAlias();
                if (!_.isNil(attributesAlias)) {
                    attributesAliasSubstitution[layers[i].id] = attributesAlias;
                }
            };
        }
        var action = new AjaxSelection(this.resultUiParams, requestParams, this.olMap, {waitingMsg: this.waitingMsg, btnDisplaySelectionLayer: this.resultLayerParams.display, attributesAliasSubstitution: attributesAliasSubstitution});
        if (action.renderer) {
            action.renderer.requestParams = requestParams;
            if (this.persist) {
                 action.renderer.events.register('close', this, this.effacerGeometrySelection);
            }
            action.renderer.events.register('close', this, this.effacerResultLayer);
        }
   },
    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au bouton.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
    },

    /**
     * Methode: effacerGeometrySelection
     * Efface la géométrie selection affichée.
     */
    effacerGeometrySelection: function () {
        if (this.getSource()) {
            this.getSource().clear();
        }
        this.removeOverlays();
    },
    /**
     * Methode: effacerResultLayer
     * Efface la couche selection affichée.
     */
    effacerResultLayer: function () {
        if (ResultLayerConstants.blinkLayerTimerId) {
            clearInterval(ResultLayerConstants.blinkLayerTimerId);
            ResultLayerConstants.blinkLayerTimerId = null;
        }
        if (ResultLayerConstants.blinkLayerDelayId) {
            clearTimeout(ResultLayerConstants.blinkLayerDelayId);
            ResultLayerConstants.blinkLayerDelayId = null;
        }
        ResultLayer.refreshSelections(null, this.olMap);
        ResultLayer.removeMarker(this.olMap);
    },
    CLASS_NAME: 'Descartes.Tool.Selection'
});

module.exports = Class;
