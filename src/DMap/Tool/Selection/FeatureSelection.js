/* global MODE, Descartes */

var ol = require('openlayers');
var _ = require('lodash');
var log = require('loglevel');

var Utils = require('../../Utils/DescartesUtils');
var Selection = require('./' + MODE + '/Selection');
var ResultLayer = require('../../Model/ResultLayer');
var ResultLayerConstants = require('../../Model/ResultLayerConstants');

var Messages = require('../../Messages');
var ExternalCallsUtils = require('../../Core/ExternalCallsUtils');
var ExternalCallsUtilsConstants = require('../../Core/ExternalCallsUtilsConstants');
var Symbolizers = require('../../Symbolizers');


/**
 * Class: Descartes.Tool.Selection.FeatureSelection
 * Classe "abstraite" définissant un bouton permettant la sélection d'objets par interaction graphique POLYGONALE sur la carte.
 *
 * Hérite de:
 *  - <Descartes.Tool.Selection>
 *
 * Classes dérivées:
 *  - <Descartes.Tool.Selection.CircleSelection>
 *  - <Descartes.Tool.Selection.PointRadiusSelection>
 *  - <Descartes.Tool.Selection.PolygonSelection>
 *  - <Descartes.Tool.Selection.PolygonBufferHaloSelection>
 *  - <Descartes.Tool.Selection.LineBufferHaloSelection>
 *  - <Descartes.Tool.Selection.RectangleSelection>
 */
var Class = Utils.Class(Selection, {

    interation: null,

    /**
     * Constructeur: Descartes.Tool.Selection.FeatureSelection
     * Constructeur d'instances
     */
    initialize: function () {
        Selection.prototype.initialize.apply(this, arguments);
    },

    /**
     * Methode: executeQuery
     * Récupère la géométrie du polygone de sélection et exécute la méthode <Descartes.Tool.Selection.executeQueryAction>, après finalisation du OpenLayer.Handler associé.
     *
     * Paramètre:
     * feature - {<ol.Feature>} objet fournit par le handler associé.
     */
    executeQuery: function (feature) {
        var gml = new ol.format.GML({
            featureType: 'feature',
            featureNS: 'http://example.com/feature',
            srsName: this.olMap.getView().getProjection().getCode()
        });
        var nodeGML = gml.writeFeaturesNode([feature]);
        nodeGML.setAttribute('srsName', "http://www.opengis.net/gml/srs/epsg.xml#" + this.olMap.getView().getProjection().getCode());
        var fluxGML = new XMLSerializer().serializeToString(nodeGML.getElementsByTagName('Polygon')[0]);
        log.debug('fluxGML=', fluxGML);
        if (!_.isNil(this.mapContent)) {
            var layers = this.mapContent.getQueryableLayers().sort(function (a, b) {
                return b.displayOrder - a.displayOrder;
            });

            if (!_.isEmpty(layers)) {
                var layersWithWFS = [];
                var errorLayersWithoutWFS = '';
                for (var iLayer = 0, len = layers.length; iLayer < len; iLayer++) {
                    var layer = layers[iLayer];
                    var serviceWFS = false;
                    for (var iResource = 0, lenResources = layer.resourceLayers.length; iResource < lenResources; iResource++) {
                        var featureServerParams = layer.resourceLayers[iResource].getFeatureServerParams();
                        if (featureServerParams !== null) {
                            layersWithWFS.push(layer);
                            serviceWFS = true;
                        }
                    }
                    if (!serviceWFS) {
                        errorLayersWithoutWFS += layer.title + '\n';
                    }
                }

                if (layersWithWFS.length === 0) {
                    alert(Messages.Descartes_SEARCH_ERROR_GETFEATURES);
                    this.effacerGeometrySelection();
                } else {
                    if (!_.isEmpty(errorLayersWithoutWFS)) {
                        alert(Messages.Descartes_SEARCH_INFO_C_INTERROGEABLE + errorLayersWithoutWFS);
                    }
                    var fluxInfos = ExternalCallsUtils.writeQueryPostBody(ExternalCallsUtilsConstants.WFS_SERVICE, layersWithWFS, this.olMap);
                    var infosJson = ExternalCallsUtils.writeGetFeatureExportRequestJson(ExternalCallsUtilsConstants.WFS_SERVICE, layersWithWFS, this.olMap, {fluxGML: fluxGML});

                    if (fluxInfos !== null) {
                        var requestParams = {
                            distantService: Descartes.FEATURE_SERVER,
                            infos: fluxInfos,
                            gmlMask: fluxGML,
                            infosJson: infosJson
                        };
                        if (this.resultLayerParams.display) {
                             ResultLayer.refreshSelections(layersWithWFS, this.olMap, this.resultLayerParams, fluxGML);

                             if (ResultLayerConstants.blinkLayerTimerId) {
                                 clearInterval(ResultLayerConstants.blinkLayerTimerId);
                             }
                             if (ResultLayerConstants.blinkLayerDelayId) {
                                 clearTimeout(ResultLayerConstants.blinkLayerDelayId);
                             }
                             ResultLayerConstants.blinkLayerTimerId = setInterval(ResultLayerConstants.blinkLayer, 1000);
                             ResultLayerConstants.blinkLayerDelayId = setTimeout(ResultLayerConstants.stopBlinkLayer, 10000);
                        }
                        this.executeQueryAction(requestParams);
                    } else {
                        alert(Messages.Descartes_SEARCH_ERROR);
                        this.effacerGeometrySelection();
                    }
                }

            } else {
                alert(Messages.Descartes_SEARCH_ERROR_C_INTERROGEABLE);
                this.effacerGeometrySelection();
            }
        }
    },

    CLASS_NAME: 'Descartes.Tool.Selection.FeatureSelection'
});

module.exports = Class;
