/* global MODE, Descartes */
var ol = require('openlayers');
var $ = require('jquery');
var _ = require('lodash');
var jsts = require('jsts');

var Utils = require('../../Utils/DescartesUtils');
var FeatureSelection = require('./FeatureSelection');
var ModalFormDialog = require('../../UI/' + MODE + '/ModalFormDialog');
var BufferInputDialog = require('../../UI/' + MODE + '/templates/BufferInputDialog.ejs');

/**
 * Class: Descartes.Tool.Selection.AbstractBufferHaloSelection
 * Classe définissant un bouton permettant la sélection d'objets par dessin d'un polygone quelconque sur la carte.
 *
 * Hérite de:
 *  - <Descartes.Tool.Selection.FeatureSelection>
 */
var Class = Utils.Class(FeatureSelection, {

    buffer: 0,
    nbPoints: null,
    /**
     * Propriete: configHalo
     * {Boolean} activer ou non la configuration de l'halo (nombre de segment pour l'arc de cercle).
     */
    configHalo: false,

    /**
     * Propriete: infoBuffer
     * {Boolean} afficher ou non l'information sur le buffer.
     */
    infoBuffer: false,

    /**
     * Propriete: exportBuffer
     * {Boolean} afficher ou non le bouton d'export de buffer.
     */
    exportBuffer: false,

    /**
     * Constructeur: Descartes.Tool.Selection.AbstractBufferHaloSelection
     * Constructeur d'instances
     */
    initialize: function (options) {
        if (!_.isNil(options)) {
            this.infoBuffer = options.infoBuffer || false;
            this.configHalo = options.configHalo || false;
            this.exportBuffer = options.exportBuffer || false;
            delete options.infoBuffer;
            delete options.exportBuffer;
        }
        this._parser = new jsts.io.OL3Parser();
        FeatureSelection.prototype.initialize.apply(this, arguments);

    },

    onDrawEnd: function (event) {

        this.feature = event.feature;

        var content = BufferInputDialog({
            id: this.id,
            bufferLabel: this.getMessage('BUFFER_LABEL'),
            nbPointsLabel: this.getMessage('NB_POINTS_LABEL'),
            isHalo: this.configHalo,
            bufferValue: this.value
        });

        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('TITLE_MESSAGE'),
            formClass: 'form-horizontal',
            sendLabel: this.getMessage('OK_BUTTON'),
            content: content
        });
        var done = this.done.bind(this);
        var cancel = this.cancel.bind(this);
        dialog.open(done, cancel);

    },

    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'createObjet',
     * ou affiche les messages d'erreur rencontrés.
     */
    done: function (result) {
        this.result = result;
        if (this.validateDatas(result)) {
            if (this.buffer !== null) {
                this.buffer = parseFloat(this.buffer, 10);
                if (!isNaN(this.buffer)) {
                    if (this.persist) {
                        this.source.clear();
                        this.source.addFeature(this.feature);
                    }
                    var projection = this.getMap().getView().getProjection();
                    var units = projection.getUnits();
                    var jstsGeometry = this._parser.read(this.feature.getGeometry());
                    if (_.isNil(this.nbPoints)) {
                        if (units === 'm') {
                            jstsGeometry = jstsGeometry.buffer(this.buffer / (ol.proj.getPointResolution(projection, 1, ol.extent.getCenter(this.feature.getGeometry().getExtent()), units) * projection.getMetersPerUnit()));
                        } else if (units === 'degrees') {
                            jstsGeometry = this._parser.read(this.feature.getGeometry().clone().transform(projection, 'EPSG:2154'));
                            jstsGeometry = jstsGeometry.buffer(this.buffer);
                        }
                    } else {
                        if (units === 'm') {
                            jstsGeometry = jstsGeometry.buffer(this.buffer / (ol.proj.getPointResolution(projection, 1, ol.extent.getCenter(this.feature.getGeometry().getExtent()), units) * projection.getMetersPerUnit()), this.nbPoints);
                        } else if (units === 'degrees') {
                            jstsGeometry = this._parser.read(this.feature.getGeometry().clone().transform(projection, 'EPSG:2154'));
                            jstsGeometry = jstsGeometry.buffer(this.buffer, this.nbPoints);
                        }
                    }
                    var bufferedGeometry = this._parser.write(jstsGeometry);
                    if (units === 'degrees') {
                        bufferedGeometry = bufferedGeometry.clone().transform('EPSG:2154', projection);
                    }
                    var newFeature = new ol.Feature({
                        geometry: bufferedGeometry
                    });
                    if (this.persist) {
                        this.source.addFeature(newFeature);
                    }
                    this.executeQuery(newFeature);
                } else {
                    alert(this.getMessage('FORMAT_MESSAGE'));
                    this.effacerGeometrySelection();
                }
            }
        } else {
            this.showErrors();
            this.effacerGeometrySelection();
        }
    },
    cancel: function () {
        this.effacerGeometrySelection();
    },
    /**
     * Methode: validateDatas
     * Effectue les contrôles de surface après la saisie.
     *
     * Met à jour le modèle si ceux-ci sont assurés.
     *
     * Alimente la liste des erreurs dans le cas contraire.
     */
    validateDatas: function (result) {
        this.errors = [];

        this.buffer = Number(result.buffer);

        if (result.nbPoints) {
            this.nbPoints = Number(result.nbPoints);
        }

        if (!_.isNumber(this.buffer)) {
            this.errors.push(this.getMessage('FORMAT_BUFFER_ERROR'));
        }
        if (this.configHalo && !_.isNil(this.nbPoints) &&
                this.nbPoints !== '' &&
                !_.isNumber(this.nbPoints)) {
            this.errors.push(this.getMessage('FORMAT_NBPOINTS_ERROR'));
        }

        return (this.errors.length === 0);
    },
    /**
     * Methode: showErrors
     * Affiche la liste des erreurs rencontrés lors des contrôles de surface.
     */
    showErrors: function () {
        if (this.errors.length !== 0) {
            var errorsMessage = '';
            _.each(this.errors, function (error) {
                errorsMessage += error + '\n';
            });
            alert(errorsMessage);
        }
    },
    /**
     * Methode: showInfo
     * Affiche le rayon.
     */
    showInfo: function () {
        if (this.persist && this.infoBuffer) {
            this.removeOverlays();
            var out = this.getMessage('INFO_BUFFER') + Utils.adaptUnits(this.buffer);

            var div = document.createElement('div');
            div.className = 'Descartes-overlay-tooltip Descartes-tooltip-selection';
            div.innerHTML = out;

            if (this.exportBuffer) {
                var span = document.createElement('span');
                span.className = 'DescartesBufferExportButton';
                div.appendChild(span);
            }

            this.nbOverlay++;

            var features = this.source.getFeatures();
            var geom = features[0].getGeometry().getExtent();
            var center = ol.extent.getCenter(geom);

            var infoOverlay = new ol.Overlay({
                id: this.id + '_' + this.nbOverlay,
                element: div,
                position: center,
                positioning: 'bottom-center'
            });
            this.getMap().addOverlay(infoOverlay);

            if (this.exportBuffer) {
                var that = this;
                $('.DescartesBufferExportButton').on('click', function () {

                    var dialogListExportFormat = new Descartes.Button.ContentTask.ExportVectorLayer();
                    dialogListExportFormat.title = 'Exporter le buffer';
                    dialogListExportFormat.runExport = function (form) {

                        var selectedFormat = form.format;
                        var Classname = null;
                        var filename = '';
                        for (var i = 0, len = this.listExportFormat.length; i < len; i++) {
                            if (selectedFormat === this.listExportFormat[i].id) {
                                Classname = this.listExportFormat[i].olClass;
                                filename = this.listExportFormat[i].fileName;
                                break;
                            }
                        }

                        if (Classname) {
                            var optionsFormatOutput = {};
                            //if (selectedFormat === "kml") {
                            //flux kml toujours en EPSG:4326 mais pour l'instant, on force aussi pour le GeoJson
                            optionsFormatOutput.featureProjection = that.getMap().getView().getProjection().getCode();
                            optionsFormatOutput.defaultDataProjection = 'EPSG:4326';
                            //}
                            var format = new Classname();
                            var buffer = [features[1]];
                            var flux = format.writeFeatures(buffer, optionsFormatOutput);
                            filename = 'buffer_epsg_4326.' + selectedFormat;
                            Utils.download(filename, flux);
                        }
                    };
                    dialogListExportFormat.execute();

                });

            }
        }

    },
    CLASS_NAME: 'Descartes.Tool.Selection.AbstractBufferHaloSelection'
});

module.exports = Class;
