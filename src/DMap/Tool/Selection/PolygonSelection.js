var ol = require('openlayers');

var Utils = require('../../Utils/DescartesUtils');
var FeatureSelection = require('./FeatureSelection');

require('./css/PolygonSelection.css');

/**
 * Class: Descartes.Tool.Selection.PolygonSelection
 * Classe définissant un bouton permettant la sélection d'objets par dessin d'un polygone quelconque sur la carte.
 *
 * Hérite de:
 *  - <Descartes.Tool.Selection.FeatureSelection>
 */
var Class = Utils.Class(FeatureSelection, {

    /**
     * Constructeur: Descartes.Tool.Selection.CircleSelection
     * Constructeur d'instances
     */
    initialize: function () {
        FeatureSelection.prototype.initialize.apply(this, arguments);

        this.interaction = new ol.interaction.Draw({
            type: 'Polygon',
            source: this.getSource(),
            style: this.styleFunction
        });
        this.interaction.set('id', 'PolygonSelection_' + Utils.createUniqueID());
        this.interaction.setActive(false);
        this.interaction.on('drawend', this.onDrawEnd.bind(this), this);
    },

    onDrawEnd: function (event) {
        if (this.persist) {
            this.getSource().clear();
        }
        this.executeQuery(event.feature);
    },

    CLASS_NAME: 'Descartes.Tool.Selection.PolygonSelection'
});

module.exports = Class;
