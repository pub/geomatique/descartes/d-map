/* global MODE */
var _ = require('lodash');
var ol = require('openlayers');

var Utils = require('../Utils/DescartesUtils');
var Tool = require('./' + MODE + '/Tool');
var MapConstants = require('../Map/MapConstants');

require('./css/ZoomIn.css');

/**
 * Class: Descartes.Tool.ZoomIn
 * Classe définissant un bouton permettant d'effectuer un zoom avant par simple clic ou dessin d'un rectangle sur la carte.
 *
 * Hérite de:
 *  - <Descartes.Tool>
 */
var ZoomIn = Utils.Class(Tool, {
    /**
     * Constructeur: Descartes.Tool.ZoomIn
     * Constructeur d'instances
     */
    initialize: function (options) {
        Tool.prototype.initialize.apply(this, arguments);
        _.extend(this, options);
    },
    /**
     * initialisation de l'interaction en fonction du type de carte.
     */
    handleClick: function () {
        if (_.isNil(this.interaction)) {
            var mapType = this.getMap().get('mapType');
            if (mapType === MapConstants.MAP_TYPES.CONTINUOUS) {
                var that = this;
                this.interaction = new ol.interaction.DragBox({
                    condition: this.condition || ol.events.condition.always,
                    className: this.className || 'ol-dragzoom'
                });
                this.interaction.on('boxend', function () {
                    this.getMap().getView().fit(this.getGeometry(), {
                        size: this.getMap().getSize(),
                        duration: that.duration || 200,
                        constrainResolution: false,
                        minResolution: this.getMap().getView().getMinResolution(),
                        maxResolution: this.getMap().getView().getMaxResolution()
                    });
                });
            } else {
                this.interaction = new ol.interaction.DragZoom({
                    condition: this.condition || ol.events.condition.always,
                    duration: this.duration || 200,
                    className: this.className || 'ol-dragzoom'
                });
            }
            this.interaction.setActive(false);
        }
        Tool.prototype.handleClick.apply(this, arguments);
    },
    CLASS_NAME: 'Descartes.Tool.ZoomIn'
});

module.exports = ZoomIn;
