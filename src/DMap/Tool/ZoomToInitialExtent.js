/* global MODE */

var _ = require('lodash');
var logger = require('loglevel');

var Utils = require('../Utils/DescartesUtils');
var Tool = require('./' + MODE + '/Tool');
var MapConstants = require('../Map/MapConstants');

require('./css/ZoomToInitialExtent.css');


/**
 * Class: Descartes.Tool.ZoomToInitialExtent
 * Classe définissant un bouton permettant de recadrer la carte sur son emprise initiale.
 *
 * Hérite de:
 *  - <Descartes.Button>
 */
var Class = Utils.Class(Tool, {

    /**
     * Propriete: initBounds
     * {Array} Emprise initiale de la carte.
     */
    initBounds: null,
   /**
	 * Constructeur: Descartes.Button.ZoomToInitialExtent
	 * Constructeur d'instances
	 *
	 * Paramètres:
	 * bounds - {Array} Emprise initiale de la carte.
	 * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        if (_.isArray(options.bounds)) {
            this.initBounds = options.bounds;
        } else {
            this.initBounds = _.values(options.bounds);
        }
        if (!_.isNil(options) && !_.isNil(options.tooloptions) && !_.isNil(options.tooloptions.title)) {
            options = _.extend(options, options.tooloptions);
            delete options.tooloptions;
        }
        Tool.prototype.initialize.apply(this, arguments);
    },
    /**
     * Methode: handleClick
     * Méthode appeler lorsque l'utilisateur clique sur l'outil.
     */
    handleClick: function () {
        var view = this.getMap().getView();
        var extent = null;

        if (!_.isNil(this.initBounds)) {
            extent = this.initBounds;
        } else {
            logger.warn('Pas d\'emprise initiale');
            extent = view.getProjection().getExtent();
        }
        var mapType = this.getMap().get('mapType');
        var constrainResolution = mapType === MapConstants.MAP_TYPES.DISCRETE;
        view.fit(extent, {
            constrainResolution: constrainResolution,
            minResolution: view.getMinResolution(),
            maxResolution: view.getMaxResolution()
        });
        document.getElementById(this.id).blur();
    },
    CLASS_NAME: 'Descartes.Tool.ZoomToInitialExtent'
});

module.exports = Class;
