/* global MODE */

var ol = require('openlayers');

var Utils = require('../Utils/DescartesUtils');
var Tool = require('./' + MODE + '/Tool');

require('./css/DragPan.css');

/**
 * Class: Descartes.Tool.DragPan
 * Classe définissant un bouton permettant de recentrer la carte suite à un clic du pointeur sur celle-ci.
 *
 * Hérite de:
 *  - <Descartes.Tool>
 */
var Class = Utils.Class(Tool, {

    /**
     * Constructeur: Descartes.Tool.CenterMap
     * Constructeur d'instances
     */
    initialize: function () {
        Tool.prototype.initialize.apply(this, arguments);
        this.interaction = new ol.interaction.DragPan();
        this.interaction.setActive(false);
    },
    CLASS_NAME: 'Descartes.Tool.DragPan'
});

module.exports = Class;
