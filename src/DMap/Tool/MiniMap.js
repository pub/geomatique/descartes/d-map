require('./css/MiniMap.css');

var ol = require('openlayers');
var _ = require('lodash');

var I18N = require('../Core/I18N');
var Utils = require('../Utils/DescartesUtils');
var Size = require('../Utils/DescartesSize');

/**
 * Class: Descartes.Tool.MiniMap
 * Classe définissant une mini-carte de navigation liée à la carte principlae.
 *
 * Hérite de:
 *  - <Descartes.I18N>
 */
var MiniMap = Utils.Class(I18N, {

    defaultDisplayClasses: {
        globalClassName: 'DescartesMiniMap'
    },

    /**
     * Propriete: defaultOverviewParams
     * {Object} Objet JSON stockant les paramètres optionnels de configuration par défaut de la mini-carte.
     *
     * size - Taille de la mini-carte ("OpenLayers.Size(150,90)" par défaut).
     */
    defaultOverviewParams: {
        size: new Size(150, 150)
    },

    /**
     * Propriete: size
     * {Descartes.Size} Taille de la mini-carte
     */
    size: null,

    /**
     * Propriete: open
     * {Boolean} Indique si la mini-carte est "ouverte" lors de l'affichage initial de la carte.
     */
    open: true,

    /**
     * Propriete: isInitExtentWorld
     * {Boolean} Indique si l'emprise initiale de la carte est une emprise mondiale.
     */
    isInitExtentWorld: false,

    /**
     * Propriete: default
     */
    version: '1.1.1',

    maxExtent: null,

    /**
     * Constructeur: Descartes.Tool.MiniMap
     * Constructeur d'instances
     *
     * Paramètres:
     * olMap - {ol.Map} Carte OpenLayers liée à la mini-carte.
     * resourceUrl - {String} URL de la ressource WMS alimentant la mini-carte
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * open - {Boolean} Indique si la mini-carte est "ouverte" lors de l'affichage initial de la carte.
     * messages - {Object} Objet JSON stockant les messages selon la forme :
     * {
     *  use:"Instructions",
     *  maximize:"Info-bulle pour ouvrir la mini-carte",
     *  minimize:"Info-bulle pour fermer la mini-carte"
     * }
     * size - {Descartes.Size} Taille de la mini-carte
     */
    initialize: function (olMap, resourceUrl, options) {
        I18N.prototype.initialize.call(this);

        var displayClasses = _.extend({}, this.defaultDisplayClasses);
        var messages = {
            title: this.getMessage('TITLE_MESSAGE')
        };

        if (_.isNil(options)) {
            options = {};
        } else {
            _.extend(displayClasses, options.displayClasses);
            delete options.displayClasses;
            _.extend(messages, options.messages);
            delete options.messages;
        }
        _.extend(this, this.defaultOverviewParams);
        _.extend(this, options);

        var params = {};

        var version = Utils.getUrlParam(resourceUrl, 'VERSION');
        if (!_.isEmpty(version)) {
            params.VERSION = version;
            resourceUrl = Utils.removeUrlParam(resourceUrl, 'VERSION');
        } else if (options.version) {
            params.VERSION = options.version;
        } else {
            params.VERSION = this.version;
        }

        this.createCssSizeRule();

        this.control = new ol.control.OverviewMap({
            collapsed: !options.open,
            collapseLabel: '\u00BB',
            label: '\u00AB',
            layers: [
                new ol.layer.Image({
                    source: new ol.source.ImageWMS({
                        url: resourceUrl,
                        params: params
                    })
                })],
            tipLabel: messages.title,
            view: new ol.View({
                center: olMap.getView().getCenter(),
                projection: olMap.getView().getProjection()
            }),
            className: 'ol-overviewmap ' + this.defaultDisplayClasses.globalClassName
        });

        //Ne pas permettre le déplacement depuis la minimap
        //car elle ne respecte pas l'emprise maximum de la carte.
        var boxOverlay = this.control.boxOverlay_;
        var element = boxOverlay.getElement();
        var clone = element.cloneNode(true);
        clone.style.cursor = '';

        this.onMoveListener = this.onMove.bind(this);
        this.onEndMovingListener = this.onEndMoving.bind(this);

        clone.addEventListener('mousedown', function () {
            this.initialOverlayPosition = this.control.boxOverlay_.getPosition();
            window.addEventListener('mousemove', this.onMoveListener);
            window.addEventListener('mouseup', this.onEndMovingListener);
        }.bind(this));

        boxOverlay.setElement(clone);

        olMap.addControl(this.control);
        this.olMap = olMap;

        if (!_.isNil(this.olMap.getView().options_.extent)) {
            this.maxExtent = this.olMap.getView().options_.extent;
        }
    },
    /**
     * Méthode createCssSizeRule
     * Permet de customiser l'overview à partir de la configuration de la minimap.
     */
    createCssSizeRule: function () {
        var style = document.createElement('style');
        style.appendChild(document.createTextNode(''));

        document.head.appendChild(style);
        Utils.addCSSRule(style.sheet, '.' + this.defaultDisplayClasses.globalClassName + ' .ol-overviewmap-map', 'width: ' + this.size.w + 'px; height: ' + this.size.h + 'px;', 0);
    },
    onMove: function (event) {
        var coordinates = this.control.getOverviewMap().getEventCoordinate(this.computeDesiredMousePosition(event));
        this.control.boxOverlay_.setPosition(coordinates);
    },
    onEndMoving: function (event) {
        var coordinates = this.control.getOverviewMap().getEventCoordinate(event);

        var canMove = true;
        if (!_.isNil(this.maxExtent)) {
            canMove = ol.extent.containsXY(this.maxExtent, coordinates[0], coordinates[1]);
        }
        if (canMove) {
            this.olMap.getView().setCenter(coordinates);
        } else {
            this.control.boxOverlay_.setPosition(this.initialOverlayPosition);
        }

        window.removeEventListener('mousemove', this.onMoveListener);
        window.removeEventListener('mouseup', this.onEndMovingListener);
    },
    computeDesiredMousePosition: function (mousePosition) {
        return {
            clientX: mousePosition.clientX - (this.control.boxOverlay_.getElement().offsetWidth / 2),
            clientY: mousePosition.clientY + (this.control.boxOverlay_.getElement().offsetHeight / 2)
        };
    },

    CLASS_NAME: 'Descartes.Tool.MiniMap'
});

module.exports = MiniMap;
