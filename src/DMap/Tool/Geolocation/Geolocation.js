/* global MODE Descartes*/

var ol = require('openlayers');
var _ = require('lodash');
var log = require('loglevel');
var $ = require('jquery');
var Utils = require('../../Utils/DescartesUtils');
var Tool = require('../' + MODE + '/Tool');
var FormDialog = require('../../UI/' + MODE + '/FormDialog');

var template = require('../' + MODE + '/templates/Tool.ejs');

/**
 * Class: Descartes.Tool.Geolocation
 * Classe "abstraite" définissant un bouton permettant de se geolocaliser.
 *
 * Hérite de:
 *  - <Descartes.Tool>
 */
var Class = Utils.Class(Tool, {

    active: false,

    /**
     * Propriete: projection
     * {Boolean} Indicateur pour la présence du nom de la projection à l'affichage (false par défaut).
     */
    projection: false,
    /**
     * Propriété: displayProjectionsList
     * {Array} Tableau définissant les projections des coordonnées à saisir.
     */
    displayProjections: [],
    /**
     * Propriété: selectedDisplayProjectionIndex
     * {Integer} Position de la projection sélectionnée
     */
    selectedDisplayProjectionIndex: 0,
    _selectedDisplayProjection: "",
    _mapProjection: null,
    /**
     * Propriete: displayInfos
     * {Boolean} Indicateur pour l'affichage ou non de la popup d'informations (true par défaut).
     */
    displayInfos: true,
    /**
     * Propriete: infos
     * {Object} informations supplémentaires à afficher dans la popup d'information'.
     */
    infos: {
       accuracy: true,
       altitude: true,
       altitudeAccuracy: true,
       heading: true,
       speed: true,
       projection: true
    },
    /**
     * Propriete: displayExportInfos
     * {Boolean} Indicateur pour l'affichage ou non du bouton d'export des informations (true par défaut).
     */
    displayExportInfos: true,
    _precisionFixed: 3,
    /**
     * Constructeur: Descartes.Tool.Geolocation
     * Constructeur d'instances
     */
    initialize: function (options) {
        Tool.prototype.initialize.apply(this, arguments);

        if (!_.isNil(options)) {
            if (!_.isNil(options.projection)) {
                this.projection = options.projection;
                delete options.projection;
            }
            if (!_.isNil(options.displayProjections)) {
                this.displayProjections = options.displayProjections;
                delete options.displayProjections;
            }
            if (!_.isNil(options.selectedDisplayProjectionIndex)) {
                this.selectedDisplayProjectionIndex = options.selectedDisplayProjectionIndex;
                delete options.selectedDisplayProjectionIndex;
            }
            if (!_.isNil(options.displayInfos)) {
                this.displayInfos = options.displayInfos;
                delete options.displayInfos;
            }
            if (!_.isNil(options.displayExportInfos)) {
                this.displayExportInfos = options.displayExportInfos;
                delete options.displayExportInfos;
            }
            if (!_.isNil(options.infos)) {
                _.extend(this.infos, options.infos);
                delete options.infos;
            }
        }

        if (!(this.projection && this.displayProjections.length > 0)) {
            this.displayProjections = [];
        }

    },

    isActive: function () {
        return this.active;
    },

    createModalDialog: function () {

        var close = this.close.bind(this);

        this._mapProjection = this.olMap.getView().getProjection();

        if (this.displayProjections.length === 0) {
            this.displayProjections.push(this._mapProjection.getCode());
            this.selectedDisplayProjectionIndex = 0;
        }

        this._selectedDisplayProjection = this.displayProjections[this.selectedDisplayProjectionIndex];


        var projectionsCodesValues = [];
        if (this.projection && this.displayProjections.length > 0) {
           for (var j = 0, jlen = this.displayProjections.length; j < jlen; j++) {
               projectionsCodesValues.push({code: this.displayProjections[j], value: Utils.getProjectionName(this.displayProjections[j])});
           }
        }

        var content = "<div id=\"geolocationinfo\" style=\"display: none;\"></div>";
        content += "<div id=\"DescartesInfosGeolocation\" class=\"DescartesDialogContent2\">";
        if (this.projection) {
            if (projectionsCodesValues.length === 1) {
                content += "Projection : " + projectionsCodesValues[0].value + "&nbsp;&nbsp;</br>" ;
            } else {
                //content += "Projection : <select class=\"DescartesUISelect form-control\" name=\"geolocation_input_proj\" id=\"geolocation_input_proj\">";
                content += "Projection : <select name=\"geolocation_input_proj\" id=\"geolocation_input_proj\">";
                for (var i = 0, len = projectionsCodesValues.length; i < len; i++) {
                    content += "<option value=\"" + projectionsCodesValues[i].code + "\"";
                    if (this.selectedDisplayProjectionIndex === i) {
                        content += "selected>" + projectionsCodesValues[i].value + "</option>";
                    } else {
                        content += ">" + projectionsCodesValues[i].value + "</option>";
                    }
                }
                content += "</select></br>";
            }
            //content += "Projection : " + Utils.getProjectionName("EPSG:4326") + "&nbsp;&nbsp;" ;
        }
        content += "Position : <code id=\"position\"></code>&nbsp;&nbsp;";
        if (this.infos.accuracy) {
            content += "</br>Précision de la position : <code id=\"accuracy\"></code>&nbsp;&nbsp;";
        }
        if (this.infos.altitude) {
            content += "</br>Altitude : <code id=\"altitude\"></code>&nbsp;&nbsp;";
        }
        if (this.infos.altitudeAccuracy) {
            content += "</br>Précision de l'altitude : <code id=\"altitudeAccuracy\"></code>&nbsp;&nbsp;";
        }
        if (this.infos.heading) {
            content += "</br>Angle : <code id=\"heading\"></code>&nbsp;&nbsp;";
        }
        if (this.infos.speed) {
            content += "</br>Vitesse : <code id=\"speed\"></code>";
        }
        content += "</div>";
        if (this.displayExportInfos) {
            content += "<button id='exportInfos' name='exportInfos' type='button' class='DescartesUIButtonSubmit btn btn-outline-dark' >Exporter la position courante</button>";
        }
        var formDialogOptions = {
                id: 'DescartesInfosGeolocation_dialog',
                title: this.getMessage('TITLE_POPUP'),
                size: 'modal-lg',
                sendLabel: 'Fermer',
                otherBtnLabel: '',
                content: content
        };
        this.formDialog = new FormDialog(formDialogOptions);

        this.formDialog.open(close, close);

        var formSelector = '#' + this.formDialog.id + '_formDialog';

        var formDialogDiv = $(formSelector);
        formDialogDiv.parent().parent()[0].className = formDialogDiv.parent().parent()[0].className + ' geolocationdialog';

        if (this.displayExportInfos) {
            var container = document.getElementById(this.formDialog.id + '_formDialog');
            container.querySelector("button[name='exportInfos']")
            .addEventListener('click', event => {
                event.preventDefault();
                this.exportInfos();
            });
        }
        var that = this;
        $('#geolocation_input_proj').change(function (event) {
            var selectedValue = $('#geolocation_input_proj').val();
            document.getElementById('position').innerText = that.adaptCoords(that.geolocation.getPosition(), that._mapProjection.getCode(), selectedValue);
            that._selectedDisplayProjection = selectedValue;
        });
    },

    adaptCoords: function (position, displayProjection, selectProjection) {
        var coordxy = ol.proj.transform(position, displayProjection, selectProjection);
        var digits = parseInt(Utils.getProjectionPrecision(selectProjection), 10);
        coordxy[0] = Number(coordxy[0].toFixed(digits));
        coordxy[1] = Number(coordxy[1].toFixed(digits));
        return coordxy;
    },

    adaptInfo: function (value, unit, trunc) {
        var display = this.getMessage('UNDEFINED_LABEL');
        if (value !== undefined) {
           if (trunc) {
               display = value.toFixed(this._precisionFixed) + " " + unit;
           } else {
               display = value + " " + unit;
           }
        }
        return display;
    },

    adaptInfoHeading: function (value, unit, trunc) {
        var display = this.getMessage('UNDEFINED_LABEL');
        if (value !== null && value !== undefined) {
           //convert radian to degree
           value = value * (180 / Math.PI);
           display = this.adaptInfo(value, unit, trunc);
        }
        return display;
    },

    adaptInfoSpeed: function (value, unit, trunc) {
        var display = this.getMessage('UNDEFINED_LABEL');
        if (value !== null && value !== undefined) {
            //convert m/s to km/h
            value = 3.6 * value;
            display = this.adaptInfo(value, unit, trunc);
        }
        return display;
    },

    exportInfos: function () {
        this.formDialog = null;

        var that = this;
        var dialogListExportFormat = new Descartes.Button.ContentTask.ExportVectorLayer();
        dialogListExportFormat.listExportFormat.unshift({
          id: "gpx", label: 'GPX', olClass: ol.format.GPX
        });
        dialogListExportFormat.title = this.getMessage('TITLE_EXPORT_POPUP');
        dialogListExportFormat.runExport = function (form) {

            var selectedFormat = form.format;
            var Classname = null;
            var filename = '';
            for (var i = 0, len = this.listExportFormat.length; i < len; i++) {
                if (selectedFormat === this.listExportFormat[i].id) {
                    Classname = this.listExportFormat[i].olClass;
                    filename = this.listExportFormat[i].fileName;
                    break;
                }
            }

            if (Classname) {
                var optionsFormatOutput = {};
                //if (selectedFormat === "kml") {
                //flux kml toujours en EPSG:4326 mais pour l'instant, on force aussi pour le GeoJson
                optionsFormatOutput.featureProjection = that.getMap().getView().getProjection().getCode();
                optionsFormatOutput.defaultDataProjection = 'EPSG:4326';
                //}
                var format = new Classname();
                var featureParams = {};
                if (that.infos.accuracy) {
                    featureParams.accuracy = that.adaptInfo(that.geolocation.getAccuracy(), '[m]', true);
                }
                if (that.infos.altitude) {
                    featureParams.altitude = that.adaptInfo(that.geolocation.getAltitude(), '[m]', true);
                }
                if (that.infos.altitudeAccuracy) {
                    featureParams.altitudeAccuracy = that.adaptInfo(that.geolocation.getAltitudeAccuracy(), '[m]', true);
                }
                if (that.infos.heading) {
                    featureParams.heading = that.adaptInfoHeading(that.geolocation.getHeading(), '[degré]', true);
                }
                if (that.infos.speed) {
                    featureParams.speed = that.adaptInfoSpeed(that.geolocation.getSpeed(), ' [km/h]', true);
                }
                if (that.geolocation.getPosition()) {
                    featureParams.geometry = new ol.geom.Point(that.geolocation.getPosition());
                    featureParams.ele = that.geolocation.get('altitude');
                    featureParams.time = Date.now() / 1000;
                    var feature = new ol.Feature(featureParams, 'XYZM');
                    var flux = format.writeFeatures([feature], optionsFormatOutput);
                    flux = flux.replace('creator="OpenLayers"', 'creator="Descartes"');
                    filename = 'export_current_position_epsg_4326.' + selectedFormat;
                    Utils.download(filename, flux);
                } else {
                    alert(that.getMessage('ERROR_MSG'));
                }
            }
            dialogListExportFormat.listExportFormat.shift({
              id: "gpx", label: 'GPX', olClass: ol.format.GPX
            });
        };
        dialogListExportFormat.close = function () {
			dialogListExportFormat.listExportFormat.shift({
              id: "gpx", label: 'GPX', olClass: ol.format.GPX
            });
        };
        dialogListExportFormat.execute();
    },

    close: function () {
        this.formDialog = null;
    },

    CLASS_NAME: 'Descartes.Tool.Geolocation'
});

module.exports = Class;
