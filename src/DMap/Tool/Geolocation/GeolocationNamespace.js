/* global MODE */
var _ = require('lodash');

var GeolocationSimple = require('./GeolocationSimple');
var GeolocationTracking = require('./GeolocationTracking');

var namespace = {
    GeolocationSimple: GeolocationSimple,
    GeolocationTracking: GeolocationTracking
};

var Geolocation = require('./Geolocation');

_.extend(Geolocation, namespace);

module.exports = Geolocation;
