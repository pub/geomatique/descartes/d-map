/* global MODE Descartes*/

var ol = require('openlayers');
var _ = require('lodash');
var log = require('loglevel');
var $ = require('jquery');
var Utils = require('../../Utils/DescartesUtils');
var Geolocation = require('./Geolocation');

require('../css/GeolocationSimple.css');

/**
 * Class: Descartes.Tool.GeolocationSimple
 * Classe définissant un bouton permettant de se geolocaliser (simple).
 *
 * Hérite de:
 *  - <Descartes.Tool.Geolocation>
 */
var Class = Utils.Class(Geolocation, {

    /**
     * Propriete: projection
     * {Boolean} Indicateur pour la présence du nom de la projection à l'affichage (false par défaut).
     */
    projection: true,
    /**
     * Propriété: displayProjectionsList
     * {Array} Tableau définissant les projections des coordonnées à saisir.
     */
    displayProjections: [],
    /**
     * Propriété: selectedDisplayProjectionIndex
     * {Integer} Position de la projection sélectionnée
     */
    selectedDisplayProjectionIndex: 0,
    _selectedDisplayProjection: "",
    _mapProjection: null,
    /**
     * Propriete: displayInfos
     * {Boolean} Indicateur pour l'affichage ou non de la popup d'informations (true par défaut).
     */
    displayInfos: true,
    /**
     * Propriete: infos
     * {Object} informations supplémentaires à afficher dans la popup d'information'.
     */
    infos: {
       accuracy: true,
       altitude: true,
       altitudeAccuracy: true,
       heading: true,
       speed: true,
       projection: true
    },
    /**
     * Propriete: displayExportInfos
     * {Boolean} Indicateur pour l'affichage ou non du bouton d'export des informations (true par défaut).
     */
    displayExportInfos: true,
    geolocationLayer: null,

    /**
     * Constructeur: Descartes.Tool.GeolocationSimple
     * Constructeur d'instances
     */
    initialize: function (options) {
        Geolocation.prototype.initialize.apply(this, arguments);
    },

    /**
     * Methode: activate
     * Active le bouton et désactive les autres boutons d'interaction de la barre d'outils.
     */
    activate: function () {
        if (this.isActive()) {
            return false;
        }
        this.active = true;

        var that = this;

        this.geolocation = new ol.Geolocation({
          trackingOptions: {
            enableHighAccuracy: true
          },
          projection: this.olMap.getView().getProjection()
        });

        this.geolocation.setTracking(true);

        this.geolocation.on('change', function () {
          if (that.formDialog) {
            document.getElementById('position').innerText = that.adaptCoords(that.geolocation.getPosition(), that._mapProjection.getCode(), that._selectedDisplayProjection);
            if (that.infos.accuracy) {
                document.getElementById('accuracy').innerText = that.adaptInfo(that.geolocation.getAccuracy(), '[m]', true);
            }
            if (that.infos.altitude) {
                document.getElementById('altitude').innerText = that.adaptInfo(that.geolocation.getAltitude(), '[m]', true);
            }
            if (that.infos.altitudeAccuracy) {
                document.getElementById('altitudeAccuracy').innerText = that.adaptInfo(that.geolocation.getAltitudeAccuracy(), '[m]', true);
            }
            if (that.infos.heading) {
                document.getElementById('heading').innerText = that.adaptInfoHeading(that.geolocation.getHeading(), '[degré]', true);
            }
            if (that.infos.speed) {
                document.getElementById('speed').innerText = that.adaptInfoSpeed(that.geolocation.getSpeed(), ' [km/h]', true);
            }
          }
        });

        this.geolocation.on('error', function (error) {
          if (that.formDialog) {
            var info = document.getElementById('geolocationinfo');
            info.innerHTML = error.message;
            info.style.display = '';
          }
        });

        var accuracyFeature = new ol.Feature();
        this.geolocation.on('change:accuracyGeometry', function () {
          accuracyFeature.setGeometry(that.geolocation.getAccuracyGeometry());
        });

        var positionFeature = new ol.Feature();
        positionFeature.setStyle(new ol.style.Style({
          image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
              color: '#3399CC'
            }),
            stroke: new ol.style.Stroke({
              color: '#fff',
              width: 2
            })
          })
        }));

        this.geolocation.on('change:position', function () {
          var coordinates = that.geolocation.getPosition();
          positionFeature.setGeometry(coordinates ? new ol.geom.Point(coordinates) : null);
          that.olMap.getView().setCenter(positionFeature.getGeometry().getCoordinates());
        });

        this.geolocationLayer = new ol.layer.Vector({
          source: new ol.source.Vector({
            features: [accuracyFeature, positionFeature]
          })
        });
        this.olMap.addLayer(this.geolocationLayer);

        if (this.displayInfos) {
           this.createModalDialog();
        }

        this.events.triggerEvent('activate', this);
        this.updateElement();
        Descartes._activeClickToolTip = false;
        return true;
    },

    /**
     * Methode: deactivate
     * Désactive le bouton
     */
    deactivate: function () {
        if (!this.isActive()) {
            return false;
        }
        this.active = false;

        this.geolocation.setTracking(false);
        this.olMap.removeLayer(this.geolocationLayer);

        if (this.formDialog) {
           this.formDialog.close();
		}

		this.updateElement();
        Descartes._activeClickToolTip = true;
        return true;

    },

    CLASS_NAME: 'Descartes.Tool.GeolocationSimple'
});

module.exports = Class;
