/* global MODE Descartes*/

var ol = require('openlayers');
var _ = require('lodash');
var log = require('loglevel');
var $ = require('jquery');
var Utils = require('../../Utils/DescartesUtils');
var Geolocation = require('./Geolocation');

require('../css/GeolocationTracking.css');

/**
 * Class: Descartes.Tool.GeolocationTracking
 * Classe définissant un bouton permettant de se geolocaliser (tracking).
 *
 * Hérite de:
 *  - <Descartes.Tool.Geolocation>
 */
var Class = Utils.Class(Geolocation, {

    /**
     * Propriete: projection
     * {Boolean} Indicateur pour la présence du nom de la projection à l'affichage (false par défaut).
     */
    projection: true,
    /**
     * Propriété: displayProjectionsList
     * {Array} Tableau définissant les projections des coordonnées à saisir.
     */
    displayProjections: [],
    /**
     * Propriété: selectedDisplayProjectionIndex
     * {Integer} Position de la projection sélectionnée
     */
    selectedDisplayProjectionIndex: 0,
    _selectedDisplayProjection: "",
    _mapProjection: null,
    /**
     * Propriete: displayInfos
     * {Boolean} Indicateur pour l'affichage ou non de la popup d'informations.
     */
    displayInfos: false,
    /**
     * Propriete: infos
     * {Object} informations supplémentaires à afficher dans la popup d'information'.
     */
    infos: {
       accuracy: true,
       altitude: true,
       altitudeAccuracy: true,
       heading: true,
       speed: true,
       projection: true
    },
    /**
     * Propriete: extraFormatExportTrace
     * {Object} formats supplémentaires pour l'export'.
     */
    extraFormatExportTrace: {
       gpxWpt: true,
       gpxRte: true,
       gpxTrk: true
    },
    /**
     * Propriete: displayExportInfos
     * {Boolean} Indicateur pour l'affichage ou non du bouton d'export des informations.
     */
    displayExportInfos: false,
    geolocationLayer: null,

    /**
     * Constructeur: Descartes.Tool.GeolocationTracking
     * Constructeur d'instances
     */
    initialize: function (options) {
        if (!_.isNil(options.extraFormatExportTrace)) {
            _.extend(this.extraFormatExportTrace, options.extraFormatExportTrace);
            delete options.extraFormatExportTrace;
        }
        Geolocation.prototype.initialize.apply(this, arguments);
    },

    /**
     * Methode: activate
     * Active le bouton et désactive les autres boutons d'interaction de la barre d'outils.
     */
    activate: function () {
        if (this.isActive()) {
            return false;
        }

        this.active = true;

        var that = this;

        this.geolocation = new ol.Geolocation({
          trackingOptions: {
              enableHighAccuracy: true
          },
          projection: this.olMap.getView().getProjection()
        });

        this.geolocation.setTracking(true);

        this.geolocation.on('change', function () {
          if (that.formDialog) {
            document.getElementById('position').innerText = that.adaptCoords(that.geolocation.getPosition(), that._mapProjection.getCode(), that._selectedDisplayProjection);
            if (that.infos.accuracy) {
                document.getElementById('accuracy').innerText = that.adaptInfo(that.geolocation.getAccuracy(), '[m]', true);
            }
            if (that.infos.altitude) {
                document.getElementById('altitude').innerText = that.adaptInfo(that.geolocation.getAltitude(), '[m]', true);
            }
            if (that.infos.altitudeAccuracy) {
                document.getElementById('altitudeAccuracy').innerText = that.adaptInfo(that.geolocation.getAltitudeAccuracy(), ' [m]', true);
            }
            if (that.infos.heading) {
                document.getElementById('heading').innerText = that.adaptInfoHeading(that.geolocation.getHeading(), '[degré]', true);
            }
            if (that.infos.speed) {
                document.getElementById('speed').innerText = that.adaptInfoSpeed(that.geolocation.getSpeed(), '[km/h]', true);
            }
          }
        });

        this.geolocation.on('error', function (error) {
          if (that.formDialog) {
            var info = document.getElementById('geolocationinfo');
            info.innerHTML = error.message;
            info.style.display = '';
          }
        });

        var positionFeature = new ol.Feature();
        positionFeature.setStyle(new ol.style.Style({
          image: new ol.style.Circle({
            radius: 6,
            fill: new ol.style.Fill({
              color: '#3399CC'
            }),
            stroke: new ol.style.Stroke({
              color: '#fff',
              width: 2
            })
          })
        }));

        this.geolocation.on('change:position', function () {
          var coordinates = that.geolocation.getPosition();
          positionFeature.setGeometry(coordinates ? new ol.geom.Point(coordinates) : null);
          that.olMap.getView().setCenter(positionFeature.getGeometry().getCoordinates());
          var currentPosition = positionFeature.clone();
          currentPosition.setStyle(new ol.style.Style({
              image: new ol.style.Circle({
                radius: 2,
                fill: new ol.style.Fill({
                  color: 'black'
                })
              })
          }));
          var ordre = that.positionListGeolocationLayer.getSource().getFeatures().length;
          currentPosition.set('ordre', ordre + 1);
          var altitude = that.geolocation.get('altitude');
          currentPosition.set('ele', altitude);
          currentPosition.set('time', Date.now() / 1000);
          that.positionListGeolocationLayer.getSource().addFeature(currentPosition);
        });

        this.geolocationLayer = new ol.layer.Vector({
          source: new ol.source.Vector({
            features: [positionFeature]
          })
        });
        this.olMap.addLayer(this.geolocationLayer);

		this.positionListGeolocationLayer = new ol.layer.Vector({
          source: new ol.source.Vector()
        });
        this.olMap.addLayer(this.positionListGeolocationLayer);

        if (this.displayInfos) {
           this.createModalDialog();
        }

        this.events.triggerEvent('activate', this);
        this.updateElement();
        Descartes._activeClickToolTip = false;
        return true;
    },
    /**
     * Methode: deactivate
     * Désactive le bouton
     */
    deactivate: function () {
        if (!this.isActive()) {
            return false;
        }

        this.active = false;

        this.geolocation.setTracking(false);
        this.olMap.removeLayer(this.geolocationLayer);

        this.olMap.removeLayer(this.positionListGeolocationLayer);

        if (this.formDialog) {
           this.formDialog.close();
		}

		this.updateElement();
        Descartes._activeClickToolTip = true;

        var that = this;
        var dialogListExportFormat = new Descartes.Button.ContentTask.ExportVectorLayer();
        var label;
        if (this.extraFormatExportTrace && this.extraFormatExportTrace.gpxTrk) {
          label = this.getMessage("GPX_FORMAT_TRK");
          dialogListExportFormat.listExportFormat.unshift({
            id: "gpxtrk", label: label, olClass: ol.format.GPX
          });
        }
        if (this.extraFormatExportTrace && this.extraFormatExportTrace.gpxRte) {
          label = this.getMessage("GPX_FORMAT_RTE");
          dialogListExportFormat.listExportFormat.unshift({
            id: "gpxrte", label: label, olClass: ol.format.GPX
          });
        }
        if (this.extraFormatExportTrace && this.extraFormatExportTrace.gpxWpt) {
          label = this.getMessage("GPX_FORMAT_WPT");
          dialogListExportFormat.listExportFormat.unshift({
            id: "gpx", label: label, olClass: ol.format.GPX
          });
        }
        dialogListExportFormat.title = this.getMessage("DIALOG_TITLE_EXPORT_FORMAT");
        dialogListExportFormat.runExport = function (form) {
            var selectedFormat = form.format;
            var Classname = null;
            var filename = "exportDescartes.txt";
            for (var i = 0, len = this.listExportFormat.length; i < len; i++) {
                if (selectedFormat === this.listExportFormat[i].id) {
                    Classname = this.listExportFormat[i].olClass;
                    filename = this.listExportFormat[i].fileName;
                    break;
                }
            }

            if (Classname) {
              var optionsFormat = {};
              //if (selectedFormat === "kml") {
                  optionsFormat.featureProjection = that.olMap.getView().getProjection().getCode();
                  optionsFormat.defaultDataProjection = 'EPSG:4326'; //flux kml toujours en EPSG:4326 mais pour l'instant, on force aussi pour le GeoJson
              //}
              var format = new Classname();
              var features = that.positionListGeolocationLayer.getSource().getFeatures();
              var flux;
              if (selectedFormat === "gpxtrk" || selectedFormat === "gpxrte") {
                  var points = [];
                  for (var m = 0, mlen = features.length; m < mlen; m++) {
                      var coordinatesXYZM = features[m].getGeometry().getCoordinates().concat(features[m].get('ele')).concat(features[m].get('time'));
                      points.push({ordre: features[m].get('ordre'), ele: features[m].get('ele'), time: features[m].get('time'), coordinates: features[m].getGeometry().getCoordinates(), coordinatesXYZM: coordinatesXYZM});
                  }
                  points.sort(function (a, b) {
                      return a.time - b.time;
                  });
                  var lines = [];
                  for (var l = 0, llen = points.length; l < llen; l++) {
                      if (points[l + 1]) {
                          lines.push({ordre: points[l].ordre + "-" + points[l + 1].ordre, linecoordinatesXYZM: [points[l].coordinatesXYZM, points[l + 1].coordinatesXYZM]});
                      }
                  }
                  var featuresLine = [];
                  var trkTrace = [];
                  for (var k = 0, klen = lines.length; k < klen; k++) {
                      var linecoordinatesXYZM = lines[k].linecoordinatesXYZM;
                      var fLine = new ol.Feature({
                          geometry: new ol.geom.LineString(linecoordinatesXYZM),
                          ordre: lines[k].ordre
                      }, 'XYZM');
                      featuresLine.push(fLine);
                      trkTrace.push(linecoordinatesXYZM);
                  }
                  if (selectedFormat === "gpxrte") {
                      flux = format.writeFeatures(featuresLine, optionsFormat);
                  } else if (selectedFormat === "gpxtrk") {
                      var featureMultiLine = new ol.Feature({
                          geometry: new ol.geom.MultiLineString(trkTrace)
                      }, 'XYZM');
                      flux = format.writeFeatures([featureMultiLine], optionsFormat);
                  }
                  filename = "trace_positions_epsg_4326.gpx";
              } else {
                  flux = format.writeFeatures(features, optionsFormat);
                  filename = "trace_positions_epsg_4326." + selectedFormat;
              }
              flux = flux.replace('creator="OpenLayers"', 'creator="Descartes"');
              Utils.download(filename, flux);
            }
            var label;
            if (that.extraFormatExportTrace && that.extraFormatExportTrace.gpxTrk) {
              label = this.getMessage("GPX_FORMAT_TRK");
              dialogListExportFormat.listExportFormat.shift({
                id: "gpxtrk", label: label, olClass: ol.format.GPX
              });
            }
            if (that.extraFormatExportTrace && that.extraFormatExportTrace.gpxRte) {
              label = this.getMessage("GPX_FORMAT_RTE");
              dialogListExportFormat.listExportFormat.shift({
                id: "gpxrte", label: label, olClass: ol.format.GPX
              });
            }
            if (that.extraFormatExportTrace && that.extraFormatExportTrace.gpxWpt) {
              label = this.getMessage("GPX_FORMAT_WPT");
              dialogListExportFormat.listExportFormat.shift({
                id: "gpx", label: label, olClass: ol.format.GPX
              });
            }
        };
        dialogListExportFormat.close = function () {
            var label;
            if (that.extraFormatExportTrace && that.extraFormatExportTrace.gpxTrk) {
              label = this.getMessage("GPX_FORMAT_TRK");
              dialogListExportFormat.listExportFormat.shift({
                id: "gpxtrk", label: label, olClass: ol.format.GPX
              });
            }
            if (that.extraFormatExportTrace && that.extraFormatExportTrace.gpxRte) {
              label = this.getMessage("GPX_FORMAT_RTE");
              dialogListExportFormat.listExportFormat.shift({
                id: "gpxrte", label: label, olClass: ol.format.GPX
              });
            }
            if (that.extraFormatExportTrace && that.extraFormatExportTrace.gpxWpt) {
              label = this.getMessage("GPX_FORMAT_WPT");
              dialogListExportFormat.listExportFormat.shift({
                id: "gpx", label: label, olClass: ol.format.GPX
              });
            }
        };

        if (that.positionListGeolocationLayer.getSource().getFeatures().length > 0) {
            dialogListExportFormat.execute();
        }

        return true;
    },

    CLASS_NAME: 'Descartes.Tool.GeolocationTracking'
});

module.exports = Class;
