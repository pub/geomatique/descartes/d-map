
var namespace = {
	/**
	* Constante: ID
    * Identifiant de la barre d'outils.
	*/
    ID: 1,
    /**
     * Constante: TEMPLATE_NAME
     * Template du nom de la barre d'outils.
     */
    TEMPLATE_NAME: 'DescartesToolBar'
};

module.exports = namespace;
