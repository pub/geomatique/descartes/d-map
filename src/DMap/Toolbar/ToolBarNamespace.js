/* global MODE */

var _ = require('lodash');
var ToolBar = require('./' + MODE + '/ToolBar');
var Constants = require('./ToolBarConstants');



var namespace = {
};
_.extend(namespace, Constants);
_.extend(ToolBar, namespace);

module.exports = ToolBar;
