/* global Descartes */
var $ = require('jquery');
var _ = require('lodash');

require('../css/TooBar.css');

var Utils = require('../../Utils/DescartesUtils');
var ToolBar = require('../../Core/ToolBar');
var ToolBarConstants = require('../ToolBarConstants');

var toolbarTemplate = require('./templates/ToolBar.ejs');

/**
 * Class: Descartes.ToolBar
 * Classe définissant une barre d'outils, pouvant accueillir des boutons de type <Descartes.Button> et/ou <Descartes.Tool>.
 *
 */
var Class = Utils.Class(ToolBar, {

    panelCss: null,

    /**
     * Constructeur: Descartes.ToolBar
     * Constructeur d'instances
     *
     */
    initialize: function () {
        this.panelCss = Descartes.UIBootstrap4Options.panelCss;
        ToolBar.prototype.initialize.apply(this, arguments);
    },
    createToolBar: function () {
        var template = toolbarTemplate({
            id: this.id,
            draggable: this.draggable,
            grouped: this.grouped,
            vertical: this.vertical,
            panel: this.panel,
            panelCss: this.panelCss,
            title: this.title,
            closable: this.closable
        });
        $(this.div).append(template);
        if (!_.isNil(this.draggable) && this.draggable.enable === true && $().draggable) {
            $(this.div).draggable(this.draggable);
            $(this.div).addClass('DescartesToolBarDraggable');
            if (this.draggable.containment === 'parent') {
                this.alignToMap();
                this.olMap.on('change:size', this.alignToMap.bind(this));
            }

        }
        if (this.resizable === true && $().resizable) {
            $(this.div).resizable({
                minHeight: 110,
                minWidth: 100,
                alsoResize: '#' + this.id + '_panel'
            });
        }
        if (this.closable) {
            var that = this;
            $('#' + this.id + '_close').on('click', function () {
                that.destroy();
            });
        }
    },
    alignToMap: function () {
        var parentHeight = $(this.div).parent().height();
        $(this.div).css('top', -parentHeight);
    },
    /*
     * Méthode privée
     */
    _computeToolBarName: function () {
        return ToolBarConstants.TEMPLATE_NAME + ToolBarConstants.ID;
    },
    alignTo: function (ref) {
        if (_.isString(ref)) {
            ref = '#' + ref;
        }

        $(this.div).css('display', 'inline-block');
        $(this.div).position({
            my: 'left top',
            at: 'left bottom',
            of: ref
        });

    },
    destroy: function () {
        ToolBar.prototype.destroy.apply(this, arguments);
        $(this.div).empty();
    },
    CLASS_NAME: 'Descartes.ToolBar'
});
module.exports = Class;
