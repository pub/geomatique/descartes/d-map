var constants = {
	UIBootstrap4Options: {
        btnCssToolBar: 'btn-outline-dark',
        btnCss: 'btn-outline-dark',
        btnCssSubmit: 'btn-outline-dark',
        btnCssRemove: 'btn-danger',
        panelCss: '',
        progressBarCss: ''
	}
};
module.exports = constants;
