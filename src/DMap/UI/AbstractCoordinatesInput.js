var ol = require('openlayers');
var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var UI = require('../Core/UI');

/**
 * Class: Descartes.UI.AbstractCoordinatesInput
 * Classe "abstraite" proposant la saisie d'un couple de coordonnées pour recentrer la carte.
 *
 * :
 * Le formulaire de saisie est constitué de deux zones de texte (abscisse et ordonnée).
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 * Classes dérivées:
 *  - <Descartes.UI.CoordinatesInputDialog>
 *  - <Descartes.UI.CoordinatesInputInPlace>
 */
var Class = Utils.Class(UI, {

    /**
     * Methode: validateDatas
     * Effectue les contrôles de surface aprçs la saisie.
     *
     * :
     * Met à jour le modèle si ceux-ci sont assurés.
     *
     * :
     * Alimente la liste des erreurs dans le cas contraire.
     */
    validateDatas: function () {
        this.errors = [];
        var maxExtent = this.model.maxExtent;
        if (this.model.proj && this.model.proj !== this.model.mapProjection) {
           maxExtent = ol.proj.transformExtent(maxExtent, this.model.mapProjection, this.model.proj);
        }
        if (this.model.checkCoordinatesInMaxExtent && !_.isNil(maxExtent)) {
            if (!ol.extent.containsXY(maxExtent, this.model.x, this.model.y)) {
                var range1 = this.getMessage('RANGE_X_PART_ONE_MESSAGE');
                var range2 = this.getMessage('RANGE_Y_PART_ONE_MESSAGE');
                if (this.model.proj === "EPSG:4326" || this.model.proj === "EPSG:4171" || this.model.proj === "EPSG:4258") {
                    range1 = this.getMessage('RANGE_LON_PART_ONE_MESSAGE');
                    range2 = this.getMessage('RANGE_LAT_PART_ONE_MESSAGE');
                }

                var xMsg = range1 +
                        maxExtent[0] +
                        this.getMessage('RANGE_PART_TWO_MESSAGE') +
                        maxExtent[2];

                var yMsg = range2 +
                        maxExtent[1] +
                        this.getMessage('RANGE_PART_TWO_MESSAGE') +
                        maxExtent[3];

                this.errors.push(xMsg);
                this.errors.push(yMsg);
            }
        }

        return (this.errors.length === 0);
    },

    /**
     * Methode: showErrors
     * Affiche la liste des erreurs rencontrés lors des contrôles de surface.
     */
    showErrors: function () {
        if (this.errors.length !== 0) {
            var errorsMessage = this.getMessage('ERRORS_LIST');
            for (var i = 0, len = this.errors.length; i < len; i++) {
                errorsMessage += '\n\t- ' + this.errors[i];
            }
            alert(errorsMessage);
        }
    },

    changeLabels: function (projectionCode) {
        var labelx = this.getMessage('INVITE1_MESSAGE');
        var labely = this.getMessage('INVITE2_MESSAGE');
        if (projectionCode === "EPSG:4326" || projectionCode === "EPSG:4171" || projectionCode === "EPSG:4258") {
            labelx = this.getMessage('INVITE1_LON_MESSAGE');
            labely = this.getMessage('INVITE2_LAT_MESSAGE');
        }
        return [labelx, labely];
    },

    CLASS_NAME: 'Descartes.UI.AbstractCoordinatesInput'
});

module.exports = Class;

