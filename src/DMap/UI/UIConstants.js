/**
 * @namespace Descartes.UI
 * @class UIConstants
 * @static
 */
module.exports = {
	/**
	* Constante: TYPE_WIDGET
	* Vue générée par construction grâce à une classe dérivée de Descartes.UI.
	*/
    TYPE_WIDGET: 0,
    /**
     * Constante: TYPE_INPLACEDIV
     * Vue générée par insertion directe d'un fragment HTML dans une DIV.
     */
    TYPE_INPLACEDIV: 1,

    /**
     * Constante: TYPE_POPUP
     * Vue générée par insertion d'un fragment HTML dans une fenêtre pop-up.
     */
    TYPE_POPUP: 2
};


