/* global Descartes */
var $ = require('jquery');
var _ = require('lodash');
var log = require('loglevel');

var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');
var formDialogTemplate = require('./templates/FormDialog.ejs');

require('../css/Dialog.css');

/**
 * Class: Descartes.UI.FormDialog
 * Classe permettant d'afficher une boite de dialogue.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 */
var Class = Utils.Class(UI, {
    id: null,
    title: null,
    size: null,
    sendLabel: 'OK',
    otherBtnLabel: 'Annuler',
    btnCss: null,
    btnCssSubmit: null,
    formClass: '',
    dialogClass: 'DescartesDialog',
    content: null,
    type: null,
    dialog: null,
    result: null,
    /**
     * Constructeur: Descartes.UI.FormDialog
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * title - {String} Titre de la boite de dialogue.
     * contenu - {String} Contenu à afficher dans la boite de dialogue.
     */
    initialize: function (options) {
        this.btnCss = Descartes.UIBootstrap4Options.btnCss;
        this.btnCssSubmit = Descartes.UIBootstrap4Options.btnCssSubmit;
        _.extend(this, options);
        UI.prototype.initialize.apply(this, arguments);
    },
    createTemplate: function (callback, otherCallback) {
        var template = formDialogTemplate({
            id: this.id,
            title: this.title,
            formClass: this.formClass,
            dialogClass: this.dialogClass,
            sendLabel: this.sendLabel,
            otherBtnLabel: this.otherBtnLabel,
            btnCss: this.btnCss,
            btnCssSubmit: this.btnCssSubmit,
            content: this.content
        });
        $('body').append(template);
        this.dialog = $('#' + this.id);

        this.handleEvents(callback, otherCallback);

    },
    handleEvents: function (callback, otherCallback) {
        var that = this;
        $('#' + this.id + '_formDialog').on('submit', function (e) {
            e.preventDefault();
            that.result = Utils.serializeFormArrayToJson($(this).serializeArray());

            log.debug('Closing dialog with result %s', that.result);
            if (!_.isNil(callback) && !_.isNil(that.result)) {
                if (callback(that.result) === false) {
                    e.preventDefault();
                }
            }
            that.result = null;
            that.close();
        });
        if (this.otherBtnLabel) {
            $('#' + this.id + '_otherAction').on('click', function (e) {
                that.otherAction(otherCallback);
        });
        }

        $('#' + this.id + '_close').on('click', function (e) {
            that.close(callback);
        });
    },
    open: function (callback, otherCallback) {
        this.createTemplate(callback, otherCallback);
        this.dialog.find('.modal-content').draggable();
    },
    close: function (callback) {
        this.dialog.remove();
        if (callback) {
            callback();
        }
    },
    otherAction: function (otherCallback) {
        this.dialog.remove();
        if (otherCallback) {
            otherCallback();
        }
    },
    CLASS_NAME: 'Descartes.UI.FormDialog'
});

module.exports = Class;
