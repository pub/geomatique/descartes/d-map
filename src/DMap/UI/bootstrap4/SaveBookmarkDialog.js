var $ = require('jquery');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');
var ModalFormDialog = require('./ModalFormDialog');

var saveTemplate = require('./templates/SaveBookmarkDialog.ejs');

require('../css/BookmarksInPlace.css');

/**
 * Class: Descartes.UI.SaveBookmarkDialog
 * Classe proposant, dans une boite de dialog de renseigner le nom du bookmark à sauvegarder.
 *
 * Hérite de:
 *  - <Descartes.UI>
 */
var Class = Utils.Class(UI, {

    /**
     * Constructeur: Descartes.UI.BookmarksInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * bookmarksModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.BookmarksManager>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
     */
    initialize: function (div, bookmarksModel, options) {
        UI.prototype.initialize.apply(this, [div, bookmarksModel, [], options]);
    },
    /**
     * Methode: draw
     * Construit la zone de la page HTML pour le gestionnaire de vues.
     */
    draw: function () {
        var content = saveTemplate();
        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.label,
            //size: 'modal-lg',
            content: content
        });
        dialog.open($.proxy(this.done, this));
    },
    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'choosed', ou affiche les messages d'erreur rencontrés.
     */
    done: function (result) {
        if (!_.isEmpty(result.name)) {
            this.events.triggerEvent('choosed', result);
        }
    },
    CLASS_NAME: 'Descartes.UI.SaveBookmarkDialog'
});

module.exports = Class;
