var $ = require('jquery');
var _ = require('lodash');
var log = require('loglevel');

var Utils = require('../../Utils/DescartesUtils');
var AbstractInPlace = require('./AbstractInPlace');
var GazetteerLevel = require('../../Model/GazetteerLevel');
var DMapConstants = require('../../DMapConstants');
var Url = require('../../Model/Url');

var gazetteerInPlaceTemplate = require('./templates/GazetteerInPlace.ejs');


require('../css/SimpleUIs.css');

/**
 * Class: Descartes.UI.GazetteerInPlace
 * Classe proposant, dans une zone fixe de la page HTML, le choix d'un objet de référence pour recadrer la carte.
 *
 * :
 * Le choix de l'objet est effectué par sélection de celui-ci dans un ensemble de listes déroulantes imbriquées, puis par activation d'un bouton HTML.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 *
 * Evénements déclenchés:
 * localize - Un objet de référence a été choisi.
 */
var Class = Utils.Class(AbstractInPlace, {

    defaultDisplayClasses: {
        globalClassName: 'DescartesUI',
        textClassName: 'DescartesUIText',
        titleClassName: 'DescartesUITitle',
        selectClassName: 'DescartesGazetteerList',
        buttonClassName: 'DescartesUIBouton'
    },

    /**
     * Propriete: proj
     * {String} Code de la projection de la carte.
     */
    proj: null,

    /**
     * Propriete: initValue
     * {String} Code de la valeur de l'objet "père" des objets du plus haut niveau de localisation (i.e. d'index 0).
     */
    initValue: null,

    /**
     * Propriete: service
     * {<Descartes.GazetteerService>} Service côté serveur pour la fourniture des objets de référence.
     */
    service: null,

    /**
     * Propriete: location
     * {String} Répertoire de localisation des fichiers statiques contenant les objets de référence ("descartes/gazetteer/" par défaut).
     */
    location: DMapConstants.GAZETTEER_LOCATION,

    /**
     * Propriete: levels
     * {Array(<Descartes.GazetteerLevel>)} Liste des niveaux de localisation.
     */
    levels: null,

    levelToActualize: null,

    selections: [],
    selectpicker: false,
    EVENT_TYPES: ['localize'],

    /**
     * Constructeur: Descartes.UI.GazetteerInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * gazetteerModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.Gazetteer>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * levels - {Array(<Descartes.GazetteerLevel>)} Liste des niveaux de localisation.
     * proj - {String} Code de la projection de la carte.
     * initValue - {String} Code de la valeur de l'objet "père" des objets du plus haut niveau de localisation (i.e. d'index 0).
     * service - {<Descartes.GazetteerService>} Service côté serveur pour la fourniture des objets de référence.
     * location - {String} Répertoire de localisation des fichiers statiques contenant les objets de référence ("descartes/gazetteer/" par défaut).
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de localisation.
     */
    initialize: function (div, gazetteerModel, options) {
        var that = this;

        this.levels = [];

        if (!_.isNil(options)) {
            this.service = options.service || null;
            delete options.service;
            if (!_.isNil(options.levels)) {
                _.each(options.levels, function (level, index) {
                    if (!(level instanceof GazetteerLevel)) {
                        level = new GazetteerLevel(level.message, level.error, level.name, level.options);
                    }
                    level.index = index;
                    that.levels.push(level);
                });
                delete options.levels;
            }
        }
        AbstractInPlace.prototype.initialize.apply(this, [div, gazetteerModel, this.EVENT_TYPES, options]);
    },

    draw: function () {
        this.showLevel(0, this.initValue);
    },
    /**
     * Methode: refresh
     * Construit la zone de la page HTML pour le choix d'un objet de référence.
     */
    refresh: function () {
        var that = this;

        var content = gazetteerInPlaceTemplate({
            id: this.id,
            levels: this.levels,
            selections: this.selections,
            btnCssSubmit: this.btnCssSubmit,
            buttonMessage: this.getMessage('BUTTON_MESSAGE')
        });

        this.renderPanel(content);

        $('#' + this.id).submit(function (event) {
            event.preventDefault();
            that.events.triggerEvent('localize');
        });

        for (var i = 0; i < this.levels.length; i++) {
            $('#' + this.id + '_' + i).change(function () {
                var value = $(this).val();
                var index = _.last($(this)[0].id.split('_'));
                index = Number(index);
                that.selections.splice(index, 0, value);
                var level = that.levels[index];
                var nextLevel = index + 1;
                if (nextLevel < that.levels.length) {
                    that.showLevel(nextLevel, value);
                }
                that.setActiveZone(level, value);
            });
            if (this.selectpicker && $('#' + this.id + '_' + i).selectpicker) {
                $('#' + this.id + '_' + i).selectpicker(this.levels[i].displayOptions);
            }
        }
    },

    setActiveZone: function (level, value) {
        for (var k = 0; k < level.datasList.length; k++) {
            var data = level.datasList[k];
            if (data.code === value) {
                this.model.activeZone = data;
                break;
            }
        }
    },
    /**
     * Methode: showLevel
     * Demande l'actualisation d'une des liste d'objets de référence.
     *
     * :
     * Cette actualisation est automatiquement demande par le changement d'un objet dans la liste immédiatement supérieure.
     *
     * Paramètres:
     * index - {Integer} Index du niveau (<Descartes.GazetteerLevel>) à actualiser.
     * value - {String} Code de la valeur de l'objet "père".
     */
    showLevel: function (index, value) {
        var existingLevel = false;
        var len = this.levels.length;
        for (var iLevel = 0; iLevel < len; iLevel++) {
            if (this.levels[iLevel].index == index) {
                existingLevel = true;
            }
        }
        if (existingLevel) {
            this.levelToActualize = index;
            log.debug('level to actualize %s', this.levelToActualize);
            for (var j = 0; j < len; j++) {

                if (j >= this.levelToActualize) {
                    var level = this.levels[j];
                    log.debug("Resetting values of level %s", j);
                    level.datasList = [];
                }
            }
            if (value !== '0') {
                this.ajaxCall(value);
            } else {
                this.refresh();
            }
        }
    },
    /**
     * Methode: ajaxCall
     * Lance une requête AJAX permettant de récupérer un ensemble d'objets de référence de même niveau.
     *
     * Paramètres:
     * value - {String} Code de la valeur de l'objet "père".
     */
    ajaxCall: function (value) {
        var actionURL;
        if (this.service === null) {
            actionURL = this.location + this.proj + '/' + this.proj + this.levels[this.levelToActualize].name + value + '.json';
        } else {
            if (!this.service.url.alterOrAddParam) {
               var params = this.service.url.params;
               var root = this.service.url.root;
               this.service.url = new Url(this.service.urlService);
               for (var i = 0; i < params.length; i++) {
                   this.service.url.alterOrAddParam(params[i].key, params[i].value);
               }
               this.service.url.root = root;
            }
            this.service.url.alterOrAddParam(this.service.levelParam, this.levels[this.levelToActualize].index);
            this.service.url.alterOrAddParam(this.service.projectionParam, this.proj);
            if (!_.isEmpty(value)) {
                this.service.url.alterOrAddParam(this.service.codeParam, value);
            }
            actionURL = this.service.url.getUrlString();
        }

        $.get(actionURL).done(function (response) {
            this.levels[this.levelToActualize].hasError = false;
            this.actualize(response);
        }.bind(this)).fail(function () {
            this.levels[this.levelToActualize].hasError = true;
            this.refresh();
        }.bind(this));
    },

    /**
     * Methode: actualize
     * Actualise le contenu d'une liste déroulante avec les objets de référence ad-hoc
     *
     * Paramètres:
     * response - {Object} jsonresponse.
     */
    actualize: function (response) {
        if (_.isString(response)) {
            response = JSON.parse(response);
        }
        var level = this.levels[this.levelToActualize];
        level.datasList = response;
        this.refresh();
    },

    CLASS_NAME: 'Descartes.UI.GazetteerInPlace'
});

module.exports = Class;
