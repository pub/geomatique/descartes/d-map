/* global Descartes */
var _ = require('lodash');
var $ = require('jquery');

var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');
var panelTemplate = require('./templates/Panel.ejs');

/**
 * Class: Descartes.UI.AbstractInPlace
 * Classe "abstraite" définissant les options des panels Boostrap.
 *
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 * Classes dérivées:
 *  - <Descartes.UI.BookmarksInPlace>
 *  - <Descartes.UI.CoordinatesInputInPlace>
 *  - <Descartes.UI.GazetteerInPlace>
 *  - <Descartes.UI.PrinterSetupInPlace>
 *  - <Descartes.UI.RequestManagerInPlace>
 *  - <Descartes.UI.ScaleChooserInPlace>
 *  - <Descartes.UI.ScaleSelectorInPlace>
 *  - <Descartes.UI.SizeSelectorInPlace>
 */
var Class = Utils.Class(UI, {
    /**
     * Propriete: label
     * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone.
     */
    label: true,
    /**
     * Propriete: defaultPanelOptions
     * {Objet} Options du panel.
     */
    defaultOptionsPanel: {
        collapsible: false,
        collapsed: false,
        panelCss: null
    },

    btnCss: null,
    btnCssSubmit: null,
    /**
     * Propriete: optionsPanel
     * {Objet} Options du panel.
     */
    optionsPanel: {},
    /**
     * Constructeur: Descartes.UI.AbstractInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * model - {Object} Modèle de la vue passé par référence par le contrôleur
     * eventTypes - {Array(String)} Types d'événements potentiellement déclenchés par la vue.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
     * optionsPanel - {Objet} Options du panel.
     */
    initialize: function (div, model, eventTypes, options) {
        this.btnCss = Descartes.UIBootstrap4Options.btnCss;
        this.btnCssSubmit = Descartes.UIBootstrap4Options.btnCssSubmit;
        this.defaultOptionsPanel.panelCss = Descartes.UIBootstrap4Options.panelCss;

        this.optionsPanel = _.extend({}, this.defaultOptionsPanel);

        if (!_.isNil(options) && !_.isNil(options.optionsPanel)) {
            _.extend(this.optionsPanel, options.optionsPanel);
            delete options.optionsPanel;
        }
        UI.prototype.initialize.apply(this, [div, model, eventTypes, options]);

        var that = this;
        $(this.div).on('hidden.bs.collapse', function () {
            that.optionsPanel.collapsed = true;
        });
        $(this.div).on('shown.bs.collapse', function () {
            that.optionsPanel.collapsed = false;
        });
    },

    renderPanel: function (content) {
        var template = panelTemplate({
            id: this.id + '_panel',
            label: this.label,
            title: this.getMessage('TITLE_MESSAGE'),
            collapsible: this.optionsPanel.collapsible,
            collapsed: this.optionsPanel.collapsed,
            panelCss: this.optionsPanel.panelCss,
            content: content
        });
        $(this.div).html(template);
    },

    CLASS_NAME: 'Descartes.UI.AbstractInPlace'
});

module.exports = Class;
