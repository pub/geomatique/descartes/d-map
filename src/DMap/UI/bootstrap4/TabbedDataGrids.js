/* global MODE, Descartes */

var $ = require('jquery');
var _ = require('lodash');
var ol = require('openlayers');
var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');
var FormDialog = require('./FormDialog');
var ResultFormDialog = require('./ResultFormDialog');
var DataGrid = require('./DataGrid');

var tabbedDataGridsTemplate = require('./templates/TabbedDataGrids.ejs');

var template = require('../../Button/ContentTask/' + MODE + '/templates/ExportVectorLayer.ejs');
var ModalFormDialog = require('../../UI/' + MODE + '/ModalFormDialog');
require('../css/TabbedDataGrids.css');

/**
 * Class: Descartes.UI.TabbedDataGrids
 * Classe permettant d'afficher, sous forme d'onglets, un ensemble de tableaux de propriétés d'objets géographiques.
 *
 * Hérite de:
 * - <Descartes.UI>
 *
 * Evénements déclenchés:
 * gotoFeatureBounds - La localisation rapide sur un objet est demandée.
 *
 * Ecouteurs mis en place:
 *  - l'événement 'gotoFeatureBounds' de la classe <Descartes.UI.DataGrid> déclenche la méthode <gotoBounds>.
 */
var Class = Utils.Class(UI, {

    defaultDisplayClasses: {
        globalClassName: 'DescartesDataGrid',
        panelHeaderContainerClassName: 'DescartesPanelHeaderContainer',
        panelHeaderClassName: 'DescartesPanelHeader',
        panelHeaderSelectedClassName: 'DescartesPanelHeaderSelected',
        panelContentClassName: 'DescartesPanelContent',
        panelHeaderExportClassName: 'DescartesPanelExportCsvButton',
        panelHeaderResultLayerExportClassName: 'DescartesPanelResultLayerExportButton'
    },

    /**
     * Propriete: activePanel
     * {Integer} Index du tableau actif.
     */
    activePanel: null,

    /**
     * Propriete: withCsvExport
     * {Boolean} Indicateur pour la possibilité d'exporter les tableaux sous forme de fichier CSV.
     */
    withCsvExport: false,
    /**
     * Propriete: withResultLayerExport
     * {Boolean} Indicateur pour la possibilité d'exporter les couches de résultat.
     */
    withResultLayerExport: false,
    /**
     * Propriete: withUIExports
     * {Boolean} Indicateur pour la possibilité d'exporter les tableaux (côté client).
     */
    withUIExports: false,

    /**
     * Propriete: withAvancedView
     * {Boolean} Indicateur pour l'affichage de la vue avancée du tableau.
     */
    withAvancedView: false,

    /**
     * Propriete: withFilterColumns
     * {Boolean} Indicateur pour la possibilité de filtrer les colonnes.
     */
    withFilterColumns: false,

    /**
     * Propriete: withListResultLayer
     * {Boolean} Indicateur pour la possibilité d'affichage de la liste des couches'.
     */
    withListResultLayer: false,

    /**
     * Propriete: withPagination
     * {Boolean} Indicateur pour la possibilité d'affichage de la pagination'.
     */
    withPagination: false,

    /**
     * Propriete: withTabs
     * {Boolean} Indicateur pour la possibilité d'affichage des onglets'.
     */
    withTabs: false,

    /**
     * Private
     */
    formCsvExport: null,

    /**
     * Private
     */
    dialogTitle: '',

    /**
     * Private
     */
    messageError: '',

    /**
     * Propriete: divPanelsContent
     * {Array(DOMElement)} Eléments DOM de la page contenant les tableaux de propriétés.
     */
    divPanelsContent: [],

    EVENT_TYPES: ['gotoFeatureBounds'],

    /**
     * Constructeur: Descartes.UI.TabbedDataGrids
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant les onglets.
     * model - null.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * withCsvExport - {Boolean} Indicateur pour la possibilité d'exporter les tableaux sous forme de fichier CSV (côté serveur).
     * withResultLayerExport - {Boolean} : Indique si le résultat doit proposer une exportation des objets retournés avec les géométries.
     * withUIExports - {Boolean} Indicateur pour la possibilité d'exporter les tableaux (côté client).
     * withAvancedView - {Boolean} : Indique si le résultat doit être affiché dans une vue (UI) avancée.
     * withFilterColumns - {Boolean} : Indicateur pour la possibilité de filtrer les colonnes.
     * withListResultLayer - {Boolean} : Indicateur pour la possibilité d'affichage de la liste des couches'.
     * withPagination - {Boolean} : Indicateur pour la possibilité d'afficher la pagination'.
     * withTabs - {Boolean} : Indicateur pour la possibilité d'afficher les onglets'.
     */
    initialize: function (div, model, options) {
        _.extend(this, options);
        UI.prototype.initialize.apply(this, [div, null, this.EVENT_TYPES, options]);
        if (_.isEmpty(this.dialogTitle)) {
            this.dialogTitle = this.getMessage("DIALOG_TITLE");
        }
        if (_.isEmpty(this.messageError)) {
            this.messageError = this.getMessage("MSG_EMPTY_RESULT");
        }
        if (this.withAvancedView && !this.withListResultLayer && !this.withTabs) {
            this.withTabs = true;
        }
    },
    /**
     * Methode: draw
     * Construit et affiche les onglets et le premier tableau.
     *
     * Paramètres:
     * datasModel - {Object} Objet JSON contenant les propriétés des tableaux, de la forme
     * (start code)
     * {
     * 		layerTitle:"Titre",
     * 		layerMinScale:echelleMini,
     * 		layerMaxScale:echellMaxi,
     * 		layersDatas:{prop1:val1, prop2:val2, ...}[]
     * }[]
     * (end)
     */
    draw: function (datasModel) {

        if (_.isEmpty(datasModel)) {
            alert(this.messageError);
            return;
        }
        this.model = datasModel;
        this.model.sort(function (a, b) {
             //return a.layerDatas.length < b.layerDatas.length;
             if (a.layerDatas.length > b.layerDatas.length) {
                return -1;
             }
             if (a.layerDatas.length < b.layerDatas.length) {
                return 1;
             }
             return 0;
        });

        var that = this;

        var content = tabbedDataGridsTemplate({
            id: this.id,
            model: this.model,
            withCsvExport: this.withCsvExport,
            withResultLayerExport: this.withResultLayerExport,
            withListResultLayer: this.withListResultLayer,
            panelHeaderExportClassName: this.displayClasses.panelHeaderExportClassName,
            panelHeaderResultLayerExportClassName: this.displayClasses.panelHeaderResultLayerExportClassName,
            panelContentClassName: this.displayClasses.panelContentClassName,
            btCsvExportTitle: this.getMessage("TAB_BT_EXPORT_CSV_TITLE"),
            btResultLayerExportTitle: this.getMessage("TAB_BT_EXPORT_RESULTLAYER_TITLE")
        });
        var formDialogOptions = {
            id: this.id + '_dialog',
            title: this.dialogTitle,
            dialogClass: 'DescartesDialog DescartesDialogResult',
            size: 'modal-lg',
            sendLabel: 'Fermer',
            content: content
        };

        var formDialog;
        if (this.btnDisplaySelectionLayer) {
            //formDialogOptions.otherBtnLabel = this.getMessage("DIALOG_OTHER_BTN_LABEL");
            formDialog = new ResultFormDialog(formDialogOptions);
        } else {
            formDialogOptions.otherBtnLabel = null;
            formDialog = new FormDialog(formDialogOptions);
        }

        var close = this.close.bind(this);
        var undisplaySelectionLayer = this.undisplaySelectionLayer.bind(this);
        var unselectSelectionLayer = this.unselectSelectionLayer.bind(this);
        var fixSelectionLayer = this.fixSelectionLayer.bind(this);
        var addSelectionLayer = this.addSelectionLayer.bind(this);
        var flashSelectionLayer = this.flashSelectionLayer.bind(this);
        if (this.btnDisplaySelectionLayer) {
            formDialog.open(close, undisplaySelectionLayer, unselectSelectionLayer, fixSelectionLayer, flashSelectionLayer, addSelectionLayer);
        } else {
        formDialog.open(close, close);
        }

        var formSelector = '#' + formDialog.id + '_formDialog';
        var formDialogDiv = $(formSelector);

        var optionsResize = {
            minHeight: 300,
            minWidth: 280,
            alsoResize: formSelector + ' .DescartesPanelContent'
        };

        if (this.withAvancedView && !_.isNil($().DataTable)) {
            optionsResize.handles = 'e, w';
        }

        /*formDialogDiv.parent().draggable({
           handle: ".modal-header"
        });*/

        formDialogDiv.parent().resizable(optionsResize);

        formDialogDiv.parent()[0].onresize = function (e) {
            if (that.withAvancedView && !_.isNil($('#' + that.id + ' .DescartesNavTabs').scrollingTabs)) {
                $('#' + that.id + ' .DescartesNavTabs').scrollingTabs('refresh');
            }
        };

        this.renderPanels();

        $('#' + this.id + ' a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
           if (this.withAvancedView && !_.isNil($().DataTable)) {
               $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
           }
            var href = e.target.href;
            var split = href.split('tab');

            this.activePanel = split[split.length - 1];

           if (that.withListResultLayer) {
              var listReultLayers = $('.DescartesUIListResultLayers a[data-toggle="tabListResultLayers"]');
              var activeIndex = parseInt(this.activePanel, 10);
              for (var i = 0; i < listReultLayers.length; i++) {
                   listReultLayers[i].className = listReultLayers[i].className.replace(/ active/g, '');
                   if (activeIndex === i) {
                       listReultLayers[i].className = listReultLayers[i].className + " active";
                   }
              }
           }
        }.bind(this));

         if (this.withListResultLayer) {
            $('.DescartesUIListResultLayers a[data-toggle="tabListResultLayers"]').on('click', function (e) {
               var listReultLayers = $('.DescartesUIListResultLayers a[data-toggle="tabListResultLayers"]');
               for (var i = 0; i < listReultLayers.length; i++) {
                   listReultLayers[i].className = listReultLayers[i].className.replace(/ active/g, '');
               }
               var currentElement = e.currentTarget;
               currentElement.className = currentElement.className + " active";
               var index = currentElement.tabIndex;
               $('.DescartesNavTabs a[href="#' + that.id + 'tab' + index + '"]').tab('show');
            });
           var maxHeight = "";
           if (document.documentElement.clientHeight < 800) {
              maxHeight = 180;
           } else {
              maxHeight = 300;
           }

            $('.DescartesUIListResultLayers').resizable({
                handles: 's',
                minHeight: 5,
                //maxHeight: 300,
                maxHeight: maxHeight,
                alsoResize: '#' + this.id + ' .DescartesUIResults'
            });
         }
         if (this.withAvancedView && !_.isNil($().DataTable)) {
           var height = "";
           if (document.documentElement.clientHeight < 800) {
              height = document.documentElement.clientHeight / 3.2;
           } else {
              //height = document.documentElement.clientHeight / 2;
              height = 365;
           }
           if (this.withFilterColumns) {
               height = height - 40;
           }
           if (!this.withTabs) {
               height += 30;
           }
           var nbElement = -1;
           if (this.withPagination) {
              nbElement = 25;
           }
           var settings = {
                 lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                 iDisplayLength: nbElement,
                 language: {
                     //"url": "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json",
                    "sProcessing": "Traitement en cours...",
                    //"sSearch": "Rechercher&nbsp;:",
                    "sSearch": "<span class='fa fa-search'></span>",
                    "sLengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
                    "sInfo": "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    "sInfoEmpty": "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    "sInfoPostFix": "",
                    "sLoadingRecords": "Chargement en cours...",
                    "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Pr&eacute;c&eacute;dent",
                        "sNext": "Suivant",
                        "sLast": "Dernier"
                    },
                    "oAria": {
                        "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                    }
                 },
                 scrollY: height,
                 //scrollY: "300px",
                 //scrollY: true,
                 scrollX: true,
                 //autoWidth: true,
                 paging: true,
                 ordering: true,
                 order: [],
                 columnDefs: [{
                     "targets": 'notsort',
                     "orderable": false
                 }],
                 info: false,
                 dom: '<"top"Bf>t<"bottom"lp>',
                 buttons: [
                    {
                      extend: 'colvis',
                      text: '<span title="' + this.getMessage("DT_BT_COLVIS_TITLE") + '">' + this.getMessage("DT_BT_COLVIS_LABEL") + '</span>'
                    }
                  ]
           };

           if (this.withFilterColumns) {
            settings.initComplete = function () {
               // Apply the search
               this.api().columns().every(function () {
                  var that = this;

                  $('input', this.footer()).on('keyup change clear', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value, true, false)
                            .draw();
                    }
                  });
              });
            };
          }

           if (this.withUIExports) {
               settings.select = true;
               settings.language.buttons = {
                   copyTitle: 'Ajouté au presse-papiers',
                   copySuccess: {
                       _: '%d lignes copiées',
                       1: '1 ligne copiée'
                   }
               };
               settings.buttons.push({
                   extend: 'collection',
                   text: 'Export',
                      buttons: [
                         {
                           extend: 'copy',
                           text: '<span title="' + this.getMessage("DT_BT_COPY_TITLE") + '">' + this.getMessage("DT_BT_COPY_LABEL") + '</span>',
                           exportOptions: {
                               columns: ':visible:not(.notexport)'
                           }
                         },
                         {
                           extend: 'csv',
                           text: '<span title="Export au format CSV">CSV</span>',
                           exportOptions: {
                               columns: ':visible:not(.notexport)'
                           }
                         },
                         {
                           extend: 'print',
                           text: '<span title="' + this.getMessage("DT_BT_PRINT_TITLE") + '">' + this.getMessage("DT_BT_PRINT_LABEL") + '</span>',
                           autoPrint: false,
                           exportOptions: {
                               columns: ':visible:not(.notexport)'
                           }
                         }
                      ]
                   });
           }

           $('#' + this.id + ' table.table').DataTable(settings);
           $('#' + this.id).addClass("DescartesUIResultsViewAvanced");

        }

        if (this.withCsvExport) {
            $('.' + this.displayClasses.panelHeaderExportClassName).on('click', function () {
                var element = $(this);
                var split = element.parent().attr('href').split('tab');
                that.exportPanel(split[split.length - 1]);
            });
        }

        if (this.withResultLayerExport) {
            $('.' + this.displayClasses.panelHeaderResultLayerExportClassName).on('click', function () {
                var element = $(this);
                var split = element.parent().attr('href').split('tab');
                that.exportResultLayerPanel(split[split.length - 1]);
            });
        }

        if (this.withAvancedView && !_.isNil($('#' + this.id + ' .DescartesNavTabs').scrollingTabs)) {
            $('#' + this.id + ' .DescartesNavTabs').scrollingTabs({
                bootstrapVersion: 4,
                scrollToTabEdge: true,
                disableScrollArrowsOnFullyScrolled: true,
                enableSwiping: true,
                cssClassLeftArrow: 'fa fa-chevron-left',
                cssClassRightArrow: 'fa fa-chevron-right'
          });
          if (!this.withTabs) {
              $('#' + this.id + " .scrtabs-tab-container").css('display', 'none');
          }
           if (!this.withPagination) {
              $('#' + this.id + " .dataTables_length").css('display', 'none');
              $('#' + this.id + " .dataTables_paginate").css('display', 'none');
          }
        }

    },
    /**
     * Methode: showPanel
     * Affiche le tableau de l'onglet activé.
     *
     * Paramètres:
     * elt - {DOMElement} Elément DOM de la page correspondant à l'onglet sélectionné.
     */
    renderPanels: function (elt) {
        this.activePanel = (elt !== undefined) ? elt.index : 0;
        this.adaptBoundsCoordinates();
        for (var i = 0; i < this.model.length; i++) {
            var filterColumns = false;
            if (this.withAvancedView && !_.isNil($().DataTable) && this.withFilterColumns) {
               filterColumns = true;
            }
            var dataGrid = new DataGrid($('#table_' + this.id + '_' + i)[0], this.model[i].layerDatas, filterColumns);
            dataGrid.events.register('gotoFeatureBounds', this, this.gotoBounds);
        }
    },
    adaptBoundsCoordinates: function () {
       if (!_.isNil(this.requestParams) && !_.isNil(this.requestParams.infosJson)) {
           for (var i = 0; i < this.model.length; i++) {
               var that = this;
               var infoLayer = _.find(this.requestParams.infosJson, function (o) {
                    return o.layerId === that.model[i].layerId;
               });
               if (!_.isNil(infoLayer) && !_.isNil(infoLayer.defaultDataProjection) && !_.isNil(infoLayer.mapProjection)) {
                   this.model[i].layerDatas.forEach(function (data) {
                       if (data.bounds) {
                          data.bounds = ol.proj.transformExtent(data.bounds, infoLayer.defaultDataProjection, infoLayer.mapProjection);
                       }
                   }, this);
               }
           }
       }
    },
    /**
     * Methode: exportPanel
     * Exporte au format CSV les données d'un tableau.
     *
     * Paramètres:
     * elt - {DOMElement} Elément DOM de la page correspondant au pictogramme d'exportation du tableau choisi.
     */
    exportPanel: function (index) {
        var tabNum = (index !== undefined) ? index : 0;
        var datas = this.model[tabNum].layerDatas;

        var csvStream = '';
        var headerRow = '';
        for (var colHeaderName in datas[0]) {
            if (!_.isNil(colHeaderName)) {
                if (!(colHeaderName === 'bounds' && datas[0].bounds instanceof Array)) {
                    headerRow += '"' + colHeaderName + '";';
                }
            }
        }
        csvStream += headerRow + '\r\n';

        for (var iRow = 0, lenRows = datas.length; iRow < lenRows; iRow++) {
            var row = '';
            for (var colName in datas[iRow]) {
                if (colName !== undefined) {
                    if (!(colName === 'bounds' && datas[0].bounds instanceof Array)) {
                        var value = datas[iRow][colName];
                        if (!_.isNil(value)) {
                            row += '"' + value.toString().replace(/"/g, '""') + '";';
                        }
                    }
                }
            }
            csvStream += row + '\r\n';
        }
        Utils.download('export.csv', csvStream);
    },
    /**
     * Methode: exportResultLayerPanel
     * Exporte la couche de résultat.
     *
     * Paramètres:
     * elt - {DOMElement} Elément DOM de la page correspondant au pictogramme d'exportation du tableau choisi.
     */
    exportResultLayerPanel: function (index) {
        var that = this;
        var dialogListExportFormat = new Descartes.Button.ContentTask.ExportVectorLayer();
        dialogListExportFormat.title = this.getMessage("DIALOG_TITLE_EXPORT_FORMAT");
        dialogListExportFormat.runExport = function (form) {

            var selectedFormat = form.format;
            var Classname = null;
            var filename = "exportDescartes.txt";
            for (var i = 0, len = this.listExportFormat.length; i < len; i++) {
                if (selectedFormat === this.listExportFormat[i].id) {
                    Classname = this.listExportFormat[i].olClass;
                    filename = this.listExportFormat[i].fileName;
                    break;
                }
            }

            if (Classname) {

                var tabNum = (index !== undefined) ? index : 0;
                var datas = that.model[tabNum].layerDatas;

                if (!_.isNil(that.requestParams.infosJson)) {
                    var infoLayer = _.find(that.requestParams.infosJson, function (o) {
                        return o.layerId === that.model[tabNum].layerId;
                    });

                    var xhr = new XMLHttpRequest();
                    xhr.open('GET', infoLayer.requestForExport);

                    var version = '1.0.0';
                    if (!_.isNil(infoLayer.version)) {
                       version = infoLayer.version;
                    }
                    var gmlFormat = new ol.format.GML2();
                    if (version === '1.1.0') {
                       gmlFormat = new ol.format.GML3();
                    }
                    var format = new ol.format.WFS({
                       featureNS: infoLayer.featureNameSpace,
                       gmlFormat: gmlFormat
                    });

                    if (!_.isNil(infoLayer.featureInternalProjection)) {
                        format.defaultDataProjection = infoLayer.featureInternalProjection;
                    }

                    if (infoLayer.serviceType === 0) {
                        format = new ol.format.WMSGetFeatureInfo();
                    }

                    var projection = infoLayer.mapProjection;

                    if (format.getType() === 'arraybuffer') {
                        xhr.responseType = 'arraybuffer';
                    }

                    xhr.onload = function (event) {
                        if (!xhr.status || xhr.status >= 200 && xhr.status < 300) {
                            var type = format.getType();
                            /** @type {Document|Node|Object|string|undefined} */
                            var source;
                            if (type === 'json' ||
                                    type === 'text') {
                                if (xhr.responseText.indexOf('html') === -1) {
                                   source = xhr.responseText;
                                } else {
                                    source = null;
                                }
                            } else if (type === 'xml') {
                                source = xhr.responseXML;
                                if (!source) {
                                    if (xhr.responseText.indexOf('html') === -1) {
                                       source = ol.xml.parse(xhr.responseText);
                                    } else {
                                        source = null;
                                    }
                                }
                            } else if (type === 'arraybuffer') {
                                source = (xhr.response);
                            }
                            if (source) {
                                var readOpts = {
                                    featureProjection: projection
                                };

                                if (!_.isNil(format.defaultDataProjection)) {
                                    readOpts.dataProjection = format.defaultDataProjection;
                                }

                                var features = format.readFeatures(source, readOpts);

                                var optionsFormatOutput = {};
                                //if (selectedFormat === "kml") {
                                    optionsFormatOutput.featureProjection = projection;
                                    optionsFormatOutput.defaultDataProjection = 'EPSG:4326'; //flux kml toujours en EPSG:4326
                                //}
                                var formatOutput = new Classname();

                                var flux = formatOutput.writeFeatures(features, optionsFormatOutput);

                                var filename = infoLayer.layerTitle.replace(/ /g, '_') + "_resultat_recherche_epsg_4326." + selectedFormat;

                                Utils.download(filename, flux);
                            }
                        }
                    };
                    xhr.onerror = function () {
                       alert(that.getMessage("MSG_EXPORT_ERROR"));
                    };

                    xhr.send();
                } else {
                    alert(that.getMessage("MSG_EXPORT_ERROR"));
                }
            }

        };
        dialogListExportFormat.execute();

    },
    close: function () {
        this.events.triggerEvent('close');
    },

    undisplaySelectionLayer: function () {
        this.events.triggerEvent('undisplaySelectionLayer');
    },
    unselectSelectionLayer: function () {
        this.events.triggerEvent('unselectSelectionLayer');
    },
    fixSelectionLayer: function () {
        this.events.triggerEvent('fixSelectionLayer');
    },
    addSelectionLayer: function () {
        this.events.triggerEvent('addSelectionLayer');
    },
    flashSelectionLayer: function () {
        this.events.triggerEvent('flashSelectionLayer');
    },
    /**
     * Methode: gotoBounds
     * Demande la localisation rapide sur un objet, en déclenchant l'événement 'gotoFeatureBounds'.
     *
     * Paramètres:
     * bounds - {Array(Float)} Coordonnées de l'emprise de l'objet sous la forme [xMin, yMin, xMax, yMax].
     */
    gotoBounds: function (bounds) {
        this.events.triggerEvent('gotoFeatureBounds', [
            bounds,
            this.model[this.activePanel].layerMinScale,
            this.model[this.activePanel].layerMaxScale
        ]);
    },

    CLASS_NAME: 'Descartes.UI.TabbedDataGrids'
});

module.exports = Class;
