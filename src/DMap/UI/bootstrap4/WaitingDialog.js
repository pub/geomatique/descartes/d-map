/* global Descartes */
var $ = require('jquery');

var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');
var waitingDialogTemplate = require('./templates/WaitingDialog.ejs');

require('../css/Dialog.css');

/**
 * Class: Descartes.UI.WaitingDialog
 * Classe permettant d'afficher une boite de dialogue d'attente.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 */
var WaitingDialog = Utils.Class(UI, {
    id: null,
    title: null,
    message: null,
    type: null,
    dialog: null,
    result: null,
    progressBarCss: null,
    /**
     * Constructeur: Descartes.UI.WaitingDialog
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * title - {String} Titre de la boite de dialogue.
     * type - {String} Type de la boite de dialogue.
     * message - {String} Message à afficher dans la boite de dialogue.
     * progressBarCss - {String} Style d'affichage de la barre de progresseion.
     */
    initialize: function (options) {
        this.id = options.id;
        this.message = options.message;
        this.progressBarCss = Descartes.UIBootstrap4Options.progressBarCss;
        UI.prototype.initialize.apply(this, arguments);
    },
    createTemplate: function () {
        var template = waitingDialogTemplate({
            id: this.id,
            progressBarCss: this.progressBarCss,
            message: this.message
        });
        $('body').append(template);
        this.dialog = $('#' + this.id);

        this.handleType();
    },
    handleType: function () {
        this.dialog.on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
            $(this).remove();
        });
    },
    open: function () {
        this.createTemplate();
        this.dialog.modal({
            backdrop: 'static'
        });
    },
    close: function () {
        this.dialog.modal('hide');
    },
    CLASS_NAME: 'Descartes.UI.WaitingDialog'
});

module.exports = WaitingDialog;
