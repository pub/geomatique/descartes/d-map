/* global Descartes */
var $ = require('jquery');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var AbstractInPlace = require('./AbstractInPlace');

var boomarksTemplate = require('./templates/BookmarksInPlace.ejs');

require('../css/BookmarksInPlace.css');

/**
 * Class: Descartes.UI.BookmarksInPlace
 * Classe proposant, dans une zone fixe de la page HTML, un ensemble de pictogrammes permettant de gérer des vues personnalisées.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 *
 * Evénements déclenchés:
 * sauvegarder - La création d'une nouvelle vue est demandée.
 * supprimer - La suppression d'une vue est demandée.
 * recharger - Le chargement d'une vue est demandé.
 */
var Class = Utils.Class(AbstractInPlace, {


    defaultDisplayClasses: {
        globalClassName: 'DescartesBookmarks',
        buttonClassName: 'DescartesUIButton',
        textClassName: 'DescartesBookmarksText',
        titleClassName: 'DescartesUITitle',
        labelClassName: 'DescartesBookmarksLabel',
        imgLoadClassName: 'DescartesBookmarksLoad',
        imgOpenClassName: 'DescartesBookmarksOpen',
        imgRemoveClassName: 'DescartesBookmarksRemove'
    },

    btnCss: null,

    EVENT_TYPES: ['sauvegarder', 'supprimer', 'recharger'],

    /**
     * Constructeur: Descartes.UI.BookmarksInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * bookmarksModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.BookmarksManager>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
     */
    initialize: function (div, bookmarksModel, options) {
        this.btnCss = Descartes.UIBootstrap4Options.btnCssToolBar;
        AbstractInPlace.prototype.initialize.apply(this, [div, bookmarksModel, this.EVENT_TYPES, options]);
    },
    /**
     * Methode: draw
     * Construit la zone de la page HTML pour le gestionnaire de vues.
     */
    draw: function () {
        var that = this;

        var vues = [];
        _.each(this.model.vues, function (vue, index) {
            var title = '';
            if (!_.isNil(vue.maxDate)) {
                title = that.getMessage('MAX_DAY_PREFIX') + vue.maxDate;
            }
            vues.push({
                id: that.id + index,
                nom: vue.nom,
                title: title,
                url: vue.url
            });
        });

        var content = boomarksTemplate({
            id: this.id,
            vues: vues,
            reloadVue: this.getMessage('RELOAD_VIEW'),
            openVue: this.getMessage('OPEN_VIEW'),
            deleteVue: this.getMessage('DELETE_VIEW'),
            noVuesMessage: this.getMessage('NO_VIEW'),
            btnCss: this.btnCss,
            btnCssSubmit: this.btnCssSubmit,
            buttonMessage: this.getMessage('BUTTON_MESSAGE')
        });

        this.renderPanel(content);

        $('.DescartesBookmarksLoad').parent().on('click', function () {
            that.doneRecharger(this);
        });

        $('.DescartesBookmarksRemove').parent().on('click', function () {
            that.doneSupprimer(this);
        });

        $('#' + this.id + '_button').parent().on('click', function () {
            that.doneSauvegarder(this);
        });

    },
    /**
     * Methode: doneSauvegarder
     * Demande la création d'une vue.
     *
     * :
     * Cette méthode est appelée suite à un clic sur le bouton "Enregistrer la vue courante".
     *
     * :
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'sauvegarder'.
     */
    doneSauvegarder: function () {
        this.events.triggerEvent('sauvegarder');
    },

    /**
     * Methode: doneRecharger
     * Demande le rechargement d'une vue.
     *
     * :
     * Cette méthode est appelée suite à un clic sur le pictogramme "Recharger" correspondant à la vue souhaitée.
     *
     * :
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'recharger'.
     *
     * Paramètres:
     * elt - {DOMElement} Pictogramme "Recharger" correspondant à la vue souhaitée.
     */
    doneRecharger: function (elt) {
        this.retrieveNumVue(elt);
        this.events.triggerEvent('recharger');
    },

    /**
     * Methode: doneSupprimer
     * Demande la suppression d'une vue.
     *
     * Cette méthode est appelée suite à un clic sur le pictogramme "Supprimer" correspondant à la vue souhaitée.
     *
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'supprimer'.
     *
     * Paramètres:
     * elt - {DOMElement} Pictogramme "Supprimer" correspondant à la vue souhaitée.
     */
    doneSupprimer: function (elt) {
        this.retrieveNumVue(elt);
        this.events.triggerEvent('supprimer');
    },
    retrieveNumVue: function (elt) {
        var elemId = $(elt).parents('.DescartesBookmarks')[0].id;
        this.model.numVue = elemId.replace(this.id, '');
    },

    CLASS_NAME: 'Descartes.UI.BookmarksInPlace'
});

module.exports = Class;
