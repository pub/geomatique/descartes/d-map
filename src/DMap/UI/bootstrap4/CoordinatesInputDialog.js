var ol = require('openlayers');
var $ = require('jquery');

var Utils = require('../../Utils/DescartesUtils');
var AbstractCoordinatesInput = require('../AbstractCoordinatesInput');

var CoordinatesInputDialog = require('./templates/CoordinatesInputDialog.ejs');
var ModalFormDialog = require('./ModalFormDialog');

/**
 * Class: Descartes.UI.CoordinatesInputDialog
 * Classe proposant, sous forme de boite de dialogue, la saisie d'un couple de coordonnées pour recentrer la carte.
 *
 * Hérite de:
 *  - <Descartes.AbstractCoordinatesInput>
 *
 *
 * Evénements déclenchés:
 * recentrage - Les coordonnées sont saisies et valides.
 *
 * Ecouteurs mis en place:
 *  - l'événement 'done' de la classe <Descartes.ModalDialog> déclenche la méthode <sendDatas>.
 *  - l'événement 'cancelled' de la classe <Descartes.ModalDialog> déclenche la méthode <deleteDialog>.
 */
var Class = Utils.Class(AbstractCoordinatesInput, {

    EVENT_TYPES: ['recentrage'],

    defaultDisplayClasses: {
        globalClassName: 'DescartesModalDialogCoordinatesInputLabelAndValue',
        labelClassName: 'DescartesModalDialogCoordinatesInputLabel',
        inputClassName: 'DescartesModalDialogInput',
        textClassName: 'DescartesModalDialogText',
        errorClassName: 'DescartesJQueryError'
    },

    /**
     * Constructeur: Descartes.UI.CoordinatesInputDialog
     * Constructeur d'instances
     *
     * Paramètres:
     * div - null.
     * model - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.CoordinatesInput>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     */
    initialize: function (div, model, options) {
        AbstractCoordinatesInput.prototype.initialize.apply(this, [div, model, this.EVENT_TYPES, options]);
    },
    draw: function () {
        var codesvalues = [];
        if (this.model.projection && this.model.displayProjections.length > 0) {
           for (var j = 0, jlen = this.model.displayProjections.length; j < jlen; j++) {
               codesvalues.push({code: this.model.displayProjections[j], value: Utils.getProjectionName(this.model.displayProjections[j])});
           }
        }
        var labels = this.changeLabels(this.model.selectedDisplayProjection);
        var content = CoordinatesInputDialog({
            id: this.id,
            x: labels[0],
            y: labels[1],
            projection: this.model.projection,
            projectionsCodesValues: codesvalues,
            selectedProjection: this.model.selectedDisplayProjectionIndex
        });

        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('DIALOG_TITLE'),
            formClass: 'form-horizontal',
            sendLabel: this.getMessage('OK_BUTTON'),
            size: 'modal-sm',
            content: content
        });
        dialog.open($.proxy(this.done, this));
        var that = this;
        $("#coordinatesproj").change(function (event) {
            var selectedValue = $('#coordinatesproj').val();
            var labels = that.changeLabels(selectedValue);
            console.log($("#coordinates_input_x"));
            $("#coordinates_input_x")[0].textContent = labels[0];
            $("#coordinates_input_y")[0].textContent = labels[1];
            var x = $('#' + that.id + '_input_x')[0].value;
            var y = $('#' + that.id + '_input_y')[0].value;
            if (x !== "" && y !== "") {
                var lonLat = [Number(x.replace(",", ".")), Number(y.replace(",", "."))];
                var nlonLat = ol.proj.transform(lonLat, that.model.selectedDisplayProjection, selectedValue);
                var digits = parseInt(Utils.getProjectionPrecision(selectedValue), 10);
                $('#' + that.id + '_input_x')[0].value = nlonLat[0].toFixed(digits).toString();
                $('#' + that.id + '_input_y')[0].value = nlonLat[1].toFixed(digits).toString();

            }
            that.model.selectedDisplayProjection = selectedValue;
        });
    },
    /**
     * Methode: done
     * Transmet le modèle au controleur, en déclenchant l'événement 'recentrage',
     * ou affiche les messages d'erreur rencontrés.
     */
    done: function (result) {
        this.model.x = Number(result.x.replace(",", "."));
        this.model.y = Number(result.y.replace(",", "."));
        var proj = this.model.mapProjection;
        if (this.model.projection && this.model.displayProjections.length > 1) {
            proj = result.coordinatesproj;
        } else if (this.model.projection && this.model.displayProjections.length === 1) {
            proj = this.model.displayProjections[0];
        }
        this.model.proj = proj;
        if (this.validateDatas()) {
            this.events.triggerEvent('recentrage');
        } else {
            this.showErrors();
            this.draw();
        }
    },

    CLASS_NAME: 'Descartes.UI.CoordinatesInputDialog'
});

module.exports = Class;
