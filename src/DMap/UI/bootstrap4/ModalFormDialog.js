/* global Descartes */
var $ = require('jquery');
var log = require('loglevel');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');
var formDialogTemplate = require('./templates/ModalFormDialog.ejs');

require('../css/Dialog.css');

/**
 * Class: Descartes.UI.ModalFormDialog
 * Classe permettant d'afficher une fenêtre modale.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 */
var Class = Utils.Class(UI, {
    id: null,
    title: null,
    size: null,
    sendLabel: 'OK',
    btnCss: null,
    btnCssSubmit: null,
    formClass: '',
    content: null,
    type: null,
    dialog: null,
    result: null,
    isContentForm: true, // true -> le contenu est un formulaire, false -> le contenu est indépendant
    /**
     * Constructeur: Descartes.UI.ModalFormDialog
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * title - {String} Titre de la boite de dialogue.
     * contenu - {String} Contenu à afficher dans la boite de dialogue.
     */
    initialize: function (options) {
        this.btnCss = Descartes.UIBootstrap4Options.btnCss;
        this.btnCssSubmit = Descartes.UIBootstrap4Options.btnCssSubmit;
        _.extend(this, options);
        UI.prototype.initialize.apply(this, arguments);
    },
    createTemplate: function (callback, cancelCallback) {
        var template = formDialogTemplate({
            id: this.id,
            title: this.title,
            size: this.size,
            formClass: this.formClass,
            sendLabel: this.sendLabel,
            btnCss: this.btnCss,
            btnCssSubmit: this.btnCssSubmit,
            content: this.content,
            isContentForm: this.isContentForm,
            maxHeight: window.innerHeight - 200
        });
        $('body').append(template);
        this.dialog = $('#' + this.id);

        this.dialog.on('shown.bs.modal', function () {
            this.cancel = true;
            this.handleEvents(callback, cancelCallback);
        }.bind(this));
    },
    handleEvents: function (callback, cancelCallback) {
        var that = this;
        $('#' + this.id + '_formDialog').on('submit', function (e) {
            e.preventDefault();
            that.result = Utils.serializeFormArrayToJson($(this).serializeArray());
            that.cancel = false;
            that.dialog.modal('hide');
        });

        this.dialog.on('hide.bs.modal', function (e) {
            log.debug('Closing dialog with result %s and cancel %s', that.result, that.cancel);
            if (that.cancel && !_.isNil(cancelCallback)) {
                cancelCallback();
            } else if (!_.isNil(callback) && !_.isNil(that.result)) {
                if (callback(that.result) === false) {
                    e.preventDefault();
                }
            }
            that.result = null;
        });
        this.dialog.on('hidden.bs.modal', function () {
            $(this).data('modal', null);
            $(this).remove();
        });
    },
    open: function (callback, cancelCallback) {
        this.createTemplate(callback, cancelCallback);
        this.dialog.modal();
    },
    CLASS_NAME: 'Descartes.UI.ModalFormDialog'
});

module.exports = Class;
