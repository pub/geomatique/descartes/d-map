var _ = require('lodash');
var ol = require('openlayers');
var $ = require('jquery');

var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');

var template = require('./templates/ToolTipContainer.ejs');

/**
 * Class: Descartes.UI.ToolTipContainer
 * Classe affichant dans une info-bulle les attributs des objets survolés par le pointeur.
 *
 * Hérite de:
 * - <Descartes.UI>
 */
var Class = Utils.Class(UI, {

    defaultDisplayClasses: {
        globalClassName: 'DescartesToolTip',
        textClassName: 'DescartesToolTipText',
        labelClassName: 'DescartesToolTipText',
        titleClassName: 'DescartesToolTipTitle',
        lienClassName: 'DescartesToolTipLien'
    },

    EVENT_TYPES: [],
    /**
     * Constructeur: Descartes.UI.ToolTipContainer
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'info-bulle.
     * toolTipModel - null.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     */
    initialize: function (div, toolTipModel, options) {
        UI.prototype.initialize.apply(this, [div, toolTipModel, this.EVENT_TYPES, options]);
    },

    /**
     * Methode: draw
     * Construit et affiche l'info-bulle.
     *
     * Paramètres:
     * jsonResult - {Object} Objet JSON contenant les objets survolés avec leurs attributs.
     */
    draw: function (jsonResult) {
        if (!_.isEmpty(jsonResult)) {
            var finalResult = [];
            var that = this;
            _.each(jsonResult, function (aResult) {
                if (!_.isEmpty(aResult.layerDatas)) {
                    for (var i = 0; i < that.toolTipLayers.length; i++) {
                        var toolTipLayer = that.toolTipLayers[i];
                        if (toolTipLayer.layer.id === aResult.layerId) {
                            finalResult.push({
                                title: aResult.layerTitle,
                                fields: toolTipLayer.fields,
                                data: aResult.layerDatas
                            });
                            break;
                        }
                    }
                }
            });

            if (!_.isEmpty(finalResult)) {
                var content = template({
                    globalClassName: this.displayClasses.globalClassName,
                    titleClassName: this.displayClasses.titleClassName,
                    labelClassName: this.displayClasses.labelClassName,
                    textClassName: this.displayClasses.textClassName,
                    lienClassName: this.displayClasses.lienClassName,
                    toolTipLayers: this.toolTipLayers,
                    results: finalResult
                });

                $(this.div).html(content);

                var popup = new ol.Overlay({
                    element: this.div,
                    autoPan: true,
                    autoPanMargin: 100
                });

                $(this.div).mouseenter(function () {
                    that.events.triggerEvent('mouseEnterToolTip');
                });
                $(this.div).mouseleave(function () {
                    that.events.triggerEvent('mouseLeaveToolTip');
                });

                this.events.triggerEvent('showToolTip', [popup]);
            } else {
                this.events.triggerEvent('showToolTip', []);
            }
        } else {
            this.events.triggerEvent('showToolTip', []);
        }
    },

    CLASS_NAME: 'Descartes.UI.ToolTipContainer'
});

module.exports = Class;
