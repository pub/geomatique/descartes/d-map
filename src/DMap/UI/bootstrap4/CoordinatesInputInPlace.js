/* global Descartes */
var ol = require('openlayers');
var $ = require('jquery');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var AbstractInPlace = require('./AbstractInPlace');

var CoordinatesInputInPlace = require('./templates/CoordinatesInputInPlace.ejs');
var AbstractCoordinatesInput = require('../AbstractCoordinatesInput');

/**
 * Class: Descartes.UI.CoordinatesInputInPlace
 * Classe proposant, dans une zone fixe de la page HTML, la saisie d'un couple de coordonnées pour recentrer la carte.
 *
 * Hérite de:
 *  - <Descartes.AbstractInPlace>
 *  - <Descartes.AbstractCoordinatesInput>
 *
 *
 * Evénements déclenchés:
 * recentrage - Les coordonnées sont saisies et valides.
 *
 */
var Class = Utils.Class(AbstractInPlace, AbstractCoordinatesInput, {

    defaultDisplayClasses: {
        buttonClassName: 'DescartesUIButton',
        errorClassName: 'DescartesJQueryError'
    },

    EVENT_TYPES: ['recentrage'],
    /**
     * Constructeur: Descartes.UI.CoordinatesInputInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * model - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.CoordinatesInput>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
     * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceJQueryUI>).
     */
    initialize: function (div, model, options) {
        _.extend(this, options);
        AbstractInPlace.prototype.initialize.apply(this, [div, model, this.EVENT_TYPES, options]);
    },
    draw: function () {
        var that = this;
        var codesvalues = [];
        if (this.model.projection && this.model.displayProjections.length > 0) {
           for (var j = 0, jlen = this.model.displayProjections.length; j < jlen; j++) {
               codesvalues.push({code: this.model.displayProjections[j], value: Utils.getProjectionName(this.model.displayProjections[j])});
           }
        }
        var labels = this.changeLabels(this.model.selectedDisplayProjection);
        var content = CoordinatesInputInPlace({
            id: this.id,
            x: labels[0],
            y: labels[1],
            projection: this.model.projection,
            projectionsCodesValues: codesvalues,
            selectedProjection: this.model.selectedDisplayProjectionIndex,
            btnCssSubmit: this.btnCssSubmit,
            buttonMessage: this.getMessage('BUTTON_MESSAGE')
        });

        this.renderPanel(content);

        $('#' + this.id).submit(function (event) {
            event.preventDefault();
            var x = $('#' + this.id + '_input_x').val();
            var y = $('#' + this.id + '_input_y').val();
            var proj = that.model.mapProjection;
            if (that.model.projection && that.model.displayProjections.length > 1) {
                proj = $('#' + this.id + '_input_proj').val();
            } else if (that.model.projection && that.model.displayProjections.length === 1) {
                proj = that.model.displayProjections[0];
            }

            if (!_.isEmpty(x) && !_.isEmpty(y)) {
                that.selected = [x, y, proj];
                that.done();
            }
        });

        $('#' + this.id + "_input_proj").change(function (event) {
            var selectedValue = $('#' + that.id + '_input_proj').val();
            var labels = that.changeLabels(selectedValue);
            $('#coordinateslabelx')[0].textContent = labels[0];
            $('#coordinateslabely')[0].textContent = labels[1];
            var x = $('#' + that.id + '_input_x')[0].value;
            var y = $('#' + that.id + '_input_y')[0].value;
            if (x !== "" && y !== "") {
                var lonLat = [Number(x.replace(",", ".")), Number(y.replace(",", "."))];
                var nlonLat = ol.proj.transform(lonLat, that.model.selectedDisplayProjection, selectedValue);
                var digits = parseInt(Utils.getProjectionPrecision(selectedValue), 10);
                $('#' + that.id + '_input_x')[0].value = nlonLat[0].toFixed(digits).toString();
                $('#' + that.id + '_input_y')[0].value = nlonLat[1].toFixed(digits).toString();

            }
            that.model.selectedDisplayProjection = selectedValue;
        });
    },
    /**
     * Methode: done
     * Transmet le modèle au controleur, en déclenchant l'événement 'recentrage',
     * ou affiche les messages d'erreur rencontrés.
     */
    done: function () {
        this.model.x = Number(this.selected[0].replace(",", "."));
        this.model.y = Number(this.selected[1].replace(",", "."));
        this.model.proj = this.selected[2];
        if (this.validateDatas()) {
            this.events.triggerEvent('recentrage');
        } else {
            this.showErrors();
        }
    },

    CLASS_NAME: 'Descartes.UI.CoordinatesInputInPlace'
});

module.exports = Class;
