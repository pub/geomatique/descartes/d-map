var $ = require('jquery');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var AbstractInPlace = require('./AbstractInPlace');
var scaleChooserTemplate = require('./templates/ScaleChooser.ejs');

require('../css/SimpleUIs.css');

/**
 * Class: Descartes.UI.ScaleChooserInPlace
 * Classe proposant, dans une zone fixe de la page HTML, la saisie d'une échelle pour recadrer la carte.
 *
 * :
 * Le formulaire de saisie est constitué d'une seule zone de texte.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 *
 * Evénements déclenchés:
 * choosed - L'échelle est saisie et valide.
 */
var Class = Utils.Class(AbstractInPlace, {

    defaultDisplayClasses: {
        textClassName: 'DescartesUIText',
        labelClassName: 'DescartesUILabel',
        titleClassName: 'DescartesUITitle',
        inputClassName: 'DescartesUIInput',
        buttonClassName: 'DescartesUIButton'
    },
    /**
     * Propriete: ascending
     * {Boolean} Indicateur pour l'ordre d'affichage des échelles disponibles.
     */
    ascending: false,

    EVENT_TYPES: ['selected'],

    selected: 0,

    /**
     * Constructeur: Descartes.UI.ScaleChooserInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * scaleModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.ScaleChooser>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
    * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
     * size - {Integer} Taille des zones de saisie (10 par défaut).
     */
    initialize: function (div, scaleModel, options) {
        AbstractInPlace.prototype.initialize.apply(this, [div, scaleModel, this.EVENT_TYPES, options]);
    },
    /**
     * Methode: draw
     * Construit la zone de la page HTML pour la saisie de l'échelle.
     *
     * Paramètres:
     * minScaleDenominator - {Float} Dénominateur de l'échelle minimale autorisée.
     * maxScaleDenominator - {Float} Dénominateur de l'échelle maximale autorisée.
     */
    draw: function (minScaleDenominator, maxScaleDenominator) {
        this.minScaleDenominator = Math.round(minScaleDenominator);
        this.maxScaleDenominator = Math.round(maxScaleDenominator);
        var that = this;

        var content = scaleChooserTemplate({
            id: this.id,
            label: this.getMessage('INVITE_MESSAGE'),
            min: this.minScaleDenominator,
            max: this.maxScaleDenominator,
            btnCssSubmit: this.btnCssSubmit,
            buttonMessage: this.getMessage('BUTTON_MESSAGE')
        });

        this.renderPanel(content);

        $('#' + this.id).submit(function (event) {
            event.preventDefault();
            var value = $('#' + this.id + '_input').val();
            if (!_.isEmpty(value)) {
                that.selected = Number(value);

                that.done();
            }
        });
    },
    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'selected'.
     *
     * Paramètres:
     * control - {DOMElement} Liste déroulante de choix de l'échelle dans le formulaire.
     */
    done: function () {
        if (this.selected > 0) {
            this.model.scale = this.selected;
            this.events.triggerEvent('choosed');
        }
    },

    CLASS_NAME: 'Descartes.UI.ScaleChooserInPlace'
});

module.exports = Class;
