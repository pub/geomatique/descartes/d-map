/* global Descartes */

var $ = require('jquery');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var AbstractInPlace = require('./AbstractInPlace');

var RequestManagerTemplate = require('./templates/RequestManagerInPlace.ejs');

/**
 * Class: Descartes.UI.RequestManagerInPlace
 * Classe proposant, dans une zone fixe de la page HTML, la saisie de critères de requêtes.
 *
 * Hérite de:
 *  - <Descartes.UI.AbstractInPlace>
 *
 * Evénements déclenchés:
 * sendRequest - Les critères d'une requête sont saisis.
 * unselect - Demande l'effacement de la sélection précédente.
 */
var Class = Utils.Class(AbstractInPlace, {

    defaultDisplayClasses: {
        buttonClassName: 'DescartesUIButton',
        separateurClassName: 'DescartesUISeparateur'
    },

    /**
     * Propriete: separateur
     * {Boolean} Indicateur pour l'affichage d'un séparateur de type HR entre chaque requête proposée.
     */
    separateur: true,

    /**
     * Propriete: withChooseExtent
     * {Boolean} Indique si l'utilsateur peut choisir l'étendu de recherche.
     */
    withChooseExtent: false,

    defaultChooseExtentGazetteerConfig: {
       initValue: "", //"" Fr, "93" PACA, "13" BDR
       startLevel: 0, //0 reg, 1 dept, 2 com
       selectpicker: false
    },

    chooseExtentGazetteerConfig: {},

    EVENT_TYPES: ['sendRequest', 'unselect', 'fixSelectionLayer', 'flashSelectionLayer', 'undisplaySelectionLayer', 'addSelectionLayer'],
    /**
     * Constructeur: Descartes.UI.RequestManagerInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * requestManagerModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.RequestManager>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * separateur - {Boolean} Indicateur pour l'affichage d'un séparateur de type HR entre chaque requête proposée.
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone des requêtes.
     * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceJQueryUI>).
     */
    initialize: function (div, requestManagerModel, options) {
        _.extend(this.chooseExtentGazetteerConfig, this.defaultChooseExtentGazetteerConfig);
        _.extend(this, options);
        AbstractInPlace.prototype.initialize.apply(this, [div, requestManagerModel, this.EVENT_TYPES, options]);
    },
    /**
     * Methode: draw
     * Construit la zone de la page HTML pour la saisie des critères de requête.
     *
     * Paramètres:
     * requests - {Array(<Descartes.Request>)} Liste des requêtes proposées.
     */
    draw: function (requests) {
        var that = this;
        if (!_.isEmpty(requests)) {

            var content = RequestManagerTemplate({
                id: this.id,
                requests: requests,
                separateur: this.separateur,
                withChooseExtent: this.withChooseExtent,
                inactiveSelectTitle: this.getMessage('INACTIVE_SELECT_TITLE'),
                btnCss: this.btnCss,
                btnCssSubmit: this.btnCssSubmit,
                buttonMessageRecherche: this.getMessage('BUTTON_MESSAGE_RECHERCHE'),
                buttonMessageActions: this.getMessage('BUTTON_MESSAGE_DROPDOWN'),
                buttonMessageUnselect: this.getMessage('BUTTON_MESSAGE_UNSELECT'),
                buttonMessageFixSelectionLayer: this.getMessage('BUTTON_MESSAGE_FIXSELECTIONLAYER'),
                buttonMessageAddSelectionLayer: this.getMessage('BUTTON_MESSAGE_ADDSELECTIONLAYER'),
                buttonMessageFlashSelectionLayer: this.getMessage('BUTTON_MESSAGE_FLASHSELECTIONLAYER'),
                buttonMessageUndisplaySelectionLayer: this.getMessage('BUTTON_MESSAGE_UNDISPLAYSELECTIONLAYER'),
                buttonMessageActionsTooltip: this.getMessage('BUTTON_MESSAGE_DROPDOWN_TOOLTIP'),
                buttonMessageUnselectTooltip: this.getMessage('BUTTON_MESSAGE_UNSELECT_TOOLTIP'),
                buttonMessageFixSelectionLayerTooltip: this.getMessage('BUTTON_MESSAGE_FIXSELECTIONLAYER_TOOLTIP'),
                buttonMessageAddSelectionLayerTooltip: this.getMessage('BUTTON_MESSAGE_ADDSELECTIONLAYER_TOOLTIP'),
                buttonMessageFlashSelectionLayerTooltip: this.getMessage('BUTTON_MESSAGE_FLASHSELECTIONLAYER_TOOLTIP'),
                buttonMessageUndisplaySelectionLayerTooltip: this.getMessage('BUTTON_MESSAGE_UNDISPLAYSELECTIONLAYER_TOOLTIP'),
                labelSearchExtent: this.getMessage('LABEL_SEARCH_ON_EXTENT'),
                labelExtentGlobal: this.getMessage('LABEL_EXTENT_GLOBAL'),
                labelExtentCurrent: this.getMessage('LABEL_EXTENT_CURRENT'),
                labelExtentEntity: this.getMessage('LABEL_EXTENT_ENTITY'),
                labelFilterLayer: this.getMessage('LABEL_FILTER_LAYER')
            });

            this.renderPanel(content);

            for (var i = 0; i < requests.length; i++) {
                var request = requests[i];
                var formSelector = '#DescartesUIRequest_' + i;
                $(formSelector).submit(function (event) {
                    event.preventDefault();
                    var result = $(this).serializeArray();
                    Utils.computeFormvalues(result);

                    var id = $(this)[0].id;
                    that.done(result, id);
                });
            }

            if (this.withChooseExtent) {
                $('#DescartesRequestsSelect').on('change', function (e) {
                   var id = $(this).val();
                   $('a[href="' + id + '"]').tab('show');
                });
            }
        }
        //disable Actions à l'initialisation'
        this.disableDropdownMenu();

        $('#DescartesRequestUnselectRequest').on('click', function () {
            that.events.triggerEvent('unselect');
            that.disableDropdownMenu();
        });
        $('#DescartesRequestFixSelectionLayer').on('click', function () {
            that.events.triggerEvent('fixSelectionLayer');
        });
        $('#DescartesRequestFlashSelectionLayer').on('click', function () {
            that.events.triggerEvent('flashSelectionLayer');
        });
        $('#DescartesRequestUndisplaySelectionLayer').on('click', function () {
            that.events.triggerEvent('undisplaySelectionLayer');
        });
        $('#DescartesRequestAddSelectionLayer').on('click', function () {
            that.events.triggerEvent('addSelectionLayer');
            that.disableDropdownMenu();
        });

        if (this.withChooseExtent) {
           var gazetteerLevels = [];
           var gazetteerService = null;
           if (this.chooseExtentGazetteerConfig && this.chooseExtentGazetteerConfig.startLevel === Descartes.LocalisationParcellaire.NIVEAU_REGION) {
              gazetteerLevels.push(new Descartes.GazetteerLevel('Choisissez une région', 'Aucune région', 'reg'));
              gazetteerLevels.push(new Descartes.GazetteerLevel('Choisissez un département', 'Aucun département', 'dept'));
              gazetteerLevels.push(new Descartes.GazetteerLevel('Choisissez une commune', 'Aucune commune', 'com'));
              gazetteerService = new Descartes.LocalisationParcellaire(Descartes.LocalisationParcellaire.NIVEAU_REGION);
           } else if (this.chooseExtentGazetteerConfig && this.chooseExtentGazetteerConfig.startLevel === Descartes.LocalisationParcellaire.NIVEAU_DEPARTEMENT) {
              gazetteerLevels.push(new Descartes.GazetteerLevel('Choisissez un département', 'Aucun département', 'dept'));
              gazetteerLevels.push(new Descartes.GazetteerLevel('Choisissez une commune', 'Aucune commune', 'com'));
              gazetteerService = new Descartes.LocalisationParcellaire(Descartes.LocalisationParcellaire.NIVEAU_DEPARTEMENT);
           } else if (this.chooseExtentGazetteerConfig && this.chooseExtentGazetteerConfig.startLevel === Descartes.LocalisationParcellaire.NIVEAU_COMMUNE) {
              gazetteerLevels.push(new Descartes.GazetteerLevel('Choisissez une commune', 'Aucune commune', 'com'));
              gazetteerService = new Descartes.LocalisationParcellaire(Descartes.LocalisationParcellaire.NIVEAU_COMMUNE);
           }
           this.requestGazetteer = new Descartes.Action.Gazetteer('DescartesRequestManagerGazetteer', this.olMap, this.olMap.getView().getProjection(), this.chooseExtentGazetteerConfig.initValue, gazetteerLevels, {service: gazetteerService, selectpicker: this.chooseExtentGazetteerConfig.selectpicker, label: false, view: Descartes.UI.RequestManagerGazetteer });

        }
    },
    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'sendRequest'.
     *
     * Paramètres:
     * elt - {DOMElement} Bouton de recherche activé dans le formulaire.
     */
    done: function (values, formId) {
        this.model.values = values;
        this.model.numRequest = formId.replace('DescartesUIRequest_', '');
        if (this.withChooseExtent) {
            if (!_.isNil($('#DescartesRequestManagerChooseExtent input:checked')) && $('#DescartesRequestManagerChooseExtent input:checked').val() === "1") {
                this.model.useCurrentExtent = false;
                this.model.useGazetteerExtent = null;
            } else if (!_.isNil($('#DescartesRequestManagerChooseExtent input:checked')) && $('#DescartesRequestManagerChooseExtent input:checked').val() === "2") {
                this.model.useCurrentExtent = true;
                this.model.useGazetteerExtent = null;
            } else if (!_.isNil($('#DescartesRequestManagerChooseExtent input:checked')) && $('#DescartesRequestManagerChooseExtent input:checked').val() === "3" && !_.isNil(this.requestGazetteer.model) && !_.isNil(this.requestGazetteer.model.activeZone)) {
                this.model.useGazetteerExtent = this.requestGazetteer.model.activeZone;
                this.model.useCurrentExtent = false;
            } else {
                alert("ERREUR AVEC L'EMPRISE DE RECHERCHE");
                this.model.useCurrentExtent = false;
                this.model.useGazetteerExtent = null;
            }
        } else {
            this.model.useCurrentExtent = $("#DescartesRequestCheckBox_" + this.model.numRequest)[0].checked;
        }
        this.events.triggerEvent('sendRequest');
    },
    activeDropdownMenu: function () {
       if ($('#DescartesRequestDropdownMenu')) {
           $('#DescartesRequestDropdownMenu').prop('disabled', false);
       }
    },
    disableDropdownMenu: function () {
       if ($('#DescartesRequestDropdownMenu')) {
           $('#DescartesRequestDropdownMenu').prop('disabled', true);
       }
    },

    CLASS_NAME: 'Descartes.UI.RequestManagerInPlace'
});

module.exports = Class;
