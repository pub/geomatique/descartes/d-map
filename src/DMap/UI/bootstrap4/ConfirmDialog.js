/* global Descartes */
var _ = require('lodash');
var $ = require('jquery');
var log = require('loglevel');

var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');
var confirmDialogTemplate = require('./templates/ConfirmDialog.ejs');

require('../css/Dialog.css');

var TYPE = {
    DEFAULT: 'default',
    REMOVE: 'remove'
};

/**
 * Class: Descartes.UI.ConfirmDialog
 * Classe permettant d'afficher une boite de dialogue de confirmation.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 */
var ConfirmDialog = Utils.Class(UI, {
    id: null,
    title: null,
    message: null,
    type: null,
    dialog: null,
    result: null,
    btnCss: null,
    btnCssSubmit: null,
    /**
     * Constructeur: Descartes.UI.ConfirmDialog
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * title - {String} Titre de la boite de dialogue.
     * type - {String} Type de la boite de dialogue.
     * message - {String} Message à afficher dans la boite de dialogue.
     */
     initialize: function (options) {
        this.btnCss = Descartes.UIBootstrap4Options.btnCss;
        this.btnCssSubmit = Descartes.UIBootstrap4Options.btnCssRemove;
        this.id = options.id;
        this.title = options.title;
        this.message = options.message;
        this.type = options.type;
        UI.prototype.initialize.apply(this, arguments);
    },
    createTemplate: function (callback) {
        var template = confirmDialogTemplate({
            id: this.id,
            type: this.type,
            title: this.title,
            btnCss: this.btnCss,
            btnCssSubmit: this.btnCssSubmit,
            message: this.message
        });
        $('body').append(template);
        this.dialog = $('#' + this.id);

        this.dialog.on('shown.bs.modal', function () {
            this.handleType(callback);
        }.bind(this));
    },
    handleType: function (callback) {
        var that = this;
        switch (this.type) {
            case TYPE.REMOVE:
                $('#' + this.id + '_remove').on('click', function (e) {
                    that.result = true;
                    that.dialog.modal('hide');
                });
                break;
        }
        this.dialog.on('hide.bs.modal', function (e) {
            log.debug('Closing dialog with result %s', that.result);
            if (!_.isNil(callback)) {
                callback(that.result);
            }
            that.result = null;
        });
        this.dialog.on('hidden.bs.modal', function () {
            $(this).data('modal', null);
            $(this).remove();
        });
    },
    open: function (callback) {
        this.createTemplate(callback);
        this.dialog.modal();
    },
    CLASS_NAME: 'Descartes.UI.ConfirmDialog'
});

module.exports = ConfirmDialog;
