var $ = require('jquery');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var AbstractInPlace = require('./AbstractInPlace');

var AbstractPrinterSetup = require('./AbstractPrinterSetup');

/**
 * Class: Descartes.UI.PrinterSetupInPlace
 * Classe proposant, dans une zone fixe de la page HTML, la saisie des paramètres de mise en page pour l'exportation PDF.
 *
 * Hérite de:
 *  - <Descartes.UI.AbstractInPlace>
 *
 * Evénements déclenchés:
 * choosed - Les paramètres sont saisis et valides.
 */
var Class = Utils.Class(AbstractInPlace, AbstractPrinterSetup, {

    defaultDisplayClasses: {
        globalClassName: 'DescartesPrinterSetupLabelAndValue',
        labelClassName: 'DescartesPrinterSetupLabel',
        inputClassName: 'DescartesPrinterSetupInput',
        textClassName: 'DescartesUIText',
        selectClassName: 'DescartesPrinterSetupSelect',
        fieldSetClassName: 'DescartesPrinterSetupFieldSet',
        legendClassName: 'DescartesPrinterSetupLegend',
        buttonClassName: 'DescartesUIButton',
        errorClassName: 'DescartesJQueryError'
    },
    /**
     * Propriete: errors
     * {Array(String)} Liste des erreurs rencontrés lors des contrôles de surface.
     */
    errors: null,

    /**
     * Propriete: papers
     * {Array(Object)} Objets JSON stockant les propriétés des "papiers" disponibles en cohérence avec la taille de la carte.
     */
    papers: [],

    /**
     * Propriete: size
     * {Integer} Taille des zones de saisie (10 par défaut).
     */
    size: 10,

    EVENT_TYPES: ['choosed'],

    /**
     * Constructeur: Descartes.UI.PrinterSetupInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * paramsModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.PrinterParamsManager>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone des paramètres d'impression.
     * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlaceJQueryUI>).
     */
    initialize: function (div, paramsModel, options) {
        _.extend(this, options);
        AbstractInPlace.prototype.initialize.apply(this, [div, paramsModel, this.EVENT_TYPES, options]);
        AbstractPrinterSetup.prototype.initialize.apply(this, [div, paramsModel, this.EVENT_TYPES, options]);
    },
    /**
     * Methode: draw
     * Construit la zone de la page HTML pour la saisie des paramètres de mise en page.
     */
    draw: function () {
        this.setPapers();
        var that = this;

        var content = '<form id="' + this.id + '" class="form-horizontal">';
        content += this.populateForm();
        content += '<button type="submit" class="DescartesUIButtonSubmit btn ' + this.btnCssSubmit + '">';
        content += this.getMessage('BUTTON_MESSAGE');
        content += '</button>';
        content += '</form>';

        this.renderPanel(content);

        $('#' + this.id).submit(function (event) {
            event.preventDefault();
            that.result = Utils.serializeFormArrayToJson($(this).serializeArray());
            that.done();
        });

    },

    /**
     * Methode: redraw
     * Reconstruit la zone de la page HTML pour la saisie des paramètres de mise en page en cas de changement de taille de la carte.
     *
     * Paramètres:
     * paramsModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.PrinterParamsManager>.
     */
    redraw: function (paramsModel) {
        this.model = paramsModel;
        this.draw();
    },

    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'choosed', ou affiche les messages d'erreur rencontrés.
     */
    done: function () {
        if (this.validateDatas()) {
            this.events.triggerEvent('choosed');
        } else {
            this.showErrors();
        }
    },

    /**
     * Methode: setPapers
     * Construit la liste des "papiers" disponibles à partir du modèle transmis par le contrôleur.
     */
    setPapers: function () {
        this.papers = [];
        for (var i = 0, len = this.model.enabledFormats.length; i < len; i++) {
            var format = this.model.enabledFormats[i];
            var text = format.code.substring(0, format.code.length - 1);
            text += (format.code.substring(format.code.length - 1) === 'L') ? this.getMessage('LANDSCAPE_OPTION') : this.getMessage('PORTRAIT_OPTION');
            this.papers.push({
                value: format.code,
                text: text,
                paperWidth: format.width,
                paperHeight: format.height
            });
        }
    },

    CLASS_NAME: 'Descartes.UI.PrinterSetupInPlace'
});

module.exports = Class;
