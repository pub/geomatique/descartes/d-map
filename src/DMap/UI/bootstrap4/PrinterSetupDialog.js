var $ = require('jquery');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var ModalFormDialog = require('./ModalFormDialog');

var AbstractPrinterSetup = require('./AbstractPrinterSetup');

/**
 * Class: Descartes.UI.PrinterSetupDialog
 * Classe proposant, sous forme de boite de dialogue, la saisie des paramètres de mise en page pour l'exportation PDF.
 *
 * Hérite de:
 *  - <Descartes.UI.AbstractPrinterSetup>
 *
 * Evénements déclenchés:
 * choosed - Les paramètres sont saisis et valides.
 *
 * Ecouteurs mis en place:
 *  - l'événement 'done' de la classe <Descartes.ModalDialog> déclenche la méthode <sendDatas>.
 *  - l'événement 'cancelled' de la classe <Descartes.ModalDialog> déclenche la méthode <deleteDialog>.
 */
var Class = Utils.Class(AbstractPrinterSetup, {

    defaultDisplayClasses: {
        globalClassName: 'DescartesModalDialogPrinterSetupLabelAndValue',
        labelClassName: 'DescartesModalDialogPrinterSetupLabel',
        inputClassName: 'DescartesModalDialogInput',
        textClassName: 'DescartesModalDialogText',
        selectClassName: 'DescartesModalDialogSelect',
        fieldSetClassName: 'DescartesModalDialogFieldSet',
        legendClassName: 'DescartesModalDialogLegend'
    },
    /**
     * Constructeur: Descartes.UI.PrinterSetupDialog
     * Constructeur d'instances
     *
     * Paramètres:
     * div - null.
     * paramsModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.PrinterParamsManager>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     */
    initialize: function (div, paramsModel, options) {
        _.extend(this, options);
        AbstractPrinterSetup.prototype.initialize.apply(this, [div, paramsModel, options]);
    },
    /**
     * Methode: draw
     * Construit la zone de la page HTML pour la saisie des paramètres de mise en page.
     */
    draw: function () {
        this.setPapers();

        var content = this.populateForm();

        if (!_.isNil(this.modalDialog)) {
            this.modalDialog.dialog.modal('hide');
        }

        this.modalDialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.getMessage('DIALOG_TITLE'),
            formClass: 'form-horizontal',
            sendLabel: this.getMessage('OK_BUTTON'),
            //size: 'modal-lg',
            content: content
        });

        var done = this.done.bind(this);
        var cancel = this.cancel.bind(this);
        this.modalDialog.open(done, cancel);
    },
    /**
     * Methode: redraw
     * Reconstruit la zone de la page HTML pour la saisie des paramètres de mise en page en cas de changement de taille de la carte.
     *
     * Paramètres:
     * paramsModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.PrinterParamsManager>.
     */
    redraw: function (paramsModel) {
        if (!_.isNil(this.modalDialog)) {
            this.model = paramsModel;
            this.draw();
        }
    },
    cancel: function () {
        this.modalDialog = null;
    },

    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'choosed', ou affiche les messages d'erreur rencontrés.
     */
    done: function (result) {
        this.result = result;
        if (this.validateDatas()) {
            this.modalDialog = null;
            this.events.triggerEvent('choosed');
        } else {
            this.showErrors();
        }
    },
    CLASS_NAME: 'Descartes.UI.PrinterSetupDialog'
});

module.exports = Class;
