var _ = require('lodash');
var $ = require('jquery');

var Utils = require('../../Utils/DescartesUtils');
var AbstractInPlace = require('./AbstractInPlace');

var localisationAdresseInPlaceTemplate = require('./templates/LocalisationAdresseInPlace.ejs');

require('../css/SimpleUIs.css');
require('../css/popup.css');

/**
 * Class: Descartes.UI.LocalisationAdresseInPlace
 * Classe proposant, dans une zone fixe de la page HTML, la saisie d'une adresse pour la localiser sur la carte.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 *
 * Evénements déclenchés:
 * recherche - L'adresse est saisie et valide.
 * localise - Un objet de référence a été choisi.
 * efface - Demande l'effacement de la recherche précédente.
 */
var Class = Utils.Class(AbstractInPlace, {

    EVENT_TYPES: ["recherche", "localise", "efface"],

    defaultDisplayClasses: {
        textClassName: "DescartesUIText",
        labelClassName: "DescartesUILabel",
        inputClassName: "DescartesUIInput",
        titleClassName: "DescartesUITitle",
        buttonClassName: "DescartesUIButton",
        datasTableClassName: "DescartesDataGridDatasTable",
        ascendingOrderClassName: "DescartesDataGridAscendingOrder",
        descendingOrderClassName: "DescartesDataGridDescendingOrder",
        headerClassName: "DescartesDataGridHeader",
        colHeaderClassName: "DescartesDataGridHeader",
        evenLocalizeCellClassName: "DescartesDataGridEvenLocalizeCell",
        oddLocalizeCellClassName: "DescartesDataGridOddLocalizeCell",
        evenCellClassName: "DescartesDataGridEvenCell",
        oddCellClassName: "DescartesDataGridOddCell",
        localizeHeaderClassName: "DescartesDataGridLocalizeHeader"
    },

    waitingMessageClassName: "DescartesQueryResultWaitingMessage",

    waitingMaskClassName: "DescartesQueryResultWaitingMask",

    inputSize: 35,

    /**
     * Constructeur: Descartes.UI.LocalisationAdresseInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * model - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.RequestManager>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone des requêtes.
     */
    initialize: function (div, model, options) {
        _.extend(this, options);
        AbstractInPlace.prototype.initialize.apply(this, [div, model, this.EVENT_TYPES, options]);
    },

    /**
     * Methode: draw
     * Construit la zone de la page HTML pour le choix d'un objet de référence.
     */
    draw: function () {
        var that = this;

        var content = localisationAdresseInPlaceTemplate({
            id: this.id,
            labelAdresseMessage: this.getMessage("LABEL_ADRESSE_MESSAGE"),
            labelCommuneMessage: this.getMessage("LABEL_COMMUNE_MESSAGE"),
            buttonSearchMessage: this.getMessage('BUTTON_SEARCH_MESSAGE'),
            buttonClearMessage: this.getMessage('BUTTON_CLEAR_MESSAGE'),
            btnCss: this.btnCss,
            btnCssSubmit: this.btnCssSubmit,
            waitMessage: this.getMessage("WAIT_MESSAGE")
        });

        this.renderPanel(content);

        $('#' + this.id).submit(function (event) {
            event.preventDefault();
            var criteria = Utils.serializeFormArrayToJson($(this).serializeArray());
            that.recherche(criteria);
        });

        $('#' + this.id + '_clear').on('click', function (event) {
            event.preventDefault();
            $('#' + that.id)[0].reset();
            that.clear();
        });
    },

    /**
     * Methode: showResults
     * Construit la zone de la page HTML pour afficher la liste des résultats.
     */
    showResults: function () {
        var that = this;
        this.closeWaitMessage();

        var result = $('#' + this.id + '_result');
        result.empty();

        var header = document.createElement('span');
        header.id = 'headerLocalisationAdresse';
        header.innerHTML = this.getNbPropositions(this.model.resultats.length);
        result.append(header);

        if (this.model.resultats.length > 0) {
            result.append('<div class="list-group"></div>');
            var list = result.children('.list-group');
            list.empty();
            _.each(this.model.resultats, function (resultat, index) {
                var text = '';
                if (!_.isEmpty(resultat.adresse)) {
                    text = resultat.adresse;
                    text += ' ';
                }
                text += resultat.commune;
                var item = document.createElement('a');
                item.href = '#';
                item.setAttribute('data-index', index);
                item.className = 'list-group-item';
                item.onclick = function () {
                    that.geocode(this);
                };
                item.innerHTML = text;
                list.append(item);
            });
        }

        result.show();
    },

    getNbPropositions: function (lenRes) {
        var nbPropositions = '';
        if (lenRes === 0) {
            nbPropositions = this.getMessage('RESULT1_MESSAGE');
        } else if (lenRes === 1) {
            nbPropositions = lenRes + ' ' + this.getMessage('RESULT2_MESSAGE');
        } else {
            nbPropositions = lenRes + ' ' + this.getMessage('RESULT2_MESSAGE') + "s";
        }
        return nbPropositions;
    },

    /**
     * Methode: recherche
     * Lance la recherche.
     */
    recherche: function (criteria) {
        this.model.adresse = criteria.choosedAdresse;
        if (this.model.adresse === this.getMessage('DEFAULT_ADRESSE_MESSAGE')) {
            this.model.adresse = '';
        }
        this.model.commune = criteria.choosedCommune;

        this.clear();
        this.showWaitMessage();

        if (this.validateDatas()) {
            this.events.triggerEvent("recherche");
        } else {
            this.closeWaitMessage();
            this.showErrors();
        }
    },

    /**
     * Methode: showWaitMessage
     * Affiche le message d'attente.
     */
    showWaitMessage: function () {
        var waitDiv = $('#' + this.id + '_wait');
        waitDiv.show();
    },

    /**
     * Methode: closeWaitMessage
     * Ferme le message d'attente.
     */
    closeWaitMessage: function () {
        var waitDiv = $('#' + this.id + '_wait');
        waitDiv.hide();
    },

    /**
     * Methode: validateDatas
     * Vérification des informations saisies.
     */
    validateDatas: function () {
        this.errors = [];
        if (_.isEmpty(this.model.commune)) {
            this.errors.push(this.getMessage('ERROR_FORMAT_MESSAGE'));
        }
        return (this.errors.length === 0);
    },

    showErrors: function () {
        if (this.errors.length !== 0) {
            var errorsMessage = this.getMessage('ERRORS_LIST');
            for (var i = 0, len = this.errors.length; i < len; i++) {
                errorsMessage += '\n\t- ' + this.errors[i];
            }
            alert(errorsMessage);
        }
    },

    /**
     * Methode: clear
     * Supprime la zone de la page HTML pour l'affichage des résultats.
     */
    clear: function () {
        var result = $('#' + this.id + '_result');
        var list = result.children('.list-group');
        list.empty();
        result.hide();

        this.closeWaitMessage();

        this.events.triggerEvent("efface");
    },

    /**
     * Methode: geocode
     * Lance la localisation de l'adresse sélectionnée.
     */
    geocode: function (elt) {
        this.model.resultatActif = elt.getAttribute('data-index');
        this.events.triggerEvent("localise");
    },

    CLASS_NAME: 'Descartes.UI.LocalisationAdresseInPlace'
});

module.exports = Class;
