/* global Descartes */
var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');
var ConfirmDialog = require('./ConfirmDialog');

var printerSetupTemplate = require('./templates/PrinterSetup.ejs');

/**
 * Class: Descartes.UI.AbstractPrinterSetup
 * Classe "abstraite" proposant la saisie des paramètres de mise en page pour l'exportation PDF.
 *
 * :
 * Le formulaire de saisie est constitué de zones de texte (marges) et d'une liste déroulante (format).
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 * Classes dérivées:
 *  - <Descartes.UI.PrinterSetupDialog>
 *  - <Descartes.UI.PrinterSetupInPlace>
 */
var Class = Utils.Class(UI, {
    /**
     * Propriete: errors
     * {Array(String)} Liste des erreurs rencontrés lors des contrôles de surface.
     */
    errors: null,

    /**
     * Propriete: form
     * {DOMElement} Elément DOM correspondant au formulaire de saisie.
     */
    form: null,

    /**
     * Propriete: papers
     * {Array(Object)} Objets JSON stockant les propriétés des "papiers" disponibles en cohérence avec la taille de la carte.
     */
    papers: [],

    /**
     * Propriete: size
     * {Integer} Taille des zones de saisie (10 par défaut).
     */
    size: 10,

    panelCss: null,

    EVENT_TYPES: ["choosed"],
    /**
     * Constructeur: Descartes.UI.AbstractPrinterSetup
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * paramsModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.PrinterParamsManager>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * size - {Integer} Taille des zones de saisie (10 par défaut).
     */
    initialize: function (div, paramsModel, options) {
        this.panelCss = Descartes.UIBootstrap4Options.panelCss;
        UI.prototype.initialize.apply(this, [div, paramsModel, this.EVENT_TYPES, options]);
    },
    /**
     * Methode: setPapers
     * Construit la liste des "papiers" disponibles à partir du modèle transmis par le contrôleur.
     */
    setPapers: function () {
        this.papers = [];
        for (var i = 0, len = this.model.enabledFormats.length; i < len; i++) {
            var format = this.model.enabledFormats[i];
            var text = format.code.substring(0, format.code.length - 1);
            text += (format.code.substring(format.code.length - 1) === "L") ? this.getMessage("LANDSCAPE_OPTION") : this.getMessage("PORTRAIT_OPTION");
            this.papers.push({
                value: format.code,
                text: text,
                paperWidth: format.width,
                paperHeight: format.height
            });
        }
    },
    /**
     * Methode: populateForm
     * Construit les zones de saisie du formulaire.
     */
    populateForm: function () {
        var content = printerSetupTemplate({
            panelCss: this.panelCss,
            paperInput: this.getMessage("PAPER_INPUT"),
            papers: this.papers,
            marginsGroupInput: this.getMessage("MARGINS_GROUP_INPUT"),
            marginTopInput: this.getMessage("MARGIN_TOP_INPUT"),
            marginTop: this.model.marginTop,
            marginBottomInput: this.getMessage("MARGIN_BOTTOM_INPUT"),
            marginBottom: this.model.marginBottom,
            marginLeftInput: this.getMessage("MARGIN_LEFT_INPUT"),
            marginLeft: this.model.marginLeft,
            marginRightInput: this.getMessage("MARGIN_RIGHT_INPUT"),
            marginRight: this.model.marginRight
        });
        return content;

    },
    /**
     * Methode: validateDatas
     * Effectue les contrôles de surface après la saisie.
     *
     * :
     * Met à jour le modèle si ceux-ci sont assurés.
     *
     * :
     * Alimente la liste des erreurs dans le cas contraire.
     */
    validateDatas: function () {
        this.errors = [];
        this.model.paper = this.result.paper;
        var validMargins = true;
        if (this.result.marginTopInput !== '') {
            this.model.marginTop = parseInt(this.result.marginTopInput, 10);
            if (isNaN(this.model.marginTop) || this.model.marginTop < 0) {
                this.errors.push(this.getMessage('MARGIN_TOP_ERROR'));
                validMargins = false;
            }
        }
        if (this.result.marginBottomInput !== '') {
            this.model.marginBottom = parseInt(this.result.marginBottomInput, 10);
            if (isNaN(this.model.marginBottom) || this.model.marginBottom < 0) {
                this.errors.push(this.getMessage('MARGIN_BOTTOM_ERROR'));
                validMargins = false;
            }
        }
        if (this.result.marginLeftInput !== '') {
            this.model.marginLeft = parseInt(this.result.marginLeftInput, 10);
            if (isNaN(this.model.marginLeft) || this.model.marginLeft < 0) {
                this.errors.push(this.getMessage('MARGIN_LEFT_ERROR'));
                validMargins = false;
            }
        }
        if (this.result.marginRightInput !== '') {
            this.model.marginRight = parseInt(this.result.marginRightInput, 10);
            if (isNaN(this.model.marginRight) || this.model.marginRight < 0) {
                this.errors.push(this.getMessage('MARGIN_RIGHT_ERROR'));
                validMargins = false;
            }
        }

        if (validMargins === true) {
            var lMap = parseFloat(Math.round(this.model.mapWidth * 25.4 / 96));
            var hMap = parseFloat(Math.round(this.model.mapHeight * 25.4 / 96));

            var lPage;
            var hPage;
            for (var i = 0, len = this.papers.length; i < len; i++) {
                var paper = this.papers[i];
                if (paper.value === this.model.paper) {
                    lPage = paper.paperWidth;
                    hPage = paper.paperHeight;
                }
            }
            var deltaL = (lMap + this.model.marginLeft + this.model.marginRight) - lPage;
            var deltaH = (hMap + this.model.marginTop + this.model.titleHeight + this.model.marginBottom) - hPage;
            if (deltaL > 0) {
                this.errors.push(this.getMessage('MARGIN_WIDTH_ERROR') + deltaL.toString() + ' mm' + this.getMessage('MARGINS_TIP'));
            }
            if (deltaH > 0) {
                this.errors.push(this.getMessage('MARGIN_HEIGHT_ERROR') + deltaH.toString() + ' mm' + this.getMessage('MARGINS_TIP'));
            }
        }
        return (this.errors.length === 0);
    },

    /**
     * Methode: showErrors
     * Affiche la liste des erreurs rencontrés lors des contrôles de surface.
     */
    showErrors: function () {
        if (this.errors.length !== 0) {

            var errorsMessage = '<ul>';
            for (var i = 0, len = this.errors.length; i < len; i++) {
                errorsMessage += '<li>' + this.errors[i] + '</li>';
            }
            errorsMessage += '</ul>';
            var dialog = new ConfirmDialog({
                id: this.id + '_confirmDialog',
                title: this.getMessage('ERRORS_LIST'),
                message: errorsMessage,
                type: 'default'
            });
            dialog.open();
        }
    },

    CLASS_NAME: 'Descartes.UI.AbstractPrinterSetup'
});

module.exports = Class;
