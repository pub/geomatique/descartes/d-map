var $ = require('jquery');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var AbstractInPlace = require('./AbstractInPlace');
var Size = require('../../Utils/DescartesSize');

var sizeTemplate = require('./templates/Size.ejs');

/**
 * Class: Descartes.UI.SizeSelectorInPlace
 * Classe proposant, dans une zone fixe de la page HTML, le choix d'une taille de carte dans une liste.
 *
 * :
 * Le formulaire de saisie est constitué d'une seule liste déroulante.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 *
 * Evénements déclenchés:
 * changeSize - La taille sélectionnée dans la liste a changé.
 */
var Class = Utils.Class(AbstractInPlace, {

    defaultDisplayClasses: {
        textClassName: 'DescartesUIText',
        titleClassName: 'DescartesUITitle',
        selectClassName: 'DescartesUISelect'
    },

    EVENT_TYPES: ['changeSize'],

    separator: 'x',
    /**
     * Constructeur: Descartes.UI.SizeSelectorInPlace
     * Constructeur d'instances
     *
     * Paramétres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * sizeSelectorModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.SizeSelector>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
     */
    initialize: function (div, sizeSelectorModel, options) {
        _.extend(this, options);
        AbstractInPlace.prototype.initialize.apply(this, [div, sizeSelectorModel, this.EVENT_TYPES, options]);
    },
    draw: function (sizeList, defaultSize) {
        var that = this;
        var sizes = [];
        if (sizeList === 'auto') {
            sizes.push({
                label: 'Auto',
                value: '',
                selected: 0
            });
        } else {
            _.each(sizeList, function (size, index) {
                var label = size[0] + that.separator + size[1];
                var value = label;
                sizes.push({
                    label: label,
                    value: value,
                    selected: index === defaultSize
                });
            });

            this.selected = sizes[defaultSize];
        }

        var content = sizeTemplate({
            id: this.id,
            sizes: sizes
        });

        this.renderPanel(content);

        $('#' + this.id).on('click', function () {
            that.selected = $(this).val();
            that.done();
        });
    },
    done: function () {
        var selection = null;
        if (!_.isNil(this.selected) && !_.isNil(this.selected.value)) {
            selection = this.selected.value;
        } else {
            selection = this.selected;
        }
        if (_.isString(selection)) {
            var split = selection.split(this.separator);
            this.model.size = new Size(split[0], split[1]);
            this.events.triggerEvent('changeSize');
        }
    },

    CLASS_NAME: 'Descartes.UI.SizeSelectorInPlace'
});

module.exports = Class;
