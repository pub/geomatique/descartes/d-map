var $ = require('jquery');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var AbstractInPlace = require('./AbstractInPlace');
var scaleTemplate = require('./templates/ScaleSelector.ejs');

/**
 * Class: Descartes.UI.ScaleSelectorInPlace
 * Classe proposant, dans une zone fixe de la page HTML, le choix d'une échelle dans une liste pour recadrer la carte.
 *
 * :
 * Le formulaire de saisie est constitué d'une seule liste déroulante.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 *
 * Evénements déclenchés:
 * selected - Une échelle est sélectionnée dans la liste.
 */
var Class = Utils.Class(AbstractInPlace, {

    defaultDisplayClasses: {
        textClassName: 'DescartesUIText',
        selectClassName: 'DescartesUISelect'
    },

    /**
     * Propriete: ascending
     * {Boolean} Indicateur pour l'ordre d'affichage des échelles disponibles.
     */
    ascending: false,

    EVENT_TYPES: ['selected'],

    selected: 0,

    enableScales: null,

    /**
     * Constructeur: Descartes.UI.ScaleSelectorInPlace
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'ihm.
     * scaleModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.ScaleSelector>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de saisie.
     * ascending - {Boolean} Indicateur pour l'ordre d'affichage des échelles disponibles.
     * optionsPanel - {Objet} Options du panel (voir <Descartes.UI.AbstractInPlace>).
     */
    initialize: function (div, scaleModel, options) {
        _.extend(this, options);
        AbstractInPlace.prototype.initialize.apply(this, [div, scaleModel, this.EVENT_TYPES, options]);
    },
    draw: function (enableScales) {
        this.enableScales = enableScales;
        if (this.ascending) {
            enableScales.sort(function (a, b) {
                return b - a;
            });
        } else {
            enableScales.sort(function (a, b) {
                return a - b;
            });
        }

        var scales = [{
                label: this.getMessage('DEFAULT_OPTION_MESSAGE'),
                value: 0
            }];
        _.each(enableScales, function (enableScale) {
            scales.push({
                label: Utils.formatScale(enableScale),
                value: enableScale
            });
        });

        var content = scaleTemplate({
            id: this.id,
            scales: scales
        });

        this.renderPanel(content);

        var that = this;
        $('#' + this.id).on('click', function () {
            that.selected = $(this).val();
            that.done();
        });
    },
    /**
     * Methode: done
     * Transmet le modèle au contrôleur, en déclenchant l'événement 'selected'.
     *
     * Paramètres:
     * control - {DOMElement} Liste déroulante de choix de l'échelle dans le formulaire.
     */
    done: function () {
        if (this.selected > 0) {
            this.model.scale = this.selected;
            this.events.triggerEvent('selected');
            this.draw(this.enableScales);
        }
    },

    CLASS_NAME: 'Descartes.UI.ScaleSelectorInPlace'
});

module.exports = Class;
