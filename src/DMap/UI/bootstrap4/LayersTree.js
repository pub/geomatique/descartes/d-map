/* global Descartes */

require('../css/LayersTree.css');
require('../css/jstree-style-custom.css');
require('bootstrap-slider/dist/css/bootstrap-slider.css');

var _ = require('lodash');
var jQuery = require('jquery');
var log = require('loglevel');
var Slider = require("bootstrap-slider");

var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');
var UIConstants = require('../UIConstants');
var Group = require('../../Model/Group');
var Layer = require('../../Model/Layer');

var treeItemTemplate = require('./templates/TreeItem.ejs');

var AjaxSelection = require('../../Action/AjaxSelection');
var ExternalCallsUtils = require('../../Core/ExternalCallsUtils');
var ExternalCallsUtilsConstants = require('../../Core/ExternalCallsUtilsConstants');

var TabbedDataGrids = require('./TabbedDataGrids');
var ResultLayer = require('../../Model/ResultLayer');
/**
 * Class: Descartes.UI.LayersTree
 * Classe affichant une arborescence du contenu de la carte et offrant des interactions sur celui-ci.
 *
 * Hérite de:
 * - <Descartes.UI>
 *
 * Evénements déclenchés:
 * layerSelected - Une couche a été sélectionnée.
 * groupSelected - Un groupe a été sélectionné.
 * nodeUnselect - Une couche ou une couche a été désélectionné.
 * rootSelected - La racine a été sélectionnée.
 */
var Class = Utils.Class(UI, {
    tree: null,
    treeConfig: null,
    canRefresh: false,
    sliders: [],
    EVENT_TYPES: ['layerSelected', 'groupSelected', 'nodeUnselect', 'rootSelected', 'treeUserEvent'],
    savItemsOpen: [],
    dragTextNodesId: [],
    dropTextNodesId: [],
    dropInactiveTextNodesId: [],
    dropBeforeNodesId: [],
    dropAfterNodesId: [],
    opacitySliders: [],
    ddDropPosition: null,
    defaultDisplayClasses: {
        tree: 'Descartes-jstree',
        treeRoot: 'fa fa-globe',
        loadErrorClassName: 'fa fa-exclamation-triangle',
        groupOpened: 'DescartesLayersTreeFolderOpened',
        groupClosed: 'DescartesLayersTreeFolderClosed',
        groupLayersVisibilityOn: 'DescartesLayersTreeGroupVisible',
        groupLayersVisibilityOff: 'DescartesLayersTreeGroupHidden',
        groupLayersVisibilityMixed: 'DescartesLayersTreeGroupMixed',
        itemSelectedText: 'DescartesLayersTreeTextSelected',
        itemText: 'DescartesLayersTreeText',
        layerVisibilityOn: 'fa fa-eye',
        layerVisibilityOff: 'fa fa-eye-slash',
        layerVisibilityOnForbidden: 'fa fa-eye DescartesLayersTreeIconColorForbidden',
        layerVisibilityNeedZoomOut: 'fa fa-minus DescartesLayersTreeBt4LayerZoomOut',
        layerVisibilityNeedZoomIn: 'fa fa-plus DescartesLayersTreeBt4LayerZoomIn',
        layerInfoEnabled: 'fa fa-info-circle DescartesLayersTreeIconColor',
        layerInfoDisabled: 'fa fa-ban DescartesLayersTreeIconColor',
        layerInfoForbidden: 'fa fa-ban DescartesLayersTreeIconColorForbidden',
        layerSheetActive: 'fa fa-th-list DescartesLayersTreeIconColor',
        layerSheetInactive: 'fa fa-th-list DescartesLayersTreeIconColorForbidden',
        layerMoreInfos: 'fa fa-chevron-down',
        layerMoreInfoLegend: 'fa fa-image',
        blankBlocQuote: 'DescartesLayersTreeBlockQuote',
        layerOpacitySliderTrack: 'DescartesLayersTreeLayerSliderTrack',
        layerOpacitySliderHandle: 'DescartesLayersTreeLayerSliderHandle',
        layerLienMetadata: 'DescartesLayersTreeIconColor fa fa-question-circle',
        layerNoLienMetadata: 'DescartesLayersTreeIconColorForbidden fa fa-question-circle',
        layerLoading: 'DescartesLayersTreeLayerLoading'
    },
    /**
     * Propriete: defaultResultUiParams
     * {Object} Objet JSON stockant les propriétés par défaut nécessaires à la génération du tableau des propriétes de l'ensemble des objets d'une couche.
     *
     * :
     * type - <Descartes.UI.TYPE_WIDGET>
     * view - <Descartes.UI.ToolTipContainer>
     * div - null
     * format - "JSON"
     * withReturn - false
     * withCsvExport - false
     * withResultLayerExport - false
     * withUIExports - false
     * withAvancedView - false
     * withFilterColumns - false
     * withListResultLayer - false
     * withPagination - false
     * withTabs - false
     */
    defaultResultUiParams: {
        type: UIConstants.TYPE_WIDGET,
        view: null,
        div: null,
        format: "JSON",
        withReturn: false,
        withCsvExport: false,
        withResultLayerExport: false,
        withUIExports: false,
        withAvancedView: false,
        withFilterColumns: false,
        withListResultLayer: false,
        withPagination: false,
        withTabs: false
    },
    /**
     * Propriete: resultUiParams
     * {Object} Objet JSON stockant les propriétés nécessaires à la génération du tableau des propriétes de l'ensemble des objets d'une couche grâce à la classe <Descartes.Action.AjaxSelection>.
     *
     * :
     * Construit à partir de defaultResultUiParams, puis surchargé en présence d'options complémentaires.
     *
     * :
     * type - <Descartes.UI.TYPE_WIDGET> | <Descartes.UI.TYPE_INPLACEDIV> | <Descartes.UI.TYPE_POPUP> : Type de vue pour le tableau
     * view - {<Descartes.UI>} : Classe de la vue si le type est <Descartes.UI.TYPE_WIDGET>
     * div - {DOMElement|String} Elément DOM de la page accueillant le tableau ou identifiant de cet élement si le type est <Descartes.UI.TYPE_WIDGET> ou <Descartes.UI.TYPE_INPLACEDIV>.
     * format - "JSON"|"HTML"|"XML" : format de retour des informations souhaité.
     * withReturn - {Boolean} Indicateur pour la présence de bouton de localisation rapide sur chaque objet.
     * withCsvExport - {Boolean} Indicateur pour la possibilité d'exporter le tableau sous forme de fichier CSV (côté serveur).
     * withResultLayerExport - {Boolean} : Indique si le résultat doit proposer une exportation des objets retournés avec les géométries.
     * withUIExports - {Boolean} Indicateur pour la possibilité d'exporter le tableau (côté client).
     * withAvancedView - {Boolean} : Indique si le résultat doit être affiché dans une vue (UI) avancée.
     * withFilterColumns - {Boolean} : Indicateur pour la possibilité de filtrer les colonnes.
     * withListResultLayer - {Boolean} : Indicateur pour la possibilité d'affichage de la liste des couches'.
     * withPagination - {Boolean} : Indicateur pour la possibilité d'afficher la pagination'.
     * withTabs - {Boolean} : Indicateur pour la possibilité d'afficher les onglets'.
     */
    resultUiParams: null,
    /**
     * Propriete: defaultResultUiParams
     * {Object} Objet JSON stockant les propriétés par défaut nécessaires à la génération du tableau des propriétes de l'ensemble des objets d'une couche.
     */
    defaultMoreInfosUiParams: {
		fctOpacity: false,
		fctDisplayLegend: false,
		fctQueryable: false,
		fctMetadataLink: false,
		displayOnClickLayerName: false
    },
    /**
     * Propriete: moreInfosUiParams
     * {Object} Objet JSON stockant les propriétés nécessaires à l'affichage des éléments dans le panneau "MoreInfos".
     *
     * :
     * Construit à partir de defaultMoreInfosUiParams, puis surchargé en présence d'options complémentaires.
     *
     */
    moreInfosUiParams: null,
    /**
     * Propriete: nodeRootTitle
     * {String} Titre du noeud principal dans l'arborescence.
     */
    nodeRootTitle: null,
    /**
     * Propriete: selectedItem
     * {String} Index hiérarchique du groupe ou de la couche sélectionné dans l'arborescence.
     */
    selectedItem: '',
    /**
     * Propriete: data
     * {Object} Objet JSON contenant les groupes et couches à afficher au format jsTree.
     */
    data: null,
    /**
     * Propriete: jstreeTheme
     * {String} Nom du thème utilisé par jstree.
     */
    jstreeTheme: 'descartes',
    /**
     * Propriete: jstreeContextMenuItems
     */
    jstreeContextMenuItems: null,
    /**
     * Propriete: jstreeContextMenuItemsCustomFct
     */
    jstreeContextMenuItemsCustomFct: null,

    initialize: function (div, layersModel, options) {
        this.resultUiParams = _.extend({}, this.defaultResultUiParams);
        this.moreInfosUiParams = _.extend({}, this.moreInfosUiParams);
        this.savItemsOpen = [];

        if ((options !== undefined)) {
            _.extend(this.resultUiParams, options.resultUiParams);
            delete options.resultUiParams;

            _.extend(this.moreInfosUiParams, options.moreInfosUiParams);
            delete options.moreInfosUiParams;

            this.waitingMsg = options.waitingMsg;
            delete options.waitingMsg;

            this.nodeRootTitle = options.nodeRootTitle;
            delete options.nodeRootTitle;

            this.jstreeContextMenuItemsCustomFct = options.contextMenuFct;
            delete options.contextMenuFct;
        }
        UI.prototype.initialize.apply(this, [div, layersModel, this.EVENT_TYPES, options]);
        if (_.isNil(this.nodeRootTitle)) {
            this.nodeRootTitle = this.getMessage('ROOT_TITLE');
        }
        this.registerAllLayerLoading();
    },
    registerAllLayerLoading: function () {
        if (this.model.displayIconLoading) {
            this.registerToLayerLoading(this.model.item);
        }
    },
    /**
     * Méthode registerToLayerLoading
     * Enregistre l'arbre comme listener de tous les chargements de layer.
     */
    registerToLayerLoading: function (item) {
        if (item instanceof Group) {
            for (var i = 0; i < item.items.length; i++) {
                var childItem = item.items[i];
                this.registerToLayerLoading(childItem);
            }
        } else if (item instanceof Layer) {
            for (var j = 0; j < item.OL_layers.length; j++) {
                var olLayer = item.OL_layers[j];
                item.checkLayerLoading(olLayer, this, this.refreshNode.bind(this), this.refreshNodeEnd.bind(this));
            }
        }
    },
    /**
     * Méthode refreshNode
     * Rafraichit le noeud correspondant au layer en train d'être chargé.
     */
    refreshNode: function (e) {
        if (this.tree) {
            log.debug("refreshNode ON START LOAD");
            this.tree.refresh(true, true);
        }
    },
    /**
     * Méthode refreshNodeEnd
     * Rafraichit le noeud correspondant au layer en fin de chargement.
     */
    refreshNodeEnd: function (e) {
        if (this.tree) {
            log.debug("refreshNode ON END LOAD");
            this.tree.refresh(true, true);
        }
    },
    /**
     * Methode: draw
     * Construit et affiche l'arborescence de gestion du contenu de la carte.
     */
    draw: function () {
        var self = this;
        var divSelector = '#' + self.div.id;

        this.tree = jQuery.jstree.reference(divSelector);
        if (this.tree) {
            log.debug('Draw refresh tree');
            this.tree.refresh(true, true);
        } else {
            log.debug('Draw First Time tree');

            var plugins = [];

            if (this.model.fctVisibility) {
                plugins.push('checkbox');
            }

            plugins.push('dnd');

            var jstreeCMFct = function () {};
            if (this.model.editable && this.model.fctContextMenu) {
                plugins.push('contextmenu');
                jstreeCMFct = this.jstreeContextMenuItemsFct;
                if (this.jstreeContextMenuItemsCustomFct) {
                     jstreeCMFct = this.jstreeContextMenuItemsCustomFct;
                }
            }

            this.treeConfig = {
                'default': {
                    dnd: {
                        is_draggable: this.model.editable
                    }
                },
                core: {
                    check_callback: this.model.editable,
                    themes: {
                        name: this.jstreeTheme,
                        icons: false,
                        dots: true
                    },
                    data: function (obj, callback) {
                        var json = self._getStructure(self.model.item);
                        self.timestamp = new Date().getTime();
                        json.data = self.timestamp;
                        callback.call(this, json);
                    },
                    animation: 0,
                    worker: false,
                    html_titles: true
                },
                checkbox: {
                    three_state: true,
                    tie_selection: false,
                    whole_node: false
                },
                dnd: {
                    copy: false
                },
                contextmenu: {
                    items: jstreeCMFct.bind(this),
                    select_node: true,
                    show_at_node: true
                },
                plugins: plugins
            };

            var treeDiv = jQuery(divSelector);
            if (!treeDiv.hasClass(this.defaultDisplayClasses.tree)) {
                treeDiv.addClass(this.defaultDisplayClasses.tree);
            }


            treeDiv.jstree(this.treeConfig);
            this.tree = jQuery.jstree.reference(divSelector);

            treeDiv.on('loaded.jstree', function (e, data) {
                log.debug('loaded', arguments);
            });
            treeDiv.on('redraw.jstree', function (e, data) {
                //suppression des checkbox
                jQuery('li[nocheckbox="true"]').find('i.jstree-checkbox:first').hide();
                var dataTimestamp = data.instance._model.data.DescartesUILayersTree.data;
                if (self.timestamp === dataTimestamp) {
                    self._afterRender();
                }
            });
            treeDiv.on('open_node.jstree', function (e, data) {
                log.debug('open', arguments);
                self.openCloseGroup(data, true);
            });
            treeDiv.on('close_node.jstree', function (e, data) {
                log.debug('close', arguments);
                self.openCloseGroup(data, false);
            });
            treeDiv.on("check_node.jstree", function (e, data) {
                self.toggleLayerVisibilityByCheckBoxChanged(data, true);
            });
            treeDiv.on("uncheck_node.jstree", function (e, data) {
                self.toggleLayerVisibilityByCheckBoxChanged(data, false);
            });
            treeDiv.on('move_node.jstree', function (e, data) {
                log.debug('move');
                self.onDropItem(e, data);
            });
            treeDiv.on("select_node.jstree", function (e, data) {
                if (self.model.editable) {
                    self.selectItem();
                }
            });
            jQuery(document).on('dnd_start.vakata', function (event) {
                log.debug('drag');
                event.preventDefault();
            });
            this.tree = jQuery.jstree.reference('#' + self.div.id);

            //Prise en compte de l'icone de chargement pour les couches ajoutées en cours d'utilisation de l'application.
            if (this.model.displayIconLoading) {
                this.registerToLayerLoading(this.model.item);
            } else {
                //besoin de rafraichir pour ne pas afficher les checkbox de l'arbre
                this.tree.refresh(true, true);
            }

            jQuery(this.div).on('hidden.bs.collapse', function (e) {
                var pos = self.savItemsOpen.indexOf(e.target.id);
                if (pos >= 0) {
                    self.savItemsOpen.splice(pos, 1);
                }
            });
            jQuery(this.div).on('shown.bs.collapse', function (e) {
                if (self.moreInfosUiParams.fctDisplayLegend && self.model.fctDisplayLegend) {
                    _.each(jQuery('.legend-' + e.target.id), function (image) {
                        jQuery(image).attr('src', jQuery(image).attr('async-src'));
                    });
                }
                var pos = self.savItemsOpen.indexOf(e.target.id);
                if (pos === -1) {
                    self.savItemsOpen.push(e.target.id);
                }
            });
        }
    },

    /**
     * Méthode _afterRender
     * Méthode appelée une fois le rendu de l'ihm effectué
     */
    _afterRender: function () {
        log.debug('afterRender call');

        this._setToolTip(false);

        if (this.model.fctOpacity) {
            this._addToolsSlider();
        }

        //ajout handler de changement de visibilité
        var classOnSelector = this.displayClasses.layerVisibilityOn.replace(/ /g, '.'); //si plusieurs classes css
        var classOffSelector = this.displayClasses.layerVisibilityOff.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector + ',.' + classOffSelector).click(this.toggleLayerVisibilityByDescartesIconChanged.bind(this));

        //ajout handler de changement de requête d'information
        classOnSelector = this.displayClasses.layerInfoEnabled.replace(/ /g, '.'); //si plusieurs classes css
        classOffSelector = this.displayClasses.layerInfoDisabled.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector + ',.' + classOffSelector).click(this.toggleLayerQueryInfo.bind(this));

        //ajout handler d'affichage des informations d'une couche
        classOnSelector = this.displayClasses.layerSheetActive.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector).click(this.showDatasLayer.bind(this));

        //ajout du handler d'ouverture du lien metadata.
        classOnSelector = this.displayClasses.layerLienMetadata.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector).click(function (event) {
            var href = event.target.href;
            window.open(href);
        });
    },
    _getNodeStructure: function (node, nodeId) {
        var result = null;
        if (node.id === nodeId) {
            result = node;
        } else if (node.children) {
            for (var i = 0; i < node.children.length; i++) {
                var child = node.children[i];
                result = this._getNodeStructure(child, nodeId);
                if (result !== null) {
                    return result;
                }
            }
        }
        return result;
    },
    _getStructure: function (node) {
        var id = this.CLASS_NAME.replace(/\./g, '') + node.index.toString();
        var sliderId = id + 'SliderA';
        var visibleClassName = this.displayClasses.layerVisibilityOff;
        var visibleTitle = '';
        var needZoomClassName = null;

        var state = {
            opened: node.opened
        };
        if (node.hide) {
            state.hidden = true;
        }
        var nocheckbox = !this.model.fctVisibility;

        if (node.index === '') {
            nocheckbox = true;
        }
        if (node.CLASS_NAME.indexOf('Descartes.Layer') === 0) {
            state.checked = node.visible;
            nocheckbox = true;
            if (!node.acceptMaxScaleRange()) {
                needZoomClassName = this.displayClasses.layerVisibilityNeedZoomOut;
                visibleClassName = this.displayClasses.layerVisibilityOnForbidden;
                node.loading = false;
            } else if (!node.acceptMinScaleRange()) {
                needZoomClassName = this.displayClasses.layerVisibilityNeedZoomIn;
                visibleClassName = this.displayClasses.layerVisibilityOnForbidden;
                node.loading = false;
            } else if (node.visible) {
                visibleClassName = this.displayClasses.layerVisibilityOn;
            }

            //récupération du tooltip à afficher
            if (node.isInScalesRange()) {
                visibleTitle = (node.visible ? this.getMessage('HIDE_LAYER') : this.getMessage('SHOW_LAYER'));
            } else if (node.minScale !== null && node.maxScale !== null) {
                visibleTitle = this.getMessage('MIN_SCALE') +
                        Utils.readableScale(node.minScale) +
                        this.getMessage('MAX_SCALE') +
                        Utils.readableScale(node.maxScale);
            } else if (node.minScale != null) {
                visibleTitle = this.getMessage('MIN_SCALE_ONLY') + Utils.readableScale(node.minScale);
            } else {
                visibleTitle = this.getMessage('MIN_SCALE_ONLY') + Utils.readableScale(node.maxScale);
            }
        }

        var queryableInfo = this.getQueryableInfo(node);
        var metadataInfo = this.getMetadataItem(node);

        var itemTextClassName = this.displayClasses.itemText;

        var text = treeItemTemplate({
            id: sliderId,
            node: node,
            displayIconLoading: this.model.displayIconLoading,
            rootTitle: this.nodeRootTitle,
            rootClassName: this.displayClasses.treeRoot,
            loadErrorClassName: this.displayClasses.loadErrorClassName,
            visibleClassName: visibleClassName,
            visibleTitle: visibleTitle,
            needZoomClassName: needZoomClassName,
            itemTextClassName: itemTextClassName,
            fctVisibility: this.model.fctVisibility,
            fctOpacity: this.model.fctOpacity,
            fctQueryable: this.model.fctQueryable,
            fctDisplayLegend: this.model.fctDisplayLegend,
            displayMoreInfos: this.model.displayMoreInfos,
            displayMoreInfosParams: this.moreInfosUiParams,
            displayMoreInfosTitle: this.getMessage("MORE_INFOS"),
            displayMoreInfosClassName: this.displayClasses.layerMoreInfos,
            displayMoreInfoLegendTitle: this.getMessage("MORE_INFO_LEGEND"),
            displayMoreInfoLegendClassName: this.displayClasses.layerMoreInfoLegend,
            queryableClassName: queryableInfo.className,
            queryableTitle: queryableInfo.title,
            sheetableClassName: queryableInfo.sheetableClassName,
            sheetableTitle: queryableInfo.sheetableTitle,
            fctMetadataLink: this.model.fctMetadataLink,
            metadataHref: metadataInfo.href,
            metadataClassName: metadataInfo.className,
            metadataTitle: metadataInfo.title,
            noMetadataClassName: this.displayClasses.layerNoLienMetadata,
            noMetadataTitle: this.getMessage('NO_LIEN_METADATA')
        });

        var json = {
            id: id,
            text: text,
            icon: '',
            state: state,

            li_attr: {
                'nocheckbox': nocheckbox
            }
        };
        if (node.items) {
            json.children = [];
            for (var i = 0; i < node.items.length; i++) {
                var item = node.items[i];
                var child = this._getStructure(item);
                json.children.push(child);
            }
        }
        return json;
    },
    getQueryableInfo: function (layer) {
        if (this.model.fctQueryable) {
            var className = '';
            var title = '';
            if (layer.queryable) {
                if (layer.visible && layer.isInScalesRange()) {
                    className = (layer.activeToQuery ? this.displayClasses.layerInfoEnabled : this.displayClasses.layerInfoDisabled);
                    title = (layer.activeToQuery ? this.getMessage('DISABLE_QUERY') : this.getMessage('ENABLE_QUERY'));
                } else {
                    className = this.displayClasses.layerInfoForbidden;
                    title = this.getMessage('FORBIDDEN_QUERY');
                }
            } else {
                className = this.displayClasses.blankBlocQuote;
                title = this.getMessage('NO_QUERY');
            }

            var sheetableClassName = '';
            var sheetableTitle = '';
            if (layer.sheetable) {
                if (layer.visible && layer.isInScalesRange()) {
                    sheetableClassName = this.displayClasses.layerSheetActive;
                    sheetableTitle = this.getMessage('ACTIVE_SHEET');
                } else {
                    sheetableClassName = this.displayClasses.layerSheetInactive;
                    sheetableTitle = this.getMessage('INACTIVE_SHEET');
                }
            } else {
                sheetableClassName = this.displayClasses.blankBlocQuote;
                sheetableTitle = this.getMessage('NO_QUERY');
            }

            return {
                className: className,
                title: title,
                sheetableClassName: sheetableClassName,
                sheetableTitle: sheetableTitle
            };
        }
        return {};
    },

    /**
     * Méthode getMetadataItem
     * Retourne le contenu html représentant l'outil d'information des metadata de la couche associé.
     */
    getMetadataItem: function (layer) {
        if (layer.metadataURL) {
            var href = layer.metadataURL.replace(/&amp;/g, '&');
            return {
                href: href,
                className: this.displayClasses.layerLienMetadata,
                title: this.getMessage('LIEN_METADATA')
            };
        }
        return {};
    },
    /**
     * Private
     *
     * Affichage de l'info bulle pour l'affichage/masquage des groupes.
     */
    _setToolTip: function (invert) {
        var closeGroupLabel = this.getMessage('CLOSE_GROUP');
        var openGroupLabel = this.getMessage('OPEN_GROUP');
        jQuery('li[class*="jstree-open"]').find('i.jstree-icon:first').attr('title', closeGroupLabel);
        jQuery('li[class*="jstree-closed"]').find('i.jstree-icon:first').attr('title', openGroupLabel);

        if (!invert) {
            var labels = jQuery('#' + this.div.id).find('a');
            for (var i = 0, len = labels.length; i < len; i++) {
                var labelChildren = labels[i].childNodes;
                for (var j = 0, lenJ = labelChildren.length; j < lenJ; j++) {
                    if (labelChildren[j].nodeType === 3 && labelChildren[j].nodeValue.trim() !== "") {
                        labels[i].title = labelChildren[j].nodeValue.trim();
                        break;
                    }
                }
            }
        }
    },
    /**
     * Private
     *
     * Ajout de l'outil permettant de modifier l'opacité d'une couche (slider).
     */
    _addToolsSlider: function () {
        var spanSliders = jQuery('span[id$="SliderA"]');

        log.debug('addToolSlider', spanSliders);

        _.each(this.sliders, function (item) {
            item.slider.destroy();
        });
        this.sliders = [];

        var self = this;
        for (var i = 0; i < spanSliders.length; i++) {
            var spanSlider = spanSliders[i];

            var descartesNodeIndex = spanSlider.id.substring(self.CLASS_NAME.replace(/\./g, "").length, spanSlider.id.indexOf('SliderA'));
            log.debug('descartesNodeIndex=', descartesNodeIndex);
            var itemDescartes = self.model.getItemByIndex(descartesNodeIndex);
            log.debug('itemDescartes=', itemDescartes);

            var slider = new Slider(spanSlider, {
                id: descartesNodeIndex,
                max: itemDescartes.opacityMax,
                min: 0,
                step: 1,
                value: itemDescartes.opacity,
                tooltip: 'hide'
            });
            self.sliders.push({
                index: descartesNodeIndex,
                slider: slider
            });
        }

        //impossible d'initialiser les listeners dans la boucle for.
        _.each(this.sliders, function (item) {
            item.slider.on('slide', function (value) {
                //remove dnd tooltip
                jQuery('#vakata-dnd').hide();
                self.changeOpacity(item.index, value);
            });
            item.slider.on('change', function (e) {
                self.changeOpacity(item.index, e.newValue);
            });
        });

    },
    /**
     * Methode: toggleLayerVisibilityByCheckBoxChanged
     * Inverse la visibilité des groupes et des couches lors du changement d'état d'une checkbox.
     *
     * Paramêtres:
     * data - {Object} contenant les informations de l'événements
     * checked - Egale à true si l'événement correspond à une case cochée. Décochée sinon.
     */
    toggleLayerVisibilityByCheckBoxChanged: function (data, checked) {
        if (!this.changingVisibilityState) {
            this.changingVisibilityState = true;

            var id = data.node.id.substring(this.CLASS_NAME.replace(/\./g, "").length);
            var groupOrLayer = this.model.getItemByIndex(id);
            log.debug(groupOrLayer);
            groupOrLayer.setVisibility(checked);

            if (groupOrLayer.CLASS_NAME === 'Descartes.Group') {
                _.each(groupOrLayer.items, function (descartesLayer) {

                    descartesLayer.setVisibility(checked);
                    this.model.refreshVisibilities(descartesLayer, true);

                }.bind(this));
            }

            this.tree.refresh(true, true);
            this.changingVisibilityState = false;
            this.events.triggerEvent('treeUserEvent');
        }
    },
    /**
     * Methode: toggleLayerVisibilityByDescartesIconChanged
     * Inverse la visibilité des groupes et des couches lors du changement de la visibilite d'une couche.
     *
     * Paramètres:
     * current - {HTMLElement} L'élément DOM concerné.
     */
    toggleLayerVisibilityByDescartesIconChanged: function (event) {
        var visibilityId = jQuery(event.target).closest('a').attr('id');
        var nodeDescartesIndex = visibilityId.substring(this.CLASS_NAME.replace(/\./g, "").length, visibilityId.indexOf('_'));
        var descartesLayer = this.model.getItemByIndex(nodeDescartesIndex);

        if (descartesLayer.isInScalesRange && descartesLayer.isInScalesRange()) {
            this.model.refreshVisibilities(descartesLayer);
            this.tree.refresh(true, true);
            this.events.triggerEvent('treeUserEvent');
        }
    },
    /**
     * Private
     *
     * Déplacement d'un noeud.
     */
    onDropItem: function (e, data) {
        var dropNodeIndex = data.node.id.replace(this.CLASS_NAME.replace(/\./g, ''), '');
        var groupOrLayer = this.model.getItemByIndex(dropNodeIndex);
        log.debug('groupOrLayer', groupOrLayer);

        var parentIndex = data.parent.replace(this.CLASS_NAME.replace(/\./g, ''), '');
        var target = null;
        if (data.parent === '#') {
            target = this.model.item;
        } else {
            target = this.model.getItemByIndex(parentIndex);
        }
        if (_.isNil(target)) {
            target = this.model.item;
        }

        log.debug('target', target);

        //suppression
        this.model.removeItemByIndex(dropNodeIndex, true);

        if (target.CLASS_NAME.indexOf('Descartes.Layer') === 0) {
            this.model.insertItemBefore(groupOrLayer, target);
        } else if (target.CLASS_NAME.indexOf('Descartes.Group') === 0) {
            if (target.items.length === 0) {
                this.model.addItem(groupOrLayer, target);
            } else if (target.items.length === data.position) {
                this.model.insertItemAfter(groupOrLayer, target.items[data.position - 1]);
            } else {
                this.model.insertItemBefore(groupOrLayer, target.items[data.position]);
            }
        }
        this.tree.refresh(true, true);
    },
    /**
     * Private
     *
     * Récupération du groupe cible lors du déplacement d'un noeud.
     */
    _getGroupItemTarget: function (items, titleTarget, oldGroupItem) {
        for (var i = 0; i < items.length; i++) {
            if (items[i].title === titleTarget &&
                    items[i].CLASS_NAME === 'Descartes.Group' &&
                    items[i].items.length === oldGroupItem.items.length) {
                var groupTargetOk = true;
                for (var j = 0, len = items[i].items.length; j < len; j++) {
                    if (items[i].items[j].title !== oldGroupItem.items[j].title) {
                        groupTargetOk = false;
                        break;
                    }
                }
                if (groupTargetOk) {
                    return items[i];
                }
            } else if (items[i].CLASS_NAME === 'Descartes.Group') {
                var newGroup = this._getGroupItemTarget(items[i].items, titleTarget, oldGroupItem);
                if (newGroup !== undefined) {
                    return newGroup;
                }
            }
        }
        return null;
    },
    /**
     * Methode: openCloseGroup
     * Inverse la présentation du groupe entre les affichages "plié" et "déplié".
     *
     * Paramètres:
     * data - {String} donnée de l'événement.
     * opened  - {Boolean} true si le noeud est ouvert, false sinon
     */
    openCloseGroup: function (data, opened) {
        var id = data.node.id.substring(this.CLASS_NAME.replace(/\./g, '').length);
        var item = this.model.getItemByIndex(id);
        item.opened = opened;
        this.tree.refresh(true, true);
    },
    /**
     * Methode: toggleLayerQueryInfo
     * Inverse la capacité à être interrogée de la couche.
     *
     * Paramêtres:
     * event - evenement associé au clic
     */
    toggleLayerQueryInfo: function (event) {
        var queryableId;
        if (this.moreInfosUiParams && this.moreInfosUiParams.fctQueryable) {
            queryableId = event.target.parentNode.parentNode.id;
        } else {
            queryableId = event.target.parentNode.id;
        }

        var nodeDescartesIndex = queryableId.substring(this.CLASS_NAME.replace(/\./g, "").length, queryableId.indexOf('_'));

        var descartesLayer = this.model.getItemByIndex(nodeDescartesIndex);

        if (descartesLayer && descartesLayer.queryable && descartesLayer.visible && descartesLayer.isInScalesRange()) {
            descartesLayer.activeToQuery = !descartesLayer.activeToQuery;
            this.tree.refresh(true, true);
            this.events.triggerEvent('treeUserEvent');
            var self = this;
            setTimeout(function () {
                console.log("timeout");
                self.reOpenItems();
            }, 500);
        }
    },
    /**
     * Methode: changeOpacity
     * Modifie l'opacité de la couche.
     *
     * Paramêtres:
     * nodeIndex - {String} Index hiérarchique de la couche dans l'arborescence.
     * value - {Integer} Valeur de l'opacité.
     */
    changeOpacity: function (nodeIndex, value) {
        var item = this.model.getItemByIndex(nodeIndex);
        this.model.changeOpacity(item, value);
        this.events.triggerEvent('treeUserEvent');
    },
    /**
     * Methode: showDatasLayer
     * Affiche les propriétes de l'ensemble des objets d'une couche.
     *
     * :
     * Lance l'appel au service d'interrogation grâce à la classe <Descartes.Action.AjaxSelection>.
     *
     * Paramètres:
     * event - evenement associé au clic.
     */
    showDatasLayer: function (event) {
        var queryableId;
        if (this.moreInfosUiParams && this.moreInfosUiParams.fctQueryable) {
            queryableId = event.target.parentNode.parentNode.id;
        } else {
            queryableId = event.target.parentNode.id;
        }
        var nodeDescartesIndex = queryableId.substring(this.CLASS_NAME.replace(/\./g, '').length, queryableId.indexOf('_'));
        var descartesLayer = this.model.getItemByIndex(nodeDescartesIndex);

        if (descartesLayer.sheetable && descartesLayer.visible && descartesLayer.isInScalesRange()) {
            if (descartesLayer.isResultLayer) {
               this.showDatasResultLayer(descartesLayer);
            } else {
               var view = descartesLayer.OL_layers[0].map.getView();
               var projection = view.getProjection();

               var bbox = view.calculateExtent();
               /*var coords = "" + bbox[0] + ',' + bbox[1] + ' ' + bbox[2] + ',' + bbox[3];
               if (descartesLayer.resourceLayers[0].featureReverseAxisOrientation) {
                   coords = "" + bbox[1] + ',' + bbox[0] + ' ' + bbox[3] + ',' + bbox[2];
               }*/
               //var maxBBOX = '<Box srsName=\"' + projection.getCode() + '\"><coordinates>' + coords + '</coordinates></Box>';
               var maxBBOX = '<Envelope srsName="' + projection.getCode() + '" xmlns="http://www.opengis.net/gml">';
               maxBBOX += '<lowerCorner>' + bbox[0] + ' ' + bbox[1] + '</lowerCorner>';
               maxBBOX += '<upperCorner>' + bbox[2] + ' ' + bbox[3] + '</upperCorner>';
               maxBBOX += '</Envelope>';

               var dataMask = '<BBOX><PropertyName>' + descartesLayer.resourceLayers[0].featureGeometryName + '</PropertyName>' + maxBBOX + '</BBOX>';

               var fluxInfos = ExternalCallsUtils.writeQueryPostBody(ExternalCallsUtilsConstants.WFS_SERVICE, [descartesLayer], descartesLayer.OL_layers[0].map);
               var infosJson = ExternalCallsUtils.writeGetFeatureExportRequestJson(ExternalCallsUtilsConstants.WFS_SERVICE, [descartesLayer], descartesLayer.OL_layers[0].map);

               if (fluxInfos !== null) {
                   var requestParams = {
                       distantService: Descartes.FEATURE_SERVER,
                       infos: fluxInfos,
                       dataMask: dataMask,
                       infosJson: infosJson,
                       withReturn: this.withReturn,
                       withCsvExport: this.withCsvExport,
                       withResultLayerExport: this.withResultLayerExport,
                       withUIExports: this.withUIExports,
                       withAvancedView: this.withAvancedView,
                       withFilterColumns: this.withFilterColumns,
                       withListResultLayer: this.withListResultLayer
                   };
                   var attributesAliasSubstitution = {};
                   var attributesAlias = descartesLayer.getAttributesAlias();
                   if (!_.isNil(attributesAlias)) {
                       attributesAliasSubstitution[descartesLayer.id] = attributesAlias;
                   }
                   var action = new AjaxSelection(this.resultUiParams, requestParams, descartesLayer.OL_layers[0].map, {waitingMsg: this.waitingMsg, btnDisplaySelectionLayer: false, attributesAliasSubstitution: attributesAliasSubstitution});
                   if (action.renderer) {
                       action.renderer.requestParams = requestParams;
                   }
                }
            }
        }
    },
    /**
     * Methode: selectItem
     * Sélectionne un groupe ou une couche pour servir de base aux fonctions de modification de l'arborescence.
     */
    selectItem: function () {
        var nodeFocused = this.tree.get_node(this.tree.get_selected()[0]);

        var descartesNodeIndex = nodeFocused.id.replace(this.CLASS_NAME.replace(/\./g, ""), '');

        if (this.selectedItem !== descartesNodeIndex) {
            this.selectedItem = descartesNodeIndex;
            if (this.selectedItem !== '') {
                if (this.model.getItemByIndex(this.selectedItem).CLASS_NAME === 'Descartes.Group') {
                    this.events.triggerEvent('groupSelected');
                } else {
                    this.events.triggerEvent('layerSelected');
                }
            } else {
                this.events.triggerEvent('rootSelected');
            }

            this.tree.select_node(nodeFocused);
        } else {
            this.selectedItem = '';
            this.tree.deselect_node(nodeFocused);
            this.events.triggerEvent('nodeUnselect');
        }
    },

    jqSelector: function (str) {
        return str.replace(/([;&,\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g, '\\$1');
    },
    /**
     * Methode: setJstreeContextMenuItems
     * Affecte les items du menu contextuel.
     */
    setJstreeContextMenuItems: function (contextMenuItems) {
        this.jstreeContextMenuItems = contextMenuItems;
    },
    /**
     * Methode: jstreeContextMenuItemsFct
     * Retourne les items du menu contextuel.
     */
    jstreeContextMenuItemsFct: function (itemTree) {
        var self = this;
        var position = itemTree.id.substring(self.CLASS_NAME.replace(/\./g, '').length);
        var descartesItem = null;
        if (position !== '') {
            descartesItem = self.model.getItemByIndex(position);
        }
        var contextMenuItems = {};
        if (self.jstreeContextMenuItems) {
            if (descartesItem === null) {
                contextMenuItems = _.clone(self.jstreeContextMenuItems.itemsRoot);
            } else if (descartesItem.CLASS_NAME === "Descartes.Group") {
                contextMenuItems.addGroupItem = _.clone(self.jstreeContextMenuItems.itemsGroup.addGroupItem);
                contextMenuItems.updateGroupItem = _.clone(self.jstreeContextMenuItems.itemsGroup.updateGroupItem);
                contextMenuItems.deleteGroupItem = _.clone(self.jstreeContextMenuItems.itemsGroup.deleteGroupItem);
                contextMenuItems.addLayerItem = _.clone(self.jstreeContextMenuItems.itemsGroup.addLayerItem);
                contextMenuItems.addLayerWMSItem = _.clone(self.jstreeContextMenuItems.itemsGroup.addLayerWMSItem);
                if (!self.model.editInitialItems && !descartesItem.addedByUser) {
                    if (contextMenuItems.deleteGroupItem) {
                        contextMenuItems.deleteGroupItem._disabled = true;
                    }
                    if (contextMenuItems.updateGroupItem) {
                        contextMenuItems.updateGroupItem._disabled = true;
                    }
                }
            } else {
                contextMenuItems.updateLayerItem = _.clone(self.jstreeContextMenuItems.itemsLayer.updateLayerItem);
                contextMenuItems.deleteLayerItem = _.clone(self.jstreeContextMenuItems.itemsLayer.deleteLayerItem);
                contextMenuItems.exportVectorLayerItem = _.clone(self.jstreeContextMenuItems.itemsLayer.exportVectorLayerItem);
                if ((!self.model.editInitialItems && !descartesItem.addedByUser)) {
                    if (contextMenuItems.updateLayerItem) {
                        contextMenuItems.updateLayerItem._disabled = true;
                    }
                    if (contextMenuItems.deleteLayerItem) {
                        contextMenuItems.deleteLayerItem._disabled = true;
                    }
                    if (contextMenuItems.exportVectorLayerItem) {
                        contextMenuItems.exportVectorLayerItem._disabled = true;
                    }
                }
                if (!(descartesItem instanceof Descartes.Layer.Vector)) {
                    if (contextMenuItems.exportVectorLayerItem) {
                        contextMenuItems.exportVectorLayerItem._disabled = true;
                    }
                }
            }
        }
        return contextMenuItems;
    },

    reOpenItems: function () {
        for (var i = 0; i < this.savItemsOpen.length; i++) {
            jQuery("#" + this.savItemsOpen[i]).collapse('show');
        }
    },
    gotoFeatureBounds: function (boundsAndScales) {
        var extent = Utils.gotoFeatureBounds(boundsAndScales, this._olMap);
        ResultLayer.displayMarker(this._olMap, extent);
    },
    showDatasResultLayer: function (resultLayer) {
       var affichageResultats = new TabbedDataGrids(null, null, this.resultUiParams);
       this._olMap = resultLayer.OL_layers[0].map;
       affichageResultats.events.register('gotoFeatureBounds', this, this.gotoFeatureBounds);
       affichageResultats.requestParams = {infosJson: resultLayer.resultLayerRequestParamsInfosJson};
       affichageResultats.draw(resultLayer.resultLayerDatas);
    },
    CLASS_NAME: 'Descartes.UI.LayersTree'
});

module.exports = Class;
