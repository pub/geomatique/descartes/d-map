/* global Descartes */
var $ = require('jquery');
var _ = require('lodash');
var log = require('loglevel');

var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');
var resultFormDialogTemplate = require('./templates/ResultFormDialog.ejs');

require('../css/Dialog.css');

/**
 * Class: Descartes.UI.ResultFormDialog
 * Classe permettant d'afficher une boite de dialogue.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 */
var Class = Utils.Class(UI, {
    id: null,
    title: null,
    size: null,
    sendLabel: 'OK',
    btnCss: null,
    btnCssSubmit: null,
    formClass: '',
    dialogClass: 'DescartesDialog',
    content: null,
    type: null,
    dialog: null,
    result: null,
	useDropMenuActions: true,

    /**
     * Constructeur: Descartes.UI.ResultFormDialog
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * title - {String} Titre de la boite de dialogue.
     * contenu - {String} Contenu à afficher dans la boite de dialogue.
     */
    initialize: function (options) {
        this.btnCss = Descartes.UIBootstrap4Options.btnCss;
        this.btnCssSubmit = Descartes.UIBootstrap4Options.btnCssSubmit;
        _.extend(this, options);
        UI.prototype.initialize.apply(this, arguments);
    },
    createTemplate: function (callback, otherCallbackUndisplay, otherCallbackUnselect, otherCallbackFix, otherCallbackFlash, otherCallbackAdd) {
        var template = resultFormDialogTemplate({
            id: this.id,
            title: this.title,
            formClass: this.formClass,
            dialogClass: this.dialogClass,
            sendLabel: this.sendLabel,
            useDropMenuActions: this.useDropMenuActions,
            dropMenuLabel: this.getMessage("DIALOG_DROPMENU_LABEL"),
            dropMenuBtnUnselectLabel: this.getMessage("DIALOG_DROPMENU_BTN_UNSELECT_LABEL"),
            dropMenuBtnFixLabel: this.getMessage("DIALOG_DROPMENU_BTN_FIX_LABEL"),
            dropMenuBtnFlashLabel: this.getMessage("DIALOG_DROPMENU_BTN_FLASH_LABEL"),
            dropMenuBtnUndisplayLabel: this.getMessage("DIALOG_DROPMENU_BTN_UNDISPLAY_LABEL"),
            dropMenuBtnAddLabel: this.getMessage("DIALOG_DROPMENU_BTN_ADD_LABEL"),
            dropMenuLabelTooltip: this.getMessage("DIALOG_DROPMENU_LABEL_TOOLTIP"),
            dropMenuBtnUnselectLabelTooltip: this.getMessage("DIALOG_DROPMENU_BTN_UNSELECT_LABEL_TOOLTIP"),
            dropMenuBtnFixLabelTooltip: this.getMessage("DIALOG_DROPMENU_BTN_FIX_LABEL_TOOLTIP"),
            dropMenuBtnFlashLabelTooltip: this.getMessage("DIALOG_DROPMENU_BTN_FLASH_LABEL_TOOLTIP"),
            dropMenuBtnUndisplayLabelTooltip: this.getMessage("DIALOG_DROPMENU_BTN_UNDISPLAY_LABEL_TOOLTIP"),
            dropMenuBtnAddLabelTooltip: this.getMessage("DIALOG_DROPMENU_BTN_ADD_LABEL_TOOLTIP"),
            btnCss: this.btnCss,
            btnCssSubmit: this.btnCssSubmit,
            content: this.content
        });
        $('body').append(template);
        this.dialog = $('#' + this.id);

        this.handleEvents(callback, otherCallbackUndisplay, otherCallbackUnselect, otherCallbackFix, otherCallbackFlash, otherCallbackAdd);

    },
    handleEvents: function (callback, otherCallbackUndisplay, otherCallbackUnselect, otherCallbackFix, otherCallbackFlash, otherCallbackAdd) {
        var that = this;
        $('#' + this.id + '_formDialog').on('submit', function (e) {
            e.preventDefault();
            that.result = Utils.serializeFormArrayToJson($(this).serializeArray());

            log.debug('Closing dialog with result %s', that.result);
            if (!_.isNil(callback) && !_.isNil(that.result)) {
                if (callback(that.result) === false) {
                    e.preventDefault();
                }
            }
            that.result = null;
            that.close();
        });
        if (this.useDropMenuActions) {
            $('#' + this.id + '_unselectRequestAction').on('click', function (e) {
                that.otherAction(otherCallbackUnselect);
                $('#' + that.id + '_dropdownMenu').prop('disabled', true);
            });
            $('#' + this.id + '_fixSelectionLayerAction').on('click', function (e) {
                that.otherAction(otherCallbackFix);
            });
            $('#' + this.id + '_flashSelectionLayerAction').on('click', function (e) {
                that.otherAction(otherCallbackFlash);
            });
            $('#' + this.id + '_undisplaySelectionLayerAction').on('click', function (e) {
                that.otherAction(otherCallbackUndisplay);
            });
            $('#' + this.id + '_addSelectionLayerAction').on('click', function (e) {
                that.otherAction(otherCallbackAdd);
                $('#' + that.id + '_dropdownMenu').prop('disabled', true);
            });
        }
        $('#' + this.id + '_close').on('click', function (e) {
            that.close(callback);
        });
    },
    open: function (callback, otherCallbackUndisplay, otherCallbackUnselect, otherCallbackFix, otherCallbackFlash, otherCallbackAdd) {
        this.createTemplate(callback, otherCallbackUndisplay, otherCallbackUnselect, otherCallbackFix, otherCallbackFlash, otherCallbackAdd);
        this.dialog.find('.modal-content').draggable();
    },
    close: function (callback) {
        this.dialog.remove();
        if (callback) {
            callback();
        }
    },
    otherAction: function (otherCallback) {
        if (otherCallback) {
            otherCallback();
        }
    },
    CLASS_NAME: 'Descartes.UI.ResultFormDialog'
});

module.exports = Class;
