var $ = require('jquery');
var _ = require('lodash');
var log = require('loglevel');

var Utils = require('../../Utils/DescartesUtils');
var GazetteerInPlace = require('./GazetteerInPlace');
var AbstractInPlace = require('./AbstractInPlace');
var GazetteerLevel = require('../../Model/GazetteerLevel');
var DMapConstants = require('../../DMapConstants');

var gazetteerInPlaceTemplate = require('./templates/GazetteerInPlace.ejs');


require('../css/SimpleUIs.css');

/**
 * Class: Descartes.UI.GazetteerInPlace
 * Classe proposant, dans une zone fixe de la page HTML, le choix d'un objet de référence pour recadrer la carte.
 *
 * :
 * Le choix de l'objet est effectué par sélection de celui-ci dans un ensemble de listes déroulantes imbriquées, puis par activation d'un bouton HTML.
 *
 * Hérite de:
 *  - <Descartes.UI>
 *
 *
 * Evénements déclenchés:
 * select - Un objet de référence a été choisi.
 */
var Class = Utils.Class(GazetteerInPlace, {

    selectpicker: false,
    EVENT_TYPES: ['select'],

    /**
     * Constructeur: Descartes.UI.RequestManagerGazetteer
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le formulaire de saisie.
     * gazetteerModel - {Object} Modèle de la vue passé par référence par le contrôleur <Descartes.Action.Gazetteer>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * levels - {Array(<Descartes.GazetteerLevel>)} Liste des niveaux de localisation.
     * proj - {String} Code de la projection de la carte.
     * initValue - {String} Code de la valeur de l'objet "père" des objets du plus haut niveau de localisation (i.e. d'index 0).
     * service - {<Descartes.GazetteerService>} Service côté serveur pour la fourniture des objets de référence.
     * location - {String} Répertoire de localisation des fichiers statiques contenant les objets de référence ("descartes/gazetteer/" par défaut).
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de localisation.
     */
    initialize: function (div, gazetteerModel, options) {
        var that = this;

        this.levels = [];

        if (!_.isNil(options)) {
            this.service = options.service || null;
            delete options.service;
            if (!_.isNil(options.levels)) {
                _.each(options.levels, function (level, index) {
                    if (!(level instanceof GazetteerLevel)) {
                        level = new GazetteerLevel(level.message, level.error, level.name, level.options);
                    }
                    level.index = index;
                    that.levels.push(level);
                });
                delete options.levels;
            }
        }
        AbstractInPlace.prototype.initialize.apply(this, [div, gazetteerModel, this.EVENT_TYPES, options]);
    },

    draw: function () {
        var that = this;
        $('#DescartesRequestManagerGazetteer').hide();
        $('#DescartesRequestManagerChooseBtn').hide();
        $('#DescartesRequestManagerChooseBtn').on('click', function () {
          $('#DescartesRequestManagerGazetteer').show();
          if (!that.levels[0].datasList) {
              that.showLevel(0, that.initValue);
          }
          $('#DescartesRequestManagerChooseBtn').hide();
        });
        $("#DescartesRequestManagerChooseExtent input[name=DescartesRequestFlexRadio]").change(function () {
          if (this.value === "3") {
              $('#DescartesRequestManagerChooseBtn').show();
              if (_.isNil(that.model.activeZone)) {
                  _.each($('.DescartesRequestTabButtonSummit'), function (btn, index) {
                      btn.disabled = true;
                  });
              }
          } else {
              $('#DescartesRequestManagerChooseBtn').hide();
              $('#DescartesRequestManagerGazetteer').hide();
              _.each($('.DescartesRequestTabButtonSummit'), function (btn, index) {
                      btn.disabled = false;
              });
          }
       });
    },
    /**
     * Methode: refresh
     * Construit la zone de la page HTML pour le choix d'un objet de référence.
     */
    refresh: function () {
        var that = this;

        var content = gazetteerInPlaceTemplate({
            id: this.id,
            levels: this.levels,
            selections: this.selections,
            btnCssSubmit: this.btnCssSubmit,
            buttonMessage: "Sélectionner"
        });

        this.renderPanel(content);

        $('#' + this.id).submit(function (event) {
            event.preventDefault();
            $('#DescartesRequestManagerGazetteer').hide();
            if (!_.isNil(that.model.activeZone)) {
               $('#DescartesRequestManagerEntityeAdmin')[0].textContent = that.getMessage('LABEL_EXTENT_ENTITY');
               $('#DescartesRequestManagerEntityeAdminChoosed')[0].textContent = that.model.activeZone.nom;
               $('#DescartesRequestManagerChooseBtn')[0].textContent = "Changer";
               _.each($('.DescartesRequestTabButtonSummit'), function (btn, index) {
                  btn.disabled = false;
               });
            } else {
               $('#DescartesRequestManagerEntityeAdmin')[0].textContent = that.getMessage('LABEL_EXTENT_ENTITY_INIT');
               $('#DescartesRequestManagerEntityeAdminChoosed')[0].textContent = "";
               $('#DescartesRequestManagerChooseBtn')[0].textContent = "Choisir";
               _.each($('.DescartesRequestTabButtonSummit'), function (btn, index) {
                  btn.disabled = true;
               });
            }

            $('#DescartesRequestManagerChooseBtn').show();
        });

        for (var i = 0; i < this.levels.length; i++) {
            $('#' + this.id + '_' + i).change(function () {
                var value = $(this).val();
                var index = _.last($(this)[0].id.split('_'));
                index = Number(index);
                that.selections.splice(index, 0, value);
                var level = that.levels[index];
                var nextLevel = index + 1;
                if (nextLevel < that.levels.length) {
                    that.showLevel(nextLevel, value);
                }
                that.setActiveZone(level, value);
            });
            if (this.selectpicker && $('#' + this.id + '_' + i).selectpicker) {
                $('#' + this.id + '_' + i).selectpicker(this.levels[i].displayOptions);
            }
        }
    },

    setActiveZone: function (level, value) {
        delete this.model.activeZone;
        for (var k = 0; k < level.datasList.length; k++) {
            var data = level.datasList[k];
            if (data.code === value) {
                this.model.activeZone = data;
                break;
            }
        }
    },

    CLASS_NAME: 'Descartes.UI.RequestManagerGazetteer'
});

module.exports = Class;
