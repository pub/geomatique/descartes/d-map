var $ = require('jquery');
var _ = require('lodash');

var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');
var dataGrid = require('./templates/DataGrid.ejs');

require('../css/SimpleUIs.css');

/**
 * Class: Descartes.UI.DataGrid
 * Classe permettant d'afficher, sous forme de tableau, les propriétés d'un ensemble d'objets géographiques.
 *
 * Hérite de:
 * - <Descartes.UI>
 *
 * Evènements déclenchés:
 * gotoFeatureBounds - La localisation rapide sur un objet est demandée.
 */
var Class = Utils.Class(UI, {
    /**
     * Private
     */
    colNames: [],

    defaultDisplayClasses: {
        globalClassName: 'DescartesDataGrid',
        headersTableClassName: 'DescartesDataGridHeadersTable',
        datasTableClassName: 'DescartesDataGridDatasTable',
        headersRowClassName: 'DescartesDataGridHeadersRow',
        datasRowsClassName: 'DescartesDataGridDatasRows',
        colHeaderClassName: 'DescartesDataGridHeader',
        ascendingOrderClassName: 'DescartesDataGridAscendingOrder',
        descendingOrderClassName: 'DescartesDataGridDescendingOrder',
        evenCellClassName: 'DescartesDataGridEvenCell',
        oddCellClassName: 'DescartesDataGridOddCell',
        localizeHeaderClassName: 'DescartesDataGridLocalizeHeader',
        evenLocalizeCellClassName: 'DescartesDataGridEvenLocalizeCell',
        oddLocalizeCellClassName: 'DescartesDataGridOddLocalizeCell'
    },

    /**
     * Propriete: acceptLocalization
     * {Boolean} Indicateur pour la présence de bouton de localisation rapide sur chaque objet.
     */
    acceptLocalization: false,

    filterColumns: false,
    /**
     * Private
     */
    EVENT_TYPES: ['gotoFeatureBounds'],

    /**
     * Constructeur: Descartes.UI.DataGrid
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le tableau.
     * datasModel - {Object} Objet JSON contenant les propriétés des objets géographiques, de la forme {prop1:"val1", prop2:"val2", ...}[].
     *
     * Options de construction propres à la classe:
     * acceptLocalization - {Boolean} Indicateur pour la présence de bouton de localisation rapide sur chaque objet.
     */
    initialize: function (div, datasModel, filterColumns) {
        this.colNames = this.getColNames(datasModel);
        this.acceptLocalization = this.checkBoundsColName(datasModel);
        if (filterColumns) {
             this.filterColumns = filterColumns;
        }
        UI.prototype.initialize.apply(this, [div, datasModel, this.EVENT_TYPES]);
        this.draw();
    },

    /**
     * Methode: draw
     * Construit et affiche le tableau (ou le met à jour en cas de tri sur une colonne).
     */
    draw: function () {

        var content = dataGrid({
            filterColumns: this.filterColumns,
            acceptLocalization: this.acceptLocalization,
            localizeHeaderClassName: this.displayClasses.localizeHeaderClassName,
            evenLocalizeCellClassName: this.displayClasses.evenLocalizeCellClassName,
            headersRowClassName: this.displayClasses.headersRowClassName,
            colNames: this.colNames,
            model: this.model
        });

        $(this.div).html(content);
        if (!_.isNil($().bootstrapTable)) {
            $(this.div).find('table').bootstrapTable({locale: 'fr-FR'});

            if (this.acceptLocalization) {
                var that = this;
                $('.' + this.displayClasses.evenLocalizeCellClassName).click(function (e) {
                    var bounds = $(this).data('bounds');
                    bounds = bounds.split(',');
                    for (var i = 0; i < bounds.length; i++) {
                        bounds[i] = Number(bounds[i]);
                    }
                    that.events.triggerEvent('gotoFeatureBounds', bounds);
                });
            }
        }

        if (this.filterColumns) {
          $(this.div).find('table tfoot th').each(function () {
             if ($(this).text() !== "") {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Filtrer" />');
             }
          });
		}
    },

    /**
     * Methode: getColNames
     * Récupère de nom des colonnes.
     *
     * Paramètres:
     * datas - {Array} Tableau des résultats
     */
    getColNames: function (datas) {
        var colNames = [];
        for (var i = 0; i < datas.length; i++) {
           var keys = Object.keys(datas[i]);
           for (var j = 0; j < keys.length; j++) {
               var key = keys[j];
               if (key !== "bounds" && !colNames.includes(key)) {
                   colNames.push(key);
               }
           }
       }
       return colNames;
    },

    /**
     * Methode: checkBoundsColName
     * Vérifie la présence de la colonne "bounds".
     */
    checkBoundsColName: function (datas) {
        for (var i = 0; i < datas.length; i++) {
           var keys = Object.keys(datas[i]);
           for (var j = 0; j < keys.length; j++) {
               if (keys[j] === "bounds" && datas[i].bounds instanceof Array) {
                   return true;
               }
           }
       }
       return false;
    },
    /**
     * Methode: gotoBounds
     * Demande la localisation rapide sur un objet, en déclenchant l'événement 'gotoFeatureBounds'.
     *
     * Paramètres:
     * elt - {DOMElement} Elément DOM de la page correspondant au pictogramme de l'objet sélectionné.
     */
    gotoBounds: function (elt) {
        var bounds = this.model[elt.parentNode.rowIndex - 1].bounds;
        this.events.triggerEvent('gotoFeatureBounds', bounds);
    },

    CLASS_NAME: 'Descartes.UI.DataGrid'
});

module.exports = Class;
