/* global Descartes */

require('../css/LayersTree.css');
require('../css/jstree-style-custom.css');
require('bootstrap-slider/dist/css/bootstrap-slider.css');

var _ = require('lodash');
var jQuery = require('jquery');
var log = require('loglevel');
var Slider = require("bootstrap-slider");

var Utils = require('../../Utils/DescartesUtils');
var UI = require('../../Core/UI');
var UIConstants = require('../UIConstants');
var Group = require('../../Model/Group');
var Layer = require('../../Model/Layer');

var treeItemTemplate = require('./templates/TreeItemSimple.ejs');

/**
 * Class: Descartes.UI.LayersTreeSimple
 * Classe affichant une arborescence du contenu de la carte et offrant des interactions sur celui-ci.
 *
 * Hérite de:
 * - <Descartes.UI>
 *
 * Evénements déclenchés:
 * layerSelected - Une couche a été sélectionnée.
 * groupSelected - Un groupe a été sélectionné.
 * nodeUnselect - Une couche ou une couche a été désélectionné.
 * rootSelected - La racine a été sélectionnée.
 */
var Class = Utils.Class(UI, {
    tree: null,
    treeConfig: null,
    sliders: [],
    EVENT_TYPES: ['treeSimpleUserEvent'],
    savItemsOpen: [],
    defaultDisplayClasses: {
        tree: 'Descartes-jstree-vuesimple',
        itemText: 'DescartesLayersTreeText',
        layerVisibilityOn: 'jstee-vuesimple fa fa-eye DescartesLayersTreeIconEnable',
        layerVisibilityOff: 'jstee-vuesimple fa fa-eye-slash DescartesLayersTreeIconEnable',
        layerVisibilityOnForbidden: 'jstee-vuesimple fa fa-eye DescartesLayersTreeIconColorForbidden',
        layerVisibilityNeedZoomOut: 'jstee-vuesimple fa fa-minus DescartesLayersTreeBt4LayerZoomOut',
        layerVisibilityNeedZoomIn: 'jstee-vuesimple fa fa-plus DescartesLayersTreeBt4LayerZoomIn',
        layerInfoEnabled: 'jstee-vuesimple fa fa-info-circle DescartesLayersTreeIconColor',
        layerInfoDisabled: 'jstee-vuesimple fa fa-ban DescartesLayersTreeIconColor',
        layerInfoForbidden: 'jstee-vuesimple fa fa-ban DescartesLayersTreeIconColorForbidden',
        blankBlocQuote: 'DescartesLayersTreeBlockQuote',
        layerOpacitySliderTrack: 'DescartesLayersTreeLayerSliderTrack',
        layerOpacitySliderHandle: 'DescartesLayersTreeLayerSliderHandle'
    },

    /**
     * Propriete: data
     * {Object} Objet JSON contenant les groupes et couches à afficher au format jsTree.
     */
    data: null,
    /**
     * Propriete: jstreeTheme
     * {String} Nom du thème utilisé par jstree.
     */
    jstreeTheme: 'descartes',

    initialize: function (div, layersModel, options) {
        this.savItemsOpen = [];
        UI.prototype.initialize.apply(this, [div, layersModel, this.EVENT_TYPES, options]);
    },

    /**
     * Methode: draw
     * Construit et affiche l'arborescence de gestion du contenu de la carte.
     */
    draw: function () {
        var self = this;
        var divSelector = '#' + self.div.id;

        this.tree = jQuery.jstree.reference(divSelector);
        if (this.tree) {
            log.debug('Draw refresh treeSimple');
            this.tree.refresh(true, true);

        } else {
            log.debug('Draw First Time treeSimple');

            this.treeConfig = {

                core: {
                    themes: {
                        name: this.jstreeTheme,
                        icons: false,
                        dots: false
                    },
                    data: function (obj, callback) {
                        self.structureaplat = [];
                        var json = self._getStructure(self.model.item);
                        self.timestamp = new Date().getTime();
                        json[0].data = self.timestamp;
                        callback.call(this, json);
                    }
                }
            };

            var treeDiv = jQuery(divSelector);
            if (!treeDiv.hasClass(this.defaultDisplayClasses.tree)) {
                treeDiv.addClass(this.defaultDisplayClasses.tree);
            }


            treeDiv.jstree(this.treeConfig);
            this.tree = jQuery.jstree.reference(divSelector);

            treeDiv.on('loaded.jstree', function (e, data) {
                log.debug('loaded', arguments);
            });
            treeDiv.on('redraw.jstree', function (e, data) {
                if (data.instance._model.data) {
                    var keys = Object.keys(data.instance._model.data);
                    if (!_.isNil(keys[1])) {
                        var dataTimestamp = data.instance._model.data[keys[1]].data;
                        if (self.timestamp === dataTimestamp) {
                           self._afterRender();
                        }
                    }
                }
            });
            this.tree = jQuery.jstree.reference('#' + self.div.id);

            if (this.tree) {
                //besoin de rafraichir pour ne pas afficher les checkbox de l'arbre
                this.tree.refresh(true, true);
            }

           jQuery(this.div).on('hidden.bs.collapse', function (e) {
               var pos = self.savItemsOpen.indexOf(e.target.id);
               if (pos >= 0) {
               self.savItemsOpen.splice(pos, 1);
               }
           });
           jQuery(this.div).on('shown.bs.collapse', function (e) {
               var pos = self.savItemsOpen.indexOf(e.target.id);
               if (pos === -1) {
                   self.savItemsOpen.push(e.target.id);
               }
           });

        }
    },

    /**
     * Méthode _afterRender
     * Méthode appelée une fois le rendu de l'ihm effectué
     */
    _afterRender: function () {
        log.debug('afterRender call');

        if (this.model.fctOpacity) {
            this._addToolsSlider();
        }

        //ajout handler de changement de visibilité
        var classOnSelector = this.displayClasses.layerVisibilityOn.replace(/ /g, '.'); //si plusieurs classes css
        var classOffSelector = this.displayClasses.layerVisibilityOff.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector + ',.' + classOffSelector).click(this.toggleLayerVisibilityByDescartesIconChanged.bind(this));

        //ajout handler de changement de requête d'information
        classOnSelector = this.displayClasses.layerInfoEnabled.replace(/ /g, '.'); //si plusieurs classes css
        classOffSelector = this.displayClasses.layerInfoDisabled.replace(/ /g, '.'); //si plusieurs classes css
        jQuery('.' + classOnSelector + ',.' + classOffSelector).click(this.toggleLayerQueryInfo.bind(this));
    },
    _getNodeStructure: function (node, nodeId) {
        var result = null;
        if (node.id === nodeId) {
            result = node;
        } else if (node.children) {
            for (var i = 0; i < node.children.length; i++) {
                var child = node.children[i];
                result = this._getNodeStructure(child, nodeId);
                if (result !== null) {
                    return result;
                }
            }
        }
        return result;
    },
    _getStructure: function (node) {

        if (node.CLASS_NAME.indexOf('Descartes.Group') === 0) {
            if (node.items) {
                for (var i = 0; i < node.items.length; i++) {
                    this._getStructure(node.items[i]);
                }

            }
        } else if (node.CLASS_NAME.indexOf('Descartes.Layer') === 0) {
            this.structureaplat.push(this._getStructureLayer(node));
        }

        return this.structureaplat;
    },
    _getStructureLayer: function (node) {
        var id = this.CLASS_NAME.replace(/\./g, '') + node.index.toString();
        var sliderId = id + 'SliderB';
        var visibleClassName = this.displayClasses.layerVisibilityOff;
        var visibleTitle = '';
        var needZoomClassName = null;

        var state = {
            opened: node.opened,
            disabled: true
        };
        if (node.hide) {
            state.hidden = true;
        }

        if (node.CLASS_NAME.indexOf('Descartes.Layer') === 0) {
            state.checked = node.visible;

            if (!node.acceptMaxScaleRange()) {
                needZoomClassName = this.displayClasses.layerVisibilityNeedZoomOut;
                visibleClassName = this.displayClasses.layerVisibilityOnForbidden;
                node.loading = false;
            } else if (!node.acceptMinScaleRange()) {
                needZoomClassName = this.displayClasses.layerVisibilityNeedZoomIn;
                visibleClassName = this.displayClasses.layerVisibilityOnForbidden;
                node.loading = false;
            } else if (node.visible) {
               visibleClassName = this.displayClasses.layerVisibilityOn;
            }

            //récupération du tooltip à afficher
            if (node.isInScalesRange()) {
                visibleTitle = (node.visible ? this.getMessage('HIDE_LAYER') : this.getMessage('SHOW_LAYER'));
            } else if (node.minScale !== null && node.maxScale !== null) {
                visibleTitle = this.getMessage('MIN_SCALE') +
                        Utils.readableScale(node.minScale) +
                        this.getMessage('MAX_SCALE') +
                        Utils.readableScale(node.maxScale);
            } else if (node.minScale != null) {
                visibleTitle = this.getMessage('MIN_SCALE_ONLY') + Utils.readableScale(node.minScale);
            } else {
                visibleTitle = this.getMessage('MIN_SCALE_ONLY') + Utils.readableScale(node.maxScale);
            }
        }

        var itemTextClassName = this.displayClasses.itemText;

        var queryableInfo = this.getQueryableInfo(node);

        var text = treeItemTemplate({
            id: sliderId,
            node: node,
            displayIconLoading: this.model.displayIconLoading,
            visibleClassName: visibleClassName,
            visibleTitle: visibleTitle,
            needZoomClassName: needZoomClassName,
            itemTextClassName: itemTextClassName,
            fctVisibility: this.model.fctVisibility,
            fctOpacity: this.model.fctOpacity,
            fctQueryable: this.model.fctQueryable,
            fctDisplayLegend: this.model.fctDisplayLegend,
            queryableClassName: queryableInfo.className,
            queryableTitle: queryableInfo.title
        });

        var json = {
            id: id,
            text: text,
            icon: '',
            state: state
        };

        return json;
    },

    getQueryableInfo: function (layer) {
        if (this.model.fctQueryable) {
            var className = '';
            var title = '';
            if (layer.queryable) {
                if (layer.visible && layer.isInScalesRange()) {
                    className = (layer.activeToQuery ? this.displayClasses.layerInfoEnabled : this.displayClasses.layerInfoDisabled);
                    title = (layer.activeToQuery ? this.getMessage('DISABLE_QUERY') : this.getMessage('ENABLE_QUERY'));
                } else {
                    className = this.displayClasses.layerInfoForbidden;
                    title = this.getMessage('FORBIDDEN_QUERY');
                }
            } else {
                className = this.displayClasses.blankBlocQuote;
                title = this.getMessage('NO_QUERY');
            }

            var sheetableClassName = '';
            var sheetableTitle = '';
            if (layer.sheetable) {
                if (layer.visible && layer.isInScalesRange()) {
                    sheetableClassName = this.displayClasses.layerSheetActive;
                    sheetableTitle = this.getMessage('ACTIVE_SHEET');
                } else {
                    sheetableClassName = this.displayClasses.layerSheetInactive;
                    sheetableTitle = this.getMessage('INACTIVE_SHEET');
                }
            } else {
                sheetableClassName = this.displayClasses.blankBlocQuote;
                sheetableTitle = this.getMessage('NO_QUERY');
            }

            return {
                className: className,
                title: title,
                sheetableClassName: sheetableClassName,
                sheetableTitle: sheetableTitle
            };
        }
        return {};
    },

    /**
     * Private
     *
     * Ajout de l'outil permettant de modifier l'opacité d'une couche (slider).
     */
    _addToolsSlider: function () {
        var spanSliders = jQuery('span[id$="SliderB"]');

        log.debug('addToolSlider', spanSliders);

        _.each(this.sliders, function (item) {
            item.slider.destroy();
        });
        this.sliders = [];

        var self = this;
        for (var i = 0; i < spanSliders.length; i++) {
            var spanSlider = spanSliders[i];

            var descartesNodeIndex = spanSlider.id.substring(self.CLASS_NAME.replace(/\./g, "").length, spanSlider.id.indexOf('SliderB'));
            log.debug('descartesNodeIndex=', descartesNodeIndex);
            var itemDescartes = self.model.getItemByIndex(descartesNodeIndex);
            log.debug('itemDescartes=', itemDescartes);

            var slider = new Slider(spanSlider, {
                id: descartesNodeIndex,
                max: itemDescartes.opacityMax,
                min: 0,
                step: 1,
                value: itemDescartes.opacity,
                tooltip: 'hide'
            });
            self.sliders.push({
                index: descartesNodeIndex,
                slider: slider
            });
        }

        //impossible d'initialiser les listeners dans la boucle for.
        _.each(this.sliders, function (item) {
            item.slider.on('slide', function (value) {
                //remove dnd tooltip
                jQuery('#vakata-dnd').hide();
                self.changeOpacity(item.index, value);
            });
            item.slider.on('change', function (e) {
                self.changeOpacity(item.index, e.newValue);
            });
        });

    },
    /**
     * Methode: toggleLayerVisibilityByDescartesIconChanged
     * Inverse la visibilité des groupes et des couches lors du changement de la visibilite d'une couche.
     *
     * Paramètres:
     * current - {HTMLElement} L'élément DOM concerné.
     */
    toggleLayerVisibilityByDescartesIconChanged: function (event) {
        var visibilityId = jQuery(event.target).closest('a').attr('id');
        var nodeDescartesIndex = visibilityId.substring(this.CLASS_NAME.replace(/\./g, "").length, visibilityId.indexOf('_'));
        var descartesLayer = this.model.getItemByIndex(nodeDescartesIndex);

        if (descartesLayer.isInScalesRange && descartesLayer.isInScalesRange()) {
            this.model.refreshVisibilities(descartesLayer);
            this.tree.refresh(true, true);
            this.events.triggerEvent('treeSimpleUserEvent');
            /*var self = this;
            setTimeout(function () {
                console.log("timeout");
                self.reOpenItems();
            }, 500);*/
        }

    },

    /**
     * Methode: toggleLayerQueryInfo
     * Inverse la capacité à être interrogée de la couche.
     *
     * Paramêtres:
     * event - evenement associé au clic
     */
    toggleLayerQueryInfo: function (event) {
        var queryableId = event.target.parentNode.parentNode.id;
        var nodeDescartesIndex = queryableId.substring(this.CLASS_NAME.replace(/\./g, "").length, queryableId.indexOf('_'));

        var descartesLayer = this.model.getItemByIndex(nodeDescartesIndex);

        if (descartesLayer && descartesLayer.queryable && descartesLayer.visible && descartesLayer.isInScalesRange()) {
            descartesLayer.activeToQuery = !descartesLayer.activeToQuery;
            this.tree.refresh(true, true);
            this.events.triggerEvent('treeSimpleUserEvent');
            var self = this;
            setTimeout(function () {
                console.log("timeout");
                self.reOpenItems();
            }, 500);
        }
    },
    /**
     * Methode: changeOpacity
     * Modifie l'opacité de la couche.
     *
     * Paramêtres:
     * nodeIndex - {String} Index hiérarchique de la couche dans l'arborescence.
     * value - {Integer} Valeur de l'opacité.
     */
    changeOpacity: function (nodeIndex, value) {
        var item = this.model.getItemByIndex(nodeIndex);
        this.model.changeOpacity(item, value);
        this.events.triggerEvent('treeSimpleUserEvent');
    },

    reOpenItems: function () {
        for (var i = 0; i < this.savItemsOpen.length; i++) {
            jQuery("#" + this.savItemsOpen[i]).collapse('show');
        }
    },

    jqSelector: function (str) {
        return str.replace(/([;&,\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g, '\\$1');
    },

    CLASS_NAME: 'Descartes.UI.LayersTreeSimple'
});

module.exports = Class;
