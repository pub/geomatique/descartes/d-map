/* global MODE, GEOREF */

var _ = require('lodash');

var UI = require('../Core/UI');
var AbstractInPlace = require('./' + MODE + '/AbstractInPlace');
var AbstractPrinterSetup = require('./' + MODE + '/AbstractPrinterSetup');
var ConfirmDialog = require('./' + MODE + '/ConfirmDialog');
var LayersTree = require('./' + MODE + '/LayersTree');
var LayersTreeSimple = require('./' + MODE + '/LayersTreeSimple');
var BookmarksInPlace = require('./' + MODE + '/BookmarksInPlace');
var CoordinatesInputDialog = require('./' + MODE + '/CoordinatesInputDialog');
var CoordinatesInputInPlace = require('./' + MODE + '/CoordinatesInputInPlace');
var DataGrid = require('./' + MODE + '/DataGrid');
var FormDialog = require('./' + MODE + '/FormDialog');
var ResultFormDialog = require('./' + MODE + '/ResultFormDialog');
var ModalFormDialog = require('./' + MODE + '/ModalFormDialog');
var GazetteerInPlace = require('./' + MODE + '/GazetteerInPlace');
var PrinterSetupDialog = require('./' + MODE + '/PrinterSetupDialog');
var PrinterSetupInPlace = require('./' + MODE + '/PrinterSetupInPlace');
var RequestManagerInPlace = require('./' + MODE + '/RequestManagerInPlace');
var RequestManagerGazetteer = require('./' + MODE + '/RequestManagerGazetteer');
var SaveBookmarkDialog = require('./' + MODE + '/SaveBookmarkDialog');
var ScaleChooserInPlace = require('./' + MODE + '/ScaleChooserInPlace');
var ScaleSelectorInPlace = require('./' + MODE + '/ScaleSelectorInPlace');
var SizeSelectorInPlace = require('./' + MODE + '/SizeSelectorInPlace');
var TabbedDataGrids = require('./' + MODE + '/TabbedDataGrids');
var ToolTipContainer = require('./' + MODE + '/ToolTipContainer');
var WaitingDialog = require('./' + MODE + '/WaitingDialog');

var namespace = {
    AbstractInPlace: AbstractInPlace,
    AbstractPrinterSetup: AbstractPrinterSetup,
    BookmarksInPlace: BookmarksInPlace,
    ConfirmDialog: ConfirmDialog,
    CoordinatesInputDialog: CoordinatesInputDialog,
    CoordinatesInputInPlace: CoordinatesInputInPlace,
    DataGrid: DataGrid,
    FormDialog: FormDialog,
    ResultFormDialog: ResultFormDialog,
    ModalFormDialog: ModalFormDialog,
    GazetteerInPlace: GazetteerInPlace,
    LayersTree: LayersTree,
    LayersTreeSimple: LayersTreeSimple,
    PrinterSetupDialog: PrinterSetupDialog,
    PrinterSetupInPlace: PrinterSetupInPlace,
    RequestManagerInPlace: RequestManagerInPlace,
    RequestManagerGazetteer: RequestManagerGazetteer,
    SaveBookmarkDialog: SaveBookmarkDialog,
    ScaleChooserInPlace: ScaleChooserInPlace,
    ScaleSelectorInPlace: ScaleSelectorInPlace,
    SizeSelectorInPlace: SizeSelectorInPlace,
    TabbedDataGrids: TabbedDataGrids,
    ToolTipContainer: ToolTipContainer,
    WaitingDialog: WaitingDialog
};

if (GEOREF) {
    namespace.LocalisationAdresseInPlace = require('./' + MODE + '/LocalisationAdresseInPlace');
}

var constants = require('./UIConstants');
_.extend(namespace, constants);
_.extend(UI, namespace);

module.exports = UI;
