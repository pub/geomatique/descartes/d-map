/* global NAME, VERSION, MODE, GEOREF */

var proj4 = require('proj4');
var _ = require('lodash');
var log = require('loglevel');

//Pris en compte lors de la compilation d'ol voir
//require('./OpenLayersExtras/tileUrlFunction_overloaded');

var ActionNamespace = require('./Action/ActionNamespace');

var ButtonNamespace = require('./Button/ButtonNamespace');

var ToolBarNamespace = require('./Toolbar/ToolBarNamespace');
var I18N = require('./Core/I18N');
var Projection = require('./Core/Projection');
var ExternalCallsUtils = require('./Core/ExternalCallsUtils');
var ExternalCallUtilsConstants = require('./Core/ExternalCallsUtilsConstants');

_.extend(ExternalCallsUtils, ExternalCallUtilsConstants);

var InfoNamespace = require('./Info/InfoNamespace');

var MapNamespace = require('./Map/MapNamespace');

var LayerNamespace = require('./Model/LayerNamespace');
var Request = require('./Model/Request');
var ResourceLayerNamespace = require('./Model/ResourceLayerNamespace');
var Group = require('./Model/Group');
var RequestMember = require('./Model/RequestMember');
var MapContent = require('./Model/MapContent');
var WmsLayer = require('./Model/WmsLayer');
var GazetteerService = require('./Model/GazetteerService');
var GazetteerLevel = require('./Model/GazetteerLevel');
var Url = require('./Model/Url');
var UrlExterne = require('./Model/UrlExterne');
var UrlParam = require('./Model/UrlParam');

var ToolNamespace = require('./Tool/ToolNamespace');

var Size = require('./Utils/DescartesSize');
var UtilsNamespace = require('./Utils/UtilsNamespace');

var UINamespace = require('./UI/UINamespace');

var DMapConstants = require('./DMapConstants');
var DMapBootstrap4Constants = require('./DMapBootstrap4Constants');
var Messages = require('./Messages');
var Symbolizers = require('./Symbolizers');

var DMap = {
    name: function () {
        return NAME + ' ' + this.version();
    },
    version: function () {
        return VERSION;
    },
    mode: function () {
        return MODE;
    },
    Action: ActionNamespace,
    Button: ButtonNamespace,
    ExternalCallsUtils: ExternalCallsUtils,
    GazetteerService: GazetteerService,
    GazetteerLevel: GazetteerLevel,
    Group: Group,
    I18N: I18N,
    Info: InfoNamespace,
    Layer: LayerNamespace,
    Log: log,
    Map: MapNamespace,
    MapContent: MapContent,
    Messages: Messages,
    Proj4: proj4,
    Projection: Projection,
    Request: Request,
    RequestMember: RequestMember,
    ResourceLayer: ResourceLayerNamespace,
    Tool: ToolNamespace,
    ToolBar: ToolBarNamespace,
    Size: Size,
    Symbolizers: Symbolizers,
    UI: UINamespace,
    Url: Url,
    UrlExterne: UrlExterne,
    UrlParam: UrlParam,
    Utils: UtilsNamespace,
    WmsLayer: WmsLayer
};

if (GEOREF) {
    DMap.LocalisationParcellaire = require('./Model/LocalisationParcellaire');
}

_.extend(DMap, DMapConstants);
_.extend(DMap, DMapBootstrap4Constants);

module.exports = DMap;
