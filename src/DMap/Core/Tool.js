/* global Descartes */
var ol = require('openlayers');
var log = require('loglevel');
var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var EventManager = require('../Utils/Events/EventManager');

var I18N = require('./I18N');

/**
 * Class: Descartes.Tool
 * Classe "abstraite" définissant un bouton d'interaction avec la carte.
 *
 * :
 * Le bouton doit être placé dans une barre d'outils définie par la classe <Descartes.ToolBar>.
 *
 * Un seul bouton de ce type peut être accessible à chaque instant parmi l'ensemble des boutons d'interaction de la barre d'outils.
 *
 * Hérite de:
 *  - <Descartes.I18N>
 *  - ol.control.Control
 *
 * Classes dérivées:
 *  - <Descartes.Tool.CenterMap>
 *  - <Descartes.Tool.DragPan>
 *  - <Descartes.Tool.GeolocationSimple>
 *  - <Descartes.Tool.GeolocationTracking>
 *  - <Descartes.Tool.Selection>
 *  - <Descartes.Tool.ZoomIn>
 *  - <Descartes.Tool.ZoomOut>
 *  - <Descartes.Tool.ZoomToInitialExtent>
 *  - <Descartes.Tool.ZoomToMaximalExtent>
 *  - <Descartes.Tool.Measure>
 */
var Tool = Utils.Class(ol.control.Control, I18N, {

    /**
	 * Propriete: id
	 * {string} Identifiant unique de l'outil.
	 */
    id: null,

    interaction: null,

    displayClass: null,

    events: null,

    /**
     * Constructeur: Descartes.Tool
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * title - {String} Texte de l'info-bulle du bouton.
     */
    initialize: function (opt_options) {
        I18N.prototype.initialize.call(this);
        var options = {};
        for (var i = 0; i < arguments.length; i++) {
            _.extend(options, arguments[i]);
        }

        this.id = Utils.createUniqueID();
        this.events = new EventManager();

        if (_.isNil(this.displayClass)) {
            this.displayClass = this.CLASS_NAME.replace('OpenLayers.', 'ol').replace(/\./g, '');
        }

        if (options.title) {
            this.title = options.title;
        } else {
            this.title = this.getMessage('TITLE');
        }


        if (!_.isNil(options.target)) {
            var element = this.createElement();
            if (_.isString(element)) {
                element = Utils.htmlToElement(element);
            }

            element.onclick = this.handleClick.bind(this);

            ol.control.Control.call(this, {
                element: element,
                target: options.target
            });
        }
    },
    /**
     * Methode: createElement
     * Méthode à implémenter par les classes filles pour créer le bouton.
	 * Return {String|DomElent}
     */
    createElement: function () {
    },
   /**
     * Methode: updateElement
     * Méthode à implémenter par les classses filles pour mettre à jour le bouton lorsque l'outil change d'état (actif/inactif)
     */
    updateElement: function () {
    },
    /**
     * Methode: handleClick
     * Méthode appeler lorsque l'utilisateur clique sur l'outil.
     */
    handleClick: function () {
        var map = this.getMap();
        var interactions = map.getInteractions().getArray();

        if (!_.isNil(this.interaction)) {
            if (_.indexOf(interactions, this.interaction) === -1) {
                map.addInteraction(this.interaction);
            }
        }
        if (_.isFunction(this.getAdditionnalInterations)) {
            var additionnalInteractions = this.getAdditionnalInterations();
            if (_.isArray(additionnalInteractions)) {
                _.each(additionnalInteractions, function (additionnalInteraction) {
                    map.addInteraction(additionnalInteraction);
                });
            }
        }

        if (this.isActive()) {
            this.deactivate();
        } else {
            this.activate();
        }
    },
    isActive: function () {
        var active = false;
        if (!_.isNil(this.interaction)) {
            active = this.interaction.getActive();
        } else if (!_.isNil(this.active)) {
            active = this.active;
        }
        return active;
    },

    /**
     * Methode: activate
     * Active le bouton et désactive les autres boutons d'interaction de la barre d'outils.
     */
    activate: function () {
        if (this.isActive()) {
            return false;
        }
        if (this.interaction) {
            this.interaction.setActive(true);
        }

        this.events.triggerEvent('activate', this);

        this.updateElement();

        Descartes._activeClickToolTip = false;

        return true;
    },
    /**
     * Methode: deactivate
     * Désactive le bouton
     */
    deactivate: function () {
        if (!this.isActive()) {
            return false;
        }
        if (this.interaction) {
            this.interaction.setActive(false);
        }
        this.updateElement();
        Descartes._activeClickToolTip = true;
        return true;

    },
    CLASS_NAME: 'Descartes.Tool'
});


module.exports = Tool;
