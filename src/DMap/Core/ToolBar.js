/* global MODE, Descartes */

var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var Button = require('../Button/' + MODE + '/Button');
var Tool = require('./Tool');
var ToolBarConstants = require('../Toolbar/ToolBarConstants');
var log = require('loglevel');

var EventManager = require('../Utils/Events/EventManager');

/**
 * Class: Descartes.ToolBar
 * Classe Abtraite définissant une barre d'outils, pouvant accueillir des boutons de type <Descartes.Button> et/ou <Descartes.Tool>.
 *
 */
var Class = Utils.Class({

    /**
	 * Propriete: OL_map
	 * {ol.Map} Carte OpenLayers réagissant aux boutons de la barre d'outils.
	 */
    olMap: null,

    /**
     * Propriete: toolBarName
     * {String} Le nom de la barre d'outils.
     */
    toolBarName: '',

    toolBarOpener: null,

    controls: [],

    visible: true,

    /**
     * Propriete: draggable
     * {Object} propriétés permettant d'activer ou non le déplacement de la barre d'outil.
     */
    draggable: {
        enable: false
    },

    /**
     * Propriete: resizable
     * {Boolean} activation ou non du redimensionnement de la barre d'outil
     */
    resizable: false,

    panel: false,

    grouped: true,

    /**
     * Propriete: vertical
     * {Boolean} création ou non d'une barre d'outil verticale
     */
    vertical: false,

    closable: false,

    events: null,
    /**
	 * Constructeur: Descartes.ToolBar
	 * Classe Abstraite
	 *
	 * Paramètres:
	 * div - {DOMElement|String} Elément DOM de la page accueillant la barre d'outils.
	 * OL_map - {ol.Map} Carte OpenLayers réagissant aux boutons de la barre d'outils.
	 *
	 * Options de construction propres à la classe:
	 */
    initialize: function (div, olMap, options) {
        this.events = new EventManager();
        this.controls = [];
        this.olMap = olMap;
        this.toolBarName = this._computeToolBarName();
        this.id = 'DescartesToolBar_' + ToolBarConstants.ID++;
        if (!_.isNil(options)) {
            _.extend(this, options);
        }
        this.div = Utils.getDiv(div);

        if (_.isNil(this.div)) {
            this.div = document.createElement('div');
            this.div.id = 'DescartesToolBarId_' + this.id;
            this.olMap.getViewport().parentNode.appendChild(this.div);
        }

        this.createToolBar();
    },
    /**
     * A implémenter par les classes filles
     */
    createToolBar: function () {
    },
    /**
     * Methode: addControls
     * Ajoute plusieurs boutons à la barre d'outils.
     *
     * Paramètres:
     * control - {Array(Descartes.Button|Descartes.Tool)} Boutons à ajouter.
     */
    addControls: function (controls) {
        if (!(controls instanceof Array)) {
            controls = [controls];
        }
        for (var i = 0; i < controls.length; i++) {
            this.addControl(controls[i]);
        }
    },

    /**
     * Methode: removeControls
     * Supprime plusieurs boutons à la barre d'outils.
     */
    removeControls: function () {
        for (var i = 0; i < this.controls.length; i++) {
            this.removeControl(this.controls[i]);
        }
        this.controls = [];
    },
    /**
     * Methode: removeControl
     * Supprime un bouton à la barre d'outils.
     *
     * Paramètres:
     * control - {Descartes.Button|Descartes.Tool} Bouton à ajouter.
     */
    removeControl: function (control) {
        if (control instanceof Button) {
            this.olMap.un('moveend', control.refresh.bind(control), control);
        } else if (control.deactivate) {
            control.deactivate();
        }
    },
    /**
     * Methode: addControl
     * Ajoute un bouton à la barre d'outils.
     *
     * Paramètres:
     * control - {Descartes.Button|Descartes.Tool} Bouton à ajouter.
     */
    addControl: function (control) {
        control.olMap = this.olMap;
        this.controls.push(control);
        if (control instanceof Button) {
            var innerDiv = document.getElementById(this.id);
            control.createButton(innerDiv);
            this.olMap.on('moveend', control.refresh.bind(control), control);
        } else if (control instanceof Tool) {
            this.olMap.addControl(control);
            if (!_.isNil(control.interaction)) {
                this.olMap.addInteraction(control.interaction);
            }
            control.events.register('activate', this, this.deactivateOthers);
        }
    },
    deactivateOthers: function (e) {
        var activateTool = e.data[0];
        var others = this.olMap.getControls().getArray();
        _.each(others, function (control) {
            if (control.id !== activateTool.id) {
                log.debug('Deactivate tool %s, with id %s', control.CLASS_NAME, control.id);
                if (control.deactivate && control.CLASS_NAME !== 'Descartes.Button.DirectionalPanPanel') {
                    control.deactivate(true, false);
                }
            }
        });
    },
    alignTo: function (anchorDiv) {
        this.div.style.marginLeft = anchorDiv.offsetLeft + 'px';
    },
    /**
     * Methode: destroy
     * Supprime la barre d'outils.
     */
    destroy: function () {
        for (var i = (this.div.childNodes.length - 1); i >= 0; i--) {
            this.div.removeChild(this.div.childNodes[i]);
        }
        this.removeControls();
        this.events.triggerEvent('closed');
    },
    CLASS_NAME: 'Descartes.AbstractToolBar'
});

module.exports = Class;
