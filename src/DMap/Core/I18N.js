var _ = require('lodash');
var Utils = require('../Utils/DescartesUtils');

/**
 * Class: Descartes.I18N
 * Classe "abstraite" assurant une gestion externalisée des messages.
 *
 * :
 * Le constructeur de la classe est chargé de récupérer les messages associés à cette classe.
 *
 * Ces messages doivent être stockés dans un objet JSON dont le nom est dérivé du nom de la classe selon les schémas suivants :
 * - Nom de la classe :
 * :     Descartes.paquetage.sousPaquetage.nomClasse
 * - Nom de l'objet JSON :
 * :     Descartes_Messages_paquetage_sousPaquetage_nomClasse
 *
 * La profondeur des paquetages et sous-paquetages n'est pas limitée.
 *
 * Classes dérivées:
 *  - <Descartes.Action>
 *  - <Descartes.Button>
 *  - <Descartes.Info>
 *  - <Descartes.Tool>
 *  - <Descartes.UI>
 *  - <Descartes.Tool.MiniMap>
 *  - <Descartes.Tool.NavigationHistory>
 */
var Class = Utils.Class({
    /**
     * Propriete: messages
     * {Object} Objet JSON stockant les messages utilisables par la classe.
     */
    messages: null,

    initialize: function () {
        this.messages = Utils.loadMessages(this.CLASS_NAME);
    },
    /**
     * Methode: getMessage
     * Retourne un message stocké dans l'objet des messages associé à la classe.
     *
     * Paramêtres:
     * key - {String} Clé du message recherché.
     *
     * Retour:
     * {String} Le texte du message recherché.
     */
    getMessage: function (key) {
        var aMessage = '';
        if (!_.isNil(this.messages) && !_.isNil(this.messages[key])) {
            aMessage = this.messages[key];
        }
        return aMessage;
    },
    CLASS_NAME: 'Descartes.I18N'
});

module.exports = Class;
