/* global Descartes */
var _ = require('lodash');

var ol = require('openlayers');

var Utils = require('../Utils/DescartesUtils');
var ExternalCallsUtilsConstants = require('./ExternalCallsUtilsConstants');
var DMapConstants = require('../DMapConstants');
var ResultLayer = require('../Model/ResultLayer');
/**
 * Class: Descartes.ExternalCallsUtils
 * Classe offrant des méthodes statiques utilitaires pour la gestion des appels aux services côté serveur.
 */
var Object = {
    /**
     * Staticmethode: getWfsRequestForQuery
     * Construit une requête WFS/GetFeature.
     *
     * Paramètres:
     * featureServerParams - {Object} Objet JSON contenant les propriétés du FeatureType, sous la forme
     * {serverUrl:"<urlServeur>", featureName:"<nomObjet>", serverVersion:"<nomObjet>"}.
     *
     * :
     * Le paramètre "VERSION" de la requête WFS/GetFeature utilise la propriété "serverVersion" du paramètre "featureServerParams" ou par défaut "1.0.0".
     *
     * Retour:
     * {String} La requête WFS/GetFeature.
     */
    getWfsRequestForQuery: function (featureServerParams, olMap) {
        var version = '1.0.0';
        if (!_.isNil(featureServerParams.serverVersion)) {
            version = featureServerParams.serverVersion;
        }
        var labelTypeName = 'TYPENAME=';
        if (version === '2.0.0') {
            labelTypeName = 'TYPENAMES=';
        }
        var urlRequest = this.adaptationUrl(featureServerParams.serverUrl) + 'SERVICE=WFS&VERSION=' + version + '&REQUEST=GetFeature&' + labelTypeName + encodeURIComponent(encodeURIComponent(featureServerParams.featureName));
        if (!_.isNil(olMap)) {
            urlRequest += '&SRSNAME=' + olMap.getView().getProjection().getCode();
        }
        return urlRequest;
    },

    /**
     * Staticmethode: getWmsRequest
     * Construit une requête WMS.
     *
     * Paramètres:
     * imageServerParams - {Object} Objet JSON contenant les propriétés de la Layer, sous la forme
     * {serverUrl:"<urlServeur>", layerName:"<nomCouche>", serverVersion:"<nomObjet>"}.
     *
     * olMap - {ol.Map} Carte OpenLayers contenant la Layer.
     * format - {String} Type MIME de la layer.
     * operation - {<Descartes.ExternalCallsUtils.WMS_GETMAP_OPERATION>|<Descartes.ExternalCallsUtils.WMS_GETFEATUREINFO_OPERATION>} Opération WMS à effectuer.
     *
     * Le paramètre "VERSION" de la requête WMS utilise la propriété "serverVersion" du paramètre "imageServerParams" ou par défaut "1.1.1".
     * Selon la version, les paramètres "SRS" ou "CRS" sont utilisés.
     *
     *
     * Retour:
     * {String} La requête WMS.
     */
    getWmsRequest: function (imageServerParams, olMap, format, operation) {
        var srs;
        var bbox = olMap.getView().calculateExtent(olMap.getSize());

        if (_.isNil(imageServerParams.internalProjection)) {
            srs = olMap.getView().getProjection().getCode();
        } else {
            srs = imageServerParams.internalProjection;
            bbox = ol.proj.transformExtent(bbox, olMap.getView().getProjection().getCode(), srs);
        }

        var version = '1.1.1';
        if (!_.isNil(imageServerParams.serverVersion)) {
            version = imageServerParams.serverVersion;
        }

        var styles = '';
        if (!_.isNil(imageServerParams.layerStyles)) {
            styles = imageServerParams.layerStyles;
        }

        var reverseAxisOrder = false;
        var labelSRS = '&SRS=';
        if (version === '1.3' || version === '1.3.0') {
            labelSRS = '&CRS=';

            if (_.indexOf(DMapConstants.PROJECTIONS_WMS_1_3_0_YX, srs) !== -1) {
                reverseAxisOrder = true;
            }
        }

        var urlRequest = this.adaptationUrl(imageServerParams.serverUrl) +
                'SERVICE=WMS' +
                '&VERSION=' + version +
                '&REQUEST=' + operation +
                labelSRS + srs +
                '&WIDTH=' + parseInt(olMap.getSize()[0], 10) +
                '&HEIGHT=' + parseInt(olMap.getSize()[1], 10) +
                '&BBOX=' + Utils.toBBOX(bbox, null, reverseAxisOrder) +
                '&LAYERS=' + encodeURIComponent(encodeURIComponent(imageServerParams.layerName)) +
                '&STYLES=' + styles +
                '&FORMAT=' + format;
        if (format === 'image/jpeg' || format === 'image/jpg') {
            urlRequest += '&TRANSPARENT=FALSE&BGCOLOR=0xffffff';
        } else {
            urlRequest += '&TRANSPARENT=TRUE&BGCOLOR=0xffffff';
        }
        if (version === '1.3' || version === '1.3.0') {
            urlRequest += '&EXCEPTIONS=inimage';
        }
        return urlRequest;
    },

    /**
     * Staticmethode: getWmsRequestForQuery
     * Construit une requête WMS/GetFeatureInfo.
     *
     * Paramètres:
     * imageServerParams - {Object} Objet JSON contenant les propriétés de la Layer,
     * sous la forme {serverUrl:"<urlServeur>", layerName:"<nomCuche>", serverVersion:"<nomObjet>"}.
     * olMap - {ol.Map} Carte OpenLayers contenant la Layer.
     * format - {String} Type MIME de la layer.
     *
     * Par défaut, le nombre d'éléments demandés est 999.
     *
     * Retour:
     * {String} La requête WMS/GetFeatureInfo.
     */
    getWmsRequestForQuery: function (imageServerParams, olMap, format) {
        var urlRequest = this.getWmsRequest(imageServerParams, olMap, format, ExternalCallsUtilsConstants.WMS_GETFEATUREINFO_OPERATION) +
                '&QUERY_LAYERS=' + encodeURIComponent(encodeURIComponent(imageServerParams.layerName)) +
                '&INFO_FORMAT=application/vnd.ogc.gml' +
                '&FEATURE_COUNT=999';
        return urlRequest;
    },

    /**
     * Staticmethode: getWmsRequestForImage
     * Construit une requête WMS/GetMap.
     *
     * Paramètres:
     * imageServerParams - {Object} Objet JSON contenant les propriétés de la Layer, sous la forme
     * {serverUrl:"<urlServeur>", layerName:"<nomCouche>", serverVersion:"<nomObjet>"}.
     *
     * olMap - {ol.Map} Carte OpenLayers contenant la Layer.
     * format - {String} Type MIME de la layer.
     *
     * Retour:
     * {String} La requête WMS/GetFeatureInfo.
     */
    getWmsRequestForImage: function (imageServerParams, olMap, format) {
        var urlRequest = this.getWmsRequest(imageServerParams, olMap, format, ExternalCallsUtilsConstants.WMS_GETMAP_OPERATION);
        return urlRequest;
    },

    /**
     * Staticmethode: writeQueryPostBody
     * Construit le flux XML correspondant au paramètre "infos" des requêtes HTTP à envoyer aux services d'interrogation.
     *
     * Paramètres:
     * service - {ExternalCallsUtilsConstants.WMS_SERVICE|ExternalCallsUtilsConstants.WFS_SERVICE} Type du service OWS
     * mapContentLayers - {Array(<Descartes.Layer>)} Couches à interroger.
     * olMap - {ol.Map} Carte OpenLayers contenant les ressources OWS.
     *
     * Retour:
     * {String} Le flux XML pour le paramètres "infos"
     */
    writeQueryPostBody: function (service, mapContentLayers, olMap) {
        var postBody = null;
        var layersParams = '';
        for (var iLayer = 0, lenLayers = mapContentLayers.length; iLayer < lenLayers; iLayer++) {
            var layer = mapContentLayers[iLayer];
            var requestParams = '';
            for (var iResource = 0, lenResources = layer.resourceLayers.length; iResource < lenResources; iResource++) {
                if (service === ExternalCallsUtilsConstants.WMS_SERVICE) {
                    var imageServerParams = layer.resourceLayers[iResource].getImageServerParams();
                    if (imageServerParams !== null) {
                        requestParams += '<Request>' + this.getWmsRequestForQuery(imageServerParams, olMap, layer.format) + '</Request>';
                        requestParams += '<Version>WMS ' + imageServerParams.serverVersion + '</Version>';

                    }
                } else if (service === ExternalCallsUtilsConstants.WFS_SERVICE) {
                    var featureServerParams = layer.resourceLayers[iResource].getFeatureServerParams();
                    if (featureServerParams !== null) {
                        requestParams += '<Request>' + this.getWfsRequestForQuery(featureServerParams, olMap) + '</Request>';
                        requestParams += '<FeatureGeometryName>' + featureServerParams.featureGeometryName + '</FeatureGeometryName>';
                    }
                }
            }
            if (requestParams !== '') {
                layersParams += '<Layer><Name>' + encodeURI(layer.title) + '</Name>' + requestParams;
                if (layer.minScale) {
                    layersParams += '<LayerMinScale>' + layer.minScale + '</LayerMinScale>';
                }
                if (layer.maxScale) {
                    layersParams += '<LayerMaxScale>' + layer.maxScale + '</LayerMaxScale>';
                }
                if (layer.id) {
                    layersParams += '<LayerId>' + layer.id + '</LayerId>';
                    }

                layersParams += '</Layer>';
            }
        }
        if (layersParams !== '') {
            postBody = '<Infos>' + layersParams + '</Infos>';
        }
        return postBody;
    },
    /**
     * Staticmethode: writeGetFeatureExportRequestJson
     * Construit le flux Json.
     *
     * Paramètres:
     * service - {ExternalCallsUtilsConstants.WMS_SERVICE|ExternalCallsUtilsConstants.WFS_SERVICE} Type du service OWS
     * mapContentLayers - {Array(<Descartes.Layer>)} Couches à interroger.
     * olMap - {ol.Map} Carte OpenLayers contenant les ressources OWS.
     *
     * Retour:
     * {Object} Le flux Json
     */
    writeGetFeatureExportRequestJson: function (service, mapContentLayers, olMap, options) {
        var infosJson = [];
        for (var iLayer = 0, lenLayers = mapContentLayers.length; iLayer < lenLayers; iLayer++) {
            var layer = mapContentLayers[iLayer];
            var info = {};
            for (var iResource = 0, lenResources = layer.resourceLayers.length; iResource < lenResources; iResource++) {
                if (service === ExternalCallsUtilsConstants.WMS_SERVICE) {
                    var imageServerParams = layer.resourceLayers[iResource].getImageServerParams();
                    if (imageServerParams !== null) {
                        info.serviceType = ExternalCallsUtilsConstants.WMS_SERVICE;
                        info.requestForExport = this.getWmsRequestForQuery(imageServerParams, olMap, layer.format);
                        info.defaultDataProjection = layer.resourceLayers[iResource].internalProjection;
                        info.mapProjection = olMap.getView().getProjection().getCode();
                    }
                } else if (service === ExternalCallsUtilsConstants.WFS_SERVICE) {
                    var featureServerParams = layer.resourceLayers[iResource].getFeatureServerParams();
                    if (featureServerParams !== null) {
                        info.serviceType = ExternalCallsUtilsConstants.WFS_SERVICE;
                        info.requestForExport = this.getWfsRequestForQuery(featureServerParams, olMap);
                        info.featureGeometryName = featureServerParams.featureGeometryName;
                        info.version = featureServerParams.serverVersion;
                        info.featureNameSpace = layer.resourceLayers[iResource].featureNameSpace;
                        info.defaultDataProjection = layer.resourceLayers[iResource].featureInternalProjection;
                        info.mapProjection = olMap.getView().getProjection().getCode();
                    }
                }
            }

            info.layerTitle = layer.title;
            if (layer.id) {
                info.layerId = layer.id;
            }
            var paramFilter;
            if (options && options.fluxGML) {
                paramFilter = "&FILTER=" + encodeURI("(<Filter><Intersects><PropertyName>" + info.featureGeometryName + "</PropertyName>" + options.fluxGML + "</Intersects></Filter>)");
                info.requestForExport = info.requestForExport + paramFilter;
            }
            if (options && options.dataMask) {
                paramFilter = "&FILTER=" + encodeURI("(<Filter>" + options.dataMask + "</Filter>)");
                info.requestForExport = info.requestForExport + paramFilter;
            }
            if (options && options.fluxPixel) {
                var fluxPixel = options.fluxPixel.replace(/#/g, '&');
                if (info.version && (info.version.equalsIgnoreCase("1.3") || info.version.equalsIgnoreCase("1.3.0"))) {
                     fluxPixel = fluxPixel.replace(/X/g, 'I').replace(/Y/g, 'J');
                }
                info.requestForExport = info.requestForExport + fluxPixel;
            }
            info.requestForExport = Utils.makeSameOrigin(info.requestForExport, Descartes.PROXY_SERVER);
            infosJson.push(info);
        }

        return infosJson;
    },
    /**
     * Staticmethode: writeExportPostBody
     * Construit le flux XML correspondant au paramètre "paramsExport" des requêtes HTTP à envoyer aux services d'exportation.
     *
     * Paramètres:
     * mapContentLayers - {Array(<Descartes.Layer>)} Couches à interroger.
     * olMap - {ol.Map} Carte OpenLayers contenant les ressources OWS.
     * infos - {Object} Objet JSON contenant les propriétés optionnelles pour l'exportation
     *
     * Retour:
     * {String} Le flux XML pour le paramètres "paramsExport"
     */
    writeExportPostBody: function (mapContentLayers, olMap, infos) {
        var layersParams = '';
        var legendsParams = '';
        for (var iLayer = 0, lenLayers = mapContentLayers.length; iLayer < lenLayers; iLayer++) {
            var layer = mapContentLayers[iLayer];
            for (var iResource = 0, lenResources = layer.resourceLayers.length; iResource < lenResources; iResource++) {
                var imageServerParams = layer.resourceLayers[iResource].getImageServerParams();
                if (imageServerParams !== null) {
                    layersParams += '<Couche>' +
                            '<Url>' + this.getWmsRequestForImage(imageServerParams, olMap, layer.format) + '</Url>' +
                            '<Opacite>' + layer.opacity.toString() + '</Opacite></Couche>';
                }
            }
            if (layer.legend !== null) {
                for (var iLegend = 0, lenLegends = layer.legend.length; iLegend < lenLegends; iLegend++) {
                    legendsParams += '<UrlLegende>' + layer.legend[iLegend] + '</UrlLegende>';
                }
            }
        }

        var selectionLayers = ResultLayer.selectionLayersToExport;
        for (var iSelectionLayer = 0, lenSelectionLayers = selectionLayers.length; iSelectionLayer < lenSelectionLayers; iSelectionLayer++) {
            layersParams += '<Couche><Url>' + selectionLayers[iSelectionLayer] + '</Url><Opacite>100</Opacite></Couche>';
        }

        var infosParams = '';
        if (!_.isEmpty(infos.logoURLs)) {
            for (var iURL = 0, lenURLs = infos.logoURLs.length; iURL < lenURLs; iURL++) {
                infosParams += '<UrlLogo>' + infos.logoURLs[iURL] + '</UrlLogo>\n';
            }
        }
        if (!_.isEmpty(infos.logoFiles)) {
            for (var iFile = 0, lenFiles = infos.logoFiles.length; iFile < lenFiles; iFile++) {
                infosParams += '<FichierLogo>' + infos.logoFiles[iFile] + '</FichierLogo>\n';
            }
        }

        if (!_.isEmpty(infos.auteur)) {
            infosParams += '<Auteur>' + infos.auteur.replace(/\x27/gi, '&#39') + '</Auteur>\n';
        }
        if (!_.isEmpty(infos.copyright)) {
            infosParams += '<Copyright>' + infos.copyright.replace(/\x27/gi, '&#39') + '</Copyright>\n';
        }
        if (!_.isEmpty(infos.description)) {
            infosParams += '<Description>' + infos.description.replace(/<br\x2F>/gi, '\n').replace(/\x27/gi, '&#39') + '</Description>\n';
        }
        if (!_.isEmpty(infos.production)) {
            infosParams += '<ApplicationInfos>' + infos.production.replace(/<br\x2F>/gi, '\n').replace(/\x27/gi, '&#39') + '</ApplicationInfos>\n';
        }
        if (!_.isEmpty(infos.createur)) {
            infosParams += '<Creator>' + infos.createur.replace(/<br\x2F>/gi, '\n').replace(/\x27/gi, '&#39') + '</Creator>\n';
        }

        var exportParams = null;
        if (!_.isEmpty(layersParams)) {
            exportParams = '<Export>' +
                    '<Titre>' + infos.title + '</Titre>' +
                    '<Largeur>' + parseInt(olMap.getSize()[0].toString().split('.'), 10) + '</Largeur>' +
                    '<Hauteur>' + parseInt(olMap.getSize()[1].toString().split('.'), 10) + '</Hauteur>' +
                    layersParams + legendsParams + infosParams + '</Export>';
        }
        return exportParams;
    },

    /**
     * Staticmethode: openExportPopup
     * Ouvre une fenêtre pop-up déclenchant l'appel au service d'exportation
     *
     * Paramètres:
     * exportService - {DMapConstants.EXPORT_PNG_SERVER|DMapConstants.EXPORT_PDG_SERVER} Service d'exportation
     * exportParams - {String} Flux XML pour le paramètres "paramsExport"
     * printerSetupParams - {String} Le flux XML pour le paramètres "printerSetupParams"
     */
    openExportPopup: function (exportService, exportParams, printerSetupParams) {
        if (DMapConstants.EXPORT_WINDOW && !DMapConstants.EXPORT_WINDOW.closed) {
            DMapConstants.EXPORT_WINDOW.close();
        }
        DMapConstants.EXPORT_WINDOW = window.open('', 'exportWindow', 'toolbar=no, menubar=no,scrollbars=no,width=500,height=100,resizable=no');
        var htmlContent = '<html><head><meta http-equiv="Content-Type" Content="text/html; charset=UTF-8" />';
        htmlContent += '</head>';
        htmlContent += '<body>';
        htmlContent += '<form name=\'appel\' action=\'' + exportService + '\' method=\'POST\'>';
        htmlContent += '<input type=\'hidden\' name=\'paramsExport\' value=\'' + exportParams + '\'>';
        if (!_.isNil(printerSetupParams)) {
            htmlContent += '<input type=\'hidden\' name=\'printerSetupParams\' value=\'' + printerSetupParams + '\'>';
        }
        htmlContent += '</form>';
        htmlContent += '<script>window.focus();document.forms["appel"].submit()</script>';
        htmlContent += '</body>';
        htmlContent += '</html>';
        DMapConstants.EXPORT_WINDOW.document.write(htmlContent);
        DMapConstants.EXPORT_WINDOW.document.close();
    },

    /**
     * private
     */
    adaptationUrl: function (url) {
        if (!_.isEmpty(url)) {
            if (url.indexOf('?') === -1) {
                url += '?';
            } else if (url.indexOf('?') !== url.length - 1 && ((url.indexOf('&') === -1) || (url.indexOf('&') !== url.length - 1))) {
                url += '&';
            }
        }
        return url;
    }
};

module.exports = Object;


