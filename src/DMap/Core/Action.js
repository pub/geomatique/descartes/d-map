var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var I18N = require('./I18N');

/**
 * Class: Descartes.Action
 * Classe "abstraite" définissant un contrôleur, dans le patron de conception MVC.
 *
 * Hérite de:
 *  - <Descartes.I18N>
 *
 * Classes dérivées:
 *  - <Descartes.Action.AjaxSelection>
 *  - <Descartes.Action.BookmarksManager>
 *  - <Descartes.Action.CoordinatesInput>
 *  - <Descartes.Action.Gazetteer>
 *  - <Descartes.Action.PrinterParamsManager>
 *  - <Descartes.Action.RequestManager>
 *  - <Descartes.Action.ScaleChooser>
 *  - <Descartes.Action.ScaleSelector>
 *  - <Descartes.Action.SizeSelector>
 */
var Class = Utils.Class(I18N, {
    /**
     * Propriete: Id
     * {String} identifiant unique de l'action généré à l'exécution.
     */
    id: null,
    /**
     * Propriete: olMap
     * {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     */
    olMap: null,

    /**
     * Propriete: model
     * {Object} Données (le modèle dans le patron de conception MVC) associé au contrôleur.
     */
    model: null,

    /**
     * Propriete: renderer
     * {<Descartes.UI>} Elément d'interface (la vue dans le patron de conception MVC) associé au contrôleur.
     */
    renderer: null,

    /**
     * Constructeur: Descartes.Action
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * olMap - {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     *
     * Options de construction propres à la classe:
     * view - {<Descartes.UI>} Classe pour la construction de l'élément d'interface.
     */
    initialize: function (div, olMap, options) {
        I18N.prototype.initialize.call(this, [arguments]);
        this.model = {};
        this.id = Utils.createUniqueID();
        this.olMap = olMap;

        var RendererClass = null;

        if (!_.isNil(options) && !_.isNil(options.view)) {
            RendererClass = options.view;
            delete options.view;
        }

        if (!_.isNil(RendererClass)) {
            this.renderer = new RendererClass(div, this.model, options);
        }
    },

    CLASS_NAME: 'Descartes.Action'
});

module.exports = Class;
