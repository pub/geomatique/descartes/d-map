var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var EventManager = require('../Utils/Events/EventManager');
var I18N = require('../Core/I18N');

/**
 * Class: Descartes.UI
 * Classe "abstraite" définissant une vue, dans le patron de conception MVC.
 *
 * Hérite de:
 *  - <Descartes.I18N>
 *
 * Classes dérivées:
 *  - <Descartes.UI.AbstractCoordinatesInput>
 *  - <Descartes.UI.AbstractPrinterSetup>
 *  - <Descartes.UI.AbstractInPlace>
 *  - <Descartes.UI.BookmarksInPlace>
 *  - <Descartes.UI.ConfirmDialog>
 *  - <Descartes.UI.DataGrid>
 *  - <Descartes.UI.FormDialog>
 *  - <Descartes.UI.GazetteerInPlace>
 *  - <Descartes.UI.LayersTree>
 *  - <Descartes.UI.ModalFormDialog>
 *  - <Descartes.UI.RequestManagerInPlace>
 *  - <Descartes.UI.SaveBookmarkDialog>
 *  - <Descartes.UI.ScaleChooserInPlace>
 *  - <Descartes.UI.ScaleSelectorInPlace>
 *  - <Descartes.UI.SizeSelectorInPlace>
 *  - <Descartes.UI.TabbedDataGrids>
 *  - <Descartes.UI.ToolTipContainer>
 *  - <Descartes.UI.WaitingDialog>
 */
var UI = Utils.Class(I18N, {
    /**
     * Propriete: Id
     * {String} identifiant unique de l'action généré à l'exécution.
     */
    id: null,
    /**
     * Propriete: div
     * {DOMElement} Elément DOM de la page accueillant la vue.
     */
    div: null,

    /**
     * Propriete: model
     * {Object} Données (le modèle dans le patron de conception MVC) manipulées par la vue et retournées au contrôleur.
     */
    model: null,

    /**
     * Propriete: events
     * {EventManager} Gestionnaire d'événements
     */
    events: null,

    defaultDisplayClasses: {},

    displayClasses: {},

    /**
     * Constructeur: Descartes.UI
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * model - {Object} Données manipulées par la vue et retournées au contrôleur.
     * eventTypes - {Array(String)} Types d'événements potentiellement déclenchés par la vue.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     */
    initialize: function (div, model, eventTypes, options) {
        I18N.prototype.initialize.call(this);
        this.div = Utils.getDiv(div);
        this.id = Utils.createUniqueID();
        this.model = model;
        if (eventTypes !== undefined) {
            this.events = new EventManager();
        }
        this.displayClasses = _.extend({}, this.defaultDisplayClasses);

        if (!_.isNil(options)) {
            _.extend(this.displayClasses, options.displayClasses);
            delete options.displayClasses;
        }
        _.extend(this, options);
    },
    CLASS_NAME: 'Descartes.UI'
});

module.exports = UI;
