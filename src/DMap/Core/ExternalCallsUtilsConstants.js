
module.exports = {
    /**
     * Constante: WMS_SERVICE
     * Service de type WMS
     */
    WMS_SERVICE: 0,
    /**
     * Constante: WFS_SERVICE
     * Service de type WFS
     */
    WFS_SERVICE: 1,
    /**
     * Constante: WMS_GETMAP_OPERATION
     * Opération WMS/GetMap
     */
    WMS_GETMAP_OPERATION: 'GetMap',
    /**
     * Constante: WMS_GETFEATUREINFO_OPERATION
     * Opération WMS/GetFeatureInfo
     */
    WMS_GETFEATUREINFO_OPERATION: 'GetFeatureInfo'
};
