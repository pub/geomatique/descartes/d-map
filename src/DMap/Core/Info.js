require('../Info/css/Info.css');

var ol = require('openlayers');
var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var I18N = require('../Core/I18N');

/**
 * Class: Descartes.Info
 * Classe "abstraite" définissant une zone informative automatiquement actualisée en fonction du contexte.
 *
 * Hérite de:
 *  - <Descartes.I18N>
 *  - ol.control.Control
 *
 * Classes dérivées:
 *  - <Descartes.Info.Attribution>
 *  - <Descartes.Info.GraphicScale>
 *  - <Descartes.Info.Legend>
 *  - <Descartes.Info.MapDimensions>
 *  - <Descartes.Info.MetricScale>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'moveend' de la classe ol.Map déclenche la méthode <update>.
 */
var Class = Utils.Class(I18N, ol.control.Control, {

    defaultDisplayClasses: {},

    displayClasses: {},
    /**
     * Constructeur: Descartes.Info
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la zone informative ou identifiant de cet élement.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     */
    initialize: function (div, options) {
        I18N.prototype.initialize.call(this);

        this.displayClasses = _.extend({}, this.defaultDisplayClasses);

        if (!_.isNil(options)) {
            _.extend(this.displayClasses, options.displayClasses);
            delete options.displayClasses;
        }
        _.extend(this, options);

        this.displayClass = this.displayClasses.globalClassName;

        this.div = Utils.getDiv(div);

        var element = document.createElement('div');
        element.className = this.displayClass;
        if (_.isNil(this.div)) {
            element.className += ' ol-unselectable ol-control';
        }

        ol.control.Control.call(this, {
            element: element,
            target: this.div
        });
    },
    /**
     * Methode: draw
     * Construit la zone informative dans la page HTML
     */
    draw: function () {
        if (!this.element) {
            this.element = document.createElement("div");
            this.element.className = this.displayClass;
            this.div.appendChild(this.element);
        }
        this.map.events.register('moveend', this, this.update);
        this.update();
        return this.div;
    },
    setMap: function (map) {
        ol.control.Control.prototype.setMap.apply(this, arguments);
        if (map) {
           map.on('moveend', this.update.bind(this), this);
        }
    },
    /**
     * Methode: update
     * Actualise la zone informative en fonction du contexte
     *
     * Doit être surchargée dans chaque classe dérivée.
     */
    update: function () {
    },
    CLASS_NAME: 'Descartes.Info'
});

module.exports = Class;
