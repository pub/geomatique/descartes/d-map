﻿/* global MODE, Descartes */

var Utils = require('../Utils/DescartesUtils');
var _ = require('lodash');
var log = require('loglevel');
var ol = require('openlayers');

var MapConstants = require('../Map/MapConstants');

var ToolBar = require('../Toolbar/' + MODE + '/ToolBar');
var ToolBarConstants = require('../Toolbar/ToolBarConstants');
var CenterToCoordinates = require('../Button/CenterToCoordinates');
var DirectionalPanPanel = require('../Button/DirectionalPanPanel');
var ToolBarOpener = require('../Button/ToolBarOpener');
var ExportPNG = require('../Button/ExportPNG');
var ExportPDF = require('../Button/ExportPDF');
var ShareLinkMap = require('../Button/' + MODE + '/ShareLinkMap');
var DisplayLayersTreeSimple = require('../Button/DisplayLayersTreeSimple');
var ExportVectorLayer = require('../Button/ContentTask/' + MODE + '/ExportVectorLayer');

var MapContentManagerConstants = require('../Action/MapContentManagerConstants');
var BookmarksManager = require('../Action/BookmarksManager');
var CoordinatesInput = require('../Action/CoordinatesInput');
var RequestManager = require('../Action/RequestManager');
var ScaleSelector = require('../Action/ScaleSelector');
var ScaleChooser = require('../Action/ScaleChooser');
var SizeSelector = require('../Action/SizeSelector');
var PrinterParamsManager = require('../Action/PrinterParamsManager');
var Gazetteer = require('../Action/Gazetteer');
var DefaultGazetteer = require('../Action/DefaultGazetteer');
var MapContentManager = require('../Action/MapContentManager');

var GraphicScale = require('../Info/GraphicScale');
var MetricScale = require('../Info/MetricScale');
var MapDimensions = require('../Info/MapDimensions');
var LocalizedMousePosition = require('../Info/' + MODE + '/LocalizedMousePosition');
var Legend = require('../Info/' + MODE + '/Legend');
var Attribution = require('../Info/Attribution');
var ToolTip = require('../Info/ToolTip');
var SelectToolTip = require('../Info/SelectToolTip');

var LayerConstants = require('../Model/LayerConstants');

var CenterMap = require('../Tool/' + MODE + '/CenterMap');
var MiniMap = require('../Tool/MiniMap');
var ZoomToInitialExtent = require('../Tool/ZoomToInitialExtent');
var ZoomToMaximalExtent = require('../Tool/ZoomToMaximalExtent');
var ZoomIn = require('../Tool/ZoomIn');
var ZoomOut = require('../Tool/ZoomOut');
var DragPan = require('../Tool/DragPan');
var GeolocationSimple = require('../Tool/Geolocation/GeolocationSimple');
var GeolocationTracking = require('../Tool/Geolocation/GeolocationTracking');
var NavigationHistory = require('../Tool/' + MODE + '/NavigationHistory');
var MeasureDistance = require('../Tool/Measure/MeasureDistance');
var MeasureArea = require('../Tool/Measure/MeasureArea');
var PointSelection = require('../Tool/Selection/PointSelection');
var PointRadiusSelection = require('../Tool/Selection/PointRadiusSelection');
var CircleSelection = require('../Tool/Selection/CircleSelection');
var PolygonSelection = require('../Tool/Selection/PolygonSelection');
var PolygonBufferHaloSelection = require('../Tool/Selection/PolygonBufferHaloSelection');
var LineBufferHaloSelection = require('../Tool/Selection/LineBufferHaloSelection');
var RectangleSelection = require('../Tool/Selection/RectangleSelection');

var ResultLayer = require('../Model/ResultLayer');

/**
 * Class: Descartes.Map
 * Classe abstraite *principale* de la librairie Descartes permettant d'associer une carte OpenLayers à un ensemble d'outils, de gestionnaires, d'informations.
 *
 * Classes dérivées:
 * - <Descartes.Map.ContinuousScalesMap>
 * - <Descartes.Map.DiscreteScalesMap>
 *
 * Description générale:
 * Pour la librairie Descartes, une carte est constituée :
 * - d'un *contenu*, défini par la classe <Descartes.MapContent>.
 * - d'une *carte OpenLayers*, alimentée par des couches OpenLayers fournies par le contenu.
 * - de *contrôles* permettant de piloter la carte selon les principes propres à OpenLayers.
 * - d'*actions* permettant de piloter la carte suite à des saisies effectuées hors de la carte. Elles sont définies par des classes dérivées de la classe <Descartes.Action>.
 * - de *zones informatives* automatiquement actualisées selon le contexte courant de la carte. Elles sont définies par des classes dérivées de la classe <Descartes.Info>.
 * - d'*outils* permettant de piloter la carte suite à des interactions sur celle-ci. Ils sont définis par des classes dérivées des classe <Descartes.Button> et <Descartes.Tool>.
 *
 * :
 * Le fonctionnement de la plupart des contrôles, actions, zones informatives et outils est relativement simple, tant pour l'utilisateur final que pour le développeur.
 *
 * :
 * Il est en conséquence aisé de les associer à la carte, grâce à des méthodes génériques :
 * - <addOpenLayersInteraction> et <addOpenLayersInteraction> pour les intéractions
 * - <addAction> et <addActions> pour les actions
 * - <addInfo> et <addInfos> pour les zones informatives
 * - <addToolInToolBar> et <addToolsInToolBar> pour les outils
 *
 * :
 * Des listes prédéfinies d'éléments disponibles pour ces méthodes facilitent leur utilisation.
 *
 * :
 * D'autres éléments, dont la manipulation est plus complexe, bénéficient de méthodes dédiées pour les associer à la carte :
 *
 * :
 * Barre d'outils - Instance de la classe <Descartes.ToolBar> ajoutée grâce à la méthode <addToolBar>
 * Gestionnaire de contenu - Instance de la classe <Descartes.Action.MapContentManager> ajouté grâce à la méthode <addContentManager>
 * Mini-carte de navigation - Instance de la classe <Descartes.Tool.MiniMap> ajoutée grâce à la méthode <addMiniMap>
 * "Rose des vents" de navigation - Instance de la classe <Descartes.Button.DirectionalPanPanel> ajoutée grâce à la méthode <addDirectionalPanPanel>
 * Gestionnaire d'info-bulles - Instance de la classe <Descartes.Info.ToolTip> ajouté grâce à la méthode <addToolTip>
 * Gestionnaire de requêtes attributaires - Instance de la classe <Descartes.ToolBar> ajouté grâce à la méthode <addRequestManager>
 * Gestionnaire de localisation rapide - Instance de la classe <Descartes.Action.Gazetteer> ajouté grâce à la méthode <addGazetteer>
 * Gestionnaire de localisation rapide administratif - Instance de la classe <Descartes.Action.DefaultGazetteer> ajouté grâce à la méthode <addDefaultGazetteer>
 * Gestionnaire de vues personnalisées - Instance de la classe <Descartes.Action.BookmarksManager> ajouté grâce à la méthode <addBookmarksManager>
 *
 * Contenu de la carte:
 * Il s'agit de couches éventuellement hiérarchisées en groupes, sous-groupes, sous-sous-groupes, etc.
 * - une couche est définie par la classe <Descartes.Layer>
 * - un groupe est défini par la classe <Descartes.Group>
 *
 * Intéraction OpenLayers disponibles (voir <OL_INTERACTIONS_NAME>) par défaut:
 * - ol.interaction.DragRotate pour le type <Descartes.Map.OL_DRAG_ROTATE>
 * - ol.interaction.DoubleClickZoom pour le type <Descartes.Map.OL_DOUBLE_CLICK_ZOOM>
 * - ol.interaction.DragPan pour le type <Descartes.Map.OL_DRAG_PAN>
 * - ol.interaction.PinchRotate pour le type <Descartes.Map.OL_PINCH_ROTATE>
 * - ol.interaction.PinchZoom pour le type <Descartes.Map.OL_PINCH_ZOOM>
 * - ol.interaction.KeyboardPan pour le type <Descartes.Map.OL_KEYBOARD_PAN>
 * - ol.interaction.KeyboardZoom pour le type <Descartes.Map.OL_KEYBOARD_ZOOM>
 * - ol.interaction.MouseWheelZoom pour le type <Descartes.Map.OL_MOUSE_WHEEL_ZOOM>
 * - ol.interaction.DragZoom pour le type <Descartes.Map.OL_DRAG_ZOOM>
 *
 * Actions disponibles (voir <ACTIONS_NAME>) par défaut:
 * - <Descartes.Action.CoordinatesInput> pour le type <Descartes.Map.COORDS_INPUT_ACTION>
 * - <Descartes.Action.ScaleChooser> pour le type <Descartes.Map.SCALE_CHOOSER_ACTION>
 * - <Descartes.Action.ScaleSelector> pour le type <Descartes.Map.SCALE_SELECTOR_ACTION>
 * - <Descartes.Action.SizeSelector> pour le type <Descartes.Map.SCALE_SELECTOR_ACTION>
 * - <Descartes.Action.PrinterParamsManager> pour le type <Descartes.Map.PRINTER_SETUP_ACTION>
 *
 * Zones informatives disponibles (voir <INFOS_NAME>) par défaut:
 * - <Descartes.Info.GraphicScale> pour le type <Descartes.Map.GRAPHIC_SCALE_INFO>
 * - <Descartes.Info.MetricScale> pour le type <Descartes.Map.METRIC_SCALE_INFO>
 * - <Descartes.Info.MapDimensions> pour le type <Descartes.Map.MAP_DIMENSIONS_INFO>
 * - <Descartes.Info.LocalizedMousePosition> pour le type <Descartes.Map.MOUSE_POSITION_INFO>
 * - <Descartes.Info.Legend> pour le type <Descartes.Map.LEGEND_INFO>
 * - <Descartes.Info.Attribution> pour le type <Descartes.Map.ATTRIBUTION_INFO>
 *
 * Outils disponibles (voir <TOOLS_NAME>) par défaut:
 * - <Descartes.Button.ZoomToInitialExtent> pour le type <Descartes.Map.MAXIMAL_EXTENT>
 * - <Descartes.Button.ZoomToMaximalExtent> pour le type <Descartes.Map.INITIAL_EXTENT>
 * - <Descartes.Tool.DragPan> pour le type <Descartes.Map.DRAG_PAN>
 * - <Descartes.Tool.GeolocationSimple> pour le type <Descartes.Map.GEOLOCATION_SIMPLE>
 * - <Descartes.Tool.GeolocationTracking> pour le type <Descartes.Map.GEOLOCATION_TRACKING>
 * - <Descartes.Tool.ZoomIn> pour le type <Descartes.Map.ZOOM_IN>
 * - <Descartes.Tool.ZoomOut> pour le type <Descartes.Map.ZOOM_OUT>
 * - <Descartes.Tool.NavigationHistory> pour le type <Descartes.Map.NAV_HISTORY>
 * - <Descartes.Tool.CenterMap> pour le type <Descartes.Map.CENTER_MAP>
 * - <Descartes.Button.CenterToCoordinates> pour le type <Descartes.Map.COORDS_CENTER>
 * - <Descartes.Tool.Measure.MeasureDistance> pour le type <Descartes.Map.DISTANCE_MEASURE>
 * - <Descartes.Tool.Measure.MeasureArea> pour le type <Descartes.Map.AREA_MEASURE>
 * - <Descartes.Button.ExportPDF> pour le type <Descartes.Map.PDF_EXPORT>
 * - <Descartes.Button.ExportPNG> pour le type <Descartes.Map.PNG_EXPORT>
 * - <Descartes.Button.ShareLinkMap> pour le type <Descartes.Map.SHARE_LINK_MAP>
 * - <Descartes.Button.DisplayLayersTreeSimple> pour le type <Descartes.Map.DISPLAY_LAYERSTREE_SIMPLE>
 * - <Descartes.Tool.Selection.PointSelection> pour le type <Descartes.Map.POINT_SELECTION>
 * - <Descartes.Tool.Selection.PointRadiusSelection> pour le type <Descartes.Map.POINT_RADIUS_SELECTION>
 * - <Descartes.Tool.Selection.CircleSelection> pour le type <Descartes.Map.CIRCLE_SELECTION>
 * - <Descartes.Tool.Selection.PolygonSelection> pour le type <Descartes.Map.POLYGON_SELECTION>
 * - <Descartes.Tool.Selection.PolygonBufferHaloSelection> pour le type <Descartes.Map.POLYGON_BUFFER_HALO_SELECTION>
 * - <Descartes.Tool.Selection.LineBufferHaloSelection> pour le type <Descartes.Map.LINE_BUFFER_HALO_SELECTION>
 * - <Descartes.Tool.Selection.RectangleSelection> pour le type <Descartes.Map.RECTANGLE_SELECTION>
 * - <Descartes.Button.ToolBarOpener> pour le type <Descartes.Map.TOOLBAR_OPENER>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'moveend' de la classe ol.Map déclenche la méthode <refreshContentManager>.
 *  - l'événement 'changed' de la classe <Descartes.MapContent> déclenche la méthode <refresh>.
 *  - l'événement 'layerRemoved' de la classe <Descartes.MapContent> déclenche la méthode <refreshRequestManager>, si un gestionnaire de requêtes est associé à la carte.
 *  - l'événement 'layerRemoved' de la classe <Descartes.MapContent> déclenche la méthode <refreshToolTip>, si un gestionnaire d'info-bulle est associé à la carte.
 */
var Map = Utils.Class({
    /**
     * Propriete: OL_map
     * {ol.Map} Carte OpenLayers associée.
     */
    OL_map: null,
    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,
    /**
     * Propriete: mapContentManager
     * {<Descartes.Action.MapContentManager>} Gestionnaire de contenu de la carte (groupes et couches).
     */
    mapContentManager: null,
    /**
     * Propriete: mainToolBar
     * {<Descartes.ToolBar>} Barre d'outils principale.
     */
    mainToolBar: null,
    /**
     * Propriete: toolBars
     * {Array} Liste des barres d'outils.
     */
    toolBars: [],
    /**
     * Propriete: contentManagerToolBar
     * {<Descartes.ToolBar>} Barre d'outils spécifique pour les outils du gestionnaire de contenu.
     */
    contentManagerToolBar: null,
    /**
     * Propriete: initExtent
     * {Array} Emprise initiale de visualisation de la carte.
     */
    initExtent: null,
    /**
     * Propriete: maxExtent
     * {Array} Emprise maximale de visualisation de la carte.
     */
    maxExtent: null,
    /**
     * Propriete: projection
     * {String} Code de la projection de la carte.
     */
    projection: 'EPSG:2154',
    /**
     * Propriete: units
     * {String} Unité de mesure associée au systême de projection de la carte.
     */
    units: 'm',
    /**
     * Propriete: size
     * {Size} Taille de la carte.
     */
    size: ol.size.toSize([750, 500]),
    /**
     * Propriete: autoSize
     * {Boolean} Taille de la carte automatique par rapport au conteneur parent.
     */
    autoSize: false,
    /**
     * Propriete: autoScreenSize
     * {Boolean} Taille de la carte prenant tout l'écran.
     */
    autoScreenSize: false,
    /**
     * Private
     */
    sizeSelector: null,

    rotation: 0,

    displayExtendedOLExtent: false,

    /**
     * Private
     */
    _toolsWithMapcontentManager: [],

    /**
     * Constructeur: Descartes.Map
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la carte OpenLayers ou identifiant de cet élément.
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction de la classe:
     * initExtent - {Array} Emprise initiale de visualisation de la carte.
     * maxExtent - {Array} Emprise maximale de visualisation de la carte.
     * projection - {String} Code de la projection de la carte.
     * size - {Size} Taille initiale de la carte.
     * autoSize - {Boolean} Taille de la carte automatique par rapport au conteneur parent.
     * autoScreenSize - {Boolean} Taille de la carte prenant tout l'écran.
     */
    initialize: function (div, mapContent, options) {
        this.defaultInfos = {
            GraphicScale: GraphicScale,
            MetricScale: MetricScale,
            MapDimensions: MapDimensions,
            LocalizedMousePosition: LocalizedMousePosition,
            Legend: Legend,
            Attribution: Attribution
        };
        this.defaultActions = {
            CoordinatesInput: CoordinatesInput,
            ScaleChooser: ScaleChooser,
            ScaleSelector: ScaleSelector,
            SizeSelector: SizeSelector,
            PrinterParamsManager: PrinterParamsManager
        };
        this.defaultTools = {
            ZoomToInitialExtent: ZoomToInitialExtent,
            ZoomToMaximalExtent: ZoomToMaximalExtent,
            DragPan: DragPan,
            GeolocationSimple: GeolocationSimple,
            GeolocationTracking: GeolocationTracking,
            ZoomIn: ZoomIn,
            ZoomOut: ZoomOut,
            NavigationHistory: NavigationHistory,
            CenterMap: CenterMap,
            CenterToCoordinates: CenterToCoordinates,
            MeasureDistance: MeasureDistance,
            MeasureArea: MeasureArea,
            ExportPDF: ExportPDF,
            ExportPNG: ExportPNG,
            ShareLinkMap: ShareLinkMap,
            DisplayLayersTreeSimple: DisplayLayersTreeSimple,
            ExportVectorLayer: ExportVectorLayer,
            PointSelection: PointSelection,
            PointRadiusSelection: PointRadiusSelection,
            CircleSelection: CircleSelection,
            PolygonSelection: PolygonSelection,
            PolygonBufferHaloSelection: PolygonBufferHaloSelection,
            LineBufferHaloSelection: LineBufferHaloSelection,
            RectangleSelection: RectangleSelection,
            ToolBarOpener: ToolBarOpener
        };
        this.defaultOlInteractions = {
            DragRotate: ol.interaction.DragRotate,
            DoubleClickZoom: ol.interaction.DoubleClickZoom,
            DragPan: ol.interaction.DragPan,
            PinchRotate: ol.interaction.PinchRotate,
            PinchZoom: ol.interaction.PinchZoom,
            KeyboardPan: ol.interaction.KeyboardPan,
            KeyboardZoom: ol.interaction.KeyboardZoom,
            MouseWheelZoom: ol.interaction.MouseWheelZoom,
            DragZoom: ol.interaction.DragZoom,
            DragBox: ol.interaction.DragBox,
            DragRotateAndZoom: ol.interaction.DragRotateAndZoom
        };
        this.defaultOlControls = {
            Attribution: ol.control.Attribution,
            FullScreen: ol.control.FullScreen,
            MousePosition: ol.control.MousePosition,
            OverviewMap: ol.control.OverviewMap,
            Rotate: ol.control.Rotate,
            ScaleLine: ol.control.ScaleLine,
            Zoom: ol.control.Zoom,
            ZoomSlider: ol.control.ZoomSlider,
            ZoomToExtent: ol.control.ZoomToExtent
        };

        this.mapContent = mapContent;

        if (!_.isNil(options)) {
            _.extend(this, options);
        }

        /*if (this.CLASS_NAME === 'Descartes.Map.ContinuousScalesMap') {
         this.maxExtent = Utils.extendBounds(this.maxExtent, this.size);
         }*/
        this.initExtent = (this.initExtent !== null) ? this.initExtent : this.maxExtent;
        this.createOlMap();
        this.OL_map.setTarget(div);
        this.mapContent.events.register('changed', this, this.refresh);
        this.OL_map.on('movestart', function (e) {
            var mapZoom = this.OL_map.getView().getZoomForResolution(e.frameState.viewState.resolution);
            this.OL_map.once('moveend', function (evt) {
                var currentZoom = this.OL_map.getView().getZoom();
                if (currentZoom !== mapZoom) {
                    this.refreshContentManager();
                }
            }.bind(this), this);
        }.bind(this), this);
    },
    /**
     * Methode: createOlMap
     * Construit la carte OpenLayers associée.
     *
     * :
     * Doit être surchargée par les classes dérivées.
     */
    createOlMap: function () {},
    /**
     * Methode: getCommonOlMapOptions
     * Fournit les paramêtres communs à tous les types de carte OpenLayers.
     *
     * Retour:
     * {Object} Objet JSON contenant les paramêtres communs.
     */
    getCommonOlMapOptions: function () {
        var olMapOptions = {
            units: this.units,
            projection: this.projection,
            maxExtent: this.maxExtent,
            restrictedExtent: this.maxExtent,
            controls: []
        };
        return olMapOptions;
    },
    /**
     * Methode: addToolBar
     * [Dépréciée] Ajoute une barre d'outils principale à la carte.
     *
     * :
     * Cette méthode permet de ne créer qu'une seule barre d'outils.
     * Depuis la version 3.2, il est recommandé d'utiliser la méthode "addNamedToolBar".
     *
     * Paramêtres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la barre d'outils ou identifiant de cet élément.
     * tools - {Array(Object)} Objets JSON décrivant les outils de la barre (peut être null). Voir <addToolInToolBar> pour la forme de ces objets.
     *
     * Retour:
     * {<Descartes.ToolBar>} La barre d'outils créée.
     */
    addToolBar: function (div, tools, options) {
        if (this.mainToolBar === null) {
            this.mainToolBar = this._addToolBar(div, tools, options);
        } else {
            throw new Error('La barre d\'outils principale existe déjà.');
        }
        return this.mainToolBar;
    },
    /**
     * Methode: addNamedToolBar
     * Ajoute une barre d'outils principale à la carte.
     *
     * Avec cette méthode, il est possible d'ajouter plusieurs barres d'outils à la carte.
     *
     * Paramêtres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la barre d'outils ou identifiant de cet élément.
     * tools - {Array(Object)} Objets JSON décrivant les outils de la barre (peut être null). Voir <addNamedToolInToolBar> pour la forme de ces objets.
     * options - {Object} est un objet JSON embarquant diverses options offertes par la classe générant la barre d'outils.
     *
     * Retour:
     * {<Descartes.ToolBar>} La barre d'outils créée.
     */
    addNamedToolBar: function (div, tools, options) {
        var toolBar = this._addToolBar(div, tools, options);
        this.toolBars.push(toolBar);
        return toolBar;
    },
    _addToolBar: function (div, tools, options) {
        var _toolBar = new ToolBar(div, this.OL_map, options);
        this._addToolsInToolBar(_toolBar, tools);
        return _toolBar;
    },

    /**
     * Methode: addToolInToolBar
     * [Dépréciée] Ajoute un outil (<Descartes.Button> ou <Descartes.Tool>) à la barre d'outils principale.
     *
     * :
     * Depuis la version 3.2, il est recommandé d'utiliser la méthode "addToolInNamedToolBar".
     *
     * Paramêtres:
     * tool - {Object} Objet JSON décrivant l'outil.
     *
     * :
     * L'objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de l'outil>,  // parmi les valeurs de Descartes.Map.TOOLS_NAME
     * 					  // ou classe personnalisée remplaçant la classe par défaut associée au type de l'outil
     * 		args: <arguments>	  // arguments du constructeur de l'outil
     * }
     * (end)
     *
     *  Retour:
     * {<Descartes.Tool>} L'outil ajouté.
     */
    addToolInToolBar: function (tool) {
        return this._addToolInToolBar(this.mainToolBar, tool);
    },
    /**
     * Methode: addToolInNamedToolBar
     * Ajoute un outil (<Descartes.Button> ou <Descartes.Tool>) à la barre d'outils principale.
     *
     * Paramêtres:
     * toolBar - {<Descartes.ToolBar>} la barre d'outil
     * tool - {Object} Objet JSON décrivant l'outil.
     *
     * :
     * L'objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de l'outil>,  // parmi les valeurs de Descartes.Map.TOOLS_NAME
     * 					  // ou classe personnalisée remplaçant la classe par défaut associée au type de l'outil
     * 		args: <arguments>	  // arguments du constructeur de l'outil
     * }
     * (end)
     *
     *  Retour:
     * {<Descartes.Tool>} L'outil ajouté.
     */
    addToolInNamedToolBar: function (toolBar, tool) {
        return this._addToolInToolBar(toolBar, tool);
    },
    _addToolInToolBar: function (toolBar, tool) {
        var tools = [];
        var toolInstance = null;
        var ToolClassName = null;
        if (typeof tool.type === 'string') {
            if (MapConstants.TOOLS_NAME.indexOf(tool.type) !== -1) {
                ToolClassName = this.defaultTools[tool.type];
            }
        } else if (typeof tool.type === 'function') {
            ToolClassName = tool.type;
        } else {
            toolInstance = tool;
            this.OL_map.removeControl(toolInstance);
            if (toolInstance.setTarget) {
                toolInstance.setTarget(toolBar.id);
            }
        }
        if (!_.isNil(ToolClassName)) {
            var args = [];
            var argsTools = null;
            if (tool.args instanceof Object && _.isNil(tool.args.CLASS_NAME)) {
                argsTools = _.extend({}, tool.args);
            } else {
                argsTools = tool.args;
            }
            if (!_.isNil(argsTools)) {
                if (!_.isArray(argsTools)) {
                    args = [argsTools];
                } else {
                    args = argsTools;
                }
            }

            args.push({
                target: document.getElementById(toolBar.id)
            });

            log.debug('Initialize tool %s \nwith args %s', JSON.stringify(tool), JSON.stringify(args));
            if (ToolClassName.prototype.CLASS_NAME === 'Descartes.Tool.NavigationHistory') {
                var navigationHistoryToolPrevious = Object.create(ToolClassName.prototype);
                var navigationHistoryToolNext = Object.create(ToolClassName.prototype);

                var prevArguments = [{}, false];
                var nextArguments = [{}, true];

                if (args.length === 1) {
                    prevArguments[0] = args[0];
                    nextArguments[0] = args[0];
                } else if (args.length === 2) {
                    if (args[0].previousOptions) {
                        prevArguments[0] = args[0].previousOptions;
                    }
                    if (args[0].nextOptions) {
                        nextArguments[0] = args[0].nextOptions;
                    }
                    _.extend(prevArguments[0], args[1]);
                    _.extend(nextArguments[0], args[1]);
                }

                ToolClassName.apply(navigationHistoryToolPrevious, prevArguments);
                ToolClassName.apply(navigationHistoryToolNext, nextArguments);

                tools.push(navigationHistoryToolPrevious);
                tools.push(navigationHistoryToolNext);

                toolInstance = [navigationHistoryToolPrevious, navigationHistoryToolNext];
            } else if (ToolClassName.prototype.CLASS_NAME === 'Descartes.Tool.ZoomToMaximalExtent') {
                toolInstance = Object.create(ToolClassName.prototype);
                args[0].maxExtent = this.maxExtent;
                ToolClassName.apply(toolInstance, args);
            } else if (ToolClassName.prototype.CLASS_NAME === 'Descartes.Tool.ZoomToInitialExtent') {
                toolInstance = Object.create(ToolClassName.prototype);
                var bounds = args[0];
                args[0] = {};
                args[0].bounds = bounds;
                args[0].tooloptions = tool.options;
                ToolClassName.apply(toolInstance, args);
            } else if (ToolClassName.prototype.CLASS_NAME === 'Descartes.Button.DisplayLayersTreeSimple') {
                toolInstance = Object.create(ToolClassName.prototype);
                ToolClassName.apply(toolInstance, args);
                this._toolsWithMapcontentManager.push(toolInstance);
            } else {
                toolInstance = Object.create(ToolClassName.prototype);
                ToolClassName.apply(toolInstance, args);
            }
        }
        if (!_.isNil(toolInstance)) {
            if (MapConstants._TOOLS_WITH_MAPCONTENT.indexOf(toolInstance.CLASS_NAME) !== -1) {
                if (!_.isNil(toolInstance.setMapContent)) {
                    toolInstance.setMapContent(this.mapContent);
                }
                tools.push(toolInstance);
            } else if (toolInstance.CLASS_NAME === 'Descartes.Button.ToolBarOpener') {
                toolInstance._initToolBarOpener(this);
                tools.push(toolInstance);
            } else {
                tools.push(toolInstance);
            }
            toolBar.addControls(tools);
            //toolBar.controls = toolBar.controls.concat(tools);
        }
        return toolInstance;
    },
    /**
     * Methode: addToolsInToolBar
     * [Dépréciée] Ajoute plusieurs outils (<Descartes.Button> ou <Descartes.Tool> à la barre d'outils principale.
     *
     * :
     * Depuis la version 3.2, il est recommandé d'utiliser la méthode "addToolsInNamedToolBar".
     *
     * Paramêtres:
     * tools - {Array(Object)} Objets JSON décrivant les outils.
     *
     * :
     * Chaque objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de l'outil>,  // parmi les valeurs de Descartes.Map.TOOLS_NAME
     * 					  // ou classe personnalisée remplaçant la classe par défaut associée au type de l'outil
     * 		args: <arguments>	  // arguments du constructeur de l'outil
     * }
     * (end)
     */
    addToolsInToolBar: function (tools) {
        this._addToolsInToolBar(this.mainToolBar, tools);
    },
    /**
     * Methode: addToolsInNamedToolBar
     * Ajoute plusieurs outils (<Descartes.Button> ou <Descartes.Tool> à la barre d'outils principale.
     *
     * Paramêtres:
     * toolBar - {<Descartes.ToolBar>} la barre d'outil
     * tools - {Array(Object)} Objets JSON décrivant les outils.
     *
     * :
     * Chaque objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de l'outil>,  // parmi les valeurs de Descartes.Map.TOOLS_NAME
     * 					  // ou classe personnalisée remplaçant la classe par défaut associée au type de l'outil
     * 		args: <arguments>	  // arguments du constructeur de l'outil
     * }
     * (end)
     */
    addToolsInNamedToolBar: function (toolBar, tools) {
        this._addToolsInToolBar(toolBar, tools);
    },
    _addToolsInToolBar: function (toolBar, tools) {
        if (!_.isNil(tools)) {
            if (!_.isArray(tools)) {
                tools = [tools];
            }
            for (var i = 0, len = tools.length; i < len; i++) {
                this._addToolInToolBar(toolBar, tools[i]);
            }
        }
    },
    /**
     * Methode: addOpenLayersInteraction
     * Ajoute une intéraction d'OpenLayers à la carte.
     *
     * Paramètres:
     * interaction - {Object} Objet JSON décrivant l'intéraction.
     *
     * :
     * L'objet JSON doit être de la forme :
     * (start code)
     * {
     *	type: <type de l'outil>,// parmi les valeurs de Descartes.Map.OL_INTERACTIONS_NAME
     * 				// ou classe personnalisée remplaçant la classe par défaut associée au type de l'intéraction
     *	args: <arguments>	// arguments du constructeur de l'intéraction
     * }
     * (end)
     * Retour:
     * {Object} L'intéraction d'OpenLayers ajoutée.
     */
    addOpenLayersInteraction: function (interaction) {
        var interactionInstance = null;
        var InteractionClassName = null;
        if (_.isString(interaction.type)) {
            if (MapConstants.OL_INTERACTIONS_NAME.indexOf(interaction.type) !== -1) {
                InteractionClassName = this.defaultOlInteractions[interaction.type];
            }
        } else if (_.isFunction(interaction.type)) {
            InteractionClassName = interaction.type;
        }
        if (!_.isNil(InteractionClassName)) {
            if (!_.isNil(interaction.args)) {
                interactionInstance = new InteractionClassName(interaction.args);
            } else {
                var options = {};
                if (interaction.type === MapConstants.OL_DRAG_PAN) {
                    options.condition = ol.events.condition.altKeyOnly;
                } else if (interaction.type === MapConstants.OL_DRAG_ROTATE) {
                    options.condition = ol.events.condition.altshiftKeysOnly;
                } else if (interaction.type === MapConstants.OL_DRAG_ZOOM) {
                    options.condition = ol.events.condition.shiftKeyOnly;
                } else if (interaction.type === MapConstants.OL_DRAG_ROTATE_AND_ZOOM) {
                    options.condition = ol.events.condition.altKeyOnly;
                }
                interactionInstance = new InteractionClassName(options);
            }

            this.OL_map.addInteraction(interactionInstance);
            interactionInstance.setActive(true);

            return interactionInstance;
        }
        return null;
    },
    /**
     * Methode: addOpenLayersInteractions
     * Ajoute plusieurs interactions d'OpenLayers à la carte.
     *
     * Paramètres:
     * interactions - {Object} Objet JSON décrivant les intéractions.
     *
     * Chaque objet JSON doit être de la forme :
     * (start code)
     * {
     *	type: <type de l'interaction>,// parmi les valeurs de Descartes.Map.OL_INTERACTIONS_NAME
     * 				// ou classe personnalisée remplaçant la classe par défaut associée au type de l'intéraction
     *	args: <arguments>	// arguments du constructeur de l'intéraction
     * }
     * (end)
     */
    addOpenLayersInteractions: function (interactions) {
        if (!_.isNil(interactions)) {
            if (!(interactions instanceof Array)) {
                interactions = [interactions];
            }
            var that = this;
            _.each(interactions, function (interaction) {
                that.addOpenLayersInteraction(interaction);
            });
        }
    },
    /**
     * Methode: addOpenLayersControl
     * Ajoute un contrôle d'OpenLayers à la carte.
     *
     * Paramètres:
     * control - {Object} Objet JSON décrivant le contrôle.
     *
     * :
     * L'objet JSON doit être de la forme :
     * (start code)
     * {
     *	type: <type de l'outil>,// parmi les valeurs de Descartes.Map.OL_CONTROLS_NAME
     * 				// ou classe personnalisée remplaçant la classe par défaut associée au type de contrôle
     *	args: <arguments>	// arguments du constructeur de contrôle
     * }
     * (end)
     * Retour:
     * {Object} Le contrôle d'OpenLayers ajoutée.
     */
    addOpenLayersControl: function (control) {
        var controlInstance = null;
        var ControlClassName = null;
        if (_.isString(control.type)) {
            if (MapConstants.OL_CONTROLS_NAME.indexOf(control.type) !== -1) {
                ControlClassName = this.defaultOlControls[control.type];
            }
        } else if (_.isFunction(control.type)) {
            ControlClassName = control.type;
        }
        if (!_.isNil(ControlClassName)) {
            if (!_.isNil(control.args)) {
                controlInstance = new ControlClassName(control.args);
            } else {
                var options = {};
                if (control.type === MapConstants.OL_ROTATE) {
                    options.autoHide = false;
                    options.tipLabel = 'Réinitialiser la rotation';
                    options.className = 'DescartesOl-rotate';
                } else if (control.type === MapConstants.OL_ZOOM_TO_EXTENT) {
                    options.extent = this.maxExtent;
                } else if (control.type === MapConstants.OL_FULL_SCREEN) {
                    options.className = 'DescartesOl-full-screen';
                    options.tipLabel = 'Affichage plein écran';
                } else if (control.type === MapConstants.OL_MOUSE_POSITION) {
                    options.className = 'DescartesOl-mouse-position';
                } else if (control.type === MapConstants.OL_SCALE_LINE) {
                    options.className = 'DescartesOl-scale-line';
                } else if (control.type === MapConstants.OL_OVERVIEW_MAP) {
                    var opts = {projection: this.OL_map.getView().getProjection()};
                    options.view = new ol.View(opts);
                }
                controlInstance = new ControlClassName(options);
            }

            this.OL_map.addControl(controlInstance);

            return controlInstance;
        }
        return null;
    },
    /**
     * Methode: addOpenLayersControls
     * Ajoute plusieurs contrôles d'OpenLayers à la carte.
     *
     * Paramètres:
     * controls - {Object} Objet JSON décrivant les contrôles.
     *
     * Chaque objet JSON doit être de la forme :
     * (start code)
     * {
     *	type: <type de contrôle>,// parmi les valeurs de Descartes.Map.OL_CONTROLS_NAME
     * 				// ou classe personnalisée remplaçant la classe par défaut associée au type de contrôle
     *	args: <arguments>	// arguments du constructeur de contrôle
     * }
     * (end)
     */
    addOpenLayersControls: function (controls) {
        if (!_.isNil(controls)) {
            if (!(controls instanceof Array)) {
                controls = [controls];
            }
            var that = this;
            _.each(controls, function (control) {
                that.addOpenLayersControl(control);
            });
        }
    },
    /**
     * Methode: addAction
     * Ajoute une action <Descartes.Action> à la carte.
     *
     * Paramêtres:
     * action - {Object} Objet JSON décrivant l'action.
     *
     * :
     * L'objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de l'action>,           // parmi les valeurs de Descartes.Map.ACTIONS_NAME
     *                                              // ou classe personnalisée remplaçant la classe par défaut associée au type de l'action
     * 		div: <zone d'affichage de la vue>,  // élément DOM de la page accueillant la vue associée ou identifiant de cet élément.
     * 		options: <options>		    // options du constructeur de l'action
     * }
     * (end)
     * Retour:
     * {<Descartes.Action>}  L'action ajoutée.
     */
    addAction: function (action) {
        var ActionClassName = null;
        var actionInstance = null;
        if (typeof action.type === 'string') {
            if (MapConstants.ACTIONS_NAME.indexOf(action.type) !== -1) {
                ActionClassName = this.defaultActions[action.type];
            }
        } else if (typeof action.type === 'function') {
            ActionClassName = action.type;
        }
        if (ActionClassName !== null) {
            if (action.options !== null) {
                actionInstance = new ActionClassName(action.div, this.OL_map, action.options);
            } else {
                actionInstance = new ActionClassName(action.div, this.OL_map);
            }
            if (MapConstants._TOOLS_WITH_MAPCONTENT.indexOf(actionInstance.CLASS_NAME) !== -1) {
                if (actionInstance.setMapContent !== undefined) {
                    actionInstance.setMapContent(this.mapContent);
                }
            }
            if (actionInstance.CLASS_NAME === 'Descartes.Action.SizeSelector') {
                this.sizeSelector = actionInstance;
            }
        }
        return actionInstance;
    },
    /**
     * Methode: addActions
     * Ajoute plusieurs actions <Descartes.Action> à la carte.
     *
     * Paramêtres:
     * actions - {Array(Object)} Objets JSON décrivant les actions.
     *
     * :
     * Chaque objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de l'action>,               // parmi les valeurs de Descartes.Map.ACTIONS_NAME
     *                                                  // ou classe personnalisée remplaçant la classe par défaut associée au type de l'action
     * 		div: <zone d'affichage de la vue>,	// élément DOM de la page accueillant la vue associée ou identifiant de cet élément.
     * 		options: <options>			// options du constructeur de l'action
     * }
     * (end)
     */
    addActions: function (actions) {
        if (actions !== undefined) {
            if (!(actions instanceof Array)) {
                actions = [actions];
            }
            for (var i = 0, len = actions.length; i < len; i++) {
                this.addAction(actions[i]);
            }
        }
    },
    /**
     * Methode: addInfo
     * Ajoute une zone informative <Descartes.Info> à la carte.
     *
     * Paramêtres:
     * info - {Object} Objet JSON décrivant la zone informative.
     *
     * :
     * L'objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de la zone>,        	// parmi les valeurs de Descartes.Map.INFOS_NAME
     * 							// ou classe personnalisée remplaçant la classe par défaut associée au type de la zone informative
     *
     * 		div: <zone d'affichage de la zone>,	// élément DOM de la page accueillant la zone informative ou identifiant de cet élément.
     * 		options: <options>			// options du constructeur de la zone informative
     * }
     * (end)
     * Retour:
     * {<Descartes.Info>}  La zone informative ajoutée.
     */
    addInfo: function (info) {
        var InfoClassName = null;
        var infoInstance = null;
        if (typeof info.type === 'string') {
            if (MapConstants.INFOS_NAME.indexOf(info.type) !== -1) {
                InfoClassName = this.defaultInfos[info.type];
            }
        } else if (typeof info.type === 'function') {
            InfoClassName = info.type;
        }
        if (InfoClassName !== null) {
            if (info.options !== null) {
                infoInstance = new InfoClassName(info.div, info.options);
            } else {
                infoInstance = new InfoClassName(info.div);
            }
            if (MapConstants._TOOLS_WITH_MAPCONTENT.indexOf(infoInstance.CLASS_NAME) !== -1) {
                if (infoInstance.setMapContent !== undefined) {
                    infoInstance.setMapContent(this.mapContent);
                }
            }
            this.OL_map.addControl(infoInstance);
        }
        return infoInstance;
    },
    /**
     * Methode: addInfos
     * Ajoute plusieurs zones informatives <Descartes.Info> à la carte.
     *
     * Paramêtres:
     * infos - {Object} Objet JSON décrivant les zones informatives.
     *
     * :
     * Chaque objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de la zone>,        	// parmi les valeurs de Descartes.Map.INFOS_NAME
     *                                                  // ou classe personnalisée remplaçant la classe par défaut associée au type de la zone informative
     * 		div: <zone d'affichage de la zone>,	// élément DOM de la page accueillant la zone informative ou identifiant de cet élément.
     * 		options: <options>			// options du constructeur de la zone informative
     * }
     * (end)
     */
    addInfos: function (infos) {
        if (infos !== null) {
            if (!(infos instanceof Array)) {
                infos = [infos];
            }
            for (var i = 0, len = infos.length; i < len; i++) {
                this.addInfo(infos[i]);
            }
        }
    },
    /**
     * Methode: addContentManager
     * Ajoute un gestionnaire de contenu <Descartes.Action.MapContentManager> à la carte.
     *
     * Paramêtres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la barre d'outils spécifique
     * pour les outils du gestionnaire de contenu ou identifiant de cet élément.
     * toolsType - {Array(Object)} Liste des types d'outils à inclure dans le gestionnaire. Voir la méthode <Descartes.Action.MapContentManager.addTool>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de la méthode:
     * toolBarDiv - {DOMElement|String} Elément DOM de la page accueillant l'arborescence associée au gestionnaire ou identifiant de cet élément.
     * Options du gestionnaire. Voir le contructeur de la classe : <Descartes.Action.MapContentManager.Descartes.Action.MapContentManager>.
     *
     * Retour:
     * {<Descartes.Action.MapContentManager>} Le gestionnaire de contenu créé.
     */
    addContentManager: function (div, toolsType, contentManagerOptions) {

        var options = _.extend({}, contentManagerOptions);
        if ((!this.mapContent.editable || !this.mapContent.fctContextMenu) && options !== undefined) {
            if (options.contextMenuTools !== undefined && options.contextMenuTools !== null) {
                delete options.contextMenuTools;
            }
            if (options.contextMenuFct !== undefined && options.contextMenuFct !== null) {
                delete options.contextMenuFct;
            }
        }
        this.mapContentManager = new MapContentManager(div, this.mapContent, options);
        if (this.mapContent.fctContextMenu && this.mapContentManager.contextMenuTools && this.mapContentManager.contextMenuTools.ChooseWmsLayers) {
            this.mapContentManager.contextMenuTools.ChooseWmsLayers.olMap = this.OL_map;
        }
        if (this.mapContent.fctContextMenu && this.mapContentManager.contextMenuTools && this.mapContentManager.contextMenuTools.ExportVectorLayer) {
            this.mapContentManager.contextMenuTools.ExportVectorLayer.olMap = this.OL_map;
        }
        var self = this;
        if (this.mapContent.editable && toolsType !== undefined && toolsType !== null) {
            if (!(toolsType instanceof Array)) {
                toolsType = [toolsType];
            }
            var contentTools = [];
            for (var i = 0, len = toolsType.length; i < len; i++) {
                this.mapContentManager.addTool(toolsType[i]);
            }
            var managerToolBar = null;
            var toolBarOpener = (options !== undefined && options.openerTool);
            if (options !== undefined && options.toolBarDiv) {
                managerToolBar = new ToolBar(Utils.getDiv(options.toolBarDiv), this.OL_map, options.toolBarOptions);
                managerToolBar.toolBarName = MapContentManagerConstants.CONTENTMANAGER_TOOLBAR_NAME;
                self.toolBars.push(managerToolBar);
            } else if (options !== undefined && options.toolBarOptions !== undefined) {
                if (options.toolBarOptions.toolBar !== undefined) {
                    managerToolBar = options.toolBarOptions.toolBar;
                } else if (!toolBarOpener) {
                    managerToolBar = new ToolBar(Utils.getDiv(options.toolBarOptions.toolBarDiv), this.OL_map, options.toolBarOptions);
                    managerToolBar.toolBarName = MapContentManagerConstants.CONTENTMANAGER_TOOLBAR_NAME;
                    self.toolBars.push(managerToolBar);
                }
            }
            if (managerToolBar === null) {
                if (this.toolBars.length !== 0) {
                    managerToolBar = this.toolBars[0];
                } else if (this.mainToolBar !== null) {
                    managerToolBar = this.mainToolBar;
                } else {
                    throw new Error('Aucune barre d\'outils n\'est définie : les outils de gestion de contenu ne peuvent être ajoutés.');
                }
            }
            this.contentManagerToolBar = managerToolBar;
            if (this.mapContentManager.getTools().length !== 0) {
                if (toolBarOpener) {
                    if (options.toolBarOptions.toolBar !== undefined) {
                        delete options.toolBarOptions.toolBar;
                    }

                    var opts = {
                        displayClass: 'mapContentOpenerButton',
                        toolBarName: MapContentManagerConstants.CONTENTMANAGER_TOOLBAR_NAME,
                        toolBarOptions: options.toolBarOptions,
                        visible: false,
                        title: options.toolBarOptions.title,
                        tools: this.mapContentManager.getTools()/*,
                        initFunction: function () {
                            self.mapContentManager.activeInitialTools();
                        }*/
                    };
                    if (options.toolBarOptions.innerDiv) {
                        opts.innerDiv = options.toolBarOptions.innerDiv;
                    }
                    if (options.toolBarOptions.toolBarName) {
                        opts.toolBarName = options.toolBarOptions.toolBarName;
                    }
                    var openerButton = new ToolBarOpener(opts);
                    openerButton._initToolBarOpener(this);
                    managerToolBar.addControls([openerButton]);
                    managerToolBar.controls = managerToolBar.controls.concat(openerButton);
                } else {
                    managerToolBar.addControls(this.mapContentManager.getTools());
                    this.mapContentManager.activeInitialTools();
                    managerToolBar.controls = managerToolBar.controls.concat(this.mapContentManager.getTools());
                }
            }
        }

        for (var t = 0; t < this._toolsWithMapcontentManager.length; t++) {
            this._toolsWithMapcontentManager[t].setMapContentManager(this.mapContentManager);
        }
        ResultLayer.setMapContentManager(this.mapContentManager);

        return this.mapContentManager;
    },
    /**
     * Methode: addContentManagerSimple
     * Ajoute un gestionnaire de contenu <Descartes.Action.MapContentManager> à la carte.
     *
     * Paramêtres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la barre d'outils spécifique
     * pour les outils du gestionnaire de contenu ou identifiant de cet élément.
     * toolsType - {Array(Object)} Liste des types d'outils à inclure dans le gestionnaire. Voir la méthode <Descartes.Action.MapContentManager.addTool>.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de la méthode:
     * toolBarDiv - {DOMElement|String} Elément DOM de la page accueillant l'arborescence associée au gestionnaire ou identifiant de cet élément.
     * Options du gestionnaire. Voir le contructeur de la classe : <Descartes.Action.MapContentManager.Descartes.Action.MapContentManager>.
     *
     * Retour:
     * {<Descartes.Action.MapContentManager>} Le gestionnaire de contenu créé.
     */
    addContentManagerSimple: function (div, contentManagerOptions) {
        var opts = _.extend({}, contentManagerOptions);
        if (!this.mapContentManager) {
            opts.onlySimpleVue = true;
            this.mapContentManager = new MapContentManager(div, this.mapContent, opts);
        } else {
            this.mapContentManager.addLayersTreeSimple(div, opts);
        }
        return this.mapContentManager;
    },
    /**
     * Methode: addMiniMap
     * Ajoute une mini-carte de navigation <Descartes.Tool.MiniMap> à la carte.
     *
     * Paramètres:
     * resourceUrl - {String} URL de la ressource WMS alimentant la mini-carte
     * options - {Object} Options de la mini-carte. Voir le contructeur de la classe : <Descartes.Tool.MiniMap.Descartes.Tool.MiniMap>.
     *
     * Retour:
     * {<Descartes.Tool.MiniMap>} La mini-carte de navigation créée.
     */
    addMiniMap: function (resourceUrl, options) {
        return new MiniMap(this.OL_map, resourceUrl, options);
    },
    /**
     * Methode: addDirectionalPanPanel
     * Ajoute une une "rose des vents" de navigation <Descartes.Button.DirectionalPanPanel> à la carte.
     *
     * Retour:
     * {<Descartes.Button.DirectionalPanPanel>} La "rose des vents" créée.
     */
    addDirectionalPanPanel: function (options) {
        var panel = new DirectionalPanPanel(options);
        this.OL_map.addControl(panel);
        return panel;
    },
    /**
     * Methode: addToolTip
     * Ajoute un gestionnaire d'info-bulles <Descartes.Info.ToolTip> à la carte.
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'info-bulles ou identifiant de cet élément.
     * toolTipLayers - {Array(Object)} Tableau d'objets JSON représentant les couches prises en compte par l'info-bulle, sous la forme {layer:Descartes.Layer, fields:String[]}
     * options - {Object} Options du gestionnaire. Voir le contructeur de la classe : <Descartes.Info.ToolTip>.
     *
     * Retour:
     * {<Descartes.Info.ToolTip>} Le gestionnaire d'info-bulles créé.
     */
    addToolTip: function (div, toolTipLayers, options) {
        this.toolTip = new ToolTip(div, this.OL_map, toolTipLayers, options);
        if (this.mapContent !== null) {
            this.mapContent.events.register('layerRemoved', this, this.refreshToolTip);
        }
        return this.toolTip;
    },
    /**
     * Methode: addSelectToolTip
     * Ajoute un gestionnaire d'info-bulles à la selection d'objet <Descartes.Info.SelectToolTip> à la carte.
     *
     * Paramètres:
     * selectToolTipLayers - {Array(Object)} Tableau d'objets JSON représentant les couches prises en compte par l'info-bulle, sous la forme {layer:Descartes.Layer, fields:String[]}
     * options - {Object} Options du gestionnaire. Voir le contructeur de la classe : <Descartes.Info.ToolTip>.
     *
     * Retour:
     * {<Descartes.Info.SelectToolTip>} Le gestionnaire d'info-bulles créé.
     */
    addSelectToolTip: function (selectToolTipLayers, options) {
        this.selectToolTip = new SelectToolTip(this.OL_map, selectToolTipLayers, options);
        if (this.mapContent !== null) {
            this.mapContent.events.register('layerRemoved', this, this.refreshSelectToolTip);
        }
        return this.selectToolTip;
    },
    /**
     * Methode: addRequestManager
     * Ajoute un gestionnaire de requêtes attributaires <Descartes.Action.RequestManager> à la carte.
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le gestionnaire de requêtes attributaires ou identifiant de cet élément.
     * options - {Object} Options du gestionnaire. Voir le contructeur de la classe : <Descartes.Action.RequestManager.Descartes.Action.RequestManager>.
     *
     * Retour:
     * {<Descartes.Action.RequestManager>} Le gestionnaire de requêtes attributaires créé.
     */
    addRequestManager: function (div, options) {
        this.requestManager = new RequestManager(div, this.OL_map, options);
        if (!_.isNil(this.mapContent)) {
            this.mapContent.events.register('layerRemoved', this, this.refreshRequestManager);
        }
        return this.requestManager;
    },
    /**
     * Methode: addGazetteer
     * Ajoute un gestionnaire de localisation rapide <Descartes.Action.Gazetteer> à la carte.
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le gestionnaire de localisation rapide ou identifiant de cet élément.
     * initValue - {String} Code de la valeur de l'objet "père" des objets du plus haut niveau de localisation (i.e. d'index 0).
     * levels - {Array(<Descartes.GazetteerLevel>)} Liste des niveaux de localisation.
     * options - {Object} Options du gestionnaire. Voir le contructeur de la classe : <Descartes.Action.Gazetteer.Descartes.Action.Gazetteer>.
     *
     * Retour:
     * {<Descartes.Action.Gazetteer>} Le gestionnaire de localisation rapide créé.
     */
    addGazetteer: function (div, initValue, levels, options) {
        return new Gazetteer(div, this.OL_map, this.projection, initValue, levels, options);
    },
    /**
     * Methode: addDefaultGazetteer
     * Ajoute un gestionnaire de localisation rapide administratif <Descartes.Action.DefaultGazetteer> à la carte.
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le gestionnaire de localisation rapide administratif ou identifiant de cet élément.
     * initValue - {String} Code de la valeur de l'objet "père" des entités administratives du plus haut niveau de localisation (i.e. d'index 0).
     * startlevel - {<Descartes.Action.DefaultGazetteer.REGION> | <Descartes.Action.DefaultGazetteer.DEPARTEMENT> | <Descartes.Action.DefaultGazetteer.COMMUNE>}
     * Type d'entités administratives de la liste de plus haut niveau.
     * options - {Object} Options du gestionnaire. Voir le contructeur de la classe : <Descartes.Action.DefaultGazetteer.Descartes.Action.DefaultGazetteer>.
     *
     * Retour:
     * {<Descartes.Action.DefaultGazetteer>} Le gestionnaire de localisation rapide administratif créé.
     */
    addDefaultGazetteer: function (div, initValue, startlevel, options) {
        return new DefaultGazetteer(div, this.OL_map, this.projection, initValue, startlevel, options);
    },
    /**
     * Methode: addBookmarksManager
     * Ajoute un gestionnaire de vues personnalisées <Descartes.Action.BookmarksManager> à la carte.
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant le gestionnaire de vues personnalisées ou identifiant de cet élément.
     * mapName - {String} Nom de la carte utilisé pour le cookie stockant les vues.
     * options - {Object} Options du gestionnaire de vues personnalisées. Voir le contructeur de la classe : <Descartes.Action.BookmarksManager.Descartes.Action.BookmarksManager>.
     *
     * Retour:
     * {<Descartes.Action.BookmarksManager>} Le gestionnaire de vues personnalisées créé.
     */
    addBookmarksManager: function (div, mapName, options) {
        return new BookmarksManager(div, this, mapName, options);
    },
    /**
     * Methode: show
     * Affiche la carte.
     */
    show: function () {
        this.mapContent.refresh(this.mapContent.item);
        if (!this.autoScreenSize) {
            this.OL_map.getTargetElement().style.width = this.size[0].toString() + 'px';
            this.OL_map.getTargetElement().style.height = this.size[1].toString() + 'px';
            this.OL_map.updateSize();
        }
        if (this.autoSize || this.autoScreenSize) {
            this.OL_map.descartesAutoSize = true;
            this.autoSizeMap();
            var that = this;
            window.onresize = function () {
                that.autoSizeMap();
            };
        }

        this._addAllLayers();
        if (!this.OL_map.getView().getCenter()) {
            var mapType = this.OL_map.get('mapType');
            var constrainResolution = mapType === MapConstants.MAP_TYPES.DISCRETE;
            this.OL_map.getView().fit(this.initExtent, {
                constrainResolution: constrainResolution,
                minResolution: this.minResolution,
                maxResolution: this.maxResolution
            });
        }
        this.mapContent.mapped = true;
        this.refreshContentManager();
    },
    /**
     * Methode: refresh
     * Actualise la carte après modifications de son contenu.
     *
     * Paramètres:
     * onlyContentManager - {boolean} Indique s'il faut seulement raffraichir le gestionnaire de contenu.
     */
    refresh: function (event) {
        if (!_.isNil(event) && event.data.length > 0 && event.data[0] === true) {
            this.refreshContentManager(event.data[0]);
        } else {
            this._removeAllLayers();
            this._addAllLayers();
            this.refreshContentManager();
        }
    },
    /**
     * Methode: refreshRequestManager
     * Rend cohérente la liste des couches du gestionnaire de requêtes en cas de suppression de couches.
     */
    refreshRequestManager: function () {
        if (!_.isNil(this.requestManager)) {
            var mapLayers = this.mapContent.getLayers();
            var requests = this.requestManager.requests;
            var indexesToDelete = [];
            for (var iRequest = 0, lenRequests = requests.length; iRequest < lenRequests; iRequest++) {
                var requestLayer = requests[iRequest].layer;
                var layerPresent = false;
                for (var iMapLayer = 0, lenMapLayers = mapLayers.length; iMapLayer < lenMapLayers; iMapLayer++) {
                    var mapLayer = mapLayers[iMapLayer];
                    if (requestLayer === mapLayer) {
                        layerPresent = true;
                        break;
                    }
                }
                if (!layerPresent) {
                    indexesToDelete.push(iRequest);
                }
            }
            this.requestManager.removeRequests(indexesToDelete);
        }
    },
    /**
     * Methode: refreshToolTip
     * Rend cohérente la liste des couches du gestionnaire d'info-bulle en cas de suppression de couches.
     */
    refreshToolTip: function () {
        if (!_.isNil(this.toolTip)) {
            var mapLayers = this.mapContent.getLayers();
            var toolTips = this.toolTip.toolTipLayers;
            var indexesToDelete = [];
            for (var iToolTip = 0, lenToolTips = toolTips.length; iToolTip < lenToolTips; iToolTip++) {
                var toolTipLayer = toolTips[iToolTip].layer;
                var layerPresent = false;
                for (var iMapLayer = 0, lenMapLayers = mapLayers.length; iMapLayer < lenMapLayers; iMapLayer++) {
                    var mapLayer = mapLayers[iMapLayer];
                    if (toolTipLayer === mapLayer) {
                        layerPresent = true;
                        break;
                    }
                }
                if (!layerPresent) {
                    indexesToDelete.push(iToolTip);
                }
            }
            this.toolTip.removeToolTipLayers(indexesToDelete);
        }
    },
    /**
     * Methode: refreshSelectToolTip
     * Rend cohérente la liste des couches du gestionnaire d'info-bulle par sélection en cas de suppression de couches.
     */
    refreshSelectToolTip: function () {
        if (!_.isNil(this.selectToolTip)) {
            var mapLayers = this.mapContent.getLayers();
            var selectToolTips = this.selectToolTip.selectToolTipLayers;
            var indexesToDelete = [];
            for (var iToolTip = 0, lenToolTips = selectToolTips.length; iToolTip < lenToolTips; iToolTip++) {
                var selectToolTipLayer = selectToolTips[iToolTip].layer;
                var layerPresent = false;
                for (var iMapLayer = 0, lenMapLayers = mapLayers.length; iMapLayer < lenMapLayers; iMapLayer++) {
                    var mapLayer = mapLayers[iMapLayer];
                    if (selectToolTipLayer === mapLayer) {
                        layerPresent = true;
                        break;
                    }
                }
                if (!layerPresent) {
                    indexesToDelete.push(iToolTip);
                }
            }
            this.selectToolTip.removeSelectToolTipLayers(indexesToDelete);
        }
    },
    /**
     * Private
     */
    _addAllLayers: function () {
        var layers = this.mapContent.getLayers();
        layers = layers.sort(function (a, b) {
            return b.displayOrder - a.displayOrder;
        });
        this.OL_map.getLayers().clear();

        for (var i = 0; i < layers.length; i++) {
            var olLayers = layers[i].OL_layers;
            for (var j = 0; j < olLayers.length; j++) {
                var olLayer = olLayers[j];
                if (!this.displayExtendedOLExtent && !_.isNil(this.maxExtent)) {
                    if (_.isEqual(this.maxExtent, [-Infinity, -Infinity, Infinity, Infinity])) {
                       olLayer.setExtent(this.OL_map.getView().getProjection().getExtent());
                    } else {
                       olLayer.setExtent(this.maxExtent);
                    }
                }
                var view = this.OL_map.getView();
                var units = view.getProjection().getUnits();
                if (units !== 'm') {
                    var layer = layers[i];
                    layer.units = units;
                    if (!_.isNil(layer.maxScale)) {
                        var minResolution = Utils.getResolutionForScale(layer.maxScale, units);
                        olLayer.setMinResolution(minResolution);
                    }
                    if (!_.isNil(layer.minScale)) {
                        var maxResolution = Utils.getResolutionForScale(layer.minScale, units);
                        olLayer.setMaxResolution(maxResolution);
                    }
                }
                this.OL_map.addLayer(olLayer);
                olLayer.map = this.OL_map;
            }
        }
    },
    /**
     * Private
     */
    _removeAllLayers: function () {
        var layers = this.OL_map.getLayers().getArray();
        for (var i = layers.length - 1; i >= 0; --i) {
            var layer = layers[i];
            if (!layer.isBaseLayer &&
                    layer.get('title') !== 'OpenLayers.Handler.Point' &&
                    layer.get('title') !== 'OpenLayers.Handler.Polygon' &&
                    layer.get('title') !== 'OpenLayers.Handler.RegularPolygon') {
                this.OL_map.removeLayer(layer);
            }
        }
    },
    /**
     * Methode: refreshContentManager
     * Actualise la vue associée au gestionnaire de la carte après modifications de l'emprise courante.
     */
    refreshContentManager: function (redrawOnly) {
        if (this.mapContentManager !== null) {
            this.mapContentManager.redrawUI();
        }
    },

    /**
     * Methode: autoSizeMap
     * Actualise la taille de la carte automatiquement.
     */
    autoSizeMap: function () {
        var w = this.OL_map.getTargetElement().parentNode.clientWidth - 30;
        var h = this.OL_map.getTargetElement().parentNode.clientHeight;
        this.refreshSizeMap(w, h);
    },

    /**
     * Methode: refreshSizeMap
     * Actualise la taille de la carte.
     *
     * Paramètres:
     * w - {Integer} largeur de la carte.
     * h - {Integer} hauteur de la carte.
     */
    refreshSizeMap: function (w, h) {
        var view = this.OL_map.getView();
        var center = view.getCenter();
        var zoom = view.getZoom();

        if (!this.autoScreenSize) {
            this.OL_map.getTargetElement().style.width = w + 'px';
            this.OL_map.getTargetElement().style.height = h + 'px';
        }

        view.setCenter(center);
        view.setZoom(zoom);

        this.OL_map.updateSize();
    },
    /**
     * Methode: getNamedToolBar
     * Retourne une barre d'outils selon son nom.
     *
     * Paramêtres:
     * name - {String} le nom de la barre d'outils.
     *
     * Retour:
     * {<Descartes.ToolBar>} La barre d'outils.
     */
    getNamedToolBar: function (name) {
        var namedToolBar = null;
        for (var i = 0, len = this.toolBars.length; i < len; i++) {
            if (this.toolBars[i].toolBarName === name) {
                namedToolBar = this.toolBars[i];
                break;
            }
        }
        return namedToolBar;
    },
    /**
     * Methode: getContentManagerToolBar
     * Retourne la barre d'outils du gestionnaire de la carte.
     *
     * Retour:
     * {<Descartes.ToolBar>} La barre d'outils du gestionnaire de la carte.
     */
    getContentManagerToolBar: function () {
        var contentManagerToolBar = this.getNamedToolBar(MapContentManagerConstants.CONTENTMANAGER_TOOLBAR_NAME);
        if (contentManagerToolBar === null) {
            contentManagerToolBar = this.contentManagerToolBar;
        }
        return contentManagerToolBar;
    },
    getActiveInteractions: function () {
        var result = [];
        var interactions = this.OL_map.getInteractions().getArray();
        _.forEach(interactions, function (interaction) {
            if (interaction.getActive()) {
                result.push(interaction);
            }
        });
        return result;
    },
    isActiveInteraction: function (interaction) {
        var activeInteractions = this.getActiveInteractions();
        for (var i = 0; i < activeInteractions.length; i++) {
            var anInteraction = activeInteractions[i];
            if (interaction === anInteraction) {
                return true;
            }
        }
        return false;
    },
    CLASS_NAME: 'Descartes.Map'
});
module.exports = Map;
