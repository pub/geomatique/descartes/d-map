var _ = require('lodash');
var log = require('loglevel');

var Utils = require('../Utils/DescartesUtils');
var I18N = require('./I18N');

/**
 * Class: Descartes.Button
 * Classe "abstraite" définissant un bouton pouvant être disponible ou non selon le contexte.
 *
 * :
 * Le bouton doit être placé dans une barre d'outils définie par la classe <Descartes.ToolBar>.
 *
 * Hérite de:
 *  - <Descartes.I18N>
 *
 * Classes dérivées:
 *  - <Descartes.Button.CenterToCoordinates>
 *  - <Descartes.Button.ContentTask>
 *  - <Descartes.Button.DirectionalPan>
 *  - <Descartes.Button.ExportPDF>
 *  - <Descartes.Button.ExportPNG>
 *  - <Descartes.Button.ShareLinkMap>
 *  - <Descartes.Button.DisplayLayersTreeSimple>
 */
var Class = Utils.Class(I18N, {
    /**
     * Propriete: Id
     * {String} identifiant unique du bouton généré à l'exécution.
     */
    id: null,
    /**
     * Propriete: enabled
     * {Boolean} Indicateur pour la disponibilité du bouton (false par défaut).
     */
    enabled: false,

    parentPanel: null,

    displayClass: "",

    title: null,
    /**
     * Constructeur: Descartes.Button
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * title - {String} Texte de l'info-bulle du bouton.
     */
    initialize: function (options) {
        I18N.prototype.initialize.apply(this);
        this.id = Utils.createUniqueID();
        this.title = this.getMessage("TITLE");
        this.displayClass = this.CLASS_NAME.replace(/\./g, "");
        _.extend(this, options);
    },
    createButton: function (div) {
    },
    updateButton: function () {
    },
    /**
     * Methode: refresh
     * Rafraichit la disponibilité du bouton en cas de changement de la taille de la carte.
     */
    refresh: function () {
        if (this.enabled) {
            this.enable();
        } else {
            this.disable();
        }
    },

    /**
     * Methode: enable
     * Rend le bouton disponible
     */
    enable: function () {
        if (this.enabled !== true) {
            this.enabled = true;
            this.updateButton();
        }
    },

    /**
     * Methode: disable
     * Rend le bouton indisponible
     */
    disable: function () {
        if (this.enabled !== false) {
            this.enabled = false;
            this.updateButton();
        }
    },

    /**
     * Methode: trigger
     * Déclenche l'exécution du traitement si le bouton est disponible.
     */
    trigger: function () {
        if (this.enabled) {
            this.execute();
        }
    },

    /**
     * Methode: execute
     * Exécute le traitement.
     *
     * Doit être surchargée dans chaque classe dérivée.
     */
    execute: function () {
        log.debug('execute');
    },
    CLASS_NAME: 'Descartes.Button'
});

module.exports = Class;
