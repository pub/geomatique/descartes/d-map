var ol = require('openlayers');

var Utils = require('../Utils/DescartesUtils');

/**
 * Class: Descartes.Projection
 * Classe permettant le mapping entre les projections d'openlayers et Descartes.
 */
var Class = Utils.Class({

    /**
     * Constructeur: Descartes.Projection
     * Constructeur d'instance, retourne la projection openlayers.
     *
     * Paramètres:
     *  {String} code
     */
    initialize: function (code) {
        return ol.proj.get(code);
    },

    CLASS_NAME: 'Descartes.Projection'
});

module.exports = Class;
