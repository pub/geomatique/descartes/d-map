var Utils = require('../Utils/DescartesUtils');
var Info = require('../Core/Info');

/**
 * Class: Descartes.Info.MetricScale
 * Classe définissant une zone informative affichant une échelle métrique.
 *
 * Hérite de:
 *  - <Descartes.Info>
 */
var Class = Utils.Class(Info, {

    defaultDisplayClasses: {
        globalClassName: 'DescartesInfos'
    },
    /**
     * Propriete: label
     * {Boolean} Indicateur pour la présence d'un texte du type "Echelle : " avant la valeur de l'échelle (true par défaut).
     *
     * :
     * L'échelle est formatée par la méthode <Descartes.Utils.readableScale>
     */
    label: true,
    /**
     * Constructeur: Descartes.Info.MetricScale
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la zone informative ou identifiant de cet élement.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * label - {Boolean} Indicateur pour la présence d'un texte du type "Echelle : " avant la valeur de l'échelle.
     */
    initialize: function (div, options) {
        Info.prototype.initialize.apply(this, [div, options]);
    },
    /**
     * Methode: update
     * Actualise l'échelle métrique en fonction du contexte
     */
    update: function () {
        this.element.innerHTML = '';
        if (this.label) {
            this.element.innerHTML = "<span class=\"DescartesUIText\">" + this.getMessage('TITLE_MESSAGE') + "</span>";
        }
        var units = this.getMap().getView().getProjection().getUnits();
        var scale = Utils.getScaleFromResolution(this.getMap().getView().getResolution(), units);
        scale = Math.round(scale);
        this.element.innerHTML += "<span class=\"DescartesUIText\">" + Utils.readableScale(scale) + "</span>";
    },

    CLASS_NAME: 'Descartes.Info.MetricScale'
});

module.exports = Class;
