require('./css/Attribution.css');

var ol = require('openlayers');
var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var Info = require('../Core/Info');


/**
 * Class: Descartes.Info.Attribution
 * Classe définissant une zone informative affichant les textes de copyright des couches (éventuellement liens vers des images).
 * Utilise ol.control.Attribution
 *
 * Hérite de:
 *  - <Descartes.Info>
 */
var Class = Utils.Class(Info, {

    defaultDisplayClasses: {
        globalClassName: 'DescartesAttribution',
        inDivClassName: 'DescartesAttributionInDiv',
        olAttributionClassName: 'ol-attribution',
        olUnselectableClassName: 'ol-unselectable'
    },
    collapsible: true,
    collapsed: false,
    /**
     * Propriete: separateur
     * {String} chaîne de caractères utilisée pour séparer les textes de copyright.
     */
    separateur: null,
    /**
     * Constructeur: Descartes.Info.Attribution
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la zone informative ou identifiant de cet élement.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction de la classe:
     * separateur - {String} chaîne de caractères à utiliser pour séparer les textes de copyright.
     */
    initialize: function (div, options) {
        if (_.isNil(options)) {
            options = {};
        }

        if (!_.isNil(options.separateur)) {
            this.separateur = options.separateur;
            delete options.separateur;
        }
        Info.prototype.initialize.apply(this, [div, options]);
        div = Utils.getDiv(div);

        if (_.isNil(options.className)) {
            if (_.isNil(div)) {
                options.className = this.defaultDisplayClasses.globalClassName + ' ' +
                        this.defaultDisplayClasses.olAttributionClassName + ' ' +
                        this.defaultDisplayClasses.olUnselectableClassName;
            } else {
                options.className = this.defaultDisplayClasses.globalClassName + ' ' +
                        this.defaultDisplayClasses.inDivClassName + ' ' + //<-- ajout
                        this.defaultDisplayClasses.olAttributionClassName + ' ' +
                        this.defaultDisplayClasses.olUnselectableClassName;
            }
        }

        options.target = div;
        options.collapsed = this.collapsed;
        options.collapsible = this.collapsible;

        options.render = this.updateAttribution.bind(this);

        this.control = new ol.control.Attribution(options);
    },
    setMap: function (map) {
        map.addControl(this.control);
    },
    /**
     * Methode: updateAttribution
     * Surcharge de la méthode définie dans OpenLayers.Control.Attribution pour éviter les doublons de textes de copyright.
     */
    updateAttribution: function () {
        var attributions = [];
        var map = this.control.getMap();
        var layers = map.getLayers();


        layers.forEach(function (layer) {
            if (layer.attribution && layer.getVisible()) {
                var found = false;
                for (var j = 0; j < attributions.length; j++) {
                    if (attributions[j] === layer.attribution) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    attributions.push(layer.attribution);
                }
            }
        }, this);
        if (!_.isEmpty(attributions)) {
            var html = '';
            _.each(attributions, function (attribution) {
                html += '<li>';
                html += attribution;
                if (!_.isEmpty(this.separateur)) {
                    html += ' ' + this.separateur;
                }
                html += '</li>';
            }.bind(this));
            var ul = this.control.element.getElementsByTagName('ul')[0];
            ul.innerHTML = html;
        }
    },

    CLASS_NAME: 'Descartes.Info.Attribution'
});

module.exports = Class;
