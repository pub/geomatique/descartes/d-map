require('./css/GraphicScale.css');

var ol = require('openlayers');

var Utils = require('../Utils/DescartesUtils');
var Info = require('../Core/Info');

/**
 * Class: Descartes.Info.GraphicScale
 * Classe définissant une zone informative affichant une échelle graphique.
 *
 * Hérite de:
 *  - <Descartes.Info>
 */
var Class = Utils.Class(Info, {

    defaultDisplayClasses: {
        globalClassName: 'DescartesGraphicScale',
        evenImageClassName: 'DescartesEvenScaleBar',
        oddImageClassName: 'DescartesOddScaleBar'
    },
    /**
     * Constructeur: Descartes.Info.GraphicScale
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la zone informative ou identifiant de cet élement.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     */
    initialize: function (div, options) {
        Info.prototype.initialize.apply(this, [div, options]);
    },
    /**
     * Methode: update
     * Actualise l'échelle graphique en fonction du contexte
     */
    update: function () {
        var map = this.getMap();
        var view = map.getView();
        var projection = view.getProjection();
        var units = projection.getUnits();

        var maxSizeKilometers;
        if (units === 'degrees') {
            maxSizeKilometers = 85 * ol.proj.getPointResolution(projection, view.getResolution(), view.getCenter(), units) * ol.proj.METERS_PER_UNIT['degrees'] / ol.proj.METERS_PER_UNIT['m'] ;
            maxSizeKilometers = maxSizeKilometers.toString().split('.')[0];
            var fn = maxSizeKilometers.toString().substr(0, 1);
            var tn = maxSizeKilometers.toString().length;
            var new_maxSizeKilometers = fn;
            for (var j = 1 ; j < tn ; j++) {
                new_maxSizeKilometers += '0';
            }
            maxSizeKilometers = new_maxSizeKilometers;
        }

        var fMapWidth = map.getSize()[0];
        var extent = view.calculateExtent(map.getSize());
        if (units === 'm') {
            fMapWidth = fMapWidth / (ol.proj.getPointResolution(projection, 1, ol.extent.getCenter(extent), units) * projection.getMetersPerUnit());
        }
        var mapWidth = ol.extent.getWidth(extent);

        var fGeoWidth = Utils.convertUnits(mapWidth, units);
        var SCALE_FACTOR = 5;
        var SCALE_ZONES = 4;
        var SCALE_HEIGHT = 5;

        var geo_width = Math.round(fGeoWidth / SCALE_FACTOR);
        var firstNumber = geo_width.toString().substr(0, 1);
        var totalNumbers = geo_width.toString().length;
        var new_geo_width = firstNumber;
        var i;
        for (i = 1; i < totalNumbers; i++) {
            new_geo_width += '0';
        }
        var new_scale_width = (new_geo_width * fMapWidth / fGeoWidth) / SCALE_ZONES;

        var scale_infos = '';
        for (i = 1; i <= SCALE_ZONES; i++) {
            var className = (Math.round(i / 2) * 2 === i) ? this.displayClasses.evenImageClassName : this.displayClasses.oddImageClassName;
            scale_infos += '<div style="width:' + new_scale_width + 'px;';
            scale_infos += 'height:' + SCALE_HEIGHT + 'px;"';
            scale_infos += ' class="' + className + '">';
            scale_infos += '</div>';
        }

        if (units === 'degrees') {
            scale_infos += ' ' + Utils.adaptUnits(maxSizeKilometers);
        } else {
            scale_infos += ' ' + Utils.adaptUnits(new_geo_width);
        }

        this.element.innerHTML = scale_infos;
    },

    CLASS_NAME: 'Descartes.Info.GraphicScale'
});

module.exports = Class;
