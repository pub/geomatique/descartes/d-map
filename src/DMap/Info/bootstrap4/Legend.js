/* global MODE, Descartes*/

var _ = require('lodash');
var $ = require('jquery');

var Utils = require('../../Utils/DescartesUtils');
var Info = require('../../Core/Info');

var panelTemplate = require('../../UI/' + MODE + '/templates/Panel.ejs');
var legendTemplate = require('./templates/Legend.ejs');

/**
 * Class: Descartes.Info.Legend
 * Classe définissant une zone informative affichant un panneau de légendes.
 *
 * Hérite de:
 *  - <Descartes.Info>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'changed' de la classe <Descartes.MapContent> déclenche la méthode <update>.
 *  - l'événement 'itemPropertyChanged' de la classe <Descartes.MapContent> déclenche la méthode <update>.
 */
var Class = Utils.Class(Info, {
    /**
     * Propriete: defaultPanelOptions
     * {Objet} Options du panel.
     */
    defaultOptionsPanel: {
        collapsible: false,
        collapsed: false,
        panelCss: null
    },

    defaultDisplayClasses: {
        globalClassName: 'DescartesInfos',
        textClassName: 'DescartesUIText',
        separateurClassName: 'DescartesInfos'
    },
    /**
     * Propriete: separateur
     * {Boolean} Indicateur pour la présence d'une ligne séparatrice entre chaque légende (true par défaut).
     */
    separateur: true,
    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,
    /**
     * Propriete: label
     * {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de legende.
     */
    label: true,
    /**
     * Propriete: displayLayerTitle
     * {Boolean} Indicateur pour l'affichage ou non des titres des couches.
     */
    displayLayerTitle: false,
    /**
     * Constructeur: Descartes.Info.Legend
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la zone informative ou identifiant de cet élement.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * separateur - {Boolean} Indicateur pour la présence d'une ligne séparatrice entre chaque légende.
     * label - {Boolean} Indicateur pour l'affichage ou non d'un titre pour la zone de legende.
     */
    initialize: function (div, options) {
        this.defaultOptionsPanel.panelCss = Descartes.UIBootstrap4Options.panelCss;
        this.optionsPanel = _.extend({}, this.defaultOptionsPanel);

        if (!_.isNil(options) && !_.isNil(options.optionsPanel)) {
            _.extend(this.optionsPanel, options.optionsPanel);
            delete options.optionsPanel;
        }
        Info.prototype.initialize.apply(this, [div, options]);
        this.id = Utils.createUniqueID();
        var that = this;
        $(this.div).on('hidden.bs.collapse', function () {
            that.optionsPanel.collapsed = true;
        });
        $(this.div).on('shown.bs.collapse', function () {
            that.optionsPanel.collapsed = false;
        });
    },
    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au panneau de légendes.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
        this.mapContent.events.register('changed', this, this.update);
        this.mapContent.events.register('itemPropertyChanged', this, this.update);
    },
    /**
     * Methode: update
     * Actualise le panneau de légendes en fonction du contexte
     */
    update: function () {
        this.layersVisible = _.extend([], this.mapContent.getVisibleLayers());

        var legends = [];
        var that = this;
        _.forEach(this.layersVisible, function (layer) {
            if (!_.isNil(layer.legend)) {
                var legend = {};
                if (that.displayLayerTitle) {
                    legend.title = layer.title;

                }
                legend.urls = layer.legend;
                legends.push(legend);
            }
        });

        var content = legendTemplate({
            legends: legends,
            separateur: this.separateur
        });


        var template = panelTemplate({
            id: this.id + '_panel',
            label: this.label,
            title: this.getMessage('TITLE_MESSAGE'),
            collapsible: this.optionsPanel.collapsible,
            collapsed: this.optionsPanel.collapsed,
            panelCss: this.optionsPanel.panelCss,
            content: content
        });
        $(this.div).html(template);
    },
    CLASS_NAME: 'Descartes.Info.Legend'
});

module.exports = Class;
