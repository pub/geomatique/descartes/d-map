var ol = require('openlayers');
var _ = require('lodash');
var $ = require('jquery');

var Utils = require('../../Utils/DescartesUtils');
var Info = require('../../Core/Info');
var Projection = require('../../Core/Projection');

var localizedMousePositionTemplate = require('./templates/LocalizedMousePosition.ejs');

/**
 * Class: Descartes.Info.LocalizedMousePosition
 * Classe définissant une zone informative affichant les coordonnées courantes.
 * Utilise ol.control.MousePosition
 *
 * Hérite de:
 *  - <Descartes.Info>
 */
var Class = Utils.Class(Info, {

    defaultDisplayClasses: {
        globalClassName: 'DescartesInfos'
    },

    displayClasses: {},
    /**
     * Propriete: projection
     * {Boolean} Indicateur pour la présence du nom de la projection sous le couple de coordonnées (true par défaut).
     */
    projection: true,
    /**
     * Propriete: numDigits
     * {Integer} Le nombre de digit à afficher. Par défaut 5.
     */
    numDigits: 5,
    /**
     * Propriete: prefix
     * {String} Chaine de caractères ajouté au début des coordonnées. Par défaut vide ''.
     */
    prefix: '',
    /**
     * Propriete: separator
     * {String} Séparateur utilisé pour séparer les coordonnées. Par défaut : ' - ' ce qui donne '42.12 - 21.22'.
     */
    separator: ' - ',
    /**
     * Propriété: suffix
     * {String} Chaine de caractères ajouté à la fin des coordonnées. Par défaut vide ''.
     */
    suffix: '',
    /**
     * Propriété: inline
     * {Boolean} affichage en ligne ou pas.
     */
    inline: false,
    /**
     * Propriété: displayProjectionsList
     * {Array} Tableau définissant les projections d'affichage des coordonnées.
     */
    displayProjections: [],
    /**
     * Propriété: selectedDisplayProjectionIndex
     * {Integer} Position de la projection d'affichage sélectionnée
     */
    selectedDisplayProjectionIndex: 0,
    _selectedDisplayProjection: "",
    _coordinateFormat: "DD",
    _precision: null,
    /**
     * Constructeur: Descartes.Info.LocalizedMousePosition
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la zone informative ou identifiant de cet élement.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * projection - {Boolean} Indicateur pour la présence du nom de la projection sous le couple de coordonnées.
     */
    initialize: function (div, options) {
        this.displayClasses = _.extend({}, this.defaultDisplayClasses);
        if (!_.isNil(options)) {
            _.extend(this.displayClasses, options.displayClasses);
            delete options.displayClasses;
        }
        _.extend(this, options);
        if (!(this.projection && this.displayProjections.length > 0)) {
            this.displayProjections = [];
        }

        this.div = Utils.getDiv(div);

        var that = this;

        this.control = new ol.control.MousePosition({
            coordinateFormat: function (lonlat) {
                return that.formatOutput(lonlat, that._coordinateFormat, that._precision);
            },
            className: this.displayClasses.globalClassName,
            target: div,
            undefinedHTML: '&nbsp;'
        });
    },
    setMap: function (map) {

        if (this.displayProjections.length === 0) {
            this.displayProjections.push(map.getView().getProjection().getCode());
            this.selectedDisplayProjectionIndex = 0;
            this._precision = this.numDigits;
        } else if (this.displayProjections.length === 1) {
            this._precision = this.numDigits;
        } else {
            this._precision = Utils.getProjectionPrecision(this.displayProjections[this.selectedDisplayProjectionIndex]);
        }
        this._selectedDisplayProjection = this.displayProjections[this.selectedDisplayProjectionIndex];
        if ((this._selectedDisplayProjection.indexOf("-DM") !== -1) || (this._selectedDisplayProjection.indexOf("-DMS") !== -1)) {
            var values = this._selectedDisplayProjection.split('-');
            this._selectedDisplayProjection = values[0];
            this._coordinateFormat = values[1];
        }

        this._selectedDisplayProjection = new Projection(this._selectedDisplayProjection);

        this.control.setProjection(this._selectedDisplayProjection);
        map.addControl(this.control);
    },
    /**
     * Methode: formatOutput
     * Actualise les coordonnées courantes en fonction du contexte
     *
     * Paramètres:
     * lonLat - {Array} Coordonnées géographiques
     */
    formatOutput: function (lonLat, displayFormat, precision) {

        var content = "";

        if (this.displayProjections.length > 1) {
           this._coordinateFormat = displayFormat;
           this._precision = precision;
        }

        if (this._coordinateFormat === "DD") {
            if (this._selectedDisplayProjection.getCode() === "EPSG:4326" || this._selectedDisplayProjection.getCode() === "EPSG:4171" || this._selectedDisplayProjection.getCode() === "EPSG:4258") {
               content = Utils.coordinateToStringHD(lonLat, this._precision, this.separator);
            } else {
          var digits = parseInt(this._precision, 10);
          var digits1 = Utils.localizeNumber(lonLat[0], digits);
          var digits2 = Utils.localizeNumber(lonLat[1], digits);
          content = digits1 + this.separator + digits2;
            }
        } else if (this._coordinateFormat === "DM") {
          content = Utils.coordinateToStringHDM(lonLat, this._precision, this.separator);
        } else if (this._coordinateFormat === "DMS") {
          content = Utils.coordinateToStringHDMS(lonLat, this._precision, this.separator);
        }

        var codesvalues = [];
        if (this.displayProjections.length > 0) {
           for (var i = 0, len = this.displayProjections.length; i < len; i++) {
               codesvalues.push({code: this.displayProjections[i], value: Utils.getProjectionName(this.displayProjections[i])});
           }
        }
        var id = "projectionSelector";
        var template = localizedMousePositionTemplate({
            id: id,
            prefix: this.prefix,
            content: content,
            projection: this.projection,
            suffix: this.suffix,
            inline: this.inline,
            codesvalues: codesvalues,
            selectedProjection: this.selectedDisplayProjectionIndex
        });
        $(this.div).html(template);

        var that = this;
        $('#' + id).on('change', function () {
            that.selectedDisplayProjectionIndex = this.selectedIndex;
            var selectedValue = $(this).val();
            var precision = Utils.getProjectionPrecision(selectedValue);
            var displayFormat = "DD";

            if ((selectedValue.indexOf("-DM") !== -1) || (selectedValue.indexOf("-DMS") !== -1)) {
                var values = selectedValue.split('-');
                selectedValue = values[0];
                displayFormat = values[1];
            }

            lonLat = ol.proj.transform(lonLat, that._selectedDisplayProjection.getCode(), selectedValue);
            that._selectedDisplayProjection = new Projection(selectedValue);

            that.control.setProjection(that._selectedDisplayProjection);
            that.formatOutput(lonLat, displayFormat, precision);
        });
    },
    CLASS_NAME: 'Descartes.Info.LocalizedMousePosition'
});
module.exports = Class;
