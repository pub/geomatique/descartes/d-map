/* global MODE */

var _ = require('lodash');
var $ = require('jquery');
var ol = require('openlayers');

var Utils = require('../../Utils/DescartesUtils');

var panelTemplate = require('../../UI/' + MODE + '/templates/Panel.ejs');
var legendTemplate = require('./templates/Legend.ejs');

/**
 * Class: Descartes.Info.MeasurePopup
 * Classe pour l'affichage des mesures graphiques effectuées sur la carte
 *
 */
var Class = Utils.Class({

    defaultDisplayClasses: {
        messageClassName: "DescartesMeasurePopup",
        closeClassName: "DescartesMeasurePopupClose"
    },

    displayClasses: null,

    messageDiv: null,
    closeDiv: null,
    /**
     * Constructeur: Descartes.Info.MeasurePopup
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
    */
    initialize: function (div, options) {
        this.displayClasses = _.extend({}, this.defaultDisplayClasses);
        if ((options !== undefined)) {
            _.extend(this.displayClasses, options.displayClasses);
            delete options.displayClasses;
        }

        var olOptions = {};
        olOptions.displayClass = this.displayClasses.messageClassName;

        var element = document.createElement('div');
        element.className = this.displayClass;

        this.div = Utils.getDiv(div);

        ol.control.Control.call(this, {
            element: element,
            target: options.target
        });
    },
    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au panneau de légendes.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
        this.mapContent.events.register('changed', this, this.update);
        this.mapContent.events.register('itemPropertyChanged', this, this.update);
    },
    /**
     * Methode: update
     * Actualise le panneau de légendes en fonction du contexte
     */
    update: function () {
        this.layersVisible = _.extend([], this.mapContent.getVisibleLayers());

        var legends = [];
        _.forEach(this.layersVisible, function (layers) {
            if (!_.isNil(layers.legend)) {
                legends = legends.concat(layers.legend);
            }
        });

        var content = legendTemplate({
            legends: legends,
            separateur: this.separateur
        });


        var template = panelTemplate({
            id: this.id + '_panel',
            label: this.label,
            title: this.getMessage('TITLE_MESSAGE'),
            collapsible: this.optionsPanel.collapsible,
            collapsed: this.optionsPanel.collapsed,
            panelCss: this.optionsPanel.panelCss,
            content: content
        });
        $(this.div).html(template);

    },
    CLASS_NAME: "Descartes.Info.MeasurePopup"
});

module.exports = Class;
