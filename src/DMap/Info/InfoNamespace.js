/* global MODE */
var _ = require('lodash');

var Info = require('../Core/Info');
var Attribution = require('./Attribution');
var Legend = require('./' + MODE + '/Legend');
var MeasurePopup = require('./' + MODE + '/MeasurePopup');
var GraphicScale = require('./GraphicScale');
var MetricScale = require('./MetricScale');
var MapDimensions = require('./MapDimensions');
var LocalizedMousePosition = require('./' + MODE + '/LocalizedMousePosition');
var ToolTip = require('./ToolTip');
var SelectToolTip = require('./SelectToolTip');

var namespace = {
    Attribution: Attribution,
    GraphicScale: GraphicScale,
    Legend: Legend,
    LocalizedMousePosition: LocalizedMousePosition,
    MapDimensions: MapDimensions,
    MetricScale: MetricScale,
    MeasurePopup: MeasurePopup,
    ToolTip: ToolTip,
    SelectToolTip: SelectToolTip
};

_.extend(Info, namespace);

module.exports = Info;
