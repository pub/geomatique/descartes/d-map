var ol = require('openlayers');

var Utils = require('../Utils/DescartesUtils');
var Info = require('../Core/Info');

/**
 * Class: Descartes.Info.MapDimensions
 * Classe définissant une zone informative affichant les dimensions "terrain" de la carte.
 *
 * Hérite de:
 *  - <Descartes.Info>
 */
var Class = Utils.Class(Info, {

    defaultDisplayClasses: {
        globalClassName: 'DescartesInfos'
    },
    /**
     * Propriete: arrondis
     * {Boolean} Indicateur pour la simplification de l'affichage des dimensions (true par défaut).
     *
     * :
     * Voir la méthode <Descartes.Utils.adaptUnits>
     */
    arrondis: true,
    /**
     * Constructeur: Descartes.Info.MapDimensions
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant la zone informative ou identifiant de cet élement.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * arrondis - {Boolean} Indicateur pour la simplification de l'affichage des dimensions.
     */
    initialize: function (div, options) {
        Info.prototype.initialize.apply(this, [div, options]);
    },
    /**
     * Methode: update
     * Actualise les dimensions "terrain" de la carte en fonction du contexte
     */
    update: function () {

        var extent = this.getMap().getView().calculateExtent(this.getMap().getSize());
        var mapWidth = ol.extent.getWidth(extent);
        var mapHeight = ol.extent.getHeight(extent);

        var units = this.getMap().getView().getProjection().getUnits();

        var w = Utils.convertUnits(mapWidth, units);
        var h = Utils.convertUnits(mapHeight, units);

        var html = "<span class=\"DescartesUIText\">" + this.getMessage('TITLE_LARGEUR_MESSAGE');
        if (this.arrondis) {
            html += Utils.adaptUnits(w.toString().split('.')[0]);
            html += this.getMessage('TITLE_HAUTEUR_MESSAGE');
            html += Utils.adaptUnits(h.toString().split('.')[0]);
        } else {
            html += w.toString();
            html += Utils.DISPLAY_MAP_UNITS;
            html += this.getMessage('TITLE_HAUTEUR_MESSAGE');
            html += h.toString();
            html += Utils.DISPLAY_MAP_UNITS;
        }
        this.element.innerHTML = html + "</span>";
    },

    CLASS_NAME: 'Descartes.Info.MapDimensions'
});

module.exports = Class;
