/* global MODE, Descartes */

var _ = require('lodash');
var log = require('loglevel');

var Utils = require('../Utils/DescartesUtils');
var ExternalCallsUtils = require('../Core/ExternalCallsUtils');
var ExternalCallsUtilsConstants = require('../Core/ExternalCallsUtilsConstants');
var AjaxSelection = require('../Action/AjaxSelection');
var ToolTipContainer = require('../UI/' + MODE + '/ToolTipContainer');

var UIConstants = require('../UI/UIConstants');

require('./css/ToolTip.css');

/**
 * Class: Descartes.Info.ToolTip
 * Classe définissant une info-bulle s'affichant en cas d'inactivité du pointeur.
 *
 */
var Class = Utils.Class({

    /**
     * Propriete: defaultHandlerOptions
     * {Object} Objet JSON stockant les propriétés par défaut du OpenLayer.Handler.Hover associé.
     *
     * :
     * delay - 500
     * displayDelay - 5000
     * pixelTolerance - null
     */
    defaultOptions: {
        'delay': 500,
        'displayDelay': 5000,
        'pixelTolerance': 10,
        'stopMove': true,
        'displayOnClick': false
    },

    /**
     * Propriete: toolTipLayers
     * {Array(Object} Tableau d'objets JSON représentant les couches prises en compte par l'info-bulle, sous la forme {layer:Descartes.Layer, fields:String[]}
     */
    toolTipLayers: null,

    /**
     * Private
     */
    mousePosition: null,

    previousPosition: null,
    overlays: [],

    /**
     * Propriete: defaultResultUiParams
     * {Object} Objet JSON stockant les propriétés par défaut nécessaires à la génération de l'info-bulle.
     *
     * :
     * type - <Descartes.UI.TYPE_WIDGET>
     * view - <Descartes.UI.ToolTipContainer>
     * format - "JSON"
     */
    defaultResultUiParams: {
        type: UIConstants.TYPE_WIDGET,
        waitingMsg: false,
        format: 'JSON'
    },

    /**
     * Propriete: resultUiParams
     * {Object} Objet JSON stockant les propriétés nécessaires à la génération de l'info-bulle grâce à la classe <Descartes.Action.AjaxSelection>.
     *
     * :
     * Construit à partir de defaultResultUiParams, puis surchargé par la DIV passée au constructeur et en présence d'options complémentaires.
     *
     * :
     * type - <Descartes.UI.TYPE_WIDGET> | <Descartes.UI.TYPE_INPLACEDIV> | <Descartes.UI.TYPE_POPUP> : Type de vue pour l'info-bulle
     * view - {<Descartes.UI>} : Classe de la vue si le type est <Descartes.UI.TYPE_WIDGET>
     * format - "JSON"|"HTML"|"XML" : format de retour des informations souhaité.
     */
    resultUiParams: null,
    /**
     * Constructeur: Descartes.Info.ToolTip
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'info-bulle.
     * olMap - {ol.Map} Carte OpenLayers concernée.
     * toolTipLayers - {Array(Object)} Tableau d'objets JSON repr�sentant les couches prises en compte par l'info-bulle, sous la forme {layer:Descartes.Layer, fields:String[]}
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * handlerOptions - {Object} Objet JSON stockant les propriétés du OpenLayer.Handler.Hover associé.
     * resultUiParams - {Object} Objet JSON stockant les propriétés nécessaires à la génération de l'info-bulle.
     */
    initialize: function (div, olMap, toolTipLayers, options) {
        this.toolTipLayers = toolTipLayers;
        this.toolTipLayers = this.replaceSpecialChar(this.toolTipLayers);
        this.olMap = olMap;
        this.resultUiParams = _.extend({}, this.defaultResultUiParams);
        this.resultUiParams.div = div;
        this.createDiv();

        _.extend(this.defaultOptions, options);

        if (!_.isNil(options)) {
            _.extend(this.resultUiParams, options.resultUiParams);
            delete options.resultUiParams;
        }
        this.options = this.defaultOptions;

        if (!this.options.displayOnClick) {
        this.olMap.on('pointermove', this.onPointerMove.bind(this), this);

        setInterval(this.checkTolerance, this.options.delay, this);
        } else {
            this.olMap.on('click', this.onClick.bind(this), this);
            Descartes._activeClickToolTip = true;
        }
    },
    createDiv: function () {
        var div = document.getElementById(this.resultUiParams.div);
        if (_.isNil(div)) {
            div = document.createElement('div');
            div.id = this.resultUiParams.div;
            document.body.appendChild(div);
        }
    },

    onPointerMove: function (e) {
        if (e.dragging) {
            return;
        }
        this.currentPosition = e.pixel;
        this.coordinate = e.coordinate;
        setTimeout(function (that) {
            that.previousPosition = e.pixel;
        }, this.options.delay / 2, this);
    },

    checkTolerance: function (that) {
        if (!that.mouseOverToolTip && that.previousPosition != null && that.currentPosition === that.previousPosition) {
            //meme position
            var deltaX = that.currentPosition[0] - that.previousPosition[0];
            var deltaY = that.currentPosition[1] - that.previousPosition[1];

            if (Math.abs(deltaX) <= that.options.pixelTolerance &&
                    Math.abs(deltaY) <= that.options.pixelTolerance) {
                        that.tooltipPositionCoordinate = that.coordinate;
                        if (!that.requesting) {
                              //that.olMap.getViewport().style.cursor = 'wait';
                              that.requesting = true;
                              that.requestInformation();
                        }
            } else {
                that.previousPosition = null;
            }
        } else {
            //Pas la même position
            that.requesting = false;
            that.removeToolTips(that);
        }
    },

    onClick: function (e) {
        if (!Descartes._activeClickToolTip) {
            return;
        }
        this.olMap.getViewport().style.cursor = 'wait';
        this.currentPosition = e.pixel;
        this.tooltipPositionCoordinate = e.coordinate;
        this.requesting = true;
        this.requestInformation();
        this.requesting = false;
    },

    requestInformation: function (e) {
        this.mouseOverToolTip = false;
        this.currentPosition[0] = parseInt(this.currentPosition[0], 10);
        this.currentPosition[1] = parseInt(this.currentPosition[1], 10);
        var fluxPixel = "#X=" + this.currentPosition[0] + "#Y=" + this.currentPosition[1];
        if (!_.isNil(this.olMap) && this.toolTipLayers.length !== 0) {
            var layers = [];
            var attributesAliasSubstitution = {};
            for (var iLayer = 0; iLayer < this.toolTipLayers.length; iLayer++) {
                if (this.toolTipLayers[iLayer].layer.isQueryable()) {
                    layers.push(this.toolTipLayers[iLayer].layer);
                    var attributesAlias = this.toolTipLayers[iLayer].layer.getAttributesAlias();
                    if (!_.isNil(attributesAlias)) {
                        attributesAliasSubstitution[this.toolTipLayers[iLayer].layer.id] = this.toolTipLayers[iLayer].layer.getAttributesAlias();
                        this._changeToolTipFieldsByAlias(this.toolTipLayers[iLayer], this.toolTipLayers[iLayer].layer.getAttributesAlias());
                    }
                }
            }
            var fluxInfos = ExternalCallsUtils.writeQueryPostBody(ExternalCallsUtilsConstants.WMS_SERVICE, layers, this.olMap);
            if (!_.isNil(fluxInfos)) {
                var requestParams = {
                    distantService: Descartes.FEATUREINFO_SERVER,
                    infos: fluxInfos,
                    pixelMask: fluxPixel
                };

                this.action = new AjaxSelection(this.resultUiParams, requestParams, this.olMap, {
                    toolTipLayers: this.toolTipLayers,
                    view: ToolTipContainer,
                    attributesAliasSubstitution: attributesAliasSubstitution
                });
                this.action.renderer.events.register('showToolTip', this, this.showToolTip);
                this.action.renderer.events.register('mouseEnterToolTip', this, this.mouseEnterToolTip);
                this.action.renderer.events.register('mouseLeaveToolTip', this, this.mouseLeaveToolTip);
            } else {
                this.olMap.getViewport().style.cursor = '';
            }
        } else {
           this.olMap.getViewport().style.cursor = '';
        }
    },

    showToolTip: function (e) {
        this.olMap.getViewport().style.cursor = '';
        var toAdd = e.data;
        var that = this;
        _.each(toAdd, function (overlay) {
            that.overlays.push(overlay);
            that.olMap.addOverlay(overlay);
            overlay.setPosition(that.tooltipPositionCoordinate);
            setTimeout(that.removeToolTip.bind(that, overlay), that.options.displayDelay, that);
        });
        this.action.renderer.events.unregister('showToolTip', this.showToolTip);
    },

    mouseEnterToolTip: function (e) {
        this.mouseOverToolTip = true;
    },

    mouseLeaveToolTip: function (e) {
        this.mouseOverToolTip = false;
        if (this.options.displayOnClick) {
            this.removeToolTips(this);
        }
    },

    removeToolTip: function (overlay) {
        if (!this.mouseOverToolTip) {
             if (overlay) {
                 this.olMap.removeOverlay(overlay);
                 this.createDiv();
             }
             this.mouseOverToolTip = false;
             this.action.renderer.events.unregister('mouseEnterToolTip', this.mouseEnterToolTip);
             this.action.renderer.events.unregister('mouseLeaveToolTip', this.mouseLeaveToolTip);
       }
    },

    removeToolTips: function (that) {
       if (!that.mouseOverToolTip) {
            _.each(that.overlays, function (overlay) {
                that.olMap.removeOverlay(overlay);
           });
           if (that.action) {
              that.action.renderer.events.unregister('mouseEnterToolTip', that.mouseEnterToolTip);
              that.action.renderer.events.unregister('mouseLeaveToolTip', that.mouseLeaveToolTip);
           }
           that.mouseOverToolTip = false;
           that.overlays = [];
           that.createDiv();
        }
	},

    /**
     * Methode: removeToolTipLayers
     * Supprime un ensemble de couches du gestionnaire d'info-bulles.
     *
     * Param�tres:
     * indexes - {Array(Integer)} Index des couches � supprimer.
     */
    removeToolTipLayers: function (indexes) {
        indexes = indexes.sort(function (a, b) {
            return b - a;
        });
        for (var i = 0, len = indexes.length; i < len; i++) {
            this.toolTipLayers.splice(indexes[i], 1);
        }
    },

    replaceSpecialChar: function (toolTipLayers) {
        var layers = toolTipLayers;
        for (var i = 0, len = layers.length; i < len; i++) {
            for (var j = 0, jlen = layers[i].fields.length; j < jlen; j++) {
                layers[i].fields[j] = layers[i].fields[j].replace(/[-:_\.]/g, " ");
            }
        }
        return layers;
    },
    _changeToolTipFieldsByAlias: function (toolTipLayer, alias) {
        var newFields = toolTipLayer.fields;
        var aliasNames = Object.keys(alias);
        for (var i = 0, len = newFields.length; i < len; i++) {
           if (aliasNames.indexOf(newFields[i]) !== -1) {
              newFields[i] = alias[newFields[i]];
           }
        }
        return newFields;
    },
    CLASS_NAME: 'Descartes.Info.ToolTip'
});


module.exports = Class;
