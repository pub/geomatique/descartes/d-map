/* global MODE, Descartes */

var _ = require('lodash');
var log = require('loglevel');
var ol = require('openlayers');

var Utils = require('../Utils/DescartesUtils');

var defaultSymbolizers = require('../Symbolizers');

require('./css/ToolTip.css');

/**
 * Class: Descartes.Info.SelectToolTip
 * Classe définissant une info-bulle s'affichant en cas de sélection d'un objet.
 *
 */
var Class = Utils.Class({

    /**
     * Propriete: selectToolTipLayers
     * {Array(Object)} Tableau d'objets JSON représentant les couches prises en compte par l'info-bulle, sous la forme {layer:Descartes.Layer, fields:String[]}
     */
    selectToolTipLayers: null,

    overlays: [],

    /**
     * Constructeur: Descartes.Info.ToolTip
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'info-bulle.
     * olMap - {ol.Map} Carte OpenLayers concernée.
     * selectToolTipLayers - {Array(Object)} Tableau d'objets JSON représentant les couches prises en compte par l'info-bulle, sous la forme {layer:Descartes.Layer, fields:String[]}
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     */
    initialize: function (olMap, selectToolTipLayers, options) {
        this.selectToolTipLayers = selectToolTipLayers;
        this.olMap = olMap;

        var olStyles = defaultSymbolizers.getOlStyle(defaultSymbolizers.Descartes_Symbolizers_SelectToolTip);
        this.olMap.on('pointermove', this.changeCursorPointerOnFeature, this);

        var that = this;
        this.selectInteraction = new ol.interaction.Select({
            //toggleCondition: ol.events.condition.singleClick,
            condition: ol.events.condition.singleClick,
            hitTolerance: 5,
            style: function (feature, resolution) {
                var featureStyleFunction = feature.getStyleFunction();
                if (featureStyleFunction) {
                    return featureStyleFunction.call(feature, resolution);
                } else {
                    var style = olStyles[feature.getGeometry().getType()];
                    return style;
                 }
            },
            filter: function (feature, layer) {
              for (var i = 0, len = that.selectToolTipLayers.length; i < len; i++) {
                  if (layer === that.selectToolTipLayers[i].layer.OL_layers[0]) {
                      feature.dSelectTooltipFields = that.selectToolTipLayers[i].fields;
                      if (that.selectToolTipLayers[i].layer.attributes && that.selectToolTipLayers[i].layer.attributes.attributesAlias) {
                         feature.dSelectTooltipAlias = that.selectToolTipLayers[i].layer.getAttributesAlias();
                      }
                      return true;
                  }
              }
              return false;
            }

        });

        this.selectInteraction.on('select', function (evt) {
            if (evt.deselected.length > 0) {
                that.clearOverlays();
            }
            if (evt.selected.length > 0) {
                this.feature = evt.selected[0];

                that.clearOverlays();

                var element = this.createContentPopup();

                var extent = this.feature.getGeometry().getExtent();
                var position = ol.extent.getTopRight(extent);

                this.popup = new ol.Overlay(({
                    id: this.feature.get('id') + '_doverlay',
                    element: element,
                    autoPan: true
                }));

                that.olMap.addOverlay(this.popup);
                this.popup.setPosition(position);
            }
        }.bind(this));

        this.olMap.addInteraction(this.selectInteraction);

    },

    clearOverlays: function () {
        var that = this;
        this.olMap.getOverlays().forEach(function (overlay) {
            if (overlay.getId().indexOf(that.id) !== -1) {
                that.olMap.removeOverlay(overlay);
            }
        });
    },

    createContentPopup: function () {
        var that = this;
        var element = document.createElement('div');
        element.setAttribute('id', 'localpopup');
        element.className = 'Descartes-popup';

        var text = '';
        if (this.feature.dSelectTooltipFields) {
            for (var i = 0, len = this.feature.dSelectTooltipFields.length; i < len; i++) {
                var filedText = this.feature.dSelectTooltipFields[i];
                if (this.feature.dSelectTooltipAlias && this.feature.dSelectTooltipAlias[filedText] && this.feature.dSelectTooltipAlias[filedText] !== "") {
                   filedText = this.feature.dSelectTooltipAlias[filedText];
                }
                text += filedText;
                text += ": ";
                text += this.feature.get(this.feature.dSelectTooltipFields[i]);
                text += "</br>";
               }
        }

        var closer = document.createElement('a');
        closer.setAttribute('href', '#');
        closer.setAttribute('id', 'localpopup-closer');
        closer.className = 'Descartes-popup-closer';
        closer.onclick = function () {
            that.olMap.removeOverlay(that.popup);
            that.selectInteraction.getFeatures().clear();
            return false;
        };
        element.appendChild(closer);

        var content = document.createElement('div');
        content.setAttribute('id', 'localpopup-content');
        content.innerHTML = text;

        element.appendChild(content);
        return element;
    },

    removeSelectToolTipLayers: function (indexes) {
        indexes = indexes.sort(function (a, b) {
            return b - a;
        });
        for (var i = 0, len = indexes.length; i < len; i++) {
            this.selectToolTipLayers.splice(indexes[i], 1);
        }
    },

    changeCursorPointerOnFeature: function (evt) {
        var that = this;
        var hit = this.olMap.hasFeatureAtPixel(evt.pixel, {
            layerFilter: function (aLayer) {
                for (var i = 0, len = that.selectToolTipLayers.length; i < len; i++) {
                    if (aLayer === that.selectToolTipLayers[i].layer.OL_layers[0]) {
                        return true;
                    }
                }
                return false;
            }
        });
        this.olMap.getTargetElement().style.cursor = (hit ? 'pointer' : '');
    },

    CLASS_NAME: 'Descartes.Info.SelectToolTip'
});


module.exports = Class;
