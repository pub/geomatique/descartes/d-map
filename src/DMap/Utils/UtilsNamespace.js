/* global MODE */

var _ = require('lodash');
var Utils = require('./DescartesUtils');
var DescartesEvent = require('./Events/DescartesEvent');
var EventManager = require('./Events/EventManager');



var namespace = {
    DescartesEvent: DescartesEvent,
    EventManager: EventManager
};
_.extend(Utils, namespace);

module.exports = Utils;
