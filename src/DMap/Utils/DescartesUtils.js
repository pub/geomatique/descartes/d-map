var _ = require('lodash');
var Messages = require('../Messages');
var ol = require('openlayers');
var LayerConstants = require('../Model/LayerConstants');
var MapConstants = require('../Map/MapConstants');
var DMapConstants = require('../DMapConstants');

/**
 * Class: Descartes.Utils
 * Classe offrant des méthodes statiques utilitaires cartographiques ou systèmes.
 */
var utils = {

    /**
     * Staticpropriete: projs
     * {Array(String[2]} Tableau stockant les associations 'nom/code' des projections
     */
    projs: [
        ['EPSG:2154', 'RGF93 - Lambert 93', 2],
        ['EPSG:27581', 'Lambert I Carto', 2],
        ['EPSG:27582', 'Lambert II Carto', 2],
        ['EPSG:27583', 'Lambert III Carto', 2],
        ['EPSG:27584', 'Lambert IV Carto', 2],
        ['EPSG:27591', 'Lambert I Nord', 2],
        ['EPSG:27592', 'Lambert II Centre', 2],
        ['EPSG:27593', 'Lambert III Sud', 2],
        ['EPSG:27594', 'Lambert IV Corse', 2],
        ['EPSG:23030', 'ED50 - UTM Zone 30 Nord', 2],
        ['EPSG:23031', 'ED50 - UTM Zone 31 Nord', 2],
        ['EPSG:23032', 'ED50 - UTM Zone 32 Nord', 2],
        ['EPSG:32630', 'WGS84 - UTM Zone 30 Nord', 2],
        ['EPSG:32631', 'WGS84 - UTM Zone 31 Nord', 2],
        ['EPSG:32632', 'WGS84 - UTM Zone 32 Nord', 2],
        ['EPSG:3042', 'ETRS89 - TM30', 2],
        ['EPSG:3043', 'ETRS89 - TM31', 2],
        ['EPSG:3044', 'ETRS89 - TM32', 2],
        ['EPSG:3034', 'ETRS89 - LCC', 2],
        ['EPSG:3035', 'ETRS89 - LAEA', 2],
        ['EPSG:3296', 'RGPF UTM 5S (Polynésie Française)', 2],
        ['EPSG:3297', 'RGPF UTM 6S (Polynésie Française)', 2],
        ['EPSG:3298', 'RGPF UTM 7S (Polynésie Française)', 2],
        ['EPSG:3299', 'RGPF UTM 8S (Polynésie Française)', 2],
        ['EPSG:3395', 'WGS84 - World Mercator', 2],
        ['EPSG:3857', 'Spherical Mercator', 2],
        ['EPSG:3942', 'RGF93 - Conique conforme Zone 1', 2],
        ['EPSG:3943', 'RGF93 - Conique conforme Zone 2', 2],
        ['EPSG:3944', 'RGF93 - Conique conforme Zone 3', 2],
        ['EPSG:3945', 'RGF93 - Conique conforme Zone 4', 2],
        ['EPSG:3946', 'RGF93 - Conique conforme Zone 5', 2],
        ['EPSG:3947', 'RGF93 - Conique conforme Zone 6', 2],
        ['EPSG:3948', 'RGF93 - Conique conforme Zone 7', 2],
        ['EPSG:3949', 'RGF93 - Conique conforme Zone 8', 2],
        ['EPSG:3950', 'RGF93 - Conique conforme Zone 9', 2],
        ['EPSG:32620', 'WGS84 - UTM Zone 20 Nord', 2],
        ['EPSG:4559', 'RRAF 1991 - UTM Zone 20 Nord', 2],
        ['EPSG:2973', 'Fort Desaix - UTM Zone 20 Nord', 2],
        ['EPSG:2969', 'Fort Marigot - UTM Zone 20 Nord', 2],
        ['EPSG:2970', 'Saint Anne - UTM Zone 20 Nord', 2],
        ['EPSG:32622', 'WGS84 - UTM Zone 22 Nord', 2],
        ['EPSG:2972', 'RGFG95 - UTM Zone 22 Nord', 2],
        ['EPSG:2971', 'CSG 67 - UTM Zone 22 Nord', 2],
        ['EPSG:32740', 'WGS84 - UTM Zone 40 Sud', 2],
        ['EPSG:2975', 'RGR92 - UTM Zone 40 Sud', 2],
        ['EPSG:2990', 'Piton des neiges', 2],
        ['EPSG:32738', 'WGS84 - UTM Zone 38 Sud', 2],
        ['EPSG:2980', 'Combani 195 - UTM Zone 38 Sud', 2],
        ['EPSG:4326', 'WGS84 - Lonlat', 6],
        ['EPSG:4326-DMS', 'WGS84 - Lonlat (DMS)', 2],
        ['EPSG:4326-DM', 'WGS84 - Lonlat (DM)', 4],
        ['EPSG:4171', 'RGF93', 6],
        ['EPSG:4171-DMS', 'RGF93 (DMS)', 2],
        ['EPSG:4171-DM', 'RGF93 (DM)', 4],
        ['EPSG:4258', 'ETRS89', 6],
        ['EPSG:4258-DMS', 'ETRS89 (DMS)', 2],
        ['EPSG:4258-DM', 'ETRS89 (DM)', 4],
        ['EPSG:4275', 'NTF', 2],
        ['EPSG:4467', 'RGSPM06U UTM 21N (St-Pierre-et-Miquelon)', 2],
        ['EPSG:4471', 'RGM04 UTM 38S (Mayotte)', 2],
        ['EPSG:4807', 'NTF (Paris)', 2],
        ['EPSG:5490', 'RGAF09 UTM 20N (Martinique/Guadeloupe)', 2],
        ['EPSG:27561', 'NTF (Paris) - Lambert Nord France', 2],
        ['EPSG:27562', 'NTF (Paris) - Lambert Centre France', 2],
        ['EPSG:27563', 'NTF (Paris) - Lambert Sud France', 2],
        ['EPSG:27564', 'NTF (Paris) - Lambert Corse', 2],
        ['EPSG:27571', 'NTF (Paris) - Lambert zone I', 2],
        ['EPSG:27572', 'NTF (Paris) - Lambert zone II', 2],
        ['EPSG:27573', 'NTF (Paris) - Lambert zone III', 2],
        ['EPSG:27574', 'NTF (Paris) - Lambert zone IV', 2],
        ['IGNF:GEOPORTALATF', 'Geoportail - Antilles francaises', 2],
        ['IGNF:GEOPORTALFXX', 'Geoportail - France metropolitaine', 2],
        ['IGNF:GEOPORTALGUF', 'Geoportail - Guyane', 2],
        ['IGNF:GEOPORTALMYT', 'Geoportail - Mayotte', 2],
        ['IGNF:GEOPORTALREU', 'Geoportail - Reunion et dependances', 2]
    ],
    /**
     * Constante: MAP_EXTEND_RATIO
     * {Float} Pourcentage d'étirement servant pour étendre une emprise.
     */
    MAP_EXTEND_RATIO: 0.1,
    /**
     * Constante: DISPLAY_MAP_UNITS
     * {String} Unité de mesure principale (= 'm').
     */
    DISPLAY_MAP_UNITS: 'm',
    /**
     * Constante: DISPLAY_MAP_SUPRA_UNITS
     * {String} Unité de mesure supérieure (= 'km').
     */
    DISPLAY_MAP_SUPRA_UNITS: 'km',
    /**
     * Constante: DISPLAY_MAP_FACTOR_UNITS
     * {Float} Rapport entre l'unité de mesure supérieure et l'unité de mesure principale (= 1000).
     */
    DISPLAY_MAP_FACTOR_UNITS: 1000,

    /**
     * Constant: DOTS_PER_INCH
     * Provient de Descartes V4
     */
    DOTS_PER_INCH: 25.4 / 0.28, //90.7142857142857,

    /**
     * Provient de Descartes V4
     */
    INCHES_PER_UNIT: {
        'm': 100 / 2.54, //39.370067740157
        'dd': 4374754,
        'degrees': 4374754
    },

    DEFAULT_FEATURE_GEOMETRY_NAME: 'the_geom',

    DEFAULT_FEATURE_PREFIX: 'feature',

    /**
     * Constant: URL_SPLIT_REGEX
     */
    URL_SPLIT_REGEX: /([^:]*:)\/\/([^:]*:?[^@]*@)?([^:\/\?]*):?([^\/\?]*)/,

    COOKIE_DATE_EXPIRATION: new Date(2035, 11, 11),

    Class: function () {
        var len = arguments.length;
        var P = arguments[0];
        var F = arguments[len - 1];

        var C = _.isFunction(F.initialize) ?
                F.initialize :
                function () {
                    P.prototype.initialize.apply(this, arguments);
                };

        if (len > 1) {
            var newArgs = [C, P].concat(
                    Array.prototype.slice.call(arguments).slice(1, len - 1), F);
            this.inherit.apply(null, newArgs);
        } else {
            C.prototype = F;
        }
        return C;
    },
    inherit: function (C, P) {
        var F = function () {};
        F.prototype = P.prototype;
        C.prototype = new F();
        var i, l, o;
        for (i = 2, l = arguments.length; i < l; i++) {
            o = arguments[i];
            if (_.isFunction(o)) {
                o = o.prototype;
            }
            _.extend(C.prototype, o);
        }
    },
    hasClass: function (element, name) {
        var names = element.className;
        return (!!names && new RegExp('(^|\\s)' + name + '(\\s|$)').test(names));
    },
    addClass: function (element, name) {
        if (!this.hasClass(element, name)) {
            element.className += (element.className ? ' ' : '') + name;
        }
        return element;
    },
    removeClass: function (element, name) {
        var names = element.className;
        if (names) {
            element.className = _.trim(
                    names.replace(
                            new RegExp('(^|\\s+)' + name + '(\\s+|$)'), ' ')
                    );
        }
        return element;
    },
    getDiv: function (param) {
        if (typeof param === 'string') {
            return document.getElementById(param);
        } else {
            return param;
        }
    },
    /**
     * Staticmethode: extendBounds
     * Etend une emprise selon une taille de carte et un éventuel ratio.
     *
     * :
     * Une première phase consiste à étirer l'emprise pour la rendre homothétique à la carte.
     *
     * :
     * Une seconde phase consiste à agrandir homothétiquement l'emprise selon un ratio.
     *
     * Paramêtres:
     * bounds - {ol.Extent} Emprise à étendre.
     * mapSize - {ol.Size} Taille de la carte servant à calculer la première homothétie.
     * ratio - {Float} Pourcentage d'étirement servant à calculer la seconde homothétie (vaut <Descartes.Utils.MAP_EXTEND_RATIO> par défaut).
     *
     * Retour:
     * {Array} L'emprise étendue.
     */
    extendBounds: function (bounds, mapSize, ratio) {
        var mapRatio = mapSize[0] / mapSize[1];
        var xMin = parseFloat(bounds[0]);
        var yMin = parseFloat(bounds[1]);
        var xMax = parseFloat(bounds[2]);
        var yMax = parseFloat(bounds[3]);

        var boundsRatio = (xMax - xMin) / (yMax - yMin);

        if (boundsRatio > mapRatio) {
            var deltaY = (xMax - xMin) / mapRatio - (yMax - yMin);
            yMin -= deltaY / 2;
            yMax += deltaY / 2;
        } else if (boundsRatio < mapRatio) {
            var deltaX = (yMax - yMin) * mapRatio - (xMax - xMin);
            xMin -= deltaX / 2;
            xMax += deltaX / 2;
        }

        if (!ratio) {
            ratio = this.MAP_EXTEND_RATIO;
        }
        var newXmin = xMin - (xMax - xMin) * ratio;
        var newXmax = xMax + (xMax - xMin) * ratio;
        var newYmin = yMin - (yMax - yMin) * ratio;
        var newYmax = yMax + (yMax - yMin) * ratio;
        return [newXmin, newYmin, newXmax, newYmax];
    },
    /**
     * Staticmethode: readableScale
     * Génère une échelle métrique sous forme de texte.
     *
     * :
     * Le texte génère ne comporte pas de décimales et comporte les éventuels séparateurs de milliers nécessaires.
     *
     * Paramêtres:
     * scale - {Float} Dénominateur de l'échelle
     *
     * Retour:
     * {String} L'échelle métrique représentée sous forme de fraction.
     */
    readableScale: function (scale) {
        var textScale = '';
        var numScale = Math.round(scale).toString();
        var numDigit = 0;
        for (var i = numScale.length - 1; i >= 0; i--) {
            if (numDigit === 3) {
                textScale = '.' + textScale;
                numDigit = 0;
            }
            textScale = numScale.charAt(i) + textScale;
            numDigit++;
        }
        return '1/' + textScale;
    },
    getWindowWidth: function () {
        var windowWidth = 0;
        if (typeof (window.innerWidth) === 'number') {
            windowWidth = window.innerWidth;
        } else if (document.documentElement && document.documentElement.clientWidth) {
            windowWidth = document.documentElement.clientWidth;
        } else if (document.body && document.body.clientWidth) {
            windowWidth = document.body.clientWidth;
        }
        return windowWidth;
    },
    getWindowHeight: function () {
        var windowHeight = 0;
        if (typeof (window.innerHeight) === 'number') {
            windowHeight = window.innerHeight;
        } else if (document.documentElement && document.documentElement.clientHeight) {
            windowHeight = document.documentElement.clientHeight;
        } else if (document.body && document.body.clientHeight) {
            windowHeight = document.body.clientHeight;
        }
        return windowHeight;
    },
    getScrollMaxX: function () {
        var scrollMaxX = 0;
        if (typeof (window.scrollMaxX) === 'number') {
            scrollMaxX = window.scrollMaxX;
        } else if (document.body && document.body.scrollWidth) {
            scrollMaxX = document.body.scrollWidth;
        } else if (document.documentElement && document.documentElement.scrollWidth) {
            scrollMaxX = document.documentElement.scrollWidth;
        }
        return scrollMaxX;
    },
    getScrollMaxY: function () {
        var scrollMaxY = 0;
        if (typeof (window.scrollMaxY) === 'number') {
            scrollMaxY = window.scrollMaxY;
        } else if (document.body && document.body.scrollHeight) {
            scrollMaxY = document.body.scrollHeight;
        } else if (document.documentElement && document.documentElement.scrollHeight) {
            scrollMaxY = document.documentElement.scrollHeight;
        }
        return scrollMaxY;
    },
    /**
     * Staticmethode: loadMessages
     * Fournit les messages utilisables par une classe.
     *
     * Paramètres:
     * className - {String} Nom de la classe.
     *
     * Retour:
     * {Object} Objet JSON stockant les messages utilisables par la classe.
     */
    loadMessages: function (className) {
        var messages = {};
        try {
            var rootPackageName = className.substring(0, className.indexOf('.'));
            //var messagesPart = eval(className.replace(rootPackageName + '.', rootPackageName + '_Messages_').replace(/\./g, '_'));
            var messagesPart = className.replace(rootPackageName + '.', rootPackageName + '_Messages_').replace(/\./g, '_');
            messagesPart = Messages[messagesPart];
            for (var message in messagesPart) {
                if (!_.isFunction(message)) {
                    messages[message] = messagesPart[message];
                }
            }
        } catch (e) {
            messages = null;
        }
        return messages;
    },
    createUniqueID: function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
        }
        return 'Descartes_' + s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
    },
    formatNumber: function (x, separator) {
        if (_.isNil(separator)) {
            separator = '.';
        }
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, separator);
    },
    formatScale: function (scale) {
        return '1/' + this.formatNumber(scale);
    },
    formatCurrentDate: function () {
        var date = new Date().toISOString();
        date = date.replaceAll("-", "");
        date = date.replaceAll(":", "");
        date = date.replaceAll(".", "");
        return date;
    },
    /**
     * Function: normalizeScale
     *
     * Parameters:
     * scale - {float}
     *
     * Returns:
     * {Float} A normalized scale value, in 1 / X format.
     *         This means that if a value less than one ( already 1/x) is passed
     *         in, it just returns scale directly. Otherwise, it returns
     *         1 / scale
     */
    normalizeScale: function (scale) {
        var normScale = (scale > 1.0) ? (1.0 / scale) : scale;
        return normScale;
    },
    /**
     * Function: getResolutionFromScale
     *
     * Parameters:
     * scale - {Float}
     * units - {String} Index into ol.proj.METERS_PER_UNIT hashtable.
     *                  Default is meters
     *
     * Returns:
     * {Float} The corresponding resolution given passed-in scale and unit
     *     parameters.  If the given scale is falsey, the returned resolution will
     *     be undefined.
     */
    getResolutionForScale: function (scale, units) {
        if (!_.isNil(scale)) {
            if (_.isNil(units)) {
                units = 'degrees';
            }
            var normScale = this.normalizeScale(scale);
            var ipu = this.INCHES_PER_UNIT[units];

            return 1 / (normScale * ipu * this.DOTS_PER_INCH);
        }
        return null;
    },
    /**
     * Function: getScaleFromResolution
     *
     * Parameters:
     * resolution - {Float}
     * units - {String} Index into INCHES_PER_UNIT hashtable.
     *                  Default is degrees
     *
     * Returns:
     * {Float} The corresponding scale given passed-in resolution and unit
     *         parameters.
     */
    getScaleFromResolution: function (resolution, units) {
        if (_.isNil(units)) {
            units = 'degrees';
        }
        var ipu = this.INCHES_PER_UNIT[units];

        return resolution * ipu * this.DOTS_PER_INCH;
    },
    appendNodeParsingForFormat: function (format) {
        for (var key in ol.format.WMSCapabilities.LAYER_PARSERS_) {
            ol.format.WMSCapabilities.LAYER_PARSERS_[key].SRS = ol.format.WMSCapabilities.LAYER_PARSERS_[key].CRS;
        }
    },
    /**
     * Staticmethode: convertUnits
     * Convertit une mesure de l'unité réelle de la map vers l'unité d'affichage.
     *
     * Paramètres:
     * number - {Float} La valeur de la mesure
     * mapUnits - {String} Unité de la map
     *
     * Retour:
     * {Float} La mesure dans l'unité d'affichage.
     */
    convertUnits: function (number, mapUnits) {
        return number * (ol.proj.METERS_PER_UNIT[mapUnits] / ol.proj.METERS_PER_UNIT[this.DISPLAY_MAP_UNITS]);
    },
    /**
     * Staticmethode: adaptUnits
     * Simplifie la valeur d'une mesure et la représente avec son unité.
     *
     * Seuls 3 chiffres significatifs sont pris en compte.
     *
     * L'unité choisie est la plus judicieuse entre l'unité principale et l'unité supérieure.
     *
     * Paramètres:
     * number - {Float} La valeur de la mesure.
     *
     * Retour:
     * {String} La mesure simplifiée accompagnée de son unité.
     */
    adaptUnits: function (number) {
        var units = this.DISPLAY_MAP_UNITS;
        if (number >= this.DISPLAY_MAP_FACTOR_UNITS) {
            number = number / this.DISPLAY_MAP_FACTOR_UNITS;
            units = this.DISPLAY_MAP_SUPRA_UNITS;
        }
        var nbAvant;
        if (number.toString().indexOf('.') === -1) {
            nbAvant = number.toString().length;
        } else {
            nbAvant = number.toString().indexOf('.');
        }
        var newNumber = '';
        if (nbAvant === 7) {
            newNumber = number.toString().substring(0, 6) + '0';
        }
        if (nbAvant === 6) {
            newNumber = number.toString().substring(0, 5) + '0';
        }
        if (nbAvant === 5) {
            newNumber = number.toString().substring(0, 4) + '0';
        }
        if (nbAvant === 4) {
            newNumber = number.toString().substring(0, 3) + '0';
        }
        if (nbAvant === 3) {
            newNumber = number.toString().substring(0, 3);
        }
        if (nbAvant === 2) {
            newNumber = number.toString().substring(0, 4);
        }
        if (nbAvant === 1) {
            newNumber = number.toString().substring(0, 4);
        }

        return(newNumber + units);
    },
    /**
     * Staticmethode: localizeNumber
     * Transforme un nombre en chaine de caractères selon le format français.
     *
     * :
     * Le séparateur décimal est la virgule.
     *
     * :
     * Le séparateur de milliers est le point.
     *
     * Paramètres:
     * nb - {Float} Le nombre à transformer.
     * dec - {Integer} Le nombre de décimales à prendre en compte.
     *
     * Retour:
     * {String} Le chaine de caractères selon le format français.
     */
    localizeNumber: function (nb, dec) {
        var text = '';
        var nbSplit = nb.toFixed(dec).split('.');
        var nbEn = nbSplit[0];
        var numDigit = 0;
        for (var i = nbEn.length - 1; i >= 0; i--) {
            if (numDigit === 3 && nbEn.charAt(i) !== '-') {
                text = '.' + text;
                numDigit = 0;
            }
            text = nbEn.charAt(i) + text;
            numDigit++;
        }
        if (dec === 0) {
            return text;
        } else {
            return text + ',' + nbSplit[1];
        }
    },
    coordinateToStringHDMS: function (coordinate, opt_fractionDigits, separator) {
		if (coordinate) {
			return this._degreesToStringHDMS('NS', coordinate[1], opt_fractionDigits).replace(".", ",") + separator + this._degreesToStringHDMS('EW', coordinate[0], opt_fractionDigits).replace(".", ",");
		} else {
			return '';
		}
    },
    _degreesToStringHDMS: function (hemispheres, degrees, opt_fractionDigits) {
		var normalizedDegrees = this.modulo(degrees + 180, 360) - 180;
		var x = Math.abs(3600 * normalizedDegrees);
		var dflPrecision = opt_fractionDigits || 0;
		var precision = Math.pow(10, dflPrecision);

		var deg = Math.floor(x / 3600);
		var min = Math.floor((x - deg * 3600) / 60);
		var sec = x - (deg * 3600) - (min * 60);
		sec = Math.ceil(sec * precision) / precision;

		if (sec >= 60) {
		sec = 0;
		min += 1;
		}

		if (min >= 60) {
		min = 0;
		deg += 1;
		}

		return deg + '\u00b0 ' + this._padNumber(min, 2) + '\u2032 ' + this._padNumber(sec, 2, dflPrecision) + '\u2033' + (normalizedDegrees == 0 ? '' : ' ' + hemispheres.charAt(normalizedDegrees < 0 ? 1 : 0));
	},
    coordinateToStringHDM: function (coordinate, opt_fractionDigits, separator) {
        if (coordinate) {
            return this._degreesToStringHDM('NS', coordinate[1], opt_fractionDigits).replace(".", ",") + separator + this._degreesToStringHDM('EW', coordinate[0], opt_fractionDigits).replace(".", ",");
        } else {
            return '';
        }
    },
    _degreesToStringHDM: function (hemispheres, degrees, opt_fractionDigits) {
        var normalizedDegrees = this.modulo(degrees + 180, 360) - 180;
        var x = Math.abs(3600 * normalizedDegrees);
        var dflPrecision = opt_fractionDigits || 0;
        var precision = Math.pow(10, dflPrecision);

        var deg = Math.floor(x / 3600);
        var min = (x - deg * 3600) / 60;
        min = Math.ceil(min * precision) / precision;

        if (min >= 60) {
           min = 0;
           deg += 1;
        }

        return deg + '\u00b0 ' + this._padNumber(min, 2, dflPrecision) + '\u2032 ' + (normalizedDegrees == 0 ? '' : ' ' + hemispheres.charAt(normalizedDegrees < 0 ? 1 : 0));
	},
    coordinateToStringHD: function (coordinate, opt_fractionDigits, separator) {
        if (coordinate) {
            return this._degreesToStringHD('NS', coordinate[1], opt_fractionDigits).replace(".", ",") + separator + this._degreesToStringHD('EW', coordinate[0], opt_fractionDigits).replace(".", ",");
        } else {
            return '';
        }
    },
    _degreesToStringHD: function (hemispheres, degrees, opt_fractionDigits) {
        var normalizedDegrees = this.modulo(degrees + 180, 360) - 180;
        var x = Math.abs(3600 * normalizedDegrees);
        var dflPrecision = opt_fractionDigits || 0;
        var precision = Math.pow(10, dflPrecision);

        var deg = x / 3600;

        return this._padNumber(deg, 1, dflPrecision) + '\u00b0 ' + (normalizedDegrees == 0 ? '' : ' ' + hemispheres.charAt(normalizedDegrees < 0 ? 1 : 0));
	},
    _padNumber: function (number, width, opt_precision) {
        var numberString = opt_precision !== undefined ? number.toFixed(opt_precision) : '' + number;
        var decimal = numberString.indexOf('.');
        decimal = decimal === -1 ? numberString.length : decimal;
        return decimal > width ? numberString : new Array(1 + width - decimal).join('0') + numberString;
    },
    /**
     * Staticmethode: getProjectionName
     * Fournit le nom d'une projection définie selon son code.
     *
     * Paramètres:
     * projCode - {String} Code de la projection exprimé selon le patron 'XXX:YYY' (ex: 'EPSG:2154').
     *
     * Retour:
     * {String} Le nom de la projection recherchée.
     */
    getProjectionName: function (projCode) {
        var projText = '';
        for (var i = 0; i < this.projs.length; i++) {
            if (this.projs[i][0] === projCode) {
                projText = this.projs[i][1];
                break;
            }
        }
        return projText;
    },
    getProjectionPrecision: function (projCode) {
        var projText = '';
        for (var i = 0; i < this.projs.length; i++) {
            if (this.projs[i][0] === projCode) {
                projText = this.projs[i][2];
                break;
            }
        }
        return projText;
    },
    computeFormvalues: function (formArray) {
        for (var i = 0; i < formArray.length; i++) {
            var value = formArray[i]['value'];
            formArray[i]['value'] = this.computeFormValue(value);
        }
    },
    computeFormValue: function (value) {
        if (value == 'true' || value == 'false') {
            //boolean convertion
            return (value == 'true');
        } else if (value === 'null') {
            //null convertion
            return null;
        }
        return value;
    },
    serializeFormArrayToJson: function (formArray) {
        var json = {};
        for (var i = 0; i < formArray.length; i++) {
            var value = formArray[i]['value'];
            json[formArray[i]['name']] = this.computeFormValue(value);
        }
        return json;
    },
    /**
     * Staticmethode: acceptCookies
     * Vérifie l'acceptation des cookies.
     *
     * Retour:
     * {Boolean} acceptation ou non des cookies.
     */
    acceptCookies: function () {
        var acceptCookies = true;
        if (document.cookie.length === 0) {
            document.cookie = 'printing=allowed; expires=' + this.COOKIE_DATE_EXPIRATION.toGMTString() + ';';
            if (document.cookie.length === 0) {
                alert(Messages.Descartes_COOKIES_DISABLED);
                acceptCookies = false;
            }
        }
        return acceptCookies;
    },
    /**
     * Method: toBBOX (Source OL2 Class Bounds)
     * Returns a boundingbox-string representation of the bounds object.
     *
     * Parameters:
     * decimal - {Integer} How many significant digits in the bbox coords?
     *                     Default is 6
     * reverseAxisOrder - {Boolean} Should we reverse the axis order?
     *
     * Returns:
     * {String} Simple String representation of bounds object.
     *          (e.g. "5,42,10,45")
     */
    toBBOX: function (bounds, decimal, reverseAxisOrder) {
        if (_.isNil(decimal)) {
            decimal = 6;
        }
        var mult = Math.pow(10, decimal);
        var xmin = Math.round(bounds[0] * mult) / mult;
        var ymin = Math.round(bounds[1] * mult) / mult;
        var xmax = Math.round(bounds[2] * mult) / mult;
        var ymax = Math.round(bounds[3] * mult) / mult;
        if (reverseAxisOrder === true) {
            return ymin + ',' + xmin + ',' + ymax + ',' + xmax;
        } else {
            return xmin + ',' + ymin + ',' + xmax + ',' + ymax;
        }
    },
    getUrlParam: function (url, name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(url);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    },
    removeUrlParam: function (url, name) {
        var urlparts = url.split('?');
        if (urlparts.length >= 2) {
            var prefix = encodeURIComponent(name) + '=';
            var pars = urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (var i = pars.length; i-- > 0; ) { // eslint-disable-line space-in-parens
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
        }
        return url;

    },
    download: function (filename, text) {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    },

    htmlToElement: function (html) {
        var template = document.createElement('template');
        template.innerHTML = html;
        if (_.isNil(template.content)) {
            //IE
            return template.children[0];
        } else {
            //autre navigateur
            if (!_.isNil(template.content.children)) {
                return template.content.children[0];
            } else {
                //Edge
                return template.content.childNodes[1];
            }
        }
    },

    htmlToElements: function (html) {
        var template = document.createElement('template');
        template.innerHTML = html;
        if (_.isNil(template.content)) {
            //IE
            return template.children;
        } else {
            //autre navigateur
            return template.content.children;
        }
    },
    getPolygonLength: function (polygon, projection) {
        var coordinates = polygon.getCoordinates();
        var length = 0;

        coordinates = coordinates[0];
        for (var i = 0; i < coordinates.length; i++) {
            var coordinate = coordinates[i];
            var next = coordinates[i + 1];
            if (!_.isNil(next)) {
                var line = new ol.geom.LineString([coordinate, next]);
                length += ol.Sphere.getLength(line, {
                    projection: projection
                });
            }
        }

        return length;
    },
    encode: function (obj) {
        var str = '';
        for (var key in obj) {
            if (!_.isEmpty(str)) {
                str += '&';
            }
            str += key + '=' + encodeURIComponent(obj[key]);
        }
        return str;
    },
    makeSameOrigin: function (url, proxy) {
        var sameOrigin = url.indexOf('http') !== 0;
        var urlParts = !sameOrigin && url.match(this.URL_SPLIT_REGEX);
        if (urlParts) {
            var location = window.location;
            sameOrigin = urlParts[1] == location.protocol && urlParts[3] == location.hostname;
            var uPort = urlParts[4], lPort = location.port;
            if (uPort != 80 && uPort != "" || lPort != "80" && lPort != "") {
                sameOrigin = sameOrigin && uPort == lPort;
            }
        }
        if (!sameOrigin) {
            if (proxy) {
                if (typeof proxy == "function") {
                    url = proxy(url);
                } else {
                    url = proxy + encodeURIComponent(url);
                }
            }
        }
        return url;
    },
    getPixelInMapFromFeature: function (map, feature) {
        var geometry = feature.getGeometry();
        var coordinate = geometry.getCoordinates();
        var pixel = map.getPixelFromCoordinate(coordinate);
        pixel[0] = parseInt(pixel[0], 10);
        pixel[1] = parseInt(pixel[1], 10);
        return pixel;
    },
    extend: function (destination, source) {
        return _.extend(destination, source);
    },
    addCSSRule: function (sheet, selector, rules, index) {
        if ('insertRule' in sheet) {
            sheet.insertRule(selector + "{" + rules + "}", index);
        } else if ('addRule' in sheet) {
            sheet.addRule(selector, rules, index);
        }
    },

    bindCallBack: function (func, object) {
        // create a reference to all arguments past the second one
        var args = Array.prototype.slice.apply(arguments, [2]);
        return function () {
            // Push on any additional arguments from the actual function call.
            // These will come after those sent to the bind call.
            var newArgs = args.concat(
                    Array.prototype.slice.apply(arguments, [0])
                    );
            return func.apply(object, newArgs);
        };
    },
    getMapExtent: function (olMap) {
        return olMap.getView().calculateExtent(olMap.getSize());
    },

    getSubGeom: function (multiGeom) {
        if (multiGeom instanceof ol.geom.MultiPoint) {
            return multiGeom.getPoints();
        } else if (multiGeom instanceof ol.geom.MultiLineString) {
            return multiGeom.getLineStrings();
        } else if (multiGeom instanceof ol.geom.MultiPolygon) {
            return multiGeom.getPolygons();
        }
        return null;
    },
    isMultipleGeometry: function (geom) {
        return geom instanceof ol.geom.MultiPolygon ||
                geom instanceof ol.geom.MultiPoint ||
                geom instanceof ol.geom.MultiLineString;
    },
    adaptationUrl: function (url) {
        if (!_.isEmpty(url)) {
            if (url.indexOf('?') === -1) {
                url += '?';
            } else if (url.indexOf('?') !== url.length - 1 &&
                    (url.indexOf('&') === -1 || url.indexOf('&') !== url.length - 1)) {
                url += '&';
            }
        }
        return url;
    },

    applyDefaults: function (to, from) {
        to = to || {};
        /*
         * FF/Windows < 2.0.0.13 reports "Illegal operation on WrappedNative
         * prototype object" when calling hawOwnProperty if the source object is an
         * instance of window.Event.
         */
        var fromIsEvt = typeof window.Event == "function" && from instanceof window.Event;

        for (var key in from) {
            if (to[key] === undefined ||
                (!fromIsEvt && from.hasOwnProperty && from.hasOwnProperty(key) && !to.hasOwnProperty(key))) {
                to[key] = from[key];
            }
        }
        /**
         * IE doesn't include the toString property when iterating over an object's
         * properties with the for(property in object) syntax.  Explicitly check if
         * the source has its own toString property.
         */
        if(!fromIsEvt && from && from.hasOwnProperty && from.hasOwnProperty('toString') && !to.hasOwnProperty('toString')) {
            to.toString = from.toString;
        }

        return to;
    },

    segmentLength: function (a, b) {
      return Math.sqrt((b[0] - a[0]) * (b[0] - a[0]) + (b[1] - a[1]) * (b[1] - a[1]));
    },

    isOnSegment: function (c, a, b) {
      var lengthAc = this.segmentLength(a, c);
      var lengthAb = this.segmentLength(a, b);
      var dot = ((c[0] - a[0]) * (b[0] - a[0]) + (c[1] - a[1]) * (b[1] - a[1])) / lengthAb;
      return Math.abs(lengthAc - dot) < 1e-6 && lengthAc < lengthAb;
    },
    checkPointOnGeometry: function (geom, point) {
        var ringCoords;
        if (geom.getType() === 'Polygon' || geom.getType() === 'MultiPolygon') {
            ringCoords = geom.getLinearRing().getCoordinates();
        } else if (geom.getType() === 'LineString' || geom.getType() === 'MultiLineString') {
            ringCoords = geom.getCoordinates();
        }
        var i, pointA, pointB;
        var isOnGeometry = false;
        for (i = 0; i < ringCoords.length; i++) {
          pointA = ringCoords[i];
          pointB = ringCoords[this.modulo(i + 1, ringCoords.length)];
          if (this.isOnSegment(point, pointA, pointB)) {
              isOnGeometry = true;
          }
        }
        return isOnGeometry;
	},
    selectMultiGeometry: function (feature, point) {
        var geom;
        var geometries;
        if (feature.getGeometry().getType() === 'MultiPolygon') {
            geometries = feature.getGeometry().getPolygons();
        }
        if (feature.getGeometry().getType() === 'MultiLineString') {
            geometries = feature.getGeometry().getLineStrings();
        }
        for (var i = 0; i < geometries.length; i++) {
            if (this.checkPointOnGeometry(geometries[i], point)) {
                geom = geometries[i];
            }
        }
        return geom;
	},
    modulo: function (a, b) {
      return ((a % b) + b) % b;
    },

    getPartialRingCoords: function (feature, startPoint, endPoint) {
      var geom = feature.getGeometry();
      var ringCoords;
      if (geom.getType() === 'MultiPolygon' || geom.getType() === 'MultiLineString') {
          geom = this.selectMultiGeometry(feature, startPoint);
      }
      if (geom.getType() === 'Polygon') {
          ringCoords = geom.getLinearRing().getCoordinates();
      } else if (geom.getType() === 'LineString') {
          ringCoords = geom.getCoordinates();
      }
      var cwCoordinates = [];
      var cwLength = 0;
      var ccwCoordinates = [];
      var ccwLength = 0;
      if (this.checkPointOnGeometry(geom, endPoint) && ringCoords) {
        var i, pointA, pointB, startSegmentIndex = -1;
        for (i = 0; i < ringCoords.length; i++) {
          pointA = ringCoords[i];
          pointB = ringCoords[this.modulo(i + 1, ringCoords.length)];
          // check if this is the start segment dot product
          if (this.isOnSegment(startPoint, pointA, pointB)) {
              startSegmentIndex = i;
              break;
          }
        }

        // build clockwise coordinates
        for (i = 0; i < ringCoords.length; i++) {
          pointA = i === 0 ? startPoint : ringCoords[this.modulo(i + startSegmentIndex, ringCoords.length)];
          pointB = ringCoords[this.modulo(i + startSegmentIndex + 1, ringCoords.length)];
          cwCoordinates.push(pointA);

          if (this.isOnSegment(endPoint, pointA, pointB)) {
            cwCoordinates.push(endPoint);
            cwLength += this.segmentLength(pointA, endPoint);
            break;
          } else {
            cwLength += this.segmentLength(pointA, pointB);
          }
        }

        // build counter-clockwise coordinates
        for (i = 0; i < ringCoords.length; i++) {
          pointA = ringCoords[this.modulo(startSegmentIndex - i, ringCoords.length)];
          pointB = i === 0 ? startPoint : ringCoords[this.modulo(startSegmentIndex - i + 1, ringCoords.length)];
          ccwCoordinates.push(pointB);

          if (this.isOnSegment(endPoint, pointA, pointB)) {
            ccwCoordinates.push(endPoint);
            ccwLength += this.segmentLength(endPoint, pointB);
            break;
          } else {
            ccwLength += this.segmentLength(pointA, pointB);
          }
        }
      } else {
         return [endPoint];
      }
      // keep the shortest path
      return ccwLength < cwLength ? ccwCoordinates : cwCoordinates;
    },

    adaptFluxWFSGetFeaturePost: function (flux, version) {
        var mFlux = flux;
        if (version === "1.0.0") {
            mFlux = mFlux.replace("xmlns=\"http://www.opengis.net/wfs\"", "xmlns=\"http://www.opengis.net/wfs\" outputFormat=\"gml2\"");
            mFlux = mFlux.replace(/1.1.0/g, "1.0.0");

            var flux1 = mFlux.split("<lowerCorner>");
            var flux2 = flux1[1].split("</lowerCorner");
            var coordinates = flux2[0].split(" ");
            var flux3 = flux2[1].split("<upperCorner>");
            var flux4 = flux3[1].split("</upperCorner>");
            var newCoordinates = "";
            coordinates = coordinates.concat(flux4[0].split(" "));
            for (var i = 0, len = coordinates.length ; i < len; i++) {
                newCoordinates += coordinates[i] + "," + coordinates[i + 1];
                if (i !== len - 2) {
                    newCoordinates += " ";
                }
                i++;
            }
            mFlux = flux1[0] + "<coordinates>" + newCoordinates + "</coordinates>" + flux4[1];
            mFlux = mFlux.replace(/Envelope/g, "Box");
        } else if (version === "2.0.0") {
            mFlux = mFlux.replace("xmlns=\"http://www.opengis.net/wfs\"", "xmlns:gml=\"http://www.opengis.net/gml/3.2\" outputFormat=\"gml3\"");
            mFlux = mFlux.replace(/1.1.0/g, "2.0.0");
            mFlux = mFlux.replace("typeName", "typeNames");
            mFlux = mFlux.replace(/Envelope/g, "gml:Envelope");
        }

        return mFlux;
    },
    /**
     * Methode: gotoFeatureBounds
     * Recadre la carte sur l'emprise d'un des objets sélectionné dans la vue affichant les données.
     *
     * Paramètres:
     * boundsAndScales - {Array} Coordonnées de l'emprise et plage d'échelle de visualisation de l'objet sous la forme
     * [[xMin, yMin, xMax, yMax], layerMinScale, layerMaxScale]].
     */
    gotoFeatureBounds: function (boundsAndScales, olMap) {
        if (!(boundsAndScales instanceof Array) && boundsAndScales.data) {
            //cas type_widget
            boundsAndScales = boundsAndScales.data;
        } else if (!(boundsAndScales instanceof Array)) {
            //cas type_popup, type_inplace
            boundsAndScales = eval(boundsAndScales);
        }
        var bounds = boundsAndScales[0];
        if (!(bounds instanceof Array) && bounds.data) {
            bounds = bounds.data;
        }
        var layerMinScale = boundsAndScales[1];
        var layerMaxScale = boundsAndScales[2];

        var xmin = bounds[0], ymin = bounds[1], xmax = bounds[2], ymax = bounds[3];

        var mapType = olMap.get('mapType');
        var constrainResolution = mapType === MapConstants.MAP_TYPES.DISCRETE;

        var extent = [xmin, ymin, xmax, ymax];
        if ((xmax - xmin) < DMapConstants.MIN_BBOX && (ymax - ymin) < DMapConstants.MIN_BBOX) {
            var newXmin = xmin + (xmax - xmin) / 2 - DMapConstants.MIN_BBOX / 2;
            var newXmax = xmin + (xmax - xmin) / 2 + DMapConstants.MIN_BBOX / 2;
            var newYmin = ymin + (ymax - ymin) / 2 - DMapConstants.MIN_BBOX / 2;
            var newYmax = ymin + (ymax - ymin) / 2 + DMapConstants.MIN_BBOX / 2;
            extent = [newXmin, newYmin, newXmax, newYmax];
        }
        olMap.getView().fit(extent, {
            constrainResolution: constrainResolution,
            minResolution: olMap.getView().getMinResolution(),
            maxResolution: olMap.getView().getMaxResolution()
        });

        var currentRes = olMap.getView().getResolution();
        var unit = olMap.getView().getProjection().getUnits();
        var newRes = currentRes;
        if (layerMinScale !== null) {
            var layerMinRes = this.getResolutionForScale(layerMinScale, unit);
            if (layerMinRes < currentRes) {
                newRes = layerMinRes;
            }
        }
        if (layerMaxScale !== null) {
            var layerMaxRes = this.getResolutionForScale(layerMaxScale, unit);
            if (layerMaxRes > currentRes) {
                newRes = layerMaxRes;
            }
        }

        if (newRes !== currentRes) {
            olMap.getView().setResolution(newRes);
        }

        return extent;
    }
};

module.exports = utils;
