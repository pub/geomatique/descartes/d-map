/* global Descartes */

var Utils = require('../Utils/DescartesUtils');

/*
 * Reprise du code d'OL2 OpenLayers.Size
 */
/**
 * Class: Descartes.Size
 * Classe permettant de créer un objet de type Size.
 */
var Size = Utils.Class({
    w: 0.0,

    h: 0.0,

    /**
     * Constructeur: Descartes.Size
     * Constructeur d'instance.
     *
     * Paramètres:
     * w - {Number} largeur
     * h - {Number} hauteur
     */
    initialize: function (w, h) {
        this.w = parseFloat(w);
        this.h = parseFloat(h);
    },

    toString: function () {
        return ('w=' + this.w + ',h=' + this.h);
    },

    clone: function () {
        return new Descartes.Size(this.w, this.h);
    },

    equals: function (sz) {
        var equals = false;
        if (sz != null) {
            equals = ((this.w == sz.w && this.h == sz.h) ||
                    (isNaN(this.w) && isNaN(this.h) && isNaN(sz.w) && isNaN(sz.h)));
        }
        return equals;
    },

    CLASS_NAME: 'Descartes.Size'
});

module.exports = Size;
