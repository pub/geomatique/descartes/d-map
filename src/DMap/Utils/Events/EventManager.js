var lodash = require('lodash');
var Utils = require('../DescartesUtils');
var DescartesEvent = require('./DescartesEvent');
/*
 * Reprise partielle de la classe de OpenLayers 2
 * OpenLayers.Events
 */
/**
 * Class: Descartes.Utils.EventManager
 * Classe permettant de gérer les événements.
 */
var EventManager = Utils.Class({
    _listeners: null,

    /**
     * Constructeur: Descartes.Utils.EventManager
     * Constructeur d'instance.
     */
    initialize: function () {
        this._listeners = {};
    },
    /**
     * Methode: register
     * Méthode d'abonnement à un événement.
     */
    register: function (type, scope, fn) {
        var listener = {
            fn: fn,
            scope: scope || window
        };
        if (!(this._listeners[type] instanceof Array)) {
            this._listeners[type] = [];
        }

        this._listeners[type].push(listener);
        return this._listeners[type];
    },
    /**
     * Methode: unregister
     * Méthode de désabonnement à un événement.
     */
    unregister: function (type, scope, fn) {
        if (arguments.length === 1 || lodash.isNil(fn)) { // remove all
            this._listeners[type] = [];
        } else {
            var listeners = this._listeners[type];
            if (listeners instanceof Array) {
                lodash.remove(listeners, function (listener) {
                    return listener.fn === fn;
                });
            }
        }

        return this._listeners[type];
    },
    remove: function (type, fn) {
        return this.unregister(type, fn);
    },
    /**
     * Methode: triggerEvent
     * Méthode d'envoi d'un événement.
     */
    triggerEvent: function (type, args) {
        var listeners = this._listeners[type];
        if (lodash.isArray(listeners)) {
            var data = [];
            if (lodash.isArray(args)) {
                data = data.concat(args);
            } else if (!lodash.isUndefined(args)) {
                data.push(args);
            }
            for (var i = 0; i < listeners.length; i++) {
                var listener = listeners[i];

                var evt = new DescartesEvent(type, data);
                listener.fn.apply(listener.scope, [evt.asObject()]);
            }
        }
        return this;
    },
    hasListeners: function (type) {
        return (this._listeners[type] === undefined ? 0 : this._listeners[type].length) > 0;
    }
});
module.exports = EventManager;
