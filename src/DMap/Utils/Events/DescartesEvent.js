var Utils = require('../DescartesUtils');

/**
 * Class: Descartes.Utils.DescartesEvent
 * Classe événement.
 */
var DecartesEvent = Utils.Class({
    type: null,
    data: null,
    /**
     * Constructeur: Descartes.Utils.Event
     * Constructeur d'instance.
     */
    initialize: function (type, data) {
        this.type = type;
        this.data = data;
    },
    asObject: function () {
        var result = {
            type: this.type,
            data: this.data
        };
        return result;
    }
});
module.exports = DecartesEvent;

