/* global Descartes, VERSION */

var DESCARTES_WEBSERVICE_URL = 'https://descartes-server.din.developpement-durable.gouv.fr/services/api/v1/';
var GEOREF_WEBSERVICE_URL = 'https://georef.application.developpement-durable.gouv.fr/geoservices/api/v1';

function getApplicationRoot() {
    return window.location.protocol + '//' + window.location.host + '/' + window.location.pathname.split('/')[1] + '/';
}

function getRoot() {
    return window.location.protocol + '//' + window.location.host + '/';
}

function setWebServiceInstance(wbinstance) {
    if (wbinstance === 'preprod') {
		DESCARTES_WEBSERVICE_URL = "https://preprod.descartes-server.din.developpement-durable.gouv.fr/services/api/v1/";
	} else if (wbinstance === 'localhost') {
		DESCARTES_WEBSERVICE_URL = "http://localhost:8080/services/api/v1/";
	}
    Descartes.fixServicesToWebServiceRoot();
}

function getWebServiceRoot() {
    return DESCARTES_WEBSERVICE_URL;
}

function setWebServiceRoot(wsroot) {
    DESCARTES_WEBSERVICE_URL = wsroot;
}

function setGeoRefWebServiceInstance(wbinstance) {
    if (wbinstance === 'integration') {
		GEOREF_WEBSERVICE_URL = "https://georef.integration.e2.rie.gouv.fr/geoservices/api/v1";
	} else if (wbinstance === 'preprod') {
		GEOREF_WEBSERVICE_URL = "https://georef.preprod.e2.rie.gouv.fr/geoservices/api/v1";
	}
    Descartes.GEOREF_SERVER = Descartes.getGeoRefWebServiceRoot();
}

function getGeoRefWebServiceRoot() {
    return GEOREF_WEBSERVICE_URL;
}

function setGeoRefWebServiceRoot(georefwsroot) {
    GEOREF_WEBSERVICE_URL = georefwsroot;
}

function _getScriptLocation() {
    var SCRIPT_NAME = null;

    var DEV_SCRIPT_NAME = 'descartes.js';
    var PROD_SCRIPT_NAME = 'Descartes_' + VERSION + '.min.js';

    var scripts = document.getElementsByTagName('script');
    for (var i = 0; i < scripts.length; i++) {
        var src = scripts[i].getAttribute('src');
        if (src) {
            if (src.indexOf(';jsessionid')) {
                src = src.split(';jsessionid')[0];
            }
            var index = src.lastIndexOf(DEV_SCRIPT_NAME);
            if (index > -1) {
                SCRIPT_NAME = DEV_SCRIPT_NAME;
            } else {
                index = src.lastIndexOf(PROD_SCRIPT_NAME);
                if (index > -1) {
                    SCRIPT_NAME = PROD_SCRIPT_NAME;
                }
            }

            if (SCRIPT_NAME !== null && (index + SCRIPT_NAME.length === src.length)) {
                return src.slice(0, -SCRIPT_NAME.length);
            }
        }
    }
    return '';
}

function checkDescartesContext(context) {
	return true;
}

function applyDescartesContext(context) {

	if (checkDescartesContext(context)) {
		try {

		var contenuCarte = new Descartes.MapContent(context.mapContent.mapContentParams);

		contenuCarte.populate(context.mapContent.items);

		var carte = null;

		if (context.map.type === Descartes.Map.MAP_TYPES.CONTINUOUS) {
			carte = new Descartes.Map.ContinuousScalesMap(context.map.div, contenuCarte, context.map.mapParams);
		} else if (context.map.type === Descartes.Map.MAP_TYPES.DISCRETE) {
			carte = new Descartes.Map.DiscreteScalesMap(context.map.div, contenuCarte, context.map.mapParams);
		}

		if (context.features) {
			if (context.features.toolBars) {
				for (var i = 0, iLen = context.features.toolBars.length; i < iLen; i++) {
					var toolBar = carte.addNamedToolBar(context.features.toolBars[i].div, context.features.toolBars[i].tools, context.features.toolBars[i].options);
					if (context.features.toolBars[i].toolBarId) {
						toolBar.toolBarName = context.features.toolBars[i].toolBarId;
					}
				}
			}
		}

		if (context.mapContent.mapContentManager) {
            carte.addContentManager(context.mapContent.mapContentManager.div, context.mapContent.mapContentManager.contentTools, context.mapContent.mapContentManager.options);
		}

		if (context.mapContent.mapContentManagerSimple) {
            carte.addContentManagerSimple(context.mapContent.mapContentManagerSimple.div);
		}

		carte.show();

		if (context.features) {

			if (context.features.infos) {
				carte.addInfos(context.features.infos);
			}
			if (context.features.actions) {
				carte.addActions(context.features.actions);
			}

			if (context.features.directionalPanPanel) {
				carte.addDirectionalPanPanel(context.features.directionalPanPanel.options);
			}
			if (context.features.miniMap) {
                carte.addMiniMap(context.features.miniMap.resourceUrl, context.features.miniMap.options);
			}
			if (context.features.toolTip) {
				var toolTipLayers = [];
				for (var m = 0, mLen = context.features.toolTip.toolTipLayers.length; m < mLen; m++) {
                    toolTipLayers.push({
                        layer: contenuCarte.getLayerById(context.features.toolTip.toolTipLayers[m].layerId),
                        fields: context.features.toolTip.toolTipLayers[m].fields
                    });
				}
				carte.addToolTip(context.features.toolTip.div, toolTipLayers, context.features.toolTip.options);
			}
			if (context.features.selectToolTip) {
				var selectToolTipLayers = [];
				for (var n = 0, nLen = context.features.selectToolTip.selectToolTipLayers.length; n < nLen; n++) {
					selectToolTipLayers.push({
                        layer: contenuCarte.getLayerById(context.features.selectToolTip.selectToolTipLayers[n].layerId),
                        fields: context.features.selectToolTip.selectToolTipLayers[n].fields
                    });
				}
				carte.addSelectToolTip(selectToolTipLayers, context.features.selectToolTip.options);
			}
			if (context.features.bookmarksManager) {
				carte.addBookmarksManager(context.features.bookmarksManager.div, context.features.bookmarksManager.mapName, context.features.bookmarksManager.options);
			}
			if (context.features.defaultGazetteer) {
				carte.addDefaultGazetteer(context.features.defaultGazetteer.div, context.features.defaultGazetteer.initValue, context.features.defaultGazetteer.startlevel, context.features.defaultGazetteer.options);
			}
			if (context.features.gazetteer) {
				carte.addGazetteer(context.features.gazetteer.div, context.features.gazetteer.initValue, context.features.gazetteer.levels, context.features.gazetteer.options);
			}
			if (context.features.requestManager) {
				var requestManager = carte.addRequestManager(context.features.requestManager.div, context.features.requestManager.options);
				for (var j = 0, jLen = context.features.requestManager.requests.length; j < jLen; j++) {
					var descartesLayer = contenuCarte.getLayerById(context.features.requestManager.requests[j].layerId);
					var requestObj = context.features.requestManager.requests[j];
					var request = new Descartes.Request(descartesLayer, requestObj.title, requestObj.geometryType, requestObj.options);
					for (var k = 0, kLen = context.features.requestManager.requests[j].requestMembers.length; k < kLen; k++) {
						var critereType = new Descartes.RequestMember(context.features.requestManager.requests[j].requestMembers[k].title, context.features.requestManager.requests[j].requestMembers[k].field, context.features.requestManager.requests[j].requestMembers[k].operator, context.features.requestManager.requests[j].requestMembers[k].value, context.features.requestManager.requests[j].requestMembers[k].visible);
						request.addMember(critereType);
					}
					requestManager.addRequest(request);
				}
			}

			if (context.features.openlayersFeatures) {
				if (context.features.openlayersFeatures.controls) {
					carte.addOpenLayersControls(context.features.openlayersFeatures.controls);
				}
				if (context.features.openlayersFeatures.interactions) {
					carte.addOpenLayersInteractions(context.features.openlayersFeatures.interactions);
				}
			}
		}

        return carte;
		} catch (e) {
			alert("Problème de chargement du context Descartes");
		}
	} else {
		alert("Problème de chargement du context Descartes");
	}
	return null;

}

/**
 * Class: Descartes
 * L'objet Descartes est l'objet technique principal de la librairie.
 *
 * :
 * Il fournit :
 * - un espace de nommage pour toutes les classes de la librairie.
 * - des propriétés et méthodes statiques de configuration générale.
 *
 * :
 * La classe fonctionnelle principale de la librairie est la classe <Descartes.Map>.
 */

var constants = {
    /**
     * Staticpropriete: FEATUREINFO_SERVER
     * {String} Url d'accès au service centralisé pour les requêtes WMS/GetFeatureInfo
     */
    FEATUREINFO_SERVER: getWebServiceRoot() + 'getFeatureInfo',

    /**
     * Staticpropriete: FEATURE_SERVER
     * {String} Url d'accès au service centralisé pour les requêtes WMS/GetFeature
     */
    FEATURE_SERVER: getWebServiceRoot() + 'getFeature',

    /**
     * Staticpropriete: EXPORT_PNG_SERVER
     * {String} Url d'accès au service centralisé pour les exportations au format PNG
     */
    EXPORT_PNG_SERVER: getWebServiceRoot() + 'exportPNG',

    /**
     * Staticpropriete: EXPORT_PDF_SERVER
     * {String} Url d'accès au service centralisé pour les exportations au format PDF
     */
    EXPORT_PDF_SERVER: getWebServiceRoot() + 'exportPDF',

    /**
     * Staticpropriete: EXPORT_CSV_SERVER
     * {String} Url d'accès au service centralisé pour les exportations au format CSV
     */
    EXPORT_CSV_SERVER: getWebServiceRoot() + 'csvExport',

    /**
     * Staticpropriete: CONTEXT_MANAGER_SERVER
     * {String} Url d'accès au service centralisé pour la gestion des contextes de consultation
     */
    CONTEXT_MANAGER_SERVER: getWebServiceRoot() + 'contextManager',

    /**
     * Staticpropriete: GAZETTEER_LOCATION
     * {String} Répertoire de localisation des fichiers statiques contenant les objets de référence ("descartes/gazetteer/" par défaut).
     */
    GAZETTEER_LOCATION: _getScriptLocation() + '../gazetteer/',

    /**
     * Staticpropriete: PROXY_SERVER
     * {String} Url d'accès au proxy centralisé
     */
    PROXY_SERVER: getWebServiceRoot() + 'proxy?',

    Descartes_Papers: [
        {name: 'A3', width: 297, length: 420},
        {name: 'A4', width: 210, length: 297},
        {name: 'A5', width: 148, length: 210}
    ],
    PROJECTIONS_WMS_1_3_0_YX: [],
    INFO_WINDOW: null,
    EXPORT_WINDOW: null,
    MIN_BBOX: 100,

    /**
     * Staticpropriete: GEOREF_SERVER
     * {String} Url d'accès au service web de localisation
     */
    GEOREF_SERVER: GEOREF_WEBSERVICE_URL,

    /**
     * Staticmethode: fixServicesToApplicationRoot
     * Fixe les services serveur par rapport à la racine de l'application.
     */
    fixServicesToApplicationRoot: function () {
        Descartes.FEATUREINFO_SERVER = getApplicationRoot() + 'getFeatureInfo';
        Descartes.FEATURE_SERVER = getApplicationRoot() + 'getFeature';
        Descartes.EXPORT_PNG_SERVER = getApplicationRoot() + 'exportPNG';
        Descartes.EXPORT_PDF_SERVER = getApplicationRoot() + 'exportPDF';
        Descartes.EXPORT_CSV_SERVER = getApplicationRoot() + 'csvExport';
        Descartes.CONTEXT_MANAGER_SERVER = getApplicationRoot() + 'contextManager';
        Descartes.PROXY_SERVER = getApplicationRoot() + 'proxy?';
    },
    /**
     * Staticmethode: fixServicesToRoot
     * Fixe les services serveur par rapport à la racine du serveur (sans "context" applicatif).
     */
    fixServicesToRoot: function () {
        Descartes.FEATUREINFO_SERVER = getRoot() + 'getFeatureInfo';
        Descartes.FEATURE_SERVER = getRoot() + 'getFeature';
        Descartes.EXPORT_PNG_SERVER = getRoot() + 'exportPNG';
        Descartes.EXPORT_PDF_SERVER = getRoot() + 'exportPDF';
        Descartes.EXPORT_CSV_SERVER = getRoot() + 'csvExport';
        Descartes.CONTEXT_MANAGER_SERVER = getRoot() + 'contextManager';
        Descartes.PROXY_SERVER = getRoot() + 'proxy?';
    },
    /**
     * Staticmethode: fixServicesToWebServiceRoot
     * Fixe les services serveur par rapport aux services web en production.
     */
    fixServicesToWebServiceRoot: function () {
        Descartes.FEATUREINFO_SERVER = getWebServiceRoot() + 'getFeatureInfo';
        Descartes.FEATURE_SERVER = getWebServiceRoot() + 'getFeature';
        Descartes.EXPORT_PNG_SERVER = getWebServiceRoot() + 'exportPNG';
        Descartes.EXPORT_PDF_SERVER = getWebServiceRoot() + 'exportPDF';
        Descartes.EXPORT_CSV_SERVER = getWebServiceRoot() + 'csvExport';
        Descartes.CONTEXT_MANAGER_SERVER = getWebServiceRoot() + 'contextManager';
        Descartes.PROXY_SERVER = getWebServiceRoot() + 'proxy?';
    },

    getScriptLocation: _getScriptLocation,
    getApplicationRoot: getApplicationRoot,
    getRoot: getRoot,
    setWebServiceInstance: setWebServiceInstance,
    getWebServiceRoot: getWebServiceRoot,
    setWebServiceRoot: setWebServiceRoot,
    setGeoRefWebServiceInstance: setGeoRefWebServiceInstance,
    getGeoRefWebServiceRoot: getGeoRefWebServiceRoot,
    setGeoRefWebServiceRoot: setGeoRefWebServiceRoot,
    checkDescartesContext: checkDescartesContext,
    applyDescartesContext: applyDescartesContext
};
module.exports = constants;
