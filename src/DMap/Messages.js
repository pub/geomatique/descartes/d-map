var _ = require('lodash');

var messages = {
    Descartes_COOKIES_DISABLED: "Votre navigateur doit accepter les cookies pour utiliser cette fonction.",
    Descartes_SEARCH_ERROR: "Problème lors de l'interrogation.",
    Descartes_SEARCH_ERROR_C_INTERROGEABLE: "Aucune couche n'est actuellement interrogeable.",
    Descartes_SEARCH_ERROR_GETFEATUREINFO: "Problème d'interrogation: aucune des couches interrogeables n'a de serveur WMS associé.",
    Descartes_SEARCH_ERROR_GETFEATURES: "Problème d'interrogation: aucune des couches interrogeables n'a de serveur WFS associé.",
    Descartes_SEARCH_INFO_C_INTERROGEABLE: "Interrogation non disponible pour certaines couches car elles n'ont pas de service WFS associé:\n",
    Descartes_SEARCH_FAILURE: "Une erreur est survenue pendant la recherche des informations.",
    Descartes_GET_CAPABILITIES_FAILURE: "Une erreur est survenue pendant l'interrogation du serveur distant.",
    Descartes_GET_CAPABILITIES_NO_LAYERS: "Aucune couche n'est disponible pour la projection courante.",
    Descartes_EXPORT_ERROR: "Aucune couche n'est actuellement visible.",
    Descartes_EXPORT_ERROR_FORMAT_PAPER: "Votre impression dépasse le format A3. Veuillez réduire la taille de votre navigateur.",
    Descartes_Messages_UI_ScaleSelectorInPlace: {
        TITLE_MESSAGE: "Choisir l\'échelle de la carte",
        DEFAULT_OPTION_MESSAGE: "Choisissez une échelle"
    },
    Descartes_Messages_UI_ScaleChooserInPlace: {
        TITLE_MESSAGE: "Choisir l\'échelle de la carte",
        INVITE_MESSAGE: "Saisissez une échelle : 1/ ",
        BUTTON_MESSAGE: "Activer",
        RANGE_PART_ONE_MESSAGE: "Les échelles de visualisation doivent être comprises entre ",
        RANGE_PART_TWO_MESSAGE: " et ",
        FORMAT_MESSAGE: "Saisissez une valeur numérique positive non nulle."
    },
    Descartes_Messages_Tool_ZoomToInitialExtent: {
        TITLE: "Retour à l'étendue initiale"
    },
    Descartes_Messages_Tool_ZoomToMaximalExtent: {
        TITLE: "Zoom sur l'étendue maximale"
    },
    Descartes_Messages_Tool_DragPan: {
        TITLE: "Panoramique, faites glisser la souris pour déplacer la carte"
    },
    Descartes_Messages_Tool_GeolocationSimple: {
        TITLE: "Localiser ma position courante",
        TITLE_POPUP: "Géolocalisation",
        TITLE_EXPORT_POPUP: "Exporter la position courante",
        UNDEFINED_LABEL: "indéterminé",
        ERROR_MSG: "Aucune position courante"
    },
    Descartes_Messages_Tool_GeolocationTracking: {
        TITLE: "Tracer mes positions courantes",
        DIALOG_TITLE_EXPORT_FORMAT: "Exporter la trace des positions",
        TITLE_POPUP: "Géolocalisation",
        TITLE_EXPORT_POPUP: "Exporter la position courante",
        UNDEFINED_LABEL: "indéterminé",
        ERROR_MSG: "Aucune position courante",
        GPX_FORMAT_WPT: "GPX",
        GPX_FORMAT_RTE: "GPX (route)",
        GPX_FORMAT_TRK: "GPX (trace)"
    },
    Descartes_Messages_Tool_ZoomIn: {
        TITLE: "Zoom avant"
    },
    Descartes_Messages_Tool_ZoomOut: {
        TITLE: "Zoom arrière"
    },
    Descartes_Messages_Tool_NavigationHistory: {
        NEXT_TITLE: "Aller à l'étendue géographique suivante",
        PREVIOUS_TITLE: "Aller à l'étendue géographique précédente"
    },
    Descartes_Messages_Button_DirectionalPanNorth: {
        TITLE: "Déplacement vers le nord"
    },
    Descartes_Messages_Button_DirectionalPanSouth: {
        TITLE: "Déplacement vers le sud"
    },
    Descartes_Messages_Button_DirectionalPanWest: {
        TITLE: "Déplacement vers l'ouest"
    },
    Descartes_Messages_Button_DirectionalPanEast: {
        TITLE: "Déplacement vers l'est"
    },
    Descartes_Messages_Button_DirectionalPanNorthWest: {
        TITLE: "Déplacement vers le nord-ouest"
    },
    Descartes_Messages_Button_DirectionalPanNorthEast: {
        TITLE: "Déplacement vers le nord-est"
    },
    Descartes_Messages_Button_DirectionalPanSouthWest: {
        TITLE: "Déplacement vers le sud-ouest"
    },
    Descartes_Messages_Button_DirectionalPanSouthEast: {
        TITLE: "Déplacement vers le sud-est"
    },
    Descartes_Messages_UI_LayersTree: {
        ROOT_TITLE: "Contenu de la carte",
        CLOSE_GROUP: "Cliquez pour fermer le groupe",
        OPEN_GROUP: "Cliquez pour ouvrir le groupe",
        HIDE_GROUP: "Cliquez pour masquer les thèmes du groupe",
        SHOW_GROUP: "Cliquez pour afficher les thèmes du groupe",
        HIDE_LAYER: "Cliquez pour masquer le thème",
        SHOW_LAYER: "Cliquez pour afficher le thème",
        MIN_SCALE: "Visible du ",
        MAX_SCALE: " au ",
        MIN_SCALE_ONLY: "Visible à partir du ",
        MAX_SCALE_ONLY: "Visible jusqu\'au ",
        DISABLE_QUERY: "Cliquez pour désactiver le thème lors des interrogations",
        ENABLE_QUERY: "Cliquez pour activer le thème lors des interrogations",
        FORBIDDEN_QUERY: "Le thème n'est pas visible : Interrogations impossibles",
        ACTIVE_SHEET: "Cliquez pour afficher les données du thème",
        INACTIVE_SHEET: "Le thème n'est pas visible : Affichage des données impossible",
        NO_QUERY: "",
        LIEN_METADATA: "Cliquez pour afficher les métadonnées associées dans une nouvelle fenêtre",
        NO_LIEN_METADATA: "Aucune métadonnée sur cette couche",
        LOADING: "Couche en cours de chargement",
        MORE_INFOS: "Plus d'informations",
        MORE_INFO_LEGEND: "Légende"
    },
    Descartes_Messages_UI_LayersTreeSimple: {
        ROOT_TITLE: "Contenu de la carte",
        HIDE_LAYER: "Cliquez pour masquer le thème",
        SHOW_LAYER: "Cliquez pour afficher le thème",
        MIN_SCALE: "Visible du ",
        MAX_SCALE: " au ",
        MIN_SCALE_ONLY: "Visible à partir du ",
        MAX_SCALE_ONLY: "Visible jusqu\'au ",
        DISABLE_QUERY: "Cliquez pour désactiver le thème lors des interrogations",
        ENABLE_QUERY: "Cliquez pour activer le thème lors des interrogations",
        FORBIDDEN_QUERY: "Le thème n'est pas visible : Interrogations impossibles",
        NO_QUERY: "",
        MORE_INFOS: "Plus d'informations",
        MORE_INFO_LEGEND: "Légende"
    },
    Descartes_Forms_Group: {
        TITLE_INPUT: "Titre du groupe",
        OPENED_INPUT: "Ouvert",
        TITLE_ERROR: "Le titre est obligatoire"
    },
    Descartes_Messages_Button_ContentTask_AddGroup: {
        TITLE: "Ajouter un sous-groupe",
        DIALOG_TITLE: "Ajout d'un sous-groupe",
        OK_BUTTON: "Créer",
        CANCEL_BUTTON: "Annuler"
    },
    Descartes_Messages_Button_ContentTask_AlterGroup: {
        TITLE: "Modifier le groupe",
        DIALOG_TITLE: "Modification d'un groupe",
        OK_BUTTON: "Modifier",
        CANCEL_BUTTON: "Annuler"
    },
    Descartes_Messages_Button_ContentTask_RemoveGroup: {
        TITLE: "Supprimer le groupe",
        DIALOG_TITLE: "Suppresssion d'un groupe",
        MSG_WARNING: "</br><div class='DescartesRemoveGroupWarning'><i class='fa fa-warning'></i>Vous allez supprimer un groupe contenant ",
        MSG_WARNING2: " Vous n'aurez aucun moyen de récupération dans cette session.</div>",
        OK_BUTTON: "Supprimer",
        CANCEL_BUTTON: "Annuler"
    },
    Descartes_Forms_Layer: {
        REQUIRED_PARAMS_FIELSET: "Paramètres obligatoires de la couche",
        OPTIONAL_PARAMS_FIELSET: "Paramètres optionnels de la couche",
        OPTIONS_FIELSET: "Options de consultation",
        TITLE_INPUT: "Titre de la couche : ",
        LAYERNAME_INPUT: "Nom OGC de la couche : ",
        SERVER_INPUT: "URL du serveur OGC : ",
        QUERYABLE_INPUT: "Interrogeable : ",
        SHEETABLE_INPUT: "Consultable en tableau de données : ",
        MAXSCALE_INPUT: "Dénominateur de l'échelle maximale : ",
        MINSCALE_INPUT: "Dénominateur de l'échelle minimale : ",
        LEGEND_INPUT: "URL de la légende : ",
        METADATAURL_INPUT: "URL des métadonnées : ",
        VERSION_INPUT: "Version du serveur OGC : ",
        STYLES_INPUT: "Styles appliqués à la couche : ",
        FORMAT_INPUT: "Format : ",
        TYPE_INPUT: "Type de couche : ",
        TYPE_WMS: "Image globale (WMS)",
        TYPE_WMSC: "Image tuilée (WMSC)",
        TYPE_TMS: "Image tuilée (TMS)",
        TYPE_WMTS: "Image tuilée (WMTS)",
        TYPE_WFS: "Objets vectoriels (WFS)",
        TYPE_KML: "Objets vectoriels (KML)",
        TYPE_GeoJSON: "Objets vectoriels (GeoJSON)",
        TYPE_GenericVector: "Objets vectoriels (GenericVector)",
        TYPE_Annotations: "Objets vectoriels (Annotations)",
        TYPE_OSM: "Image tuilée (OSM)",
        EMPTY_TITLE_ERROR: "Le titre est obligatoire",
        EMPTY_LAYERNAME_ERROR: "Le nom OGC est obligatoire",
        EMPTY_SERVERURL_ERROR: "L'URL du serveur est obligatoire",
        NOTHTTP_SERVERURL_ERROR: "L'URL du serveur doit commencer par 'http://' ou 'https://'",
        NOTHTTP_LEGEND_ERROR: "L'URL de la légende doit commencer par 'http://' ou 'https://'",
        NOTHTTP_METADATAURL_ERROR: "L'URL des métadonnées doit commencer par 'http://' ou 'https://'",
        MAXSCALE_ERROR: "Le dénominateur de l'échelle maximale doit être un nombre supérieur à 1",
        MINSCALE_ERROR: "Le dénominateur de l'échelle minimale doit être un nombre supérieur à 1",
        SCALES_ERROR: "Le dénominateur de l'échelle maximale doit être inférieur au dénominateur de l'échelle minimale",
        ERRORS_LIST: "La couche ne peut pas être enregistrée car les erreurs suivantes ont été rencontrées :"
    },
    Descartes_Messages_Button_ContentTask_AddLayer: {
        TITLE: "Ajouter une couche WMS",
        DIALOG_TITLE: "Ajout d'une couche WMS",
        OK_BUTTON: "Créer",
        CANCEL_BUTTON: "Annuler"
    },
    Descartes_Messages_Button_ContentTask_RemoveLayer: {
        TITLE: "Supprimer la couche",
        DIALOG_TITLE: "Suppression d'une couche",
        OK_BUTTON: "Supprimer",
        CANCEL_BUTTON: "Annuler"
    },
    Descartes_Messages_Button_ContentTask_AlterLayer: {
        TITLE: "Modifier la couche",
        DIALOG_TITLE: "Modification d'une couche",
        OK_BUTTON: "Modifier",
        CANCEL_BUTTON: "Annuler"
    },
    Descartes_Messages_Button_ContentTask_ChooseWmsLayers: {
        TITLE: "Ajouter des couches WMS après découverte",
        PROMPT_URL_SERVER: "URL du serveur WMS à interroger :",
        PROMPT_VERSION_SERVER: "VERSION du serveur WMS à interroger :",
        PROMPT_USE_URL_SERVER: "Ne pas utiliser l'url retournée par le server interrogé",
        DIALOG_TITLE: "Sélection des couches",
        OK_BUTTON: "Ajouter",
        CANCEL_BUTTON: "Annuler",
        TITLE_COL_NAME: "Titre",
        FORMAT_COL_NAME: "Format",
        MINSCALE_COL_NAME: "Echelle mini",
        MAXSCALE_COL_NAME: "Echelle maxi",
        ABSTRACT_COL_NAME: "Description",
        STYLE_COL_NAME: "Style",
        KEYWORDS_COL_NAME: "Mots-clés",
        METADATA_COL_NAME: "Plus d'infos",
        WAITING_MESSAGE: "Recherche en cours, merci de patienter"
    },
    Descartes_Messages_Button_ContentTask_ExportVectorLayer: {
        TITLE: "Exporter une couche",
        FORMAT: "Veuillez sélectionner le format d'export :"
    },
    Descartes_Messages_Button_CenterToCoordinates: {
        TITLE: "Centrer selon des coordonnées"
    },
    Descartes_Forms_CoordinatesInput: {
        INVITE1_MESSAGE: "X : ",
        INVITE2_MESSAGE: "Y : ",
        RANGE_X_PART_ONE_MESSAGE: "X doit être compris entre ",
        RANGE_Y_PART_ONE_MESSAGE: "Y doit être compris entre ",
        INVITE1_LON_MESSAGE: "Lon : ",
        INVITE2_LAT_MESSAGE: "Lat : ",
        RANGE_LON_PART_ONE_MESSAGE: "Lon doit être compris entre ",
        RANGE_LAT_PART_ONE_MESSAGE: "Lat doit être compris entre ",
        RANGE_PART_TWO_MESSAGE: " et ",
        FORMAT_MESSAGE: "Saisissez une valeur numérique.",
        ERRORS_LIST: "Le recentrage ne peut être effectué car les erreurs suivantes ont été rencontrées :"
    },
    Descartes_Messages_UI_CoordinatesInputInPlace: {
        TITLE_MESSAGE: "Choisir les coordonnées",
        BUTTON_MESSAGE: "Recentrer"
    },
    Descartes_Messages_UI_CoordinatesInputDialog: {
        DIALOG_TITLE: "Choisir les coordonnées",
        OK_BUTTON: "Recentrer",
        CANCEL_BUTTON: "Annuler"
    },

    Descartes_Messages_UI_SizeSelectorInPlace: {
        TITLE_MESSAGE: "Choisir la taille de la carte",
        INFO_BUL1: "Afficher la carte avec une taille en pixels de ",
        INFO_BUL2: " par "
    },

    Descartes_Messages_Info_MetricScale: {
        TITLE_MESSAGE: "&Eacute;chelle : <br>"
    },
    Descartes_Messages_Info_MapDimensions: {
        TITLE_LARGEUR_MESSAGE: "Largeur :&nbsp;",
        TITLE_HAUTEUR_MESSAGE: "<br>Hauteur :&nbsp;"
    },

    Descartes_Messages_Tool_CenterMap: {
        TITLE: "Centrer la carte"
    },

    Descartes_Messages_Tool_Measure_MeasureDistance: {
        TITLE: "Calcul de distance",
        TITLE_DISTANCE: "Distance: "
    },

    Descartes_Messages_Tool_Measure_MeasureArea: {
        TITLE: "Calcul de surface",
        TITLE_AIRE: "Aire: ",
        TITLE_AIRE_UNITE: "<sup>2</sup>",
        TITLE_PERIMETRE: "<br/>Périmètre: "
    },
    Descartes_Messages_Tool_Selection_PointSelection: {
        TITLE: "Selection par point"
    },

    Descartes_Messages_Tool_Selection_PointRadiusSelection: {
        TITLE: "Selection par point-rayon",
        TITLE_MESSAGE: "Veuillez entrer les caractéristiques",
        OK_BUTTON: "OK",
        CANCEL_BUTTON: "Annuler",
        BUFFER_LABEL: "Rayon (m) ",
        FORMAT_BUFFER_ERROR: "Saisissez une valeur numérique pour la distance.",
        INFO_RADIUS: "Rayon: "
    },

    Descartes_Messages_Tool_Selection_PolygonSelection: {
        TITLE: "Selection par polygone"
    },

    Descartes_Messages_Tool_Selection_PolygonBufferHaloSelection: {
        TITLE: "Selection par polygone avec buffer",
        INFO_BUFFER: "Buffer: ",
        OK_BUTTON: "OK",
        CANCEL_BUTTON: "Annuler",
        BUFFER_LABEL: "Distance (m) ",
        NB_POINTS_LABEL: "Nombre de segments pour créer l'arc de cercle (facultatif)",
        TITLE_MESSAGE: "Veuillez entrer les caractéristiques du buffer",
        FORMAT_BUFFER_ERROR: "Saisissez une valeur numérique pour la distance.",
        FORMAT_NBPOINTS_ERROR: "Saisissez une valeur numérique pour le nombre de points."
    },

    Descartes_Messages_Tool_Selection_LineBufferHaloSelection: {
        TITLE: "Selection par ligne avec buffer",
        INFO_BUFFER: "Buffer: ",
        OK_BUTTON: "OK",
        CANCEL_BUTTON: "Annuler",
        BUFFER_LABEL: "Distance (m) ",
        NB_POINTS_LABEL: "Nombre de segments pour créer l'arc de cercle (facultatif)",
        TITLE_MESSAGE: "Veuillez entrer les caractéristiques du buffer",
        FORMAT_BUFFER_ERROR: "Saisissez une valeur numérique pour la distance.",
        FORMAT_NBPOINTS_ERROR: "Saisissez une valeur numérique pour le nombre de points."
    },

    Descartes_Messages_Tool_Selection_CircleSelection: {
        TITLE: "Selection par cercle",
        INFO_RADIUS: "Rayon approximatif: "
    },

    Descartes_Messages_Tool_Selection_RectangleSelection: {
        TITLE: "Selection par rectangle"
    },
    Descartes_Messages_Button_DisplayLayersTreeSimple: {
        TITLE: "Afficher l'arbre des couches simplifié"
    },
    Descartes_Messages_Button_ShareLinkMap: {
        TITLE: "Partager la vue courante de la carte",
        DIALOG_LABEL_TTL: "Durée du partage (en jours): ",
        DIALOG_BTN_GENERATE: "Cliquez pour générer un lien",
        DIALOG_LABEL_LINK: "Lien"
    },
    Descartes_Messages_Button_ExportPNG: {
        TITLE: "Enregistrement PNG",
        DIALOG_TITLE: "Paramètres de la mise en page"
    },

    Descartes_Messages_Button_ExportPDF: {
        TITLE: "Mise en page PDF"
    },
    Descartes_Forms_PrinterSetup: {
        LANDSCAPE_OPTION: " Paysage",
        PORTRAIT_OPTION: " Portrait",
        PAPER_INPUT: "Format de feuille : ",
        MARGINS_GROUP_INPUT: "Marges (en mm)",
        MARGIN_TOP_INPUT: "Haut : ",
        MARGIN_BOTTOM_INPUT: "Bas : ",
        MARGIN_LEFT_INPUT: "Gauche : ",
        MARGIN_RIGHT_INPUT: "Droit : ",
        MARGIN_TOP_ERROR: "La valeur de la marge \"haut\" doit être un nombre positif",
        MARGIN_BOTTOM_ERROR: "La valeur de la marge \"bas\" doit être un nombre positif",
        MARGIN_LEFT_ERROR: "La valeur de la marge \"gauche\" doit être un nombre positif",
        MARGIN_RIGHT_ERROR: "La valeur de la marge \"droit\" doit être un nombre positif",
        MARGIN_WIDTH_ERROR: "En largeur, il manque pour inclure la carte ",
        MARGIN_HEIGHT_ERROR: "En hauteur, il manque pour inclure la carte ",
        MARGINS_TIP: ". Diminuez les marges.",
        ERRORS_LIST: "La génération du PDF ne peut être déclenchée car les erreurs suivantes ont été rencontrées :"
    },

    Descartes_Messages_UI_PrinterSetupInPlace: {
        TITLE_MESSAGE: "Paramètres de la mise en page",
        BUTTON_MESSAGE: "Générer le PDF"
    },
    Descartes_Messages_UI_PrinterSetupDialog: {
        DIALOG_TITLE: "Paramètres de la mise en page",
        OK_BUTTON: "Générer le PDF",
        CANCEL_BUTTON: "Annuler"
    },
    Descartes_Messages_UI_GazetteerInPlace: {
        TITLE_MESSAGE: 'Localisation',
        BUTTON_MESSAGE: "Localiser"
    },
    Descartes_Messages_Action_AjaxSelection: {
        WAITING_MESSAGE: 'Recherche en cours, merci de patienter',
        ERROR_JSON_PARSER: "ERREUR: Problème lors de l'affichage des résultats - "
    },
    Descartes_Messages_Action_DefaultGazetteer: {
        TITLE_MESSAGE_REGION: "Choisissez une région",
        TITLE_ERROR_REGION: "Aucune région",
        TITLE_NAME_REGION: "reg",
        TITLE_MESSAGE_DEPT: "Choisissez un département",
        TITLE_ERROR_DEPT: "Aucun département",
        TITLE_NAME_DEPT: "dept",
        TITLE_MESSAGE_COMMUNE: "Choisissez une commune",
        TITLE_ERROR_COMMUNE: "Aucune commune",
        TITLE_NAME_COMMUNE: "com"
    },

    Descartes_Messages_UI_DataGrid: {
        ASCENDING_ORDER: "Trier dans l'ordre ascendant",
        DESCENDING_ORDER: "Trier dans l'ordre descendant",
        TITLE_OPEN_LINK: "Consulter",
        TITLE_MAIL_LINK: "Envoyer un message",
        TITLE_DOWNLOAD_FILE: "Télécharger",
        TITLE_GOTO_FEATURES_BOUNDS: "Recentrer sur l'objet"
    },
    Descartes_Messages_Action_RequestManager: {
        MISSING_VALUES: "Saisie incomplète",
        ERROR_FLUX_INFOS: "Problème: pas de flux infos"
    },

    Descartes_Messages_UI_RequestManagerInPlace: {
        TITLE_MESSAGE: 'Requêtes attributaires',
        BUTTON_MESSAGE_RECHERCHE: "Rechercher",
        BUTTON_MESSAGE_DROPDOWN: "Actions sur la sélection",
        BUTTON_MESSAGE_UNSELECT: "Effacer",
        BUTTON_MESSAGE_FIXSELECTIONLAYER: "Afficher",
        BUTTON_MESSAGE_FLASHSELECTIONLAYER: "Faire clignoter",
        BUTTON_MESSAGE_UNDISPLAYSELECTIONLAYER: "Masquer",
        BUTTON_MESSAGE_ADDSELECTIONLAYER: "Créer une couche temporaire",
        BUTTON_MESSAGE_DROPDOWN_TOOLTIP: "Actions sur la sélection affichée dans la carte",
        BUTTON_MESSAGE_UNSELECT_TOOLTIP: "Effacer la sélection dans la carte",
        BUTTON_MESSAGE_FIXSELECTIONLAYER_TOOLTIP: "Afficher la sélection dans la carte en arrêtant le clignotement",
        BUTTON_MESSAGE_FLASHSELECTIONLAYER_TOOLTIP: "Faire clignoter la sélection dans la carte",
        BUTTON_MESSAGE_UNDISPLAYSELECTIONLAYER_TOOLTIP: "Masquer la sélection dans la carte en arrêtant le clignotement",
        BUTTON_MESSAGE_ADDSELECTIONLAYER_TOOLTIP: "Créer une couche temporaire à partir de la sélection affichée dans la carte le temps de votre session d'utilisation",
        LABEL_SEARCH_ON_EXTENT: "Rechercher sur :",
        LABEL_EXTENT_GLOBAL: "l'emprise globale de la carte",
        LABEL_EXTENT_CURRENT: "l'emprise courante",
        LABEL_EXTENT_ENTITY: "l'emprise d'une entité administrative",
        LABEL_FILTER_LAYER: "Filtrer les objets de la couche :",
        INACTIVE_SELECT_TITLE: "Choisissez une valeur"
    },

    Descartes_Messages_UI_RequestManagerGazetteer: {
        LABEL_EXTENT_ENTITY_INIT: "l'emprise d'une entité administrative",
        LABEL_EXTENT_ENTITY: "l'emprise de l'entité administrative: "
    },

    Descartes_Messages_UI_ToolTipContainer: {
        TITLE_OPEN_LINK: "Consulter",
        TITLE_MAIL_LINK: "Envoyer un message",
        TITLE_DOWNLOAD_FILE: "Télécharger"
    },

    Descartes_Messages_Action_BookmarksManager: {
        NO_COOKIES_ALERT: "\n(Votre navigateur n'accepte pas les cookies :\nla vue ne sera pas disponible lors de prochaines visites)",
        SAVE_VIEW: "Choisissez un nom pour la vue :",
        EXISTING_VIEW_ALERT: "La vue existe deja",
        NAME_VIEW_ALERT: "Le nom de la vue doit être obligatoirement renseigné",
        BAD_NAME_VIEW_ALERT: "Le nom de la vue ne peut comporter que les caractères suivants :\n - lettres non accentuées,\n - chiffres,\n - points,\n - espaces,\n - tirets,\n - soulignés.",
        DELETE_VIEW_TITLE: 'Veuillez confirmer la suppression',
        DELETE_VIEW: "Confirmez-vous la suppression de la vue \"",
        DELETE_VIEW_END: "\" ?",
        MISSING_VIEW: "La vue n'est plus disponible"
    },
    Descartes_Messages_UI_BookmarksInPlace: {
        TITLE_MESSAGE: "Gérer des vues personnalisées",
        NO_VIEW: "Aucune vue enregistrée",
        RELOAD_VIEW: "Recharger cette vue",
        OPEN_VIEW: "Ouvrir cette vue dans une nouvelle fenêtre",
        DELETE_VIEW: "Supprimer cette vue",
        BUTTON_MESSAGE: "Enregistrer la vue courante",
        MAX_DAY_PREFIX: "Disponible juqu'au "
    },

    Descartes_Messages_Tool_MiniMap: {
        TITLE_MESSAGE: "Cliquez pour ouvrir/cacher la mini-carte de navigation"
    },

    Descartes_Messages_Info_Legend: {
        TITLE_MESSAGE: "Légende"
    },
    Descartes_Messages_Action_MapContentManager: {
        CONTEXT_MENU_LABEL_ADD_GROUP: 'Ajouter un groupe',
        CONTEXT_MENU_LABEL_ADD_SS_GROUP: 'Ajouter un sous-groupe',
        CONTEXT_MENU_LABEL_ADD_LAYER: 'Ajouter une couche WMS (manuellement)',
        CONTEXT_MENU_LABEL_ADD_LAYER_WMS: 'Ajouter une couches WMS après découverte',
        CONTEXT_MENU_LABEL_UPDATE_GROUP: 'Modifier le groupe',
        CONTEXT_MENU_LABEL_REMOVE_GROUP: 'Supprimer le groupe',
        CONTEXT_MENU_LABEL_UPDATE_LAYER: 'Modifier la couche',
        CONTEXT_MENU_LABEL_REMOVE_LAYER: 'Supprimer la couche',
        CONTEXT_MENU_LABEL_EXPORT_VECTORLAYER: 'Exporter une couche'
    },

    Descartes_Messages_UI_LocalisationAdresseInPlace: {
        TITLE_MESSAGE: "Localisation à l'adresse",
        LABEL_ADRESSE_MESSAGE: "Rue, Avenue, Hameau...",
        LABEL_COMMUNE_MESSAGE: "Code postal et/ou localité",
        DEFAULT_ADRESSE_MESSAGE: "ex : 3 rue de Savoie",
        DEFAULT_COMMUNE_MESSAGE: "ex : 75006 PARIS",
        BUTTON_SEARCH_MESSAGE: "Rechercher",
        BUTTON_CLEAR_MESSAGE: "Effacer les résultats",
        COLUM_HEADER_MESSAGE: "Cliquez sur l'icône pour la situer",
        TITLE_GOTO_ADRESSE_MESSAGE: "Positionner l'adresse",
        WAIT_MESSAGE: "Recherche en cours",
        RESULT1_MESSAGE: "Aucune proposition",
        RESULT2_MESSAGE: "proposition",
        ERROR_FORMAT_MESSAGE: "L\'adresse ne comprend ni code postal ni nom de commune.",
        ERRORS_LIST: "La recherche ne peut pas être lancée:"
    },
    Descartes_Messages_UI_TabbedDataGrids: {
        TAB_BT_EXPORT_CSV_TITLE: "Export CSV",
        TAB_BT_EXPORT_RESULTLAYER_TITLE: "Exporter la couche de résultat",
        DT_BT_COLVIS_TITLE: "Afficher/masquer les colonnes",
        DT_BT_COLVIS_LABEL: "Colonnes visibles",
        DT_BT_COPY_TITLE: "Copier dans le presse-papiers",
        DT_BT_COPY_LABEL: "Copier",
        DT_BT_PRINT_TITLE: "Afficher dans une nouvelle fenêtre pour impression",
        DT_BT_PRINT_LABEL: "Imprimer",
        DIALOG_OTHER_BTN_LABEL: "Afficher/Masquer la sélection",
        DIALOG_TITLE: "Résultats de la recherche",
        MSG_EMPTY_RESULT: "Aucun résultat",
        MSG_EXPORT_ERROR: "Erreur lors de l'export",
        DIALOG_TITLE_EXPORT_FORMAT: "Exporter la couche de résultat"
    },

    Descartes_Messages_UI_ResultFormDialog: {
        DIALOG_DROPMENU_LABEL: "Actions sur la sélection",
        DIALOG_DROPMENU_BTN_UNSELECT_LABEL: "Effacer",
        DIALOG_DROPMENU_BTN_FIX_LABEL: "Afficher",
        DIALOG_DROPMENU_BTN_FLASH_LABEL: "Faire clignoter",
        DIALOG_DROPMENU_BTN_UNDISPLAY_LABEL: "Masquer",
        DIALOG_DROPMENU_BTN_ADD_LABEL: "Créer une couche temporaire",
        DIALOG_DROPMENU_LABEL_TOOLTIP: "Actions sur la sélection affichée dans la carte",
        DIALOG_DROPMENU_BTN_UNSELECT_LABEL_TOOLTIP: "Effacer la sélection dans la carte",
        DIALOG_DROPMENU_BTN_FIX_LABEL_TOOLTIP: "Afficher la sélection dans la carte en arrêtant le clignotement",
        DIALOG_DROPMENU_BTN_FLASH_LABEL_TOOLTIP: "Faire clignoter la sélection dans la carte",
        DIALOG_DROPMENU_BTN_UNDISPLAY_LABEL_TOOLTIP: "Masquer la sélection dans la carte en arrêtant le clignotement",
        DIALOG_DROPMENU_BTN_ADD_LABEL_TOOLTIP: "Créer une couche temporaire à partir de la sélection affichée dans la carte le temps de votre session d'utilisation"
    }
};

messages.Descartes_Messages_Button_ContentTask_AddGroup = _.extend(messages.Descartes_Messages_Button_ContentTask_AddGroup, messages.Descartes_Forms_Group);
messages.Descartes_Messages_Button_ContentTask_AlterGroup = _.extend(messages.Descartes_Messages_Button_ContentTask_AlterGroup, messages.Descartes_Forms_Group);
messages.Descartes_Messages_Button_ContentTask_RemoveGroup = _.extend(messages.Descartes_Messages_Button_ContentTask_RemoveGroup, messages.Descartes_Forms_Group);
messages.Descartes_Messages_Button_ContentTask_AddLayer = _.extend(messages.Descartes_Messages_Button_ContentTask_AddLayer, messages.Descartes_Forms_Layer);
messages.Descartes_Messages_Button_ContentTask_RemoveLayer = _.extend(messages.Descartes_Messages_Button_ContentTask_RemoveLayer, messages.Descartes_Forms_Layer);
messages.Descartes_Messages_Button_ContentTask_AlterLayer = _.extend(messages.Descartes_Messages_Button_ContentTask_AlterLayer, messages.Descartes_Forms_Layer);
messages.Descartes_Messages_UI_CoordinatesInputInPlace = _.extend(messages.Descartes_Messages_UI_CoordinatesInputInPlace, messages.Descartes_Forms_CoordinatesInput);
messages.Descartes_Messages_UI_CoordinatesInputDialog = _.extend(messages.Descartes_Messages_UI_CoordinatesInputDialog, messages.Descartes_Forms_CoordinatesInput);
messages.Descartes_Messages_UI_PrinterSetupInPlace = _.extend(messages.Descartes_Messages_UI_PrinterSetupInPlace, messages.Descartes_Forms_PrinterSetup);
messages.Descartes_Messages_UI_PrinterSetupDialog = _.extend(messages.Descartes_Messages_UI_PrinterSetupDialog, messages.Descartes_Forms_PrinterSetup);


module.exports = messages;

