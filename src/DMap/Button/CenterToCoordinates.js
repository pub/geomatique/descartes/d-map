/* global MODE*/

var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var Button = require('../Button/' + MODE + '/Button');
var Projection = require('../Core/Projection');

var CoordinatesInput = require('../Action/CoordinatesInput');
var CoordinatesInputDialog = require('../UI/' + MODE + '/CoordinatesInputDialog');

require('./css/CenterToCoordinates.css');


/**
 * Class: Descartes.Button.CenterToCoordinates
 * Classe définissant un bouton permettant de recentrer la carte sur un couple de coordonnées géographiques.
 *
 * Hérite de:
 *  - <Descartes.Button>
 */
var Class = Utils.Class(Button, {
    /**
     * Constructeur: Descartes.Button.ContentTask.AbstractEditGroup
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        this.enabled = true;
        Button.prototype.initialize.apply(this, [options]);
    },
    /**
     * Methode: execute
     * Lance une action <Descartes.Action.CoordinatesInput> avec comme interface une <Descartes.UI.CoordinatesInputDialog>.
     */
    execute: function () {
        var viewOptions = {
            view: CoordinatesInputDialog
        };
        if (!_.isNil(this.size)) {
            _.extend(viewOptions, {size: this.size});
        }
        if (!_.isNil(this.projection)) {
            _.extend(viewOptions, {projection: this.projection});
        }
        if (!_.isNil(this.displayProjections)) {
            _.extend(viewOptions, {displayProjections: this.displayProjections});
        }
        if (!_.isNil(this.selectedDisplayProjectionIndex)) {
            _.extend(viewOptions, {selectedDisplayProjectionIndex: this.selectedDisplayProjectionIndex});
        }
        this.action = new CoordinatesInput(null, this.olMap, viewOptions);

    },

    CLASS_NAME: 'Descartes.Button.CenterToCoordinates'
});

module.exports = Class;
