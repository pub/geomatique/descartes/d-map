/* global MODE */
var _ = require('lodash');
var ol = require('openlayers');

var Utils = require('../Utils/DescartesUtils');
var Button = require('../Core/Button');

var DirectionalPanConstants = require('./DirectionalPanConstants');

/**
 * Class: Descartes.Button.DirectionalPan
 * Classe "interne" construisant un bouton de déplacement incrémental d'une "rose des vents" <Descartes.Button.DirectionalPanPanel>.
 *
 * Hérite de:
 *  - <Descartes.Button>
 */
var Class = Utils.Class(Button, {
    /**
     * Propriete: horizontalSlideFactor
     * {Integer} Valeur en pixels des déplacements horizontaux.
     */
    horizontalSlideFactor: 100,
    /**
     * Propriete: verticalSlideFactor
     * {Integer} Valeur en pixels des déplacements verticaux.
     */
    verticalSlideFactor: 50,

    direction: null,

    /**
     * Propriété : duration
     * {Integer} La durée de l'animation de pan en milliseconds
     */
    duration: 1000,
    /**
     * Constructeur: Descartes.Button.DirectionalPan
     * Constructeur d'instances
     *
     * Paramètres:
     * direction - {String} Direction de déplacement pour le bouton.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * horizontalSlideFactor - {Integer} Valeur en pixels des déplacements horizontaux.
     * verticalSlideFactor - {Integer} Valeur en pixels des déplacements verticaux.
     */
    initialize: function (direction, options) {
        this.direction = direction;
        this.CLASS_NAME += this.direction;
        Button.prototype.initialize.apply(this, [options]);
    },
    setMap: function (olMap) {
        this.olMap = olMap;
    },
    createButton: function (div) {
        this.parent = div;
        this.templateButton();
    },
    updateButton: function () {
        this.templateButton(true);
    },
    templateButton: function (update) {
        var buttonClass = this.displayClass + 'ItemInactive';
        if (this.enabled === true) {
            buttonClass = this.displayClass + 'ItemActive';
        }
        var element = document.createElement('div');
        element.id = this.id;
        element.className = buttonClass;
        if (update) {
            var old = document.getElementById(this.id);
            this.parent.replaceChild(element, old);
        } else {
            this.parent.appendChild(element);
        }
        element.onclick = this.trigger.bind(this);
    },
    /**
     * Methode: enable
     * Rend le bouton disponible
     */
    enable: function () {
        if (this.enabled !== true) {
            this.enabled = true;
        }
    },

    /**
     * Methode: disable
     * Rend le bouton indisponible
     */
    disable: function () {
        if (this.enabled !== false) {
            this.enabled = false;
        }
    },

    /**
     * Methode: trigger
     * Effectue le déplacement incrémental de la carte.
     */
    execute: function () {
        var view = this.olMap.getView();
        var center = view.getCenter();
        var centerInPx = this.olMap.getPixelFromCoordinate(center);
        var newCenterInPx = [];
        switch (this.direction) {
            case DirectionalPanConstants.NORTH:
                newCenterInPx = [centerInPx[0], centerInPx[1] - this.verticalSlideFactor];
                break;
            case DirectionalPanConstants.SOUTH:
                newCenterInPx = [centerInPx[0], centerInPx[1] + this.verticalSlideFactor];
                break;
            case DirectionalPanConstants.WEST:
                newCenterInPx = [centerInPx[0] - this.horizontalSlideFactor, centerInPx[1]];
                break;
            case DirectionalPanConstants.EAST:
                newCenterInPx = [centerInPx[0] + this.horizontalSlideFactor, centerInPx[1]];
                break;
            case DirectionalPanConstants.NORTHWEST:
                newCenterInPx = [centerInPx[0] - this.horizontalSlideFactor, centerInPx[1] - this.verticalSlideFactor];
                break;
            case DirectionalPanConstants.SOUTHWEST:
                newCenterInPx = [centerInPx[0] - this.horizontalSlideFactor, centerInPx[1] + this.verticalSlideFactor];
                break;
            case DirectionalPanConstants.NORTHEAST:
                newCenterInPx = [centerInPx[0] + this.horizontalSlideFactor, centerInPx[1] - this.verticalSlideFactor];
                break;
            case DirectionalPanConstants.SOUTHEAST:
                newCenterInPx = [centerInPx[0] + this.horizontalSlideFactor, centerInPx[1] + this.verticalSlideFactor];
                break;
        }
        var newCenter = this.olMap.getCoordinateFromPixel(newCenterInPx);

        var canPan = true;
        if (!_.isNil(this.olMap.getView().options_.extent)) {
            var maxExtent = this.olMap.getView().options_.extent;
            canPan = ol.extent.containsXY(maxExtent, newCenter[0], newCenter[1]);
        }
        if (canPan) {
            view.animate({
                center: newCenter,
                duration: this.duration
            });
        }
    },
    CLASS_NAME: 'Descartes.Button.DirectionalPan'
});
module.exports = Class;
