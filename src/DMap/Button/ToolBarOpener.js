/* global MODE, Descartes */

var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var Button = require('../Button/' + MODE + '/Button');

require('./css/ToolBarOpener.css');

/**
 * Class: Descartes.Button.ToolBarOpener
 * Classe définissant un bouton permettant d'ouvrir un nouvelle barre d'outils, pouvant accueillir des boutons de type <Descartes.Button> et/ou <Descartes.Tool>.
 */
var Class = Utils.Class(Button, {
    /**
     * Propriete: tools
     * {Array} La liste des outils de la barre d'outils.
     */
    tools: [],

    descartesMap: null,
    /**
     * Propriete: toolBar
     * {Descartes.ToolBar} La barre d'outils associée.
     */
    toolBar: null,
    /**
     * Propriete: toolBarName
     * {String} Le nom de la barre d'outils associée.
     */
    toolBarName: null,
    /**
     * Propriete: defaultToolBarOption
     * {Object} Options par défaut de la barre d'outils associée.
     */
    defaultToolBarOptions: {
        closable: true
    },
    /**
     * Propriete: toolBarOption
     * {Object} Options de la barre d'outils associée.
     */
    toolBarOptions: null,
    /**
     * Propriete: title
     * {String} Titre de la barre d'outils.
     */
    title: '',
    /**
     * Propriete: initFunction
     * {Function} fonction lancée à l'ouverture de la barre d'outil.
     */
    initFunction: null,
    /**
     * Propriete: innerDiv
     * {String} le nom de la div si fixe.
     */
    innerDiv: null,
    /**
     * Propriete: activeFirstElement
     * {Boolean} activation ou non du premier element de la barre d'outils si celui-ci est de type Descartes.Tool.
     */
    activeFirstElement: false,

    /**
     * Constructeur: Descartes.Button.ToolBarOpener
     * Constructeur d'instances
     *
     * Paramètres:
     * options -  - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * activeFirstElement - {Boolean} activation ou non du premier element de la barre d'outils si celui-ci est de type Descartes.Tool.
     */
    initialize: function (options) {
        this.enabled = true;

        this.toolBarOptions = _.extend({}, this.defaultToolBarOptions);

        if (!_.isNil(options) && !_.isNil(options.activeFirstElement)) {
            this.activeFirstElement = options.activeFirstElement;
            delete options.activeFirstElement;
        }

        if (!_.isNil(options) && !_.isNil(options.toolBarOptions)) {
            _.extend(this.toolBarOptions, options.toolBarOptions);
            delete options.toolBarOptions;
        }

        Button.prototype.initialize.apply(this, [options]);
    },
    /*
     * Private
     */
    _initToolBarOpener: function (descartesMap) {
        this.descartesMap = descartesMap;
        this._createToolBar();
        this.toolBar.visible = false;
        this.closeToolBar();
    },
    /**
     * Methode: execute
     * Lance l'ouverture ou la fermeture de la barre d'outils.
     */
    execute: function () {
        if (_.isNil(this.toolBar)) {
            this.opening = true;
            this.openToolBar();
            this.opening = false;
        } else {
            this.closeToolBar();
            document.getElementById(this.id).blur();
        }
    },
    /*
     * Private
     */
    _createToolBar: function () {
        var i, len;
        for (i = 0, len = this.descartesMap.toolBars.length; i < len; i++) {
            if (this.descartesMap.toolBars[i].toolBarName === this.toolBarName) {
                //this.tools = this.descartesMap.toolBars[i].controls;
                this.descartesMap.toolBars.splice(i, 1);
                break;
            }
        }

        if (!_.isNil(this.toolBarName)) {
            this.toolBarOptions.toolBarName = this.toolBarName;
        }
        this.toolBarOptions.title = this.title;

        this.toolBar = this.descartesMap.addNamedToolBar(this.innerDiv, this.tools, this.toolBarOptions);
        this.toolBarName = this.toolBar.toolBarName;
        this.toolBar.toolBarOpener = this;
    },

    /**
     * Methode: openToolBar
     * ouverture de la barre d'outils.
     */
    openToolBar: function () {
        if (_.isNil(this.toolBar)) {
            this._createToolBar();
        }
        this.toolBar.visible = true;

        if (_.isNil(this.innerDiv)) {
            this.toolBar.alignTo(this.id);
        }

        this.enabledClass = 'active';
        this.updateButton();

        try {
            this.toolBar.events.register('closed', this, this.destroyToolBar);
        } catch (e) {
        }
        if (!_.isNil(this.initFunction)) {
            this.initFunction();
        }
    },
    /**
     * Methode: closeToolBar
     * Lance la fermeture de la barre d'outils.
     */
    closeToolBar: function () {
        var i, len;

        //rendre invisible la toolbar
        for (i = 0, len = this.descartesMap.toolBars.length; i < len; i++) {
            if (this.descartesMap.toolBars[i].toolBarName === this.toolBarName) {
                this.descartesMap.toolBars[i].visible = false;
                break;
            }
        }

        //fermer les tools bar descendantes
        for (i = 0, len = this.tools.length; i < len; i++) {
            var tool = this.tools[i];
            if (tool.CLASS_NAME === this.CLASS_NAME) {
                if (tool.toolBar !== null) {
                    tool.closeToolBar();
                }
            }
        }
        this.enabledClass = '';
        this.updateButton();

        this.toolBar.destroy();
        this.toolBar = null;
    },
    /**
     * Methode: destroyToolBar
     * Ferme la barre d'outils.
     */
    destroyToolBar: function () {
        this.enabledClass = '';
        this.updateButton();
        this.toolBar.events.unregister('closed', this.destroyToolBar);
        this.toolBar = null;
    },
    CLASS_NAME: 'Descartes.Button.ToolBarOpener'
});

module.exports = Class;
