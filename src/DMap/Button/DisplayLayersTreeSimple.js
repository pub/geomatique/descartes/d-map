/* global MODE */

var _ = require('lodash');
var $ = require('jquery');

var Utils = require('../Utils/DescartesUtils');
var Button = require('../Button/' + MODE + '/Button');
var MessagesConstants = require('../Messages');
//var PrinterParamsManager = require('../Action/PrinterParamsManager');
var MapContentManager = require('../Action/MapContentManager');
var FormDialog = require('../UI/' + MODE + '/FormDialog');

require('./css/DisplayLayersTreeSimple.css');

/**
 * Class: Descartes.Button.DisplayLayersTreeSimple
 * Classe définissant un bouton permettant d'exporter la carte courante sous forme de fichier PDF.
 *
 * Hérite de:
 *  - <Descartes.Button>
 *
 * Ecouteurs mis en place:
 *  - l'événement.
 */
var Class = Utils.Class(Button, {

    /**
     * Propriete: mapContentManager
     * {<Descartes.Action.MapContentManager>} Gestionnaire du contenu de la carte (groupes et couches).
     */
    mapContentManager: null,
    /**
     * Constructeur: Descartes.Button.DisplayLayersTreeSimple
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * infos - Objet JSON stockant les informations complémentaires à insérer dans le fichier PDF.
     */
    initialize: function (options) {
        this.enabled = true;
        this.formDialog = null;
//        if (!_.isNil(options) && !_.isNil(options.infos)) {
//            _.extend(this.infos, options.infos);
//            delete options.infos;
//        }
        Button.prototype.initialize.apply(this, arguments);
    },
    /**
     * Methode: execute
     * Lance une action <Descartes.Action.PrinterParamsManager> avec comme interface une <Descartes.UI.PrinterSetupDialog>.
     */
    execute: function () {
        if (_.isNil(this.formDialog)) {
            this.createModalDialog();
        }
        if (!_.isNil(this.mapContentManager)) {
            if (_.isNil(this.mapContentManager.uiLayersTreeSimple)) {
                this.mapContentManager.addLayersTreeSimple("DescartesLayersTreeSimple", {});
            }
        } else {
            var opts = {};
            opts.onlySimpleVue = true;
            this.mapContentManager = new MapContentManager("DescartesLayersTreeSimple", this.mapContent, opts);
        }
        this.mapContentManager.redrawUI();
    },
    close: function () {
        //this.events.triggerEvent('close');
        this.formDialog = null;
    },
    createModalDialog: function () {
        var close = this.close.bind(this);
        var content = "<div id=\"DescartesLayersTreeSimple\" class=\"DescartesDialogContent\"></div>";
        var formDialogOptions = {
                id: this.id + '_dialog',
                title: "Arbre des couches simplifié",
                size: 'modal-lg',
                sendLabel: 'Fermer',
                otherBtnLabel: '',
                content: content
            };
        this.formDialog = new FormDialog(formDialogOptions);

        this.formDialog.open(close, close);
        var formSelector = '#' + this.formDialog.id + '_formDialog';

        var formDialogDiv = $(formSelector);

        var optionsResize = {
            minHeight: 300,
            minWidth: 280,
            alsoResize: formSelector + ' .DescartesDialogContent'
        };

        formDialogDiv.parent().resizable(optionsResize);
    },
    /**
     * Methode: setMapContentManager
     * Associe le gestionnaire du contenu de la carte au bouton.
     *
     * Paramètres:
     * mapContentManager - {<Descartes.Action.MapContentManager>} Gestionnaire du contenu de la carte (groupes et couches).
     */
    setMapContentManager: function (mapContentManager) {
        this.mapContentManager = mapContentManager;
    },
    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au bouton.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
    },
    CLASS_NAME: 'Descartes.Button.DisplayLayersTreeSimple'
});

module.exports = Class;
