/* global MODE */

require('./css/AddLayer.css');

var Utils = require('../../Utils/DescartesUtils');
var AbstractEditLayer = require('./' + MODE + '/AbstractEditLayer');

/**
 * Class: Descartes.Button.ContentTask.AddLayer
 * Classe définissant un bouton permettant la création d'une couche dans le contenu d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask.AbstractEditGroup>
 */
var Class = Utils.Class(AbstractEditLayer, {
    /**
     * Constructeur: Descartes.Button.ContentTask.AddLayer
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        AbstractEditLayer.prototype.initialize.apply(this, [options]);
    },
    CLASS_NAME: 'Descartes.Button.ContentTask.AddLayer'
});

module.exports = Class;
