/* global MODE, Descartes*/

var _ = require('lodash');
var log = require('loglevel');
var $ = require('jquery');

var Utils = require('../../../Utils/DescartesUtils');
var ContentTask = require('../../ContentTask');
var ModalFormDialog = require('../../../UI/' + MODE + "/ModalFormDialog");
var ConfirmDialog = require('../../../UI/' + MODE + "/ConfirmDialog");
var LayerConstants = require('../../../Model/LayerConstants');

var template = require('./templates/AbstractEditLayer.ejs');

/**
 * Class: Descartes.Button.ContentTask.AbstractEditLayer
 * Classe "abstraite" définissant un bouton permettant la création ou la modification d'une couche dans le contenu d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask>
 *
 * Classes dérivées:
 *  - <Descartes.Button.ContentTask.AddLayer>
 *  - <Descartes.Button.ContentTask.AlterLayer>
 */
var Class = Utils.Class(ContentTask, {
    panelCss: null,
    /**
     * Constructeur: Descartes.Button.ContentTask.AbstractEditLayer
     * Constructeur d'instances
     *
     * Paramtres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        this.panelCss = Descartes.UIBootstrap4Options.panelCss;
        ContentTask.prototype.initialize.apply(this, [options]);
    },
    /**
     * Methode: execute
     * Ouvre la boite de dialogue modale pour la saisie des propriétés du groupe.
     */
    execute: function () {

        var layerDefinition = {
            layerName: '',
            serverUrl: ''
        };
        if (!_.isNil(this.datas.layersDefinition)) {
            _.extend(layerDefinition, this.datas.layersDefinition[0]);
        }
        var options = {
            format: 'image/png',
            legend: '',
            metadataURL: '',
            visible: false,
            queryable: false,
            sheetable: false,
            maxScale: '',
            minScale: '',
            opacityMax: '100'
        };
        if (!_.isNil(this.datas.options)) {
            _.extend(options, this.datas.options);
        }
        if (_.isNil(this.datas.type)) {
            this.datas.type = LayerConstants.TYPE_WMS;
        }

        var type = this.datas.type;
        if (type === LayerConstants.TYPE_WFS) {
            type = LayerConstants.TYPE_WFS;
        }

        var formats = [
            {value: 'image/png', text: 'png'},
            {value: 'image/jpeg', text: 'jpeg'},
            {value: 'image/gif', text: 'gif'}
        ];

        var types = [
            this.getMessage('TYPE_WMS'),
            this.getMessage('TYPE_WMSC'),
            this.getMessage('TYPE_TMS'),
            this.getMessage('TYPE_WMTS'),
            this.getMessage('TYPE_WFS'),
            this.getMessage('TYPE_KML'),
            "", //couche GEOPORTAIL Plus pris en compte depuis la v5
            this.getMessage('TYPE_GeoJSON'),
            this.getMessage('TYPE_GenericVector'),
            this.getMessage('TYPE_Annotations'),
            this.getMessage('TYPE_OSM')
        ];

        var content = template({
            requiredParamsFielset: this.getMessage('REQUIRED_PARAMS_FIELSET'),
            titleInput: this.getMessage('TITLE_INPUT'),
            title: this.datas.title,
            panelCss: this.panelCss,
            layernameInput: this.getMessage('LAYERNAME_INPUT'),
            layerName: layerDefinition.layerName,
            serverInput: this.getMessage('SERVER_INPUT'),
            optionalParamsFielset: this.getMessage('OPTIONAL_PARAMS_FIELSET'),
            urlserver: layerDefinition.serverUrl,
            typeInput: this.getMessage('TYPE_INPUT'),
            type: types[type],
            versionInput: this.getMessage('VERSION_INPUT'),
            version: layerDefinition.serverVersion,
            stylesInput: this.getMessage('STYLES_INPUT'),
            styles: layerDefinition.layerStyles,
            formatInput: this.getMessage('FORMAT_INPUT'),
            format: options.format,
            formats: formats,
            legendInput: this.getMessage('LEGEND_INPUT'),
            legend: options.legend,
            metadataUrlInput: this.getMessage('METADATAURL_INPUT'),
            metadataURL: options.metadataURL,
            optionsFielset: this.getMessage("OPTIONS_FIELSET"),
            queryableInput: this.getMessage('QUERYABLE_INPUT'),
            queryable: options.queryable,
            sheetableInput: this.getMessage('SHEETABLE_INPUT'),
            sheetable: options.sheetable,
            maxScaleInput: this.getMessage('MAXSCALE_INPUT'),
            maxScale: options.maxScale,
            minScaleInput: this.getMessage('MINSCALE_INPUT'),
            minScale: options.minScale
        });

        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.title,
            size: 'modal-lg',
            formClass: 'form-horizontal',
            content: content
        });
        dialog.open($.proxy(this.sendDatas, this));
    },

    /**
     * Methode: sendDatas
     * Envoie les propriétés saisies du groupe au gestionnaire de contenu.
     */
    sendDatas: function (result) {
        log.debug('result=', result);
        var errors = [];

        if (_.isEmpty(result.title)) {
            errors.push(this.getMessage('EMPTY_TITLE_ERROR'));
        }

        if (_.isEmpty(result.layerName)) {
            errors.push(this.getMessage('EMPTY_LAYERNAME_ERROR'));
        }

        if (_.isEmpty(result.urlserver)) {
            errors.push(this.getMessage('EMPTY_SERVERURL_ERROR'));
        } else if (result.urlserver.toLowerCase().indexOf('http://') !== 0 && result.urlserver.toLowerCase().indexOf('https://') !== 0) {
            errors.push(this.getMessage('NOTHTTP_SERVERURL_ERROR'));
        }

        if (!_.isEmpty(result.legend)) {
            if (result.legend.toLowerCase().indexOf('http://') !== 0 && result.legend.toLowerCase().indexOf('https://') !== 0) {
                errors.push(this.getMessage('NOTHTTP_LEGEND_ERROR'));
            }
        }

        if (!_.isEmpty(result.metadataURL)) {
            if (result.metadataURL.toLowerCase().indexOf('http://') !== 0 && result.metadataURL.toLowerCase().indexOf('https://') !== 0) {
                errors.push(this.getMessage('NOTHTTP_METADATAURL_ERROR'));
            }
        }

        var maxScale = null;
        if (!_.isEmpty(result.maxScale)) {
            maxScale = parseInt(result.maxScale, 10);
            if (isNaN(maxScale) || maxScale < 1.0) {
                errors.push(this.getMessage("MAXSCALE_ERROR"));
            }
        }

        var minScale = null;
        if (!_.isEmpty(result.minScale)) {
            minScale = parseInt(result.minScale, 10);
            if (isNaN(minScale) || minScale < 1.0) {
                errors.push(this.getMessage("MINSCALE_ERROR"));
            }
        }

        if (!_.isNil(maxScale) && !_.isNil(minScale) && maxScale >= minScale) {
            errors.push(this.getMessage("SCALES_ERROR"));
        }


        if (_.isEmpty(errors)) {
            this.datas.title = result.title;
            this.datas.layersDefinition = [{
                    layerName: result.layerName,
                    serverUrl: result.urlserver
                }];

            this.datas.options = {
                alwaysVisible: false,
                queryable: result.queryable,
                activeToQuery: result.queryable,
                sheetable: result.sheetable,
                format: result.format
            };

            if (!_.isEmpty(result.version)) {
                this.datas.layersDefinition[0].serverVersion = result.version;
            }

            if (!_.isEmpty(result.styles)) {
                this.datas.layersDefinition[0].layerStyles = result.styles;
            }

            if (!_.isEmpty(result.legend)) {
                this.datas.options.legend = [result.legend];
            }

            if (!_.isEmpty(result.metadataURL)) {
                this.datas.options.metadataURL = result.metadataURL;
            }

            if (!_.isEmpty(result.maxScale)) {
                this.datas.options.maxScale = parseInt(result.maxScale, 10);
            }

            if (!_.isEmpty(result.minScale)) {
                this.datas.options.minScale = parseInt(result.minScale, 10);
            }

            this.events.triggerEvent('done');
            return true;
        } else {
            var message = '<p>' + this.getMessage('ERRORS_LIST') + '</p>';
            message += '<ul>';
            _.each(errors, function (error) {
                message += '<li>' + error + '</li>';
            });
            message += '</ul>';
            var confirmDialog = new ConfirmDialog({
                id: this.id + '_confirmDialog',
                title: 'Erreurs',
                message: message,
                type: 'default'
            });
            confirmDialog.open();
            return false;
        }

    },
    CLASS_NAME: 'Descartes.Button.ContentTask.AbstractEditLayer'
});

module.exports = Class;
