/* global MODE, Descartes */

require('../css/ChooseWmsLayers.css');

var _ = require('lodash');
var log = require('loglevel');
var ol = require('openlayers');
var $ = require('jquery');

var Utils = require('../../../Utils/DescartesUtils');
var ContentTask = require('../../ContentTask');
var ModalFormDialog = require('../../../UI/' + MODE + '/ModalFormDialog');
var ConfirmDialog = require('../../../UI/' + MODE + '/ConfirmDialog');
var WaitingDialog = require('../../../UI/' + MODE + '/WaitingDialog');

var template = require('./templates/ChooseWmsLayers.ejs');
var templateGrid = require('./templates/ChooseWmsLayersGrid.ejs');
var Message = require('../../../Messages');

var WmsLayer = require('../../../Model/WmsLayer');
var Url = require('../../../Model/Url');

var Messages = require('../../../Messages');

/**
 * Class: Descartes.Button.ContentTask.ChooseWmsLayers
 * Classe définissant un bouton permettant l'ajout de couches dans le contenu d'une carte, après découverte des capacités d'un serveur WMS.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask>
 */
var Class = Utils.Class(ContentTask, {
    /**
     * Propriete: layers
     * {Array(Object)} Liste des couches découvertes disponibles dans la projection de la carte (selon le format défini par la classe OpenLayers.Format.WMSCapabilities).
     */
    layers: null,
    /**
     * Propriete: wmsVersion
     * {String} Version du serveur WMS.
     */
    wmsVersion: null,
    /**
     * Propriete: wmsUrl
     * {String} Url du serveur WMS.
     */
    wmsUrl: null,

    waitingMsg: true,

    defaultUIVersion: '1.3.0',

    _useUrlCapabilities: true,

    displayClasses: {
        globalClassName: 'DescartesWmsChooserContainer',
        tableClassName: 'DescartesWmsChooserTable',
        headersRowClassName: 'DescartesWmsChooserHeaderRow',
        headerClassName: 'DescartesWmsChooserHeaderCell',
        datasRowClassName: 'DescartesWmsChooserDataRow',
        textClassName: 'DescartesWmsChooserDataTextCell',
        selectClassName: 'DescartesWmsChooserDataSelectCell',
        checkBoxClassName: 'DescartesWmsChooserDataCheckBoxCell'
    },
    /**
     * Constructeur: Descartes.Button.ContentTask.ChooseWmsLayers
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        ContentTask.prototype.initialize.apply(this, [options]);
    },
    /**
     * Methode: execute
     * Ouvre la boite de dialogue modale pour la saisie des propriétés du groupe.
     */
    execute: function () {
        /*var testUrls = [
            'http://ws.carmencarto.fr/WMS/119/fxx_inpn?',
            'http://mapdmz.brgm.fr/cgi-bin/mapserv?map=/carto/infoterre/mapFiles/geocat_metr.map',
            'http://mapserveur.application.developpement-durable.gouv.fr/map/mapserv?map=/opt/data/carto/cartelie/prod/PNE_IG/OSM_2.www.map',
            'https://openlayers.org/en/v4.1.1/examples/data/ogcsample.xml'
        ];*/
        var content = template({
            labelUrlText: this.getMessage('PROMPT_URL_SERVER'),
            url: '',
            //url: testUrls[1],
            labelVersionText: this.getMessage('PROMPT_VERSION_SERVER'),
            version: this.defaultUIVersion,
            labelNoUseUrlCapabilitiesText: this.getMessage('PROMPT_USE_URL_SERVER')
        });
        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.title,
            content: content
        });
        dialog.open($.proxy(this.requestServer, this));
    },
    requestServer: function (result) {
        var url = new Url(result.url);
        var version = result.version;
        this._useUrlCapabilities = !$("#DescartesNoUseUrlCapabilitiesCheckBox")[0].checked;
        this.wmsUrl = result.url;
        url.alterOrAddParam('REQUEST', 'GetCapabilities');
        url.alterOrAddParam('SERVICE', 'WMS');
        url.alterOrAddParam('VERSION', version);
        var urlProxy = new Url(Descartes.PROXY_SERVER);
        var urlProxyString = urlProxy.getUrlString();
        if (urlProxyString.indexOf('?') === -1) {
            urlProxyString += '?';
        }

        var finalUrl = urlProxyString + url.getUrlString();

        if (this.waitingMsg) {
            this.showWaitingMessage();
        }

        $.ajax({
            url: finalUrl
        }).done($.proxy(this.showLayers, this)).fail($.proxy(this.handleError, this));
    },
    handleError: function (err, data) {
        this.closeWaitingMessage();
        var confirmDialog = new ConfirmDialog({
            id: this.id + '_ErrorDialog',
            title: 'Erreurs',
            message: Message.Descartes_GET_CAPABILITIES_FAILURE,
            type: 'default'
        });
        confirmDialog.open(this.closeWaitingMessage.bind(this));
        log.error('err=', err);
        log.error('data=', data);
        return false;
    },
    showLayers: function (e, status, response) {
        this.closeWaitingMessage();
        var parser = new ol.format.WMSCapabilities();
        Utils.appendNodeParsingForFormat(parser);
        var data = response.responseText;
        if (!_.isNil(response.responseXML) && !_.isNil(response.responseXML.documentElement)) {
            data = response.responseXML;
        }
        var wmsCapabilities;
        try {
            wmsCapabilities = parser.read(data);
        } catch (e) {
            var errorDialog = new ConfirmDialog({
                id: this.id + '_dialog',
                title: this.getMessage('DIALOG_TITLE'),
                message: Message.Descartes_GET_CAPABILITIES_FAILURE,
                type: 'default'
            });
            errorDialog.open(this.closeWaitingMessage().bind(this));
            return false;
        }
        if (!_.isNil(wmsCapabilities.version)) {
            this.wmsVersion = wmsCapabilities.version;
        }

        this.layers = [];
        var that = this;
        if (!_.isNil(wmsCapabilities.Capability) && !_.isNil(wmsCapabilities.Capability.Layer)) {
            var capability = wmsCapabilities.Capability;
            var layers = this.getLayers(capability.Layer);

            var urls = this.getHref(capability);
            var formats = this.getFormats(capability);
            _.each(layers, function (layer) {
                if (that._useUrlCapabilities) {
					that.wmsUrl = urls.get;
				}
                var wmsLayer = new WmsLayer(layer, that.wmsUrl, formats);
                that.layers.push(wmsLayer);
            });
        }
        var mapProjection = this.olMap.getView().getProjection();
        this.layers = this.layers.filter(function (element) {
            return (element.isValid(mapProjection));
        });
        if (this.layers.length > 0) {

            var template = templateGrid({
                id: this.id,
                data: this.layers,
                columns: [{
                        title: this.getMessage('TITLE_COL_NAME')
                    }, {
                        title: this.getMessage('FORMAT_COL_NAME')
                    }, {
                        title: this.getMessage('MINSCALE_COL_NAME')
                    }, {
                        title: this.getMessage('MAXSCALE_COL_NAME')
                    }, {
                        title: this.getMessage('ABSTRACT_COL_NAME')
                    }, {
                        title: this.getMessage('STYLE_COL_NAME')
                    }, {
                        title: this.getMessage('KEYWORDS_COL_NAME')
                    }, {
                        title: this.getMessage('METADATA_COL_NAME')
                    }]
            });
            var dialog = new ModalFormDialog({
                id: this.id + '_dialogGrid',
                title: this.getMessage('DIALOG_TITLE'),
                size: 'modal-lg',
                content: template
            });
            dialog.open($.proxy(this.sendDatas, this), $.proxy(this.cancel, this));
            $('#selectAll').on('click', function () {
                var checked = $(this).prop('checked');
                $('#' + that.id + '_body').find('input[name^="selected"][value="true"]').prop('checked', checked);
            });
        } else {
            this.closeWaitingMessage();
            var message = Messages.Descartes_GET_CAPABILITIES_NO_LAYERS;
            var confirmDialog = new ConfirmDialog({
                id: this.id + '_dialog',
                title: this.getMessage('DIALOG_TITLE'),
                message: message,
                type: 'default'
            });
            confirmDialog.open(this.closeWaitingMessage.bind(this));
        }
        return true;
    },
    getLayers: function (layer, parentLayer) {
        var result = [];
        if (!_.isNil(layer)) {
            if (!_.isNil(layer.Layer) && _.isArray(layer.Layer)) {
                var that = this;
                _.each(layer.Layer, function (aLayer) {
                    result = result.concat(that.getLayers(aLayer, layer));
                });
            }
            if (!_.isNil(parentLayer)) {
                layer.parent = parentLayer;
            }
            result.push(layer);
        }
        return result;
    },
    getHref: function (capability) {
        var request = capability.Request;
        var getMap = request.GetMap;
        var http = getMap.DCPType[0].HTTP;
        var get = http.Get.OnlineResource;
        var post = null;
        if (!_.isNil(http.Post)) {
            post = http.Post.OnlineResource;
        }
        return {
            get: get,
            post: post
        };
    },
    getFormats: function (capability) {
        var request = capability.Request;
        var getMap = request.GetMap;
        return getMap.Format;
    },
    /**
     * Methode: sendDatas
     * Envoie les propriétés saisies du groupe au gestionnaire de contenu.
     */
    sendDatas: function (result) {
        this.closeWaitingMessage();
        this.datas = [];

        var that = this;
        _.forOwn(result, function (value, key, object) {
            if (value === true) {
                var split = key.split('selected_');
                var index = Number(split[1]);
                var format = object['format_' + index];
                var style = object['style_' + index];

                var layer = that.layers[index];

                if (!_.isNil(that.wmsVersion)) {
                    layer.serverVersion = that.wmsVersion;
                }

                layer.selectedFormat = format;
                layer.selectedStyle = style;

                var layerDatas = layer.toJSON();

                that.datas.push(layerDatas);
            }
        });

        if (this.datas.length > 0) {
            this.events.triggerEvent('done');
        }
    },

    showWaitingMessage: function () {
        this.waitingDialog = new WaitingDialog({
            message: this.getMessage('WAITING_MESSAGE')
        });
        this.waitingDialog.open();
    },

    /**
     * Methode: closeWaitingMessage
     * Ferme le message d'attente.
     */
    closeWaitingMessage: function () {
        if (this.waitingDialog) {
            this.waitingDialog.close();
        }
    },

    cancel: function () {
        this.closeWaitingMessage();
    },

    CLASS_NAME: 'Descartes.Button.ContentTask.ChooseWmsLayers'
});

module.exports = Class;
