/* global MODE, Descartes */

require('../css/ExportVectorLayer.css');

var _ = require('lodash');
var log = require('loglevel');
var ol = require('openlayers');
var $ = require('jquery');

var Utils = require('../../../Utils/DescartesUtils');
var ContentTask = require('../../ContentTask');
var ModalFormDialog = require('../../../UI/' + MODE + '/ModalFormDialog');
var ConfirmDialog = require('../../../UI/' + MODE + '/ConfirmDialog');
var WaitingDialog = require('../../../UI/' + MODE + '/WaitingDialog');

var template = require('./templates/ExportVectorLayer.ejs');
var Message = require('../../../Messages');

var Url = require('../../../Model/Url');

var Messages = require('../../../Messages');

/**
 * Class: Descartes.Button.ContentTask.ExportVectorLayer
 * Classe définissant un bouton permettant l'export KML/GeoJSON d'une couche du contenu d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask>
 */
var Class = Utils.Class(ContentTask, {

	listExportFormat: [
       {
          id: "kml", label: 'KML', olClass: ol.format.KML, fileName: "export_epsg_4326.kml"
       },
       {
          id: "geojson", label: 'GeoJson', olClass: ol.format.GeoJSON, fileName: "export_epsg_4326.geojson"
       }
    ],
    _exportLayer: null,
    /**
     * Constructeur: Descartes.Button.ContentTask.ExportVectorLayer
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        ContentTask.prototype.initialize.apply(this, [options]);
    },
    /**
     * Methode: execute
     * Ouvre la boite de dialogue modale pour la saisie des propriétés du groupe.
     */
    execute: function () {

        if (this.datas && !_.isEmpty(this.datas)) {
            this._exportLayer = this.datas;
        }
        var content = template({
            id: "exportvectorlayer",
            labelText: this.getMessage('FORMAT'),
            listExportFormat: this.listExportFormat
        });
        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.title,
            content: content
        });
        dialog.open($.proxy(this.runExport, this), $.proxy(this.close, this));
    },
    runExport: function (form) {
        var selectedFormat = form.format;
        var Classname = null;
        var filename = "export_epsg_4326.txt";
        for (var i = 0, len = this.listExportFormat.length; i < len; i++) {
            if (selectedFormat === this.listExportFormat[i].id) {
                Classname = this.listExportFormat[i].olClass;
                filename = this.listExportFormat[i].fileName;
                break;
            }
        }

        if (Classname) {
            var optionsFormat = {};
            //if (selectedFormat === "kml") {
                optionsFormat.featureProjection = this.olMap.getView().getProjection().getCode();
                optionsFormat.defaultDataProjection = 'EPSG:4326'; //flux kml toujours en EPSG:4326 mais pour l'instant, on force aussi pour le GeoJson
            //}
            var format = new Classname();
            var features = this._exportLayer.OL_layers[0].getSource().getFeatures();

            var flux = format.writeFeatures(features, optionsFormat);
            Utils.download(filename, flux);
        }

    },

    close: function () {
		//à surcharger si besoin
    },
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
    },

    CLASS_NAME: 'Descartes.Button.ContentTask.ExportVectorLayer'
});

module.exports = Class;
