/* global MODE Descartes*/

require('../css/RemoveLayer.css');

var _ = require('lodash');
var $ = require('jquery');

var Utils = require('../../../Utils/DescartesUtils');
var ContentTask = require('../../ContentTask');
var ModalFormDialog = require('../../../UI/' + MODE + "/ModalFormDialog");
var LayerConstants = require('../../../Model/LayerConstants');

var template = require('./templates/RemoveLayer.ejs');

/**
 * Class: Descartes.Button.ContentTask.RemoveLayer
 * Classe "abstraite" définissant un bouton permettant la suppression d'une couche dans le contenu d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask>
 */
var Class = Utils.Class(ContentTask, {
	panelCss: null,
	btnCssSubmit: null,

    /**
     * Constructeur: Descartes.Button.ContentTask.RemoveLayer
     * Constructeur d'instances
     *
     * Paramtres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        this.panelCss = Descartes.UIBootstrap4Options.panelCss;
        this.btnCssSubmit = Descartes.UIBootstrap4Options.btnCssRemove;
        ContentTask.prototype.initialize.apply(this, [options]);
    },
    /**
     * Methode: execute
     * Ouvre la boite de dialogue modale pour la saisie des propriétés du groupe.
     */
    execute: function () {

        var layerDefinition = {
            layerName: '',
            serverUrl: ''
        };
        if (!_.isNil(this.datas.layersDefinition)) {
            _.extend(layerDefinition, this.datas.layersDefinition[0]);
        }
        var options = {
            format: 'image/png',
            legend: '',
            metadataURL: '',
            visible: false,
            queryable: false,
            sheetable: false,
            maxScale: '',
            minScale: '',
            opacityMax: '100'
        };
        if (!_.isNil(this.datas.options)) {
            _.extend(options, this.datas.options);
        }
        if (_.isNil(this.datas.type)) {
            this.datas.type = LayerConstants.TYPE_WMS;
        }

        var type = this.datas.type;
        if (type === LayerConstants.TYPE_WFS) {
            type = LayerConstants.TYPE_WFS;
        }

        var formats = [
            {value: 'image/png', text: 'png'},
            {value: 'image/jpeg', text: 'jpeg'},
            {value: 'image/gif', text: 'gif'}
        ];

        var types = [
            this.getMessage('TYPE_WMS'),
            this.getMessage('TYPE_WMSC'),
            this.getMessage('TYPE_TMS'),
            this.getMessage('TYPE_WMTS'),
            this.getMessage('TYPE_WFS'),
            this.getMessage('TYPE_KML'),
            "", //couche GEOPORTAIL Plus pris en compte depuis la v5
            this.getMessage('TYPE_GeoJSON'),
            this.getMessage('TYPE_GenericVector'),
            this.getMessage('TYPE_Annotations'),
            this.getMessage('TYPE_OSM')
        ];

        var content = template({
            requiredParamsFielset: this.getMessage('REQUIRED_PARAMS_FIELSET'),
            titleInput: this.getMessage('TITLE_INPUT'),
            title: this.datas.title,
            panelCss: this.panelCss,
            layernameInput: this.getMessage('LAYERNAME_INPUT'),
            layerName: layerDefinition.layerName,
            serverInput: this.getMessage('SERVER_INPUT'),
            optionalParamsFielset: this.getMessage('OPTIONAL_PARAMS_FIELSET'),
            urlserver: layerDefinition.serverUrl,
            typeInput: this.getMessage('TYPE_INPUT'),
            type: types[type],
            versionInput: this.getMessage('VERSION_INPUT'),
            version: layerDefinition.serverVersion,
            stylesInput: this.getMessage('STYLES_INPUT'),
            styles: layerDefinition.layerStyles,
            formatInput: this.getMessage('FORMAT_INPUT'),
            format: options.format,
            formats: formats,
            legendInput: this.getMessage('LEGEND_INPUT'),
            legend: options.legend,
            metadataUrlInput: this.getMessage('METADATAURL_INPUT'),
            metadataURL: options.metadataURL,
            optionsFielset: this.getMessage("OPTIONS_FIELSET"),
            queryableInput: this.getMessage('QUERYABLE_INPUT'),
            queryable: options.queryable,
            sheetableInput: this.getMessage('SHEETABLE_INPUT'),
            sheetable: options.sheetable,
            maxScaleInput: this.getMessage('MAXSCALE_INPUT'),
            maxScale: options.maxScale,
            minScaleInput: this.getMessage('MINSCALE_INPUT'),
            minScale: options.minScale
        });

        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.title,
            size: 'modal-lg',
            sendLabel: this.getMessage('OK_BUTTON'),
            btnCssSubmit: this.btnCssSubmit,
            formClass: 'form-horizontal',
            content: content
        });
        dialog.open($.proxy(this.sendDatas, this));
    },

    /**
     * Methode: sendDatas
     * Envoie les propriétés saisies du groupe au gestionnaire de contenu.
     */
    sendDatas: function () {
        this.events.triggerEvent('done');
        return true;
    },
    CLASS_NAME: 'Descartes.Button.ContentTask.RemoveLayer'
});

module.exports = Class;
