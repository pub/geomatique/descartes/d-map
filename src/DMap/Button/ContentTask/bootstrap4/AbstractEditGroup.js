/* global MODE*/

var _ = require('lodash');

var Utils = require('../../../Utils/DescartesUtils');
var ContentTask = require('../../ContentTask');
var ModalFormDialog = require('../../../UI/' + MODE + "/ModalFormDialog");

var template = require('./templates/AbstractEditGroup.ejs');

/**
 * Class: Descartes.Button.ContentTask.AbstractEditGroup
 * Classe "abstraite" définissant un bouton permettant la création ou la modification d'un groupe dans le contenu d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask>
 *
 * Classes dérivées:
 *  - <Descartes.Button.ContentTask.AddGroup>
 *  - <Descartes.Button.ContentTask.AlterGroup>
 */
var Class = Utils.Class(ContentTask, {
    /**
     * Constructeur: Descartes.Button.ContentTask.AbstractEditGroup
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        ContentTask.prototype.initialize.apply(this, [options]);
    },
    /**
     * Methode: execute
     * Ouvre la boite de dialogue modale pour la saisie des propriétés du groupe.
     */
    execute: function () {

        var content = template({
            labelText: this.getMessage('TITLE_INPUT'),
            value: this.datas.title
        });

        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.title,
            content: content
        });
        dialog.open(this.sendDatas.bind(this));
    },

    /**
     * Methode: sendDatas
     * Envoie les propriétés saisies du groupe au gestionnaire de contenu.
     */
    sendDatas: function (result) {
        this.datas.title = result.title;
        this.datas.opened = true;
        if (!_.isNil(this.datas.title) && !_.isEmpty(this.datas.title)) {
            this.events.triggerEvent('done');
        } else {
            alert(this.getMessage('TITLE_ERROR'));
        }
    },
    CLASS_NAME: 'Descartes.Button.ContentTask.AbstractEditGroup'
});

module.exports = Class;
