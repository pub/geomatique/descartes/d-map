/* global MODE */
require('./css/AlterLayer.css');

var Utils = require('../../Utils/DescartesUtils');
var AbstractEditLayer = require('./' + MODE + '/AbstractEditLayer');

/**
 * Class: Descartes.Button.ContentTask.AlterLayer
 * Classe définissant un bouton permettant la modification d'une couche dans le contenu d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask.AbstractEditLayer>
 */
var Class = Utils.Class(AbstractEditLayer, {
    /**
     * Constructeur: Descartes.Button.ContentTask.AlterLayer
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        AbstractEditLayer.prototype.initialize.apply(this, [options]);
    },
    CLASS_NAME: 'Descartes.Button.ContentTask.AlterLayer'
});

module.exports = Class;
