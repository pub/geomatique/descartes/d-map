/* global MODE */

var AddGroup = require('./AddGroup');
var AddLayer = require('./AddLayer');
var AlterGroup = require('./AlterGroup');
var AlterLayer = require('./AlterLayer');
var ChooseWmsLayers = require('./' + MODE + '/ChooseWmsLayers');
var AbstractEditGroup = require('./' + MODE + '/AbstractEditGroup');
var AbstractEditLayer = require('./' + MODE + '/AbstractEditLayer');
var RemoveGroup = require('./RemoveGroup');
var RemoveLayer = require('./' + MODE + '/RemoveLayer');
var ExportVectorLayer = require('./' + MODE + '/ExportVectorLayer');

module.exports = {
    AddGroup: AddGroup,
    AddLayer: AddLayer,
    AlterGroup: AlterGroup,
    AlterLayer: AlterLayer,
    ChooseWmsLayers: ChooseWmsLayers,
    AbstractEditGroup: AbstractEditGroup,
    AbstractEditLayer: AbstractEditLayer,
    RemoveGroup: RemoveGroup,
    RemoveLayer: RemoveLayer,
    ExportVectorLayer: ExportVectorLayer
};
