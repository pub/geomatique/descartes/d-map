/* global MODE */

require('./css/AddGroup.css');

var Utils = require('../../Utils/DescartesUtils');
var AbstractEditGroup = require('./' + MODE + '/AbstractEditGroup');

/**
 * Class: Descartes.Button.ContentTask.AddGroup
 * Classe définissant un bouton permettant la cr"ation d'un groupe dans le contenu d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask.AbstractEditGroup>
 */
var Class = Utils.Class(AbstractEditGroup, {
    /**
     * Constructeur: Descartes.Button.ContentTask.AddGroup
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        AbstractEditGroup.prototype.initialize.apply(this, [options]);
    },
    CLASS_NAME: 'Descartes.Button.ContentTask.AddGroup'
});

module.exports = Class;
