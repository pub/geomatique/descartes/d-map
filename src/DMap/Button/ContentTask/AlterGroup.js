/* global MODE */

require('./css/AlterGroup.css');

var Utils = require('../../Utils/DescartesUtils');
var AbstractEditGroup = require('./' + MODE + '/AbstractEditGroup');

/**
 * Class: Descartes.Button.ContentTask.AlterGroup
 * Classe définissant un bouton permettant la modification d'un groupe dans le contenu d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask.AbstractEditGroup>
 */
var Class = Utils.Class(AbstractEditGroup, {
    /**
     * Constructeur: Descartes.Button.ContentTask.AlterGroup
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        AbstractEditGroup.prototype.initialize.apply(this, [options]);
    },
    CLASS_NAME: 'Descartes.Button.ContentTask.AlterGroup'
});

module.exports = Class;
