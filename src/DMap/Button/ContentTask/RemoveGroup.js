/* global MODE */

var Utils = require('../../Utils/DescartesUtils');
var ContentTask = require('../ContentTask');
var ConfirmDialog = require('../../UI/' + MODE + '/ConfirmDialog');

require('./css/RemoveGroup.css');


/**
 * Class: Descartes.Button.ContentTask.RemoveGroup
 * Classe définissant un bouton permettant la suppression d'un groupe dans le contenu d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button.ContentTask>
 */
var Class = Utils.Class(ContentTask, {
    /**
     * Constructeur: Descartes.Button.ContentTask.RemoveGroup
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        ContentTask.prototype.initialize.apply(this, [options]);
    },
    execute: function () {
        var that = this;
        var message = this.getMessage('TITLE_INPUT') + ' : ' + this.datas.title;
        if (this.datas.infosContent && this.datas.infosContent.nbLayer > 0) {
            message += this.getMessage('MSG_WARNING');
            if (this.datas.infosContent.nbLayer == 1) {
                message += "1 couche";
            } else {
                message += this.datas.infosContent.nbLayer + " couches";
            }
            if (this.datas.infosContent.nbLayerAddedByUser > 0) {
                if (this.datas.infosContent.nbLayerAddedByUser == 1) {
                    message += " (dont 1 ajoutée par vous-même).";
                } else {
                    message += " (dont " + this.datas.infosContent.nbLayerAddedByUser + " ajoutés par vous-même).";
                }
            } else {
                    message += ".";
            }
            message += this.getMessage('MSG_WARNING2');
        }

        var confirmDialog = new ConfirmDialog({
            id: this.id + '_dialog',
            title: this.getMessage('DIALOG_TITLE'),
            message: message,
            type: 'remove'
        });

        confirmDialog.open(function (result) {
            if (result) {
                 that.sendDatas();
            }
        });
    },
    sendDatas: function () {
        this.events.triggerEvent('done');
        return true;
    },
    CLASS_NAME: 'Descartes.Button.ContentTask.RemoveGroup'
});

module.exports = Class;
