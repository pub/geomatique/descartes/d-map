/* global MODE, Descartes */

var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var Button = require('../Button/' + MODE + '/Button');
var MessagesConstants = require('../Messages');
var ExternalCallsUtils = require('../Core/ExternalCallsUtils');

require('./css/ExportPNG.css');

/**
 * Class: Descartes.Button.ExportPNG
 * Classe définissant un bouton permettant d'exporter la carte courante sous forme de fichier PNG.
 *
 * Hérite de:
 *  - <Descartes.Button>
 */
var Class = Utils.Class(Button, {
    /**
     * Propriete: defaultInfos
     * {Object} Objet JSON stockant les informations complémentaires par défaut à insérer dans le fichier PDF.
     *
     * :
     * title - Titre de la carte (utilisé pour le nom du fichier généré).
     * copyright - Texte du copyright à insérer en sur-impression de l'image.
     */
    defaultInfos: {
        title: 'descartes',
        copyright: ''
    },
    /**
     * Propriete: infos
     * {Object} Objet JSON stockant les informations complémentaires à insérer dans le fichier PNG.
     *
     * :
     * Construit à partir de defaultInfos, puis surchargé en présence d'options complémentaires.
     */
    infos: null,

    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,
    /**
     * Constructeur: Descartes.Button.ExportPNG
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * infos - Objet JSON stockant les informations complémentaires à insérer dans le fichier PNG.
     */
    initialize: function (options) {
        this.enabled = true;
        this.infos = _.extend({}, this.defaultInfos);
        if (!_.isNil(options) && !_.isNil(options.infos)) {
            _.extend(this.infos, options.infos);
            delete options.infos;
        }
        Button.prototype.initialize.apply(this, arguments);
    },
    /**
     * Methode: execute
     * Lance la génération du PNG grâce au service d'exportation PNG.
     */
    execute: function () {
        if (!_.isNil(this.mapContent)) {
            var layers = this.mapContent.getVisibleLayers().sort(function (a, b) {
                return b.displayOrder - a.displayOrder;
            });
            var exportParams = ExternalCallsUtils.writeExportPostBody(layers, this.olMap, this.infos);

            if (!_.isNil(exportParams)) {
                ExternalCallsUtils.openExportPopup(Descartes.EXPORT_PNG_SERVER, exportParams);
            } else {
                alert(MessagesConstants.Descartes_EXPORT_ERROR);
            }
            document.getElementById(this.id).blur();
        }
    },

    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au bouton.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
    },
    CLASS_NAME: 'Descartes.Button.ExportPNG'
});

module.exports = Class;
