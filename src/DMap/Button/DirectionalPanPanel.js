/* global MODE */

var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var Button = require('../Button/' + MODE + '/Button');

var DirectionalPan = require('./DirectionalPan');
var DirectionalPanConstants = require('./DirectionalPanConstants');

require('./css/DirectionalPanPanel.css');

/**
 * Class: Descartes.Button.DirectionalPanPanel
 * Classe construisant un ensemble de boutons permettant un déplacement incrémental de la carte dans une des huit directions d'une "rose des vents".
 *
 * :
 * La rose des vents est construites grâce à huit <Descartes.Button.DirectionalPan>.
 *
 */
var Class = Utils.Class(Button, {

    /**
     * Propriete: horizontalSlideFactor
     * {Integer} Valeur en pixels des déplacements horizontaux.
     */
    horizontalSlideFactor: 100,

    /**
     * Propriete: verticalSlideFactor
     * {Integer} Valeur en pixels des déplacements verticaux.
     */
    verticalSlideFactor: 50,

    buttons: [],

    /**
     * Constructeur: Descartes.Button.DirectionalPanPanel
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * horizontalSlideFactor - {Integer} Valeur en pixels des déplacements horizontaux.
     * verticalSlideFactor - {Integer} Valeur en pixels des déplacements verticaux.
     */
    initialize: function (options) {
        Button.prototype.initialize.apply(this, [options]);

        var panOptions = {
            horizontalSlideFactor: this.horizontalSlideFactor,
            verticalSlideFactor: this.verticalSlideFactor,
            duration: this.duration
        };

        var pan = new DirectionalPan(DirectionalPanConstants.NORTH, panOptions);
        this.addButton(pan);

        pan = new DirectionalPan(DirectionalPanConstants.SOUTH, panOptions);
        this.addButton(pan);

        pan = new DirectionalPan(DirectionalPanConstants.EAST, panOptions);
        this.addButton(pan);

        pan = new DirectionalPan(DirectionalPanConstants.WEST, panOptions);
        this.addButton(pan);

        pan = new DirectionalPan(DirectionalPanConstants.NORTHEAST, panOptions);
        this.addButton(pan);

        pan = new DirectionalPan(DirectionalPanConstants.NORTHWEST, panOptions);
        this.addButton(pan);

        pan = new DirectionalPan(DirectionalPanConstants.SOUTHEAST, panOptions);
        this.addButton(pan);

        pan = new DirectionalPan(DirectionalPanConstants.SOUTHWEST, panOptions);
        this.addButton(pan);
    },

    addButton: function (button) {
        this.buttons.push(button);
    },

    /*
     * Méthode deactivate
     */
    deactivate: function () {
        this.div.style.display = 'none';
        _.each(this.buttons, function (button) {
            button.disable();
        });
    },

    /*
     * Méthode activate
     */
    activate: function () {
        this.div.style.display = 'block';
        _.each(this.buttons, function (button) {
            button.enable();
        });
    },

    setMap: function (map) {
        this.olMap = map;

        this.div = document.createElement('div');
        this.div.className = 'ol-control ' + this.displayClass;
        this.div.className = this.displayClass;
        this.div.id = this.id;

        this.olMap.getViewport().appendChild(this.div);

        var that = this;
        _.each(this.buttons, function (button) {
            button.createButton(that.div);
            button.setMap(map);
        });
        this.activate();
    },

    CLASS_NAME: 'Descartes.Button.DirectionalPanPanel'
});


module.exports = Class;

