/* global Descartes */
var log = require('loglevel');
var $ = require('jquery');

var Utils = require('../../Utils/DescartesUtils');
var Button = require('../../Core/Button');

var buttonTemplate = require('./templates/Button.ejs');

/**
 * Class: Descartes.Button
 * Classe "abstraite" définissant un bouton pouvant être disponible ou non selon le contexte.
 *
 * :
 * Le bouton doit être placé dans une barre d'outils définie par la classe <Descartes.ToolBar>.
 *
 * Hérite de:
 *  - <Descartes.I18N>
 */
var Class = Utils.Class(Button, {
    parent: null,

    btnCss: null,

    enabledClass: '',
    /**
     * Constructeur: Descartes.Button
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * title - {String} Texte de l'info-bulle du bouton.
     */
    initialize: function (options) {
        this.btnCss = Descartes.UIBootstrap4Options.btnCssToolBar;
        Button.prototype.initialize.apply(this, [options]);
    },
    createButton: function (div) {
        this.parent = div;
        log.debug('createButton ', this.title);
        //$('<span/>', {id: this.id}).appendTo(div);
        this.templateButton();
    },
    updateButton: function () {
        log.debug('updateButton ', this.title);
        this.templateButton(true);
    },
    templateButton: function (update) {
        var enabledClass = 'disabled';
        var buttonClass = this.displayClass + 'ItemInactive';
        if (this.enabled === true) {
            enabledClass = this.enabledClass;
            buttonClass = this.displayClass + 'ItemActive';
        }
        var template = buttonTemplate({
            id: this.id,
            title: this.title,
            btnCss: this.btnCss,
            displayClass: buttonClass,
            enableClass: enabledClass
        });
        if (update) {
            $('#' + this.id).replaceWith(template);
        } else {
            $(this.parent).append(template);
        }
        $('#' + this.id).click($.proxy(this.trigger, this));
    },
    CLASS_NAME: 'Descartes.Button'
});

module.exports = Class;
