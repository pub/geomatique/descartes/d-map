/* global MODE, Descartes*/

var _ = require('lodash');
var $ = require('jquery');

var Utils = require('../../Utils/DescartesUtils');
var Url = require('../../Model/Url');
var Button = require('../../Button/' + MODE + '/Button');
var ModalFormDialog = require('../../UI/' + MODE + '/ModalFormDialog');
var MessagesConstants = require('../../Messages');

var template = require('./templates/ShareLinkMap.ejs');

require('../css/ShareLinkMap.css');

/**
 * Class: Descartes.Button.ShareLinkMap
 * Classe définissant un bouton permettant de partager le context courant d'une carte.
 *
 * Hérite de:
 *  - <Descartes.Button>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'done' de la classe <Descartes.ModalDialog> déclenche la méthode <sendDatas>.
 *  - l'événement 'cancelled' de la classe <Descartes.ModalDialog> déclenche la méthode <deleteDialog>.
 *
 */
var Class = Utils.Class(Button, {

    prefixNameFile: "dsharelink",
    listTtl: [
        {label: "7", value: "007"},
        {label: "30", value: "030"},
        {label: "90", value: "090"},
        {label: "180", value: "180"}
    ],
    defaultTtl: "030",

    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,
    /**
     * Constructeur: Descartes.Button.ExportPDF
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * infos - Objet JSON stockant les informations complémentaires à insérer dans le fichier PDF.
     */
    initialize: function (options) {
        this.enabled = true;
        this.btnCss = Descartes.UIBootstrap4Options.btnCss;
        Button.prototype.initialize.apply(this, arguments);
    },
    /**
     * Methode: execute
     * Lance une action <Descartes.Action.PrinterParamsManager> avec comme interface une <Descartes.UI.PrinterSetupDialog>.
     */
    execute: function () {
        var that = this;
        var content = template({
            btnCss: this.btnCss,
            labelTtl: this.getMessage("DIALOG_LABEL_TTL"),
            listTtl: this.listTtl,
            defaultTtl: this.defaultTtl,
            labelBtnGenerate: this.getMessage("DIALOG_BTN_GENERATE"),
            labelLink: this.getMessage("DIALOG_LABEL_LINK")
        });
        var dialog = new ModalFormDialog({
            id: this.id + '_dialog',
            title: this.title,
            content: content,
            isContentForm: false
        });
        dialog.open();

        $('#DescartesShareLinkGenerateBtn').on('click', function () {
            that.generateUrl();
        });
        $('#DescartesShareLinkCopyBtn').on('click', function () {
            that.copyUrl();
        });

    },
    generateUrl: function () {
        if (!_.isNil(this.mapContent)) {
                var ttl = $('#DescartesShareLinkTTL').val();
                var date = Utils.formatCurrentDate();
                var olMap = this.olMap;
                var vue = {
                    nom: this.prefixNameFile,
                    url: '',
                    contextFile: ''
                };
                var size = olMap.getSize();
                var bbox = olMap.getView().calculateExtent(size);

                var context = {
                    items: this.mapContent.serialize(),
                    bbox: {
                        xMin: bbox[0],
                        yMin: bbox[1],
                        xMax: bbox[2],
                        yMax: bbox[3]
                    },
                    size: {
                        w: size[0],
                        h: size[1]
                    },
                    zoom: olMap.getView().getZoom(),
                    infosSiteOrig: {
                        locationHref: document.location.href.split('#')[0],
                        action: "dShareLinkMap",
                        date: date,
                        ttl: ttl
                   }
                };

                var self = this;

                var xhr = new XMLHttpRequest();
                xhr.open('POST', Descartes.CONTEXT_MANAGER_SERVER);
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                xhr.onload = function () {
                    if (xhr.status === 200) {
                       self.displayVue(vue, xhr.responseText);
                    } else {
                        alert("Problème de génération du lien");
                    }
                };
                xhr.send(Utils.encode({
                    view: encodeURI(this.prefixNameFile + "ttl" + ttl + date),
                    context: JSON.stringify(context)
                }));

        }
    },
    copyUrl: function () {
        if ($('#DescartesShareLinkInputUrl').val() !== "") {
           if (navigator.clipboard) { //localhost or site https
               navigator.clipboard.writeText($('#DescartesShareLinkInputUrl').val());
           } else { //site http
               $('#DescartesShareLinkInputUrl').select();
               document.execCommand("copy");
           }
           $("#DescartesShareLinkLabel").text("Ce lien a été copié dans le presse papier");
        }
    },
    displayVue: function (vue, response) {
        response = JSON.parse(response);
        vue.contextFile = response.contextFile;
        vue.maxDate = null;
        if (!_.isNil(response.maxDate)) {
            vue.maxDate = response.maxDate;
        }
        var urlVue = new Url(document.location.href.split('#')[0]);
        urlVue.alterOrAddParam('context', vue.contextFile);
        vue.url = urlVue.getUrlString();

        $('#DescartesShareLinkInputUrl').val(vue.url);
    },
    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au bouton.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
    },
    CLASS_NAME: 'Descartes.Button.ShareLinkMap'
});

module.exports = Class;
