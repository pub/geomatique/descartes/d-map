/* global MODE */
var _ = require('lodash');

var Button = require('./' + MODE + '/Button');
var CenterToCoordinates = require('./CenterToCoordinates');
var ContentTask = require('./ContentTask');
var ContentTaskNamespace = require('./ContentTask/ContentTaskNamespace');
var DirectionalPanPanel = require('./DirectionalPanPanel');
var DirectionalPan = require('./DirectionalPan');
var DirectionalPanConstants = require('./DirectionalPanConstants');
var ExportPDF = require('./ExportPDF');
var ExportPNG = require('./ExportPNG');
var ShareLinkMap = require('./' + MODE + '/ShareLinkMap');
var DisplayLayersTreeSimple = require('./DisplayLayersTreeSimple');
var ToolBarOpener = require('./ToolBarOpener');

_.extend(ContentTask, ContentTaskNamespace);

_.extend(DirectionalPan, DirectionalPanConstants);

var namespace = {
    CenterToCoordinates: CenterToCoordinates,
    ContentTask: ContentTask,
    DirectionalPanPanel: DirectionalPanPanel,
    DirectionalPan: DirectionalPan,
    ExportPDF: ExportPDF,
    ExportPNG: ExportPNG,
    ShareLinkMap: ShareLinkMap,
    DisplayLayersTreeSimple: DisplayLayersTreeSimple,
    ToolBarOpener: ToolBarOpener
};

_.extend(Button, namespace);

module.exports = Button;
