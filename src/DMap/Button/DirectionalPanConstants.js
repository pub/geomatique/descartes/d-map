
module.exports = {
    /**
     * Constante: Descartes.Button.DirectionalPan.NORTH
     * Déplacement dirigé vers le Nord
     */
    NORTH: 'North',

    /**
     * Constante: Descartes.Button.DirectionalPan.SOUTH
     * Déplacement dirigé vers le Sud
     */
    SOUTH: 'South',

    /**
     * Constante: Descartes.Button.DirectionalPan.EAST
     * Déplacement dirigé vers l'Est
     */
    EAST: 'East',

    /**
     * Constante: Descartes.Button.DirectionalPan.WEST
     * Déplacement dirigé vers l'ouest
     */
    WEST: 'West',

    /**
     * Constante: Descartes.Button.DirectionalPan.NORTHWEST
     * Déplacement dirigé vers le Nord-Ouest
     */
    NORTHWEST: 'NorthWest',

    /**
     * Constante: Descartes.Button.DirectionalPan.SOUTHWEST
     * Déplacement dirigé vers le Sud-Ouest
     */
    SOUTHWEST: 'SouthWest',

    /**
     * Constante: Descartes.Button.DirectionalPan.NORTHEAST
     * Déplacement dirigé vers le Nord-Est
     */
    NORTHEAST: 'NorthEast',

    /**
     * Constante: Descartes.Button.DirectionalPan.SOUTHEAST
     * Déplacement dirigé vers le Sud-Est
     */
    SOUTHEAST: 'SouthEast'
};

