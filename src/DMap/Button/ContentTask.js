/* global MODE */

var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var Button = require('../Button/' + MODE + '/Button');
var EventManager = require('../Utils/Events/EventManager');

/**
 * Class: Descartes.Button.ContentTask
 * Classe "abstraite" définissant un bouton permettant la modification du contenu d'une carte.
 *
 * :
 * Les saisies nécessaires aux traitements associés au bouton sont assurées par une instance de <Descartes.ModalDialog>.
 *
 * :
 * Le bouton doit être instancié par un gestionnaire de contenu (voir <Descartes.action.MapContentManager>) à qui il transmet les valeurs saisies pour modification du contenu de la carte.
 *
 * Hérite de:
 *  - <Descartes.Button>
 *
 * Classes dérivées:
 *  - <Descartes.Button.ContentTask.AbstractEditGroup>
 *  - <Descartes.Button.ContentTask.AbstractEditLayer>
 *  - <Descartes.Button.ContentTask.RemoveGroup>
 *  - <Descartes.Button.ContentTask.RemoveLayer>
 *  - <Descartes.Button.ContentTask.ChooseWmsLayers>
 *  - <Descartes.Button.ContentTask.ExportVectorLayer>
 *
 * Evénements déclenchés:
 * done - Fermeture de la boite de dialogue avec validation des saisies.
 *
 * Ecouteurs mis en place:
 *  - l'événement 'done' de la classe <Descartes.ModalDialog> déclenche la méthode <sendDatas>.
 *  - l'événement 'cancelled' de la classe <Descartes.ModalDialog> déclenche la méthode <deleteDialog>.
 */
var Class = Utils.Class(Button, {
    /**
     * Propriété: datas
     * {Object} Objet JSON modélisant l'élément à traiter.
     */
    datas: null,

    /**
     * Propriete: modalDialog
     * {<Descartes.ModalDialog>} Boite de dialogue modale pour la saisie des propriétés de l'élément du contenu de la carte à traiter.
     */
    modalDialog: null,

    /**
     * Propriete: elements
     * {Array(DOMElement)} Eléments DOM des zones de saisie de la boite de dialogue.
     */
    elements: [],

    displayClasses: {
        globalClassName: 'DescartesModalDialogLabelAndValue',
        labelClassName: 'DescartesModalDialogLabel',
        inputClassName: 'DescartesModalDialogInput',
        textClassName: 'DescartesModalDialogText',
        selectClassName: 'DescartesModalDialogSelect',
        fieldSetClassName: 'DescartesModalDialogFieldSet',
        legendClassName: 'DescartesModalDialogLegend'
    },

    EVENT_TYPES: ['done'],
    /**
     * Constructeur: Descartes.Button.ContentTask
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     */
    initialize: function (options) {
        this.enabled = false;
        this.datas = {};
        this.events = new EventManager();
        Button.prototype.initialize.apply(this, [options]);
    },
    /**
     * Methode: setDatas
     * Associe au bouton les propriétés de l'élément du contenu de la carte à traiter.
     *
     * Paramêtres:
     * datas - {Object} Objet JSON modélisant l'élément à traiter.
     *
     * :
     * L'objet est vide dans le cas d'une création. Dans le cas d'une modification, il contient les propriétés initiales.
     */
    setDatas: function (datas) {
        _.extend(this.datas, datas);
    },

    /**
     * Methode: execute
     * Ouvre la boite de dialogue modale pour la saisie des propriétés de l'élément du contenu de la carte à traiter.
     */
    execute: function () {

    },
    CLASS_NAME: 'Descartes.Button.ContentTask'
});

module.exports = Class;
