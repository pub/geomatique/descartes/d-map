/* global MODE */

var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var Button = require('../Button/' + MODE + '/Button');
var MessagesConstants = require('../Messages');
var PrinterParamsManager = require('../Action/PrinterParamsManager');
var ExternalCallsUtils = require('../Core/ExternalCallsUtils');

require('./css/ExportPDF.css');

/**
 * Class: Descartes.Button.ExportPDF
 * Classe définissant un bouton permettant d'exporter la carte courante sous forme de fichier PDF.
 *
 * Hérite de:
 *  - <Descartes.Button>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'paramsChanged' de la classe <Descartes.Action.PrinterParamsManager> déclenche la méthode <actualizeParams>.
 */
var Class = Utils.Class(Button, {
    /**
     * Propriete: defaultInfos
     * {Object} Objet JSON stockant les informations complémentaires par défaut à insérer dans le fichier PDF.
     *
     * title - {String} Titre de la carte (utilisé pour le nom du fichier généré).
     * auteur - {String} Organisme auteur de la carte.
     * copyright - Texte du copyright à insérer en sur-impression de l'image.
     * description - {String} Description de la carte.
     * production - {String} Mentions "légales" de l'application productrice.
     * createur - {String} Nom de l'application productrice.
     * logoFiles - {Array(String)} Ressources fichiers des logos à insérer.
     * logoURLs - {Array(String)} Ressources HTTP des logos à insérer.
     */
    defaultInfos: {
        title: 'descartes',
        auteur: '',
        copyright: '',
        description: '',
        production: '',
        createur: '',
        logoFiles: [],
        logoURLs: []
    },
    /**
     * Propriete: infos
     * {Object} Objet JSON stockant les informations complémentaires à insérer dans le fichier PDF.
     *
     * :
     * Construit à partir de defaultInfos, puis surchargé en présence d'options complémentaires.
     */
    infos: null,

    /**
     * Propriete: action
     * {<Descartes.Action.PrinterParamsManager>} Action permettant de saisir les paramètres de mise en page de l'exportation PDF.
     */
    action: null,

    /**
     * Propriete: currentParams
     * {Object} Objet JSON stockant les derniers paramètres de mise en page choisis.
     */
    currentParams: null,

    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,
    /**
     * Constructeur: Descartes.Button.ExportPDF
     * Constructeur d'instances
     *
     * Paramètres:
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * infos - Objet JSON stockant les informations complémentaires à insérer dans le fichier PDF.
     */
    initialize: function (options) {
        this.enabled = true;
        this.infos = _.extend({}, this.defaultInfos);
        if (!_.isNil(options) && !_.isNil(options.infos)) {
            _.extend(this.infos, options.infos);
            delete options.infos;
        }
        Button.prototype.initialize.apply(this, arguments);
    },
    /**
     * Methode: execute
     * Lance une action <Descartes.Action.PrinterParamsManager> avec comme interface une <Descartes.UI.PrinterSetupDialog>.
     */
    execute: function () {
        if (!_.isNil(this.mapContent)) {
            var layers = this.mapContent.getVisibleLayers().sort(function (a, b) {
                return b.displayOrder - a.displayOrder;
            });
            var exportParams = ExternalCallsUtils.writeExportPostBody(layers, this.olMap, this.infos);

            if (!_.isNil(exportParams)) {
                this.infos.dialog = true;

                var options = {
                    params: this.currentParams,
                    infos: this.infos,
                    dialog: true
                };

                if (!_.isNil(this.size)) {
                    _.extend(options, {size: this.size});
                }

                this.action = new PrinterParamsManager(null, this.olMap, options);
                this.action.setMapContent(this.mapContent);
                this.action.events.register('paramsChanged', this, this.actualizeParams);
            } else {
                alert(MessagesConstants.Descartes_EXPORT_ERROR);
            }
        }
    },

    /**
     * Methode: actualizeParams
     * Met à jour les paramètres de mise en page pour une prochaine exportation.
     */
    actualizeParams: function () {
        this.currentParams = this.action.model;
    },

    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au bouton.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
    },
    CLASS_NAME: 'Descartes.Button.ExportPDF'
});

module.exports = Class;
