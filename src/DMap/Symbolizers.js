/* eslint-disable camelcase, max-len */

var ol = require('openlayers');
var _ = require('lodash');

require('ol-ext');
// require('ol-ext-css');

function colorOpacity(hex, opacity) {
    var color = ol.color.asArray(hex);
    color = _.clone(color);
    color[3] = opacity;
    return color;
}

function getStroke(json) {
    var lineDash = null;
    var lineCap = "round";
    var lineJoin = "round";
    if (_.isArray(json.strokeDashstyle)) {
        lineDash = json.strokeDashstyle;
    }
    if (!_.isNil(json.strokeCapstyle)) {
        lineCap = json.strokeCapstyle;
    }
    if (!_.isNil(json.strokeJoinstyle)) {
        lineJoin = json.strokeJoinstyle;
    }
    return new ol.style.Stroke({
        color: colorOpacity(json.strokeColor, json.strokeOpacity),
        width: json.strokeWidth,
        lineDash: lineDash,
        lineCap: lineCap,
        lineJoin: lineJoin
    });
}

function getFill(json) {
     var style = null;
     if (!_.isNil(json.externalGraphic)) {
        style = new ol.style.FillPattern({
                image: new ol.style.Icon({
                    src: json.externalGraphic
                }),
                scale: json.scaleGraphic,
                color: json.fillColor
        });
     } else if (!_.isNil(json.fillPatternName)) {
        var fillColor = "white";
        var fillOpacity = 0;
        if (!_.isNil(json.fillColor)) {
            fillColor = json.fillColor;
        }
        if (!_.isNil(json.fillOpacity)) {
           fillOpacity = json.fillOpacity;
        }
        style = new ol.style.FillPattern({
             pattern: json.fillPatternName,
             size: json.fillPatternSize,
             spacing: json.fillPatternSpacing,
             angle: json.fillPatternAngle,
             scale: json.fillPatternScale,
             color: json.fillPatternColor,
             fill: new ol.style.Fill({color: colorOpacity(fillColor, fillOpacity)})
        });
     } else {
        style = new ol.style.Fill({
            color: colorOpacity(json.fillColor, json.fillOpacity)
        });
     }

    /*return new ol.style.Fill({
        color: colorOpacity(json.fillColor, json.fillOpacity)
    });*/
    return style;
}

function getImageStyle(json) {
    var image = 'null';
    var radius = json.radius;

    //Compatibilité OL2
    if (_.isNil(radius)) {
        radius = json.pointRadius;
    }
    if (json.graphicName === 'circle') {
        image = new ol.style.Circle({
            stroke: getStroke(json),
            fill: getFill(json),
            radius: radius
        });
    } else if (!_.isNil(json.externalGraphic)) {
        image = new ol.style.Icon({
            src: json.externalGraphic,
            scale: json.scaleGraphic,
            color: colorOpacity(json.fillColor, json.fillOpacity)
        });
    } else {
        image = new ol.style.RegularShape({
            stroke: getStroke(json),
            fill: getFill(json),
            radius: radius,
            radius1: json.radius1,
            radius2: json.radius2,
            points: json.points,
            angle: json.angle,
            rotation: json.rotation
        });
    }
    return image;
}

var symbolizers = {
	'colorOpacity': colorOpacity,
    'pointToOlStyle': function (json) {
        var style = new ol.style.Style({
            image: getImageStyle(json)
        });
        return style;
    },
    'lineToOlStyle': function (json) {
        return new ol.style.Style({
            image: getImageStyle(json),
            stroke: getStroke(json)
        });
    },
    'polygonToOlStyle': function (json) {
        return new ol.style.Style({
            fill: getFill(json),
            stroke: getStroke(json),
            image: getImageStyle(json)
        });
    },
    'getOlStyle': function (json) {
        var result = {};

        _.each(json, function (jsonStyle, geom) {
            var olStyle = null;
            switch (geom) {
                case 'Point':
                case 'MultiPoint':
                    olStyle = symbolizers.pointToOlStyle(jsonStyle);
                    break;
                case 'Line':
                case 'MultiLine':
                    olStyle = symbolizers.lineToOlStyle(jsonStyle);
                    break;
                case 'Polygon':
                case 'Circle':
                case 'MultiPolygon':
                    olStyle = symbolizers.polygonToOlStyle(jsonStyle);
                    break;
            }

            if (geom === 'Line') {
                geom = 'LineString';
            } else if (geom === 'MultiLine') {
                geom = 'MultiLineString';
            }

            result[geom] = olStyle;
        });

        return result;
    },
    'getGeostylerStyle': function (jsonStyle, geom) {
        var result = {};

        switch (geom) {
            case 'Point':
            case 'MultiPoint':
                result = [{
                    kind: "Mark",
                    wellKnownName: jsonStyle.graphicName.charAt(0).toUpperCase() + jsonStyle.graphicName.slice(1),
                    radius: jsonStyle.pointRadius,
                    color: jsonStyle.fillColor,
                    opacity: jsonStyle.fillOpacity,
                    strokeColor: jsonStyle.strokeColor,
                    strokeWidth: jsonStyle.strokeWidth,
                    strokeOpacity: jsonStyle.strokeOpacity
                }];
                break;
            case 'Line':
            case 'MultiLine':
                result = [{
                    kind: "Line",
                    width: jsonStyle.strokeWidth,
                    color: jsonStyle.strokeColor,
                    opacity: jsonStyle.strokeOpacity,
                    dasharray: Array.isArray(jsonStyle.strokeDashstyle) ? jsonStyle.strokeDashstyle : [],
                    cap: jsonStyle.strokeLinecap ? jsonStyle.strokeLinecap : 'stroke-linecap'
                }];
                break;
            case 'Polygon':
            case 'Circle':
            case 'MultiPolygon':
                result = [{
                    kind: "Fill",
                    width: jsonStyle.strokeWidth,
                    color: jsonStyle.fillColor,
                    opacity: jsonStyle.fillOpacity,
                    outlineColor: jsonStyle.strokeColor,
                    outlineWidth: jsonStyle.strokeWidth,
                    outlineOpacity: jsonStyle.strokeOpacity
                }];
                break;
        }

        return result;
    },
    'getRandomColorStyleGeostylerStyles': function () {
        var results = {};
        var randomColor = '#' + Math.floor(Math.random() * 16777215).toString(16);
        var fillColor = randomColor;
        var strokeColor = randomColor;
        results["Point"] = [{
                    kind: "Mark",
                    wellKnownName: "circle",
                    radius: 8,
                    color: fillColor
                }];
        results["MultiPoint"] = results["Point"];
        results["Line"] = [{
                    kind: "Line",
                    width: 8,
                    color: strokeColor,
                    opacity: 1
                }];
        results["MultiLine"] = results["Line"];
        results["Polygon"] = [{
                kind: "Fill",
                width: 1,
                color: fillColor,
                fillOpacity: 1,
                outlineColor: strokeColor,
                outlineWidth: 1,
                outlineOpacity: 1
            }];
        results["Circle"] = results["Polygon"];
        results["MultiPolygon"] = results["Polygon"];

        return results;
    }
};

symbolizers.Descartes_Symbolizers_Measure = {
    'Point': {
        pointRadius: 4,
        graphicName: 'circle',
        fillColor: '#ee9900',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#ee9900'
    },
    'Line': {
        fillColor: '#ee9900',
        fillOpacity: 1,
        strokeWidth: 3,
        strokeOpacity: 1,
        strokeColor: '#ee9900',
        strokeDashstyle: [10, 10]
    },
    'Polygon': {
        strokeWidth: 2,
        strokeOpacity: 1,
        strokeColor: '#ee9900',
        fillColor: '#ee9900',
        fillOpacity: 0.3
    }
};

symbolizers.Descartes_Symbolizers_Selection = {
    'Point': {
        pointRadius: 1,
        graphicName: 'circle',
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#666666'
    },
    'Line': {
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 3,
        strokeOpacity: 1,
        strokeColor: '#666666',
        strokeDashstyle: 'solide'
    },
    'Polygon': {
        strokeWidth: 2,
        strokeOpacity: 1,
        strokeColor: '#666666',
        fillColor: '#666666',
        fillOpacity: 0.3
    },
    'Circle': {
        strokeWidth: 2,
        strokeOpacity: 1,
        strokeColor: '#666666',
        fillColor: '#666666',
        fillOpacity: 0.3
    }
};

symbolizers.Descartes_Symbolizers_RequestSLD = {
    'Point': {
        graphicName: 'circle',
        fillColor: '#A5F38D',
        pointRadius: 10.0
    },
    'Line': {
        fillColor: '#A5F38D',
        fillOpacity: 1,
        strokeColor: '#A5F38D',
        strokeWidth: 5.0,
        strokeOpacity: 1
    },
    'Polygon': {
        //graphicName: 'cross',
        graphicName: 'circle',
        points: 4,
        radius: 8,
        radius2: 0,
        angle: 0,
        fillColor: '#A5F38D',
        fillOpacity: 0.5,
        strokeOpacity: 1,
        strokeColor: '#A5F38D',
        strokeWidth: 2.0
    }
};

symbolizers.Descartes_Symbolizers_WFS = {
    'Point': {
        pointRadius: 4, //6
        graphicName: 'circle',
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#666666',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Line': {
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 3,
        strokeOpacity: 1,
        strokeColor: '#666666',
        strokeDashstyle: 'solide',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Polygon': {
        strokeWidth: 2, //1
        strokeOpacity: 1, //1
        strokeColor: '#666666',
        fillColor: '#666666',
        fillOpacity: 0.3,
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    }
};

symbolizers.Descartes_Symbolizers_WFS['MultiPoint'] = _.extend(symbolizers.Descartes_Symbolizers_WFS['Point'], {});
symbolizers.Descartes_Symbolizers_WFS['MultiLine'] = _.extend(symbolizers.Descartes_Symbolizers_WFS['Line'], {});
symbolizers.Descartes_Symbolizers_WFS['MultiPolygon'] = _.extend(symbolizers.Descartes_Symbolizers_WFS['Polygon'], {});

symbolizers.Descartes_Symbolizers_KML = {
    'Point': {
        pointRadius: 4, //6
        graphicName: 'circle',
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#666666',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Line': {
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 3,
        strokeOpacity: 1,
        strokeColor: '#666666',
        strokeDashstyle: 'solide',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Polygon': {
        strokeWidth: 2,
        strokeOpacity: 1,
        strokeColor: '#666666',
        fillColor: '#666666',
        fillOpacity: 0.3,
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    }
};
symbolizers.Descartes_Symbolizers_KML['MultiPoint'] = _.extend(symbolizers.Descartes_Symbolizers_KML['Point'], {});
symbolizers.Descartes_Symbolizers_KML['MultiLine'] = _.extend(symbolizers.Descartes_Symbolizers_KML['Line'], {});
symbolizers.Descartes_Symbolizers_KML['MultiPolygon'] = _.extend(symbolizers.Descartes_Symbolizers_KML['Polygon'], {});

symbolizers.Descartes_Symbolizers_GEOJSON = {
    'Point': {
        pointRadius: 4, //6
        graphicName: 'circle',
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#666666',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Line': {
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 3,
        strokeOpacity: 1,
        strokeColor: '#666666',
        strokeDashstyle: 'solide',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Polygon': {
        strokeWidth: 2,
        strokeOpacity: 1,
        strokeColor: '#666666',
        fillColor: '#666666',
        fillOpacity: 0.3,
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    }
};
symbolizers.Descartes_Symbolizers_GEOJSON['MultiPoint'] = _.extend(symbolizers.Descartes_Symbolizers_GEOJSON['Point'], {});
symbolizers.Descartes_Symbolizers_GEOJSON['MultiLine'] = _.extend(symbolizers.Descartes_Symbolizers_GEOJSON['Line'], {});
symbolizers.Descartes_Symbolizers_GEOJSON['MultiPolygon'] = _.extend(symbolizers.Descartes_Symbolizers_GEOJSON['Polygon'], {});

symbolizers.Descartes_Symbolizers_GenericVector = {
    'Point': {
        pointRadius: 4, //6
        graphicName: 'circle',
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#666666',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Line': {
        fillColor: '#666666',
        fillOpacity: 1,
        strokeWidth: 3,
        strokeOpacity: 1,
        strokeColor: '#666666',
        strokeDashstyle: 'solide',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Polygon': {
        strokeWidth: 2, //1
        strokeOpacity: 1, //1
        strokeColor: '#666666',
        fillColor: '#666666',
        fillOpacity: 0.3,
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    }
};

symbolizers.Descartes_Symbolizers_GenericVector['MultiPoint'] = _.extend(symbolizers.Descartes_Symbolizers_GenericVector['Point'], {});
symbolizers.Descartes_Symbolizers_GenericVector['MultiLine'] = _.extend(symbolizers.Descartes_Symbolizers_GenericVector['Line'], {});
symbolizers.Descartes_Symbolizers_GenericVector['MultiPolygon'] = _.extend(symbolizers.Descartes_Symbolizers_GenericVector['Polygon'], {});

symbolizers.Descartes_Symbolizers_DefaultLocalisationAdresse = {
    'Point': {
        pointRadius: 4,
        graphicName: 'circle',
        fillColor: '#000000',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#000000'
    }
};

symbolizers.Descartes_Symbolizers_SelectLocalisationAdresse = {
    'Point': {
        pointRadius: 4,
        graphicName: 'circle',
        fillColor: '#ee9900',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: '#ee9900'
    }
};

symbolizers.Descartes_Symbolizers_SelectToolTip = {
    'Point': {
        pointRadius: 4, //6
        graphicName: 'circle',
        fillColor: 'black',
        fillOpacity: 1,
        strokeWidth: 1,
        strokeOpacity: 1,
        strokeColor: 'black',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
            },
    'Line': {
        fillColor: 'black',
        fillOpacity: 1,
        strokeWidth: 3,
        strokeOpacity: 1,
        strokeColor: 'black',
        strokeDashstyle: 'solide',
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    },
    'Polygon': {
        strokeWidth: 2,
        strokeOpacity: 1,
        strokeColor: 'black',
        fillColor: 'black',
        fillOpacity: 0.3,
        pointerEvents: 'visiblePainted',
        cursor: 'pointer'
    }
};

module.exports = symbolizers;
