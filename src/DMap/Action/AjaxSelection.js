/* global MODE, Descartes */

var _ = require('lodash');
var log = require('loglevel');

var Action = require('../Core/Action');

var DMapConstants = require('../DMapConstants');
var UIConstants = require('../UI/UIConstants');
var Messages = require('../Messages');
var TabbedDataGrids = require('../UI/' + MODE + '/TabbedDataGrids');
var Utils = require('../Utils/DescartesUtils');
var WaitingDialog = require('../UI/' + MODE + '/WaitingDialog');
var MapConstants = require('../Map/MapConstants');
var ResultLayer = require('../Model/ResultLayer');
var ResultLayerConstants = require('../Model/ResultLayerConstants');

/**
 * Class: Descartes.Action.AjaxSelection
 * Classe technique assurant les communications entre les services d'interrogation côté serveur et les vues accueillant les résultats.
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'gotoFeatureBounds' de la classe définissant la vue MVC associée, quand elle est de type <Descartes.UI.TYPE_WIDGET>, déclenche la méthode <gotoFeatureBounds>.
 */
var Class = Utils.Class(Action, {
    /**
     * Propriete: type
     * <Descartes.UI.TYPE_WIDGET> | <Descartes.UI.TYPE_INPLACEDIV> | <Descartes.UI.TYPE_POPUP> : Type de vue pour l'affichage des résultats.
     */
    type: null,

	/**
	 * Propriete: div
	 * {DOMElement} Elément DOM de la page accueillant la vue.
     *
	 * :
	 * Cette propriété n'a pas de sens pour les vues de type <Descartes.UI.TYPE_POPUP>.
     */
    div: null,

	/**
	 * Propriete: format
	 * {"JSON" | "HTML" | "XML"} Format de réponse demandé au service d'interrogation.
	 *
	 * :
	 * Il vaut obligatoirement "HTML" pour les vues de type <Descartes.UI.TYPE_INPLACEDIV> ou <Descartes.UI.TYPE_POPUP>.
	 */
    format: null,

	/**
	 * Propriete: waitingMsg
	 * {Boolean} Indique si le message d'attente s'afficher.
	 */
    waitingMsg: true,

	/**
	 * Propriete: waitingMessageClassName
	 * {String} Classe CSS pour le message d'attente.
	 */
    waitingMessageClassName: 'DescartesQueryResultWaitingMessage',

	/**
	 * Propriete: waitingMaskClassName
	 * {String} Classe CSS pour le masque inhibant la page pendant l'attente.
	 */
    waitingMaskClassName: 'DescartesQueryResultWaitingMask',

	/**
	 * Propriete: attributesAliasSubstitution
	 * {Object} Libellés pour les attributs à afficher dans les résultats des interrogations,
	 */
    attributesAliasSubstitution: null,

	/**
	 * Constructeur: Descartes.Action.AjaxSelection
	 * Constructeur d'instances
	 *
	 * Paramètres:
	 * resultUiParams - {Object} Objet JSON stockant les propriétés nécessaires à la génération de la vue.
	 * requestParams - {Object} Objet JSON stockant les paramètres nécessaires pour la génération de la requête au service d'interrogation.
	 * olMap - {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
	 * options - {Object} Objet optionnel contenant les propriéts à renseigner dans l'instance.
	 *
	 * Propriétés nécessaires pour la génération de la vue:
     * type - <Descartes.UI.TYPE_WIDGET> | <Descartes.UI.TYPE_INPLACEDIV> | <Descartes.UI.TYPE_POPUP> : Type de vue pour l'affichage des r�sultats.
     * view - {<Descartes.UI>} : Classe de la vue si le type est <Descartes.UI.TYPE_WIDGET>
     * div - {DOMElement|String} : Elément DOM de la page accueillant le résultat de la sélection si le type est <Descartes.UI.TYPE_WIDGET> ou <Descartes.UI.TYPE_INPLACEDIV>.
     * format - "JSON"|"HTML"|"XML" : format de retour des informations souhaité.
     * withReturn - {Boolean} : Indique si le résultat doit proposer une localisation rapide sur les objets retournés.
     * withCsvExport - {Boolean} : Indique si le résultat doit proposer une exportation CSV (côté serveur) pour les objets retournés.
     * withResultLayerExport - {Boolean} : Indique si le résultat doit proposer une exportation des objets retournés avec les géométries.
     * withUIExports - {Boolean} : Indique si le résultat doit proposer les exports (côté client) pour les objets retournés.
     * withAvancedView - {Boolean} : Indique si le résultat doit être affiché dans une vue (UI) avancée.
     * withFilterColumns - {Boolean} : Indicateur pour la possibilité de filtrer les colonnes.
     * withListResultLayer - {Boolean} : Indicateur pour la possibilité d'afficher la liste de couche'.
     * withPagination - {Boolean} : Indicateur pour la possibilité d'afficher la pagination'.
     * withTabs - {Boolean} : Indicateur pour la possibilité d'afficher les onglets'.
	 *
	 * Paramètres nécessaires pour la génération de la requête au service d'interrogation:
	 * distantService - <Descartes.FEATUREINFO_SERVER> | <Descartes.FEATURE_SERVER> URL d'accés au service d'interrogation.
	 * infos - {String} Flux XML pour le paramètre contenant la liste des couches à interroger (voir <Descartes.ExternalCallsUtils.writeQueryPostBody>).
	 * gmlMask - {String} Flux GML représentant la géométrie d'un polygone d'intersection. Utilisé pour les interrogations déclenchées par <Descartes.Tool.Selection.FeatureSelection> et ses sous-classes.
	 * dataMask - {String} Flux FEI représentant un ensemble de critères de sélection. Utilisé pour les interrogations déclenchées par <Descartes.Action.RequestManager> et <Descartes.UI.LayersTree.showDatasLayer>.
	 * pixelMask - {String} Coordonnées en pixels sur la carte sous la forme "#X=<x>#Y=<y>. Utilisé pour les interrogations déclenchées par <Descartes.Tool.Selection.PointSelection> et <Descartes.Info.ToolTip>.
	 *
	 * Options de construction propres à la classe:
	 * waitingMsg - {Boolean} Indique si le message d'attente s'afficher.
	 * waitingMessageClassName - {String} Classe CSS pour le message d'attente.
	 * waitingMaskClassName - {String} Classe CSS pour le masque inhibant la page pendant l'attente.
     * attributesLabelsSubstitution - {Object} : Libellés pour les attributs contenus dans la réponse, au format : `{descartesLayerId: [{ nom_attribut: "Libellé attribut" }]}`
	 */
    initialize: function (resultUiParams, requestParams, olMap, options) {
        _.extend(this, resultUiParams);
        if (_.isNil(options)) {
            options = {};
        }

        if (!_.isNil(options.waitingMsg)) {
            this.waitingMsg = options.waitingMsg;
            delete options.waitingMsg;
        }

        if (!_.isNil(options.waitingMessageClassName)) {
            this.waitingMessageClassName = options.waitingMessageClassName;
            delete options.waitingMessageClassName;
        }
        if (!_.isNil(options.waitingMaskClassName)) {
            this.waitingMaskClassName = options.waitingMaskClassName;
            delete options.waitingMaskClassName;
        }
        if (!_.isNil(options.attributesAliasSubstitution)) {
            this.attributesAliasSubstitution = options.attributesAliasSubstitution;
            delete options.attributesAliasSubstitution;
        }

        this.div = Utils.getDiv(this.div);
        if (_.isNil(this.div) && this.type === UIConstants.TYPE_INPLACE) {
            log.error('Pas de div id="%s" pour afficher les résultats.', resultUiParams.div);
            return;
        }

        switch (this.type) {
            case UIConstants.TYPE_WIDGET :
                if (_.isNil(this.format)) {
                    this.format = 'JSON';
                }
                break;
            case UIConstants.TYPE_INPLACEDIV :
                this.format = 'HTML';
                break;
            case UIConstants.TYPE_POPUP :
                this.format = 'HTML';
                break;
        }
        if (this.view) {
            options.view = this.view;
        }

        var postBody = 'infos=' + encodeURIComponent(requestParams.infos);
        if (!_.isNil(requestParams.gmlMask)) {
            postBody += '&gmlMask=' + requestParams.gmlMask;
        }
        if (!_.isNil(requestParams.dataMask)) {
            postBody += '&dataMask=' + requestParams.dataMask;
        }
        if (!_.isNil(requestParams.pixelMask)) {
            postBody += '&pixelMask=' + requestParams.pixelMask;
        }
        if (!_.isNil(requestParams.timeout)) {
            postBody += '&timeout=' + requestParams.timeout;
        }
        if (this.withReturn) {
            postBody += '&withReturn=true';
        }

        if (this.withCsvExport) {
            options.withCsvExport = true;
            postBody += '&withCsvExport=true';
        }
        postBody += '&format=' + this.format;

        if (this.withUIExports) {
            options.withUIExports = true;
        }

        if (this.withAvancedView) {
            options.withAvancedView = true;
        }

        if (this.withFilterColumns) {
            options.withFilterColumns = true;
        }

        if (this.withListResultLayer) {
            options.withListResultLayer = true;
        }

        if (this.withPagination) {
            options.withPagination = true;
        }

        if (this.withTabs) {
            options.withTabs = true;
        }

        if (this.withResultLayerExport) {
            options.withResultLayerExport = true;
        }

        Action.prototype.initialize.apply(this, [this.div, olMap, options]);
        if (this.type === UIConstants.TYPE_WIDGET) {
            if (_.isNil(this.renderer)) {
                this.renderer = new TabbedDataGrids(this.div, this.model, options);
            }
            this.renderer.events.register('gotoFeatureBounds', this, this.gotoFeatureBounds);
            this.renderer.events.register('close', this, this.close);
            this.renderer.events.register('undisplaySelectionLayer', this, this.undisplaySelectionLayer);
            this.renderer.events.register('unselectSelectionLayer', this, this.unselect);
            this.renderer.events.register('fixSelectionLayer', this, this.fixSelectionLayer);
            this.renderer.events.register('flashSelectionLayer', this, this.flashSelectionLayer);
            this.renderer.events.register('addSelectionLayer', this, this.addSelectionLayer);
        } else {
            this.olMap = olMap;
        }

        if (this.waitingMsg) {
            this.showWaitingMessage();
        }

        var that = this;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', requestParams.distantService);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        xhr.onload = function () {
            that.closeWaitingMessage();
            setTimeout(function () {
                if (xhr.status === 200) {
                    that.showResults(xhr.responseText);
                } else {
                    that.showError();
                }
            }, 1000);

        };
        xhr.send(postBody);
    },

    /**
     * Methode: showWaitingMessage
     * Affiche le message d'attente.
     */
    showWaitingMessage: function () {
        this.closeWaitingMessage();
        this.waitingDialog = new WaitingDialog({
            message: this.getMessage('WAITING_MESSAGE')
        });
        this.waitingDialog.open();
    },

    /**
     * Methode: closeWaitingMessage
     * Ferme le message d'attente.
     */
    closeWaitingMessage: function () {
        if (this.waitingDialog) {
            this.waitingDialog.close();
        }
    },
    /**
     * Methode: showResults
     * Actualise le contenu de la vue avec les données transmises par le service d'interrogation.
     *
     * Paramètres:
     * ajaxResponseText - {String} Réponse AJAX contenant les objets de référence.
     */
    showResults: function (ajaxResponseText) {
        this.closeWaitingMessage();
        ResultLayer.searchDataOnError = false;
        var imgs, submits;
        switch (this.type) {
            case UIConstants.TYPE_WIDGET :
                if (this.format === 'JSON') {
                    ajaxResponseText = ajaxResponseText.replace(/\r\n|\n/gi, '<br/>').replace(/\\\\\\/gi, '&#92;').replace(/\\"/gi, '&quot;').replace(/\t/gi, ' ');
                    try {
                    ajaxResponseText = JSON.parse(ajaxResponseText);
                    } catch (e) {
                        alert(this.getMessage("ERROR_JSON_PARSER") + e);
                    }
                }
                this._changeAttributesLabels(ajaxResponseText);
                this.renderer.draw(ajaxResponseText);
                this.closeWaitingMessage();
                break;
            case UIConstants.TYPE_INPLACEDIV :
                var self = this;
                this.div.innerHTML = ajaxResponseText;
                imgs = this.div.getElementsByTagName('img');
                for (var iImg = 0, lenImgs = imgs.length; iImg < lenImgs; iImg++) {
                    if (imgs[iImg].getAttribute('src').includes("icon_preview")) {
                        imgs[iImg].setAttribute('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAMAAABhEH5lAAAAB3RJTUUH0wEIERgVUohwiwAAAAlwSFlzAAALEgAACxIB0t1+/AAAAARnQU1BAACxjwv8YQUAAAA/UExURQAAAAAAAIrf37jr68fw8JmZmTMzM/r+/mZmZqXL95nMzNLl+8zm5mOaY5jNmMzmzDGaMbHNsbPMzGaZmTFlY9beknwAAAABdFJOUwBA5thmAAAAiUlEQVR42o2PWw7DIAwEWQgkvBJIff+z1ja0qMpPjZTdDCMQxjzHHz/jGR0ptpqirFZbDYKqtA9pitJkSobFnUgtokrTIv4vXcz7HBbvxwL09uLv98YTyOg3mA1LSbmuXLIytlIGTxETioRsu7MoM40PwWN3XP1MfSe4OawUZK3bsFKZHrvyj3kDM48HPEAjJ+gAAAAASUVORK5CYIJjmM2YzObMMZoxsc2xs8zMZpmZMWVj1t6SfAAAAAF0Uk5TAEDm2GYAAACJSURBVHjajY9bDsMgDARZCCS8Eg==');
                    } else if (imgs[iImg].getAttribute('src').includes("icon_locate")) {
                        imgs[iImg].setAttribute('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUBAMAAAB/pwA+AAAAB3RJTUUH0wkLDiwV3B2DHgAAAAlwSFlzAAALEgAACxIB0t1+/AAAAARnQU1BAACxjwv8YQUAAAAYUExURQAAACYmJgAJUNTf8GGBr0hlmC5EbBUlR56UY2AAAAABdFJOUwBA5thmAAAAUElEQVR42mNgIAwYhUAIwjQCIQjTCYTAQFBQAYjATGFXBSACywuCgCJEMMjM0RksLOyqEl4sDGOaOcOYQQpKEAVCSgpQggEsB1MAZyIpIAAA9ZkMtkszrAAAAAAASUVORK5CYILTCQsOLBXcHYMeAAAACXBIWXMAAAsSAAALEgHS3X78AAAABGdBTUEAALGPC/xhBQAAABhQTFRFAAAAJiYmAAlQ1N/wYYGvSGWYLkRsFSVHnpRjYAAAAAF0Uk5TAEDm2GYAAABQSURBVHjaY2AgDBiFQAjCNAIhCNMJhMBAUFABiMBMYVcFIALLC4KAIkQwyMzRGSws7KoSXiwMY5o5ww==');
                    } else if (imgs[iImg].getAttribute('src') !== null) {
                        imgs[iImg].setAttribute('src', Descartes.getWebServiceRoot() + imgs[iImg].getAttribute('src'));
                    }
                    if (imgs[iImg].getAttribute('boundsAndScales') !== null) {
                        imgs[iImg].onclick = function () {
                            self.gotoFeatureBounds(this.getAttribute('boundsAndScales'));
                        };
                    }
                }
                submits = this.div.getElementsByTagName('input');
                for (var iSubmit = 0, lenSubmits = submits.length; iSubmit < lenSubmits; iSubmit++) {
                    if (submits[iSubmit].getAttribute('csvExport') !== null) {
                        submits[iSubmit].onclick = function () {
                            self.getCsvExport(this.parentNode.previousSibling.previousSibling);
                        };
                    }
                }
                break;
            case UIConstants.TYPE_POPUP :
                if (DMapConstants.INFO_WINDOW && !DMapConstants.INFO_WINDOW.closed) {
                    DMapConstants.INFO_WINDOW.close();
                }
                DMapConstants.INFO_WINDOW = window.open('', 'infoWindow', 'toolbar=no, menubar=no,scrollbars=yes,width=500,height=400,resizable=yes');


                var ajaxResponseDom = document.createElement('html');
                ajaxResponseDom.innerHTML = ajaxResponseText;
                //var ajaxResponseDom = xmlFormat.read(ajaxResponseText);
                imgs = ajaxResponseDom.getElementsByTagName('img');
                for (var jImg = 0, lengthImgs = imgs.length; jImg < lengthImgs; jImg++) {
                    var img = imgs[jImg];
                    if (img.getAttribute('src').includes("icon_preview")) {
                        img.setAttribute('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAMAAABhEH5lAAAAB3RJTUUH0wEIERgVUohwiwAAAAlwSFlzAAALEgAACxIB0t1+/AAAAARnQU1BAACxjwv8YQUAAAA/UExURQAAAAAAAIrf37jr68fw8JmZmTMzM/r+/mZmZqXL95nMzNLl+8zm5mOaY5jNmMzmzDGaMbHNsbPMzGaZmTFlY9beknwAAAABdFJOUwBA5thmAAAAiUlEQVR42o2PWw7DIAwEWQgkvBJIff+z1ja0qMpPjZTdDCMQxjzHHz/jGR0ptpqirFZbDYKqtA9pitJkSobFnUgtokrTIv4vXcz7HBbvxwL09uLv98YTyOg3mA1LSbmuXLIytlIGTxETioRsu7MoM40PwWN3XP1MfSe4OawUZK3bsFKZHrvyj3kDM48HPEAjJ+gAAAAASUVORK5CYIJjmM2YzObMMZoxsc2xs8zMZpmZMWVj1t6SfAAAAAF0Uk5TAEDm2GYAAACJSURBVHjajY9bDsMgDARZCCS8Eg==');
                    } else if (img.getAttribute('src').includes("icon_locate")) {
                        img.setAttribute('src', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUBAMAAAB/pwA+AAAAB3RJTUUH0wkLDiwV3B2DHgAAAAlwSFlzAAALEgAACxIB0t1+/AAAAARnQU1BAACxjwv8YQUAAAAYUExURQAAACYmJgAJUNTf8GGBr0hlmC5EbBUlR56UY2AAAAABdFJOUwBA5thmAAAAUElEQVR42mNgIAwYhUAIwjQCIQjTCYTAQFBQAYjATGFXBSACywuCgCJEMMjM0RksLOyqEl4sDGOaOcOYQQpKEAVCSgpQggEsB1MAZyIpIAAA9ZkMtkszrAAAAAAASUVORK5CYILTCQsOLBXcHYMeAAAACXBIWXMAAAsSAAALEgHS3X78AAAABGdBTUEAALGPC/xhBQAAABhQTFRFAAAAJiYmAAlQ1N/wYYGvSGWYLkRsFSVHnpRjYAAAAAF0Uk5TAEDm2GYAAABQSURBVHjaY2AgDBiFQAjCNAIhCNMJhMBAUFABiMBMYVcFIALLC4KAIkQwyMzRGSws7KoSXiwMY5o5ww==');
                    } else if (img.getAttribute('src') !== null) {
                        img.setAttribute('src', Descartes.getWebServiceRoot() + img.getAttribute('src'));
                    }
                    if (img.getAttribute('boundsAndScales') !== null) {
                        img.setAttribute('onclick', "window.opener.Descartes.AjaxSelectionListener.gotoFeatureBounds(this.getAttribute('boundsAndScales'));");
                    }
                }
                submits = ajaxResponseDom.getElementsByTagName('input');
                for (var jSubmit = 0, lengthSubmits = submits.length; jSubmit < lengthSubmits; jSubmit++) {
                    var submit = submits[jSubmit];
                    if (submit.getAttribute('csvExport') !== null) {
                        submit.setAttribute('onclick', 'window.opener.Descartes.AjaxSelectionListener.getCsvExport(this.parentNode.previousSibling.previousSibling);');
                    }
                }
                Descartes.AjaxSelectionListener = this;
                DMapConstants.INFO_WINDOW.document.write(ajaxResponseDom.innerHTML);
                DMapConstants.INFO_WINDOW.document.close();
                break;
        }
    },

    /**
     * Methode: showError
     * Affiche un message d'erreur en cas d'échec de communication avec le service d'interrogation.
     */
    showError: function () {
        if (this.type === UIConstants.TYPE_WIDGET || this.type === UIConstants.TYPE_INPLACEDIV) {
            //this.div.removeChild(this.waitingMessage);
            this.closeWaitingMessage();
        } else {
            document.body.removeChild(this.waitingMask);
        }
        ResultLayer.refreshSelections(null, this.olMap);
        ResultLayer.removeMarker(this.olMap);
        ResultLayer.searchDataOnError = true;
        this.olMap.getViewport().style.cursor = '';
        alert(Messages.Descartes_SEARCH_FAILURE);
    },

    /**
     * Methode: gotoFeatureBounds
     * Recadre la carte sur l'emprise d'un des objets sélectionné dans la vue affichant les données.
     *
     * Paramètres:
     * boundsAndScales - {Array} Coordonnées de l'emprise et plage d'échelle de visualisation de l'objet sous la forme
     * [[xMin, yMin, xMax, yMax], layerMinScale, layerMaxScale]].
     */
    gotoFeatureBounds: function (boundsAndScales) {
        var extent = Utils.gotoFeatureBounds(boundsAndScales, this.olMap);
        ResultLayer.displayMarker(this.olMap, extent);
    },
    unselect: function () {
        if (ResultLayerConstants.blinkLayerTimerId) {
            clearInterval(ResultLayerConstants.blinkLayerTimerId);
            ResultLayerConstants.blinkLayerTimerId = null;
        }
        if (ResultLayerConstants.blinkLayerDelayId) {
            clearTimeout(ResultLayerConstants.blinkLayerDelayId);
            ResultLayerConstants.blinkLayerDelayId = null;
        }
        ResultLayer.refresh(null, this.olMap);
        ResultLayer.removeMarker(this.olMap);
    },
    fixSelectionLayer: function () {
        if (ResultLayer.olLayersResult) {
            _.each(ResultLayer.olLayersResult, function (olLayerResult) {
                 olLayerResult.setVisible(true);
            });
            if (ResultLayerConstants.blinkLayerTimerId) {
                clearInterval(ResultLayerConstants.blinkLayerTimerId);
                ResultLayerConstants.blinkLayerTimerId = null;
            }
            if (ResultLayerConstants.blinkLayerDelayId) {
                clearTimeout(ResultLayerConstants.blinkLayerDelayId);
                ResultLayerConstants.blinkLayerDelayId = null;
            }
        }
    },
    addSelectionLayer: function () {
        var datas = this.renderer.model;
        var requestParams = this.renderer.requestParams;
        ResultLayer.addInLayersTree(datas, requestParams);
        this.unselect();
    },
    flashSelectionLayer: function () {
        if (ResultLayer.olLayersResult) {
            _.each(ResultLayer.olLayersResult, function (olLayerResult) {
                 olLayerResult.setVisible(false);
             });
            if (!ResultLayerConstants.blinkLayerTimerId) {
                ResultLayerConstants.blinkLayerTimerId = setInterval(ResultLayerConstants.blinkLayer, 1000);
            }
            if (!ResultLayerConstants.blinkLayerDelayId) {
                ResultLayerConstants.blinkLayerDelayId = setTimeout(ResultLayerConstants.stopBlinkLayer, 10000);
            }
        }
    },
    undisplaySelectionLayer: function () {
        if (ResultLayer.olLayersResult) {
            _.each(ResultLayer.olLayersResult, function (olLayerResult) {
                 olLayerResult.setVisible(false);
             });
            if (ResultLayerConstants.blinkLayerTimerId) {
                clearInterval(ResultLayerConstants.blinkLayerTimerId);
                ResultLayerConstants.blinkLayerTimerId = null;
            }
            if (ResultLayerConstants.blinkLayerDelayId) {
                clearTimeout(ResultLayerConstants.blinkLayerDelayId);
                ResultLayerConstants.blinkLayerDelayId = null;
            }
        }
    },

    close: function () {
        ResultLayer.removeMarker(this.olMap);
    },

    /**
     * Methode: getCsvExport
     * Exporte au format CSV les données d'un tableau.
     *
     * :
     * Cette méthode n'est utilisée que par les vues de type <Descartes.UI.TYPE_INPLACEDIV> ou <Descartes.UI.TYPE_POPUP>.
     *
     * :
     * Les vues de type <Descartes.UI.TYPE_WIDGET> doivent implémenter elles-même les appels au service d'exportation CSV.
     *
     * Paramètres:
     * table - {DOMElement} Elément DOM correspondant au tableau HTML contenant les données à exporter.
     */
    getCsvExport: function (table) {
        var csvStream = '';

        var rows = table.getElementsByTagName('tr');
        var cells = rows[0].getElementsByTagName('th');
        var cell;
        var headerRow = '';
        var withGoto = false;
        for (var iHeader = 0, lenHeaderCols = cells.length; iHeader < lenHeaderCols; iHeader++) {
            if (cells[iHeader].getElementsByTagName('img').length === 0) {
                cell = cells[iHeader].innerHTML;
                headerRow += '"' + cell + '";';
            } else {
                withGoto = true;
            }
        }
        csvStream += headerRow + '\r\n';

        for (var i = 1, lenRows = rows.length; i < lenRows; i++) {
            cells = rows[i].getElementsByTagName('td');
            var row = '';
            var firstCol = (withGoto ? 1 : 0);
            for (var j = firstCol, lenCols = cells.length; j < lenCols; j++) {
                cell = '';
                if (cells[j].getElementsByTagName('a').length === 0) {
                    cell = cells[j].innerHTML.replace(/"/g, '""');
                } else {
                    cell = cells[j].getElementsByTagName('a')[0].href;
                }
                row += '\"' + cell + '\";';
            }
            csvStream += row + '\r\n';
        }

        var windowDocument;
        if (this.type === UIConstants.TYPE_INPLACEDIV) {
            windowDocument = this.div;
        } else {
            windowDocument = DMapConstants.INFO_WINDOW.document;
        }
        var forms = windowDocument.getElementsByTagName('form');
        forms[0].action = Descartes.EXPORT_CSV_SERVER;
        forms[0].CSVcontent.value = csvStream;
        forms[0].submit();
    },

    _changeAttributesLabels: function (resultatJson) {
        if (!_.isNil(this.attributesAliasSubstitution) && !_.isEmpty(this.attributesAliasSubstitution)) {
            var layerListSubstitution = Object.keys(this.attributesAliasSubstitution);
            for (var i = 0, len = resultatJson.length; i < len; i++) {
                 if (layerListSubstitution.indexOf(resultatJson[i].layerId) !== -1) {
                      var attributeListSubstitution = this.attributesAliasSubstitution[resultatJson[i].layerId];
                      var attributesSubstitution = Object.keys(attributeListSubstitution);
                      var datas = resultatJson[i].layerDatas;
                      var newDatas = [];
                      for (var j = 0, jlen = datas.length; j < jlen; j++) {
                           var fieldNames = Object.keys(datas[j]);
                           var data = {};
                           for (var k = 0, klen = fieldNames.length; k < klen; k++) {
                               if (attributesSubstitution.indexOf(fieldNames[k]) !== -1) {
                                     var alias = attributeListSubstitution[fieldNames[k]];
                                     data[alias] = datas[j][fieldNames[k]];
                               } else {
                                     data[fieldNames[k]] = datas[j][fieldNames[k]];
                               }
                           }
                           newDatas.push(data);
                      }
                      resultatJson[i].layerDatas = newDatas;
                 }
            }
         }
    },
    CLASS_NAME: 'Descartes.Action.AjaxSelection'
});
module.exports = Class;
