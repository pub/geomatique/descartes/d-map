
module.exports = {
    /**
     * Constante: BEHAVIOR_DEFAULT
     * Comportement par défaut du gestionnaire
     */
    BEHAVIOR_DEFAULT: 0,
    /**
     * Constante: BEHAVIOR_PERSONAL
     * Comportement du gestionnaire dans lequel les vues ne sont disponibles que pour l'utilisateur les ayant créées.
     */
    BEHAVIOR_PERSONAL: 1,
    /**
     * Constante: BEHAVIOR_MANAGED_BY_CREATOR
     * Comportement du gestionnaire dans lequel la disponibilité des vues est pilotée par l'utilisateur les ayant créées.
     */
    BEHAVIOR_MANAGED_BY_CREATOR: 2
};


