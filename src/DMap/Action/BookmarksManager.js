/* global MODE, Descartes */

var _ = require('lodash');
var log = require('loglevel');

var Utils = require('../Utils/DescartesUtils');
var BookmarksManagerConstants = require('./BookmarksManagerConstants');

var Action = require('../Core/Action');
var MapConstants = require('../Map/MapConstants');
var Url = require('../Model/Url');

var BookmarksInPlace = require('../UI/' + MODE + '/BookmarksInPlace');
var SaveBookmarkDialog = require('../UI/' + MODE + '/SaveBookmarkDialog');
var ConfirmDialog = require('../UI/' + MODE + '/ConfirmDialog');

/**
 * Class: Descartes.Action.BookmarksManager
 * Classe offrant un gestionnaire de vues personnalisées permettant la sauvegarde et la restauration de contextes de consultation.
 *
 * :
 * Le modèle MVC manipulé est un objet JSON de la forme :
 * (start code)
 * {
 *  vues: [{
 *      nom: <nom de la vue>,
 *      url: <URL d'accès rapide à la vue>,
 * 	contextFile: <fichier de contexte sauvegardé sur le serveur pour rechargement rapide>,
 *      maxDate: <éventuelle date limite d'accès au fichier de contexte>
 *  },
 *  ...
 *  ],
 *  numVue: <index de la vue traitée par la vue MVC>
 * }
 * (end)
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'sauvegarder' de la classe définissant la vue MVC associée (<Descartes.UI.BookmarksInPlace> par défaut) déclenche la méthode <prepareSaveVue>.
 *  - l'événement 'supprimer' de la classe définissant la vue MVC associée (<Descartes.UI.BookmarksInPlace> par défaut) déclenche la méthode <removeVue>.
 *  - l'événement 'recharger' de la classe définissant la vue MVC associée (<Descartes.UI.BookmarksInPlace> par défaut) déclenche la méthode <prepareRefreshVue>.
 */
var Class = Utils.Class(Action, {
    /**
     * Propriete: acceptCookies
     * {Boolean} Indique si les vues peuvent être sauvegardées dans un cookie pour les sessions suivantes (lecture seule).
     */
    acceptCookies: true,

    /**
     * Propriete: map
     * {<Descartes.Map>} Carte sur laquelle opère le gestionnaire.
     */
    map: null,

    /**
     * Propriete: behavior
     * <Descartes.Action.BookmarksManager.BEHAVIOR_DEFAULT> |
     * <Descartes.Action.BookmarksManager.BEHAVIOR_PERSONAL> |
     * <Descartes.Action.BookmarksManager.BEHAVIOR_MANAGED_BY_CREATOR> : Comportement du gestionnaire
     */
    behavior: 0,

    /**
     * Propriete: withDelay
     * {Boolean} Indique si le serveur d'application proposant le service de gestion des contextes met en place un mécanisme de purge périodique.
     */
    withDelay: false,
    /**
     * Constructeur: Descartes.Action.BookmarksManager
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * map - {Descartes.Map} Carte sur laquelle intervient le contrôleur.
     * mapName - {String} Nom de la carte utilisé pour le cookie stockant les vues.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * behavior - <Descartes.Action.BookmarksManager.BEHAVIOR_DEFAULT> |
     * <Descartes.Action.BookmarksManager.BEHAVIOR_PERSONAL> |
     * <Descartes.Action.BookmarksManager.BEHAVIOR_MANAGED_BY_CREATOR>: Comportement du gestionnaire
     * withDelay - {Boolean} Indique si le serveur d'application proposant le service de gestion des contextes met en place un mécanisme de purge périodique.
     */
    initialize: function (div, map, mapName, options) {
        this.map = map;
        this.cookieName = 'contexte|*$' + mapName + '=';

        if (_.isNil(options)) {
            options = {};
        }

        if (!_.isNil(options.behavior)) {
            this.behavior = options.behavior;
            delete options.behavior;
        }
        if (!_.isNil(options.withDelay)) {
            this.withDelay = options.withDelay;
            delete options.withDelay;
        }


        Action.prototype.initialize.apply(this, [div, map, options]);

        this.model = {};
        this.model.vues = [];

        var cookies = document.cookie.split('; ');

        if (document.cookie.length === 0) {
            document.cookie = this.cookieName + JSON.stringify(this.model.vues) + '; expires=' + Utils.COOKIE_DATE_EXPIRATION.toGMTString() + ';';
            if (document.cookie.length === 0) {
                this.acceptCookies = false;
            }
        } else {
            for (var iCookie = 0; iCookie < cookies.length; iCookie++) {
                var pos = cookies[iCookie].indexOf(this.cookieName);
                if (pos !== -1) {
                    var cookieValue = cookies[iCookie].substring((this.cookieName).length + 1, cookies[iCookie].length - 1);
                    cookieValue = cookieValue.replace(/\\/g, '');
                    if (!_.isEmpty(cookieValue)) {
                        log.debug('cookieValue=', cookieValue);
                        cookieValue = '[' + cookieValue + ']';
                        this.model.vues = JSON.parse(cookieValue);
                    }

                }
            }
            if (this.behavior === BookmarksManagerConstants.BEHAVIOR_PERSONAL) {
                _.each(this.model.vues, function (vue) {
                    vue.url = null;
                });
            }
        }
        if (_.isNil(this.renderer)) {
            this.renderer = new BookmarksInPlace(div, this.model, options);
        }
        this.renderer.events.register('sauvegarder', this, this.prepareSaveVue);
        this.renderer.events.register('supprimer', this, this.tryRemoveVue);
        this.renderer.events.register('recharger', this, this.prepareRefreshVue);

        if (this.model.vues.length !== 0 && (this.behavior === BookmarksManagerConstants.BEHAVIOR_MANAGED_BY_CREATOR || this.withDelay)) {
            var checkBookmarks = '';
            for (var iVue = 0, lenVues = this.model.vues.length; iVue < lenVues; iVue++) {
                if (iVue !== 0) {
                    checkBookmarks += ',';
                }
                checkBookmarks += this.model.vues[iVue].contextFile;
            }

            var xhr = new XMLHttpRequest();
            xhr.open('GET', Descartes.CONTEXT_MANAGER_SERVER);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            xhr.onload = function () {
                if (xhr.status === 200) {
                    this.actualizeCheckedBookmarks(xhr.responseText);
                } else {
                    this.renderer.draw();
                }
            }.bind(this);
            xhr.send(Utils.encode({
                check: encodeURI(checkBookmarks)
            }));
        } else {
            this.renderer.draw();
        }
    },

    /**
     * Methode: actualizeCheckedBookmarks
     * Vérifie la disponibilité des vues sur le serveur d'application
     *
     * Paramètres:
     * response - {String} Réponse AJAX contenant les informations de disponibilité des fichiers sauvegardés.
     */
    actualizeCheckedBookmarks: function (response) {
        var checks = response.split('|');
        var missingFiles = false;
        for (var i = checks.length - 1; i >= 0; i--) {
            if (checks[i] === 'F') {
                this.model.vues.splice(i, 1);
                missingFiles = true;
            } else if (checks[i] !== 'T' && this.withDelay) {
                this.model.vues[i].maxDate = checks[i];
            } else {
                this.model.vues[i].maxDate = null;
            }
        }
        if (missingFiles && this.acceptCookies) {
            document.cookie = this.cookieName + JSON.stringify(this.model.vues) + '; expires=' + Utils.COOKIE_DATE_EXPIRATION.toGMTString() + ';';
        }
        this.renderer.draw();
    },
    /**
     * Methode: prepareSaveVue
     * Prépare le stockage d'une vue personnalisée.
     *
     * :
     * Une requête AJAX est envoyée au le serveur pour stocker le fichier de contexte.
     * La méthode <saveVue> est ensuite appelée.
     *
     * Paramètres:
     * nomIHM - {String} Nom de la vue.
     */
    prepareSaveVue: function (nomIHM) {
        var alertMsg = this.getMessage('NO_COOKIES_ALERT');
        if (typeof nomIHM !== 'string') {
            nomIHM = '';
        }

        var dialog = new SaveBookmarkDialog(null, this.model, {
            id: this.id + '_BookmarkDialog',
            title: this.getMessage('SAVE_VIEW'),
            label: this.getMessage('SAVE_VIEW'),
            type: 'default'
        });
        dialog.events.register('choosed', this, this.handleSaveVue);
        dialog.draw();


    },
    handleSaveVue: function (event) {

        var name = event.data[0].name;

        if (!_.isNil(name)) {
            if (name.replace(/^\s+|\s+$/g, '') !== "") {
                var nomVue = name.replace(/^\s+|\s+$/g, '');
                if (!nomVue.match("^[a-zA-Z0-9_. -]*$")) {
                    alert(this.getMessage('BAD_NAME_VIEW_ALERT'));
                    this.prepareSaveVue(name);
                } else {
                    if (this.model.vues.some(function (element) {
                        return (element.nom === nomVue);
                    })) {
                        alert(this.getMessage('EXISTING_VIEW_ALERT'));
                        this.prepareSaveVue(name);
                    } else {
                        var olMap = this.map.OL_map;
                        var vue = {
                            nom: nomVue,
                            url: '',
                            contextFile: ''
                        };
                        var size = olMap.getSize();
                        var bbox = olMap.getView().calculateExtent(size);

                        var context = {
                            items: this.map.mapContent.serialize(),
                            bbox: {
                                xMin: bbox[0],
                                yMin: bbox[1],
                                xMax: bbox[2],
                                yMax: bbox[3]
                            },
                            size: {
                                w: size[0],
                                h: size[1]
                            },
                            infosSiteOrig: {
                                locationHref: document.location.href.split('#')[0],
                                action: "dBookmarks",
                                date: Utils.formatCurrentDate()
                            }
                        };

                        var self = this;

                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', Descartes.CONTEXT_MANAGER_SERVER);
                        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                        xhr.onload = function () {
                            self.saveVue(vue, xhr.responseText);
                        };
                        xhr.send(Utils.encode({
                            view: encodeURI(nomVue),
                            context: JSON.stringify(context)
                        }));
                    }
                }
            } else {
                alert(this.getMessage('NAME_VIEW_ALERT'));
                this.prepareSaveVue(name);
            }
        }
    },
    /**
     * Methode: saveVue
     * Actualise la vue MVC et met à jour le cookie avec le nom du fichier de contexte correspondant la vue créée.
     *
     * Paramètres:
     * vue - {Object} Vue concerné (élément du tableau stocké dans le modèle).
     * response - {String} Réponse AJAX contenant le nom du fichier de contexte.
     */
    saveVue: function (vue, response) {
        response = JSON.parse(response);
        vue.contextFile = response.contextFile;
        vue.maxDate = null;
        if (!_.isNil(response.maxDate)) {
            vue.maxDate = response.maxDate;
        }
        if (this.behavior === BookmarksManagerConstants.BEHAVIOR_PERSONAL) {
            vue.url = null;
        } else {
            var urlVue = new Url(document.location.href);
            urlVue.alterOrAddParam('context', vue.contextFile);
            vue.url = urlVue.getUrlString();
        }

        this.model.vues.push(vue);
        if (this.acceptCookies) {
            document.cookie = this.cookieName + JSON.stringify(this.model.vues) + '; expires=' + Utils.COOKIE_DATE_EXPIRATION.toGMTString() + ';';
        }
        this.renderer.draw();
    },
    /**
     * Methode: removeVue
     * Supprime une vue personnalisée.
     */
    tryRemoveVue: function () {
        var that = this;

        var message = this.getMessage('DELETE_VIEW') + this.model.vues[this.model.numVue].nom + this.getMessage('DELETE_VIEW_END');
        var dialog = new ConfirmDialog({
            id: this.id + '_removeDialog',
            title: this.getMessage('DELETE_VIEW_TITLE'),
            message: message,
            type: 'remove'
        });
        dialog.open(function (result) {
            if (result) {
                 that.removeVue();
            }
        });

    },
    removeVue: function () {
        if (this.behavior === BookmarksManagerConstants.BEHAVIOR_PERSONAL ||
                this.behavior === BookmarksManagerConstants.BEHAVIOR_MANAGED_BY_CREATOR) {
            var deleteParam = 'delete=' + encodeURI(this.model.vues[this.model.numVue].contextFile);
            var url = Descartes.CONTEXT_MANAGER_SERVER + '?' + deleteParam;

            var xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            xhr.send();
        }
        this.model.vues.splice(this.model.numVue, 1);
        if (this.acceptCookies) {
            document.cookie = this.cookieName + JSON.stringify(this.model.vues) + '; expires=' + Utils.COOKIE_DATE_EXPIRATION.toGMTString() + ';';
        }

        this.renderer.draw();
    },
    /**
     * Methode: prepareRefreshVue
     * Prépare la restauration d'une vue personnalisée.
     *
     * :
     * Une requête AJAX est envoyée au le serveur pour récupérer le fichier de contexte.
     * La méthode <refreshVue> est ensuite appelée.
     */
    prepareRefreshVue: function () {
        var that = this;
        var url = Descartes.CONTEXT_MANAGER_SERVER + '?' + encodeURI(this.model.vues[this.model.numVue].contextFile);

        var xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        xhr.onload = function () {
            if (xhr.status === 200) {
                that.refreshVue(xhr.responseText);
            } else {
                that.showError();
            }
        };
        xhr.send();
    },
    /**
     * Methode: refreshVue
     * Actualise la carte avec le contexte correspondant à la vue demandée.
     *
     * Paramètres:
     * response - {String} Réponse AJAX contenant le contexte sous forme d'objet JSON.
     */
    refreshVue: function (response) {
        if (response.charAt(0) !== '{') {
            response = response.substring(response.indexOf('{'));
        }
        var context = JSON.parse(response);
        var content = context.items;
        this.map.mapContentManager.populate(content);

        var bbox = context.bbox;
        var mapType = this.map.OL_map.get('mapType');
        var constrainResolution = mapType === MapConstants.MAP_TYPES.DISCRETE;

        this.map.OL_map.getView().fit([bbox.xMin, bbox.yMin, bbox.xMax, bbox.yMax], {
            constrainResolution: constrainResolution,
            minResolution: this.map.OL_map.getView().getMinResolution(),
            maxResolution: this.map.OL_map.getView().getMaxResolution()
        });
        var mapSize = context.size;
        if (!_.isNil(this.map.sizeSelector)) {
            this.map.sizeSelector.selectSize(mapSize.w, mapSize.h);
        }
        if (this.map.selectToolTip && this.map.selectToolTip.selectToolTipLayers && this.map.selectToolTip.selectToolTipLayers.length !== 0) {
           this.map.selectToolTip.selectToolTipLayers.forEach(function (selectToolTipLayer) {
              if (selectToolTipLayer.layer.id === Descartes.AnnotationsLayer.id) {
                  selectToolTipLayer.layer.OL_layers[0] = Descartes.AnnotationsLayer.OL_layers[0];
              }
           }, this);
        }
        this.map.OL_map.removeInteraction(this.map.selectToolTip.selectInteraction);
        this.map.OL_map.addInteraction(this.map.selectToolTip.selectInteraction);
        this.map.selectToolTip.selectInteraction.setActive(true);
    },

    /**
     * Methode: showError
     * Affiche les messages d'erreur.
     */
    showError: function () {
        alert(this.getMessage('MISSING_VIEW'));
    },
    CLASS_NAME: 'Descartes.Action.BookmarksManager'
});

module.exports = Class;
