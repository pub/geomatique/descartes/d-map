/* global MODE */

var _ = require('lodash');
var log = require('loglevel');

var Utils = require('../Utils/DescartesUtils');

var Action = require('../Core/Action');

var SizeSelectorInPlace = require('../UI/' + MODE + '/SizeSelectorInPlace');

/**
 * Class: Descartes.Action.SizeSelector
 * Classe permettant de changer la taille de la carte suite au choix dans une liste.
 *
 * :
 * Le modèle MVC manipulé est un objet JSON de la forme :
 * (start code)
 * {
 * 	size: <taille>
 * }
 * (end)
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'changeSize' de la classe définissant la vue MVC associée (<Descartes.UI.SizeSelectorInPlace> par défaut) déclenche la méthode <gotoSize>.
 */
var SizeSelector = Utils.Class(Action, {
    /**
     * Propriete: sizeList
     * {Array(Object)} Liste des tailles disponibles pour le choix.
     */
    sizeList: [[450, 300], [600, 400], [750, 500], [900, 600]],

    /**
     * Propriete: defaultSize
     * {Integer} Index de la taille courante dans le tableau de la liste des tailles.
     */
    defaultSize: 1,

    /**
     * Constructeur: Descartes.Action.SizeSelector
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * olMap - {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * sizeList - {Array(Size)} Liste des tailles disponibles pour le choix.
     * defaultSize - {Integer} Index de la taille courante dans le tableau de la liste des tailles.
     *
     * :
     * Options propres à la vue associée.
     * Voir <Descartes.UI.SizeSelectorInPlace.Descartes.UI.SizeSelectorInPlace> pour la vue par défaut.
     */
    initialize: function (div, olMap, options) {
        if (!_.isNil(options)) {
            _.extend(this, options);
        }

        Action.prototype.initialize.apply(this, arguments);

        if (_.isNil(this.renderer)) {
            this.renderer = new SizeSelectorInPlace(div, this.model, options);
        }

        this.renderer.events.register('changeSize', this, this.gotoSize);
        if (olMap.descartesAutoSize) {
            this.sizeList = "auto";
        }
        this.renderer.draw(this.sizeList, this.defaultSize);
    },

    /**
     * Methode: gotoSize
     * Change la taille de la carte selon la valeur portée par le modèle.
     */
    gotoSize: function () {
        var view = this.olMap.getView();
        var center = view.getCenter();
        var zoom = view.getZoom();

        this.olMap.getTargetElement().style.width = this.model.size.w + 'px';
        this.olMap.getTargetElement().style.height = this.model.size.h + 'px';

        view.setCenter(center);
        view.setZoom(zoom);

        this.olMap.updateSize();
    },
    /**
     * Methode: selectSize
     * Sélectionne une taille parmi celles disponibles et enchaîne avec la méthode <done>.
     *
     * :
     * Cette méthode a vocation à être utilisée pour déclencher un changement de taille demandé par un autre moyen que la sélection
     * grâce à la vue (à partir de la restauration d'un contexte de consultation par exemple).
     *
     * Paramètres:
     * width - {Integer} Largeur souhaitée de la carte.
     * height - {Integer} Hauteur souhaitée de la carte.
     */
    selectSize: function (width, height) {
        var selectedIndex = null;
        _.each(this.sizeList, function (size, index) {
            if (size[0] === width && size[1] === height) {
                selectedIndex = index;
            }
        });

        if (!_.isNil(selectedIndex)) {
            this.renderer.draw(this.sizeList, selectedIndex);
            this.renderer.done();
        } else {
            log.warn('Impossible de selectionner la taille %sx%s', width, height);
        }
    },
    CLASS_NAME: 'Descartes.Action.SizeSelector'
});
module.exports = SizeSelector;
