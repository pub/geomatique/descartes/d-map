/* global MODE */

var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var DMapConstants = require('../DMapConstants');

var Action = require('../Core/Action');
var MapConstants = require('../Map/MapConstants');

var GazetteerInPlace = require('../UI/' + MODE + '/GazetteerInPlace');
var LocalisationParcellaire = require('../Model/LocalisationParcellaire');

/**
 * Class: Descartes.Action.Gazetteer
 * Classe permettant le recadrage de la carte suite au choix d'un objet de référence dans un ensemble de listes déroulantes imbriquées.
 *
 * :
 * Le modèle MVC manipulé est un objet JSON de la forme :
 * (start code)
 * {
 *      activeZone: {
 * 		xmin: <abscisse minimale de l'emprise de l'objet>,
 * 		ymin: <ordonnée minimale de l'emprise de l'objet>,
 * 		xmax: <abscisse maximale de l'emprise de l'objet>,
 * 		ymax: <ordonnée maximale de l'emprise de l'objet>
 * 	}
 * }
 * (end)
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Classes dérivées:
 *  - <Descartes.Action.DefaultGazetteer>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'localize' de la classe définissant la vue MVC associée (<Descartes.UI.GazetteerInPlace> par défaut) déclenche la méthode <localize>.
 */
var Class = Utils.Class(Action, {
    /**
     * Constructeur: Descartes.Action.Gazetteer
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * OL_map - {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * projection - {String} Code de la projection de la carte.
     * initValue - {String} Code de la valeur de l'objet "père" des objets du plus haut niveau de localisation (i.e. d'index 0).
     * levels - {Array(<Descartes.GazetteerLevel>)} Liste des niveaux de localisation.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * service - {<Descartes.GazetteerService>} Service côté serveur pour la fourniture des objets de référence.
     * location - {String} Répertoire de localisation des fichiers statiques contenant les objets de référence ("descartes/gazetteer/" par défaut).
     */
    initialize: function (div, OL_map, projection, initValue, levels, options) {
        this.model = {};

        var opts = {};
        opts.proj = projection.getCode().replace(':', '_');
        opts.initValue = initValue;
        opts.levels = levels;
        _.extend(opts, options);
        if (options && options.service && options.service.type === LocalisationParcellaire.TYPE_SERVICE) {
            opts.service = new LocalisationParcellaire(opts.service.niveauBase);
        }
        Action.prototype.initialize.apply(this, [div, OL_map, opts]);

        if (_.isNil(this.renderer)) {
            this.renderer = new GazetteerInPlace(div, this.model, opts);
        }
        this.renderer.events.register('localize', this, this.localize);
        this.renderer.draw();
    },

    /**
     * Methode: localize
     * Recadre la carte selon l'emprise de l'objet de référence portée par le modèle.
     */
    localize: function () {
        try {
            var mapType = this.olMap.get('mapType');
            var constrainResolution = mapType === MapConstants.MAP_TYPES.DISCRETE;

            var extent = [
                this.model.activeZone.xmin,
                this.model.activeZone.ymin,
                this.model.activeZone.xmax,
                this.model.activeZone.ymax
            ];
            if ((this.olMap.getView().getProjection().getCode() !== 'EPSG:4326') && (this.model.activeZone.xmax - this.model.activeZone.xmin) < DMapConstants.MIN_BBOX &&
                    (this.model.activeZone.ymax - this.model.activeZone.ymin) < DMapConstants.MIN_BBOX) {
                var newXmin = this.model.activeZone.xmin + (this.model.activeZone.xmax - this.model.activeZone.xmin) / 2 - DMapConstants.MIN_BBOX / 2;
                var newXmax = this.model.activeZone.xmin + (this.model.activeZone.xmax - this.model.activeZone.xmin) / 2 + DMapConstants.MIN_BBOX / 2;
                var newYmin = this.model.activeZone.ymin + (this.model.activeZone.ymax - this.model.activeZone.ymin) / 2 - DMapConstants.MIN_BBOX / 2;
                var newYmax = this.model.activeZone.ymin + (this.model.activeZone.ymax - this.model.activeZone.ymin) / 2 + DMapConstants.MIN_BBOX / 2;
                extent = [newXmin, newYmin, newXmax, newYmax];
            }
            this.olMap.getView().fit(extent, {
                constrainResolution: constrainResolution,
                minResolution: this.olMap.getView().getMinResolution(),
                maxResolution: this.olMap.getView().getMaxResolution()
            });
        } catch (e) {
        }
    },
    CLASS_NAME: 'Descartes.Action.Gazetteer'
});
module.exports = Class;
