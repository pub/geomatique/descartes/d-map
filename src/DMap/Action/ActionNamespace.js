/* global GEOREF */
var _ = require('lodash');

var AjaxSelection = require('./AjaxSelection');
var BookmarksManager = require('./BookmarksManager');
var BookmarksManagerConstants = require('./BookmarksManagerConstants');
var CoordinatesInput = require('./CoordinatesInput');
var DefaultGazetteer = require('./DefaultGazetteer');
var DefaultGazetteerConstants = require('./DefaultGazetteerConstants');
var Gazetteer = require('./Gazetteer');
var MapContentManager = require('./MapContentManager');
var PrinterParamsManager = require('./PrinterParamsManager');
var RequestManager = require('./RequestManager');
var RequestManagerConstants = require('./RequestManagerConstants');
var ScaleChooser = require('./ScaleChooser');
var ScaleSelector = require('./ScaleSelector');
var SizeSelector = require('./SizeSelector');
var MapContentManagerConstants = require('./MapContentManagerConstants');
var Action = require('../Core/Action');

_.extend(BookmarksManager, BookmarksManagerConstants);
_.extend(MapContentManager, MapContentManagerConstants);
_.extend(DefaultGazetteer, DefaultGazetteerConstants);
_.extend(RequestManager, RequestManagerConstants);

var namespace = {
    AjaxSelection: AjaxSelection,
    BookmarksManager: BookmarksManager,
    CoordinatesInput: CoordinatesInput,
    DefaultGazetteer: DefaultGazetteer,
    Gazetteer: Gazetteer,
    MapContentManager: MapContentManager,
    PrinterParamsManager: PrinterParamsManager,
    RequestManager: RequestManager,
    ScaleSelector: ScaleSelector,
    ScaleChooser: ScaleChooser,
    SizeSelector: SizeSelector
};

if (GEOREF) {
    var LocalisationAdresseConstants = require('./LocalisationAdresseConstants');
    var LocalisationAdresse = require('./LocalisationAdresse');

    _.extend(LocalisationAdresse, LocalisationAdresseConstants);
    namespace.LocalisationAdresse = LocalisationAdresse;
}

_.extend(Action, namespace);

/**
 * @module Descartes
 * @submodule Action
 */
module.exports = Action;

