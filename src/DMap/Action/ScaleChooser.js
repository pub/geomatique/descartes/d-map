/* global MODE */

var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var Action = require('../Core/Action');
var ScaleSelectorInPlace = require('../UI/' + MODE + '/ScaleChooserInPlace');

/**
 * Class: Descartes.Action.ScaleChooser
 * Classe permettant le recadrage de la carte suite à la saisie d'une échelle de visualisation.
 *
 * :
 * Le modèle MVC manipulé est un objet JSON de la forme :
 * (start code)
 * {
 *      scale: <dénominateur d'échelle>
 * }
 * (end)
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'choosed' de la classe définissant la vue MVC associée (<Descartes.UI.ScaleChooserInPlace> par défaut) déclenche la méthode <gotoScale>.
 */
var Class = Utils.Class(Action, {
    /**
     * Constructeur: Descartes.Action.ScaleChooser
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * olMap - {ol.Map} Carte OpenLayers sur laquelle intervient le contrêleur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * Options propres à la vue associée. Voir <Descartes.UI.ScaleChooserInPlace.Descartes.UI.ScaleChooserInPlace> pour la vue par défaut.
     */
    initialize: function (div, olMap, options) {
        this.model = {};
        Action.prototype.initialize.apply(this, arguments);

        if (_.isNil(this.renderer)) {
            this.renderer = new ScaleSelectorInPlace(div, this.model, options);
        }

        var maxResolution = this.olMap.getView().getMaxResolution();
        var minResolution = this.olMap.getView().getMinResolution();

        var units = this.olMap.getView().getProjection().getUnits();

        var minScaleDenominator = Utils.getScaleFromResolution(minResolution, units);
        var maxScaleDenominator = Utils.getScaleFromResolution(maxResolution, units);

        this.renderer.events.register('choosed', this, this.gotoScale);
        this.renderer.draw(minScaleDenominator, maxScaleDenominator);
    },

    gotoScale: function () {
        var unit = this.olMap.getView().getProjection().getUnits();
        var resolution = Utils.getResolutionForScale(this.model.scale, unit);
        this.olMap.getView().setResolution(resolution);
    },

    CLASS_NAME: 'Descartes.Action.ScaleChooser'
});
module.exports = Class;
