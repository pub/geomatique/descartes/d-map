module.exports = {
    /**
     * Constante: REGION
     * Type d'entités administratives représentant des régions.
     */
    REGION: 0,
    /**
     * Constante: DEPARTEMENT
     * Type d'entités administratives représentant des départements.
     */
    DEPARTEMENT: 1,
    /**
     * Constante: COMMUNE
     * Type d'entités administratives représentant des communes.
     */
    COMMUNE: 2
};
