var LocalisationAdresseLayer = require('../Model/LocalisationAdresseLayer');

var defaultName = 'LocalisationAdresseLayer';

module.exports = {
    /**
     * Constante: Name
     * Nom de la couche associée à l'assistant de localisation à l'adresse.
     */
    Name: defaultName,
    /**
     * Constante: Layer
     * Couche <Descartes.Layer.LocalisationAdresseLayer> associée à l'assistant de localisation à l'adresse.
     */
    Layer: new LocalisationAdresseLayer(defaultName)
};
