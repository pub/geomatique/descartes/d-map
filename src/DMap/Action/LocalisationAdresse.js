/* global MODE, Descartes */

var _ = require('lodash');
var ol = require('openlayers');

var Utils = require('../Utils/DescartesUtils');
var Url = require('../Model/Url');

var Action = require('../Core/Action');

var LocalisationAdresseInPlace = require('../UI/' + MODE + '/LocalisationAdresseInPlace');

var LocalisationAdresseConstants = require('./LocalisationAdresseConstants');

var defaultSymbolizers = require('../Symbolizers');

/**
 * Class: Descartes.Action.LocalisationAdresse
 * Classe permettant la localisation d'une adresse sur la carte suite à une saisie.
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'recherche' de la classe définissant la vue MVC associée (<Descartes.UI.LocalisationAdresseInPlace> par défaut) déclenche la méthode <recherche>.
 *  - l'événement 'localise' de la classe définissant la vue MVC associée (<Descartes.UI.LocalisationAdresseInPlace> par défaut) déclenche la méthode <localize>.
 *  - l'événement 'efface' de la classe définissant la vue MVC associée (<Descartes.UI.LocalisationAdresseInPlace> par défaut) déclenche la méthode <clear>.
 */
var Class = Utils.Class(Action, {

    tempResults: [],

    vectorLayer: null,

    pointFeatures: [],
    vectorPopup: null,
    vectorLayerControl: null,

    jdonrefProjection: null,
    addokProjection: null,
    mapProjection: null,

    mapContent: null,

    /**
     * Propriete: urlService
     * {String} l'url du service distant.
     */
    urlService: null,

    /**
     * Propriete: urlProxy
     * {String} l'url du proxy.
     */
    urlProxy: null,

    _useGeorefServer: false,
    useProxy: false,

    defaultAddockServer: "https://api-adresse.data.gouv.fr/search/",
    defaultAddockSearchLimit: 15,
    /**
     * Constructeur: Descartes.Action.LocalisationAdresse
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * olMap - {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     */
    initialize: function (div, olMap, options) {
        Action.prototype.initialize.apply(this, arguments);

        var urlDescartesProxy = new Url(Descartes.PROXY_SERVER);
        this.urlProxy = urlDescartesProxy.getUrlString();
        if (this.urlProxy.indexOf('?') === -1) {
            this.urlProxy += '?';
        }

        this.urlService = this.defaultAddockServer;
        this.limit = this.defaultAddockSearchLimit;
        this._useGeorefServer = false;

        if (!_.isNil(Descartes.ADDOK_SERVER)) {
             this.urlService = Descartes.ADDOK_SERVER;
             this._useGeorefServer = false;
             this.useProxy = false;
        }

        if (!_.isNil(options) && !_.isNil(options.useProxy)) {
             this.useProxy = options.useProxy;
        }
        if (!_.isNil(options) && !_.isNil(options.limit)) {
            this.limit = options.limit;
       }

        this.model = {
            criteres: {
                adresse: '',
                commune: ''
            },
            resultats: []
        };
        if (_.isNil(this.renderer)) {
            this.renderer = new LocalisationAdresseInPlace(div, this.model, options);
        }

        if (!this.isPresentVectorLayerAdresse()) {
            this.vectorLayer = LocalisationAdresseConstants.Layer.olLayer;
            this.olMap.addLayer(this.vectorLayer);
        } else {
            var layers = this.olMap.getLayers().getArray();
            for (var i = 0, len = layers.length; i < len; i++) {
                var layer = layers[i];
                if (layer.get('title') === LocalisationAdresseConstants.Name) {
                    this.vectorLayer = layer;
                }
            }
        }

        var that = this;

        var olStyles = defaultSymbolizers.getOlStyle(defaultSymbolizers.Descartes_Symbolizers_SelectLocalisationAdresse);

        this.selectInteraction = new ol.interaction.Select({
            condition: ol.events.condition.pointerMove,
            layers: [this.vectorLayer],
            style: function (feature, resolution) {
                var featureStyleFunction = feature.getStyleFunction();
                if (featureStyleFunction) {
                    return featureStyleFunction.call(feature, resolution);
                } else {
                    var style = olStyles[feature.getGeometry().getType()];
                    return style;
                }
            }
        });
        this.selectInteraction.on('select', function (evt) {
            if (evt.selected.length > 0) {
                this.feature = evt.selected[0];

                that.clearOverlays();

                var element = document.createElement('div');
                element.setAttribute('id', 'locapopup');
                element.className = 'Descartes-popup';

                var text = '';
                if (!_.isEmpty(this.feature.get('adresse'))) {
                    text = this.feature.get('adresse');
                    text += ' ';
                }
                text += this.feature.get('commune');

                var closer = document.createElement('a');
                closer.setAttribute('href', '#');
                closer.setAttribute('id', 'locapopup-closer');
                closer.className = 'Descartes-popup-closer';
                closer.onclick = function () {
                    that.olMap.removeOverlay(that.popup);
                    return false;
                  };
                 element.appendChild(closer);

                var content = document.createElement('div');
                content.setAttribute('id', 'locapopup-content');
                content.innerHTML = text;

                element.appendChild(content);

                this.popup = new ol.Overlay(({
                    id: this.feature.get('id') + '_overlay',
                    element: element,
                    autoPan: true,
                    position: this.feature.getGeometry().getCoordinates()
                }));

                that.olMap.addOverlay(this.popup);
            }
        }.bind(this));


        this.olMap.addInteraction(this.selectInteraction);

        this.mapProjection = this.olMap.getView().getProjection();
        this.jdonrefProjection = new Descartes.Projection("EPSG:2154");
        this.addokProjection = new Descartes.Projection("EPSG:4326");

        this.renderer.events.register('recherche', this, this.recherche);
        this.renderer.events.register('localise', this, this.localize);
        this.renderer.events.register('efface', this, this.clear);
        this.renderer.draw();
    },

    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
        this.mapContent.events.register('changed', this, this.mapContentChanged);
    },

    mapContentChanged: function () {
        if (!this.isPresentVectorLayerAdresse()) {
            this.olMap.addLayer(this.vectorLayer);
        }
    },

    /**
     * Methode: recherche
     * Recherche de l'adresse sur le serveur distant.
     */
    recherche: function () {
        this.model.resultats = [];

        if (!this.isPresentVectorLayerAdresse()) {
            this.olMap.addLayer(this.vectorLayer);
        }

        this.vectorLayer.getSource().clear();

        this.pointFeatures = [];
        var self = this;

        var url = this.urlService;
        if (!_.endsWith(this.urlService, '?')) {
            url += '?';
        }
        if (this._useGeorefServer) {
            url += Utils.encode({
                operation: 'recherche',
                adresse: encodeURIComponent(this.model.adresse),
                commune: encodeURIComponent(this.model.commune)
            });
        } else {
            var coords = ol.proj.transform(this.olMap.getView().getCenter(), this.mapProjection.getCode(), this.addokProjection.getCode());
            var query = "";
            if (this.model.adresse !== "") {
                query = this.model.adresse.replace(' ', '+') + '+';
            }
            query += this.model.commune.replace(' ', '+');
            url += Utils.encode({
                q: query,
                limit: this.limit,
                lon: coords[0],
                lat: coords[1]
            });
        }
        var finalUrl;
        if (this.useProxy) {
            url = encodeURIComponent(url);
            finalUrl = this.urlProxy + url;
        } else {
            finalUrl = url;
        }

        var xhr = new XMLHttpRequest();
        xhr.open('GET', finalUrl);
        xhr.onload = function () {
            if (this.status === 200) {
                 if (self._useGeorefServer) {
                     self.geocodeResults(this.responseText);
                 } else {
                     var res = JSON.parse(this.responseText);
                     self.tempResults = res.features;
                     self.validResultsAddok();
                 }
            } else {
                self.showError();
            }
        };
        xhr.send();
    },

    /**
     * Methode: geocodeResults
     * Géocodage des résultats.
     */
    geocodeResults: function (responseText) {
        var self = this;
        var res = JSON.parse(responseText);
        this.tempResults = [];
        if (!_.isNil(res) && !_.isNil(res.adresses)) {
            for (var iA = 0, lenA = res.adresses.length; iA < lenA; iA++) {
                this.tempResults.push({
                    codeInsee: res.adresses[iA].codeinsee,
                    idVoie: res.adresses[iA].idvoie,
                    adresse: res.adresses[iA].ligne4,
                    commune: res.adresses[iA].ligne6,
                    note: res.adresses[iA].note
                });
            }
        }
        if (!_.isNil(res) && !_.isNil(res.communes)) {
            for (var iC = 0, lenC = res.communes.length; iC < lenC; iC++) {
                this.tempResults.push({
                    codeInsee: res.communes[iC].codeinsee,
                    idVoie: '',
                    adresse: '',
                    commune: res.communes[iC].ligne6,
                    note: res.communes[iC].note
                });
            }
        }

        if (this.tempResults.length === 0) {
            this.renderer.showResults();
        } else {
            var index = -1;
            this.resultsGeocoded = 0;
            for (var iR = 0, lenR = this.tempResults.length; iR < lenR; iR++) {
                this.tempResults[iR].ok = false;
                index++;

                var url = this.urlService;
                if (!_.endsWith(this.urlService, '?')) {
                    url += '?';
                }
                url += Utils.encode({
                    operation: 'geocode',
                    id_voie: encodeURIComponent(this.tempResults[iR].idVoie),
                    adresse: encodeURIComponent(this.tempResults[iR].adresse),
                    commune: encodeURIComponent(this.tempResults[iR].codeInsee),
                    rang: index
                });
                var finalUrl;
                if (this.useProxy) {
                    url = encodeURIComponent(url);
                    finalUrl = this.urlProxy + url;
                } else {
                    finalUrl = url;
                }

                var xhr = new XMLHttpRequest();
                xhr.open('GET', finalUrl);
                xhr.onload = function () {
                    if (this.status === 200) {
                        self.addGeomToResults(this.responseText);
                    } else {
                        self.ignoreGeomToResults();
                    }
                };
                xhr.send();
            }
        }
    },

    addGeomToResults: function (responseText) {
        this.resultsGeocoded++;
        var res = JSON.parse(responseText);
        var index = res.rang;
        if (!_.isNil(res.geocodage)) {
            this.tempResults[index].coords = [res.geocodage.x, res.geocodage.y];
            this.tempResults[index].ok = true;
        }
        if (this.resultsGeocoded === this.tempResults.length) {
            this.validResults();
        }
    },

    ignoreGeomToResults: function () {
        this.resultsGeocoded++;
        if (this.resultsGeocoded === this.tempResults.length) {
            this.validResults();
        }
    },

    /**
     * Methode: validResults
     * Vérification pour ne retourner que les résultats contenus dans l'emprise de la carte.
     */
    validResults: function () {
        this.model.resultats = [];
        for (var iR = 0, lenR = this.tempResults.length; iR < lenR; iR++) {
            if (this.tempResults[iR].ok) {
                for (var j = 0, len = this.tempResults[iR].coords.length; j < len; j++) {
                    this.tempResults[iR].coords[j] = parseFloat(this.tempResults[iR].coords[j]);
                }

                // transformation XY -> map Projection
                this.tempResults[iR].coords = ol.proj.transform(this.tempResults[iR].coords, this.jdonrefProjection.getCode(), this.mapProjection.getCode());

                var extent = this.olMap.getView().getUpdatedOptions_().extent;
                if (!_.isNil(extent)) {
                    if (ol.extent.containsCoordinate(extent, this.tempResults[iR].coords)) {
                        this.model.resultats.push(this.tempResults[iR]);
                    }
                } else {
                    this.model.resultats.push(this.tempResults[iR]);
                }
            }
        }
        this.renderer.showResults();
        this.showResults();
    },

    /**
     * Methode: validResultsBAN
     * Vérification pour ne retourner que les résultats contenus dans l'emprise de la carte.
     */
    validResultsAddok: function () {
        this.model.resultats = [];
        for (var iR = 0, lenR = this.tempResults.length; iR < lenR; iR++) {

                // transformation XY -> map Projection
                this.tempResults[iR].coords = ol.proj.transform(this.tempResults[iR].geometry.coordinates, this.addokProjection.getCode(), this.mapProjection.getCode());
                this.tempResults[iR].adresse = this.tempResults[iR].properties.label;
                this.tempResults[iR].commune = '';
                var extent = this.olMap.getView().getUpdatedOptions_().extent;
                if (!_.isNil(extent)) {
                    if (ol.extent.containsCoordinate(extent, this.tempResults[iR].coords)) {
                         this.model.resultats.push(this.tempResults[iR]);
                    }
                } else {
                    this.model.resultats.push(this.tempResults[iR]);
                }
        }
        this.renderer.showResults();
        this.showResults();
    },

    /**
     * Methode: showResults
     * Affichage des résultats sur la carte.
     */
    showResults: function () {
        this.pointFeatures = [];
        for (var i = 0, len = this.model.resultats.length; i < len; i++) {
            var result = this.model.resultats[i];
            var point = new ol.geom.Point(result.coords);

            var pointFeature = new ol.Feature({
                id: this.id + '_' + i,
                geometry: point,
                adresse: this.model.resultats[i].adresse,
                commune: this.model.resultats[i].commune
            });
            this.pointFeatures.push(pointFeature);
        }
        this.vectorLayer.getSource().addFeatures(this.pointFeatures);
    },

    /**
     * Methode: clear
     * Suppression des résultats affichés sur la carte.
     */
    clear: function () {
        this.vectorLayer.getSource().clear();
        this.selectInteraction.getFeatures().clear();
        this.clearOverlays();
    },

    clearOverlays: function () {
        var that = this;
        this.olMap.getOverlays().forEach(function (overlay) {
            if (overlay.getId().indexOf(that.id) !== -1) {
                that.olMap.removeOverlay(overlay);
            }
        });
    },

    showError: function () {
        alert('Erreur asssitant localisation à l\'adresse');
    },

    /**
     * Methode: localize
     * Localise l'adresse.
     */
    localize: function () {
        var coords = this.model.resultats[this.model.resultatActif].coords;
        for (var i = 0; i < coords.length; i++) {
            coords[i] = Number(coords[i]);
        }

        var extent = this.olMap.getView().getUpdatedOptions_().extent;
        if (!_.isNil(extent)) {
            if (ol.extent.containsCoordinate(extent, coords)) {
                this.olMap.getView().setCenter(coords);
            }
        } else {
            this.olMap.getView().setCenter(coords);
        }
        var feature = this.pointFeatures[this.model.resultatActif];
        this.selectInteraction.getFeatures().clear();
        this.selectInteraction.getFeatures().push(feature);
        this.selectInteraction.dispatchEvent({type: 'select', selected: [feature]});
    },

    /*
     * private
     */
    isPresentVectorLayerAdresse: function () {
        var layers = this.olMap.getLayers().getArray();
        for (var i = 0, len = layers.length; i < len; i++) {
            if (layers[i].get('title') === LocalisationAdresseConstants.Name) {
                return true;
            }
        }
        return false;
    },
    CLASS_NAME: 'Descartes.Action.LocalisationAdresse'
});
module.exports = Class;
