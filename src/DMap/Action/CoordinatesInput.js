/* global MODE */
var ol = require('openlayers');
var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');

var Action = require('../Core/Action');
var Projection = require('../Core/Projection');

var CoordinatesInputInPlace = require('../UI/' + MODE + '/CoordinatesInputInPlace');

/**
 * Class: Descartes.Action.CoordinatesInput
 * Classe permettant le recentrage de la carte suite à la saisie d'un couple de coordonnées géographiques.
 *
 * :
 * Le modèle MVC manipulé est un objet JSON de la forme :
 * (start code)
 * {
 * 		x: <abscisse>,
 * 		y: <ordonnée>
 * }
 * (end)
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'recentrage' de la classe définissant la vue MVC associée (<Descartes.UI.CoordinatesInputInPlace> par défaut) déclenche la méthode <gotoCoordinates>.
 */
var Class = Utils.Class(Action, {
    /**
     * Propriete: projection
     * {Boolean} Indicateur pour la présence du nom de la projection à l'affichage (false par défaut).
     */
    projection: false,
    /**
     * Propriété: displayProjectionsList
     * {Array} Tableau définissant les projections des coordonnées à saisir.
     */
    displayProjections: [],
    /**
     * Propriété: checkCoordinatesInMAxExtent
     * {Boolean} Vérifier si les coordonnées saisies sont dans l'emprise max de la carte.
     */
    checkCoordinatesInMaxExtent: true,
    /**
     * Propriété: selectedDisplayProjectionIndex
     * {Integer} Position de la projection sélectionnée
     */
    selectedDisplayProjectionIndex: 0,
    _selectedDisplayProjection: "",
    _mapProjection: null,
    /**
     * Constructeur: Descartes.Action.CoordinatesInput
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * olMap - {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * Options propres à la vue associée.
     * Voir <Descartes.UI.CoordinatesInputInPlace> | <Descartes.UI.CoordinatesInputDialog> pour la vue par défaut.
     */
    initialize: function (div, olMap, options) {
        if (!_.isNil(options)) {
            _.extend(this, options);
        }

        Action.prototype.initialize.apply(this, arguments);
        if (!_.isNil(olMap.getView().options_.extent)) {
            this.model.maxExtent = olMap.getView().options_.extent;
        }

        this._mapProjection = olMap.getView().getProjection();

        if (!(this.projection && this.displayProjections.length > 0)) {
            this.displayProjections = [];
        }

        if (this.displayProjections.length === 0) {
            this.displayProjections.push(this._mapProjection.getCode());
            this.selectedDisplayProjectionIndex = 0;
        }

        this._selectedDisplayProjection = this.displayProjections[this.selectedDisplayProjectionIndex];

        this.model.projection = this.projection;
        this.model.mapProjection = this._mapProjection.getCode();
        this.model.displayProjections = this.displayProjections;
        this.model.selectedDisplayProjectionIndex = this.selectedDisplayProjectionIndex;
        this.model.selectedDisplayProjection = this._selectedDisplayProjection;
        this.model.checkCoordinatesInMaxExtent = this.checkCoordinatesInMaxExtent;

        if (_.isNil(this.renderer)) {
            this.renderer = new CoordinatesInputInPlace(div, this.model, options);
        }

        this.renderer.events.register('recentrage', this, this.gotoCoordinates);
        this.renderer.draw();
    },

    /**
     * Methode: gotoCoordinates
     * Recentre la carte sur les coordonnées portées par le modèle.
     */
    gotoCoordinates: function () {
        var center = [this.model.x, this.model.y];
        if (this.model.proj && this.model.proj !== this.model.mapProjection) {
            center = ol.proj.transform(center, this.model.proj, this.model.mapProjection);
        }
        this.olMap.getView().setCenter(center);
    },
    CLASS_NAME: 'Descartes.Action.CoordinatesInput'
});
module.exports = Class;
