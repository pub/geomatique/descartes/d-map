/* global MODE */

var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var Action = require('../Core/Action');
var ScaleSelectorInPlace = require('../UI/' + MODE + '/ScaleSelectorInPlace');

/**
 * Class: Descartes.Action.ScaleSelector
 * Classe permettant le recadrage de la carte suite au choix d'une échelle de visualisation dans une liste.
 *
 * :
 * Le modèle MVC manipulé est un objet JSON de la forme :
 * (start code)
 * {
 *     scale: <dénominateur d'échelle>
 * }
 * (end)
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'selected' de la classe définissant la vue MVC associée (<Descartes.UI.ScaleSelectorInPlace> par défaut) déclenche la méthode <gotoScale>.
 */
var Class = Utils.Class(Action, {
    /**
     * Propriete: availableScales
     * {Array(Integer)} Liste des dénominateurs d'échelle potentiellement disponibles pour le choix.
     *
     * Cette liste est en pratique limitée en cohérence avec les échelles minimale et maximale de la carte.
     */
    availableScales: [5000, 10000, 25000, 50000, 100000, 250000, 500000, 1000000],

    /**
     * Constructeur: Descartes.Action.ScaleSelector
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * olMap - {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propre à la classe:
     * availableScales - {Array(Integer)} Liste des dénominateurs d'échelle potentiellement disponibles pour le choix.
     *
     * :
     * Options propres à la vue associée. Voir <Descartes.UI.ScaleSelectorInPlace> pour la vue par défaut.
     */
    initialize: function (div, olMap, options) {
        if (!_.isNil(options)) {
            _.extend(this, options);
        }

        Action.prototype.initialize.apply(this, arguments);

        if (_.isNil(this.renderer)) {
            this.renderer = new ScaleSelectorInPlace(div, this.model, options);
        }

        var minResolution = this.olMap.getView().getMinResolution();
        var maxResolution = this.olMap.getView().getMaxResolution();

        var unit = this.olMap.getView().getProjection().getUnits();

        var enabledScales = [];
        _.each(this.availableScales, function (scale) {
            var resolution = Utils.getResolutionForScale(scale, unit);
            if (minResolution <= resolution && resolution <= maxResolution) {
                enabledScales.push(scale);
            }
        });

        this.renderer.events.register('selected', this, this.gotoScale);
        this.renderer.draw(enabledScales);
    },
    gotoScale: function () {
        var unit = this.olMap.getView().getProjection().getUnits();
        var resolution = Utils.getResolutionForScale(this.model.scale, unit);
        this.olMap.getView().setResolution(resolution);
    },

    CLASS_NAME: 'Descartes.Action.ScaleSelector'
});
module.exports = Class;
