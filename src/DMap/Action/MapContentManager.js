/* global MODE Descartes*/

var _ = require('lodash');

var I18N = require('../Core/I18N');
var Utils = require('../Utils/DescartesUtils');
var Group = require('../Model/Group');

var LayersTree = require('../UI/' + MODE + '/LayersTree');
var LayersTreeSimple = require('../UI/' + MODE + '/LayersTreeSimple');

var ContentManagerConstants = require('./MapContentManagerConstants');
var LayerConstants = require('../Model/LayerConstants');
var WMS = require('../Model/Layer/WMS');
var WFS = require('../Model/Layer/WFS');

var AddGroup = require('../Button/ContentTask/AddGroup');
var AlterGroup = require('../Button/ContentTask/AlterGroup');
var RemoveGroup = require('../Button/ContentTask/RemoveGroup');
var AddLayer = require('../Button/ContentTask/AddLayer');
var AlterLayer = require('../Button/ContentTask/AlterLayer');
var RemoveLayer = require('../Button/ContentTask/' + MODE + '/RemoveLayer');
var ChooseWmsLayers = require('../Button/ContentTask/' + MODE + '/ChooseWmsLayers');
var ExportVectorLayer = require('../Button/ContentTask/' + MODE + '/ExportVectorLayer');

/**
 * Class: Descartes.Action.MapContentManager
 * Classe définissant un gestionnaire pour le contenu (couches et groupes) d'une carte.
 *
 * :
 * Le gestionnaire met à disposition une vue <Descartes.UI.LayersTree> présentant le contenu de la carte sous forme d'arborescence.
 *
 * :
 * Il instancie de plus une série d'outils pour ajouter, modifier ou supprimer des éléments du contenu de la carte.
 *
 * :
 * Les outils disponibles (voir <CONTENT_TOOLS_NAME>) sont par défaut :
 * - <Descartes.Button.ContentTask.AddGroup> pour le type <Descartes.Action.MapContentManager.ADD_GROUP_TOOL>
 * - <Descartes.Button.ContentTask.AddLayer> pour le type <Descartes.Action.MapContentManager.ADD_LAYER_TOOL>
 * - <Descartes.Button.ContentTask.RemoveGroup> pour le type <Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL>
 * - <Descartes.Button.ContentTask.RemoveLayer> pour le type <Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL>
 * - <Descartes.Button.ContentTask.AlterGroup> pour le type <Descartes.Action.MapContentManager.ALTER_GROUP_TOOL>
 * - <Descartes.Button.ContentTask.AlterLayer> pour le type <Descartes.Action.MapContentManager.ALTER_LAYER_TOOL>
 * - <Descartes.Button.ContentTask.ChooseWmsLayers> pour le type <Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL>
 * - <Descartes.Button.ContentTask.ExportVectorLayer> pour le type <Descartes.Action.MapContentManager.EXPORT_VECTORLAYER_TOOL>
 *
 * :
 * Les outils inclus dans le gestionnaire sont actifs en fonction du contexte de sélection dans l'arborescence.
 *
 * Ecouteurs mis en place:
 *  - l'événement 'layerSelected' de la classe <Descartes.UI.LayersTree> déclenche la méthode <layerSelected>.
 *  - l'événement 'groupSelected' de la classe <Descartes.UI.LayersTree> déclenche la méthode <groupSelected>.
 *  - l'événement 'nodeUnselect' de la classe <Descartes.UI.LayersTree> déclenche la méthode <nodeUnselect>.
 *  - l'événement 'done' de la classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ADD_GROUP_TOOL> déclenche la méthode <createGroup>.
 *  - l'événement 'done' de la classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ADD_LAYER_TOOL> déclenche la méthode <createLayer>.
 *  - l'événement 'done' de la classe correspondant à l'outil de type <Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL> déclenche la méthode <removeGroup>.
 *  - l'événement 'done' de la classe correspondant à l'outil de type <Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL> déclenche la méthode <removeLayer>.
 *  - l'événement 'done' de la classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ALTER_GROUP_TOOL> déclenche la méthode <alterGroup>.
 *  - l'événement 'done' de la classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ALTER_LAYER_TOOL> déclenche la méthode <alterLayer>.
 *  - l'événement 'done' de la classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL> déclenche la méthode <addWmsLayers>.
 */
var MapContentManager = Utils.Class(I18N, {
    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,

    /**
     * Propriete: uiLayersTree
     * {<Descartes.UI.LayersTree>} Vue présentant le contenu de la carte sous forme d'arborescence.
     */
    uiLayersTree: null,

    /**
     * Propriete: tools
     * {Objet} Objet JSON stockant les outils inclus dans le gestionnaire. Les propriétés *possibles* sont :
     *
     * :
     * AddGroup - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ADD_GROUP_TOOL>
     * AddLayer - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ADD_LAYER_TOOL>
     * RemoveGroup - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL>
     * RemoveLayer - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL>
     * AlterGroup - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ALTER_GROUP_TOOL>
     * AlterLayer - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ALTER_LAYER_TOOL>
     * ChooseWmsLayers - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL>
     * ExportVectorLayer - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.EXPORT_VECTORLAYER_TOOL>
     */
    tools: {},

    /**
     * Propriete: contextMenuTools
     * {Objet} Objet JSON stockant les outils inclus dans le menu contextuel. Les propriétés *possibles* sont :
     *
     * :
     * AddGroup - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ADD_GROUP_TOOL>
     * AddLayer - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ADD_LAYER_TOOL>
     * RemoveGroup - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL>
     * RemoveLayer - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL>
     * AlterGroup - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ALTER_GROUP_TOOL>
     * AlterLayer - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ALTER_LAYER_TOOL>
     * ChooseWmsLayers - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL>
     * ExportVectorLayer - Classe correspondant à l'outil de type <Descartes.Action.MapContentManager.EXPORT_VECTORLAYER_TOOL>
     */
    contextMenuTools: {},

    contextMenuItems: {
            itemsRoot: {},
            itemsGroup: {},
            itemsLayer: {}
    },

    /**
     * Constructeur: Descartes.Action.MapContentManager
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'arborescence ou identifiant de cet élement.
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * uiOptions - {Object} Objet JSON stockant les options de la vue générant l'arborescence (voir <Descartes.UI.LayersTree.Descartes.UI.LayersTree>).
     */
    initialize: function (div, mapContent, options) {
        I18N.prototype.initialize.call(this, [arguments]);
        this.availableTools = {
            AddGroup: AddGroup,
            AddLayer: AddLayer,
            RemoveGroup: RemoveGroup,
            RemoveLayer: RemoveLayer,
            AlterGroup: AlterGroup,
            AlterLayer: AlterLayer,
            ChooseWmsLayers: ChooseWmsLayers,
            ExportVectorLayer: ExportVectorLayer
        };
        this.mapContent = mapContent;

        var contextMenuTools = null;
        var uiOptions = {};
        if (!_.isNil(options)) {
            _.extend(uiOptions, options.uiOptions);
            contextMenuTools = uiOptions.contextMenuTools;
            delete uiOptions.contextMenuTools;
            delete options.uiOptions;
        }
        _.extend(this, options);

        if (this.onlySimpleVue) {
            this.uiLayersTree = new LayersTreeSimple(Utils.getDiv(div), this.mapContent, uiOptions);
        } else {
            this.uiLayersTree = new LayersTree(Utils.getDiv(div), this.mapContent, uiOptions);

            if (contextMenuTools) {
                 this.uiLayersTree.setJstreeContextMenuItems(this.configContextMenuItems(contextMenuTools));
            }

            if (this.mapContent.editable) {
                this.uiLayersTree.events.register('layerSelected', this, this.layerSelected);
                this.uiLayersTree.events.register('groupSelected', this, this.groupSelected);
                this.uiLayersTree.events.register('nodeUnselect', this, this.nodeUnselect);
                this.uiLayersTree.events.register('rootSelected', this, this.rootSelected);
            }
        }

    },

    /**
     * Methode: configContextMenuItems
     * Configure les items présents dans le menu contextuel.
     */
    configContextMenuItems: function (contextMenuToolsType) {
        var self = this;
        if (!(contextMenuToolsType instanceof Array)) {
            contextMenuToolsType = [contextMenuToolsType];
        }
        for (var i = 0, len = contextMenuToolsType.length; i < len; i++) {
            this.addContextMenuTool(contextMenuToolsType[i]);
        }

        if (this.contextMenuTools.AddGroup) {
            var addGroupItemRoot = {
                label: this.getMessage('CONTEXT_MENU_LABEL_ADD_GROUP'),
				icon: 'DescartesButtonContentTaskAddGroupContextMenu',
				separator_before: false,
                separator_after: false,
                _disabled: false,
                action: function () {
                     self._activeContextMenuTool(ContentManagerConstants.ADD_GROUP_TOOL, self.createGroupContextMenu, {});
                }
            };
            this.contextMenuItems.itemsRoot.addGroupItem = addGroupItemRoot;

            var addGroupItem = {
                label: this.getMessage('CONTEXT_MENU_LABEL_ADD_SS_GROUP'),
                icon: "DescartesButtonContentTaskAddGroupContextMenu",
                separator_before: false,
                separator_after: false,
                _disabled: false,
                action: function () {
                    self._activeContextMenuTool(ContentManagerConstants.ADD_GROUP_TOOL, self.createGroupContextMenu, {});
                }
            };
            this.contextMenuItems.itemsGroup.addGroupItem = addGroupItem;
        }
        if (this.contextMenuTools.AlterGroup) {
            var updateGroupItem = {
                label: this.getMessage('CONTEXT_MENU_LABEL_UPDATE_GROUP'),
                icon: "DescartesButtonContentTaskAlterGroupContextMenu",
				separator_before: false,
                separator_after: false,
                _disabled: false,
                action: function () {
                    self._activeContextMenuTool(ContentManagerConstants.ALTER_GROUP_TOOL, self.alterGroupContextMenu, self.mapContent.getItemByIndex(self.uiLayersTree.selectedItem).toJSON());
                }
            };
            this.contextMenuItems.itemsGroup.updateGroupItem = updateGroupItem;
        }
        if (this.contextMenuTools.RemoveGroup) {
            var deleteGroupItem = {
            label: this.getMessage('CONTEXT_MENU_LABEL_REMOVE_GROUP'),
            icon: "DescartesButtonContentTaskRemoveGroupContextMenu",
			separator_before: false,
            separator_after: false,
            _disabled: false,
            action: function () {
                    self._activeContextMenuTool(ContentManagerConstants.REMOVE_GROUP_TOOL, self.removeGroup, self.mapContent.getItemByIndex(self.uiLayersTree.selectedItem).toJSON());
                }
            };
            this.contextMenuItems.itemsGroup.deleteGroupItem = deleteGroupItem;
        }
        if (this.contextMenuTools.AddLayer) {
            var addLayerItem = {
                label: this.getMessage('CONTEXT_MENU_LABEL_ADD_LAYER'),
				icon: "DescartesButtonContentTaskAddLayerContextMenu",
				separator_before: false,
                separator_after: false,
                _disabled: false,
                action: function () {
                    self._activeContextMenuTool(ContentManagerConstants.ADD_LAYER_TOOL, self.createLayerContextMenu, {});
                }
            };
            this.contextMenuItems.itemsRoot.addLayerItem = addLayerItem;
            this.contextMenuItems.itemsGroup.addLayerItem = addLayerItem;
        }
        if (this.contextMenuTools.ChooseWmsLayers) {
            var addLayerWMSItem = {
                label: this.getMessage('CONTEXT_MENU_LABEL_ADD_LAYER_WMS'),
				icon: "DescartesButtonContentTaskChooseWmsLayersContextMenu",
				separator_before: false,
                separator_after: false,
                _disabled: false,
                action: function () {
                    self._activeContextMenuTool(ContentManagerConstants.ADD_WMS_LAYERS_TOOL, self.addWmsLayersContextMenu, {});
                }
            };
            this.contextMenuItems.itemsRoot.addLayerWMSItem = addLayerWMSItem;
            this.contextMenuItems.itemsGroup.addLayerWMSItem = addLayerWMSItem;
        }
        if (this.contextMenuTools.AlterLayer) {
            var updateLayerItem = {
                label: this.getMessage('CONTEXT_MENU_LABEL_UPDATE_LAYER'),
                icon: "DescartesButtonContentTaskAlterLayerContextMenu",
				separator_before: false,
                separator_after: false,
                _disabled: false,
                action: function () {
                    self._activeContextMenuTool(ContentManagerConstants.ALTER_LAYER_TOOL, self.alterLayerContextMenu, self.mapContent.getItemByIndex(self.uiLayersTree.selectedItem).toJSON());
                }
            };
            this.contextMenuItems.itemsLayer.updateLayerItem = updateLayerItem;
        }
        if (this.contextMenuTools.RemoveLayer) {
            var deleteLayerItem = {
                label: this.getMessage('CONTEXT_MENU_LABEL_REMOVE_LAYER'),
                icon: "DescartesButtonContentTaskRemoveLayerContextMenu",
				separator_before: false,
                separator_after: false,
                _disabled: false,
                action: function () {
                    self._activeContextMenuTool(ContentManagerConstants.REMOVE_LAYER_TOOL, self.removeLayer, self.mapContent.getItemByIndex(self.uiLayersTree.selectedItem).toJSON());
                }
            };
            this.contextMenuItems.itemsLayer.deleteLayerItem = deleteLayerItem;
        }
        if (this.contextMenuTools.ExportVectorLayer) {
            var exportVectorLayerItem = {
                label: this.getMessage('CONTEXT_MENU_LABEL_EXPORT_VECTORLAYER'),
				icon: "DescartesButtonContentTaskExportVectorLayerContextMenu",
				separator_before: false,
                separator_after: false,
                _disabled: false,
                action: function () {
                    self._activeContextMenuTool(ContentManagerConstants.EXPORT_VECTORLAYER_TOOL, null, self.mapContent.getItemByIndex(self.uiLayersTree.selectedItem));
                }
            };
            this.contextMenuItems.itemsLayer.exportVectorLayerItem = exportVectorLayerItem;
        }

        return this.contextMenuItems;
    },

    /**
     * Methode: addTool
     * Ajoute un outil au gestionnaire.
     *
     * Paramètres:
     * toolDef - {Object} Objet JSON décrivant l'outil.
     *
     * :
     * L'objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de l'outil>,            // parmi les valeurs de Descartes.Action.MapContentManager.CONTENT_TOOLS_NAME
     * 		className: <classe personnalisée>,  // classe personnalisée remplaçant la classe par défaut associée au type de l'outil
     * 		options: <options>                  // options propres à l'outil
     * }
     * (end)
     */
    addTool: function (toolDef) {
        if (ContentManagerConstants.CONTENT_TOOLS_NAME.indexOf(toolDef.type) !== -1) {
            var ClassName;
            if (!_.isNil(toolDef.className)) {
                ClassName = toolDef.className;
            } else {
                ClassName = this.availableTools[toolDef.type];
            }
            var tool = new ClassName(toolDef.options);
            this.tools[toolDef.type] = tool;
        }
    },

    /**
     * Methode: addContextMenuTool
     * Ajoute un outil au menu contextuel.
     *
     * Paramètres:
     * toolDef - {Object} Objet JSON décrivant l'outil.
     *
     * :
     * L'objet JSON doit être de la forme :
     * (start code)
     * {
     * 		type: <type de l'outil>,            // parmi les valeurs de Descartes.Action.MapContentManager.CONTENT_TOOLS_NAME
     * 		className: <classe personnalisée>,  // classe personnalisée remplaçant la classe par défaut associée au type de l'outil
     * 		options: <options>                  // options propres à l'outil
     * }
     * (end)
     */
    addContextMenuTool: function (toolDef) {
        if (ContentManagerConstants.CONTENT_TOOLS_NAME.indexOf(toolDef.type) !== -1) {
            var ClassName;
            if (!_.isNil(toolDef.className)) {
                ClassName = toolDef.className;
            } else {
                ClassName = this.availableTools[toolDef.type];
            }
            this.contextMenuTools[toolDef.type] = new ClassName(toolDef.options);
        }
    },

    /**
     * Methode: getTools
     * Fournit les outils inclus dans le gestionnaire.
     *
     * Retour:
     * {Array(<Descartes.Button.ContentTask>)} Liste des outils.
     */
    getTools: function () {
        var activeTools = [];
        for (var tool in this.tools) {
            if (!_.isNil(this.tools[tool])) {
                activeTools.push(this.tools[tool]);
            }
        }
        return activeTools;
    },

    /**
     * Methode: activeInitialTools
     * Active les outils initialement disponibles, c'est-à-dire ceux ne dépendant pas de la sélection d'un élément dans l'arborescence.
     */
    activeInitialTools: function () {
        this._activeTool(ContentManagerConstants.ADD_GROUP_TOOL, this.createGroup, {});
        this._activeTool(ContentManagerConstants.ADD_LAYER_TOOL, this.createLayer, {});
        this._activeTool(ContentManagerConstants.ADD_WMS_LAYERS_TOOL, this.addWmsLayers, {});
        if (!_.isEmpty(this.uiLayersTree.selectedItem)) {
            var item = this.mapContent.getItemByIndex(this.uiLayersTree.selectedItem);
            if (item instanceof Group) {
                this.groupSelected();
            } else {
                this.layerSelected();
            }
        }
    },

    /**
     * Methode: layerSelected
     * Active les outils correspondant au contexte où une couche est sélectionnée dans l'arborescence.
     */
    layerSelected: function () {
        this._desactiveTools();
        var layer = this.mapContent.getItemByIndex(this.uiLayersTree.selectedItem);
        if (this.mapContent.editInitialItems || layer.addedByUser) {
            this._activeTool(ContentManagerConstants.REMOVE_LAYER_TOOL, this.removeLayer, layer.toJSON());
            this._activeTool(ContentManagerConstants.ALTER_LAYER_TOOL, this.alterLayer, layer.toJSON());
            if (layer instanceof Descartes.Layer.Vector) {
                this._activeTool(ContentManagerConstants.EXPORT_VECTORLAYER_TOOL, null, layer);
            }
        }
    },

    /**
     * Methode: groupSelected
     * Active les outils correspondant au contexte où un groupe est sélectionné dans l'arborescence.
     */
    groupSelected: function () {
        this._desactiveTools();
        var group = this.mapContent.getItemByIndex(this.uiLayersTree.selectedItem);
        this._activeTool(ContentManagerConstants.ADD_GROUP_TOOL, this.createGroup, {});
        this._activeTool(ContentManagerConstants.ADD_LAYER_TOOL, this.createLayer, {});
        this._activeTool(ContentManagerConstants.ADD_WMS_LAYERS_TOOL, this.addWmsLayers, {});
        if (this.mapContent.editInitialItems || group.addedByUser) {
            this._activeTool(ContentManagerConstants.REMOVE_GROUP_TOOL, this.removeGroup, group.toJSON());
            this._activeTool(ContentManagerConstants.ALTER_GROUP_TOOL, this.alterGroup, group.toJSON());
        }
    },

    /**
     * Methode: rootSelected
     * Active les outils correspondant au contexte où la racine est sélectionné dans l'arborescence.
     */
    rootSelected: function () {
        this._desactiveTools();
        this._activeTool(ContentManagerConstants.ADD_GROUP_TOOL, this.createGroup, {});
        this._activeTool(ContentManagerConstants.ADD_LAYER_TOOL, this.createLayer, {});
        this._activeTool(ContentManagerConstants.ADD_WMS_LAYERS_TOOL, this.addWmsLayers, {});
    },

    /**
     * Methode: nodeUnselect
     * Active les outils correspondant au contexte où aucun élément n'est sélectionné dans l'arborescence.
     */
    nodeUnselect: function () {
        this._desactiveTools();
        this.activeInitialTools();
    },

    /**
     * Private
     */
    _desactiveTools: function () {
        for (var i = 0, len = ContentManagerConstants.CONTENT_TOOLS_NAME.length; i < len; i++) {
            var tool = this.tools[ContentManagerConstants.CONTENT_TOOLS_NAME[i]];
            if (tool) {
                tool.disable();
                tool.events.remove('done');
            }
        }
    },

    /**
     * Private
     */
    _activeTool: function (toolName, callback, datas) {
        var tool = this.tools[toolName];
        if (tool) {
            tool.enable();
            tool.events.register('done', this, callback);
            if (datas !== undefined) {
                tool.setDatas(datas);
            }
        }
    },

    /**
     * Private
     */
    _activeContextMenuTool: function (toolName, callback, datas) {
        var tool = this.contextMenuTools[toolName];
        if (tool) {
            tool.events.remove('done');
            tool.enable();
            tool.events.register('done', this, callback);
            if (datas !== undefined) {
                tool.setDatas(datas);
            }
            tool.execute();
            tool.disable();
        }
    },

    /**
     * Methode: createGroup
     * Ajoute un groupe au contenu de la carte et actualise l'arborescence.
     */
    createGroup: function () {
        var title = this.tools.AddGroup.datas.title;
        var opened = this.tools.AddGroup.datas.opened;
        var parentGroup = this._getMapContentItem(this.uiLayersTree.selectedItem);

        var childsParentGroup = parentGroup.items;
        if (childsParentGroup.length > 0) {
            this.mapContent.insertItemBefore(
                    new Group(title, {
                        opened: opened,
                        addedByUser: true
                    }),
                    this.mapContent.getItemByIndex(childsParentGroup[0].index));
        } else {
            this.mapContent.addItem(new Group(title, {
                opened: opened,
                addedByUser: true
            }), parentGroup);
        }

        this.tools.AddGroup.datas = {};
        this.redrawUI();
    },

    /**
     * Methode: createGroupContextMenu
     * Ajoute un groupe au contenu de la carte et actualise l'arborescence.
     */
    createGroupContextMenu: function () {
        var title = this.contextMenuTools.AddGroup.datas.title;
        var opened = this.contextMenuTools.AddGroup.datas.opened;
        var parentGroup = this._getMapContentItem(this.uiLayersTree.selectedItem);

        var childsParentGroup = parentGroup.items;
        if (childsParentGroup.length > 0) {
            this.mapContent.insertItemBefore(
                    new Group(title, {
                        opened: opened,
                        addedByUser: true
                    }),
                    this.mapContent.getItemByIndex(childsParentGroup[0].index));
        } else {
            this.mapContent.addItem(new Group(title, {
                opened: opened,
                addedByUser: true
            }), parentGroup);
        }

        this.contextMenuTools.AddGroup.datas = {};
        this.redrawUI();
    },

    /**
     * Methode: createLayer
     * Ajoute une couche au contenu de la carte et actualise l'arborescence.
     */
    createLayer: function () {
        this._addLayer(this.tools.AddLayer.datas);
        this.tools.AddLayer.datas = {};
        this.redrawUI();
    },

    /**
     * Methode: createLayerContextMenu
     * Ajoute une couche au contenu de la carte et actualise l'arborescence.
     */
    createLayerContextMenu: function () {
        this._addLayer(this.contextMenuTools.AddLayer.datas);
        this.contextMenuTools.AddLayer.datas = {};
        this.redrawUI();
    },

    /**
     * Methode: removeGroup
     * Supprime un groupe du contenu de la carte et actualise l'arborescence.
     */
    removeGroup: function () {
        this.mapContent.removeItemByIndex(this.uiLayersTree.selectedItem);
        this.redrawUI();
        this.uiLayersTree.selectedItem = '';
        this.uiLayersTree.events.triggerEvent('nodeUnselect');
    },

    /**
     * Methode: removeLayer
     * Supprime une couche du contenu de la carte et actualise l'arborescence.
     */
    removeLayer: function () {
        this.mapContent.removeItemByIndex(this.uiLayersTree.selectedItem);
        this.redrawUI();
        this.uiLayersTree.selectedItem = '';
        this.uiLayersTree.events.triggerEvent('nodeUnselect');
    },

    /**
     * Methode: alterGroup
     * Modifie un groupe du contenu de la carte et actualise l'arborescence.
     */
    alterGroup: function () {
        this.tools.RemoveGroup.setDatas(this.tools.AlterGroup.datas);
        var group = this.mapContent.getItemByIndex(this.uiLayersTree.selectedItem);
        group.setFromJSON(this.tools.AlterGroup.datas);
        this.mapContent.refresh(group);
        this.redrawUI();
    },

    /**
     * Methode: alterGroupContextMenu
     * Modifie un groupe du contenu de la carte et actualise l'arborescence.
     */
    alterGroupContextMenu: function () {
        this.contextMenuTools.RemoveGroup.setDatas(this.contextMenuTools.AlterGroup.datas);
        var group = this.mapContent.getItemByIndex(this.uiLayersTree.selectedItem);
        group.setFromJSON(this.contextMenuTools.AlterGroup.datas);
        this.mapContent.refresh(group);
        this.redrawUI();
    },

    /**
     * Methode: alterLayer
     * Modifie une couche du contenu de la carte et actualise l'arborescence.
     */
    alterLayer: function () {
        this.tools.RemoveLayer.setDatas(this.tools.AlterLayer.datas);
        var layer = this.mapContent.getItemByIndex(this.uiLayersTree.selectedItem);
        layer.setFromJSON(this.tools.AlterLayer.datas);
        this.mapContent.refresh(layer);
        this.redrawUI();
    },

    /**
     * Methode: alterLayerContextMenu
     * Modifie une couche du contenu de la carte et actualise l'arborescence.
     */
    alterLayerContextMenu: function () {
        this.contextMenuTools.RemoveLayer.setDatas(this.contextMenuTools.AlterLayer.datas);
        var layer = this.mapContent.getItemByIndex(this.uiLayersTree.selectedItem);
        layer.setFromJSON(this.contextMenuTools.AlterLayer.datas);
        this.mapContent.refresh(layer);
        this.redrawUI();
    },

    /**
     * Methode: addWmsLayers
     * Ajoute un ensemble de couches WMS au contenu de la carte et actualise l'arborescence.
     */
    addWmsLayers: function () {
        for (var i = 0, len = this.tools.ChooseWmsLayers.datas.length; i < len; i++) {
            this._addLayer(this.tools.ChooseWmsLayers.datas[i]);
        }
        this.tools.ChooseWmsLayers.datas = {};
        this.redrawUI();
    },

    /**
     * Methode: addWmsLayersContextMenu
     * Ajoute un ensemble de couches WMS au contenu de la carte et actualise l'arborescence.
     */
    addWmsLayersContextMenu: function () {
        for (var i = 0, len = this.contextMenuTools.ChooseWmsLayers.datas.length; i < len; i++) {
            this._addLayer(this.contextMenuTools.ChooseWmsLayers.datas[i]);
        }
        this.contextMenuTools.ChooseWmsLayers.datas = {};
        this.redrawUI();
    },

    /**
     * Methode: redrawUI
     * Actualise l'arborescence.
     */
    redrawUI: function () {
        if (this.uiLayersTree.registerAllLayerLoading) {
            this.uiLayersTree.registerAllLayerLoading();
        }
        this.uiLayersTree.draw();
        if (this.uiLayersTreeSimple) {
            this.uiLayersTreeSimple.draw();
        }
    },

    populate: function (content) {
        this.mapContent.populate(content);
        this.redrawUI();
    },

    /**
     * Private
     */
    _addLayer: function (datas) {
        var options = datas.options;
        options.addedByUser = true;

        var childs;
        if (datas.type === LayerConstants.TYPE_WMS) {
            if (!_.isEmpty(this.uiLayersTree.selectedItem)) {
                childs = this.mapContent.getItemByIndex(this.uiLayersTree.selectedItem).items;
                if (childs && childs.length > 0) {
                    this.mapContent.insertItemBefore(new WMS(datas.title, datas.layersDefinition, options), this.mapContent.getItemByIndex(childs[0].index));
                } else {
                    this.mapContent.addItem(new WMS(datas.title, datas.layersDefinition, options), this.mapContent.getItemByIndex(this.uiLayersTree.selectedItem));
                }
            } else if (this.mapContent.getItemByIndex('0') !== undefined && this.mapContent.getItemByIndex('0') !== null) {
                this.mapContent.insertItemBefore(new WMS(datas.title, datas.layersDefinition, options), this.mapContent.getItemByIndex('0'));
            } else {
                this.mapContent.addItem(new WMS(datas.title, datas.layersDefinition, options));
            }
        } else if (datas.type === LayerConstants.TYPE_WFS) {
            if (!_.isEmty(this.uiLayersTree.selectedItem)) {
                childs = this.mapContent.getItemByIndex(this.uiLayersTree.selectedItem).items;
                if (childs && childs.length > 0) {
                    this.mapContent.insertItemBefore(new WFS(datas.title, datas.layersDefinition, options), this.mapContent.getItemByIndex(childs[0].index));
                } else {
                    this.mapContent.addItem(new WFS(datas.title, datas.layersDefinition, options), this.mapContent.getItemByIndex(this.uiLayersTree.selectedItem));
                }
            } else if (!_.isNil(this.mapContent.getItemByIndex('0'))) {
                this.mapContent.insertItemBefore(new WFS(datas.title, datas.layersDefinition, options), this.mapContent.getItemByIndex('0'));
            } else {
                this.mapContent.addItem(new WFS(datas.title, datas.layersDefinition, options));
            }
        }
    },

    /**
     * Private
     */
    _getMapContentItem: function (selectedIndex) {
        var item = this.mapContent.item;
        if (!_.isEmpty(selectedIndex)) {
            item = this.mapContent.getItemByIndex(selectedIndex);
        }
        return item;
    },

    addLayersTreeSimple: function (div, options) {

        this.uiLayersTreeSimple = new LayersTreeSimple(Utils.getDiv(div), this.mapContent, options);
        this.uiLayersTreeSimple.events.register('treeSimpleUserEvent', this, this.treeSimpleUserEvent);

        this.uiLayersTree.events.register('treeUserEvent', this, this.treeUserEvent);

    },

    treeUserEvent: function () {
        console.log("TREE EVT");
        if (this.uiLayersTreeSimple) {
            this.uiLayersTreeSimple.draw();
            /*var self = this;
            setTimeout(function () {
                self.uiLayersTreeSimple.reOpenItems();
            }, 500);*/
        }
    },

    treeSimpleUserEvent: function () {
        console.log("TREE SIMPLE EVT");
        if (this.uiLayersTree) {
            this.uiLayersTree.draw();
        }
    },

    CLASS_NAME: 'Descartes.Action.MapContentManager'
});

module.exports = MapContentManager;
