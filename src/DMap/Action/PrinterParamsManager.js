/* global MODE, Descartes */

var _ = require('lodash');
var log = require('loglevel');

var Utils = require('../Utils/DescartesUtils');
var EventManager = require('../Utils/Events/EventManager');
var Action = require('../Core/Action');
var MessagesConstants = require('../Messages');

var PrinterSetupInPlace = require('../UI/' + MODE + '/PrinterSetupInPlace');
var PrinterSetupDialog = require('../UI/' + MODE + '/PrinterSetupDialog');
var ExternalCallsUtils = require('../Core/ExternalCallsUtils');

/**
 * Class: Descartes.Action.PrinterParamsManager
 * Classe permettant d'exporter la carte courante sous forme de fichier PDF, après saisie des paramètres de mise en page
 *
 * Le modèle MVC manipulé est un objet JSON de la forme :
 * (start code)
 * {
 * 		mapWidth: <largeur carte>,
 * 		mapHeigth: <hauteur carte>,
 * 		marginTop: <marge haut>,
 * 		marginBottom: <marge bas>,
 * 		marginLeft: <marge gauche>,
 * 		marginRight: <marge droit>,
 * 		paper: <code format composite>,
 * 		enabledFormats: {
 * 							code: <code format>,
 * 							width: <largeur papier>,
 * 							height: <hauteur papier>
 * 						}[]
 * }
 * (end)
 * Les formats disponibles sont déterminés en fonction de la taille de la carte.
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Evénements déclenches:
 * paramsChanged - Les paramètres sont saisis et valides.
 *
 * Ecouteurs mis en place:
 *  - l'événement 'choosed' de la classe définissant la vue MVC associée (<Descartes.UI.PrinterSetupInPlace> par défaut) déclenche la méthode <doExport>.
 *  - l'événement 'resize' de la classe ol.Map déclenche la méthode <redrawRendererWithParams>.
 */
var Class = Utils.Class(Action, {
    /**
     * Propriete: defaultParams
     * {Object} Objet JSON stockant les paramètres de mise en page par défaut.
     *
     * :
     * titleHeight - Hauteur en mm de la zone réservée pour le titre (20 par défaut).
     * marginTop - Dimension en mm de la marge haut (10 par défaut).
     * marginBottom - Dimension en mm de la marge bas (10 par défaut).
     * marginLeft - Dimension en mm de la marge gauche (10 par défaut).
     * marginRight - Dimension en mm de la marge droit (10 par défaut).
     */
    defaultParams: {
        titleHeight: 20,
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10
    },
    /**
     * Propriete: defaultInfos
     * {Object} Objet JSON stockant les informations complémentaires par défaut à insérer dans le fichier PDF.
     *
     * :
     * title - {String} Titre de la carte (utilisé pour le nom du fichier généré).
     * auteur - {String} Organisme auteur de la carte.
     * copyright - Texte du copyright à insérer en sur-impression de l'image.
     * description - {String} Description de la carte.
     * production - {String} Mentions "légales" de l'application productrice.
     * createur - {String} Nom de l'application productrice.
     * logoFiles - {Array(String)} Ressources fichiers des logos à insérer.
     * logoURLs - {Array(String)} Ressources HTTP des logos à insérer.
     */
    defaultInfos: {
        title: 'descartes',
        auteur: '',
        copyright: '',
        description: '',
        production: '',
        createur: '',
        logoFiles: [],
        logoURLs: []
    },
    /**
     * Propriete: infos
     * {Object} Objet JSON stockant les informations complémentaires à insérer dans le fichier PDF.
     *
     * Construit à partir de defaultInfos, puis surchargé en présence d'options complémentaires.
     */
    infos: null,

    /**
     * Propriete: mapContent
     * {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    mapContent: null,

    EVENT_TYPES: ['paramsChanged'],
    /**
     * Constructeur: Descartes.Action.PrinterParamsManager
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * olMap - {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     *
     * Options de construction propres à la classe:
     * params -  {Object} Objet JSON stockant les paramètres de mise en page initiaux (étendus par <defaultParams>), structuré comme le modèle associé.
     * infos - {Object} Objet JSON stockant les informations complémentaires à insérer dans le fichier PDF.
     */
    initialize: function (div, olMap, options) {
        this.model = {};
        if (_.isNil(options)) {
            options = {};
        }
        Action.prototype.initialize.apply(this, arguments);
        if (!_.isNil(options) && !_.isNil(options.params)) {
            this.model = options.params;
            delete options.params;
        }

        this._configureParams(olMap.getSize()[0], olMap.getSize()[1]);

        this.infos = _.extend({}, this.defaultInfos);
        if (!_.isNil(options) && !_.isNil(options.infos)) {
            _.extend(this.infos, options.infos);
            delete options.infos;
        }

        if (_.isNil(this.renderer)) {
            if (options && options.dialog) {
                this.renderer = new PrinterSetupDialog(div, this.model, options);
            } else {
                this.renderer = new PrinterSetupInPlace(div, this.model, options);
            }
        }
        this.events = new EventManager();

        if (this.model.enabledFormats.length !== 0) {
            this.olMap.on('change:size', this.redrawRendererWithParams.bind(this), this);
            this.renderer.events.register('choosed', this, this.doExport);
            this.renderer.draw();
        } else {
            alert(MessagesConstants.Descartes_EXPORT_ERROR_FORMAT_PAPER);
        }
    },
    /**
     * Methode: setMapContent
     * Associe le contenu de la carte au gestionnaire.
     *
     * Paramètres:
     * mapContent - {<Descartes.MapContent>} Contenu de la carte (groupes et couches).
     */
    setMapContent: function (mapContent) {
        this.mapContent = mapContent;
    },

    /**
     * Methode: redrawRendererWithParams
     * Reconstruit la vue associée en cas de changement de taille de la carte.
     */
    redrawRendererWithParams: function () {
        if (this._configureParams(this.olMap.getSize()[0], this.olMap.getSize()[1])) {
            this.renderer.redraw(this.model);
        }
    },

    /**
     * Private
     */
    _configureParams: function (mapWidth, mapHeight) {
        if (this.model === null || this.model.mapWidth !== mapWidth || this.model.mapHeight !== mapHeight) {
            this.model = _.extend({}, this.defaultParams);
            this.model.mapWidth = mapWidth;
            this.model.mapHeight = mapHeight;
            this.model.enabledFormats = [];
            for (var i = 0, len = Descartes.Descartes_Papers.length; i < len; i++) {
                var format = Descartes.Descartes_Papers[i];
                this._checkPaperSizes(format.name, format.width, format.length, 'P');
                this._checkPaperSizes(format.name, format.length, format.width, 'L');
            }
            return true;
        }
        return false;
    },

    /**
     * Private
     */
    _checkPaperSizes: function (paperName, width, height, orientation) {
        var lMap = parseFloat(Math.round(this.model.mapWidth * 25.4 / 96));
        var hMap = parseFloat(Math.round(this.model.mapHeight * 25.4 / 96));
        if ((lMap < width) && ((hMap + this.model.titleHeight) < height)) {
            if ((lMap + this.model.marginLeft + this.model.marginRight) > width) {
                var newMarginWidth = Math.round((width - lMap) / 2);
                this.model.marginLeft = newMarginWidth;
                this.model.marginRight = newMarginWidth;
            }
            if ((hMap + this.model.marginTop + this.model.marginBottom) > height) {
                var newMarginHeight = Math.round((height - hMap) / 2);
                this.model.marginTop = newMarginHeight;
                this.model.marginBottom = newMarginHeight;
            }
            this.model.enabledFormats.push({code: paperName + orientation, width: width, height: height});
            if (this.model.paper === undefined) {
                this.model.paper = paperName + orientation;
            }
        }
    },

    /**
     * Methode: doExport
     * Lance la génération du PDF grâce au service d'exportation PDF, et déclenche l'événement 'paramsChanged'.
     */
    doExport: function () {
        var size = this.model.paper.substring(0, this.model.paper.length - 1);
        var orientation = this.model.paper.substring(this.model.paper.length - 1);

        this.events.triggerEvent('paramsChanged');

        if (Utils.acceptCookies() && this.mapContent !== null) {
            var printerSetupParams = '<PrinterSetup>' +
                    '<Format>' + size + '</Format>' +
                    '<Orientation>' + orientation + '</Orientation>' +
                    '<Margins>' +
                    '<Units>mm</Units>' +
                    '<Top>' + this.model.marginTop + '</Top>' +
                    '<Bottom>' + this.model.marginBottom + '</Bottom>' +
                    '<Left>' + this.model.marginLeft + '</Left>' +
                    '<Right>' + this.model.marginRight + '</Right>' +
                    '</Margins>' +
                    '</PrinterSetup>';

            var layers = this.mapContent.getVisibleLayers().sort(function (a, b) {
                return b.displayOrder - a.displayOrder;
            });
            var exportParams = ExternalCallsUtils.writeExportPostBody(layers, this.olMap, this.infos);
            log.debug('exportParams=', exportParams);

            if (exportParams !== null) {
                ExternalCallsUtils.openExportPopup(Descartes.EXPORT_PDF_SERVER, exportParams, printerSetupParams);
            } else {
                alert(MessagesConstants.Descartes_EXPORT_ERROR);
            }
        }
    },

    CLASS_NAME: 'Descartes.Action.PrinterParamsManager'
});
module.exports = Class;
