var _ = require('lodash');

var tools = {
    /**
     * Constante: ADD_GROUP_TOOL
     * Code de l'outil pour l'ajout d'un groupe.
     */
    ADD_GROUP_TOOL: 'AddGroup',
    /**
     * Constante: ADD_LAYER_TOOL
     * Code de l'outil pour l'ajout d'une couche.
     */
    ADD_LAYER_TOOL: 'AddLayer',
    /**
     * Constante: REMOVE_GROUP_TOOL
     * Code de l'outil pour la suppression d'un groupe.
     */
    REMOVE_GROUP_TOOL: 'RemoveGroup',
    /**
     * Constante: REMOVE_LAYER_TOOL
     * Code de l'outil pour la suppression d'une couche.
     */
    REMOVE_LAYER_TOOL: 'RemoveLayer',
    /**
     * Constante: ALTER_GROUP_TOOL
     * Code de l'outil pour la modification d'un groupe.
     */
    ALTER_GROUP_TOOL: 'AlterGroup',
    /**
     * Constante: ALTER_LAYER_TOOL
     * Code de l'outil pour la modification d'une couche.
     */
    ALTER_LAYER_TOOL: 'AlterLayer',
    /**
     * Constante: ADD_WMS_LAYERS_TOOL
     * Code de l'outil pour l'ajout de couches après interrogation d'un serveur WMS.
     */
    ADD_WMS_LAYERS_TOOL: 'ChooseWmsLayers',
    /**
     * Constante: EXPORT_VECTORLAYER_TOOL
     * Code de l'outil pour l'export d'une couche vector.
     */
    EXPORT_VECTORLAYER_TOOL: 'ExportVectorLayer',
    /**
     * Constante: CONTENTMANAGER_TOOLBAR_NAME
     * {String} Nom de la barre d'outils.
     */
    CONTENTMANAGER_TOOLBAR_NAME: 'DescartesContentManagerToolBar'
};

var toolsName = _.values(tools);

tools = _.extend(tools, {
    CONTENT_TOOLS_NAME: toolsName
});

module.exports = tools;

