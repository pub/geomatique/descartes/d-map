/* global MODE, Descartes */

var ol = require('openlayers');
var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');

var Action = require('../Core/Action');
var AjaxSelection = require('./AjaxSelection');

var UIConstants = require('../UI/UIConstants');

var ExternalCallsUtils = require('../Core/ExternalCallsUtils');
var ExternalCallsUtilsConstants = require('../Core/ExternalCallsUtilsConstants');

var RequestManagerConstants = require('./RequestManagerConstants');
var RequestManagerInPlace = require('../UI/' + MODE + '/RequestManagerInPlace');

var ResultLayer = require('../Model/ResultLayer');
var ResultLayerConstants = require('../Model/ResultLayerConstants');

/**
 * Class: Descartes.Action.RequestManager
 * Classe permettant l'exécution de requêtes attributaires suite à la saisie de valeurs pour les critères de sélection.
 *
 * :
 * Les résultats d'une requête sont affichés :
 * - dans une vue présentant les données attributaires sous forme de tableau
 * - dans une couche graphique de sélection ajoutée à la carte
 *
 * :
 * Le modèle MVC manipulé est un objet JSON de la forme :
 * (start code)
 * {
 * 		numRequest: <index de la requête à exécuter>,
 * 		values: [<valeur critere 1>, <valeur critere 2>, ..., <valeur critere N>],
 *      useCurrentExtent: <Boolean>
 *      useGazetteerExtent: <Object>
 * }
 * (end)
 *
 * Hérite de:
 * - <Descartes.Action>
 *
 * Ecouteurs mis en place:
 *  - l'événement 'sendRequest' de la classe définissant la vue MVC associée (<Descartes.UI.RequestManagerInPlace> par défaut) déclenche la méthode <execute>.
 *  - l'événement 'unselect' de la classe définissant la vue MVC associée (<Descartes.UI.RequestManagerInPlace> par défaut) déclenche la méthode <unselect>.
 *  - l'événement 'moveend' de la classe ol.Map déclenche la méthode <refreshSelectionLayer>.
 */
var Class = Utils.Class(Action, {
    /**
     * Propriete: requests
     * {Array(<Descartes.Request>)} Liste des requêtes disponibles dans le gestionnaire.
     */
    requests: [],

    /**
     * Propriete: activeRequest
     * {<Descartes.Request>} Requête active mémorisée pour les rafraichissements.
     */
    activeRequest: null,

    /**
     * Propriete: alwaysExportSelectionLayers
     * {Boolean} Indique si la couche de sélection doit être systématiquement incluse dans les exportations PNG et PDF.
     */
    alwaysExportSelectionLayers: false,

    /**
     * Propriete: withChooseExtent
     * {Boolean} Indique si l'utilsateur peut choisir l'étendu de recherche.
     */
    withChooseExtent: false,
    /**
     * Propriete: defaultResultUiParams
     * {Object} Objet JSON stockant les propriétés par défaut nécessaires à la génération de l'affichage des résultats de la sélection.
     *
     * :
     * type - <Descartes.UI.TYPE_WIDGET>
     * withReturn - false
     * withCsvExport - false
     * withResultLayerExport - false
     * withUIExports - false
     * withAvancedView - false
     * withFilterColumns - false
     * withListResultLayer - false
     * withPagination - false
     * withTabs - false
     */
    defaultResultUiParams: {
        type: UIConstants.TYPE_WIDGET,
        withReturn: false,
        withCsvExport: false,
        withResultLayerExport: false,
        withUIExports: false,
        withAvancedView: false,
        withFilterColumns: false,
        withListResultLayer: false,
        withPagination: false,
        withTabs: false
    },
    /**
     * Propriete: resultUiParams
     * {Object} Objet JSON stockant les propriétés nécessaires à la génération de l'affichage des résultats de la sélection.
     *
     * :
     * Construit à partir de defaultResultUiParams, puis surchargé en présence d'options complémentaires.
     *
     * :
     * type - <Descartes.UI.TYPE_WIDGET> | <Descartes.UI.TYPE_INPLACEDIV> | <Descartes.UI.TYPE_POPUP> : Type de vue pour l'affichage des résultats.
     * view - {<Descartes.UI>} : Classe de la vue si le type est <Descartes.UI.TYPE_WIDGET>
     * div - {DOMElement|String} : Elément DOM de la page accueillant le résultat de la sélection
     * si le type est <Descartes.UI.TYPE_WIDGET> ou <Descartes.UI.TYPE_INPLACEDIV>.
     * format - "JSON"|"HTML"|"XML" : format de retour des informations souhaité.
     * withReturn - {Boolean} : Indique si le résultat doit proposer une localisation rapide sur les objets retournés.
     * withCsvExport - {Boolean} : Indique si le résultat doit proposer une exportation CSV (côté serveur) pour les objets retournés.
     * withResultLayerExport - {Boolean} : Indique si le résultat doit proposer une exportation des objets retournés avec les géomatries.
     * withUIExports - {Boolean} : Indique si le résultat doit proposer les exports (côté client) pour les objets retournés.
     * withAvancedView - {Boolean} : Indique si le résultat doit être affiché dans une vue (UI) avancée.
     * withFilterColumns - {Boolean} : Indicateur pour la possibilité de filtrer les colonnes.
     * withListResultLayer - {Boolean} : Indicateur pour la possibilité d'affichage de la liste des couches'.
     * withPagination - {Boolean} : Indicateur pour la possibilité d'afficher la pagination'.
     * withTabs - {Boolean} : Indicateur pour la possibilité d'afficher les onglets'.
     */
    resultUiParams: null,

    /**
     * Private
     */
    maxExtentAsGmlPolygon: null,
    /**
     * Constructeur: Descartes.Action.RequestManager
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant les zones de saisie des critères de sélection ou identifiant de cet élement.
     * olMap - {ol.Map} Carte OpenLayers concernée.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance.
     *
     * Options de construction propres à la classe:
     * alwaysExportSelectionLayers - {Boolean} Indique si la couche de sélection doit être systématiquement incluse dans les exportations PNG et PDF.
     * resultUiParams - {Object} Objet JSON stockant les propriétés nécessaires à la génération de l'affichage des résultats de la sélection.
     */
    initialize: function (div, olMap, options) {
        this.model = {};
        this.requests = [];

        this.resultUiParams = _.extend({}, this.defaultResultUiParams);

        if (!_.isNil(options)) {
            _.extend(this.resultUiParams, options.resultUiParams);
            delete options.resultUiParams;
            this.alwaysExportSelectionLayers = options.alwaysExportSelectionLayers || false;

            this.waitingMsg = options.waitingMsg;
            delete options.waitingMsg;

            this.withChooseExtent = options.withChooseExtent || false;
        } else {
           options = {};
        }
        options.withChooseExtent = this.withChooseExtent;
        options.olMap = olMap;
        Action.prototype.initialize.apply(this, arguments);
        if (_.isNil(this.renderer)) {
            this.renderer = new RequestManagerInPlace(div, this.model, options);
        }

        var bbox = this.olMap.getView().calculateExtent(this.olMap.getSize());
        this.maxExtentAsGmlPolygon = '<Envelope srsName="' + this.olMap.getView().getProjection().getCode() + '" xmlns="http://www.opengis.net/gml">';
        this.maxExtentAsGmlPolygon += '<lowerCorner>' + bbox[0] + ' ' + bbox[1] + '</lowerCorner>';
        this.maxExtentAsGmlPolygon += '<upperCorner>' + bbox[2] + ' ' + bbox[3] + '</upperCorner>';
        this.maxExtentAsGmlPolygon += '</Envelope>';

        this.renderer.events.register('sendRequest', this, this.execute);
        this.renderer.events.register('unselect', this, this.unselect);
        this.renderer.events.register('fixSelectionLayer', this, this.fixSelectionLayer);
        this.renderer.events.register('flashSelectionLayer', this, this.flashSelectionLayer);
        this.renderer.events.register('undisplaySelectionLayer', this, this.undisplaySelectionLayer);
        this.renderer.events.register('addSelectionLayer', this, this.addSelectionLayer);
        this.olMap.on('moveend', this.refreshSelectionLayer.bind(this), this);
    },
    /**
     * Methode: addRequest
     * Ajoute une requête au gestionnaire.
     *
     * Paramètres:
     * request - {<Descartes.Request>} Requête à ajouter.
     */
    addRequest: function (request) {
        this.requests.push(request);
        this.renderer.draw(this.requests);
    },

    /**
     * Methode: removeRequests
     * Supprime un ensemble de requêtes du gestionnaire.
     *
     * Paramètres:
     * indexes - {Array(Integer)} Index des requêtes à supprimer.
     */
    removeRequests: function (indexes) {
        indexes = indexes.sort(function (a, b) {
            return b - a;
        });
        for (var i = 0, len = indexes.length; i < len; i++) {
            this.requests.splice(indexes[i], 1);
        }
        this.unselect();
        this.renderer.draw(this.requests);
    },
    /**
     * Methode: execute
     * Exécute une requête attributaire avec les critères portés par le modèle.
     *
     * :
     * L'exécution de la requête se concrétise par deux actions :
     * - Lancement de l'appel au service d'interrogation grâce à la classe <Descartes.Action.AjaxSelection>
     * - Affichage la couche de sélection graphique
     */
    execute: function () {
        var indexRequest = this.model.numRequest;

        this.useCurrentExtent = false;
        if (!_.isNil(this.model.useCurrentExtent)) {
        this.useCurrentExtent = this.model.useCurrentExtent;
        }
        this.useGazetteerExtent = false;
        if (!_.isNil(this.model.useGazetteerExtent)) {
            this.useGazetteerExtent = true;
        }

        var customExtentAsGmlPolygon;
        if (this.useCurrentExtent || this.useGazetteerExtent) {
            var bbox = [];
        if (this.useCurrentExtent) {
                bbox = this.olMap.getView().calculateExtent();
            } else if (this.useGazetteerExtent) {
                bbox = [this.model.useGazetteerExtent.xmin, this.model.useGazetteerExtent.ymin, this.model.useGazetteerExtent.xmax, this.model.useGazetteerExtent.ymax];
            }

            customExtentAsGmlPolygon = '<Envelope srsName="' + this.olMap.getView().getProjection().getCode() + '" xmlns="http://www.opengis.net/gml">';
            customExtentAsGmlPolygon += '<lowerCorner>' + bbox[0] + ' ' + bbox[1] + '</lowerCorner>';
            customExtentAsGmlPolygon += '<upperCorner>' + bbox[2] + ' ' + bbox[3] + '</upperCorner>';
            customExtentAsGmlPolygon += '</Envelope>';

            var bottomLeft = ol.extent.getBottomLeft(bbox);
            var topRight = ol.extent.getTopRight(bbox);
            var bottomRight = ol.extent.getBottomRight(bbox);
            var topLeft = ol.extent.getTopLeft(bbox);
            var ring = [
               [bottomLeft[0], bottomLeft[1]],
               [topLeft[0], topLeft[1]],
               [topRight[0], topRight[1]],
               [bottomRight[0], bottomRight[1]],
               [bottomLeft[0], bottomLeft[1]]
            ];
            var feature = new ol.Feature({
                geometry: new ol.geom.Polygon([ring])
            });
           var gml = new ol.format.GML({
            featureType: 'feature',
            featureNS: 'http://example.com/feature',
            srsName: this.olMap.getView().getProjection().getCode()
           });
           var nodeGML = gml.writeFeaturesNode([feature]);
           nodeGML.setAttribute('srsName', "http://www.opengis.net/gml/srs/epsg.xml#" + this.olMap.getView().getProjection().getCode());
           this.fluxGMLCustomExtent = new XMLSerializer().serializeToString(nodeGML.getElementsByTagName('Polygon')[0]);
        }

        var request = this.requests[indexRequest];

        var atLeastOneValue = false;
        _.each(this.model.values, function (value) {
            if (value.value !== null) {
                atLeastOneValue = true;
            }
        });

        var withVisibleMember = false;
        _.each(request.members, function (member) {
            if (member.visible) {
                withVisibleMember = true;
            }
        });

        if (atLeastOneValue || !withVisibleMember) {
            var extentAsGmlPolygon = this.maxExtentAsGmlPolygon;
            if (this.useCurrentExtent || this.useGazetteerExtent) {
                extentAsGmlPolygon = customExtentAsGmlPolygon;
            }
            var dataMask = request.getQueryWithBBOX(this.model.values, extentAsGmlPolygon, request.layer.resourceLayers[0].featureGeometryName);
            var fluxInfos = ExternalCallsUtils.writeQueryPostBody(ExternalCallsUtilsConstants.WFS_SERVICE, [request.layer], this.olMap);
            var infosJson = ExternalCallsUtils.writeGetFeatureExportRequestJson(ExternalCallsUtilsConstants.WFS_SERVICE, [request.layer], this.olMap, {dataMask: dataMask});

            if (fluxInfos !== null) {
                var requestParams = {
                    distantService: Descartes.FEATURE_SERVER,
                    infos: fluxInfos,
                    dataMask: dataMask,
                    infosJson: infosJson,
                    timeout: RequestManagerConstants.TIMEOUT
                };
                var attributesAliasSubstitution = {};
                var attributesAlias = request.layer.getAttributesAlias();
                if (!_.isNil(attributesAlias)) {
                    attributesAliasSubstitution[request.layer.id] = attributesAlias;
                }

                this.ajaxSelection = new AjaxSelection(this.resultUiParams, requestParams, this.olMap, {waitingMsg: this.waitingMsg, btnDisplaySelectionLayer: false, attributesAliasSubstitution: attributesAliasSubstitution});
                if (this.ajaxSelection.renderer) {
                     this.ajaxSelection.renderer.requestParams = requestParams;
                }
                this.renderer.activeDropdownMenu();
            } else {
                alert(this.getMessage('ERROR_FLUX_INFOS'));
                this.activeRequest = null;
                this.renderer.disableDropdownMenu();
            }

            this.unselect();

            //if (request.checkPropertyFilterType(this.model.values)) {
                if (this.useCurrentExtent || this.useGazetteerExtent) {
                    ResultLayer.refreshWithCustomExtent(request, this.olMap, this.alwaysExportSelectionLayers, this.model.values, this.fluxGMLCustomExtent);
                } else {
                ResultLayer.refresh(request, this.olMap, this.alwaysExportSelectionLayers, this.model.values);
                }
                this.activeRequest = request;

                if (ResultLayerConstants.blinkLayerTimerId) {
                    clearInterval(ResultLayerConstants.blinkLayerTimerId);
                }
                if (ResultLayerConstants.blinkLayerDelayId) {
                    clearTimeout(ResultLayerConstants.blinkLayerDelayId);
                }
                ResultLayerConstants.blinkLayerTimerId = setInterval(ResultLayerConstants.blinkLayer, 1000);
                ResultLayerConstants.blinkLayerDelayId = setTimeout(ResultLayerConstants.stopBlinkLayer, 10000);
            /*} else {
                ResultLayer.refresh(null, this.olMap);
                ResultLayer.removeMarker(this.olMap);
                this.activeRequest = null;
            }*/
        } else {
            alert(this.getMessage('MISSING_VALUES'));
            this.activeRequest = null;
            this.renderer.disableDropdownMenu();
        }
    },
    /**
     * Methode: refreshSelectionLayer
     * Rafraichit la couche graphique de sélection.
     */
    refreshSelectionLayer: function () {
        if (!ResultLayer.searchDataOnError && this.activeRequest !== null /*&& this.activeRequest.checkPropertyFilterType(this.model.values)*/) {
            if (this.useCurrentExtent || this.useGazetteerExtent) {
                ResultLayer.refreshWithCustomExtent(this.activeRequest, this.olMap, this.alwaysExportSelectionLayers, this.model.values, this.fluxGMLCustomExtent);
            } else {
            ResultLayer.refresh(this.activeRequest, this.olMap, this.alwaysExportSelectionLayers, this.model.values);
        }
        }
    },

    /**
     * Methode: unselect
     * Efface la couche graphique de sélection.
     */
    unselect: function () {
        if (ResultLayerConstants.blinkLayerTimerId) {
            clearInterval(ResultLayerConstants.blinkLayerTimerId);
            ResultLayerConstants.blinkLayerTimerId = null;
        }
        if (ResultLayerConstants.blinkLayerDelayId) {
            clearTimeout(ResultLayerConstants.blinkLayerDelayId);
            ResultLayerConstants.blinkLayerDelayId = null;
        }
        ResultLayer.refresh(null, this.olMap);
        ResultLayer.removeMarker(this.olMap);
        this.activeRequest = null;
    },
    /**
     * Methode: fixSelectionLayer
     * Fixe la couche graphique de sélection.
     */
    fixSelectionLayer: function () {
        if (ResultLayer.olLayersResult) {
            _.each(ResultLayer.olLayersResult, function (olLayerResult) {
                 olLayerResult.setVisible(true);
            });
            if (ResultLayerConstants.blinkLayerTimerId) {
                clearInterval(ResultLayerConstants.blinkLayerTimerId);
                ResultLayerConstants.blinkLayerTimerId = null;
            }
            if (ResultLayerConstants.blinkLayerDelayId) {
                clearTimeout(ResultLayerConstants.blinkLayerDelayId);
                ResultLayerConstants.blinkLayerDelayId = null;
            }
        }
    },
    /**
     * Methode: addSelectionLayer
     * Fixe la couche graphique de sélection.
     */
    addSelectionLayer: function () {
        var datas = this.ajaxSelection.renderer.model;
        var requestParams = this.ajaxSelection.renderer.requestParams;
        ResultLayer.addInLayersTree(datas, requestParams);
        this.unselect();
    },
    /**
     * Methode: flashSelectionLayer
     * Fait clignoter la couche graphique de sélection.
     */
    flashSelectionLayer: function () {
        if (ResultLayer.olLayersResult) {
            _.each(ResultLayer.olLayersResult, function (olLayerResult) {
                 olLayerResult.setVisible(false);
             });
            if (!ResultLayerConstants.blinkLayerTimerId) {
                ResultLayerConstants.blinkLayerTimerId = setInterval(ResultLayerConstants.blinkLayer, 1000);
            }
            if (!ResultLayerConstants.blinkLayerDelayId) {
                ResultLayerConstants.blinkLayerDelayId = setTimeout(ResultLayerConstants.stopBlinkLayer, 10000);
            }
        }
    },
    /**
     * Methode: undisplaySelectionLayer
     * Masquer la couche graphique de sélection.
     */
    undisplaySelectionLayer: function () {
        if (ResultLayer.olLayersResult) {
            _.each(ResultLayer.olLayersResult, function (olLayerResult) {
                 olLayerResult.setVisible(false);
            });
            if (ResultLayerConstants.blinkLayerTimerId) {
                clearInterval(ResultLayerConstants.blinkLayerTimerId);
                ResultLayerConstants.blinkLayerTimerId = null;
            }
            if (ResultLayerConstants.blinkLayerDelayId) {
                clearTimeout(ResultLayerConstants.blinkLayerDelayId);
                ResultLayerConstants.blinkLayerDelayId = null;
            }
        }
    },
    CLASS_NAME: 'Descartes.Action.RequestManager'
});
module.exports = Class;
