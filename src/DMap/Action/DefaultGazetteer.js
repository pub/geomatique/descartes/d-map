/* global MODE */
var _ = require('lodash');

var Utils = require('../Utils/DescartesUtils');
var GazetteerLevel = require('../Model/GazetteerLevel');
var DefaultGazetteerConstants = require('./DefaultGazetteerConstants');

var Gazetteer = require('./Gazetteer');


/**
 * Class: Descartes.Action.DefaultGazetteer
 * Classe permettant le recadrage de la carte suite au choix d'une entité administrative dans un ensemble de listes déroulantes imbriquées.
 *
 * Hérite de:
 * - <Descartes.Action.Gazetteer>
 */
var Class = Utils.Class(Gazetteer, {
    /**
     * Constructeur: Descartes.Action.DefaultGazetteer
     * Constructeur d'instances
     *
     * Paramètres:
     * div - {DOMElement|String} Elément DOM de la page accueillant l'interface associée ou identifiant de cet élement.
     * olMap - {ol.Map} Carte OpenLayers sur laquelle intervient le contrôleur.
     * proj - {String} Code de la projection de la carte.
     * initValue - {String} Code de la valeur de l'objet "père" des entités administratives du plus haut niveau de localisation (i.e. d'index 0).
     * startlevel - {<Descartes.Action.DefaultGazetteer.REGION> | <Descartes.Action.DefaultGazetteer.DEPARTEMENT> | <Descartes.Action.DefaultGazetteer.COMMUNE>}
     * Type d'entités administratives de la liste de plus haut niveau.
     * options - {Object} Objet optionnel contenant les propriétés à renseigner dans l'instance
     */
    initialize: function (div, olMap, proj, initValue, startlevel, options) {
        this.messages = Utils.loadMessages(this.CLASS_NAME, "Action");
        var displayOptions = {};
        if (options !== undefined) {
            _.extend(displayOptions, options.displayOptions);
            delete options.displayOptions;
        }
        var levels = [];
        if (startlevel === DefaultGazetteerConstants.REGION) {
            levels.push(new GazetteerLevel(this.getMessage("TITLE_MESSAGE_REGION"), this.getMessage("TITLE_ERROR_REGION"), this.getMessage("TITLE_NAME_REGION"), displayOptions));
        }
        if (startlevel <= DefaultGazetteerConstants.DEPARTEMENT) {
            levels.push(new GazetteerLevel(this.getMessage("TITLE_MESSAGE_DEPT"), this.getMessage("TITLE_ERROR_DEPT"), this.getMessage("TITLE_NAME_DEPT"), displayOptions));
        }
        if (startlevel <= DefaultGazetteerConstants.COMMUNE) {
            levels.push(new GazetteerLevel(this.getMessage("TITLE_MESSAGE_COMMUNE"), this.getMessage("TITLE_ERROR_COMMUNE"), this.getMessage("TITLE_NAME_COMMUNE"), displayOptions));
        }
        Gazetteer.prototype.initialize.apply(this, [div, olMap, proj, initValue, levels, options]);
    },
    CLASS_NAME: 'Descartes.Action.DefaultGazetteer'
});
module.exports = Class;
